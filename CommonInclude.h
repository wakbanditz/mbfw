/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CommonInclude.h
 @author  BmxIta FW dept
 @brief   Contains the common defines.
 @details

 ****************************************************************************
*/

#ifndef COMMONINCLUDE_H
#define COMMONINCLUDE_H

 #include <unistd.h>

/*! *******************************************************************************************************************
 * General defines
 * ********************************************************************************************************************
 */
// Macros
#define SAFE_DELETE(p)							if( (p) != 0 ) {delete (p); (p) = 0;}
#define SAFE_DELETE_ARRAY(p)					if( (p) != 0 ) {delete [] (p); (p) = 0;}
#define SAFE_FREE(p)							if( (p) != 0 ) {free(p); p = 0;}
#define msleep(ms)								usleep(ms * 1000)

// IPC general defines
#define MQ_MAX_NAME_LENGTH_BYTES				256
#define MQ_DEFAULT_PERMISSIONS					0660
#define MQ_DEFAULT_OPEN_RETRY_MSLEEP            500

#define ZERO_CHAR	'0'
#define A_CHAR		'A'

/*! *******************************************************************************************************************
 * Files and Folders defines
 * ********************************************************************************************************************
 */

#define BASE_WORKING_DIR       "/home/root/"
// configuration file
#define SERIAL_SETUP_FILE           BASE_WORKING_DIR "serialNumber.cfg"
#define COUNTERS_SETUP_FILE         BASE_WORKING_DIR "countersNumber.cfg"
#define RELEASE_SETUP_FILE          BASE_WORKING_DIR "releaseLabel.cfg"

#define CONF_FILE                   BASE_WORKING_DIR "mbfw.config"
#define LOGIN_SETUP_FILE            BASE_WORKING_DIR "loginSetup.cfg"

#define ERROR_FOLDER                BASE_WORKING_DIR "ERRORS/"
#define CAMERA_ERROR_FILE_NAME      ERROR_FOLDER "CAMERA.err"
#define NSH_ERROR_FILE_NAME         ERROR_FOLDER "NSH.err"
#define SECTION_ERROR_FILE_NAME     ERROR_FOLDER "SECTION.err"
#define PRESSURE_ERROR_FILE_NAME    ERROR_FOLDER "PRESSURE.err"
#define GENERAL_ERROR_FILE_NAME     ERROR_FOLDER "GENERAL.ERR"
#define MASTER_ERROR_FILE_NAME      ERROR_FOLDER "MASTERBOARD.err"

#define POSITIONS_FOLDER            BASE_WORKING_DIR "POSITIONS/"
#define CONF_CAMERA_FILE            POSITIONS_FOLDER "Camera_cal.cfg"
#define CONF_CAMERA_DEFAULT_FILE    POSITIONS_FOLDER "Camera_cal_default.cfg"
#define CONF_NSH_FILE               POSITIONS_FOLDER "NSHcalibration.cal"
#define CONF_NSH_DEFAULT_FILE       POSITIONS_FOLDER "NSHcalibration_default.cal"
#define CONF_PUMP_VOLUMES           POSITIONS_FOLDER "PumpVolume.cfg"

#define RUN_FOLDER                  BASE_WORKING_DIR "RUN/"

#define FANS_FOLDER                 BASE_WORKING_DIR "FANS/"

#define CAMERA_BATCH_FOLDER		BASE_WORKING_DIR "CAMERA/CAMERA_BATCH_FILES/"
#define	CAMERA_DEF_CONFIG_FILENAME	BASE_WORKING_DIR "CAMERA/Camera_def.conf"
#define	CAMERA_UPD_CONFIG_FILENAME	BASE_WORKING_DIR "CAMERA/Camera_upd.conf"

#define DEFAULT_BACKUP_DIR		BASE_WORKING_DIR "BACKUPS/InstrumentPack.tar"
#define DEFAULT_INSTR_PACK_PATH	BASE_WORKING_DIR "InstrumentPack.tar"

#define UPDATE_LOG_FOLDER			BASE_WORKING_DIR "UPDATE/"
#define UPDATE_LOG_PATH             UPDATE_LOG_FOLDER "UpdateLog.log"
#define UPDATE_TIME_PATH            UPDATE_LOG_FOLDER "updateTime.cfg"

#define FAN_TABLE_FILE			BASE_WORKING_DIR "fanTable.cfg"

#define COMMAND_FOLDER			BASE_WORKING_DIR "COMMANDS/"

#define APPLOGS_FOLDER			BASE_WORKING_DIR "APPLOGS/"

#define NSH_FOLDER              BASE_WORKING_DIR "NSH/"
#define NSH_SS_FILE             NSH_FOLDER "SolidStandard.cal"
#define NSH_OPTCHECK_FILE       NSH_FOLDER "OPTCheck.cfg"
#define NSH_OPTID_FILE          NSH_FOLDER "OPTID.cfg"

#define HASHTAG_SEPARATOR		"#"


//define for killing process in stand alone mode
#define KILL_PROCESS_STAND_ALONE_MODE
//#define KILL_TEST_PROC

// !!! DEFINE to be removed (delete final undescore to activate it)
#define __DEBUG_PROTOCOL__


/*! *******************************************************************************************************************
 * Connection Identifiers
 * ********************************************************************************************************************
 */
#define WEB_COMMERCIAL_CONNECTION		"COMMERCIAL"
#define WEB_SERVICE_CONNECTION			"SERVICE"
#define WEB_ALL_CONNECTION				"ALL"


/*! *******************************************************************************************************************
 * Login default values
 * ********************************************************************************************************************
 */
#define WEB_VALID_LOGIN_ID_VALUE					"Vidas"
#define WEB_VALID_LOGIN_PWD_VALUE					"Vidas"

/*! *******************************************************************************************************************
 * Configuration keys
 * ********************************************************************************************************************
 */
#define CFG_PROCESS_MONITOR_LOG_ENABLE              0
#define CFG_PROCESS_MONITOR_LOG_FILE                1
#define CFG_PROCESS_MONITOR_LOG_LEVEL               2
#define CFG_PROCESS_MONITOR_LOG_NUM_FILES           3

#define CFG_FW_CORE_LOG_ENABLE						4
#define CFG_FW_CORE_LOG_FILE						5
#define CFG_FW_CORE_LOG_LEVEL						6
#define CFG_FW_CORE_LOG_NUM_FILES					7

#define CFG_CAN_MANAGER_LOG_ENABLE					8
#define CFG_CAN_MANAGER_LOG_FILE					9
#define CFG_CAN_MANAGER_LOG_LEVEL					10
#define CFG_CAN_MANAGER_LOG_NUM_FILES				11

#define CFG_FW_CORE_SCT_A_ENABLE					20
#define CFG_FW_CORE_SCT_B_ENABLE					21

#define	CFG_FW_CORE_WEB_ENABLE						22
#define	CFG_FW_CORE_GPIO_IF_ENABLE					23
#define	CFG_FW_CORE_CAM_ENABLE						24
#define	CFG_FW_CORE_POWER_MAN_ENABLE				25
#define	CFG_FW_CORE_EEPROM_ENABLE					26
#define	CFG_FW_CORE_FAN_CTRL_ENABLE					27
#define	CFG_FW_CORE_AUDIO_CODEC_ENABLE				28
#define	CFG_FW_CORE_GPIO_EXP_SEC_ENABLE				29
#define	CFG_FW_CORE_LED_DRV_ENABLE					30
#define CFG_FW_CORE_NSH_ENABLE						31
#define CFG_FW_CORE_STEPPER_MOTOR_ENABLE			32
#define CFG_FW_CORE_ENCODER_COUNTER_ENABLE			33
#define CFG_FW_CORE_OPER_MAN_ENABLE					34
#define CFG_FW_CORE_FOLDER_MAN_ENABLE				35
#define CFG_FW_CORE_CAM_DEBUG_TEST_ENABLE			36
#define CFG_FW_CORE_TESTMANAGER_ENABLE				37
#define CFG_FW_CORE_PRESSURE_UDP_ENABLE				38
#define	CFG_FW_CORE_GPIO_EXP_LED_ENABLE				39
#define CFG_FW_CORE_EVENTRECORDER_ENABLE			40

#define CFG_POFF_MANAGER_LOG_ENABLE					50
#define CFG_POFF_MANAGER_LOG_FILE					51
#define CFG_POFF_MANAGER_LOG_LEVEL					52
#define CFG_POFF_MANAGER_LOG_NUM_FILES				53

#define CFG_TEST_APP_LOG_ENABLE                     60
#define CFG_TEST_APP_LOG_FILE                       61
#define CFG_TEST_APP_LOG_LEVEL                      62
#define CFG_TEST_APP_LOG_NUM_FILES                  63

#define	CFG_TEST_APP_WEB_ENABLE						70
#define	CFG_TEST_APP_GPIO_IF_ENABLE					71
#define	CFG_TEST_APP_CAM_ENABLE						72
#define	CFG_TEST_APP_POWER_MAN_ENABLE				73
#define	CFG_TEST_APP_EEPROM_ENABLE					74
#define	CFG_TEST_APP_FAN_CTRL_ENABLE				75
#define	CFG_TEST_APP_AUDIO_CODEC_ENABLE				76
#define	CFG_TEST_APP_GPIO_EXP_SEC_ENABLE			77
#define	CFG_TEST_APP_LED_DRV_ENABLE					78
#define CFG_TEST_APP_NSH_ENABLE						79
#define CFG_TEST_APP_STEPPER_MOTOR_ENABLE			80
#define CFG_TEST_APP_ENCODER_COUNTER_ENABLE			81
#define	CFG_TEST_APP_GPIO_EXP_LED_ENABLE			87
#define CFG_TEST_APP_SCT_A_ENABLE					88
#define CFG_TEST_APP_SCT_B_ENABLE					89

#define CFG_WEB_ADDRESS_DEFAULT                     90
#define CFG_WEB_PORT_DEFAULT                        91

/*! *******************************************************************************************************************
 * Section Boards management
 * ********************************************************************************************************************
 */
// Section general defines

#define SCT_NUM_TOT_SECTIONS			2
#define SCT_A_ID						0
#define SCT_B_ID						1
#define SCT_NONE_ID						SCT_NUM_TOT_SECTIONS

#define SCT_SLOT_1_ID						0
#define SCT_SLOT_2_ID						1
#define SCT_SLOT_3_ID						2
#define SCT_SLOT_4_ID						3
#define SCT_SLOT_5_ID						4
#define SCT_SLOT_6_ID						5
#define SCT_NUM_TOT_SLOTS					6

#define SCT_NUM_TOT_MQ_CANLINK				7

#define SCT_IDX_MQ_GPF_CANLINK				0 //where gpf means general protocol format
#define SCT_IDX_MQ_BPF_CANLINK				1 //where bpf means binary protocol format
#define SCT_NUM_TOT_TX_MQ					2
// SLOT_NONE_ID is used when a specific SLOT is not linked to the event or if all the slots are linked to the section
#define SCT_SLOT_NONE_ID					SCT_NUM_TOT_SLOTS

#define SCT_SLOT_NUM_WELLS					10

/** *******************************************************************************************************************
 * Message Queues management
 * ********************************************************************************************************************
 */
// Section IPC defines
#define SCT_MQ_CANLINK_PERMISSIONS				MQ_DEFAULT_PERMISSIONS
#define SCT_MQ_OPEN_RETRY_MSLEEP				MQ_DEFAULT_OPEN_RETRY_MSLEEP
#define SCT_MQ_TIMEOUT_RX                       1000

#define SCT_MQ_CANLINK_MAX_MSG_SIZE             256 //To use in create
#define SCT_MQ_CANLINK_MSG_BUFFER_SIZE          SCT_MQ_CANLINK_MAX_MSG_SIZE + 16//to use in receive

#define SCT_MQ_CANLINK_MAX_NUM_MESSAGES         256

#define MQ_CANBOARD_MAX_MSG_SIZE                256//1024//To use in create
#define MQ_CANBOARD_MAX_MSG_SIZE_BUFF           MQ_CANBOARD_MAX_MSG_SIZE + 16//to use in receive

#define LOCAL_MQ_CANBOARD_MAX_MESSAGES          256//64
#define LOCAL_MQ_CANBOARD_MAX_MESSAGES_BIN      256

#define LOCAL_MQ_CANLINK_BIN_MSGSIZE            32
#define LOCAL_MQ_CANLINK_BIN_MSGSIZE_BUFF       LOCAL_MQ_CANLINK_BIN_MSGSIZE + 16

#define DIM_SECTIONLINK_MSG_BUFFER_SIZE         MQ_CANBOARD_MAX_MSG_SIZE
#define DIM_SECTIONLINK_BIN_PROTO_SIZE          8
#define DIM_SECTIONLINK_BIN_PROTO_MAXSIZE       DIM_SECTIONLINK_BIN_PROTO_SIZE + 16

//Interprocess MQ
#define SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_A			"/mq-can-link-to-manager-tx-a"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_A			"/mq-can-manager-to-link-rx-a"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX1_A		"/mq-can-manager-to-link-rx-aux1-a"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX2_A		"/mq-can-manager-to-link-rx-aux2-a"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX3_A		"/mq-can-manager-to-link-rx-aux3-a"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX4_A		"/mq-can-manager-to-link-rx-aux4-a"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX5_A		"/mq-can-manager-to-link-rx-aux5-a"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX6_A		"/mq-can-manager-to-link-rx-aux6-a"
#define SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_UPFW_A		"/mq-can-link-to-manager-tx-upfw-a"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_UPFW_A		"/mq-can-manager-to-link-rx-upfw-a"


#define SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_B			"/mq-can-link-to-manager-tx-b"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_B			"/mq-can-manager-to-link-rx-b"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX1_B		"/mq-can-manager-to-link-rx-aux1-b"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX2_B		"/mq-can-manager-to-link-rx-aux2-b"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX3_B		"/mq-can-manager-to-link-rx-aux3-b"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX4_B		"/mq-can-manager-to-link-rx-aux4-b"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX5_B		"/mq-can-manager-to-link-rx-aux5-b"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX6_B		"/mq-can-manager-to-link-rx-aux6-b"
#define SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_UPFW_B		"/mq-can-link-to-manager-tx-upfw-b"
#define SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_UPFW_B		"/mq-can-manager-to-link-rx-upfw-b"


//local MQ
#define SCT_MQ_CAN_LINK_NAME_A_GENERAL_ANSW             "/mq-can-link-local-a-general-answ"
#define SCT_MQ_CAN_LINK_NAME_B_GENERAL_ANSW             "/mq-can-link-local-b-general-answ"
#define SCT_MQ_CAN_LINK_NAME_A_GENERAL_AUTOANSW			"/mq-can-link-local-a-general-autoansw"
#define SCT_MQ_CAN_LINK_NAME_B_GENERAL_AUTOANSW			"/mq-can-link-local-b-general-autoansw"

#define SCT_MQ_CAN_LINK_NAME_A_TRAY_ANSW                "/mq-can-link-local-a-tray-answ"
#define SCT_MQ_CAN_LINK_NAME_B_TRAY_ANSW                "/mq-can-link-local-b-tray-answ"
#define SCT_MQ_CAN_LINK_NAME_A_TRAY_AUTOANSW			"/mq-can-link-local-a-tray-autoansw"
#define SCT_MQ_CAN_LINK_NAME_B_TRAY_AUTOANSW			"/mq-can-link-local-b-tray-autoansw"

#define SCT_MQ_CAN_LINK_NAME_A_PUMP_ANSW                "/mq-can-link-local-a-pump-answ"
#define SCT_MQ_CAN_LINK_NAME_B_PUMP_ANSW                "/mq-can-link-local-b-pump-answ"
#define SCT_MQ_CAN_LINK_NAME_A_PUMP_AUTOANSW			"/mq-can-link-local-a-pump-autoansw"
#define SCT_MQ_CAN_LINK_NAME_B_PUMP_AUTOANSW			"/mq-can-link-local-b-pump-autoansw"

#define SCT_MQ_CAN_LINK_NAME_A_TOWER_ANSW               "/mq-can-link-local-a-tower-answ"
#define SCT_MQ_CAN_LINK_NAME_B_TOWER_ANSW               "/mq-can-link-local-b-tower-answ"
#define SCT_MQ_CAN_LINK_NAME_A_TOWER_AUTOANSW			"/mq-can-link-local-a-tower-autoansw"
#define SCT_MQ_CAN_LINK_NAME_B_TOWER_AUTOANSW			"/mq-can-link-local-b-tower-autoansw"

#define SCT_MQ_CAN_LINK_NAME_A_SPR_ANSW				"/mq-can-link-local-a-spr-answ"
#define SCT_MQ_CAN_LINK_NAME_B_SPR_ANSW				"/mq-can-link-local-b-spr-answ"
#define SCT_MQ_CAN_LINK_NAME_A_SPR_AUTOANSW			"/mq-can-link-local-a-spr-autoansw"
#define SCT_MQ_CAN_LINK_NAME_B_SPR_AUTOANSW			"/mq-can-link-local-b-spr-autoansw"

#define SCT_MQ_CAN_LINK_NAME_A_PIB_ANSW				"/mq-can-link-local-a-pib-answ"
#define SCT_MQ_CAN_LINK_NAME_B_PIB_ANSW				"/mq-can-link-local-b-pib-answ"
#define SCT_MQ_CAN_LINK_NAME_A_PIB_AUTOANSW			"/mq-can-link-local-a-pib-autoansw"
#define SCT_MQ_CAN_LINK_NAME_B_PIB_AUTOANSW			"/mq-can-link-local-b-pib-autoansw"

#define SCT_MQ_CAN_LINK_NAME_A_UPFW_BIN_ANSW		"/mq-can-link-local-a-upfw-answ"
#define SCT_MQ_CAN_LINK_NAME_B_UPFW_BIN_ANSW		"/mq-can-link-local-b-upfw-answ"

/*! *******************************************************************************************************************
 * Camera management
 * ********************************************************************************************************************
 */

#define CAMERA_ID									(SCT_NONE_ID + 1)

#define CAM_INTERFACE_DEFAULT_READ_TIMEOUT_MSEC			1
#define CAM_INTERFACE_DEFAULT_NUMBITSPERWORD			8

#define	SAMPLE_DETECTION_TEST_RESULT_LIQUID_X0			"LiquidResultsX0.csv"
#define	SAMPLE_DETECTION_TEST_RESULT_NO_LIQUID_X0		"NoLiquidResultsX0.csv"
#define	SAMPLE_DETECTION_TEST_RESULT_LIQUID_X3			"LiquidResultsX3.csv"
#define	SAMPLE_DETECTION_TEST_RESULT_NO_LIQUID_X3		"NoLiquidResultsX3.csv"
#define SAMPLE_DETECTION_TEST_BAR_WIDTH					50


/*! *******************************************************************************************************************
 * NSH Motor management
 * ********************************************************************************************************************
 */

#define NSH_ID								SCT_NONE_ID

#define NSHM_L6472_REG_NUM					34
#define NSHM_HOME_FAST_SPEED				1400    // 800 was ok in step/sec
#define NSHM_MOVEMENT_TIMEOUT				15000   // in msecs
#define NSHM_NOP							0x00


/*! *******************************************************************************************************************
 * Gpio Interface management
 * ********************************************************************************************************************
 */
#define GPIO_VAL_LOW						0
#define GPIO_VAL_HIGH						1
#define GPIO_VAL_UNDEF						2


/*! *******************************************************************************************************************
 * WorkFlowManager management
 * ********************************************************************************************************************
 */
#define WF_MQ_OPMAN_PERMISSIONS					MQ_DEFAULT_PERMISSIONS
#define WF_MQ_OPMAN_MAX_NUM_MESSAGES			64
#define WF_MQ_OPMAN_MAX_MSG_SIZE				64
#define WF_MQ_OPMAN_MSG_BUFFER_SIZE				WF_MQ_OPMAN_MAX_MSG_SIZE + 16
#define WF_MQ_OPMAN_TO_POSTMAN_NAME				"/mq-wf-opman-to-postman"


/*! *******************************************************************************************************************
 * I2C defines
 * ********************************************************************************************************************
 */

#define I2C_TEST_EEPROM_DEVICE_ADDRESS			0x51
#define I2C_TEST_UCD9090_DEVICE_ADDRESS			0x65
#define I2C_TEST_PCA9633_DEVICE_ADDRESS         0x1F
#ifdef PCA9952_REG_CONF
#define I2C_TEST_PCALED_DEVICE_ADDRESS         0x64
#else
#define I2C_TEST_PCALED_DEVICE_ADDRESS         0x65
#endif
#define I2C_TEST_ADT7470_DEVICE_ADDRESS         0x2C
#define I2C_TEST_AT24C256C_DEVICE_ADDRESS		0x50
#define I2C_TEST_PCA9537_DEVICE_ADDRESS			0x49

/*! *******************************************************************************************************************
 * CAN defines
 * ********************************************************************************************************************
 */
#define MAX_CAN_ID_LIST						14

#define CAN_SECTION_A_BOARD_ID_TX			0x0000
#define CAN_SECTION_B_BOARD_ID_TX			0x0001
#define CAN_MASTER_TO_SECTION_A_UPFW_TX		0x00001000
#define CAN_MASTER_TO_SECTION_B_UPFW_TX		0x00002000


#define CAN_SECTION_A_BOARD_ID_MASTER		0x0020
#define CAN_SECTION_A_BOARD_ID_CHANNEL_1	0x0101
#define CAN_SECTION_A_BOARD_ID_CHANNEL_2	0x0102
#define CAN_SECTION_A_BOARD_ID_CHANNEL_3	0x0104
#define CAN_SECTION_A_BOARD_ID_CHANNEL_4	0x0401
#define CAN_SECTION_A_BOARD_ID_CHANNEL_5	0x0402
#define CAN_SECTION_A_BOARD_ID_CHANNEL_6	0x0404
#define CAN_SECTION_A_BOARD_ID_UPFW			0x00000100

#define CAN_SECTION_B_BOARD_ID_MASTER		0x0021
#define CAN_SECTION_B_BOARD_ID_CHANNEL_1	0x0201
#define CAN_SECTION_B_BOARD_ID_CHANNEL_2	0x0202
#define CAN_SECTION_B_BOARD_ID_CHANNEL_3	0x0204
#define CAN_SECTION_B_BOARD_ID_CHANNEL_4	0x0801
#define CAN_SECTION_B_BOARD_ID_CHANNEL_5	0x0802
#define CAN_SECTION_B_BOARD_ID_CHANNEL_6	0x0804
#define CAN_SECTION_B_BOARD_ID_UPFW			0x00000200


/*! *******************************************************************************************************************
 * Section Protocol defines
 * ********************************************************************************************************************
 */

#define INSTRUMENT_ID		(CAMERA_ID + 1)

//Timeout for the receiveing messages from the sections
#define TIMEOUT_PROTO_CMD_GETSENSOR			2000

//Calibration position offset
/*! *******************************************************************************************************************
 * Sensor defines
 * ********************************************************************************************************************
 */
#define OP_GET_SENSOR_CMD_NUM_PRESS	6

/*! *******************************************************************************************************************
 * Protocol defines
 * ********************************************************************************************************************
 */
#define MAX_PROTOCOL_GLOBALS_LEN	1024
#define MAX_PROTOCOL_LOCALS_LEN		1024

#define J_MAXMIN_VECTOR_SIZE		8

/*! *******************************************************************************************************************
 * LED defines
 * ********************************************************************************************************************
 */
#define GREEN_COLOR_STRING	"GREEN"
#define ORANGE_COLOR_STRING	"ORANGE"
#define RED_COLOR_STRING	"RED"
#define BLUE_COLOR_STRING	"BLUE"

#define ON_MODE_STRING		"ON"
#define OFF_MODE_STRING		"OFF"
#define BLK1_MODE_STRING	"BLK1"
#define BLK2_MODE_STRING	"BLK2"

/*! *******************************************************************************************************************
 * PRESSURE defines
 * ********************************************************************************************************************
 */
#define RAW_DATA_HEADER_ADC_VALUE	33554431	// value > 24 bits (16.777.215)

/*! *******************************************************************************************************************
 * Timeout commands requests
 * ********************************************************************************************************************
 */
#define	TH_MOVE_CMD_TIMEOUT			0
#define TH_GETPOSITION_CMD_TIMEOUT	0
#define TH_GETCALIBRATION_CMD_TIMEOUT	0

/*! *******************************************************************************************************************
 * ERROR defines
 * ********************************************************************************************************************
 */
enum
{
    GENERAL_ERROR_LEVEL_NONE    = 0,
    GENERAL_ERROR_LEVEL_WARNING = 1,
    GENERAL_ERROR_LEVEL_ERROR   = 2,
    GENERAL_ERROR_LEVEL_ALARM   = 3
};


#endif // COMMONINCLUDE_H
