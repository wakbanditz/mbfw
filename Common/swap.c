/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    swap.c
 @author  BmxIta FW dept
 @brief   Module containing the swap functions
 @details

 ****************************************************************************
*/

#include "swap.h"

void Swap32(unsigned char *ptuLong)
{
    unsigned char *ptCh,tmpCh;
    ptCh=(unsigned char*)ptuLong;
    tmpCh=ptCh[0];
    ptCh[0]=ptCh[3];
    ptCh[3]=tmpCh;
    tmpCh=ptCh[2];
    ptCh[2]=ptCh[1];
    ptCh[1]=tmpCh;
}

void Swap16(unsigned char *ptuShort)
{
	unsigned char *ptCh,tmpCh;
	ptCh=(unsigned char*)ptuShort;
	tmpCh=ptCh[0];
	ptCh[0]=ptCh[1];
	ptCh[1]=tmpCh;
}


uint16_t swap16Linux(uint16_t pusData)
{
	uint8_t		ubTmp[2];
	uint16_t	usres;

	ubTmp[0] = pusData;
	ubTmp[1] = pusData>>8;


	usres = ubTmp[0]<<8;
	usres |= ubTmp[1];
	return usres;
}
