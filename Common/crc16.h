/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    crc16.h
 @author  BmxIta FW dept
 @brief   Contains the functions for the CRC16 algorithm.
 @details

 ****************************************************************************
*/

#ifndef _CRC16_H_
#define _CRC16_H_

#ifdef __cplusplus  /* extern "C" */
extern "C" {
#endif

#define CRC16_INIT_VALUE 0xFFFF
#define CRC16_INIT_VALUE0 0x0
#define CRC16_XORVALUE 0xFFFF

/*!**************************************************************
* function : crc16_hash 
	purpose  : the function performs the crc16 algorithm  
						 using the polynomial 0x1021 
  input 	 : *buf (pointer void), buffer to calculate the crc
  				 : len  (integer) , len of buffer in bytes
  output	 : crc  (unsigned short) , result
******************************************************************/
unsigned short crc16_hash(const void *buf, int len);

void crc16_seq(short *crc, unsigned char m);

#ifdef __cplusplus  /* extern "C" */
}
#endif

#endif
