/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    StatusInclude.h
 @author  BmxIta FW dept
 @brief   Contains the Status defines.
 @details

 ****************************************************************************
*/

#ifndef STATUSINCLUDE_H
#define STATUSINCLUDE_H

/*! *******************************************************************************************************************
 * Device Status
 * ********************************************************************************************************************
 */

#include <string>
#include <vector>

typedef enum
{
    eIdModule   = 1,
    eIdSectionA = 2,
    eIdSectionB = 3,
    eIdNsh      = 4,
    eIdCamera   = 5,
    eIdMax      = 6
} eDeviceId;

typedef enum
{
    eStatusNull			= 0x0000,
    eStatusUnknown		= 0x0001,
    eStatusInit 		= 0x0002,
    eStatusIdle			= 0x0004,
    eStatusUnload		= 0x0008,
    eStatusProtocol		= 0x0010,
    eStatusError		= 0x0020,
    eStatusDisable		= 0x0040,
    eStatusSleep		= 0x0080,
    eStatusReadsec      = 0x0100
} eDeviceStatus;


// alphabetical equivalent in the same order of the numeric enum
const std::vector<std::string> global_strDeviceStatus =
{
	"UNKNOWN",
	"INIT",
	"IDLE",
    "UNLOAD",
    "PROCESSING",
	"ERROR",
	"DISABLED",
    "SLEEP",
    "PREPROCESSING"
};

#define MAINTENANCE_STATUS_STRING	"MAINTENANCE"

#define ALL_STATUS_DEVICE	"ALL"
#define BACK_STATUS_DEVICE	"BACK"
#define NONE_STATUS_DEVICE	"NONE"

typedef enum
{
	eLockOff	= 0,
	eLockOn	 	= 1
} eDeviceLockStatus;

typedef enum
{
	eSectionInit =			'0',
	eSectionIdle =			'1',
	eSectionMaintenance =	'2',
	eSectionProtocol =		'3',
	eSectionPause =			'4',
	eSectionSleep =			'5',
	eSectionDisable =		'6',
    eSectionOther =			'7',
    eSectionWaitForSerialUpgrade = 'B',
    eSectionWaitForCanUpgrade = 'C'
}eSectionStatus;



#endif // STATUSINCLUDE_H
