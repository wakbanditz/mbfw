/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanSender.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CanSender class.
 @details

 ****************************************************************************
*/

#include "CanSender.h"
#include "CommonInclude.h"

#include "CanBuffer.h"
#include "CanIfr.h"

/* ********************************************************************************************************************
 * CanSenderThread class implementation
 * ********************************************************************************************************************
 */
CanSenderThread::CanSenderThread()
{
	m_pLogger = NULL;
	m_pCanInterface = NULL;
    m_pCanBuffer = NULL;
    m_pcommonMutex = NULL;

    float freq = (float)FREQ_CAN;
    // Cal period in useconds
    float periodBitCanBus = (float)(1/freq)*1000000;
    // Calc time to send a frame: a can frame is composed by 16 byte
    m_timeFrameCanus = periodBitCanBus * 8 * 16;
    // Add an overhead
    m_timeFrameCanus = m_timeFrameCanus *2;
}

CanSenderThread::~CanSenderThread()
{

}

int CanSenderThread::init(Log *pLogger, CanIfr *pCanInterface, CanBuffer *pCanBuffer)
{
    if ( (pLogger == NULL) || (pCanInterface == NULL) || (pCanBuffer == NULL) )
	{
		return 1;
	}
	else
	{
		m_pLogger = pLogger;
		m_pCanInterface = pCanInterface;
        m_pCanBuffer = pCanBuffer;
	}

	#ifdef	CAN_LOG_CANFRAME_TIMESTAMP
	char nameFile[1024];
	snprintf(nameFile, 1024,"CanManagerTimeFrame.csv");
	m_canTimeLogger.enable( true );
	m_canTimeLogger.setLevel(6);
	m_canTimeLogger.setFile(nameFile);
	m_canTimeLogger.setAppName("CanManager");
	m_canTimeLogger.setMaxNumFiles(5);
	#endif
	return 0;
}

int CanSenderThread::setMutex(Mutex *pmutex)
{
    if (pmutex == NULL)
    {
        return -1;
    }

    m_pcommonMutex = pmutex;
    return 0;
}

void CanSenderThread::beforeWorkerThread()
{
	m_pLogger->log(LOG_INFO,"CanTx: starting");
}

//#define SCT_DEBUG_CAN_ENABLE_SENDER_LOG 1
int CanSenderThread::workerThread()
{
#ifdef SECTION_CAN_SEND_FRAME_WITH_TIMESTAMP
	uint32_t nNumCountTxMsg = 0;
#elif (SCT_DEBUG_CAN_ENABLE_SENDER_LOG != 0)
	uint32_t nNumCountTxMsg = 0;
#endif

	while( isRunning() )
	{
		#ifdef SECTION_CAN_SEND_FRAME_WITH_TIMESTAMP

		t_canFrame	canFrame;
		int res = m_pCanBuffer->getDataTS(&canFrame);
		if ( res > 0 )
		{
			nNumCountTxMsg++;
			m_pLogger->log(LOG_INFO, "CanTx: ID[0x%x], n%d sent", canFrame.Frame.can_id, nNumCountTxMsg);
			res = m_pCanInterface->sendFrame(canFrame.Frame.can_id, canFrame.Frame.data, canFrame.Frame.len);
			#ifdef	SCT_DEBUG_CAN_ENABLE_SENDER_LOG
			m_canTimeLogger.log(LOG_INFO,
								"CanTx: TX n=%u, sn=%u, ID[0x%x], ts=%s",
								nNumCountTxMsg, canFrame.Frame.data[0], canFrame.Frame.can_id, canFrame.time_msg);
			#endif
		}

		#else

        can_frame  *pCanFrame;

        int res = m_pCanBuffer->getData(pCanFrame);
        if (res >= 0)
		{
            m_pcommonMutex->lock();

			#ifdef SCT_DEBUG_CAN_ENABLE_SENDER_LOG
			nNumCountTxMsg++;
			m_pLogger->log(LOG_INFO,"CanSenderThread::workerThread: ID[0x%x], n%d sent", canFrame.can_id, nNumCountTxMsg);
			#endif

//            m_pLogger->log(LOG_DEBUG_IPER_PARANOIC, "CanSenderThread: request sendFrame %d", pCanFrame->can_dlc);

            res = m_pCanInterface->sendFrame(pCanFrame->can_id, pCanFrame->data, pCanFrame->can_dlc);
            if (res < 0)
            {
                m_pLogger->log(LOG_ERR, "CanSenderThread: sendFrame FAILED  %d %d", res, pCanFrame->can_dlc);
            }
/*            else
            {
                m_pLogger->log(LOG_DEBUG_PARANOIC, "CanSenderThread: sendFrame %d %d", res, pCanFrame->can_dlc);
            } */
            m_pcommonMutex->unlock();
		}

		#endif

        usleep(TO_WT_CANSENDERTHREAD_US);
	}
    m_pLogger->log(LOG_INFO, "CanSenderThread: exit");
    return 0;
}

void CanSenderThread::afterWorkerThread()
{
	m_pLogger->log(LOG_INFO,"CanTx: closing");
}


/* ********************************************************************************************************************
 * CanSenderManagerThread class implementation
 * ********************************************************************************************************************
 */
CanSenderManagerThread::CanSenderManagerThread()
{
	m_pCanInterface = NULL;
	m_pLogger = NULL;
	m_pcanBuffer = NULL;
	m_pcanMsgQueue = NULL;
    m_pcommonMutex = NULL;
}

CanSenderManagerThread::~CanSenderManagerThread()
{
	/* Nothing to do yet */
}

int CanSenderManagerThread::init(Log* pLogger, CanIfr* pCanInterface, CanBuffer* pCanBuffer,
                                 MessageQueue* pCanMsgQueue, const char* sName)
{
	if ( ( pLogger == NULL ) || ( pCanInterface == NULL ) ||
         ( pCanBuffer == NULL ) || ( pCanMsgQueue == NULL )	|| ( sName == NULL ) )
	{
		return -1;
	}
	else
	{
		m_pLogger = pLogger;
		m_pCanInterface = pCanInterface;
        m_pcanBuffer = pCanBuffer;
        m_pcanMsgQueue = pCanMsgQueue;
		snprintf(m_sNameMsgQueue, MQ_MAX_NAME_LENGTH_BYTES-1, "%s", sName);
	}
	return 0;
}

int CanSenderManagerThread::setMutex(Mutex *pmutex)
{
    if (pmutex == NULL)
    {
        return -1;
    }

    m_pcommonMutex = pmutex;
    return 0;
}

void CanSenderManagerThread::waitForMsgQueue( void )
{
	while (1)
	{
		bool bRet = m_pcanMsgQueue->open(m_sNameMsgQueue, O_RDONLY);
		if ( bRet == false )
		{
			m_pLogger->log(LOG_ERR, "CanTxManager::waitForMsgQueue: unable to open MSG QUEUE <%s>", m_sNameMsgQueue);
		}
		else
		{
			m_pLogger->log(LOG_DEBUG_PARANOIC, "CanTxManager::waitForMsgQueue: MSG QUEUE <%s> opened", m_sNameMsgQueue);
			break;
		}
		msleep(SCT_MQ_OPEN_RETRY_MSLEEP);
	}
}

int CanSenderManagerThread::closeMQ(void)
{
	bool bRet = m_pcanMsgQueue->close();
	if ( bRet == false )
	{
		m_pLogger->log(LOG_ERR, "CanTxManager: unable to close MQ <%s>", m_sNameMsgQueue);
	}

	return 0;
}

void CanSenderManagerThread::beforeWorkerThread()
{
	m_pLogger->log(LOG_INFO,"CanTxManager: starting");
}

int CanSenderManagerThread::workerThread()
{

	char sInBuffer[SCT_MQ_CANLINK_MSG_BUFFER_SIZE];

	waitForMsgQueue();

	while ( isRunning() )
	{
		unsigned int nMsgPriority;

		if ( m_pcanMsgQueue->retrieveCurrentNumMessages() < 0 )
		{
            waitForMsgQueue();
		}
		else
		{
            // wait until a message has been received
			int nNumBytesReceived = m_pcanMsgQueue->receive(sInBuffer,
															SCT_MQ_CANLINK_MSG_BUFFER_SIZE,
															&nMsgPriority,                                                           
                                                            0);
			if ( nNumBytesReceived == -1 )
			{
				if ( errno == ETIMEDOUT )
				{
					m_pLogger->log(LOG_DEBUG_PARANOIC, "CanTxManager: TIME-OUT on mq0_timedsend");
				}
				else
				{
					m_pLogger->log(LOG_ERR, "CanTxManager: error from mq0_timedsend");
				}

				// Close MQTX because if the MQ no more exists the only way to unlock is by closing it
				bool bRet = m_pcanMsgQueue->close();
				if ( bRet == false )
				{
					m_pLogger->log(LOG_ERR, "CanTxManager: unable to close %s", m_sNameMsgQueue);
				}
				waitForMsgQueue();
			}
			else
			{
                m_pcommonMutex->lock();

				#ifdef	SECTION_CAN_SEND_FRAME_WITH_TIMESTAMP
                    t_canFrame  canFrame;
                    memcpy(&canFrame, &sInBuffer, sizeof(t_canFrame));
                    m_pcanBuffer->setDataTS(&canFrame);
				#else
                    t_canFrame *  pCanFrame;
                    pCanFrame = (t_canFrame *)&sInBuffer;
                    m_pcanBuffer->setData(&pCanFrame->Frame);
                #endif

                m_pcommonMutex->unlock();
			}
		}

        // usleep(TO_WT_CANSENDERMANAGERTHREAD_US);
	}

	return 0;
}

void CanSenderManagerThread::afterWorkerThread()
{
	m_pLogger->log(LOG_INFO,"CanTxManager: closing");
}


/* ********************************************************************************************************************
 * CanSenderManager class implementation
 * ********************************************************************************************************************
 */
CanSenderManager::CanSenderManager()
{
	m_pLogger = NULL;
	m_pCanInterface = NULL;
	m_enableA = true;
	m_enableB = true;
}

CanSenderManager::~CanSenderManager()
{
	/* Nothing to do yet */
}

int CanSenderManager::init( Log *pLogger, CanIfr *pCanIfr, bool enableA, bool enableB)
{
    if ( ( pLogger == NULL ) || ( pCanIfr == NULL )	)
	{
            return -1;
	}
	else if(enableA == false && enableB == false)
	{
            m_pLogger = pLogger;
            return -1;
	}
	else
	{
            m_pLogger = pLogger;
            m_pCanInterface = pCanIfr;
            m_enableA = enableA;
            m_enableB = enableB;
	}

	if (m_enableA)
	{
        m_pSenderToSectionA.init( m_pLogger, m_pCanInterface, &m_canTxBuffer, &m_CanMsgQueueA,
                              SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_A );
        // UpFw Protocol LINK
        m_pSenderToSectionUpFwA.init(m_pLogger, m_pCanInterface, &m_canTxBuffer, &m_CanMsgQueueUpFwA,
                                SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_UPFW_A);

        m_pSenderToSectionA.setMutex(&m_localMutexSender);
        m_pSenderToSectionUpFwA.setMutex(&m_localMutexSender);
	}

	if (m_enableB)
	{
        // ASCII Protocol LINK
        m_pSenderToSectionB.init( m_pLogger, m_pCanInterface, &m_canTxBuffer, &m_CanMsgQueueB,
								  SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_B );
		// UpFw Protocol LINK
        m_pSenderToSectionUpFwB.init(m_pLogger, m_pCanInterface, &m_canTxBuffer, &m_CanMsgQueueUpFwB,
								SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_UPFW_B);


        m_pSenderToSectionB.setMutex(&m_localMutexSender);
        m_pSenderToSectionUpFwB.setMutex(&m_localMutexSender);
	}

    int res = m_canSender.init(m_pLogger, m_pCanInterface, &m_canTxBuffer );
    m_canSender.setMutex(&m_localMutexSender);

    if (res != 0)
    {
        m_pLogger->log(LOG_ERR, "CanSenderThread initilization failed");
        return -1;
    }

    return 0;
}

bool CanSenderManager::startThreads(void)
{
	if (m_enableA)
	{
		if (!m_pSenderToSectionA.startThread())
		{
			m_pLogger->log( LOG_ERR, "CanSenderManager::startThreads: impossible to start m_pSenderToSectionA. Exit." );
			return false;
		}

		if (!m_pSenderToSectionUpFwA.startThread())
		{
			m_pLogger->log( LOG_ERR, "CanSenderManager::startThreads: impossible to start m_pSenderToSectionUpFwA. Exit." );
			return false;
		}
	}

	if (m_enableB)
	{
		if (!m_pSenderToSectionB.startThread())
		{
			m_pLogger->log( LOG_ERR, "CanSenderManager::startThreads: impossible to start m_pSenderToSectionB. Exit." );
			return false;
		}

		if (!m_pSenderToSectionUpFwB.startThread())
		{
			m_pLogger->log( LOG_ERR, "CanSenderManager::startThreads: impossible to start m_pSenderToSectionUpFwB. Exit." );
			return false;
		}
	}


    if(m_enableA || m_enableB)
    {
        if ( !m_canSender.startThread() )
        {
            m_pLogger->log( LOG_ERR, "CanSenderManager: impossible to start canSender. Exit." );
            return false;
        }
    }

	return true;
}

int CanSenderManager::stopThreads( void )
{
	if (m_enableA)
	{
		m_pSenderToSectionA.stopThread();
		m_pSenderToSectionA.closeMQ();

		m_pSenderToSectionUpFwA.stopThread();
		m_pSenderToSectionUpFwA.closeMQ();
	}

	if (m_enableB)
	{
		m_pSenderToSectionB.stopThread();
		m_pSenderToSectionB.closeMQ();

		m_pSenderToSectionUpFwB.stopThread();
		m_pSenderToSectionUpFwB.closeMQ();
	}

    if(m_canSender.isRunning())
    {
        m_canSender.stopThread();
    }

	return 0;
}

bool CanSenderManager::isRunning( void )
{
	if (m_enableA)
	{
		if (!m_pSenderToSectionA.isRunning())
		{
			m_pLogger->log(LOG_DEBUG,"CanSenderManager::isRunning: m_pSenderToSectionA thread is not running anymore...");
			return false;
		}

		if (!m_pSenderToSectionUpFwA.isRunning())
		{
			m_pLogger->log(LOG_DEBUG,"CanSenderManager::isRunning: m_pSenderToSectionUpFwA thread is not running anymore...");
			return false;
		}
	}

	if (m_enableB)
	{
		if (!m_pSenderToSectionB.isRunning())
		{
			m_pLogger->log(LOG_DEBUG,"CanSenderManager::isRunning: m_pSenderToSectionB thread is not running anymore...");
			return false;
		}

		if (!m_pSenderToSectionUpFwB.isRunning())
		{
			m_pLogger->log(LOG_DEBUG,"CanSenderManager::isRunning: m_pSenderToSectionUpFwB thread is not running anymore...");
			return false;
		}
	}

    if (!m_canSender.isRunning())
    {
        m_pLogger->log(LOG_DEBUG,"CanSenderManager::isRunning: m_canSender thread is not running anymore...");
        return false;
    }

	return true;
}

