/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanSender.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CanSender class.
 @details

 ****************************************************************************
*/

#ifndef CANSENDER_H
#define CANSENDER_H

//******************************************************************************
// Includes
//******************************************************************************
#include "Thread.h"
#include "Log.h"
#include "MessageQueue.h"
#include "Mutex.h"
#include "CanBuffer.h"

class CanIfr;

//******************************************************************************
// Class Implementation
//******************************************************************************
#define FREQ_CAN                           400000
#define TO_WT_CANSENDERTHREAD_US           2000//RBB too slow for section fwupgrade
#define TO_WT_CANSENDERMANAGERTHREAD_US    2

/*! *******************************************************************************************************************
 * @class	CanSenderThread
 * @brief	This non-blocking thread reads data frame present in the related queue and then send
 *			data to the peripheral device.
 * ********************************************************************************************************************
 */
class CanSenderThread : public Thread
{

	public:
        /*! ***********************************************************************************************
         * @brief           CanSenderThread
         * @brief               Constructor
         * ***********************************************************************************************
         */
		CanSenderThread();

        /*! ***********************************************************************************************
         * @brief           CanSenderManagerThread
         * @brief               Destructor
         * ***********************************************************************************************
         */
		virtual ~CanSenderThread();

        /*! ***********************************************************************************************
         * @brief           init
         * @brief               The function initializes the own logger and interface objects. It also sets
         *                      the buffer where data are stored.
         * @param pLogger [in]  logger of the class
         * @param pCanInterface [in]  can interface where send the massages
         * @param pcanBuffer [in] buffer for queueing pricess of the messages. It is shared with
         *                      CanSenderManagerThread.
         * @return              0 if success | -1 otherwise
         * ***********************************************************************************************
         */
		int init(Log* pLogger, CanIfr* pCanInterface, CanBuffer* pcanBuffer);

        /*! ***********************************************************************************************
         * @brief           setMutex
         * @brief           It sets the shared mutex for sending operation
         * @param pmutex [in]
         * @return  0 if success | -1 otherwise
         * ***********************************************************************************************
         */
        int setMutex(Mutex* pmutex);

	protected:
		void beforeWorkerThread(void);
		int workerThread(void);
		void afterWorkerThread(void);

    public:

        float       m_timeFrameCanus;

    private:
        Log*		m_pLogger;
        CanIfr*		m_pCanInterface;
        CanBuffer*	m_pCanBuffer;
        Log			m_canTimeLogger;
        Mutex*      m_pcommonMutex;


};

/*! *******************************************************************************************************************
 * @class	CanSenderManagerThread
 * @brief   Thread used to control if shared area has frame to read. If they are present workerThread
 *			send the frames to a MessageQueue.
 * ********************************************************************************************************************
 */
class CanSenderManagerThread : public Thread
{

	public:
        /*! ***********************************************************************************************
         * @brief           CanSenderManagerThread
         * @brief               Constructor
         * ***********************************************************************************************
         */
		CanSenderManagerThread();

        /*! ***********************************************************************************************
         * @brief           CanSenderManagerThread
         * @brief               Destructor
         * ***********************************************************************************************
         */
		virtual ~CanSenderManagerThread();

        /*! ***********************************************************************************************
         * @brief           init
         * @brief           The function initializes the own logger and interface objects. It also sets
         *                  the message queue to read for interprocess data exchange.
         * @param pLogger [in]  logger of the class
         * @param pCanInterface [in]  can interface where send the messages
         * @param pcanBuffer [in] buffer for queueing messages
         * @param pcanMsgQueue [in]  message queue to use
         * @param sName [in]    name of the message queue to read
         * @return              0 if success | -1 otherwise
         * ***********************************************************************************************
         */
		int init(Log* pLogger, CanIfr* pCanInterface, CanBuffer* pcanBuffer, MessageQueue* pcanMsgQueue, const char* sName);

        /*! ***********************************************************************************************
         * @brief           closeMQ
         * @brief               It closes message queue in reading
         * @return          0 if success | -1 otherwise
         * ***********************************************************************************************
         */
		int closeMQ(void);

        /*! ***********************************************************************************************
         * @brief           setMutex
         * @brief               set mutex for the data send
         * @return          0 if success | -1 otherwise
         * ***********************************************************************************************
         */
        int setMutex(Mutex* pmutex);

	private:
        /*! ***********************************************************************************************
         * @brief           waitForMsgQueue
         * @brief               It tries to open message queue continuously every SCT_MQ_OPEN_RETRY_MSLEEP ms.
         *                      It breaks the loop when open operation have been successfully
         * ***********************************************************************************************
         */
		void waitForMsgQueue(void);

	protected:
		void beforeWorkerThread(void);
		int workerThread(void);
		void afterWorkerThread(void);

    private:
        CanIfr*			m_pCanInterface;
        Log*			m_pLogger;
        CanBuffer*		m_pcanBuffer;
        char			m_sNameMsgQueue[MAX_NAME_LENGTH_BYTES];
        MessageQueue*	m_pcanMsgQueue;
        Mutex*          m_pcommonMutex;

};

/*! ***********************************************************************************************
 * @class	CanSenderManager
 * @brief	The class is used to manage the objects CanSenderManagerThread, that represent
 *			the channel for the can frame send. In this case the class contains two channels
 *			in order to the number of section device.
 * ************************************************************************************************
 */
class CanSenderManager
{
	public:

        /*! ***********************************************************************************************
         * @brief           CanSenderManager
         * @brief               Constructor
         * ***********************************************************************************************
         */
        CanSenderManager();

        /*! ***********************************************************************************************
         * @brief           CanSenderManager
         * @brief               Destructor
         * ***********************************************************************************************
         */
        virtual ~CanSenderManager();

        /*! ***********************************************************************************************
         * @brief           init
         * @brief               The function initializes the own logger and interface objects. It also sets
         *                      the canSenderManagerThread threads based on the enabled section.
         * @param pLogger [in]  logger of the class
         * @param pcanIfr [in]  can interface where send the massages
         * @param enableA [in]  enabling variable for sectionA
         * @param enableB [in]  enabling variable for sectionB
         * @return              0 if success | -1 otherwise
         * ***********************************************************************************************
         */
        int init(Log* pLogger, CanIfr* pcanIfr, bool enableA = true, bool enableB = true);

        /*! ***********************************************************************************************
         * @brief           startThreads
         * @brief               It starts all the sender threads based on the enabled section
         * @return          true if success | false otherwise
         * ***********************************************************************************************
         */
        bool startThreads();

        /*! ***********************************************************************************************
         * @brief          stopThreads
         * @brief               It stops all the sender threads based on the enabled section
         * @return         0 if success | -1 otherwise
         * ***********************************************************************************************
         */
        int	stopThreads();

        /*! ***********************************************************************************************
         * @brief           isRunning
         * @brief               It controls if all thread are running.
         * @return          true if all threads are running | false otherwise
         * ***********************************************************************************************
         */
        bool isRunning();

    private:

        Log* m_pLogger;
        CanIfr* m_pCanInterface;

        CanBuffer   m_canTxBuffer;
        Mutex       m_localMutexSender;


        CanSenderManagerThread m_pSenderToSectionA;
        CanSenderManagerThread m_pSenderToSectionB;
        CanSenderManagerThread m_pSenderToSectionUpFwA;
        CanSenderManagerThread m_pSenderToSectionUpFwB;

        CanSenderThread m_canSender;

        MessageQueue m_CanMsgQueueA;
        MessageQueue m_CanMsgQueueB;
        MessageQueue m_CanMsgQueueUpFwA;
        MessageQueue m_CanMsgQueueUpFwB;

    private:

        bool m_enableA;
        bool m_enableB;


};
#endif // CANSENDER_H
