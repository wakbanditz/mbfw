/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanReceiver.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CanReceiver class.
 @details

 ****************************************************************************
*/

#include <unistd.h>

#include "Log.h"
#include "CanIfr.h"
#include "CanBuffer.h"
#include "CanReceiver.h"
#include "CommonInclude.h"
#include "TimeStamp.h"

/* ********************************************************************************************************************
 * CanReceiverThread class implementation
 * ********************************************************************************************************************
 */
CanReceiverThread::CanReceiverThread()
{
    m_pCanInterface				= NULL;
    m_pLogger					= NULL;
    m_pCanBuffer				= NULL;
}

CanReceiverThread::~CanReceiverThread()
{
    /* Nothing to do yet */
}

int CanReceiverThread::init(Log *pLogger, CanIfr *pCanInterface, CanBuffer *pCanBuffer)
{
    if ( (pLogger == NULL) || (pCanInterface == NULL) || (pCanBuffer == NULL) )
    {
        return -1;
    }
    else
    {
        m_pLogger		= pLogger;
        m_pCanInterface = pCanInterface;
        m_pCanBuffer	= pCanBuffer;
    }
    return 0;
}

void CanReceiverThread::setCanBuffer(CanBuffer *pcanBuffer)
{
    m_pCanBuffer = pcanBuffer;
}

void CanReceiverThread::beforeWorkerThread()
{
    m_pLogger->log(LOG_INFO,"CanRx: starting");
}

int CanReceiverThread::workerThread()
{
    int nNumRxByte;
    const int maxRes = (2 * DIM_CAN_TS_BUFF / 3);

    while( isRunning() )
    {
        can_frame   canFrame;

        nNumRxByte = m_pCanInterface->readFrame(&canFrame);
        if ( nNumRxByte < 0 )
        {
            m_pLogger->log(LOG_ERR, "CanRx: <%s>", strerror(errno));
        }
        else
        {
            int res = m_pCanBuffer->setData(&canFrame);

            if(res > maxRes)
            {
                m_pLogger->log(LOG_DEBUG, "CanRx BufferSize: %d", res);
            }
        }
        // usleep(TO_WT_CANRECEIVERTHREAD_US);
    }
    return 0;
}

void CanReceiverThread::afterWorkerThread(void)
{
    m_pLogger->log(LOG_INFO, "CanRx: closing");
}

/* ********************************************************************************************************************
 * CanReceiverManagerThread class implementation
 * ********************************************************************************************************************
 */
CanReceiverManagerThread::CanReceiverManagerThread()
{
	m_pLogger = NULL;
	m_pCanBuffer = NULL;
	m_pCanInterface = NULL;
	m_enableA = false;
	m_enableB = false;

	m_pCanMsgQueueA.push_back(&m_CanMsgQueueA);
	m_pCanMsgQueueA.push_back(&m_CanMsgQueueAuxA1);
	m_pCanMsgQueueA.push_back(&m_CanMsgQueueAuxA2);
	m_pCanMsgQueueA.push_back(&m_CanMsgQueueAuxA3);
	m_pCanMsgQueueA.push_back(&m_CanMsgQueueAuxA4);
	m_pCanMsgQueueA.push_back(&m_CanMsgQueueAuxA5);
	m_pCanMsgQueueA.push_back(&m_CanMsgQueueAuxA6);
	m_pCanMsgQueueA.push_back(&m_CanMsgQueueUpFwA);

	m_pCanMsgQueueB.push_back(&m_CanMsgQueueB);
	m_pCanMsgQueueB.push_back(&m_CanMsgQueueAuxB1);
	m_pCanMsgQueueB.push_back(&m_CanMsgQueueAuxB2);
	m_pCanMsgQueueB.push_back(&m_CanMsgQueueAuxB3);
	m_pCanMsgQueueB.push_back(&m_CanMsgQueueAuxB4);
	m_pCanMsgQueueB.push_back(&m_CanMsgQueueAuxB5);
	m_pCanMsgQueueB.push_back(&m_CanMsgQueueAuxB6);
	m_pCanMsgQueueB.push_back(&m_CanMsgQueueUpFwB);
}

CanReceiverManagerThread::~CanReceiverManagerThread()
{
	closeSectionQueues(SCT_A_ID);
	closeSectionQueues(SCT_B_ID);
}

int CanReceiverManagerThread::init(Log *pLogger, CanIfr *pCanInterface, CanBuffer *pCanBuffer,
								   bool enableA, bool enableB )
{
	if ( (pLogger == NULL) || (pCanInterface == NULL) || (pCanBuffer == NULL) )
	{
		return -1;
	}
	else if (enableA == false && enableB == false)
	{
		m_pLogger = pLogger;
        m_pLogger->log(LOG_INFO,  "CanRxManager::init: No sections enabled");
	}
	else
	{
		m_pLogger = pLogger;
		m_pCanInterface = pCanInterface;
		m_pCanBuffer = pCanBuffer;
		m_enableA = enableA;
		m_enableB = enableB;
	}

	return 0;
}

void CanReceiverManagerThread::openMsgQueues(void)
{
	bool bRetMQ0 = true;
	bool bRetMQ1 = true;

	while ( 1 )
	{
		bRetMQ0 = openSectionQueues(SCT_A_ID, 0);
		bRetMQ1 = openSectionQueues(SCT_B_ID, 0);

		if ( ( bRetMQ0 == true ) && ( bRetMQ1 == true ) )
		{
			m_pLogger->log(LOG_INFO,  "CanRxManager::checkMsgQueue: MSG QUEUEs correctly opened");
			break;
		}

		msleep(SCT_MQ_OPEN_RETRY_MSLEEP);
	}
}

bool CanReceiverManagerThread::openSectionQueues(uint8_t section, uint16_t delay)
{
	bool enable = false;
	bool bRet = false;
	int errorCounter;
	std::vector<MessageQueue *> * pVectorMsgQueue;
	char queueName[64];

	switch(section)
	{
		case SCT_A_ID :
			enable = m_enableA;
			pVectorMsgQueue = &m_pCanMsgQueueA;
		break;
		case SCT_B_ID :
			enable = m_enableB;
			pVectorMsgQueue = &m_pCanMsgQueueB;
		break;
	}

	if(enable == false) return true;

	while(true)
	{
		errorCounter = 0;
		for(unsigned int i = 0; i < pVectorMsgQueue->size(); i++)
		{
			MessageQueue * pMsgQueue = pVectorMsgQueue->at(i);

			switch(i)
			{
				// name of the queue depends on section and index !
				case 0:
					if(section == SCT_A_ID) strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_A);
					else					strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_B);
				break;
				case 1:
					if(section == SCT_A_ID) strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX1_A);
					else					strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX1_B);
				break;
				case 2:
					if(section == SCT_A_ID) strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX2_A);
					else					strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX2_B);
				break;
				case 3:
					if(section == SCT_A_ID) strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX3_A);
					else					strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX3_B);
				break;
				case 4:
					if(section == SCT_A_ID) strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX4_A);
					else					strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX4_B);
				break;
				case 5:
					if(section == SCT_A_ID) strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX5_A);
					else					strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX5_B);
				break;
				case 6:
					if(section == SCT_A_ID) strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX6_A);
					else					strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX6_B);
				break;
				case 7:
					if(section == SCT_A_ID) strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_UPFW_A);
					else					strcpy(queueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_UPFW_B);
				break;
				default:
					return false;
				break;
			}


			bRet = pMsgQueue->open(queueName, O_WRONLY);
			if (bRet == false)
			{
				errorCounter++;
				m_pLogger->log(LOG_DEBUG_PARANOIC,
							   "CanRxManager::checkMsgQueue: unable to open MSG QUEUE <%s>", queueName);
			}
			else
			{
				m_pLogger->log(LOG_INFO,
							   "CanRxManager::checkMsgQueue: MSG QUEUE <%s> opened", queueName);
			}
		}

		if(errorCounter == 0) break;	// all queues correctly opened

		if(delay == 0) break;	// no loop -> return to caller

		msleep(SCT_MQ_OPEN_RETRY_MSLEEP);
	}

	return (errorCounter == 0);
}

bool CanReceiverManagerThread::closeSectionQueues(uint8_t section)
{
	bool enable = false;
	bool bRet = false;
	int errorCounter;
	std::vector<MessageQueue *> * pVectorMsgQueue;

	switch(section)
	{
		case SCT_A_ID :
			enable = m_enableA;
			pVectorMsgQueue = &m_pCanMsgQueueA;
		break;
		case SCT_B_ID :
			enable = m_enableB;
			pVectorMsgQueue = &m_pCanMsgQueueB;
		break;
	}

	if(enable == false) return true;

	// Close MQRXs
	errorCounter = 0;
	for(unsigned int i = 0; i < pVectorMsgQueue->size(); i++)
	{
		MessageQueue * pMsgQueue = pVectorMsgQueue->at(i);
		bRet = pMsgQueue->close();
		if ( bRet == false )
		{
			m_pLogger->log(LOG_ERR, "CanRxManager: unable to close MQRX %d %d", section, i);
			errorCounter++;
		}
	}
	return (errorCounter == 0);
}

int CanReceiverManagerThread::checkMsgQueueA( void )
{
	openSectionQueues(SCT_A_ID, SCT_MQ_OPEN_RETRY_MSLEEP);
	return 0;
}

int CanReceiverManagerThread::checkMsgQueueB( void )
{
	openSectionQueues(SCT_B_ID, SCT_MQ_OPEN_RETRY_MSLEEP);
	return 0;
}

void CanReceiverManagerThread::openSingleMsgQueue(MessageQueue * pMsgQueue, char * strNameQueue)
{
	while (1)
	{
		bool bRetMQ0 = pMsgQueue->open(strNameQueue, O_WRONLY);
		if ( bRetMQ0 == false )
		{
			m_pLogger->log(LOG_DEBUG_PARANOIC, "CanRxManager::checkMsgQueue0--> unable to open MSG QUEUE <%s>",
						   strNameQueue);
		}
		else
		{
			m_pLogger->log(LOG_DEBUG_PARANOIC, "CanRxManager::checkMsgQueue0--> MSG QUEUE <%s> opened",
						   strNameQueue);
			break;
		}
		msleep(SCT_MQ_OPEN_RETRY_MSLEEP);
	}
}

void CanReceiverManagerThread::setCanBuffer(CanBuffer *pcanBuffer)
{
	m_pCanBuffer = pcanBuffer;
}

int CanReceiverManagerThread::stop(void)
{
	closeSectionQueues(SCT_A_ID);
	closeSectionQueues(SCT_B_ID);

	// Stop the running thread
	stopThread(0);
	return 0;
}

MessageQueue * CanReceiverManagerThread::getMsgQueueFromCanId(canid_t canId, char * strQueueName)
{
	MessageQueue * pMsgQueue = 0;
	*strQueueName = 0;

	switch((uint16_t)canId)
	{
		case CAN_SECTION_A_BOARD_ID_MASTER:
			if(m_enableA == true)
				pMsgQueue = m_pCanMsgQueueA.at(0);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_A);
		break;
		case CAN_SECTION_A_BOARD_ID_CHANNEL_1:
			if(m_enableA == true)
				pMsgQueue = m_pCanMsgQueueA.at(1);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX1_A);
		break;
		case CAN_SECTION_A_BOARD_ID_CHANNEL_2:
			if(m_enableA == true)
				pMsgQueue = m_pCanMsgQueueA.at(2);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX2_A);
		break;
		case CAN_SECTION_A_BOARD_ID_CHANNEL_3:
			if(m_enableA == true)
				pMsgQueue = m_pCanMsgQueueA.at(3);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX3_A);
		break;
		case CAN_SECTION_A_BOARD_ID_CHANNEL_4:
			if(m_enableA == true)
				pMsgQueue = m_pCanMsgQueueA.at(4);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX4_A);
		break;
		case CAN_SECTION_A_BOARD_ID_CHANNEL_5:
			if(m_enableA == true)
				pMsgQueue = m_pCanMsgQueueA.at(5);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX5_A);
		break;
		case CAN_SECTION_A_BOARD_ID_CHANNEL_6:
			if(m_enableA == true)
				pMsgQueue = m_pCanMsgQueueA.at(6);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX6_A);
		break;
		case CAN_SECTION_A_BOARD_ID_UPFW:
			if(m_enableA == true)
				pMsgQueue = m_pCanMsgQueueA.at(7);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_UPFW_A);
		break;
		case CAN_SECTION_B_BOARD_ID_MASTER:
			if(m_enableB == true)
				pMsgQueue = m_pCanMsgQueueB.at(0);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_B);
		break;
		case CAN_SECTION_B_BOARD_ID_CHANNEL_1:
			if(m_enableB == true)
				pMsgQueue = m_pCanMsgQueueB.at(1);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX1_B);
		break;
		case CAN_SECTION_B_BOARD_ID_CHANNEL_2:
			if(m_enableB == true)
				pMsgQueue = m_pCanMsgQueueB.at(2);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX2_B);
		break;
		case CAN_SECTION_B_BOARD_ID_CHANNEL_3:
			if(m_enableB == true)
				pMsgQueue = m_pCanMsgQueueB.at(3);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX3_B);
		break;
		case CAN_SECTION_B_BOARD_ID_CHANNEL_4:
			if(m_enableB == true)
				pMsgQueue = m_pCanMsgQueueB.at(4);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX4_B);
		break;
		case CAN_SECTION_B_BOARD_ID_CHANNEL_5:
			if(m_enableB == true)
				pMsgQueue = m_pCanMsgQueueB.at(5);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX5_B);
		break;
		case CAN_SECTION_B_BOARD_ID_CHANNEL_6:
			if(m_enableB == true)
				pMsgQueue = m_pCanMsgQueueB.at(6);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX6_B);
		break;
		case CAN_SECTION_B_BOARD_ID_UPFW:
			if(m_enableB == true)
				pMsgQueue = m_pCanMsgQueueB.at(7);
			strcpy(strQueueName, SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_UPFW_B);
		break;
	}

	m_pLogger->log(LOG_DEBUG_PARANOIC, "CanRxManager:getMsgQueueFromCanId 0x%x %d", canId, pMsgQueue);

	return pMsgQueue;
}

void CanReceiverManagerThread::beforeWorkerThread()
{
    m_pLogger->log(LOG_INFO,"CanRxManager: starting");
}

int CanReceiverManagerThread::workerThread()
{
#ifdef	CHECK_MQ_TIMEOUT
        uint64_t ullOldMsec = 0;
#endif

    can_frame *  pCanFrame;

    openMsgQueues();

    while( isRunning() )
    {
            int res = m_pCanBuffer->getData(pCanFrame);
            if ( res >= 0 )
            {

#ifdef	CHECK_MQ_TIMEOUT
               TimeStamp timeStampOld;
               ullOldMsec = timeStampOld.get() / NSEC_PER_MSEC;
#endif
                MessageQueue * pMsgQueue;
                char strQueueName[64];

                if ((pMsgQueue = getMsgQueueFromCanId(pCanFrame->can_id, strQueueName)) != 0)
                {
                    if ( pMsgQueue->retrieveCurrentNumMessages() < 0 )
                    {
                        m_pLogger->log(LOG_ERR, "CanRxManager: retrieveCurrentNumMessages");

                        // Close MQRX0 because if the MQ no more exists the only way to unlock is by closing it
                        bool bRet = pMsgQueue->close();
                        if ( bRet == false )
                        {
                            m_pLogger->log(LOG_ERR, "CanRxManager: unable to close MQRX 0x%x", pCanFrame->can_id);
                        }
                        openSingleMsgQueue(pMsgQueue, strQueueName);
                    }
                    else
                    {
                        // Send to Message queue
                        if ( !pMsgQueue->send((char *)pCanFrame, sizeof(can_frame), 0, 1000) )
                        {
                            m_pLogger->log(LOG_ERR, "CanRxManager: unable to send on MQRX 0x%x", pCanFrame->can_id);
                        }
                    }
                }
                else
                {
                    m_pLogger->log(LOG_ERR, "CanRxManager: received ID[0x%x] not managed", pCanFrame->can_id);
                }
            }
            else
            {
                #ifdef	CHECK_MQ_TIMEOUT
                    // Check message queues integrity
                    TimeStamp timeStampNow;
                    uint64_t ullNowMsec = timeStampNow.get() / NSEC_PER_MSEC;
                    uint64_t ullTime = ullNowMsec - ullOldMsec;
                    if ( ullTime >= TIME_CHECK_MQ )
                    {
                        m_pLogger->log(LOG_INFO, "CanRxManager: timeout expired");

                        closeSectionQueues(SCT_A_ID);
                        checkMsgQueueA();

                        closeSectionQueues(SCT_B_ID);
                        checkMsgQueueB();

                        ullOldMsec = ullNowMsec;
                    }
                #endif
            }
//        usleep(TO_WT_CANRECEIVERMANAGERTHREAD_US);
    }

    return 0;
}

void CanReceiverManagerThread::afterWorkerThread()
{
    m_pLogger->log(LOG_INFO,"CanRxManager: closing");
}
