/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    main.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the main function.
 @details

 ****************************************************************************
*/

//******************************************************************************
// Includes
//******************************************************************************
#include <signal.h>

#include "CanBuffer.h"
#include "CanReceiver.h"
#include "CanSender.h"
#include "Log.h"
#include "CommonInclude.h"
#include "Config.h"

//******************************************************************************
// Implementation
//******************************************************************************
static bool bKeepRunning = true;

void signalHandler(int nSignal)
{
	static int sig_counter = 1;
	printf("Received signal %d\n", nSignal);
	switch(nSignal)
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
			bKeepRunning = false;
			if ( ++sig_counter > 5 )
			{
				// Fifth time that the soft mode has failed brute exits
				printf("Brute force exit ...");
				exit(1);
			}
		break;
		case SIGTSTP:
			bKeepRunning = false;
		break;
	}
}

int main( void )
{
#ifdef KILL_PROCESS_STAND_ALONE_MODE
	signal(SIGINT, signalHandler);
	signal(SIGKILL, signalHandler);
	signal(SIGTERM, signalHandler);
	signal(SIGTSTP, signalHandler);
#else
	signal(SIGTERM, signalHandler);
#endif
	/* ********************************************************************************************
	 * READS CONFIGURATION FILE
	 * ********************************************************************************************
	 */
	Config configurator;
	char sConfigFileName[1024] = CONF_FILE;
	char sError[1024];
	config_key_t config_keys[] =
	{
		{ CFG_CAN_MANAGER_LOG_ENABLE,		(char*)"log.CanManager.enable", (char*)"GENERAL", T_int, (char*)"1", DEFAULT_ACCEPTED},
		{ CFG_CAN_MANAGER_LOG_FILE,			(char*)"log.CanManager.file", (char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_CAN_MANAGER_LOG_LEVEL,		(char*)"log.CanManager.level", (char*)"GENERAL", T_int, (char*)"6", DEFAULT_ACCEPTED},
		{ CFG_CAN_MANAGER_LOG_NUM_FILES,	(char*)"log.CanManager.numFiles", (char*)"GENERAL", T_int, (char*)"5", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_SCT_A_ENABLE,			(char*)"fwCore.sectionA.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_SCT_B_ENABLE,			(char*)"fwCore.sectionB.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
	};
	int num_keys = sizeof(config_keys) / sizeof(config_key_t);
	if ( !configurator.Init(config_keys, num_keys, sConfigFileName) )
	{
		Config::GetLastError(sError, 1024);
		printf("Impossible to read configuration file <%s> (error: %s)", sConfigFileName, sError);
        return 0;
	}

	/* ********************************************************************************************
	 * LOGGER INITIALIZATION
	 * ********************************************************************************************
	 */
	Log logger;
	int liLogEnable = configurator.GetInt(CFG_CAN_MANAGER_LOG_ENABLE);
	bool bEnable = (liLogEnable > 0 ? true : false);
	logger.enable(bEnable);
	logger.setAppName(PROGRAM_NAME);
	logger.setFile(configurator.GetString(CFG_CAN_MANAGER_LOG_FILE));
	logger.setLevel(configurator.GetInt(CFG_CAN_MANAGER_LOG_LEVEL));
	logger.setMaxNumFiles(configurator.GetInt(CFG_CAN_MANAGER_LOG_NUM_FILES));
	logger.log(LOG_INFO, "Starting << %s >>", PROGRAM_NAME);

	/* ********************************************************************************************
	 * INTERFACE INITIALIZATION
	 * ********************************************************************************************
	 */
	bool enableSectionA = false;
	bool enableSectionB = false;
	uint16_t canIdListSFF[MAX_CAN_ID_LIST];
	uint16_t canIdSizeSFF = 0;
	uint16_t canIdListEFF[MAX_CAN_ID_LIST];
	uint16_t canIdSizeEFF = 0;

	char enableStr[32];

	sprintf(enableStr, "%s", configurator.GetString(CFG_FW_CORE_SCT_A_ENABLE));
	if ( strcmp(enableStr, "true") == 0 )
	{
		enableSectionA = true;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_MASTER;	canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_1;canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_2;canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_3;canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_4;canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_5;canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_6;canIdSizeSFF++;

		canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_UPFW;canIdSizeSFF++;
	}

	sprintf(enableStr, "%s", configurator.GetString(CFG_FW_CORE_SCT_B_ENABLE));
	if ( strcmp(enableStr, "true") == 0 )
	{
		enableSectionB = true;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_MASTER;	canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_1;canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_2;canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_3;canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_4;canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_5;canIdSizeSFF++;
		canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_6;canIdSizeSFF++;

		canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_UPFW;canIdSizeSFF++;
	}

	CanIfr canInterface;
	canInterface.setInterfaceName("can0");
	canInterface.setLogger(&logger);
	bool bInitOk = canInterface.init(canIdListSFF, canIdSizeSFF, canIdListEFF, canIdSizeEFF);
	if ( bInitOk == false )
	{
		logger.log(LOG_ERR, "canInterface initilization failed");
		return 0;
	}
	logger.log(LOG_INFO, "canInterface correctly initialized");

	/* ********************************************************************************************
	 * RECEIVER THREADS INITIALIZATION
	 * ********************************************************************************************
	 */
	int res = 0;
	CanBuffer canRxBuffer;

	CanReceiverManagerThread canReceiveManager;
	res = canReceiveManager.init(&logger, &canInterface, &canRxBuffer, enableSectionA, enableSectionB);
	if (res != 0)
	{
		logger.log(LOG_ERR, "CanReceiverManagerThread initilization failed");
		return 0;
	}

	CanReceiverThread canReceiver;
	res = canReceiver.init(&logger, &canInterface, &canRxBuffer);
	if (res != 0)
	{
		logger.log(LOG_ERR, "CanReceiverThread initilization failed");
		return 0;
	}

	/* ********************************************************************************************
	 * SENDER THREADS INITIALIZATION
	 * ********************************************************************************************
	 */
	CanSenderManager	canSenderManager;
    res = canSenderManager.init( &logger, &canInterface, enableSectionA, enableSectionB );
	if (res != 0)
	{
		logger.log(LOG_ERR, "canSenderManager initilization failed");
		return 0;
	}

	/* ********************************************************************************************
	 * START APPLICATION
	 * ********************************************************************************************
	 */
	if ( !canReceiver.startThread() )
	{
		logger.log( LOG_ERR, "CANTr: impossible to start canReceiverThread. Exit." );
		return 1;
	}

	if ( !canReceiveManager.startThread() )
	{
		logger.log( LOG_ERR, "CANTr: impossible to start canRxManager. Exit." );
		return 1;
	}

	if ( !canSenderManager.startThreads() )
	{
		logger.log( LOG_ERR, "CANTr: impossible to start canSenderManager. Exit." );
		return 1;
	}


	while ( bKeepRunning )
	{
		if ( !canReceiver.isRunning() )
		{
			logger.log(LOG_INFO,"CANTr: receiver thread is not more running...");
			bKeepRunning = false;
		}

		if ( !canReceiveManager.isRunning() )
		{
			logger.log(LOG_INFO,"CANTr: receiver thread is not more running...");
			bKeepRunning = false;
		}

		if ( !canSenderManager.isRunning() )
		{
			logger.log(LOG_INFO,"CANTr: receiver thread is not more running...");
			bKeepRunning = false;
		}

		usleep(100);
	}


	/* ********************************************************************************************
	 * CLOSE AND FREE RESOURCES
	 * ********************************************************************************************
	 */
	logger.log(LOG_INFO, "Stopping CanReceiver Thread");

	canReceiveManager.stop();
	canReceiver.stopThread(0);
	canSenderManager.stopThreads();


	msleep(1000);
	logger.log(LOG_INFO, "Closing");

	return 0;
}

