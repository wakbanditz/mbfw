TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Thread
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/CanInterface/ -lCanInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/CanInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/CanInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/CanInterface/libCanInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Log
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/IPC/ -lIPC
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/IPC
DEPENDPATH += $$PWD/../../bmx_libs/Libs/IPC
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/IPC/libIPC.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/TimeStamp
DEPENDPATH += $$PWD/../../bmx_libs/Libs/TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/TimeStamp/libTimeStamp.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Config/ -lConfig
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Config
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Config/libConfig.a


INCLUDEPATH += $$PWD/../../

LIBS += -pthread -lrt -lpthread

HEADERS += \
    CanReceiver.h \
    CanSender.h \
    CanBuffer.h

SOURCES += \
    main.cpp \
    CanReceiver.cpp \
    CanSender.cpp \
    CanBuffer.cpp

TARGET = CanManager

# deployment directives
target.path = /home/root
INSTALLS += target

# inclusions and defines
include(../../mbfwInclude.pri)
#DEFINES += CONF_FILE=\\\"$${CONFIG_FILE}\\\"
DEFINES += PROGRAM_NAME=\\\"$${PROCESS_NAME_CAN_MANAGER}\\\"

