/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanBuffer.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CanBuffer class.
 @details

 ****************************************************************************
*/

#ifndef CANBUFFER_H
#define CANBUFFER_H

#include <CanIfr.h>

#define DIM_CAN_NOTS_BUFF	 2048
#define DIM_CAN_TS_BUFF      2048

#define DIM_CAN_BUFF         2048

/*! *******************************************************************************************************************
 * @class	CanBuffer
 * @brief	The CanBuffer class is used for data packets passing to and from the physical peripheral CAN device.
 *			It has the possibility to use directly the canfd_frame system struct or the t_canFrame struct,
 *			which includes TimeStamp info.
 * ********************************************************************************************************************
 */
class CanBuffer
{

	public:

        /*! *************************************************************************************************
         * @brief default constructor
         * **************************************************************************************************
         */
        CanBuffer();

        /*! *************************************************************************************************
         * @brief destructor
         * **************************************************************************************************
         */
        ~CanBuffer();


        /*! *************************************************************************************************
         * @brief set data in the NO timestamp buffer
         * @param pCanFrame pointer to the message to append
         * @return the number of messages in buffer
         * **************************************************************************************************
         */
        int setDataFD(canfd_frame* pCanFrame);

        /*! *************************************************************************************************
         * @brief get data from the NO timestamp buffer
         * @param pCanFrame [out] pointer to the message read
         * @return the number of messages in buffer
         * **************************************************************************************************
         */
        int getDataFD(canfd_frame* pCanFrame);

        /*! *************************************************************************************************
         * @brief set data in the timestamp buffer
         * @param pCanFrame pointer to the message to append
         * @return the number of messages in buffer
         * **************************************************************************************************
         */
        int setDataTSFD(t_canFrameFD* pCanFrame);

        /*! *************************************************************************************************
         * @brief get data from the timestamp buffer
         * @param pCanFrame [out] pointer to the message read
         * @return the number of messages in buffer
         * **************************************************************************************************
         */
        int getDataTSFD(t_canFrameFD* pCanFrame);

        /*! *************************************************************************************************
         * @brief set data in the NO timestamp buffer
         * @param pCanFrame pointer to the message to append
         * @return the number of messages in buffer
         * **************************************************************************************************
         */
        int setData(can_frame* pCanFrame);

        /*! *************************************************************************************************
         * @brief get data from the NO timestamp buffer
         * @param pCanFrame [out] pointer to the message read
         * @return the number of messages in buffer
         * **************************************************************************************************
         */
        int getData(can_frame* pCanFrame);

    private:
        canfd_frame     m_canFrameFDBuffer[DIM_CAN_NOTS_BUFF];
        canfd_frame*    m_pcanFDWr;
        canfd_frame*    m_pcanFDRd;
        uint16_t        m_nNumCanFDToRead;

        t_canFrameFD    m_canFrameFDTSBuffer[DIM_CAN_TS_BUFF];
        t_canFrameFD*   m_pcanFrameFDTSWr;
        t_canFrameFD*   m_pcanFrameFDTSRd;
        uint16_t        m_nNumFrameFDTSToRead;

        can_frame       m_canFrameBuffer[DIM_CAN_BUFF];
        can_frame*      m_pcanWr;
        can_frame*      m_pcanRd;
        uint16_t        m_nNumCanToRead;
        uint16_t        m_nNumFrameTSToRead;

        Mutex m_MutexTx, m_MutexRx;
};

#endif // CANBUFFER_H
