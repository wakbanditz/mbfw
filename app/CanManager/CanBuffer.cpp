/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanBuffer.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CanBuffer class.
 @details

 ****************************************************************************
*/


#include "Mutex.h"

#include "CanBuffer.h"
#include "CommonInclude.h"

CanBuffer::CanBuffer()
{
    m_pcanFDWr              = &m_canFrameFDBuffer[0];
    m_pcanFDRd              = &m_canFrameFDBuffer[0];
    m_nNumCanFDToRead       = 0;
    m_pcanFrameFDTSWr       = &m_canFrameFDTSBuffer[0];
    m_pcanFrameFDTSRd       = &m_canFrameFDTSBuffer[0];
    m_nNumFrameFDTSToRead   = 0;

    m_pcanWr                = &m_canFrameBuffer[0];
    m_pcanRd                = &m_canFrameBuffer[0];
    m_nNumCanToRead         = 0;
    m_nNumFrameTSToRead     = 0;
}

CanBuffer::~CanBuffer()
{

}

int CanBuffer::setData(can_frame *pCanFrame)
{
    m_MutexTx.lock();

    *m_pcanWr = *pCanFrame;
    m_pcanWr++;
    if (m_pcanWr >= &m_canFrameBuffer[DIM_CAN_BUFF])
    {
        m_pcanWr = &m_canFrameBuffer[0];
    }

    int res = ++m_nNumCanToRead;
    m_MutexTx.unlock();
    return res;
}

int CanBuffer::setDataFD(canfd_frame *pCanFrame)
{    
    m_MutexTx.lock();

	memcpy(m_pcanFDWr, pCanFrame, sizeof(canfd_frame));
	m_pcanFDWr++;
    if (m_pcanFDWr >= &m_canFrameFDBuffer[DIM_CAN_NOTS_BUFF])
	{
		m_pcanFDWr = &m_canFrameFDBuffer[0];
	}

    int res = ++m_nNumCanFDToRead;
    m_MutexTx.unlock();
    return res;
}

int CanBuffer::getData(can_frame *pCanFrame)
{
    int res = -1;

    m_MutexTx.lock();

    if (m_nNumCanToRead > 0)
    {
        *pCanFrame = *m_pcanRd;
        m_pcanRd++;
        if ( m_pcanRd >= &m_canFrameBuffer[DIM_CAN_BUFF] )
        {
            m_pcanRd = &m_canFrameBuffer[0];
        }
        res = --m_nNumCanToRead;
    }
    m_MutexTx.unlock();
    return res;
}

int CanBuffer::getDataFD(canfd_frame *pCanFrame)
{
    int res = -1;

    m_MutexTx.lock();

	if (m_nNumCanFDToRead > 0)
	{
        *pCanFrame = *m_pcanFDRd;
        m_pcanFDRd++;
        if ( m_pcanFDRd >= &m_canFrameFDBuffer[DIM_CAN_NOTS_BUFF] )
		{
			m_pcanFDRd = &m_canFrameFDBuffer[0];
		}
        res = --m_nNumCanFDToRead;
	}
    m_MutexTx.unlock();
    return res;
}

int CanBuffer::setDataTSFD(t_canFrameFD *pCanFrame)
{
    m_MutexRx.lock();

	memcpy(m_pcanFrameFDTSWr, pCanFrame, sizeof(t_canFrameFD));
	m_pcanFrameFDTSWr++;
    if (m_pcanFrameFDTSWr >= &m_canFrameFDTSBuffer[DIM_CAN_TS_BUFF])
	{
		m_pcanFrameFDTSWr = &m_canFrameFDTSBuffer[0];
	}
    int res = ++m_nNumFrameFDTSToRead;
    m_MutexRx.unlock();
    return res;
}

int CanBuffer::getDataTSFD(t_canFrameFD *pCanFrame)
{
    int res = -1;
    m_MutexRx.lock();

	if ( m_nNumFrameFDTSToRead > 0 )
	{
		memcpy(pCanFrame, m_pcanFrameFDTSRd, sizeof(t_canFrameFD));
		m_pcanFrameFDTSRd++;
        if ( m_pcanFrameFDTSRd >= &m_canFrameFDTSBuffer[DIM_CAN_TS_BUFF] )
		{
			m_pcanFrameFDTSRd = &m_canFrameFDTSBuffer[0];
		}
        res = --m_nNumFrameFDTSToRead;
	}
    m_MutexRx.unlock();
    return res;
}

