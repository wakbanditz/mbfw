/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanReceiver.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CanReceiver class.
 @details

 ****************************************************************************
*/

#ifndef CANRECEIVER_H
#define CANRECEIVER_H

//******************************************************************************
// Includes
//******************************************************************************
#include <Thread.h>
#include <vector>
#include <stdint.h>
#include <linux/can.h>

#include "MessageQueue.h"
#include "Mutex.h"

#define TIME_CHECK_MQ		5000

class Log;
class CanIfr;
class CanBuffer;

//******************************************************************************
// Class Implementation
//******************************************************************************
#define TO_WT_CANRECEIVERTHREAD_US          2
#define TO_WT_CANRECEIVERMANAGERTHREAD_US   2

/*! ***********************************************************************************************
 * @class	CanReceiverThread
 * @brief   Thread used to receive continuously can frame from the interface. The body of the thread
 *			is connected to blocking system read of can interface. When the frame is detected it is
 *			sent to canBuffer structure with the related timestamp.
 * ************************************************************************************************
 */
class CanReceiverThread : public Thread
{

    private:

        CanIfr*			m_pCanInterface;
        Log*			m_pLogger;
        CanBuffer*		m_pCanBuffer;
        Mutex			m_mutexRead;

    public:
        /*! ***********************************************************************************************
         * @brief           CanReceiverThread
         * @brief               Constructor
         * ***********************************************************************************************
         */
        CanReceiverThread();

       /*! ***********************************************************************************************
        * @brief           CanReceiverThread
        * @brief               Destructor
        * ***********************************************************************************************
        */
       virtual ~CanReceiverThread();

       /*! ***********************************************************************************************
        * @brief           init
        * @brief           Initialization of class objects and variables.
        * @param pLogger [in]          logger of the class
        * @param pCanInterface [in]    can interface where receive the messages
        * @param pCanBuffer [in]       buffer where data to read are stored
        * @return 0 if success | otherwise
        * ***********************************************************************************************
        */
       int				init(Log* pLogger, CanIfr* pCanInterface, CanBuffer* pCanBuffer);

       /*! ***********************************************************************************************
        * @brief           setCanBuffer
        * @brief           It sets the buffer to share with  CanReceiverManagerThread for the storage
        *                   of the data.
        * @param  pcanBuffer [in]  pointer to the shared buffer
        * @return 0 if success | -1 otherwise
        * ***********************************************************************************************
        */
       void				setCanBuffer(CanBuffer* pcanBuffer);

    protected:

        void			beforeWorkerThread(void);
        int				workerThread(void);
        void			afterWorkerThread(void);

};

/*! ***************************************************************************************************
 * @class	CanReceiverManagerThread
 * @brief	The body of the thread consists in the control of the data packet present in CanBuffer
 *          structure.
 *			The programs provides only to receive data frames only from can id configured in
 *			the can interface.
 * ****************************************************************************************************
 */
class CanReceiverManagerThread : public Thread
{

	public:
        /*! ***********************************************************************************************
         * @brief           CanReceiverManagerThread
         * @brief               Constructor. All message queues are added to struct conteiner
         *                      vector<MessageQueue *>
         * ***********************************************************************************************
         */
		CanReceiverManagerThread();

        /*! ***********************************************************************************************
         * @brief           CanReceiverManagerThread
         * @brief               Destructor
         * ***********************************************************************************************
         */
		virtual ~CanReceiverManagerThread();

        /*! ***********************************************************************************************
         * @brief           init
         * @brief           Initialization of class objects and variables.
         * @param pLogger [in]          logger of the class
         * @param pCanInterface [in]    can interface where receive the messages
         * @param pCanBuffer [in]       buffer where data to read are stored
         * @param enableA [in]          parameter for enabling section A
         * @param enableB [in]          parameter for enabling section B
         * @return 0 if success | otherwise
         * ***********************************************************************************************
         */
		int init(Log* pLogger, CanIfr* pCanInterface, CanBuffer* pCanBuffer,
				 bool enableA = true, bool enableB = true);

        /*! ***********************************************************************************************
         * @brief           setCanBuffer
         * @brief           It sets the buffer to share with  CanReceiverThread for the storage of the data.
         * @param  pcanBuffer [in]  pointer to the shared buffer
         * @return 0 if success | -1 otherwise
         * ***********************************************************************************************
         */
		void setCanBuffer(CanBuffer* pcanBuffer);

        /*! ***********************************************************************************************
         * @brief           stop
         * @brief           It closes message queue in reading and stops the threads
         * @return          0 if success | -1 otherwise
         * ***********************************************************************************************
         */
		int stop(void);

    protected:

        void beforeWorkerThread(void);
        int workerThread(void);
        void afterWorkerThread(void);


    private:

        /*! ***********************************************************************************************
         * @brief           openSectionQueues
         * @brief           All the sections are stored in pVectorMsgQueue,
         *                  so the functions checks the vector and opens all message queues conteined.
         * @param section [in]  Identifier of the section: SCT_A_ID or SCT_B_ID
         * @param delay [in]   If the function finds an error during the opening process it retries to open
         *                      the message queue every delay time
         * @return 0 if success | -1 otherwise
         * ***********************************************************************************************
         */
		bool openSectionQueues(uint8_t section, uint16_t delay);

        /*! ***********************************************************************************************
         * @brief           closeSectionQueues
         * @brief           Closes the message queues of the requested section.
         * @param section [in]  Identifier of the section: SCT_A_ID or SCT_B_ID
         * @return 0 if success | -1 otherwise
         * ***********************************************************************************************
         */
		bool closeSectionQueues(uint8_t section);

        /*! ***********************************************************************************************
         * @brief           getMsgQueueFromCanId
         * @brief           It retrives the pointer to the message queue according to the canid passed as
         *                  input paramters
         * @param canId [in]  CAN id coming from section
         * @param strQueueName [out]  name of the message queue requested
         * @return 0 if success | -1 otherwise
         * ***********************************************************************************************
         */
        MessageQueue * getMsgQueueFromCanId(canid_t canId, char * strQueueName);

        /*! ***********************************************************************************************
         * @brief           getMsgQueueFromCanId
         * @brief           The function remains in a loop trying to open the passed message queu. When the
         *                  opening is done the function breaks.
         * @param pMsgQueue [in]  pointer to message queue
         * @param strNameQueue [in]  name of the message queue requested
         * @return 0 if success | -1 otherwise
         * ***********************************************************************************************
         */
		void openSingleMsgQueue(MessageQueue * pMsgQueue, char * strNameQueue);

        /*! ***********************************************************************************************
         * @brief           openMsgQueues
         * @brief           It  calls openSectionQueues methods in order to open all message queues.
         * ***********************************************************************************************
         */
		void openMsgQueues(void);

        /*! ***********************************************************************************************
         * @brief           openMsgQueues
         * @brief           It  calls openSectionQueues methods only for section A
         * ***********************************************************************************************
         */
		int	checkMsgQueueA(void);

        /*! ***********************************************************************************************
         * @brief           openMsgQueues
         * @brief           It  calls openSectionQueues methods only for section B
         * ***********************************************************************************************
         */
		int	checkMsgQueueB(void);


    private:

        Log * m_pLogger;
        CanIfr * m_pCanInterface;

        MessageQueue m_CanMsgQueueA;
        MessageQueue m_CanMsgQueueAuxA1;
        MessageQueue m_CanMsgQueueAuxA2;
        MessageQueue m_CanMsgQueueAuxA3;
        MessageQueue m_CanMsgQueueAuxA4;
        MessageQueue m_CanMsgQueueAuxA5;
        MessageQueue m_CanMsgQueueAuxA6;
        MessageQueue m_CanMsgQueueUpFwA;

        MessageQueue m_CanMsgQueueB;
        MessageQueue m_CanMsgQueueAuxB1;
        MessageQueue m_CanMsgQueueAuxB2;
        MessageQueue m_CanMsgQueueAuxB3;
        MessageQueue m_CanMsgQueueAuxB4;
        MessageQueue m_CanMsgQueueAuxB5;
        MessageQueue m_CanMsgQueueAuxB6;
        MessageQueue m_CanMsgQueueUpFwB;

        CanBuffer * m_pCanBuffer;
        bool m_enableA, m_enableB;

        std::vector<MessageQueue *> m_pCanMsgQueueA;
        std::vector<MessageQueue *> m_pCanMsgQueueB;

};

#endif // CANRECEIVER_H
