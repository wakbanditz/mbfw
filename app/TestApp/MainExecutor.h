#ifndef MAINEXECUTOR_H
#define MAINEXECUTOR_H

#include "CommonInclude.h"
#include "StaticSingleton.h"
#include "Log.h"
#include "Loggable.h"
#include "Config.h"
#include "GpioInterface.h"
#include "SPILinkBoard.h"
#include "I2CLinkBoard.h"
#include "USBLinkBoard.h"
#include "CanLinkBoard.h"



class MainExecutor	:	public StaticSingleton<MainExecutor>,
                        public Loggable
{
    public:
        /*! ****************************************************************************************************
         * @brief MainExecutor void constructor
         * *****************************************************************************************************
         */
        MainExecutor();

        /*! ***************************************************************************************************
         * @brief ~MainExecutor default destructor
         * ****************************************************************************************************
         */
        virtual ~MainExecutor();

        /*! *************************************************************************************************
         * @brief	initConfigurator	initializes configuration structure
         * @return	true upon succesfull initialization, false otherwise
         * ************************************************************************************************
         */
        bool initConfigurator(void);

        /*! *************************************************************************************************
         * @brief	initLogger	initializes the logger
         * @return	true upon succesfull initialization, false otherwise
         * **********************************************************************************************
         */
        bool initLogger(void);

        /*! *******************************************************************************************
         * @brief initGpioInterface initializes the gpioInterface object by calling its init() method
         * @return true upon succesfull initialization, false otherwise
         * ********************************************************************************************
         */
        bool initGpioInterface(void);

        /*! ************************************************************************************************
         * @brief closeGpioInterface close the GPIO interface by calling it reset() method
         * ********************************************************************************************
         */
        void closeGpioInterface(void);

        /*! *********************************************************************************************
         * @brief initI2CPeripherals todo...
         * @return
         * *************************************************************************************************
         */
        bool initI2CPeripherals( void );

        /*! ***********************************************************************************************
         * @brief stopI2CPeripherals close I2C peripherals and free resources
         * ********************************************************************************************
         */
        void stopI2CPeripherals(void);

        /*! *************************************************************************************************
         * @brief  initSPIPeripherals initialize SPI peripheral and all the devices SPI related.
         * @return true upon succesfull initialization, false otherwise.
         * ********************************************************************************************
         */
        bool initSPIPeripherals(void);

        /*! ***********************************************************************************************
         * @brief stopSPIPeripherals close SPI peripheral and free resources.
         * ************************************************************************************************
         */
        void stopSPIPeripherals(void);

        /*! **********************************************************************************************
         * @brief  initUSBPeripherals initializes USB and the camera.
         * @return true upon succesfull initialization, false otherwise.
         * ************************************************************************************************
         */
        bool initUSBPeripherals(void);

        /*! *******************************************************************************************
         * @brief stopCamera close USB channel and free resources.
         *******************************************************************************************
         */
        void stopUSBPeripherals(void);

        /*! **********************************************************************************************
         * @brief  initCANPeripherals initializes CAN and the Sections.
         * @return true upon succesfull initialization, false otherwise.
         * ************************************************************************************************
         */
        bool initCANPeripherals(void);

        /*! *******************************************************************************************
         * @brief stop Section and CANLink  and free resources.
         *******************************************************************************************
         */
        void stopCANPeripherals(void);


        bool isPowerManagerEnabled(void);
        bool isGPIOExpSectionEnabled(void);
        bool isLedDriverEnabled(void);
        bool isFanControllerEnabled(void);
        bool isEEPromEnabled(void);
        bool isCodecAudioEnabled(void);
//        bool isStepperMotorEnabled(void);
//        bool isEncoderCounterEnabled(void);
//        bool isNSHEnabled(void);
        bool isCameraEnabled(void);
        bool isSectionAEnabled(void);
        bool isSectionBEnabled(void);



    public:
        //Logger
        Log m_logger;

        // Configurator
        bool m_bConfigIsOk;
        Config m_configurator;

        // Gpio Interface
        GpioInterface m_gpioInterface;

        // I2CBoard
        I2CLinkBoard m_i2cBoard;

        // SPI Board
        SPILinkBoard m_SPIBoard;

        // USB board
        USBLinkBoard m_USBBoard;

        //CAN Board
        CanLinkBoard m_CANLinkBoard;



    private:
        bool    bPowerManager;
        bool    bGPIOExpSection;
        bool    bLedDriver;
        bool    bFanController;
        bool    bEEprom;
        bool    bCodecAudio;
//        bool    bStepperMotor;
//        bool    bEncoderCounter;
//        bool    bNSH;
        bool    bCamera;
        bool    bSectionA;
        bool    bSectionB;

};

#endif // MAINEXECUTOR_H
