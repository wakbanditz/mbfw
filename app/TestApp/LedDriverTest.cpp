#include "LedDriverTest.h"

LedDriverTest::LedDriverTest()
{

}

LedDriverTest::~LedDriverTest()
{

}

void LedDriverTest::showMainMenu()
{
    printf("\n--< CMD LIST >--\n");
    printf("a - LEDOUT\n" );
    printf("b - PWM\n" );
    printf("c - IREF\n");
    printf("d - PWMALL\n");
    printf("f - IREFALL\n" );
    printf("g - GRPFREQ\n" );
    printf("h - GRPPWM\n" );
    printf("i - Enable Dim/Blnk Mode\n");
    printf("l - Set Blinking\n");
    printf("r - resetSoft\n");
    printf("p - clearError\n");
    printf("x - End Test\n" );
}

int LedDriverTest::actionCmd(uint8_t ubCmd)
{
    printf("Prova actionCmd: %X\n", ubCmd);

    if (ubCmd == 'x')
    {
        return -1;
    }

//    uint8_t ubLed = 0;
//	uint8_t ubConf = 0;
//	uint8_t ubState = 0;
//	uint8_t	ubAppo = 0;
//	uint8_t ubPeriod = 0;
//	bool	bLoopCmd = true;

//    switch(sCmd[0])
//	{
//		case 'a':
//			printf("LEDOUT [w,r]: " );
//			scanf("%s",sCmd);
//			printf("\n");

//			if (sCmd[0] == 'w')
//			{
//				printf("LED [0-16]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubLed = atoi(&sCmd[0]);

//				printf("CONF [0-3]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubConf = atoi(&sCmd[0]);

//				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(ubLed, ubConf);
//			}
//			else if (sCmd[0] == 'r')
//			{
//				printf("LED [0-16]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubLed = atoi(&sCmd[0]);

//				if (m_ubTypeCmd == 'b')
//				{
//					while (bLoopCmd == true)
//					{
//						int res = ctrlExit();
//						if (res != 0)
//						{
//							bLoopCmd = false;
//							break;
//						}

//						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readLedOut(ubLed, ubState);
//						printf("LEDOUT conf%d \n", ubState);

//						msleep(200);
//					}
//				}
//				else
//				{
//					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readLedOut(ubLed, ubState);
//					printf("LEDOUT conf%d \n", ubState);
//				}

//			}
//			else
//			{

//			}
//			break;

//		case 'b':
//			printf("PWM [w,r]: " );
//			scanf("%s",sCmd);
//			printf("\n");

//			if (sCmd[0] == 'w')
//			{
//				printf("LED [0-16]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubLed = atoi(&sCmd[0]);

//				printf("BRIGHT [0-255]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubAppo = atoi(&sCmd[0]);

//				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWM(ubLed, ubAppo);
//			}
//			else if (sCmd[0] == 'r')
//			{
//				printf("LED [0-16]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubLed = atoi(&sCmd[0]);

//				if (m_ubTypeCmd == 'b')
//				{
//					while (bLoopCmd == true)
//					{
//						int res = ctrlExit();
//						if (res != 0)
//						{
//							bLoopCmd = false;
//							break;
//						}

//						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readPWM(ubLed, ubState);
//						printf("Brightness: %d \n", ubState);

//						msleep(200);
//					}
//				}
//				else
//				{
//					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readPWM(ubLed, ubState);
//					printf("Brightness: %d \n", ubState);
//				}
//			}
//			else
//			{

//			}
//			break;

//		case 'c':
//			printf("IREF [w,r]: " );
//			scanf("%s",sCmd);
//			printf("\n");

//			if (sCmd[0] == 'w')
//			{
//				printf("LED [0-16]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubLed = atoi(&sCmd[0]);

//				printf("CURR [0-100]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubAppo = atoi(&sCmd[0]);

//				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIref(ubLed, ubAppo);
//			}
//			else if (sCmd[0] == 'r')
//			{
//				printf("LED [0-16]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubLed = atoi(&sCmd[0]);

//				if (m_ubTypeCmd == 'b')
//				{
//					while (bLoopCmd == true)
//					{
//						int res = ctrlExit();
//						if (res != 0)
//						{
//							bLoopCmd = false;
//							break;
//						}

//						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readIref(ubLed, ubState);
//						printf("Current: %d \n", ubState);

//						msleep(200);
//					}
//				}
//				else
//				{
//					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readIref(ubLed, ubState);
//					printf("Current: %d \n", ubState);
//				}
//			}
//			else
//			{

//			}
//			break;

//		case 'd':
//			printf("PWMALL [w,r]: " );
//			scanf("%s",sCmd);
//			printf("\n");

//			if (sCmd[0] == 'w')
//			{
//				printf("PWM [0-255]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubAppo = atoi(&sCmd[0]);

//				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWMAll(ubAppo);
//			}
//			else if (sCmd[0] == 'r')
//			{
//				if (m_ubTypeCmd == 'b')
//				{
//					while (bLoopCmd == true)
//					{
//						int res = ctrlExit();
//						if (res != 0)
//						{
//							bLoopCmd = false;
//							break;
//						}

//						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readPWMAll( ubState);
//						printf("PWM: %d \n", ubState);

//						msleep(200);
//					}
//				}
//				else
//				{
//					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readPWMAll( ubState);
//					printf("PWM: %d \n", ubState);
//				}
//			}
//			else
//			{
//			}
//			break;

//		case 'f':
//			printf("IREFALL [w,r]: " );
//			scanf("%s",sCmd);
//			printf("\n");

//			if (sCmd[0] == 'w')
//			{
//				printf("CURRENT [0-100]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubAppo = atoi(&sCmd[0]);

//				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIrefAll(ubAppo);
//			}
//			else if (sCmd[0] == 'r')
//			{
//				if (m_ubTypeCmd == 'b')
//				{
//					while (bLoopCmd == true)
//					{
//						int res = ctrlExit();
//						if (res != 0)
//						{
//							bLoopCmd = false;
//							break;
//						}

//						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readIrefAll(ubState);
//						printf("CURR: %d \n", ubState);

//						msleep(200);
//					}
//				}
//				else
//				{
//					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readIrefAll(ubState);
//					printf("CURR: %d \n", ubState);
//				}
//			}
//			else
//			{
//			}
//			break;

//		case 'g':
//			printf("GRPFREQ [w,r]: " );
//			scanf("%s",sCmd);
//			printf("\n");

//			if (sCmd[0] == 'w')
//			{
//				printf("FREQ [0-100]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubAppo = atoi(&sCmd[0]);

//				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpFreq(ubAppo);
//			}
//			else if (sCmd[0] == 'r')
//			{
//				if (m_ubTypeCmd == 'b')
//				{
//					while (bLoopCmd == true)
//					{
//						int res = ctrlExit();
//						if (res != 0)
//						{
//							bLoopCmd = false;
//							break;
//						}

//						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readGrpFreq(ubState);
//						printf("FREQ: %d \n", ubState);

//						msleep(200);
//					}
//				}
//				else
//				{
//					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readGrpFreq(ubState);
//					printf("FREQ: %d \n", ubState);
//				}
//			}
//			else
//			{
//			}
//			break;

//		case 'h':
//			printf("GRPPWM [w,r]: " );
//			scanf("%s",sCmd);
//			printf("\n");

//			if (sCmd[0] == 'w')
//			{
//				printf("PWM [0-100]: " );
//				scanf("%s",sCmd);
//				printf("\n");

//				ubAppo = atoi(&sCmd[0]);

//				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpPWM(ubAppo);
//			}
//			else if (sCmd[0] == 'r')
//			{
//				if (m_ubTypeCmd == 'b')
//				{
//					while (bLoopCmd == true)
//					{
//						int res = ctrlExit();
//						if (res != 0)
//						{
//							bLoopCmd = false;
//							break;
//						}

//						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readGrpPWM(ubState);
//						printf("PWM: %d \n", ubState);

//						msleep(200);
//					}
//				}
//				else
//				{
//					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readGrpPWM(ubState);
//					printf("PWM: %d \n", ubState);
//				}
//			}
//			else
//			{
//			}
//			break;

//		case 'i':
//			printf("1 - DIM Mode\n " );
//			printf("2 - BLNK Mode\n " );
//			scanf("%s",sCmd);
//			printf("\n");

//			if (sCmd[0] == '2')
//			{
//				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_BLNK_CTL);
//				printf("Set blinking mode\n " );
//			}
//			else
//			{
//				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_DIM_CTL);
//				printf("Set dimming mode\n " );
//			}
//			break;

//		case 'l':
//			printf("LED [0-16]: " );
//			scanf("%s",sCmd);
//			printf("\n");

//			ubLed = atoi(&sCmd[0]);

//            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIref(ubLed, 25);
//			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWM(ubLed, 50);
//			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpPWM(50);//dc 50%

//			printf("Freq [1s-15s]" );
//			scanf("%s",sCmd);
//			printf("\n");

//			ubAppo = atoi(&sCmd[0]);
//			ubPeriod = (15.26*ubAppo)-1;
//			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpFreq(ubPeriod);
//			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(ubLed, 3);
//			break;

//		case 'r':

//			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->resetSoft();
//			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_DIM_CTL);
//			printf("RESET \n" );
//			break;

//        case 'p':

//            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->clearError();

//            printf("CLEAR ERROR \n" );
//            break;

//		case 'x':
//		default:
//			return -1;
//		break;
//	}
    return -1;
}
