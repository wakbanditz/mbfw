TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#internal libraries inclusion
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/StaticSingleton
DEPENDPATH += $$PWD/../../bmx_libs/Libs/StaticSingleton

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/IPC/ -lIPC
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/IPC
DEPENDPATH += $$PWD/../../bmx_libs/Libs/IPC
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/IPC/libIPC.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/GpioInterface/ -lGpioInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/GpioInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/GpioInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/GpioInterface/libGpioInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/I2CInterface/ -lI2CInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/I2CInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/I2CInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/I2CInterface/libI2CInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/SPIInterface/ -lSPIInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/SPIInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/SPIInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/SPIInterface/libSPIInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/USBInterface/ -lUSBInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/USBInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/USBInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/USBInterface/libUSBInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/CanInterface/ -lCanInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/CanInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/CanInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/CanInterface/libCanInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Thread
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Log
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/TimeStamp
DEPENDPATH += $$PWD/../../bmx_libs/Libs/TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/TimeStamp/libTimeStamp.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/DataCollector/ -lDataCollector
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/DataCollector
DEPENDPATH += $$PWD/../../bmx_libs/Libs/DataCollector
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/DataCollector/libDataCollector.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Config/ -lConfig
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Config
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Config/libConfig.a


INCLUDEPATH += $$PWD/../../
INCLUDEPATH += $$PWD/TestManagers/

# system libraries
LIBS += -pthread -lrt -lpthread  -lcurl -lasound
LIBS += -lusb-1.0
LIBS += -ludev

SOURCES += \
        MainExecutor.cpp \
        main.cpp

HEADERS += \
    MainExecutor.h

# inclusions and defines
include(../../mbfwInclude.pri)
include(PeripheralLinks/PeripheralLinks.pri)
include(TestManagers/TestManagers.pri)

#DEFINES += CONF_FILE=\\\"$${CONFIG_FILE}\\\"
DEFINES += PROGRAM_NAME=\\\"$${PROCESS_NAME_TEST_APP}\\\"

TARGET = TestApp

# deployment directives
target.path = /home/root
INSTALLS += target

DISTFILES += \
#    PeripheralLinks/I2CLink/I2CLink.pri \
#    PeripheralLinks/PeripheralLinks.pri \
#    PeripheralLinks/SPILink/SPILink.pri
