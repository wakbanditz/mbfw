#include <signal.h>

#include "CommonInclude.h"
#include "Log.h"
#include "Config.h"
#include "MainExecutor.h"
#include "SelectorTest.h"

static bool bKeepRunning = true;

void signalHandler(int nSignal)
{
    static int sig_counter = 1;
    printf("Received signal %d\n", nSignal);
    switch(nSignal)
    {
        case SIGTERM:
        case SIGINT:
        case SIGKILL:
            bKeepRunning = false;
            if ( ++sig_counter > 5 )
            {
                // Fifth time that the soft mode has failed brute exits
                printf("Brute force exit ...");
                exit(1);
            }
        break;
    }
}
int main()
{
    signal(SIGINT, signalHandler);
    signal(SIGKILL, signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGTSTP, signalHandler);

    /* *************************************************************************************************************
     * INITIALIZATION STAGE
     * *************************************************************************************************************
     */
    bool bRes;
    int startError = 0;
    bRes = MainExecutor::getInstance()->initConfigurator();
    if ( bRes == false )
    {
        printf("Main unable to init Configurator\n");
        exit(1);
    }

    bRes = MainExecutor::getInstance()->initLogger();
    if ( bRes == false )
    {
        printf("Main unable to init Logger\n");
        exit(1);
    }

    // Logging
    Log* plog = MainExecutor::getInstance()->getLogger();
    plog->log(LOG_INFO, "Starting << %s >>", PROGRAM_NAME);

    //GPIO Initialization
    bRes = MainExecutor::getInstance()->initGpioInterface();
    if ( bRes == false )
    {
        plog->log(LOG_ERR, "Main unable to initialize Gpio interface");
        startError++;
    }

    //I2C Peripherals
    bRes = MainExecutor::getInstance()->initI2CPeripherals();
    if ( bRes == false )
    {
        plog->log(LOG_ERR, "Main unable to initialize I2C peripherals");
        startError++;
    }

    //SPI Peripherals
    bRes = MainExecutor::getInstance()->initSPIPeripherals();
    if (bRes == false)
    {
        plog->log(LOG_ERR, "Main unable to initialize SPI peripherals");
        startError++;
    }

    //USB Peripherals
    bRes = MainExecutor::getInstance()->initUSBPeripherals();
    if (bRes == false)
    {
        plog->log(LOG_ERR, "Main unable to initialize USB peripherals");
        startError++;
    }

    //CAN Peripherals
    bRes = MainExecutor::getInstance()->initCANPeripherals();
    if (bRes == false)
    {
        plog->log(LOG_ERR, "Main unable to initialize CAN peripherals");
        startError++;
    }

    /*
     * *************************************************************************************************************
     * MAIN CYCLE
     * *************************************************************************************************************
     */
    SelectorTest selector;
    selector.setLogger(plog);
    selector.startThread();


    while ( bKeepRunning )
    {
        usleep(100);

        if (selector.isStopped())
        {
            bKeepRunning = false;
        }

    }


    //Closing STAGE
    MainExecutor::getInstance()->stopI2CPeripherals();
    MainExecutor::getInstance()->stopSPIPeripherals();
    MainExecutor::getInstance()->stopUSBPeripherals();
    MainExecutor::getInstance()->stopCANPeripherals();
    MainExecutor::getInstance()->closeGpioInterface();


    plog->log(LOG_INFO, "Main: stop main cycle. Exit program");

    return 0;
}
