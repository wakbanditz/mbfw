/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    L6472Base.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the L6472Base class.
 @details

 ****************************************************************************
*/

#ifndef L6472BASE_H
#define L6472BASE_H

#include <string.h>

#include "SPIInterface.h"
#include "ErrorCodes.h"
#include "SPILinkCommonInclude.h"
#include "L6472Library.h"
#include "NSHMotorGeneral.h"



class L6472Base : protected NSHMotorGeneral
{

	public:

		/*! **********************************************************************************************************************
		 * @brief L6472BoardBase default constructor.
		 * ***********************************************************************************************************************
		 */
		L6472Base();

		/*! **********************************************************************************************************************
		 * @brief ~L6472BoardBase virtual destructor.
		 * ***********************************************************************************************************************
		 */
		virtual ~L6472Base();

		/*! **********************************************************************************************************************
		 * @brief  initdSPINRegsStruct initialize dSPIN_RegsStruct with the values to be sent to the motor driver.
		 * @return true if initialization ended with success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool initdSPINRegsStruct();

		/*! **********************************************************************************************************************
		 * @brief  sendL6472Cmd send a command to the motor driver (execute function or set  register).
		 * @param  eCmd command to be executed.
         * @param  liValue parameter related to the command sent.
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -2 if cmd not managed, -3 if communication error.
		 * ***********************************************************************************************************************
		 */
		int8_t sendL6472Cmd(enumL6472Commands eCmd, int32_t liValue);

		/*! **********************************************************************************************************************
		 * @brief  getL6472Param send a command to the board and retrieve the response
         * @param  eParam command to send to the board. The response given by the motor driver is stored in the apposite
		 *		   field of dSPINRegsStruct.
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -2 if cmd not managed, -3 if communication error.
		 * ***********************************************************************************************************************
		 */
		int8_t getL6472Param(enumL6472GetParams eParam);

		/*! **********************************************************************************************************************
		 * @brief  resetPos reset the home position (to be called at the end of the home procedure).
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -2 if cmd not menaged.
		 * ***********************************************************************************************************************
		 */
		virtual int8_t resetPos() = 0;

	private:

		/*! **********************************************************************************************************************
		 * @brief  parseSendL6472Cmd1 parse sendL6472Cmd function with expected txLength = 1 and set the cmd byte.
		 * @param  ucCommand command to be parsed.
		 * @return true if the command is found, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool parseSendL6472Cmd1(uint8_t ucCommand);

		/*! **********************************************************************************************************************
		 * @brief  parseSendL6472Cmd2 parse sendL6472Cmd function with expected txLength = 2 and set the cmd byte.
		 * @param  ucCommand command to be parsed.
		 * @return true if the command is found, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool parseSendL6472Cmd2(uint8_t ucCommand);


		/*! **********************************************************************************************************************
		 * @brief  parseSendL6472Cmd3 parse sendL6472Cmd function with expected txLength = 3 and set the cmd byte.
		 * @param  ucCommand command to be parsed.
		 * @return true if the command is found, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool parseSendL6472Cmd3(uint8_t ucCommand);


		/*! **********************************************************************************************************************
		 * @brief  parseSendL6472Cmd4 parse sendL6472Cmd function with expected txLength = 4 and set the cmd byte.
		 * @param  ucCommand command to be parsed.
		 * @return true if the command is found, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool parseSendL6472Cmd4(uint8_t ucCommand);


		/*! **********************************************************************************************************************
		 * @brief  parseGetL6472Param1 parse getL6472Param function with expected rxLength = 1 and set the cmd byte.
		 * @param  ucCommand command to be parsed.
		 * @return true if the command is found, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool parseGetL6472Param1(uint8_t ucCommand);

		/*! **********************************************************************************************************************
		 * @brief  parseGetL6472Param2 parse getL6472Param function with expected rxLength = 2 and set the cmd byte.
		 * @param  ucCommand command to be parsed.
		 * @return true if the command is found, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool parseGetL6472Param2(uint8_t ucCommand);

		/*! **********************************************************************************************************************
		 * @brief  processL6472Reply processes the rx message from the motor driver.
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -2 if cmd not menaged.
		 * ***********************************************************************************************************************
		 */
		int8_t processL6472Reply();

		/*! **********************************************************************************************************************
		 * @brief  initRegisters initialize a specific register with the values to be sent to the motor driver.
		 * @param  ucReg register to be initilized.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool initRegisters(uint8_t ucReg);

		/*! **********************************************************************************************************************
		 * @brief  processL6472Status take an action depending on structL6472.ucStatus.
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -2 if cmd not menaged.
		 * ***********************************************************************************************************************
		 */
		int8_t processL6472Status();

		/*! **********************************************************************************************************************
		 * @brief  checkL6472Connection check the status of the motor driver and in case notify the error.
		 * @return false in case of error, true otherwise.
		 * ***********************************************************************************************************************
		 */
		bool checkL6472Connection();

		/*! **********************************************************************************************************************
		 * @brief stopMotor pure virtual function used to find out if the motor is at home.
		 * ***********************************************************************************************************************
		 */
		virtual bool isSensorAtHome() = 0;

		/*! **********************************************************************************************************************
		 * @brief  releaseSW executes the release sw command.
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -2 if cmd not menaged.
		 * ***********************************************************************************************************************
		 */
		int8_t releaseSW();

		/*! **********************************************************************************************************************
		 * @brief  goUntil send the go until home command.
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -2 if cmd not menaged.
		 * ***********************************************************************************************************************
		 */
		int8_t goUntil();

		/*! **********************************************************************************************************************
		 * @brief  isIdle check if the motor is in idle decoding the status variable.
		 * @return true if the motor is in idle, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool isIdle();

    protected:

        bool				m_bConfigOK;
        bool				m_bIsMotorEnabled;
        SPIInterface		*m_pSPIIfr;
        dSPIN_RegsStruct	dSPINRegsStruct;
        structMotorL6472	structL6472;
        uint8_t				m_rgcTxBuf[NSH_MOTOR_MAX_PKT_SIZE];
        uint8_t				m_rgcRxBuf[NSH_MOTOR_MAX_PKT_SIZE];

};

#endif // L6472BASE_H
