/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHMotorGeneral.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the L6472Busy class.
 @details

 ****************************************************************************
*/

#ifndef NSHMOTORGENERAL_H
#define NSHMOTORGENERAL_H

#include "CommonInclude.h"
#include "Loggable.h"
#include "GpioInterface.h"
#include "TimeStamp.h"


class NSHMotorGeneral
{
	public:

		/*! **********************************************************************************************************************
		 * @brief NSHMotorBusy default constructor.
		 * ***********************************************************************************************************************
		 */
		NSHMotorGeneral();

		/*! **********************************************************************************************************************
		 * @brief ~NSHMotorBusy virtual destructor.
		 * ***********************************************************************************************************************
		 */
		virtual ~NSHMotorGeneral();

		/*! **********************************************************************************************************************
		 * @brief  getBUSYGpioVal used to retrieve the pin's value.
		 * @return -1 in case of error, gpio's value otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t getBUSYGpioVal(void);

		/*! **********************************************************************************************************************
		 * @brief  checkBUSYValWithTimeout check if the motor is busy or not with a timeout. The GPIO is active low.
		 * @param  uliTimeOutMsec timeout during which the gpio is monitored.
		 * @return -1 in case of error, -2 if the motor is still busy after timeout, 0 in case of success.
		 * ***********************************************************************************************************************
		 */
		int8_t checkBUSYValWithTimeout(uint32_t uliTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  getFaultGpioVal used to retrieve the pin's value.
		 * @return -1 in case of error, gpio's value otherwise. 0 is fault, 1 is ok.
		 * ***********************************************************************************************************************
		 */
		int8_t getFaultGpioVal(void);

	protected:

		/*! **********************************************************************************************************************
		 * @brief stopMotor pure virtual function used to hard stop the motor. Defined in L6472BoardGeneral.
		 * ***********************************************************************************************************************
		 */
		virtual bool stopMotor() = 0;

    protected:

        GpioInterface*	m_pGpio;
        Log*			m_pLogger;

    public:

		uint8_t m_nBusyGpioIdx;
		uint8_t m_nFaultGpioIdx;

};

#endif // NSHMOTORGENERAL_H
