/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    L6472Base.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the L6472Base class.
 @details

 ****************************************************************************
*/

#include "L6472Base.h"

/*! **********************************************************************************************************************
 *
 * All possible workflows:
 *
 * 1) called public function from L6472BoardGeneral;
 * 2) set/get command is called and the command is sent;
 * 3) the reply is processed through processL6472Reply();
 * 4) the workflow can end here or:
 *		4.1) can be checked if L6472 is busy, if not releaseSW/resetPos are called. Start again from 2).
 *		4.2) can be checked if L6472 is busy, if not getL6472Command is called. Start again from 3).
 *		4.3) can be checked if L6472 is busy, if not processL6472Status is called. Is sent the checkL6472Connection
 *			 command (restart from 2). Then workflow can end here or:
 *				4.1.1) can be called commands (as releaseSw or goUntil). Start again from 2).
 *				4.2.2) can be called checkBUSYValWithTimeout.
 *
 * ***********************************************************************************************************************
 */


L6472Base::L6472Base()
{
	m_bConfigOK = false;
	m_bIsMotorEnabled = false;
	m_pSPIIfr = NULL;

	memset(&dSPINRegsStruct, 0x00, sizeof(dSPIN_RegsStruct));
	memset(&structL6472, 0x00, sizeof(structMotorL6472));

	memset(&m_rgcTxBuf, 0x00, sizeof(m_rgcTxBuf));
	memset(&m_rgcRxBuf, 0x00, sizeof(m_rgcRxBuf));
}


L6472Base::~L6472Base()
{

}


bool L6472Base::initdSPINRegsStruct()
{
	structL6472.ucStatus = L6472_Idle;
	structL6472.usMotorStatus.usiData = 0;
	structL6472.ucForwardDirection = FORWARD;
	structL6472.ucBackDirection = BACK;

	dSPINRegsStruct.ABS_POS = 0;
	dSPINRegsStruct.EL_POS = 0;
	dSPINRegsStruct.MARK = 0;
	/* Acceleration rate settings to dSPIN_CONF_PARAM_ACC in steps/s2, range 14.55 to 59590 steps/s2 */
	dSPINRegsStruct.ACC 		= AccDec_Steps_to_Par(dSPIN_CONF_PARAM_ACC);
	/* Deceleration rate settings to dSPIN_CONF_PARAM_DEC in steps/s2, range 14.55 to 59590 steps/s2 */
	dSPINRegsStruct.DEC 		= AccDec_Steps_to_Par(dSPIN_CONF_PARAM_DEC);
	/* Maximum speed settings to dSPIN_CONF_PARAM_MAX_SPEED in steps/s, range 15.25 to 15610 steps/s */
	dSPINRegsStruct.MAX_SPEED 	= MaxSpd_Steps_to_Par(dSPIN_CONF_PARAM_MAX_SPEED);
	/* Full step speed settings dSPIN_CONF_PARAM_FS_SPD in steps/s, range 7.63 to 15625 steps/s */
	dSPINRegsStruct.FS_SPD		= FSSpd_Steps_to_Par(dSPIN_CONF_PARAM_FS_SPD);
	/* Minimum speed settings to dSPIN_CONF_PARAM_MIN_SPEED in steps/s, range 0 to 976.3 steps/s */
	dSPINRegsStruct.MIN_SPEED	= MinSpd_Steps_to_Par(dSPIN_CONF_PARAM_MIN_SPEED);
	/* Torque regulation DAC current during motor acceleration, range 31.25mA to 4000mA */
	dSPINRegsStruct.TVAL_ACC 	= Tval_Current_to_Par(dSPIN_CONF_PARAM_TVAL_ACC);
	/* Torque regulation DAC current during motor deceleration, range 31.25mA to 4000mA */
	dSPINRegsStruct.TVAL_DEC 	= Tval_Current_to_Par(dSPIN_CONF_PARAM_TVAL_DEC);
	/* Torque regulation DAC current when motor is running, range 31.25mA to 4000mA */
	dSPINRegsStruct.TVAL_RUN 	= Tval_Current_to_Par(dSPIN_CONF_PARAM_TVAL_RUN);
	/* Torque regulation DAC current when motor is stopped, range 31.25mA to 4000mA */
	dSPINRegsStruct.TVAL_HOLD 	= Tval_Current_to_Par(dSPIN_CONF_PARAM_TVAL_HOLD);
	/* Maximum fast decay and fall step times used by the current control system, range 2us to 32us */
	dSPINRegsStruct.T_FAST		= (uint8_t)dSPIN_CONF_PARAM_TOFF_FAST | (uint8_t)dSPIN_CONF_PARAM_FAST_STEP;
	/* Minimum ON time value used by the current control system, range 0.5us to 64us */
	dSPINRegsStruct.TON_MIN 	= Tmin_Time_to_Par(dSPIN_CONF_PARAM_TON_MIN);
	/* Minimum OFF time value used by the current control system, range 0.5us to 64us */
	dSPINRegsStruct.TOFF_MIN	= Tmin_Time_to_Par(dSPIN_CONF_PARAM_TOFF_MIN);

	/* Set Config register according to config parameters */
	/* clock setting, switch hard stop interrupt mode,	  */
	/* supply voltage compensation, overcurrent shutdown  */
	/* slew-rate , target switching period, predictive current control */
	dSPINRegsStruct.CONFIG 	= (uint16_t)dSPIN_CONF_PARAM_CLOCK_SETTING	|
							  (uint16_t)dSPIN_CONF_PARAM_SW_MODE		|
							  (uint16_t)dSPIN_CONF_PARAM_TQ_REG			|
							  (uint16_t)dSPIN_CONF_PARAM_OC_SD			|
							  (uint16_t)dSPIN_CONF_PARAM_SR				|
							  (uint16_t)dSPIN_CONF_PARAM_TSW			|
							  (uint16_t)dSPIN_CONF_PARAM_PRED			|

							  (uint16_t)dSPIN_CONFIG_SW_USER;
	/* Overcurrent threshold settings to dSPIN_CONF_PARAM_OCD_TH in mA */
	dSPINRegsStruct.OCD_TH 	= dSPIN_CONF_PARAM_OCD_TH;
	/* Alarm settings to dSPIN_CONF_PARAM_ALARM_EN */
	dSPINRegsStruct.ALARM_EN 	=  dSPIN_CONF_PARAM_ALARM_EN;
	/* Step mode and sync mode settings via dSPIN_CONF_PARAM_SYNC_MODE and dSPIN_CONF_PARAM_STEP_MODE */
	dSPINRegsStruct.STEP_MODE 	= (uint8_t)dSPIN_CONF_PARAM_SYNC_MODE | (uint8_t)dSPIN_CONF_PARAM_STEP_MODE;

	structL6472.ucStatus = L6472_Init;

	bool bRes;
	for ( uint8_t ucReg = 0; ucReg < NSHM_L6472_REG_NUM; ucReg++ )
	{
		bRes = initRegisters(ucReg);

		if ( !bRes )
		{
			m_pLogger->log(LOG_ERR, "L6472Base::initdSPINRegsStruct: unable to initialize stepper motor. Error in initializing registers.");
			break;
		}
	}
	return bRes;
}


int8_t L6472Base::sendL6472Cmd(enumL6472Commands eCmd, int32_t liValue)
{
	memset(&m_rgcTxBuf, NSHM_NOP, sizeof(m_rgcTxBuf));
	memset(&m_rgcRxBuf, NSHM_NOP, sizeof(m_rgcRxBuf));

	bool bCmdFound = false;

	uint8_t ucTxLength = 0;
	uint8_t ucRxLength = 0;

	while (true)
	{
		bCmdFound = parseSendL6472Cmd1(eCmd);

		if ( bCmdFound )
		{
			ucTxLength	= 1;
			break;
		}

		bCmdFound = parseSendL6472Cmd2(eCmd);
		if ( bCmdFound )
		{
			m_rgcTxBuf[1] = (uint8_t)(liValue);			// parameter value
			ucTxLength = 2;
			break;
		}

		bCmdFound = parseSendL6472Cmd3(eCmd);

		if ( bCmdFound )
		{
			m_rgcTxBuf[1] = (uint8_t)(liValue >> 8);	// MSB parameter value
			m_rgcTxBuf[2] = (uint8_t)(liValue);			// LSB parameter value
			ucTxLength	= 3;
			break;
		}

		bCmdFound = parseSendL6472Cmd4(eCmd);

		if ( bCmdFound )
		{
			m_rgcTxBuf[1] = (uint8_t)(liValue >> 16);	// position MSB value
			m_rgcTxBuf[2] = (uint8_t)(liValue >> 8);	//
			m_rgcTxBuf[3] = (uint8_t)(liValue);			// position LSB value
			ucTxLength = 4;
			break;
		}

		bCmdFound = true;	// Other accepted commands

		switch( eCmd )
		{
			case eCmdGetStatus :
				m_rgcTxBuf[0] = dSPIN_GET_STATUS;		// code for getStatus
				ucTxLength = 1;
				ucRxLength = 2;
			break;

			case eCmdGoUntil :
				m_rgcTxBuf[0] = (dSPIN_GO_UNTIL | (uint8_t)(liValue));	// code for goUntil + direction
				m_rgcTxBuf[1] = (uint8_t)(liValue >> 24);				// speed MSB value
				m_rgcTxBuf[2] = (uint8_t)(liValue >> 16);				//
				m_rgcTxBuf[3] = (uint8_t)(liValue >> 8);				// speed LSB value
				ucTxLength = 4;
			break;

			case eCmdReleaseSw :
				m_rgcTxBuf[0] = (dSPIN_RELEASE_SW | (uint8_t)liValue);	// code for releaseSW + direction
				ucTxLength = 1;
			break;

			case eCmdMove :
				m_rgcTxBuf[0] = (dSPIN_MOVE | (uint8_t)(liValue));		// code for Move + direction
				m_rgcTxBuf[1] = (uint8_t)(liValue >> 24);				// steps MSB value
				m_rgcTxBuf[2] = (uint8_t)(liValue >> 16);				//
				m_rgcTxBuf[3] = (uint8_t)(liValue >> 8);				// steps LSB value
				ucTxLength = 4;
			break;

			default :
				bCmdFound = false;
			break;
		}

		break;
    }

	if ( bCmdFound )
	{
		bool bSendOpRes;
		// send command to the driver
		for (int i = 0; i < ucTxLength; i++)
		{
			bSendOpRes = m_pSPIIfr->singleTransfer(&m_rgcTxBuf[i], &m_rgcRxBuf[0], 1);

			if ( bSendOpRes != true )	return ERR_NSH_MOTOR_COMMUNICATION;
		}
		// send empty bytes to get the answer from the driver
		for (int i = 0; i < ucRxLength; i++)
		{
			bSendOpRes = m_pSPIIfr->singleTransfer(NSHM_NOP, &m_rgcRxBuf[i], 1);

			if ( bSendOpRes != true )	return ERR_NSH_MOTOR_COMMUNICATION;
		}
	}
	else
	{
		m_pLogger->log(LOG_ERR, "L6472Base::setL6472Command: Command not managed.");
		return ERR_NSH_MOTOR_CMD_NOT_MANAGED;
	}

	int8_t cRes = processL6472Reply();

	return cRes;
}


int8_t L6472Base::getL6472Param(enumL6472GetParams eParam)
{
	memset(&m_rgcTxBuf, NSHM_NOP, sizeof(m_rgcTxBuf));
	memset(&m_rgcRxBuf, NSHM_NOP, sizeof(m_rgcRxBuf));

	bool bCmdFound = false;

	uint8_t ucRxLenght = 0;

	bCmdFound = parseGetL6472Param1(eParam);

	if ( bCmdFound )
	{
		ucRxLenght = 1;
	}
	else
	{
		bCmdFound = parseGetL6472Param2(eParam);

		if( bCmdFound )
		{
			ucRxLenght = 2;
		}
	}

	if( !bCmdFound )
	{
		ucRxLenght = 3;
		bCmdFound = 1;

		switch ( eParam )
		{
			case eGetAbsPos:
				m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_ABS_POS);
			break;

			case eGetMark:
				m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_MARK);
			break;

			case eGetSpeed:
				m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_SPEED);
			break;

			default:
				m_pLogger->log(LOG_ERR, "L6472Base::setL6472Command: Command not managed.");
				return -2;
			break;
		}
	}

	if ( !bCmdFound )
	{
		m_pLogger->log(LOG_ERR, "L6472Base::setL6472Command: Command not managed.");
		return -2;
	}

	bool bSendOpRes;
	// Get parameter is always 1B command.
	bSendOpRes = m_pSPIIfr->singleTransfer(&m_rgcTxBuf[0], &m_rgcRxBuf[0], 1);

	if ( bSendOpRes != true )	return ERR_NSH_MOTOR_COMMUNICATION;

	// send empty bytes to get the answer from the driver
	for (int i = 0; i < ucRxLenght; i++)
	{
		bSendOpRes = m_pSPIIfr->singleTransfer(NSHM_NOP, &m_rgcRxBuf[i], 1);

		if ( bSendOpRes != true )	return ERR_NSH_MOTOR_COMMUNICATION;
	}

	int8_t cRes = processL6472Reply();

	return cRes;
}


bool L6472Base::parseSendL6472Cmd1(uint8_t ucCommand)
{
	switch (ucCommand)
	{
		case eCmdSetHiZ:
			m_rgcTxBuf[0] = dSPIN_HARD_HIZ;
		break;

		case eCmdReset:
			m_rgcTxBuf[0] = dSPIN_RESET_DEVICE;
		break;

		case eCmdResetPos:
			m_rgcTxBuf[0] = dSPIN_RESET_POS;
		break;

		case eCmdHardStop :
			m_rgcTxBuf[0] = dSPIN_HARD_STOP;
		break;

		default:
			return false;
		break;
	}

	return true;
}


bool L6472Base::parseSendL6472Cmd2(uint8_t ucCommand)
{
	switch (ucCommand)
	{
		case eSetTValHold:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_TVAL_HOLD);	// code for setParam, step register
		break;

		case eSetTValRun:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_TVAL_RUN);
		break;

		case eSetTValAcc:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_TVAL_ACC);
		break;

		case eSetTValDec:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_TVAL_DEC);
		break;

		case eSetTFast:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_T_FAST);
		break;

		case eSetOnMinTime:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_TON_MIN);
		break;

		case eSetOffMinTime:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_TOFF_MIN);
		break;

		case eSetODCThreshold:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_OCD_TH);
		break;

		case eSetAlarmEnable:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_ALARM_EN);
		break;

		case eSetStepMode:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_STEP_MODE);
		break;

		case eSetADCOut:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_ADC_OUT);
		break;

		default:
			return false;
		break;
	}

	return true;
}


bool L6472Base::parseSendL6472Cmd3(uint8_t ucCommand)
{
	switch (ucCommand)
	{
		case eSetElPos:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_EL_POS);		// code for setParam + specific register
		break;

		case eSetAcc:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_ACC);
		break;

		case eSetDec:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_DEC);
		break;

		case eSetMaxSpeed:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_MAX_SPEED);
		break;

		case eSetMinSpeed:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_MIN_SPEED);
		break;

		case eSetFullStepSpeed:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_FS_SPD);
		break;

		case eSetConfig:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_CONFIG);
		break;

		default:
			return false;
		break;
	}

	return true;
}


bool L6472Base::parseSendL6472Cmd4(uint8_t ucCommand)
{
	switch (ucCommand)
	{
		case eSetAbsPos:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_ABS_POS);
		break;

		case eSetMark:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_MARK);
		break;

		case eSetSpeed:
			m_rgcTxBuf[0] = (dSPIN_SET_PARAM | dSPIN_SPEED);
		break;

		case eCmdGoTo:
			m_rgcTxBuf[0] = dSPIN_GO_TO;
		break;

		default:
			return false;
		break;
	}

	return true;
}


bool L6472Base::parseGetL6472Param1(uint8_t ucCommand)
{	
	switch (ucCommand)
	{
		case eGetTValHold:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_TVAL_HOLD);	// code for setParam, step register
		break;

		case eGetTValRun:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_TVAL_RUN);
		break;

		case eGetTValAcc:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_TVAL_ACC);
		break;

		case eGetTValDec:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_TVAL_DEC);
		break;

		case eGetTFast:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_T_FAST);
		break;

		case eGetOnMinTime:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_TON_MIN);
		break;

		case eGetOffMinTime:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_TOFF_MIN);
		break;

		case eGetODCThreshold:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_OCD_TH);
		break;

		case eGetAlarmEnable:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_ALARM_EN);
		break;

		case eGetStepMode:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_STEP_MODE);
		break;

		case eGetADCOut:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_ADC_OUT);
		break;

		case eGetStatus:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_STATUS);
		break;

		default:
			return false;
		break;
	}

	return true;
}


bool L6472Base::parseGetL6472Param2(uint8_t ucCommand)
{	
	switch (ucCommand)
	{
		case eGetElPos:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_EL_POS);	// code for getParam + specific register
		break;

		case eGetAcc:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_ACC);
		break;

		case eGetDec:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_DEC);
		break;

		case eGetMaxSpeed:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_MAX_SPEED);
		break;

		case eGetMinSpeed:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_MIN_SPEED);
		break;

		case eGetFullStepSpeed:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_FS_SPD);
		break;

		case eGetConfig:
			m_rgcTxBuf[0] = (dSPIN_GET_PARAM | dSPIN_CONFIG);
		break;

		default :
			return false;
		break;
	}

	return true;
}


int8_t L6472Base::processL6472Reply()
{
	uint8_t ucAddress = m_rgcTxBuf[0] & 0x1F;			// isolate the register address
	uint8_t ucOperationCode = m_rgcTxBuf[0] & 0xE0;		// isolate the opcode (3 bits)
	int32_t liPosition;
	int8_t  cRes = 0;

	switch (ucOperationCode)
	{
		case dSPIN_SET_PARAM:
		case dSPIN_GET_PARAM:

			if ( ucOperationCode == dSPIN_GET_PARAM && ucAddress == dSPIN_ABS_POS )
			{
					liPosition  = ( m_rgcRxBuf[0] << 16 );	// MSB
					liPosition |= ( m_rgcRxBuf[1] << 8  );	// LSB
					liPosition |= ( m_rgcRxBuf[2] );		// LSB
					dSPINRegsStruct.ABS_POS = liPosition;
					m_pLogger->log(LOG_DEBUG_PARANOIC, "L6472Base::processL6472Reply: dSPINRegsStruct.ABS_POS = %d.", liPosition);
			}

			switch (structL6472.ucStatus)
			{
				case L6472_SetGetCommand :
					// a parameter has been set -> notify reply to supervisor and back to idle
					structL6472.ucStatus = L6472_Idle;
				break;
			}
		break;

		case 0x80:
			// this code can be associated to different commands according to the status
			if ( structL6472.ucStatus == L6472_GoUntilHome )
			{
				cRes = checkBUSYValWithTimeout(NSHM_MOVEMENT_TIMEOUT); // command Move sent: wait for the end
				if ( !cRes )
				{
					cRes = releaseSW();
				}
			}
			else if ( structL6472.ucStatus == L6472_ReleaseSWHome )
			{
				cRes = checkBUSYValWithTimeout(NSHM_MOVEMENT_TIMEOUT); // command Move sent: wait for the end
				if ( !cRes )
				{
					cRes = resetPos();
				}
			}
		break;

		case 0x40:
			// this code can be associated to different commands according to the status
			if ( structL6472.ucStatus == L6472_Move )
			{
				cRes = checkBUSYValWithTimeout(NSHM_MOVEMENT_TIMEOUT);	// command Move sent: wait for the end
				if ( !cRes )
				{
					structL6472.ucStatus = L6472_Idle;
					m_pLogger->log(LOG_DEBUG_PARANOIC, "L6472Base::processL6472Reply: Motion ended with success.");
					cRes = getL6472Param(eGetAbsPos);
				}
			}
		break;

		default :
			switch (m_rgcTxBuf[0])		// commands identified by the single byte
			{
				case dSPIN_HARD_STOP :
					//no action needed, error already notified
				break;

				case dSPIN_GET_STATUS :
					structL6472.usMotorStatus.usiData  = ( m_rgcRxBuf[0] << 8 );	// MSB
					structL6472.usMotorStatus.usiData |=   m_rgcRxBuf[1];			// LSB
					cRes = processL6472Status();
				break;

				case dSPIN_GO_TO:
					cRes = checkBUSYValWithTimeout(NSHM_MOVEMENT_TIMEOUT);	// command Move sent: wait for the end
					if ( !cRes )
					{
						structL6472.ucStatus = L6472_Idle;
						m_pLogger->log(LOG_DEBUG_PARANOIC, "L6472Base::processL6472Reply: Motion ended with success.");
						cRes = getL6472Param(eGetAbsPos);
					}
				break;

				case dSPIN_RESET_DEVICE :
					structL6472.ucStatus = L6472_Idle;
					m_pLogger->log(LOG_DEBUG, "L6472Base::processL6472Reply: SPI operation of motor driver is completed.");
					break;

				case dSPIN_RESET_POS :
					// this is the ResetPos command
					structL6472.ucStatus = L6472_Idle;
					m_pLogger->log(LOG_DEBUG_PARANOIC, "L6472Base::processL6472Reply: Motor at home.");

					// get position to update local variable
					cRes = getL6472Param(eGetAbsPos);
				break;
			}
		break;
	}

	return cRes;
}


bool L6472Base::initRegisters(uint8_t ucReg)
{

	int8_t cRes;
	int32_t liParamToBeChecked = 0;

	switch ( ucReg )
	{
		case 0:
			cRes = sendL6472Cmd(eCmdSetHiZ, 0);
		break;

		case 1:
			cRes = sendL6472Cmd(eSetStepMode, dSPINRegsStruct.STEP_MODE);
		break;

		case 2:
			cRes = sendL6472Cmd(eSetAcc,dSPINRegsStruct.ACC);
		break;

		case 3:
			cRes = sendL6472Cmd(eSetDec, dSPINRegsStruct.DEC);
		break;

		case 4:
			cRes = sendL6472Cmd(eSetMaxSpeed, dSPINRegsStruct.MAX_SPEED);
		break;

		case 5:
			cRes = sendL6472Cmd(eSetMinSpeed, dSPINRegsStruct.MIN_SPEED);
		break;

		case 6:
			cRes = sendL6472Cmd(eSetFullStepSpeed, dSPINRegsStruct.FS_SPD);
		break;

		case 7:
			cRes = sendL6472Cmd(eSetTValHold, dSPINRegsStruct.TVAL_HOLD);
			m_bIsMotorEnabled = true;
		break;

		case 8:
			cRes = sendL6472Cmd(eSetTValAcc, dSPINRegsStruct.TVAL_ACC);
		break;

		case 9:
			cRes = sendL6472Cmd(eSetTValDec, dSPINRegsStruct.TVAL_DEC);
		break;

		case 10:
			cRes = sendL6472Cmd(eSetTFast, dSPINRegsStruct.T_FAST);
		break;

		case 11:
			cRes = sendL6472Cmd(eSetOnMinTime, dSPINRegsStruct.TON_MIN);
		break;

		case 12:
			cRes = sendL6472Cmd(eSetOffMinTime, dSPINRegsStruct.TOFF_MIN);
		break;

		case 13:
			cRes = sendL6472Cmd(eSetODCThreshold, dSPINRegsStruct.OCD_TH);
		break;

		case 14:
			cRes = sendL6472Cmd(eSetAbsPos, dSPINRegsStruct.ABS_POS);
		break;

		case 15:
			cRes = sendL6472Cmd(eSetElPos, dSPINRegsStruct.EL_POS);
		break;

		case 16:
			cRes = sendL6472Cmd(eSetAlarmEnable, dSPINRegsStruct.ALARM_EN);
		break;

		case 17:
			cRes = sendL6472Cmd(eSetConfig, dSPINRegsStruct.CONFIG);
		break;

		case 18:
			cRes = getL6472Param(eGetStepMode);
			liParamToBeChecked = m_rgcRxBuf[0];
			if ( dSPINRegsStruct.STEP_MODE != liParamToBeChecked )			return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: Step Mode: %d.", liParamToBeChecked);
		break;

		case 19:
			cRes = getL6472Param(eGetAcc);
			liParamToBeChecked = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];
			if ( dSPINRegsStruct.ACC != liParamToBeChecked )				return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: Acceleration: %d.", liParamToBeChecked);
		break;

		case 20:
			cRes = getL6472Param(eGetDec);
			liParamToBeChecked = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];
			if ( dSPINRegsStruct.DEC != liParamToBeChecked )				return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: Deceleration: %d.", liParamToBeChecked);
		break;

		case 21:
			cRes = getL6472Param(eGetMaxSpeed);
			liParamToBeChecked = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];
			if ( dSPINRegsStruct.MAX_SPEED != liParamToBeChecked )			return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: Max speed: %d.", dSPINRegsStruct.MAX_SPEED);
		break;

		case 22:
			cRes = getL6472Param(eGetMinSpeed);
			liParamToBeChecked = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];
			if ( dSPINRegsStruct.MIN_SPEED != liParamToBeChecked )			return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: Min speed: %d.", liParamToBeChecked);
		break;

		case 23:
			cRes = getL6472Param(eGetFullStepSpeed);
			liParamToBeChecked = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];
			if ( dSPINRegsStruct.FS_SPD != liParamToBeChecked )				return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: Full step speed: %d.", liParamToBeChecked);
		break;

		case 24:
			cRes = getL6472Param(eGetTValHold);
			liParamToBeChecked = m_rgcRxBuf[0];
			if ( dSPINRegsStruct.TVAL_HOLD != liParamToBeChecked )			return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: TVal hold: %d.", liParamToBeChecked);
		break;

		case 25:
			cRes = getL6472Param(eGetTValAcc);
			liParamToBeChecked = m_rgcRxBuf[0];
			if ( dSPINRegsStruct.TVAL_ACC != liParamToBeChecked )			return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: TVal acc: %d.", liParamToBeChecked);
		break;

		case 26:
			cRes = getL6472Param(eGetTValDec);
			liParamToBeChecked = m_rgcRxBuf[0];
			if ( dSPINRegsStruct.TVAL_DEC != liParamToBeChecked )			return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: TVal dec: %d.", liParamToBeChecked);
		break;

		case 27:
			cRes = getL6472Param(eGetTFast);
			liParamToBeChecked = m_rgcRxBuf[0];
			if ( dSPINRegsStruct.T_FAST != liParamToBeChecked )				return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: TFast: %d.", liParamToBeChecked);
		break;

		case 28:
			cRes = getL6472Param(eGetOnMinTime);
			liParamToBeChecked = m_rgcRxBuf[0];
			if ( dSPINRegsStruct.TON_MIN != liParamToBeChecked )			return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: Min time on: %d.", liParamToBeChecked);
		break;

		case 29:
			cRes = getL6472Param(eGetOffMinTime);
			liParamToBeChecked = m_rgcRxBuf[0];
			if ( dSPINRegsStruct.TOFF_MIN != liParamToBeChecked )			return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: Min time off: %d.", liParamToBeChecked);
		break;

		case 30:
			cRes = getL6472Param(eGetODCThreshold);
			liParamToBeChecked = m_rgcRxBuf[0];
			if ( dSPINRegsStruct.OCD_TH != liParamToBeChecked )				return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: ODC threshold: %d.", liParamToBeChecked);
		break;

		case 31:
			cRes = getL6472Param(eGetAlarmEnable);
			liParamToBeChecked = m_rgcRxBuf[0];
			if ( dSPINRegsStruct.ALARM_EN != liParamToBeChecked )			return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: Alarm enable: %d.", liParamToBeChecked);
                break;

		case 32:
			cRes = getL6472Param(eGetConfig);
			liParamToBeChecked = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];
			if ( dSPINRegsStruct.CONFIG != liParamToBeChecked )			return false;
			m_pLogger->log(LOG_DEBUG, "L6472Base::initRegisters: IC configuration: %d.", liParamToBeChecked);
		break;

		default:
			structL6472.ucStatus = L6472_Idle;
			m_pLogger->log(LOG_INFO, "L6472Base::initRegisters: initialization completed.");
			cRes = 0;
		break;

	}

	if ( cRes != 0 )	return false;

	return true;
}


int8_t L6472Base::processL6472Status()
{
	if ( !checkL6472Connection() )	return ERR_NSH_MOTOR_HARD_STOPPED_FOR_ERR;

	int8_t cRes = 0;

	switch (structL6472.ucStatus)
	{
		case L6472_SetGetCommand :
			structL6472.ucStatus = L6472_Idle;
		break;

		case L6472_StartHome:
			// acquisition of the sensor at beginning of home procedure
			cRes = ( isSensorAtHome() == true) ? releaseSW() : goUntil();
		break;

		case L6472_GoUntilHome:
			// waiting for the motor movement to complete
			cRes = ( isIdle() == true) ? releaseSW() : checkBUSYValWithTimeout(NSHM_MOVEMENT_TIMEOUT);
		break;

		case L6472_ReleaseSWHome:
			cRes = ( isIdle() == true) ? resetPos() : checkBUSYValWithTimeout(NSHM_MOVEMENT_TIMEOUT);
		break;

		case L6472_Move:
		case L6472_GoTo:
			if ( isIdle() )
			{
				structL6472.ucStatus = L6472_Idle;
				m_pLogger->log(LOG_DEBUG_PARANOIC, "L6472Base::processL6472Status: motor stopped its motion. Movement completed.");
				// get position to update local variable
				cRes = getL6472Param(eGetAbsPos);
			}
			else
				cRes = checkBUSYValWithTimeout(NSHM_MOVEMENT_TIMEOUT);
		break;
	}
	return cRes;
}


bool L6472Base::checkL6472Connection()
{
	if ( (structL6472.usMotorStatus.usiData == 0xFFFF) || (structL6472.usMotorStatus.usiData == NSHM_NOP) )
	{
		structL6472.ucStatus = L6472_Idle;

		sendL6472Cmd(eCmdHardStop, 0);

		m_pLogger->log(LOG_ERR, "L6472Base::checkL6472Connection: Error %d\n", structL6472.usMotorStatus.usiData);

		return false;
	}

	return true;
}


int8_t L6472Base::releaseSW()
{
	uint32_t uliParams;

	structL6472.ucStatus = L6472_ReleaseSWHome;

	uliParams = structL6472.ucBackDirection;			// motor direction

	return sendL6472Cmd(eCmdReleaseSw, uliParams);
}


int8_t L6472Base::goUntil()
{
	uint32_t uliParams;
	uint32_t uliSpeed;

	uliSpeed  = Speed_Steps_to_Par(1450); //NSHM_HOME_FAST_SPEED

	structL6472.ucStatus = L6472_GoUntilHome;

	uliParams = structL6472.ucForwardDirection;	// motor direction
	uliParams |= (uliSpeed << 8);				// motor speed

	return sendL6472Cmd(eCmdGoUntil, uliParams);
}


bool L6472Base::isIdle()
{
	if ( structL6472.usMotorStatus.structStatus.cBusy )
	{
		return true;
	}

	return false;
}

