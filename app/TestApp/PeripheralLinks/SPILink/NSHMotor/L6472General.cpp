/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    L6472General.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the L6472General class.
 @details

 ****************************************************************************
*/

#include "L6472General.h"

#define MOTOR_MIN_CURR_VAL					31.25f // mA
#define MOTOR_MAX_CURR_VAL					4000.0f // mA
#define ACC_DEFAULT_VAL						0
#define NSH_STEP_TO_UM_CONVERSION_FACTOR	50 	// 0.05 per half step
#define ABS_POS_BITS_NUM					21


L6472General::L6472General()
{
	m_intResolution = 0;
	m_intHighCurrent = 0;
	m_intLowCurrent = 0;
	m_intPosition = 0;
}

L6472General::~L6472General()
{
	setMotorCurrent(eSetTValHold, 31.25f);
	stopMotor();	
}

bool L6472General::initL6472(Log* pLogger, SPIInterface* pSPIIfr, GpioInterface* pGpio, uint8_t ucBusyGpioIdx, uint8_t ucFaultGpioIdx)
{
	m_bConfigOK = false;

	if ( pLogger == NULL )			return m_bConfigOK;
	if ( pSPIIfr == NULL )			return m_bConfigOK;
	if ( pGpio == NULL )			return m_bConfigOK;

	m_pLogger = pLogger;
	m_pSPIIfr = pSPIIfr;
	m_pGpio = pGpio;
	m_nBusyGpioIdx = ucBusyGpioIdx;
	m_nFaultGpioIdx = ucFaultGpioIdx;

	if ( !initdSPINRegsStruct() )	return m_bConfigOK;

	m_bConfigOK = true;

	return m_bConfigOK;
}


int16_t L6472General::getMotorStatus(void)
{
	if ( !m_bConfigOK )		return -3;

	structL6472.ucStatus = L6472_SetGetCommand;

	int8_t cRes = sendL6472Cmd(eCmdGetStatus, 0);

	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: High impedence: %d.", structL6472.usMotorStatus.structStatus.cHiZ);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Busy line val (active low): %d.", structL6472.usMotorStatus.structStatus.cBusy);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Switch input status (low=open): %d.", structL6472.usMotorStatus.structStatus.cSwitchStatus);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Switch turn-on event: %d.", structL6472.usMotorStatus.structStatus.cSwitchEvent);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Direction (0 reverse, 1 forward): %d.", structL6472.usMotorStatus.structStatus.cDir);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Motor status (0 stopped, 1 acc, 2 dec, 3 constant speed): %d.", structL6472.usMotorStatus.structStatus.cMotorStatus);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Not performed command: %d.", structL6472.usMotorStatus.structStatus.cNotPerformedCmd);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Wrong command: %d.", structL6472.usMotorStatus.structStatus.cWrongCmd);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Undervoltage lockout (active low): %d.", structL6472.usMotorStatus.structStatus.cUnderVoltageLockOut);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Thermal warning (active low): %d.", structL6472.usMotorStatus.structStatus.cThermalWarning);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Thermal shutdown (active low): %d.", structL6472.usMotorStatus.structStatus.cThermalShutDown);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Overcurrent (active low): %d.", structL6472.usMotorStatus.structStatus.cOverCurrThresh);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::getMotorStatus: Step clock mode: %d.", structL6472.usMotorStatus.structStatus.cStepClkMode);

	return cRes;
}


bool L6472General::stopMotor()
{
	if ( !m_bConfigOK )		return false;

	structL6472.ucStatus = L6472_Idle;

	sendL6472Cmd(eCmdHardStop, 0);

	m_pLogger->log(LOG_INFO, "NSHMotorGeneral::stopMotor: motor hard stopped.");

	return true;
}

bool L6472General::setHiZ()
{
    if ( !m_bConfigOK )		return false;

    structL6472.ucStatus = L6472_Idle;

    sendL6472Cmd(eCmdSetHiZ, 0);

    m_pLogger->log(LOG_INFO, "NSHMotorGeneral::setHiZ: motor set to HiZ");

    return true;
}


bool L6472General::isSensorAtHome()
{
	getMotorStatus();

	if ( structL6472.usMotorStatus.structStatus.cSwitchStatus )
	{
		return true;
	}

	return false;
}


int8_t L6472General::moveRelativeSteps(enumMotorDirection eDir, uint32_t uliMicroStepsNum)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	uint32_t uliParams;

	structL6472.ucStatus = L6472_Move;

	// clean the MSB
	uliParams = ((uliMicroStepsNum & 0x00FFFFFF) << 8);

	if ( eDir == eForwardDir )
	{
		uliParams |= BACK;//FORWARD; NSH motor moves opposite
		structL6472.ucBackDirection = BACK;
	}
	else
	{
		uliParams |= FORWARD;//BACK; NSH motor moves opposite
		structL6472.ucForwardDirection = FORWARD;
	}

	int8_t cRes = sendL6472Cmd(eCmdMove, uliParams);
	return cRes;
}


int8_t L6472General::moveToAbsolutePosition(int liPosition)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_GoTo;

	// Motor phases are not mounted as we want.
	liPosition = -liPosition;
	int8_t cRes = sendL6472Cmd(eCmdGoTo, liPosition);

	return cRes;
}


int8_t L6472General::searchForHome()
{
	/*! Note: in Vidas Modular we have 30 steps (= 3mm) to be completely stopped **/

	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_StartHome;

	int8_t cRes = sendL6472Cmd(eCmdGetStatus, 0);

	return cRes;
}


int8_t L6472General::sendReset()
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_ResetCommand;

	int8_t cRes = sendL6472Cmd(eCmdReset, 0);

	return cRes;
}


int8_t L6472General::resetPos()
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_ResetPos;

	return sendL6472Cmd(eCmdResetPos, 0);
}


int8_t L6472General::setAcceleration(uint16_t usAcc)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	dSPINRegsStruct.ACC = usAcc;

	int8_t cRes = sendL6472Cmd(eSetAcc, dSPINRegsStruct.ACC);

	return cRes;
}


int8_t L6472General::getAcceleration(uint16_t& usAcc)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	int8_t cRes = getL6472Param(eGetAcc);
	usAcc = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];

	return cRes;
}


int8_t L6472General::setDeceleration(uint16_t usDec)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	dSPINRegsStruct.DEC = usDec;

	int8_t cRes = sendL6472Cmd(eSetAcc, dSPINRegsStruct.DEC);

	return cRes;
}


int8_t L6472General::getDeceleration(uint16_t& usDec)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	int8_t cRes = getL6472Param(eGetDec);
	usDec = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];

	return cRes;
}


int8_t L6472General::setMaxSpeed(uint16_t usMaxSpeed)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	dSPINRegsStruct.MAX_SPEED = usMaxSpeed;

	int8_t cRes = sendL6472Cmd(eSetAcc, dSPINRegsStruct.MAX_SPEED);

	return cRes;
}


int8_t L6472General::getMaxSpeed(uint16_t& usMaxSpeed)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	int8_t cRes = getL6472Param(eGetMaxSpeed);
	usMaxSpeed = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];

	return cRes;
}


int8_t L6472General::setMinSpeed(uint16_t usMinSpeed)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	dSPINRegsStruct.MIN_SPEED = usMinSpeed;

	int8_t cRes = sendL6472Cmd(eSetAcc, dSPINRegsStruct.MIN_SPEED);

	return cRes;
}


int8_t L6472General::getMinSpeed(uint16_t& usMinSpeed)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	int8_t cRes = getL6472Param(eGetMinSpeed);
	usMinSpeed = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];

	return cRes;
}


int8_t L6472General::setFullSpeed(uint16_t usFullSpeed)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	dSPINRegsStruct.FS_SPD = usFullSpeed;

	int8_t cRes = sendL6472Cmd(eSetAcc, dSPINRegsStruct.FS_SPD);

	return cRes;
}

int8_t L6472General::setMotorCurrent(enumL6472Commands eCmd, float fCurrentVal)
{
	if ( !m_bConfigOK )									return ERR_NSH_MOTOR_CONFIG_NOK;
	if ( eCmd != eSetTValHold || eCmd != eSetTValRun )	return ERR_NSH_MOTOR_WRONG_PARAM;
	if ( fCurrentVal < MOTOR_MIN_CURR_VAL)				return ERR_NSH_MOTOR_WRONG_PARAM;
	if ( fCurrentVal > MOTOR_MAX_CURR_VAL)				return ERR_NSH_MOTOR_WRONG_PARAM;

	uint8_t ucCurrentVal = Tval_Current_to_Par(fCurrentVal);

	if ( ucCurrentVal == MOTOR_MIN_CURR_VAL )
	{
		m_bIsMotorEnabled = false;
	}

	structL6472.ucStatus = L6472_SetGetCommand;
	// Update structs
	if ( eCmd == eSetTValHold)
	{
		dSPINRegsStruct.TVAL_HOLD = ucCurrentVal;
	}
	else
	{
		dSPINRegsStruct.TVAL_RUN = ucCurrentVal;
	}

	char cComNum = 4;
	char cRes = -1;
	for ( int i = 0; i != cComNum; ++i )
	{
		switch (i)
		{
			case 0:
				cRes = sendL6472Cmd(eCmdHardStop, 0);
			break;

			case 1:
				cRes = sendL6472Cmd(eCmdSetHiZ, 0);
			break;

			case 2:
				cRes = sendL6472Cmd(eCmd, ucCurrentVal);
			break;

			case 3:
				cRes = sendL6472Cmd(eCmdHardStop, 0);	// needed to really activate the curr val
			break;

			default:
			break;
		}

		if ( cRes != 0 )	return false;
	}

	return cRes;
}

int8_t L6472General::setAbsPosition(int liAbsPos)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;

	dSPINRegsStruct.ABS_POS = liAbsPos;

	int8_t cRes = sendL6472Cmd(eSetAbsPos, dSPINRegsStruct.ABS_POS);

	return cRes;
}

int8_t L6472General::getStillMotorCurrent(uint8_t& cCurrentVal)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	int8_t cRes = getL6472Param(eGetTValHold);
	cCurrentVal = m_rgcRxBuf[0];

	// store value
	m_intLowCurrent = cCurrentVal;

	return cRes;
}

int8_t L6472General::getRunningMotorCurrent(uint8_t& cCurrentVal)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	int8_t cRes = getL6472Param(eGetTValRun);
	cCurrentVal = m_rgcRxBuf[0];

	// store value
	m_intHighCurrent = cCurrentVal;

	return cRes;
}

int8_t L6472General::getFullSpeed(uint16_t& usFullSpeed)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	int8_t cRes = getL6472Param(eGetFullStepSpeed);
	usFullSpeed = ( m_rgcRxBuf[0] << 8 ) + m_rgcRxBuf[1];

	return cRes;
}

int8_t L6472General::getAbsPosition(int& liAbsPos)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	int8_t cRes = getL6472Param(eGetAbsPos);

	liAbsPos = ( m_rgcRxBuf[0] << 16 ) + ( m_rgcRxBuf[1] << 8 )
			 + m_rgcRxBuf[2];

	char sign = liAbsPos >> ABS_POS_BITS_NUM;
	if ( sign == 1 )
	{
		liAbsPos -= ( 2 << ABS_POS_BITS_NUM );
	}

	// store value
	m_intPosition = liAbsPos;
	return cRes;
}

int8_t L6472General::getStepResolution(uint8_t& ucStepMode)
{
	if ( !m_bConfigOK )		return ERR_NSH_MOTOR_CONFIG_NOK;

	structL6472.ucStatus = L6472_SetGetCommand;
	int8_t cRes = getL6472Param(eGetStepMode);
	ucStepMode = m_rgcRxBuf[0]; // 0 full-step, 1 half-step, 2 1/4 step, 3 1/8 step, >3 1/16 step

	// store value
	m_intResolution = ucStepMode;

	return cRes;
}

bool L6472General::isMotorEnabled()
{
	return m_bIsMotorEnabled;
}

bool L6472General::isMotorRunningOk()
{
	return ( getFaultGpioVal() == GPIO_VAL_HIGH );
}

int8_t L6472General::readAccelerationFactor()
{
	return ACC_DEFAULT_VAL;	 // not needed for NSH
}

uint32_t L6472General::readConversionFactor()
{
	return NSH_STEP_TO_UM_CONVERSION_FACTOR;
}

int8_t L6472General::readMotorResolution()
{
	return m_intResolution;
}

uint16_t L6472General::readMotorHighCurrent()
{
	return m_intHighCurrent;
}

uint16_t L6472General::readMotorLowCurrent()
{
	return m_intLowCurrent;
}

int32_t L6472General::readMotorPosition()
{
	return m_intPosition;
}
