/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SPILinkBoard.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SPILinkBoard class.
 @details

 ****************************************************************************
*/

#ifndef SPIBOARD_H
#define SPIBOARD_H

#include <linux/spi/spidev.h>

#include "SPIInterfaceCollector.h"
#include "SPILinkCommonInclude.h"
#include "NSHReaderLink.h"
#include "L6472General.h"
#include "LS7366RGeneral.h"

typedef enum
{
	eRelPos,
	eAbsPos,
	eHome,
	eNone
} eNSHMotorPositions;

typedef enum
{
	eErrNone,
	eErrStepLoss,
	eErrFailure
} eErrNSHMotor;

class SPILinkBoard : public Loggable
{
	public:

		/*! **********************************************************************************************************************
		 * @brief SPILinkBoard default constructor.
		 * ***********************************************************************************************************************
		 */
		SPILinkBoard();

		/*! **********************************************************************************************************************
		 * @brief ~SPILinkBoard virtual destructor.
		 * ***********************************************************************************************************************
		 */
		virtual ~SPILinkBoard();

		/*! **********************************************************************************************************************
		 * @brief  createAndInitSPIInterfaces set logger and creates PSI interfaces.
		 * @return 0 in case of success, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t createAndInitSPIInterfaces(void);

		/*! **********************************************************************************************************************
		 * @brief  createAndInitNSHMotor create and init NSH stepper motor driver.
		 * @param  pGpio pointer to GPIO interface to initialize BUSY line.
		 * @return 0 in case of success, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t createAndInitNSHMotor(GpioInterface* pGpio);

		/*! **********************************************************************************************************************
		 * @brief  createAndInitNSHReader create and init NSH reader.
		 * @param  pGpio pointer to GPIO interface to initialize DRDY.
		 * @return 0 in case of success, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t createAndInitNSHReader(GpioInterface* pGpio);


		/*! **********************************************************************************************************************
		 * @brief  createAndInitNSHEncoder create and init NSH encoder for stepper motor.
		 * @return 0 in case of success, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t createAndInitNSHEncoder(void);

		/*! **********************************************************************************************************************
		 * @brief destroyNSHMotor
		 * ***********************************************************************************************************************
		 */
		void destroyNSHMotor();

		/*! **********************************************************************************************************************
		 * @brief destroyNSHReader
		 * ***********************************************************************************************************************
		 */
		void destroyNSHReader();

		/*! **********************************************************************************************************************
		 * @brief destroyNSHEncoder
		 * ***********************************************************************************************************************
		 */
		void destroyNSHEncoder();

		/*! **********************************************************************************************************************
		 * @brief destroySPIInterfaces
		 * ***********************************************************************************************************************
		 */
		void destroySPIInterfaces(void);

		/*! **********************************************************************************************************************
		 * @brief  move move NSH motor, checking if some steps are loss.
		 * @param  ePos can be absolute, relative or home
		 * @param  liHalfSteps half steps to be moved, or abs position where to go (not needed with home ePos)
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool move(eNSHMotorPositions ePos, int liHalfSteps = 0);

		/*! **********************************************************************************************************************
		 * @brief  getMotorState get if the motor has lost steps or not
		 * @return eErrNone or eErrStepLoss
		 * ***********************************************************************************************************************
		 */
		char getMotorState(void);

		/*! **********************************************************************************************************************
		 * @brief isNSHReaderEnabled check if NSHReader is correctly initialized and enabled
		 * @return true if enabled, false if not
		 * ***********************************************************************************************************************
		 */
		bool isNSHReaderEnabled(void);

		/*! **********************************************************************************************************************
		 * @brief isNSHMotorEnabled check if isNSHMotorEnabled is correctly initialized and enabled
		 * @return true if enabled, false if not
		 * ***********************************************************************************************************************
		 */
		bool isNSHMotorEnabled(void);

		/*! **********************************************************************************************************************
		 * @brief isNSHEncoderEnabled check if isNSHEncoderEnabled is correctly initialized and enabled
		 * @return true if enabled, false if not
		 * ***********************************************************************************************************************
		 */
		bool isNSHEncoderEnabled(void);

		/*! **********************************************************************************************************************
		 * @brief isSPIErrEnabled check if isSPIErrEnabled is correctly initialized and enabled
		 * @return true if enabled, false if not
		 * ***********************************************************************************************************************
		 */
		bool isSPIErrEnabled(void);

    public:

        NSHReaderLink			*m_pNSHReader;
        L6472General			*m_pNSHMotor;
        LS7366RGeneral			*m_pNSHEncoder;

        // it is shared between all the 3 previous classes
//        SPIErrors				*m_pSPIErr;

    private:

        SPIInterfaceCollector	m_SPIIfrCollector;
        char					m_cMotorErr;
        bool					m_bIsNSHReaderEnabled;
        bool					m_bIsNSHEncoderEnabled;
        bool					m_bIsNSHMotorEnabled;
        bool					m_bIsSPIErrEnabled;

};

#endif // SPIBOARD_H
