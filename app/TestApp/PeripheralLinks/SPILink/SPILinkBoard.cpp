/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SPILinkBoard.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SPILinkBoard class.
 @details

 ****************************************************************************
*/

#include "SPILinkBoard.h"


#define ENC_TO_MOT_COUNT_RATIO					( MOT_RIS_MICROSTEP /	ENC_MULTIPLIER )
#define MAX_DISCREPANCY_ADMITTED_HALF_STEPS_W	1.0f //(2*MOTOR_MICROSTEP/ENCODER_MULTIPLIER)
#define MAX_DISCREPANCY_ADMITTED_HALF_STEPS_A	2.0f
#define NSH_MOTOR_HOME_NOT_FOUND_ERR			799


SPILinkBoard::SPILinkBoard()
{
	m_pNSHReader = nullptr;
	m_pNSHMotor = nullptr;
	m_pNSHEncoder = nullptr;
//    m_pSPIErr = nullptr;
	m_cMotorErr = eErrNone;

	m_bIsNSHReaderEnabled = false;
	m_bIsNSHEncoderEnabled = false;
	m_bIsNSHMotorEnabled = false;
	m_bIsSPIErrEnabled = false;
}


SPILinkBoard::~SPILinkBoard()
{
//    SAFE_DELETE(m_pSPIErr);
	SAFE_DELETE(m_pNSHReader);
	SAFE_DELETE(m_pNSHMotor);
	SAFE_DELETE(m_pNSHEncoder);
}

int8_t SPILinkBoard::createAndInitSPIInterfaces(void)
{
	m_SPIIfrCollector.setLogger(getLogger());
	int8_t cRet = m_SPIIfrCollector.createAndInitInterfaces(CONF_FILE);

//    SAFE_DELETE(m_pSPIErr);
//    m_pSPIErr = new SPIErrors();
//    if ( m_pSPIErr == nullptr )
//    {
//        log(LOG_ERR, "SPILinkBoard::createAndInitSPIInterfaces: memory error");
//    }

    // Init NSH errors handling
//    m_bIsSPIErrEnabled = false;
//    int liRes = m_pSPIErr->initSPIErr(m_pLogger);
//    if ( liRes )
//    {
//        string strFileName(NSH_ERROR_FILE_NAME);
//        log(LOG_ERR, "SPILinkBoard::createAndInitSPIInterfaces: unable to open %s", strFileName.c_str());
//    }
//    m_bIsSPIErrEnabled = true;

	return cRet;
}

void SPILinkBoard::destroySPIInterfaces()
{
	m_SPIIfrCollector.destroyInterfaces();
}

int8_t SPILinkBoard::createAndInitNSHMotor(GpioInterface* pGpio)
{
	m_bIsNSHMotorEnabled = false;

	SAFE_DELETE(m_pNSHMotor);
	m_pNSHMotor = new L6472General();
	if ( m_pNSHMotor == 0 )
	{
		log(LOG_ERR, "SPILinkBoard::createAndInitNSHMotor: unable to create NSH motor, memory error");
		return -1;
	}

	bool bRes = m_pNSHMotor->initL6472(m_pLogger, m_SPIIfrCollector.getInterface(eSPI_2), pGpio,
									   m_SPIIfrCollector.m_NSHMotorBusyGpioIdx, m_SPIIfrCollector.m_NSHMotorFaultGpioIdx);
	if ( ! bRes )		return -1;

	m_bIsNSHMotorEnabled = true;
	return 0;
}

int8_t SPILinkBoard::createAndInitNSHReader(GpioInterface* pGpio)
{
	m_bIsNSHReaderEnabled = false;

	SAFE_DELETE(m_pNSHReader);
	m_pNSHReader = new NSHReaderLink();
	if ( m_pNSHReader == 0 )
	{
		log(LOG_ERR, "SPILinkBoard::createAndInitNSHReader: unable to create NSH reader, memory error");
		return -1;
	}

	bool bRet = m_pNSHReader->init(m_pLogger, m_SPIIfrCollector.getInterface(eSPI_1), pGpio,
								   m_SPIIfrCollector.m_NSHReaderDRDYGpioIdx, m_SPIIfrCollector.m_NSHReaderResetGpioIdx);
	if ( !bRet )
	{
		return -1;
	}

//	if ( m_pSPIErr == nullptr )	return -1;

//	bRet = m_pNSHReader->m_NSH.setNSHErrors(m_pSPIErr);
//	if ( !bRet )
//	{
//		return -1;
//	}
//	m_bIsNSHReaderEnabled = true;

	return 0;
}

int8_t SPILinkBoard::createAndInitNSHEncoder(void)
{
	m_bIsNSHEncoderEnabled = false;

	SAFE_DELETE(m_pNSHEncoder);
	m_pNSHEncoder = new LS7366RGeneral();
	if ( m_pNSHEncoder == 0 )
	{
		log(LOG_ERR, "SPILinkBoard::createAndInitNSHEncoder: unable to create NSH encoder, memory error");
		return -1;
	}

	bool bRet = m_pNSHEncoder->init(m_pLogger, m_SPIIfrCollector.getInterface(eSPI_3));
	if ( !bRet )
	{
		return -1;
	}
	m_bIsNSHEncoderEnabled = true;

	return 0;
}

void SPILinkBoard::destroyNSHEncoder()
{
	SAFE_DELETE(m_pNSHEncoder);
}

void SPILinkBoard::destroyNSHMotor()
{
	SAFE_DELETE(m_pNSHMotor);
}

void SPILinkBoard::destroyNSHReader()
{
	SAFE_DELETE(m_pNSHReader);
}

bool SPILinkBoard::move(eNSHMotorPositions ePos, int liHalfSteps)
{
	if ( m_pNSHMotor == nullptr || m_pNSHEncoder == nullptr )
	{
		return false;
	}

	int8_t cRes = -1;
//	bool bRes = false;
	m_cMotorErr = eErrNone;
	int liPosReadFromDriver = 0;
	int liPosReadFromEncoder = 0;
	int liMicroSteps = liHalfSteps * MOT_HALF_TO_MICRO_STEPS;

	switch (ePos)
	{
		case eHome:
			m_pNSHEncoder->readCounter(liPosReadFromEncoder);
			cRes = m_pNSHMotor->searchForHome();
			if ( cRes )
			{
				int liNewPos = 0;
				m_pNSHEncoder->readCounter(liNewPos);

				if ( liNewPos - liPosReadFromEncoder == 0 ) // motor driver broken
				{
//					m_pSPIErr->decodeNotifyEvent((-ERR_NSH_MOTOR_HOME_NOT_FOUND_NO_MOTION));
				}
				else // home sensor broken
				{
//					m_pSPIErr->decodeNotifyEvent((-ERR_NSH_MOTOR_HOME_NOT_FOUND_MOTION_DET));
				}
				return false;
			}

			m_pNSHMotor->resetPos();
			m_pNSHEncoder->resetCounter();

			// The position found is the first ustep outside the home sensor.
			// Move back be inside the home sensor
			move(eRelPos, -10);

			// Check if motor is healthy
//			bRes = m_pNSHMotor->isMotorRunningOk();
//			if ( ! bRes )
//			{
//				m_cMotorErr = eErrFailure;
//				m_pSPIErr->decodeNotifyEvent((-ERR_NSH_MOTOR_FAILURE));
//				return false;
//			}
			return true;
		break;

		case eRelPos:
			cRes = ( liHalfSteps > 0 ) ? m_pNSHMotor->moveRelativeSteps(eForwardDir, abs(liMicroSteps)) :
										 m_pNSHMotor->moveRelativeSteps(eReverseDir, abs(liMicroSteps));
			if ( cRes )
			{
				log(LOG_ERR, "SPILinkBoard::move: unable to move of %d steps", liHalfSteps);
				return false;
			}
		break;

		case eAbsPos:
			cRes = m_pNSHMotor->moveToAbsolutePosition(liMicroSteps);
			if ( cRes )
			{
				log(LOG_ERR, "SPILinkBoard::move: unable to move to abs position [%d]", liHalfSteps);
				return false;
			}
		break;

		default:
			log(LOG_ERR, "SPILinkBoard::move: position not coded");
			return false;
		break;
	}

//	bRes = m_pNSHMotor->isMotorRunningOk();
//	if ( ! bRes )
//	{
//		m_cMotorErr = eErrFailure;
//		m_pSPIErr->decodeNotifyEvent((-ERR_NSH_MOTOR_FAILURE));
//		return false;
//	}

	cRes = m_pNSHMotor->getAbsPosition(liPosReadFromDriver);
	if ( cRes )
	{
		log(LOG_ERR, "SPILinkBoard::move: unable to get the abs position from the motor driver");
		return false;
	}

	cRes = m_pNSHEncoder->readCounter(liPosReadFromEncoder);
	if ( cRes )
	{
		log(LOG_ERR, "SPILinkBoard::move: unable to read the position from the encoder");
		return false;
	}

	float fDifference = float(liPosReadFromEncoder);
	fDifference -= ( float(liPosReadFromDriver) / float(ENC_TO_MOT_COUNT_RATIO) );
	fDifference = ( fDifference >= 0.0f ) ? fDifference : -fDifference;
	fDifference /= ENC_COUNTS_TO_HALF_STEPS;

	// Send warning or alarm depending on the number of step lost
	if ( fDifference > MAX_DISCREPANCY_ADMITTED_HALF_STEPS_W &&
		 fDifference <= MAX_DISCREPANCY_ADMITTED_HALF_STEPS_A )
	{
		// We are still in acceptability range
//        m_pSPIErr->decodeNotifyEvent((-1) * (ERR_NSH_MOTOR_STEP_LOSS_W));
	}
	else if ( fDifference  > MAX_DISCREPANCY_ADMITTED_HALF_STEPS_A )
	{
		m_cMotorErr = eErrStepLoss;
//        m_pSPIErr->decodeNotifyEvent((-1) * (ERR_NSH_MOTOR_STEP_LOSS_A));
		log(LOG_ERR, "SPILinkBoard::move: position read from the encoder[%d], position read from the driver[%d], "
					 "difference %0.2f half steps.", -liPosReadFromEncoder, -liPosReadFromDriver, fDifference);
		return false;
	}

	log(LOG_DEBUG, "SPILinkBoard::move: position read from the encoder[%d], position read from the driver[%d], "
				 "difference %0.2f half steps.", -liPosReadFromEncoder, -liPosReadFromDriver, fDifference);


	// Align motor with encoder
    cRes = m_pNSHMotor->setAbsPosition(liPosReadFromEncoder * ENC_TO_MOT_COUNT_RATIO);
	if ( cRes )
	{
		log(LOG_ERR, "SPILinkBoard::move: unable to get the abs position from the motor driver");
		return -1;
	}

	return true;
}

char SPILinkBoard::getMotorState()
{
	return m_cMotorErr;
}

bool SPILinkBoard::isNSHReaderEnabled()
{
	return m_bIsNSHReaderEnabled;
}

bool SPILinkBoard::isNSHMotorEnabled()
{
	return m_bIsNSHMotorEnabled;
}

bool SPILinkBoard::isNSHEncoderEnabled()
{
	return m_bIsNSHEncoderEnabled;
}

bool SPILinkBoard::isSPIErrEnabled()
{
	return m_bIsSPIErrEnabled;
}



