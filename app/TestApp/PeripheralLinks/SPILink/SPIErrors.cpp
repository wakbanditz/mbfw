/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SPIErrors.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SPIErrors class.
 @details

 ****************************************************************************
*/

#include "SPIErrors.h"
#include "MainExecutor.h"

SPIErrors::SPIErrors()
{
	m_pLogger = nullptr;
	m_vectorErrors.clear();
}

SPIErrors::~SPIErrors()
{
	NSHError * pError;

	for ( int i = m_vectorErrors.size() -1; i >= 0; i-- )
	{
		pError = m_vectorErrors.at(i);
        SAFE_DELETE(pError);
	}

	m_vectorErrors.clear();
}

int SPIErrors::initSPIErr(Log* pLogger)
{
	if ( pLogger == nullptr )	return -1;

	m_pLogger = pLogger;

	int liVectorSize = buildErrorVector();
	if ( liVectorSize == 0  )
	{
		return -1;
	}

	return 0;
}

void SPIErrors::clearAllErrors()
{
	NSHError* pError;

	for ( size_t i = 0; i < m_vectorErrors.size(); i++ )
	{
		pError = m_vectorErrors.at(i);
		pError->setActive(false);
	}
}

int SPIErrors::updateErrorsTime(uint64_t ulCurrentMsec, tm* currentTime)
{
	int counter = 0;

	for ( size_t i = 0; i < m_vectorErrors.size(); i++ )
	{
		// check if the message sent from the board correspond to one stored in the list
		NSHError* pNSHError = m_vectorErrors.at(i);

		if ( pNSHError->getActive() == true && pNSHError->getStrTime().empty() )
		{
			// the error is set but no string time is linked to it
			// calculate the difference between the error msecs and the current one

			uint64_t ulErrorMsec = pNSHError->getIntTime();
			uint64_t ulDiffSecs = 0;

			m_pLogger->log(LOG_INFO, "SPIErrors current %llu : error %llu", ulCurrentMsec, ulErrorMsec);

			if ( ulCurrentMsec > ulErrorMsec )
			{
				ulDiffSecs = ulCurrentMsec - ulErrorMsec;
			}

			float fClkRatio = infoSingleton()->getClockRatio();
			ulDiffSecs = (uint64_t)((float)ulDiffSecs / fClkRatio);

			// secs equivalent to the new time structure
			time_t totalSeconds = mktime(currentTime);
			time_t diffSecToSet = (totalSeconds - (time_t)ulDiffSecs);

			m_pLogger->log(LOG_INFO, "SPIErrors %llu %llu %llu", totalSeconds, ulDiffSecs, diffSecToSet);

			struct tm* setTime = gmtime(&diffSecToSet);

			char bufferTmp[64];
			sprintf(bufferTmp, "%04d%02d%02d%02d%02d%02d",
				setTime->tm_year + 1900, setTime->tm_mon + 1, setTime->tm_mday,
				setTime->tm_hour, setTime->tm_min, setTime->tm_sec);

			string strTime;
			strTime.assign(bufferTmp);
			pNSHError->setStrTime(strTime);

			m_pLogger->log(LOG_INFO, "SPIErrors: %s %s",
						   pNSHError->getStrTime().c_str(),
						   pNSHError->getDescription().c_str());
			counter++;
		}
	}
	return counter;
}

int SPIErrors::getErrorList(vector<string>* pVectorList, uint8_t errorLevel)
{
	// NOTE don't clear the vector but add elements !
	string strError("");
	size_t i;

	for(i = 0; i < m_vectorErrors.size(); i++)
	{
		// check if the error is activated
		NSHError* pNSHError = m_vectorErrors.at(i);

		if ( pNSHError->getActive() == true && pNSHError->getStrTime().empty() == false)
		{
            if(errorLevel == GENERAL_ERROR_LEVEL_NONE ||
                    pNSHError->getLevel() == errorLevel)
            {
                // the string is formatted as timestamp#code#category#description
                strError.assign(pNSHError->getStrTime());
                strError.append(HASHTAG_SEPARATOR);
                strError.append(pNSHError->getCode());
                strError.append(HASHTAG_SEPARATOR);
                strError.append(pNSHError->getCategory());
                strError.append(HASHTAG_SEPARATOR);
                strError.append(pNSHError->getDescription());

                pVectorList->push_back(strError);
            }
		}
	}

	return pVectorList->size();
}

int SPIErrors::buildErrorVector()
{
	// build the errors list
	string strDevice("NSH");
	m_vectorErrors.clear();
	int ret = parseErrorFile(strDevice, (vector<GeneralError*>*)&m_vectorErrors, eErrorNsh);

	return ret;
}

bool SPIErrors::checkErrors()
{
    bool bRes = false;

    for ( size_t i = 0; i < m_vectorErrors.size(); i++ )
    {
        NSHError* pErr = m_vectorErrors.at(i);

        // if the error is enabled, and active -> check level
        if ( (pErr->getEnabled() == true) && (pErr->getActive() == true))
        {
            if (pErr->getLevel() > GENERAL_ERROR_LEVEL_WARNING)
            {
                // change the internal status of the NSH
                changeDeviceStatus(eIdNsh, eStatusError);

                // request the MasterBoard to set the Module LED
                requestModuleLed();
            }

            bRes = true;
        }
    }

    if(bRes == false)
    {
        // no critical error has been detected -> set status in IDLE

        // change the internal status of the NSH
        changeDeviceStatus(eIdNsh, eStatusIdle);

        // request the MasterBoard to set the Module LED
        requestModuleLed();
    }

    return bRes;
}

bool SPIErrors::clearError(int liEventCode)
{
    bool bRes = false;

    for ( size_t i = 0; i < m_vectorErrors.size(); i++ )
    {
        // check if the message sent from the board correspond to one stored in the list
        NSHError* pErr = m_vectorErrors.at(i);

        if ( ! pErr->getFlag().compare(to_string(liEventCode)) )
        {
            // get the info
            string strDescription(pErr->getDescription());

            m_pLogger->log(LOG_INFO, "SPIErrors::clearError: %s - %d - %d - %s - %d",
                           pErr->getCode().c_str(), pErr->getLevel(), pErr->getEnabled(),
                           strDescription.c_str(), pErr->getRestoreEnabled());

            // log the error on Masterboard
            registerErrorEvent(NSH_ID, "NSH", pErr->getCode(), pErr->getLevel(),
                               strDescription, pErr->getEnabled(), pErr->getActive());

            // if the error is enabled, restorable and active -> reset it
            if ( (pErr->getEnabled() == true) && (pErr->getActive() == true) &&
                 (pErr->getRestoreEnabled() == true))
            {
                pErr->setActive(false);

                m_pLogger->log(LOG_INFO, "SPIErrors::clearError -> %s - %d  %s - %d",
                               pErr->getCode().c_str(), pErr->getLevel(), strDescription.c_str(),
                               pErr->getActive());

                if (pErr->getLevel() > GENERAL_ERROR_LEVEL_WARNING)
                {
                    // change the internal status of the NSH
                    changeDeviceStatus(eIdNsh, eStatusError);

                    // request the MasterBoard to set the Module LED
                    requestModuleLed();
                }

                bRes = true;
            }
            break;
        }
    }

    return bRes;
}

bool SPIErrors::decodeNotifyEvent(int liEventCode)
{
	NSHError* pErr = nullptr;
	int liRes = searchEventError(to_string(liEventCode), pErr);

	switch (liRes)
	{
		// the order is important
		case 0:		requestVidasEp();
		case 1:		m_pLogger->log(LOG_INFO, "SPIErrors::decodeNotifyEvent: error Flag[%s] Code[%s] Description[%s]",
								   pErr->getFlag().c_str(), pErr->getCode().c_str(), pErr->getDescription().c_str());
		default:	break;
	}

	return ( liRes != -1 );
}

int SPIErrors::searchEventError(const string& strErrCode, NSHError*& pNSHErrorFound)
{
	int liRes = -1;
	pNSHErrorFound = nullptr;

	for ( size_t i = 0; i < m_vectorErrors.size(); i++ )
	{
		// check if the message sent from the board correspond to one stored in the list
		NSHError* pErr = m_vectorErrors.at(i);

		if ( ! pErr->getFlag().compare(strErrCode) )
		{
			// get the info
			string strDescription(pErr->getDescription());

			m_pLogger->log(LOG_INFO, "SPIErrors::searchEventError: %s - %d - %d - %s - %d",
						   pErr->getCode().c_str(), pErr->getLevel(), pErr->getEnabled(),
						   strDescription.c_str(), pErr->getRestoreEnabled());

			// log the error on Masterboard
			registerErrorEvent(NSH_ID, "NSH", pErr->getCode(), pErr->getLevel(),
							   strDescription, pErr->getEnabled(), pErr->getActive());

			pNSHErrorFound = pErr;
			liRes = 1;

			// if the error is enabled (and not yet active)
			// or don't care aboute the status (pressure error can be multiples) -> we activate it
			if ( ((pNSHErrorFound->getEnabled() == true) && (pNSHErrorFound->getActive() == true)) ||
				  (pNSHErrorFound->getActive() == false))
			{
				// read from the database if the time has been set by Gateway
				pErr->setActive(true, infoSingleton()->getSetTime());

				m_pLogger->log(LOG_INFO, "SPIErrors::searchEventError: %s - %d  %s - %ld",
							   pErr->getCode().c_str(), pErr->getLevel(), strDescription.c_str(),
							   pErr->getIntTime());

                if (pErr->getLevel() > GENERAL_ERROR_LEVEL_WARNING)
                {
                    // change the internal status of the NSH
                    changeDeviceStatus(eIdNsh, eStatusError);

                    // request the MasterBoard to set the Module LED
                    requestModuleLed();
                }

				liRes = 0;
			}
			break;
		}
	}

	return liRes;
}
