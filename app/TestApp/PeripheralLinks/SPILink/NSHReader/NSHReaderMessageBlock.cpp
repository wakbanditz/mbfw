/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderMessageBlock.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the NSHReaderMessageBlock class.
 @details

 ****************************************************************************
*/

#include "NSHReaderMessageBlock.h"

NSHReaderMessageBlock::NSHReaderMessageBlock()
{
	memset(&m_NSHMsgStructTx, 0x00, sizeof(m_NSHMsgStructTx));
	memset(&m_NSHMsgStructRx, 0x00, sizeof(m_NSHMsgStructRx));
	m_strBinMsgs.clear();
	m_siPktNum = 0;
}


NSHReaderMessageBlock::~NSHReaderMessageBlock()
{

}


int8_t NSHReaderMessageBlock::setLog(Log* pLogger)
{
	if ( pLogger == NULL )	return -1;
	m_pLogger = pLogger;
	return 0;
}


int8_t NSHReaderMessageBlock::buff2MsgBlocks(const char* pcBuff, uint16_t usBuffLength, eSPIProtoType typeProt)
{
	if ( pcBuff == NULL )	return -1;

	int8_t result;
	const char *pcMsg;
	pcMsg = pcBuff;

	if ( typeProt == eSPIAscii )
	{
		const char *pcHeader;

		memset(&m_NSHMsgStructTx, 0x00, sizeof(m_NSHMsgStructTx));
		m_siPktNum = 0;
		uint16_t idHeader = 0;

		// Fill command block
		m_NSHMsgStructTx.ucCmdBlock[0] = eSPIFirstPktID;

		/*! ****************************************************************************************
		 * Splits the buffer into multiple packets 3 characters long in order to be compliant with
		 *	the protocol specification.
		 ******************************************************************************************/
		int16_t sMsgLeftLength = usBuffLength;

		while(sMsgLeftLength > 0)
		{
			m_siPktNum++;
			uint32_t uliPktLen = sMsgLeftLength;

			if ( uliPktLen > NSH_READER_SPI_PKT_DATA_LEN )
			{
				uliPktLen = NSH_READER_SPI_PKT_DATA_LEN;
			}

			if ( m_siPktNum == 1 )
			{
				m_NSHMsgStructTx.ucCmdBlock[1] = *(pcMsg++);
				m_NSHMsgStructTx.ucCmdBlock[2] = *(pcMsg++);
				m_NSHMsgStructTx.ucCmdBlock[3] = *(pcMsg++);
			}
			else
			{
				idHeader++;
				for (uint8_t i = 0; i < uliPktLen; i++)
				{
					m_NSHMsgStructTx.ucPayloadBlock[idHeader+i] = *(pcMsg++);
				}
				idHeader += uliPktLen;
			}

			if ( uliPktLen < NSH_READER_SPI_PKT_DATA_LEN )
			{
				/* Fill with SPIH_PAD_CHAR the remaining of the current payload */
				for (uint8_t i = 0; i < NSH_READER_SPI_PKT_DATA_LEN-uliPktLen; i++)
				{
					m_NSHMsgStructTx.ucPayloadBlock[idHeader+i] = eSPICmdPAD;
				}
			}
			sMsgLeftLength -= NSH_READER_SPI_PKT_DATA_LEN;
		}

		/*! ****************************************************************************************
		 * Insert the sequence character into the header of each packet.
		 ******************************************************************************************/
		if ( m_siPktNum > (NSH_READER_SPI_PKT_MAX_NUM + NSH_READER_SPI_PKT_HEAD_NUM) )
		{
			m_pLogger->log(LOG_ERR, "NSHReaderMessageBlock::buff2MsgBlocks: NumPackets: %d > MaxNumAllowed(%d)", m_siPktNum, NSH_READER_SPI_PKT_MAX_NUM + NSH_READER_SPI_PKT_HEAD_NUM);
			return -1;
		}
		else if ( m_siPktNum < NSH_READER_SPI_PKT_HEAD_NUM )
		{
			m_pLogger->log(LOG_ERR, "NSHReaderMessageBlock::buff2MsgBlocks: NoPackets to be sent");
			return -1;
		}
		else
		{
			m_siPktNum--; //remove header packet
			m_NSHMsgStructTx.ucPayloadPacketNum = m_siPktNum;
			pcHeader = &cLeftPktId[m_siPktNum - 1];  // points to the highest ID to insert

			for (int i = 0; i < m_siPktNum; i++)
			{
				// Insert the correct header - # has already been inserted
				m_NSHMsgStructTx.ucPayloadBlock[i * NSH_READER_SPI_PKT_SIZE] = *pcHeader;
				pcHeader--;                                // points to the previous ID
			}
		}

		/*! ****************************************************************************************
		 * Computes the checksum and fill che checksum field of m_SpiMsgStruct.
		 ******************************************************************************************/
		uint8_t rgcCheckSum[NSH_READER_SPI_PKT_SIZE] = {0};
		result = computeChecksum(&m_NSHMsgStructTx, rgcCheckSum);

		memcpy(m_NSHMsgStructTx.ucChecksumBlock, rgcCheckSum, NSH_READER_SPI_PKT_SIZE);
	}

	else if ( typeProt == eSPIbin )
	{
		if ( m_strBinMsgs.max_size() < strlen((char*)pcMsg) )	return -1;

		for (uint32_t i = 0; i < strlen((char*)pcMsg); i++ )
		{
			 m_strBinMsgs.push_back(pcBuff[i]);
		}
		result = 0;
	}

	return result;
}


int8_t NSHReaderMessageBlock::computeChecksum(structSPImsgBlock* pSPIMsgStruct, uint8_t* pCheckSumBuf)
{
	uint8_t cChk[NSH_READER_SPI_PKT_DATA_LEN]; // loop counters

	/*!* consider header for checksum computation ***/
	cChk[0] = pSPIMsgStruct->ucCmdBlock[1];
	cChk[1] = pSPIMsgStruct->ucCmdBlock[2];
	cChk[2] = pSPIMsgStruct->ucCmdBlock[3];

	/*!* consider payload for checksum computation ***/
	for ( uint8_t i = 0; i < pSPIMsgStruct->ucPayloadPacketNum; ++i )
	{
		for ( uint8_t m = 0; m < NSH_READER_SPI_PKT_DATA_LEN; ++m)
		{
			 cChk[m] += pSPIMsgStruct->ucPayloadBlock[i*NSH_READER_SPI_PKT_SIZE + NSH_READER_SPI_PKT_DATA_IDX + m];
		}
	}

	pCheckSumBuf[0] = eSPIChkPktId;

	for ( uint8_t i = 0; i < NSH_READER_SPI_PKT_DATA_LEN; ++i )
	{
		pCheckSumBuf[i+1] = ( ~cChk[i] );
	}

	return 0;
}


int8_t NSHReaderMessageBlock::msgBlocks2Buff(uint8_t *pcBuff, uint32_t &uliBuffLen)
{
	uint8_t usPktNum = m_NSHMsgStructRx.ucPayloadBlock[0] + NSH_READER_SPI_PKT_HEAD_NUM - 0x40;
	uint16_t usPayloadSize = ( usPktNum - 1 ) * NSH_READER_SPI_PKT_DATA_LEN;

	if ( usPayloadSize > NSH_READER_SPI_PKT_MAX_PAYLOAD_SIZE )
	{
		m_pLogger->log(LOG_ERR, "NSHReaderMessageBlock::msgBlocks2Buff: Memory buffer not sufficient: [%d]>[%d].", usPayloadSize, uliBuffLen);
		return -1;
	}

	uliBuffLen = 0;

	/*!* create buff from header ***/
	pcBuff[0] = m_NSHMsgStructRx.ucCmdBlock[1];
	pcBuff[1] = m_NSHMsgStructRx.ucCmdBlock[2];
	pcBuff[2] = m_NSHMsgStructRx.ucCmdBlock[3];

	uliBuffLen += 3;

	/*!* create buf from payload ***/
	for ( int i = 0; i < usPktNum-1; ++i)
	{
		//skips first characters
		for (int m = 0; m < NSH_READER_SPI_PKT_DATA_LEN; m++)
		{
			pcBuff[uliBuffLen] = m_NSHMsgStructRx.ucPayloadBlock[i * NSH_READER_SPI_PKT_SIZE + m + NSH_READER_SPI_PKT_DATA_IDX];
			uliBuffLen++;
		}
	}
	pcBuff[uliBuffLen]='\0';

	return 0;
}


void NSHReaderMessageBlock::reset()
{
	memset(&m_NSHMsgStructRx, 0x00, sizeof(m_NSHMsgStructRx));
	memset(&m_NSHMsgStructTx, 0x00, sizeof(m_NSHMsgStructTx));
	m_strBinMsgs.clear();
	m_siPktNum = 0;
}
