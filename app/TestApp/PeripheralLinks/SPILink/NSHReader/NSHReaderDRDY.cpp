/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderDRDY.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the NSHReaderDRDY class.
 @details

 ****************************************************************************
*/

#include "NSHReaderDRDY.h"

NSHReaderGPIO::NSHReaderGPIO()
{
	m_pLog = NULL;
	m_pGpio = NULL;
	m_bConfigOk = false;
	m_nDRDYGpioIdx = -1;
	m_nResetGpioIdx = -1;
}


NSHReaderGPIO::~NSHReaderGPIO()
{
//	SAFE_DELETE(m_pLog);
}


int8_t NSHReaderGPIO::initDRDY(Log* pLogger, GpioInterface* pGpio, uint8_t ucDRDYGpioIdx, uint8_t ucResetGpioIdx)
{
	m_bConfigOk = false;

	if ( pLogger == NULL )	return -1;
	if ( pGpio == NULL )	return -1;

	m_pLog = pLogger;
	m_pGpio = pGpio;
	m_nDRDYGpioIdx = ucDRDYGpioIdx;
	m_nResetGpioIdx = ucResetGpioIdx;

	m_bConfigOk = true;

	return 0;
}


int8_t NSHReaderGPIO::getDRDYvalue(void)
{
	if ( !m_bConfigOk )
	{
		m_pLog->log(LOG_ERR, "NSHReaderGPIO::getDRDYvalue: Configuration NOK");
		return -1;
	}

	int8_t cGpioValue = m_pGpio->readValue(m_nDRDYGpioIdx);
	switch(cGpioValue)
	{
		case 2:
			m_pLog->log(LOG_ERR, "NSHReaderGPIO::getDRDYvalue: Impossible to read gpio");
			return -1;
		break;
		case GPIO_VAL_LOW:		return GPIO_VAL_LOW;	break;
		case GPIO_VAL_HIGH:		return GPIO_VAL_HIGH;	break;
		default:				return -1;				break;
	}
}


int8_t NSHReaderGPIO::checkDRDYstatusWithTimeout(uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )
	{
		m_pLog->log(LOG_ERR, "NSHReaderGPIO::getDRDYvalue: Configuration NOK");
		return -1;
	}

	usleep(50); // let the DSP set the gpio val
	int8_t cDRDYValue;

	cDRDYValue = getDRDYvalue();

	if ( cDRDYValue < 0 )	return -1;
	else if ( cDRDYValue )	return 0;

	if ( !m_pGpio->waiForIrq(m_nDRDYGpioIdx, uliTimeOutMsec) )
	{
		m_pLog->log(LOG_ERR, "NSHReaderGPIO::monitorDRDYwithTimeout: DSP busy");
		return -1;
	}

	m_pLog->log(LOG_DEBUG_PARANOIC, "NSHReaderGPIO::monitorDRDYwithTimeout: got IRQ");

	return 0;
}

int8_t NSHReaderGPIO::pullLowResetLine(uint32_t uliLowTime)
{
	if ( !m_bConfigOk )
	{
		m_pLog->log(LOG_ERR, "NSHReaderGPIO::getDRDYvalue: Configuration NOK");
		return -1;
	}

	bool bRes = m_pGpio->writeValue(m_nResetGpioIdx, GPIO_VAL_LOW);
	if ( ! bRes )		return -2;
	m_pLog->log(LOG_DEBUG_PARANOIC, "NSHReaderGPIO::pullLowResetLine: gpio[%d] now low", m_nResetGpioIdx);

	msleep(uliLowTime);

	bRes = m_pGpio->writeValue(m_nResetGpioIdx, GPIO_VAL_HIGH);
	if ( ! bRes )		return -2;
	m_pLog->log(LOG_DEBUG_PARANOIC, "NSHReaderGPIO::pullLowResetLine: gpio[%d] high again", m_nResetGpioIdx);

	return 0;
}

int8_t NSHReaderGPIO::checkForHwReset(uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )
	{
		m_pLog->log(LOG_ERR, "NSHReaderGPIO::checkDRDYforHwReset: Configuration NOK");
		return -1;
	}

	if ( !m_pGpio->waiForIrq(m_nDRDYGpioIdx, uliTimeOutMsec) )
	{
		m_pLog->log(LOG_ERR, "NSHReaderGPIO::checkDRDYforHwReset: DSP busy");
		return -1;
	}

	m_pLog->log(LOG_DEBUG_PARANOIC, "NSHReaderGPIO::checkDRDYforHwReset: got bootloader IRQ");

	if ( !m_pGpio->waiForIrq(m_nDRDYGpioIdx, uliTimeOutMsec) )
	{
		m_pLog->log(LOG_ERR, "NSHReaderGPIO::checkDRDYforHwReset: DSP busy");
		return -1;
	}

	m_pLog->log(LOG_DEBUG_PARANOIC, "NSHReaderGPIO::monitorDRDYwithTimeout: got applicative IRQ");
	return 0;
}
