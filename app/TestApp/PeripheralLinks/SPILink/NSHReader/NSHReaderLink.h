/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderLink.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the NSHReaderLink class.
 @details

 ****************************************************************************
*/

#ifndef NSHBOARDLINK_H
#define NSHBOARDLINK_H

#include "NSHReaderGeneral.h"
//#include "NSHReaderFwUpdate.h"

class NSHReaderLink
{
	public:

		/*! *************************************************************************************************
		 * @brief NSHReaderLink default constructor
		 * **************************************************************************************************
		 */
		NSHReaderLink();

		/*! *************************************************************************************************
		 * @brief NSHReaderLink NSHReaderLink destructor
		 * **************************************************************************************************
		 */
		virtual ~NSHReaderLink();

		/*! *************************************************************************************************
		 * @brief  init initialize NSH reader
		 * @param  pLogger pointer to the logger
		 * @param  pSPIIfr pointe to the related SPI interface
		 * @param  pGpio pointer to GPIO interface
		 * @param  ucDRDYGpioIdx idx of the GPIO used for data ready
		 * @param  ucResetGpioIdx idx of the GPIO used for resetting NSH
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool init(Log* pLogger, SPIInterface* pSPIIfr, GpioInterface* pGpio, uint8_t ucDRDYGpioIdx, uint8_t ucResetGpioIdx);

    public:

        NSHReaderGeneral	m_NSH;
//        NSHReaderFwUpdate 	m_NSHFwUpdate;

};

#endif // NSHBOARDLINK_H
