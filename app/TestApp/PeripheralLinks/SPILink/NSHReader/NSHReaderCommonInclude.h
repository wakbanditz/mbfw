/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderCommonInclude.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the NSH classes.
 @details

 ****************************************************************************
*/

#ifndef NSHREADERCOMMONINCLUDE_H
#define NSHREADERCOMMONINCLUDE_H

#include <stdint.h>

#define FACTOR_MULTIPLY		1000.0
#define NSH_DEFAULT_FACTOR	1000


// MSG
#define NSH_READER_SPI_PKT_SIZE					4
#define NSH_READER_SPI_PKT_DATA_LEN				3
#define NSH_READER_SPI_PKT_MAX_NUM				26	// max number of packets data A->Z 26
#define NSH_READER_SPI_PKT_MAX_PAYLOAD_SIZE		( NSH_READER_SPI_PKT_DATA_LEN * ( NSH_READER_SPI_PKT_MAX_NUM-1 ) )
#define NSH_READER_SPI_PKT_HEAD_NUM				1	// number of packet header 1
#define NSH_READER_SPI_PKT_HEAD_IDX				0
#define NSH_READER_SPI_PKT_HEAD_LEN				1
#define NSH_READER_SPI_PKT_DATA_IDX				(NSH_READER_SPI_PKT_HEAD_IDX + NSH_READER_SPI_PKT_HEAD_LEN)
#define NSH_READER_SPI_PKT_A					'A'
#define NSH_READER_SPI_PKT_Z					'Z'



typedef struct
{
	uint8_t ucCmdBlock[NSH_READER_SPI_PKT_SIZE];
	uint8_t ucPayloadBlock[NSH_READER_SPI_PKT_SIZE*NSH_READER_SPI_PKT_MAX_NUM+1];
	uint8_t ucPayloadPacketNum;
	uint8_t ucChecksumBlock[NSH_READER_SPI_PKT_SIZE];
} structSPImsgBlock;


/*! **********************************************************************************************
 *									 Message Block  											 *
 * ********************************************************************************************* */
typedef enum
{
	eSPIFirstPktID		= '#',		// first packet identifier
	eSPICmdPAD			= ' ',		// packet pad character or filling char
	eSPIChkPktId		= '*',		// checksum/last packet identifier
	eSPIErrPktId		= '?',		// error packet identifier
	eSPIDmyPktId		= '!',		// dummy packet identifier
	eSPISinglePktId		= '%',		// single packet identifier - not used
	eSPICmdPADBin		= 0xFF,		// packet pad character
	eSPIErrBinProtId	= '?',		// error with binary protocol
	eSPIErrBinPktId		= '&',		// single packet identifier & and not ?
	eSPIEndBinPktId		= '&',		// end bin pkt packet identifier
	eSPIRaedInProgrId	= '@',		// read in progress
	eSPIAckBinPktId		= '@',		// ack  packet binary
	eSPINextPktCRC		= 0xFFF		// packet is a crc

} eSPIPktId;

typedef enum
{
	eSPIbin,
	eSPIAscii
}eSPIProtoType;


/*! **********************************************************************************************
 *									NSH Reader Protocol											 *
 * ********************************************************************************************* */

typedef enum
{
	eGetDefaultTimeoutMsec		= 1000,		// Note: forse tutti meno ad eccezione del SW reset
	eGetFwVersionTimeoutMsec	= 1000,
	eGetNSHStatusTimeoutMsec	= 3000,
	eGetConvFluoTimeoutMsec		= 1000,
	eGetFluoReadTimeoutMsec		= 1000,
	eGetSwResetTimeoutMsec		= 15000,
	eGetHwResetTimeoutMsec		= 200,
	eGetDRDYLineWighTimeoutMsec = 20000,
	eGetRestoreParamTimeoutMec	= 10000,
	eGetLastFluoValTimeoutMsec	= 1000,
	eGetLastRefValTimeoutMsec	= 1000,
	eGetSHIdTimeoutMsec			= 1000,
	eGetAirReadingTimeoutMsec	= 1500,
	eGetSSReadingTimeoutMsec	= 1000,
	eGetLedEnableTimeoutMsec	= 500,
	eGetSSStoredTimeoutMsec		= 1000,
	eStoreRFUTimeoutMsec		= 4000,
	eAutoCalSSTimeoutMsec		= 2000,
	eCalibrAirTimeoutMsec		= 8000,
	eCalibr3144RFUTimeoutMsec	= 4000,
	eCalibrAndReadLimitsMsec	= 2000,
	eSSCalibrParamsTimeoutMsec	= 1000,
	eFirmwareUpgradeTimeoutMsec	= 1000

} eNSHTimeout;


typedef enum
{
	eGetFirmwareId		= 1,
	eGetNSHStatusId		= 2,
	eGetFluoReadId		= 3,
	eConvFluoReadId		= 4,
	eGetLastFluoValue	= 5,
	eGetLastRefValue	= 6,
	eGetSHId			= 7,
	eGetAirReadingId	= 8,
	eReadSolidStdRFUId	= 9,
	eSetLedId			= 10,
	eGetSolidStdRFUId	= 11,
	eStoreSolidStdRFUId = 12,
	eAutoCalSolidStdId	= 13,
	eCalibAirFactoryId	= 14,
	eLimitId			= 15,
	eFwUpg				= 16,
	eGetCalibParam		= 17,
	eRouteCmdId			= 18,

	eNSHRestoreParamId	= 19,
	eNSHResetId			= 20
} eCmdId;


typedef enum
{
	eNSHBinProtErrCnt			= -5,		// error on msg received with extended protocol
	eNSHProtError				= -2,
	eNSHParamError				= -1,
	//Communication errors
	eNSHProtNoError				= 0x00,		// No error
	eNSHErrStreamLen			= 0x01,		// Wrong stream length
	eNSHErrUnknownCmd			= 0x02,		// Unknown command
	eNSHErrBadParams			= 0x03,		// Bad command parameters
	eNSHErrStrFormat			= 0x04,		// Wrong stream format
	eNSHErrCheckSum				= 0x05,		// Checksum error
	//Reading errors
	eNSHErrADCConvTimeout		= 0x0B,		// ADC conversion timeout
	eNSHErrLedOutOfRange		= 0x0C,		// Led feedback out of range
	eNSHErrRefOutOfRange		= 0x0D,		// Reference signal out of range
	eNSHErrFluoOutOfRange		= 0x0E,		// Fluorescence out of range
	// Sanity Auto Calibration Errors
	eNSHErrPreAmpADOutOfTemp	= 0x15,		// Preamp A/D out of range
	eNSHErrOutOfAirWarning		= 0x16,		// Out of air warning limit
	eNSHErrOutOfAirSupLimit		= 0x17,		// Out of air superior error limit
	eNSHErrOutOfAirInfLimit		= 0x18,		// Out of air inferior error limit
	eNSHErrSolidStdWarning		= 0x19,		// Solid standard auto calibration warning
	eNSHErrSolidStdAutoErr		= 0x1A,		// Auto calibration error
	eNSHErrSolidCalibEEpromErr	= 0x1B,		// Calibration error or eeprom error
	// Equalization errors
	eNSHErrFluoCalibErr			= 0x29,		// Fluorescence calibration error
	eNSHErrReferenceCalibErr	= 0x2A,		// Reference calibration error
	eNSHErrExtFluCalibErr		= 0x2B,		// Extended range flou calibration error
	eNSHErrExtRefCalibErr		= 0x2C,		// Extended range ref calibration error
	eNSHErrExtFluoRefErr		= 0x2D,		// Ext range flou/ref calibration error
	eNSHErrAirExtRangeErr		= 0x2E,		// Air ext range calibration error
	eNSHErrAirCalibErr			= 0x2F,		// Air calibration error
	eNSHErrCalibSeqError		= 0x36,		// Calibration sequence error
	// Generics Error
	eNSHErrEEpromReadWrite		= 0x47,		// EEprom data read/write error
	eNSHErrMonitorModeActive	= 0x48,		// Monitor mode active
	eNSHErrBarcodeRead			= 0x49,		// Barcode reade error
} eNSHProtocolErr;


typedef enum
{
	eLittleEndian,
	eBigEndian
} eMachineEndianness;



/*! **********************************************************************************************
 *									NSH Reader General											 *
 * ********************************************************************************************* */

#define NSH_DONE						"DNE"
#define MAX_NUM_STRIP_ADMITTED			12
#define AIR_RFU_CAL_LOW_LIMIT			1
#define AIR_RFU_CAL_UP_LIMIT			9
#define RFU_3144_CAL_LOW_LIMIT			2700
#define RFU_3144_CAL_UP_LIMIT			3700

#define NSH_ENABLE_CONVERSION_FACTOR	0

typedef enum
{
	eAppVersion			= '0',
	eBootVersion		= '1',
	eSelectorVersion	= '3',
	eBoardHwVersion		= '4',

	eVersionNone		= -1
} enumFwVersion;

typedef enum
{
	eAllParam			= '0',
	eAllButEqualization = '1',
	eAllButEqCounters	= '2'
} enumRestoreParam;

typedef struct
{
	uint8_t ucApplication;
	uint8_t ucBoot1;
	uint8_t ucBoot2;
	uint8_t ucEEprom;
	uint8_t ucRam;
	uint8_t ucCameraID;
	uint8_t ucSpare1;
	uint8_t ucSpare2;
	uint8_t ucStep;
} structNSHStatus;


typedef struct
{
	int32_t  liRFUValue;
	uint32_t uliStdDev;
	int32_t  liMaxRFVValue;
	int32_t  liMinRFVValue;
} structNSHFluoRead;


typedef struct
{
	int32_t liRes[3];
	int32_t liFactor;
	int32_t liMinValueRead;
	int32_t liMaxValueRead;
	int32_t liReading;
} structNSHLastReference;


typedef struct
{
	int32_t liRes[3];
	int32_t liFactor;
	int32_t liMinValueRead;
	int32_t liMaxValueRead;
	int32_t liReading;
} structNSHLastRFV;


typedef struct
{
	uint32_t uliMostSignifSHId;
	uint32_t uliMediumSHId;
	uint32_t uliLeastSignifSHId;
} structSHIdentification;

typedef struct
{
	float fSSCalibPar;
	float fSectionACalibPar;
	float fSectionBCalibPar;
} strucCalibrParam;

typedef enum
{
	eNSHModelDisabled	= 1,	// disable conversion
	eNSHModelEnabled	= 0		// with model
} enumNSHConvModelFluoRead;

typedef enum
{
	eNSHStatusCheckNotDone  = 0x24,	// '$' character :  checksum verification has never been done yet
	eNSHStatusCheckNotOK	= 0x58, // 'X' character :  checksum verification was not OK
	eNSHStatusCheckOK		= 0x2D	// '-' character :  checksum was OK
} enumNSHStatusCheck;

typedef enum
{
	eNSHStatusBoot			= 0x30,	// 0 in Ascii code, NSH board is booting
	eNSHStatusUpload		= 0x31,	// 1 in Ascii code, NSH board is performing an update with Serial Ymodem
	eNSHStatusRun			= 0x32, // 2 in Ascii code, NSH board is running
	eNSHStatusMonitor		= 0x33,	// 3 in Ascii code, NSH board is in Monitor Mode
	eNSHStatusError			= 0x34, // 4 in Ascii code, NSH board encountered an error
	eNSHStatusAppInit		= 0x35,	// 5 in Ascii code, NSH board is initializing application structure

	// the following defines are valid for the firmware update
	eNSHStatusWaitDbg		= 0x36,	// 6 in ascii code, NSH board is waiting an update with debug serial link
	eNSHStatusWaitSpi		= 0x37,	// 7 in ascii code, NSH board is waiting an update with spi
	eNSHStatusProgFlash		= 0x38,	// 8 in ascii mode, program flash
	eNSHStatusUpgError		= 0x39, // 9 in ascii code, NSH board detects an error during the upload
	eNSHStatusBootError		= 0x41,	// A in ascii code, NSH board detects an error during the boot
	eNSHStatusWaitReset		= 0x42,	// B in ascii code, NSH board is waiting a reset
	eNSHStatusSpare1		= 0x43,	// C in ascii code, NSH board spare 1
	eNSHStatusSpare2		= 0x44,	// D in ascii code, NSH board spare 2
	eNSHStatusSpare3		= 0x45,	// E in ascii code, NSH board spare 3
	eNSHStatusSpare4		= 0x46,	// F in ascii code, NSH board spare 4

	eNSHUpdErrTxAbort		= 0x50
}eNSHBoardStatus;

typedef enum
{
	eNSHLedEnabled	= 0,
	eNSHLedDisabled = 1
} enumNSHLed;


#endif // NSHREADERCOMMONINCLUDE_H
