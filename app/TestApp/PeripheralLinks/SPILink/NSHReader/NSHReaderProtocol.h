﻿/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderProtocol.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the NSHReaderProtocol class.
 @details

 ****************************************************************************
*/

#ifndef NSHREADERPROTOCOL_H
#define NSHREADERPROTOCOL_H

#include <sstream>
#include <vector>

#include "SPIInterface.h"
#include "NSHReaderDRDY.h"
#include "NSHReaderCommonInclude.h"
#include "NSHReaderMessageBlock.h"
//#include "SPIErrors.h"

class NSHReaderProtocol : public NSHReaderGPIO,
						  public NSHReaderMessageBlock
{
	public:

		/*! **********************************************************************************************************************
		 * @brief NSHBoardProtocol default constructor
		 * ***********************************************************************************************************************
		 */
		NSHReaderProtocol();

		/*! **********************************************************************************************************************
		 * @brief ~NSHBoardProtocol virtual constructor
		 * ***********************************************************************************************************************
		 */
		virtual ~NSHReaderProtocol();

		/*! **********************************************************************************************************************
		 * @brief  sendNSHMessage handle a complete TX event: command block, payload blocks and checksum block.
		 * @param  cErrCode error code of the transmission event.
		 * @param  typeProt can be eSPIbin or eSpiAscii.
		 * @param  uliTimeOutMsec timeout for tx event with SPI.
		 * @return 0 in case of success, -1 in case of incomplete configuration, -2 in case of errors.
		 * ***********************************************************************************************************************
		 */
		int8_t sendNSHMessage(int32_t& cErrCode, eSPIProtoType typeProt, uint32_t uliTimeOutMsec = eGetDefaultTimeoutMsec);

		/*! **********************************************************************************************************************
		 * @brief  receiveNSHResponse handle a complete RX event.
         * @param strResponse the received string
		 * @param  cErrCode error code of the transmission event.
		 * @param  typeProt can be eSPIbin or eSpiAscii.
		 * @param  uliTimeOutMsec timeout for tx event with SPI.
		 * @return 0 in case of success, -1 in case of msg sent error, -2 err message rx, -3 for parsing err.
		 * ***********************************************************************************************************************
		 */
		int8_t receiveNSHResponse(string& strResponse, int32_t& cErrCode, eSPIProtoType typeProt, uint32_t uliTimeOutMsec = eGetDefaultTimeoutMsec);

		/*! **********************************************************************************************************************
		 * @brief  sendSingleMsgSPIProt send a single msg (4B) waiting for DRDY line.
		 * @param  pTxBuffer data to be sent.
		 * @param  pRxBuffer data received.
		 * @param  uliTimeOutMsec timeout for DRDY line to be ready.
		 * @return 0 in case of success, -1 bad parameters, -2 DSP busy, -3 in case of msg sent error.
		 * ***********************************************************************************************************************
		 */
		int8_t sendSingleMsgSPIProt(uint8_t *pTxBuffer, uint8_t *pRxBuffer, uint32_t uliTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  checkErrSPIMsgReceived parse packet received and check if it's an error message.
		 * @param  pRxBuffer packet received where the control has to be performed.
		 * @param  cErrCode error code of the transmission event.
		 * @param  typeProt protocol type, can be binary or ASCII.
		 * @return eNSHProtNoError if there is no error, eNSHProtError in case of error.
		 * ***********************************************************************************************************************
		 */
		eNSHProtocolErr checkErrSPIMsgReceived(uint8_t *pRxBuffer, int32_t& cErrCode, eSPIProtoType typeProt);

		/*! **********************************************************************************************************************
		 * @brief  setNSHErrors set m_pNSHError
		 * @param  pNSHError pointer to NSHError instance
		 * @return true in case of success, false otherwise
		 * ***********************************************************************************************************************
		 */
//        bool setNSHErrors(SPIErrors* pNSHError);

		/*! **********************************************************************************************************************
		 * @brief  getMachineEndiannes find out if the machine is little or big endian
		 * @return endianness
		 * ***********************************************************************************************************************
		 */
		eMachineEndianness getMachineEndiannes(void);

    protected:

        /*! **********************************************************************************************************************
         * @brief  parseSubUnitAnswer virtual function, reimplemented in NSHBoardGeneral.
         * @param  strRes string where the output should be saved.
         * @return 0 in case of success, < 0 otherwise.
         * ***********************************************************************************************************************
         */
        virtual int8_t parseSubUnitAnswer(string& strRes) = 0;

    private:

		/*! **********************************************************************************************************************
		 * @brief littleToBigEndian32 switch from little endian to big endian. The NSH wants big endian type messages.
		 * @param pcBuffer buffer to be switched.
		 * ***********************************************************************************************************************
		 */
		void littleToBigEndian32(uint8_t* pcBuffer);

    protected:

        bool				m_bConfigOk;
        eMachineEndianness	m_eEndianness;
        uint8_t				m_ucCmdIdToBeSent;

        SPIInterface*	m_pSPIIfr;

    private:

        string			m_strReadPkt;
//        SPIErrors*		m_pNSHError;

};

#endif // NSHREADERPROTOCOL_H
