/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderFwUpdate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the NSHReaderFwUpdate class.
 @details

 ****************************************************************************
*/

#include "NSHReaderFwUpdate.h"
#include "NSHReaderGeneral.h"
#include "NSHReaderCommonInclude.h"
#include "FolderManager.h"
#include "CRCFile.h"

#define NSH_TIMEOUT_FLASHBURN			50//30 // sec
#define NSH_TIMEOUT_RESTARTIMX27		15	//sec

#define MAX_NUM_ERROR_SPI				256
#define PROT_START_BINMSG				(0)
#define PROT_MASK_BINMSG				(~0x80)
#define PROT_END_BINMSG					0x80

#define POLY		0x1021

NSHReaderFwUpdate::NSHReaderFwUpdate()
{
	memset(&m_rgMemBuf, 0x00, sizeof(m_rgMemBuf));
	memset(&m_rgCrc16Table, 0x00, sizeof(m_rgCrc16Table));
	m_pNSHReaderGeneral = 0;
	m_isTableCreated = 0;
}

NSHReaderFwUpdate::~NSHReaderFwUpdate()
{

}

bool NSHReaderFwUpdate::init(NSHReaderGeneral* pNSHReaderGeneral, Log* pLogger)
{
	if ( pNSHReaderGeneral == 0 )	return false;
	if ( pLogger == 0 )				return false;

	m_pNSHReaderGeneral = pNSHReaderGeneral;
	m_pLogger = pLogger;

	return true;
}

int NSHReaderFwUpdate::getVersion(enumUpdFirmware eFwType, string& sVersion)
{
	sVersion.clear();
	int cRes = -1;

	if ( m_pNSHReaderGeneral != NULL )
	{
		switch ( eFwType )
		{
			case eUpdFwBootLoader:
				cRes = m_pNSHReaderGeneral->getFwVersion(eBootVersion, sVersion);
			break;

			case eUpdFwApplicative:
				cRes = m_pNSHReaderGeneral->getFwVersion(eAppVersion, sVersion);
			break;

			default:
				sVersion = "NONE";
			break;
		}
	}
	return cRes;
}

int NSHReaderFwUpdate::updateFirmware(enumUpdType eFwType, string& sSourcePath, long lLength, unsigned long ullCrc32)
{
	if ( m_pNSHReaderGeneral == NULL )							return -1;
	if ( eFwType != eBootLoader && eFwType != eApplicative )	return -1;

	enumUpdFirmware eNSHType;
	eNSHType = (eFwType==eBootLoader) ? eUpdFwBootLoader : eUpdFwApplicative;

	int liRes = updateFirmwareNSH(eNSHType, sSourcePath, lLength, ullCrc32);
	if ( ( liRes == -1 ) && ( eNSHType == eUpdFwBootLoader ) )
	{
		return -1; // report error to break the firmware update
	}

	return 0;
}

int NSHReaderFwUpdate::exeCommand(enumFWUpgradeCmd eCommand)
{
	if ( m_pNSHReaderGeneral == NULL )	return -1;

	int liRes;

	switch ( eCommand )
	{
		case eReset:

		case eResetNominal:

		case eResetUpgrade:
			m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::exeCommand: firmware update.reset[%d]",eCommand);
			liRes = m_pNSHReaderGeneral->hwResetForFwUpdate();
			if ( ! liRes )
			{
				//bootloader
				liRes = m_pNSHReaderGeneral->checkDRDYstatusWithTimeout();
				m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::exeCommand: firmware update - bootloader");

				if ( ! liRes )
				{
					//if reset or nominal wait for the second DRDY
					if ( (eCommand == eReset) || (eCommand == eResetNominal) )
					{
						//applicative
						liRes = m_pNSHReaderGeneral->checkDRDYstatusWithTimeout();
						m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::exeCommand: firmware update - applicative");
					}
				}

				if ( liRes )
				{
					m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::exeCommand: waitDRDY after reset failure");
					return -1;
				}
				m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::exeCommand: reset completed");
			}
		break;

		default:
			return -1;
		break;
	}

	return 0;
}

int NSHReaderFwUpdate::updateFirmwareNSH(enumUpdFirmware eFwType, string& sSourcePath, unsigned long uliLength, uint32_t uliCrc32)
{
	if ( m_pNSHReaderGeneral == NULL )	return -1;

	m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::updateFirmwareNSH: fimware upload started");
	int liFwType = eVersionNone;

	switch ( eFwType )
	{
		case eUpdFwBootLoader:
			liFwType = eBootVersion;
		break;

		case eUpdFwApplicative:
			liFwType = eAppVersion;
		break;

		default:
		break;
	}

	if ( liFwType == eVersionNone)
	{
		m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::updateFirmwareNSH: Memory type Not supported [%d]", liFwType);
		return -1;
	}

	//check file to upgrade
	FolderManager manager;
	manager.loadFileName(sSourcePath);

	if ( ! manager.exists() )
	{
		m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::updateFirmwareNSH: error opening file %s\n", sSourcePath.c_str());
		return -1;
	}

	bool bIsFileOK = false;
	unsigned long uliLenFile;
	int liRes;

	manager.getFileLen(uliLenFile);

	if ( uliLenFile == uliLength )
	{
		//check crc of the file
		CRCFile fileImageCrc(m_pLogger);
		unsigned long uliCrc32File;
		liRes = fileImageCrc.calculateCRCFileMaxAlloc(sSourcePath, uliCrc32File);

		if ( ! liRes )
		{
			if ( uliCrc32File == uliCrc32 )
			{
				bIsFileOK = true;
			}
		}

		if ( liRes || ( bIsFileOK == false ) )
		{
			m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::updateFirmwareNSH: CRC32 mismatch");
			return -1;
		}
	}
	else
	{
		m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::updateFirmwareNSH: lenght error, %d != %d", uliLenFile, uliLength);
		return -1;
	}

//	increaseOpProgress();
//	int liProgress = getProgressTableOP();

	if ( bIsFileOK==true )
	{
		//reset board and wait bootloader
		liRes = exeCommand(eResetUpgrade);
		if ( liRes )
		{
			m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::updateFirmwareNSH: Unable to reset the board");
		}

		int liTimeoutOpNSH = NSH_TIMEOUT_RESTARTIMX27;
		structNSHStatus NSHstatus;
		uint8_t ucStatusReceived = 0;

		do
		{
			liRes = m_pNSHReaderGeneral->getNSHStatus(&NSHstatus);
			if ( ! liRes )
			{
				switch ( NSHstatus.ucStep )
				{
					case eNSHStatusError:		// 4 in Ascii code, NSH board encountered an error
						m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::updateFirmwareSpi: error on NSH during boot");
					break;

					case eNSHStatusWaitDbg:		// 6 in ascii code, NSH board is waiting an update with debug serial link
					case eNSHStatusBoot:		// 0 in Ascii code, NSH board is booting
						/* nothing to do, wait status change */
					break;

					case eNSHStatusWaitSpi:		// 7 in ascii code, NSH board is waiting an update with spi
						m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::updateFirmwareNSH: NSH Board status ready to upgrade with spi");
						liTimeoutOpNSH = 0;
						ucStatusReceived = 1;
					break;

					default:
						m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::updateFirmwareNSH: NSH Board in incorrect state %C", NSHstatus.ucStep);
						liTimeoutOpNSH = 0;
					break;
				}
			}

			if ( liTimeoutOpNSH > 0 )
			{
				sleep(1);
			}
			liTimeoutOpNSH--;
		}
		while ( liTimeoutOpNSH > 0 );

		if ( ucStatusReceived != 1 )
		{
			m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::updateFirmwareNSH: error, unit not ready %C", NSHstatus.ucStep);
			return -1;
		}

//		increaseOpProgress();
//		liProgress = getProgressTableOP();

		//board ready to upgrade
		liRes = m_pNSHReaderGeneral->upgradeNSHFirmwareRequest(eFwType, uliLenFile);
		if ( liRes )
		{
			m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::updateFirmwareNSH: error, unit not ready %C", NSHstatus.ucStep);
			return -1;
		}

		liRes = upgradeFirmware(sSourcePath, uliLenFile);

		int flashBurnStarted = 0;
		if ( ! liRes )
		{
			flashBurnStarted = 1;
		}

		if ( flashBurnStarted == 1 )
		{
			// wait start program flash
			liTimeoutOpNSH = NSH_TIMEOUT_FLASHBURN;
			sleep(1);
			do
			{
				/*......................................................................
				// verify status of board --> send throught SPI gst
				.......................................................................*/
				liRes = m_pNSHReaderGeneral->getNSHStatus(&NSHstatus);
				if ( ! liRes )
				{
					switch ( NSHstatus.ucStep  )
					{
						case eNSHStatusProgFlash:
							m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::updateFirmwareNSH: flash upgrade in progress");
						break;

						case eNSHStatusWaitReset:
							m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::updateFirmwareNSH: wait for reset");
							liRes = exeCommand(eResetNominal);
							if ( liRes )
							{
								m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::updateFirmwareNSH: error to reset NSH");
							}
						break;

						case eNSHStatusError:		// exit the loop correctly
						case eNSHStatusUpgError:	// exit the loop correctly
						case eNSHStatusBootError:	// exit the loop correctly
							m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::updateFirmwareNSH: firmware upgraded on NSH");
							liTimeoutOpNSH = 0;
						break;
					}
				}

				sleep(1);
				liTimeoutOpNSH--;

			} while ( liTimeoutOpNSH > 0 );

			if ( liTimeoutOpNSH == 0 )
			{
				/*......................................................................
				  // procedure ended with a timeout error
				  .......................................................................*/
				m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::updateFirmwareNSH: error, timeout on flash elapsed");
				liRes = -1;
			}
		}
	}

//	increaseOpProgress();
//	liProgress = getProgressTableOP();

	return liRes;
}

int NSHReaderFwUpdate::getProgressTableOP()
{
	return getOperationProgress();
}


/*!*******************************************************************************************
Send buffer with spi in binary format.

 structure
   header
			stopbit |	Lastpacketmodule 128	| byte1 | byte2 | byte3 |
			0 cont	|	counter %128			| XX    | XX    |  XX   |
				...................................................
			0 cont	|	counter %128			| XX    | XX    |  FF   |
			0 cont	|	counter %128			| CRCMSB| CRCLSB|  00   |
			1 cont	|	counter %128			| CRCMSB| CRCLSB|  00   | // end protocol

  0xFF to pad the buffer
*********************************************************************************************/

int NSHReaderFwUpdate::upgradeFirmware(string& sFileName, uint32_t uliLenFile)
{
	if ( m_pNSHReaderGeneral == NULL )	return -1;

	fstream fileUpdate;
	uint32_t uliFileLen;

	/*..........................................................................
	  . Check File to upload                                                   .
	  .........................................................................*/
	fileUpdate.open(sFileName.c_str(), std::fstream::in|std::fstream::binary);

	if ( fileUpdate.is_open() )
	{
		uliFileLen = uliLenFile;

		if ( uliFileLen == 0 )
		{
			fileUpdate.seekp(0,ios_base::end);
			uliFileLen = fileUpdate.tellp();
			fileUpdate.seekp(0,ios_base::beg);
		}
		m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::upgradeFirmware: length of %s is %d", sFileName.c_str(), uliFileLen);
	}
	else
	{
		m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::upgradeFirmware: error opening file %s", sFileName.c_str());
		return -1;
	}

	/*..........................................................................
	  . Communication channel free for next command. Extracts the always 3 char  .
	  . long data from the received buffer and put it into the stream            .
	  . structure together with the "header" identifier.                 				.
	  ...........................................................................*/
	// first packet
	uint32_t uliIdxMemBuf = 0;
	uint8_t  ucCntSpiBin = PROT_START_BINMSG & PROT_MAX_CNT_BIN;
	uint8_t	ucSpiHeaderBin = PROT_START_BINMSG + ucCntSpiBin;
	uint32_t uliDataToCopy;
	uint8_t rgDataTx[4] = {0};
	uint8_t rgDataRx[4] = {0};
	unsigned char ucMsgBuf[NSH_READER_SPI_PKT_DATA_LEN + NSH_READER_SPI_PKT_HEAD_LEN];
	int liErrCode;
	int8_t cRes = -1;
	uint16_t usCounterError = 0;

	int liPrintfCounter = 0; // debug

	/*..........................................................................
	  . Splits the remaining memory into multiple packets 3 characters long to   .
	  . comply with the protocol specification.                                  .
	  ...........................................................................*/
	while ( uliIdxMemBuf < uliFileLen )
	{
		m_rgMemBuf[ucCntSpiBin] = fileUpdate.tellp();
		uliDataToCopy = uliFileLen - uliIdxMemBuf;

		if  ( uliDataToCopy > NSH_READER_SPI_PKT_DATA_LEN )
		{
			uliDataToCopy = NSH_READER_SPI_PKT_DATA_LEN;
		}

		rgDataTx[NSH_READER_SPI_PKT_HEAD_IDX] = ucSpiHeaderBin;

		// ucMsgBuf contains the 4 B of the fw update
		fileUpdate.read((char*)&(ucMsgBuf), uliDataToCopy);
		memcpy((char*)&(rgDataTx[NSH_READER_SPI_PKT_DATA_IDX]), ucMsgBuf, uliDataToCopy);

		uliIdxMemBuf += uliDataToCopy;
		upgradeFirmwareProgress(uliIdxMemBuf, uliFileLen);

		/*..................................................................
		  . Fill with SPIH_PAD_CHAR the remaining of the current payload.    .
		 ...................................................................*/
		if ( uliDataToCopy < NSH_READER_SPI_PKT_DATA_LEN )
		{
			uint8_t ucIdHeader = NSH_READER_SPI_PKT_DATA_IDX + uliDataToCopy;
			memset( (char *)&(rgDataTx[ucIdHeader]), eSPICmdPADBin, NSH_READER_SPI_PKT_DATA_LEN - uliDataToCopy );
		}

		/*..........................................................................
		  . Send the packet toward the head. The packet returned from the    			.
		  . head is meaningless for this first exchange.                           .
		  ...........................................................................*/
		cRes = m_pNSHReaderGeneral->sendSingleMsgSPIProt(rgDataTx, rgDataRx, eFirmwareUpgradeTimeoutMsec);		
		if ( cRes )
		{
			m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::upgradeFirmware: unable to send single SPI .bin message.");
			break;
		}

		float fPerc = (float(uliIdxMemBuf*100.0f)) / float(uliFileLen);

		if ( fPerc >= float(liPrintfCounter) )
		{
			printf("FW Update::percentage of packets sent: %.2f%% \r", fPerc); fflush(stdout);
			liPrintfCounter += 2;
		}

		if ( ( rgDataRx[NSH_READER_SPI_PKT_HEAD_IDX] == eSPIErrBinPktId ) ||
			 ( rgDataRx[NSH_READER_SPI_PKT_HEAD_IDX] == eSPIEndBinPktId ) ||
			 ( rgDataRx[NSH_READER_SPI_PKT_HEAD_IDX] == eSPIErrBinProtId ) )
		{
			/*..................................................................
			  . Error following the previos packet. Repeat last packet
			  .  the err_code contains the packet counter with error						  .
			  ...................................................................*/
			m_pNSHReaderGeneral->checkErrSPIMsgReceived(rgDataRx, liErrCode, eSPIbin);

			if ( liErrCode == ucCntSpiBin )
			{
				// actual packet sent is equal to packet error required
				ucCntSpiBin++;
				ucCntSpiBin &= PROT_MAX_CNT_BIN;
				ucSpiHeaderBin = PROT_MASK_BINMSG & ucCntSpiBin;
				m_rgMemBuf[ucCntSpiBin] = uliIdxMemBuf;
			}
			else
			{
				// resend last packet error
				ucCntSpiBin = (unsigned char)(liErrCode);
				ucCntSpiBin &= PROT_MAX_CNT_BIN;
				ucSpiHeaderBin = PROT_MASK_BINMSG & ucCntSpiBin;
				// move pointer memory
				uliIdxMemBuf = m_rgMemBuf[ucCntSpiBin];
				// move pointer file to pos
				fileUpdate.seekp(uliIdxMemBuf, ios_base::beg);
				usCounterError++;
				if ( usCounterError >= MAX_NUM_ERROR_SPI )
				{
					m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::upgradeFirmware: exeeded max num error of packets sent");
					break;
				}
			}
		}
		else
		{
			if ( ( rgDataRx[NSH_READER_SPI_PKT_HEAD_IDX]   == eSPIDmyPktId ) &&
				 ( rgDataRx[NSH_READER_SPI_PKT_DATA_IDX]   == 'D' ) &&
				 ( rgDataRx[NSH_READER_SPI_PKT_DATA_IDX+1] == 'M' ) &&
				 ( rgDataRx[NSH_READER_SPI_PKT_DATA_IDX+2] == 'Y' ) )
			{
				//counterError=0;// reset counter errors
				ucCntSpiBin++;
				ucCntSpiBin &= PROT_MAX_CNT_BIN;
				ucSpiHeaderBin = PROT_MASK_BINMSG & ucCntSpiBin;
				m_rgMemBuf[ucCntSpiBin] = uliIdxMemBuf;
			}
			else
			{
				// not valid response
				usCounterError = MAX_NUM_ERROR_SPI;
				m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::upgradeFirmware: exeeded max num error of packets sent");
				break;
			}
		}
	}

	// If an error was encountered, close the file and exit the function.
	if ( cRes || usCounterError >= MAX_NUM_ERROR_SPI )
	{
		if ( fileUpdate.is_open() )
		{
			fileUpdate.close();
		}
		return -1;
	}

	/*..........................................................................
	  . Computes the checksum on the just set stream and stores it into the      .
	  . reserved space.                                                          .
	 ...........................................................................*/

	uint16_t usCrc16 = 0;

	fileUpdate.seekp(0,ios_base::beg);

	for ( uliIdxMemBuf = 0; uliIdxMemBuf < uliFileLen; uliIdxMemBuf++)
	{
		fileUpdate.read((char*)&ucMsgBuf[0], 1);
		crc16Seq((int16_t*)&usCrc16, ucMsgBuf[0]); //short
	}
	swap16IfLilEndian((unsigned char*)&usCrc16);

	// ucCntSpiBin++; is just increased by while statement
	ucCntSpiBin &= PROT_MAX_CNT_BIN;
	ucSpiHeaderBin = PROT_MASK_BINMSG & ucCntSpiBin;

	memset(rgDataTx, 0x00, sizeof(rgDataTx));
	rgDataTx[NSH_READER_SPI_PKT_HEAD_IDX] = ucSpiHeaderBin;
	memcpy((char *)&rgDataTx[NSH_READER_SPI_PKT_DATA_IDX], &(usCrc16), 2);

	/*..........................................................................
	  . Send the packet toward the head. The packet returned from the    .
	  . head is meaningless for this first exchange.                             .
	  ...........................................................................*/

	cRes = m_pNSHReaderGeneral->sendSingleMsgSPIProt(rgDataTx, rgDataRx, eFirmwareUpgradeTimeoutMsec);
	if ( cRes == 0 )
	{
		/*..........................................................................
		  . Send the packet with end protocol flag toward the head.                 .
		  ...........................................................................*/
		ucCntSpiBin++;
		ucCntSpiBin &= PROT_MAX_CNT_BIN;
		ucSpiHeaderBin = PROT_END_BINMSG + ucCntSpiBin;
		rgDataTx[NSH_READER_SPI_PKT_HEAD_IDX] = ucSpiHeaderBin;

		cRes = m_pNSHReaderGeneral->sendSingleMsgSPIProt(rgDataTx, rgDataRx, eFirmwareUpgradeTimeoutMsec);

		if ( rgDataRx[NSH_READER_SPI_PKT_HEAD_IDX] == eSPIEndBinPktId )
		{
			m_pNSHReaderGeneral->checkErrSPIMsgReceived(rgDataRx, liErrCode, eSPIbin);

			if ( liErrCode == eSPINextPktCRC )
			{
				m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::upgradeFirmware: end of Upgrade");
			}
		}

		if ( rgDataRx[NSH_READER_SPI_PKT_HEAD_IDX] == eSPIErrPktId )
		{
			/*......................................................................
			  . Error following the previos packet. Stops transmitting and returns .
			  . back to the calling function.                                      .
			  ......................................................................*/
			m_pNSHReaderGeneral->checkErrSPIMsgReceived(rgDataRx, liErrCode, eSPIbin);
			m_pLogger->log(LOG_ERR, "NSHReaderFwUpdate::upgradeFirmware: error updating FW, err[%d]", liErrCode);
			cRes = -1;
		}
	}

	/* end close file*/
	if ( fileUpdate.is_open() )
	{
		fileUpdate.close();
	}

	return cRes;
}

void NSHReaderFwUpdate::upgradeFirmwareProgress(unsigned int uliCurrPos, unsigned int uliEndPos)
{
	if ( m_pNSHReaderGeneral == NULL )	return;

	int liProgress=0;
	static int iLastProgress=0;

	if ( ! uliEndPos )
	{
		liProgress = uliCurrPos * 100 / uliEndPos;
		if ( iLastProgress != liProgress )
		{
			iLastProgress = liProgress;
			m_pLogger->log(LOG_INFO, "NSHReaderFwUpdate::upgradeFirmwareProgress: current progress [%d], end[%d], percentage[%d] \n", uliCurrPos, uliEndPos, liProgress);
		}
	}
}

void NSHReaderFwUpdate::crc16Seq(short *crc, unsigned char m)
{
	if ( ! m_isTableCreated )
	{
		initCRC16();
	}
	*crc = m_rgCrc16Table[(((*crc) >> 8) ^ m) & 0xFF] ^ ((*crc) << 8);
	*crc &= 0xFFFF;
}

void NSHReaderFwUpdate::swap16IfLilEndian(unsigned char *ptuShort)
{
	/*! The scanner head has big endianness. **/

	if ( m_pNSHReaderGeneral->getMachineEndiannes() == eLittleEndian )
	{
		unsigned char *ptCh,tmpCh;

		ptCh = (unsigned char*)ptuShort;
		tmpCh = ptCh[0];
		ptCh[0] = ptCh[1];
		ptCh[1] = tmpCh;
	}
}

void NSHReaderFwUpdate::initCRC16(void)
{
	// Create CRC table dynamically
	short sCrc;

	m_isTableCreated = 0;

	for (int i = 0; i < 256; i++)
	{
		sCrc = (i << 8);
		for (int j = 0; j < 8; j++)
		{
			sCrc = (sCrc << 1) ^ ((sCrc & 0x8000) ? POLY : 0);
		}
		m_rgCrc16Table[i] = sCrc & 0xFFFF;
	}

	m_isTableCreated = 1;
}
