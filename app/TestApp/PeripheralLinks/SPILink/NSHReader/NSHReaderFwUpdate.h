/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderFwUpdate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the NSHReaderFwUpdate class.
 @details

 ****************************************************************************
*/

#ifndef NSHREADERFWUPDATE_H
#define NSHREADERFWUPDATE_H

#include "FWUpdateInterface.h"
#include "FolderManager.h"

#define PROT_MAX_CNT_BIN				(0x7F)


class NSHReaderGeneral;


class NSHReaderFwUpdate : public FWUpdateInterface
{
	public:

		/*! *************************************************************************************************
		 * @brief NSHReaderFwUpdate default constructor
		 * **************************************************************************************************
		 */
		NSHReaderFwUpdate();

		/*! *************************************************************************************************
		 * @brief NSHReaderFwUpdate virtual destructor
		 * **************************************************************************************************
		 */
		virtual ~NSHReaderFwUpdate();

		/*! *************************************************************************************************
		 * @brief  init initialize the class
		 * @param  pNSHReaderGeneral pointer to
		 * @param  pLogger pointer to log
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool init(NSHReaderGeneral* pNSHReaderGeneral, Log* pLogger);

		/*! *************************************************************************************************
		 * @brief  getVersion get fw version
		 * @param  eFwType can be bootloader or app
		 * @param  sVersion response of the NSH
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		virtual int getVersion(enumUpdFirmware eFwType, string& sVersion);

		/*! *************************************************************************************************
		 * @brief  updateFirmware function used to launch the fw update
		 * @param  eFwType can be boot or applicative
		 * @param  sSourcePath path to reach the file
		 * @param  lLength file length
		 * @param  ullCrc32 crc32 of the fw file (given by the outside)
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
		int updateFirmware(enumUpdType eFwType, string& sSourcePath, long lLength, unsigned long ullCrc32);


	protected:

		/*! *************************************************************************************************
		 * @brief  exeCommand execute command requested
		 * @param  eCommand there are many kind of commands
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
		int exeCommand(enumFWUpgradeCmd eCommand);

		/*! *************************************************************************************************
		 * @brief  updateFirmwareNSH function called by updateFirmware that handles all the upg
		 * @param  eFwType can be boot or applicative
		 * @param  sSourcePath path to reach the file
         * @param  uliLength file length
         * @param  uliCrc32 crc32 of the fw file (given by the outside)
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
		int updateFirmwareNSH(enumUpdFirmware eFwType, string& sSourcePath, unsigned long uliLength, uint32_t uliCrc32);

		/*! *************************************************************************************************
		 * @brief getProgressTableOP
		 * @return
		 * **************************************************************************************************
		 */
		int getProgressTableOP(void);

		/*! *************************************************************************************************
		 * @brief  upgradeFirmware send all the pkts for the fw upgrade
		 * @param  sFileName name of the file
		 * @param  uliLenFile length of the file
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
		int upgradeFirmware(string& sFileName, uint32_t uliLenFile);

		// this to be substituted by getProgressTableOp!!!! TODO
		/*! *************************************************************************************************
		 * @brief upgradeFirmwareProgress
		 * @param uliCurrPos
		 * @param uliEndPos
		 * **************************************************************************************************
		 */
		void upgradeFirmwareProgress(unsigned int uliCurrPos, unsigned int uliEndPos);

	private:

		/*! *************************************************************************************************
		 * @brief crc16Seq calculate crc16 from the data
		 * @param crc calculate crc
		 * @param m starting value (0).
		 * **************************************************************************************************
		 */
		void crc16Seq(short* crc, unsigned char m);

		/*! *************************************************************************************************
		 * @brief swap16IfLilEndian swap 2B if the machine is little endian
		 * @param ptuShort pointer to the data to be swapped
		 * **************************************************************************************************
		 */
		void swap16IfLilEndian(unsigned char* ptuShort);

		/*! *************************************************************************************************
		 * @brief initCRC16 initialize CRC16 table
		 * **************************************************************************************************
		 */
		void initCRC16(void);

    protected:

        uint32_t m_rgMemBuf[PROT_MAX_CNT_BIN + 1];
        short	 m_rgCrc16Table[256];
        bool	 m_isTableCreated = 0;

        NSHReaderGeneral* m_pNSHReaderGeneral;

};

#endif // NSHREADERFWUPDATE_H
