/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderLink.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the NSHReaderLink class.
 @details

 ****************************************************************************
*/

#include "NSHReaderLink.h"

NSHReaderLink::NSHReaderLink()
{

}

NSHReaderLink::~NSHReaderLink()
{

}

bool NSHReaderLink::init(Log* pLogger, SPIInterface* pSPIIfr, GpioInterface* pGpio, uint8_t ucDRDYGpioIdx, uint8_t ucResetGpioIdx)
{
	if ( pLogger == NULL )			return false;
	if ( pSPIIfr == NULL )			return false;
	if ( pGpio == NULL )			return false;

	bool bRes = m_NSH.initNSH(pLogger, pSPIIfr, pGpio, ucDRDYGpioIdx, ucResetGpioIdx);
	if ( ! bRes )
	{
		return false;
	}

//    bRes = m_NSHFwUpdate.init(&m_NSH, pLogger);
//    if ( ! bRes )
//    {
//        return false;
//    }

	return true;
}

