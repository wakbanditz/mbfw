/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    LS7366RBase.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the LS7366Rbase class.
 @details

 ****************************************************************************
*/

#ifndef LS7366RBASE_H
#define LS7366RBASE_H

#include <string.h>

#include "SPIInterface.h"
#include "LS7366RCommonInclude.h"
#include "ErrorCodes.h"

typedef enum
{
	eCLR_MDR0	= 0x08,
	eCLR_MDR1	= 0x10,
	eCLR_CNTR	= 0x20,
	eCLR_STR	= 0x30
} enumClearOpCode;

typedef enum
{
	eREAD_MDR0	= 0x48,
	eREAD_MDR1	= 0x50,
	eREAD_CNTR	= 0x60,
	eREAD_OTR	= 0x68,
	eREAD_STR	= 0x70
} enumReadOpCode;

typedef enum
{
	eWRITE_MDR0	= 0x88,
	eWRITE_MDR1	= 0x90,
	eWRITE_DTR	= 0x98
} enumWriteOpCode;

typedef enum
{
	eLOAD_CNTR	= 0xE0,
	eLOAD_OTR	= 0xE4
} enumLoadOpCode;


/*!*************************************************************************************************
 * Registers:
 *	- DTR  that contains the data sent from MOSI (8, 16, 24 or 32 bits configurable).
 *	- CNTR that is the counter which counts the up/down pulses from the inputs. Can be cleared
 *		   loaded from DTR or transferred into OTR. (8, 16, 24 or 32 bits configurable).
 *	- OTR  that can be read back from the MISO (8, 16, 24 or 32 bits configurable).
 *	- STR  where are stored count related status information (8 bits).
 *	- IR   fatches instruction from received data stream (8 bits).
 *	- MDR0 sets up the operating mode for the LS7366R (8 bits).
 *	- MDR1 is appended to MDR0 for additional modes (8 bits)
 ****************************************************************************************************/
class LS7366RBase
{
	public:

		/*! ***********************************************************************************************************
		 * @brief LS7366RBase default constructor.
		 * ************************************************************************************************************
		 */
		LS7366RBase();

		/*! ***********************************************************************************************************
		 * @brief ~LS7366RBase virtual destructor.
		 * ************************************************************************************************************
		 */
		virtual ~LS7366RBase();

		/*! ***********************************************************************************************************
		 * @brief  readReg read from the desired register.
		 * @param  eOpCode enum, register to be read.
		 * @param  ucDataSizeBytes bytes to be read.
		 * @return 0 in case of success, otherwise ERR_NSH_ENCODER_COMMUNICATION or ERR_NSH_ENCODER_CONFIG_NOK.
		 * ************************************************************************************************************
		 */
		int8_t readReg(enumReadOpCode eOpCode, uint8_t ucDataSizeBytes);

		/*! ***********************************************************************************************************
		 * @brief writeReg write value to a register.
		 * @param eOpCode enum, register to be written.
		 * @param uliData data to be written.
		 * @param ucDataSizeBytes data to be written (up to 4B).
		 * @return 0 in case of success, otherwise ERR_NSH_ENCODER_COMMUNICATION,r ERR_NSH_ENCODER_CONFIG_NOK or
		 *		   ERR_NSH_ENCODER_BAD_PARAM
		 * ************************************************************************************************************
		 */
		int8_t writeReg(enumWriteOpCode eOpCode, uint32_t uliData, uint8_t ucDataSizeBytes);

		/*! ***********************************************************************************************************
		 * @brief  clearReg clear desired register to 0x00.
		 * @param  eOpCode enum, register to be cleared.
		 * @return 0 in case of success, otherwise ERR_NSH_ENCODER_COMMUNICATION or ERR_NSH_ENCODER_CONFIG_NOK.
		 * ************************************************************************************************************
		 */
		int8_t clearReg(enumClearOpCode eOpCode);

		/*! ***********************************************************************************************************
		 * @brief  loadReg transfer data from DTR to CNTR (eLOAD_CNTR) or from CNTR to OTR (eLOAD_OTR).
		 * @param  eOpCode enum, register to be loaded.
		 * @return 0 in case of success, otherwise ERR_NSH_ENCODER_COMMUNICATION or ERR_NSH_ENCODER_CONFIG_NOK.
		 * ************************************************************************************************************
		 */
		int8_t loadReg(enumLoadOpCode eOpCode);

    protected:

        Log*		  m_pLogger;
        SPIInterface* m_pSPIIfr;

        bool		 m_bConfigOk;
        uint8_t		 m_rgcTxBuf[NSH_ENCODER_MAX_PKT_SIZE+1];
        uint8_t		 m_rgcRxBuf[NSH_ENCODER_MAX_PKT_SIZE+1];

};

#endif // LS7366RBASE_H
