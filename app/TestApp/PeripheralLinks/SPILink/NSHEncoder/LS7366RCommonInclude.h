/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    LS7366RCommonInclude.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the LS7366 class.
 @details

 ****************************************************************************
*/

#ifndef LS7366RCOMMONINCLUDE_H
#define LS7366RCOMMONINCLUDE_H

#include <stdint.h>

// Project defines

#define ENC_MULTIPLIER					4
#define ENC_COUNTS_TO_HALF_STEPS		( ENC_MULTIPLIER / 2 )



// LS7366R  general defines
#define NSH_ENCODER_MAX_PKT_SIZE	5
#define REGISTER_SIZE_BYTES			4

/*!* MDR0 configuration data ***/

//Count modes
#define NQUAD		0x00		//non-quadrature mode
#define QUADRX1		0x01		//X1 quadrature mode
#define QUADRX2		0x02		//X2 quadrature mode
#define QUADRX4		0x03		//X4 quadrature mode

//Running modes
#define FREE_RUN	0x00
#define SINGE_CYCLE 0x04
#define RANGE_LIMIT 0x08
#define MODULO_N	0x0C

//Index modes
#define DISABLE_INDX	0x00	//index_disabled
#define INDX_LOADC		0x10	//index_load_CNTR
#define INDX_RESETC		0x20	//index_rest_CNTR
#define INDX_LOADO		0x30	//index_load_OL
#define ASYNCH_INDX		0x00	//asynchronous index
#define SYNCH_INDX		0x80	//synchronous index

//Clock filter modes
#define FILTER_1	0x00		//filter clock frequncy division factor 1
#define FILTER_2	0x80		//filter clock frequncy division factor 2


/*!* MDR1 configuration data ***/

//Flag modes
#define NO_FLAGS	0x00		//all flags disabled
#define IDX_FLAG	0x10		//IDX flag
#define CMP_FLAG	0x20		//CMP flag
#define BW_FLAG		0x40		//BW flag
#define CY_FLAG		0x80		//CY flag

//1 to 4 bytes data-width
#define BYTE_4		0x00		//four byte mode
#define BYTE_3		0x01		//three byte mode
#define BYTE_2		0x02		//two byte mode
#define BYTE_1		0x03		//one byte mode

//Enable/disable counter
#define EN_CNTR		0x00		//counting enabled
#define DIS_CNTR	0x04		//counting disabled


/* LS7366R op-code list */

#define CLR_MDR0	0x08
#define CLR_MDR1	0x10
#define CLR_CNTR	0x20
#define CLR_STR		0x30

#define READ_MDR0	0x48
#define READ_MDR1	0x50
#define READ_CNTR	0x60
#define READ_OTR	0x68
#define READ_STR	0x70

#define WRITE_MDR1	0x90
#define WRITE_MDR0	0x88
#define WRITE_DTR	0x98

#define LOAD_CNTR	0xE0
#define LOAD_OTR	0xE4

// Connection Protocol
typedef struct
{
	uint8_t ucSignBit:1;		// 1 negative, 0 positive
	uint8_t ucCountDir:1;		// 0 count down, 1 count up
	uint8_t ucPowerLoss:1;		// Power loss indicator
	uint8_t ucCountEnable:1;	// 0 counting disabled, 1 counting enabled
	uint8_t ucLatchIdx:1;		// Index latch
	uint8_t ucCompareLatch:1;	// Compare (CNTR = DTR) latch
	uint8_t ucBorrowLatch:1;	// Borrow (CNTR underflow) latch
	uint8_t ucCarryLatch:1;		// Carry (CNTR overflow) latch
} structSTRReg;

typedef union
{
	structSTRReg structStatusReg;
	uint8_t		 ucStatus;
} uSTRReg;

#endif // LS7366RCOMMONINCLUDE_H
