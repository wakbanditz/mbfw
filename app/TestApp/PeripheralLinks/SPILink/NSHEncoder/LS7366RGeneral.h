/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    LS7366RGeneral.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the LS7366RGeneral class.
 @details

 ****************************************************************************
*/

#ifndef LS7366RGENERAL_H
#define LS7366RGENERAL_H

#include "LS7366RBase.h"

class LS7366RGeneral : private LS7366RBase
{

	public:

		/*! ***********************************************************************************************************
		 * @brief LS7366RGeneral default constructor.
		 * ************************************************************************************************************
		 */
		LS7366RGeneral();

		/*! ***********************************************************************************************************
		 * @brief ~LS7366RGeneral virtual destructor.
		 * ************************************************************************************************************
		 */
		virtual ~LS7366RGeneral();

		/*! ***********************************************************************************************************
		 * @brief  init initialize encoder.
		 * @param  pLogger pointer to logger instance.
		 * @param  pSPIIfr pointer to SPI interface instance.
		 * @return true in case of success, false otherwise.
		 * ************************************************************************************************************
		 */
		bool init(Log* pLogger, SPIInterface* pSPIIfr);

		/*! ***********************************************************************************************************
		 * @brief  readCounter read value from the quadrature counter.
		 * @param  liCnt variable where the value will be stored.
		 * @return ERR_NSH_ENCODER_COMMUNICATION in case of error, 0 in case of success.
		 * ************************************************************************************************************
		 */
		int8_t readCounter(int& liCnt);

		/*! ***********************************************************************************************************
		 * @brief  resetCounter clear counter.
		 * @return 0 in case of success, ERR_NSH_ENCODER_COMMUNICATION otherwise.
		 * ************************************************************************************************************
		 */
		int8_t resetCounter(void);

		/*! ***********************************************************************************************************
		 * @brief  enableCounter enable counter to count (enabled by default).
		 * @return 0 in case of success, ERR_NSH_ENCODER_COMMUNICATION otherwise.
		 * ************************************************************************************************************
		 */
		int8_t enableCounter(void);

		/*! ***********************************************************************************************************
		 * @brief  disableCounter disable counter to count.
		 * @return 0 in case of success, ERR_NSH_ENCODER_COMMUNICATION otherwise.
		 * ************************************************************************************************************
		 */
		int8_t disableCounter(void);

		/*! ***********************************************************************************************************
		 * @brief  loadCounterWithValue load counter register with a value. The counter will start count from the
		 *		   loaded value.
		 * @param  liValue value to be loaded.
		 * @return 0 in case of success, ERR_NSH_ENCODER_COMMUNICATION otherwise.
		 * ************************************************************************************************************
		 */
		int8_t loadCounterWithValue(int liValue);

		/*! ***********************************************************************************************************
		 * @brief  getStatus get encoder status.
		 * @param  uEncoderStatus struct where all the bit of the status will be saved.
		 * @return 0 in case of success, ERR_NSH_ENCODER_COMMUNICATION otherwise.
		 * ************************************************************************************************************
		 */
		int8_t getStatus(uSTRReg& uEncoderStatus);

	private:

		/*! ***********************************************************************************************************
		 * @brief initLS7366RRegisters init MDR0 and MDR1 with default values.
		 * @return true in case of success, false otherwise.
		 * ************************************************************************************************************
		 */
		bool initLS7366RRegisters();
};

#endif // LS7366RGENERAL_H
