/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    LS7366RGeneral.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the LS7366RGeneral class.
 @details

 ****************************************************************************
*/

#include "LS7366RGeneral.h"

#define STRINGIFY(x)		#x //to convert x to label memory

LS7366RGeneral::LS7366RGeneral()
{

}

LS7366RGeneral::~LS7366RGeneral()
{

}

bool LS7366RGeneral::init(Log* pLogger, SPIInterface* pSPIIfr)
{
	m_bConfigOk = false;

	if ( pLogger == 0 )	return false;
	if ( pSPIIfr == 0 )	return false;

	m_pLogger = pLogger;
	m_pSPIIfr = pSPIIfr;
	// This is needed, otherwise clear/write/read/load operation in init can't be performed.
	m_bConfigOk = true;
	bool bRes = initLS7366RRegisters();

	if ( !bRes )
	{
		m_bConfigOk = false;
		return false;
	}

	return true;
}

int8_t LS7366RGeneral::readCounter(int& liCnt)
{
	int8_t cRes = readReg(eREAD_CNTR, REGISTER_SIZE_BYTES);
	if ( cRes )
	{
		m_pLogger->log(LOG_ERR, "NSHEncoderGeneral::readCounter: unable to read from encoder");
		return ERR_NSH_ENCODER_COMMUNICATION;
	}
	// m_rgcRxBuf[0] is a dummy value
	liCnt  = ( m_rgcRxBuf[1] << 24);
	liCnt += ( m_rgcRxBuf[2] << 16 );
	liCnt += ( m_rgcRxBuf[3] << 8 );
	liCnt += ( m_rgcRxBuf[4] );

	// If everything is ok clear STR register
//	cRes = clearReg(eCLR_STR);
//	if ( cRes )	return ERR_NSH_ENCODER_COMMUNICATION;

	return 0;
}

int8_t LS7366RGeneral::resetCounter()
{
	int8_t cRes = clearReg(eCLR_CNTR);
	if ( cRes )
	{
		m_pLogger->log(LOG_ERR, "LS7366RGeneral::resetCounter: unable to clear counter to 0");
		return ERR_NSH_ENCODER_COMMUNICATION;
	}
	return cRes;
}

int8_t LS7366RGeneral::enableCounter()
{
	int8_t cRes = writeReg(eWRITE_MDR1, IDX_FLAG|CMP_FLAG|BYTE_4|EN_CNTR, 1);
	if ( cRes )
	{
		m_pLogger->log(LOG_ERR, "LS7366RGeneral::enableCounter: encoder enabled to count");
		return ERR_NSH_ENCODER_COMMUNICATION;
	}
	return cRes;
}

int8_t LS7366RGeneral::disableCounter()
{
	int8_t cRes = writeReg(eWRITE_MDR1, IDX_FLAG|CMP_FLAG|BYTE_4|DIS_CNTR, 1);
	if ( cRes )
	{
		m_pLogger->log(LOG_ERR, "LS7366RGeneral::enableCounter: encoder enabled to count");
		return ERR_NSH_ENCODER_COMMUNICATION;
	}
	return cRes;
}

int8_t LS7366RGeneral::loadCounterWithValue(int liValue)
{
	int8_t cRes = writeReg(eWRITE_DTR, liValue, 4);
	if ( cRes )
	{
		m_pLogger->log(LOG_ERR, "LS7366RGeneral::loadCounterWithValue: unable to write in DTR register");
		return ERR_NSH_ENCODER_COMMUNICATION;
	}

	cRes = loadReg(eLOAD_CNTR);
	if ( cRes )
	{
		m_pLogger->log(LOG_ERR, "LS7366RGeneral::loadCounterWithValue: unable to load value in CNTR register");
		return ERR_NSH_ENCODER_COMMUNICATION;
	}
	return cRes;
}

bool LS7366RGeneral::initLS7366RRegisters()
{
	if ( clearReg(eCLR_CNTR) )
	{
		m_pLogger->log(LOG_ERR, "LS7366RGeneral::initLS7366RRegisters: unable to init encoder");
		return false;
	}

	// init MDR0 register and check if it is initialized correctly
	int foo = QUADRX4|FREE_RUN|INDX_LOADC|SYNCH_INDX|FILTER_2;
	writeReg(eWRITE_MDR0, foo, 1);
	readReg(eREAD_MDR0, 1);
	if ( m_rgcRxBuf[1] != foo )
	{
		m_pLogger->log(LOG_ERR, "LS7366RGeneral::initLS7366RRegisters: unable to init register MDR0 of the encoder");
		return false;
	}
	m_pLogger->log(LOG_INFO, "LS7366RGeneral::initLS7366RRegisters: register MDR0 correctly initialized. Parameters: %s, %s, %s, %s, %s.",
				   STRINGIFY(QUADRX4), STRINGIFY(FREE_RUN), STRINGIFY(SYNCH_INDX), STRINGIFY(INDX_LOADC), STRINGIFY(FILTER_2));

	// init MDR1 register and check if it is initialized correctly
	foo = IDX_FLAG|CMP_FLAG|BYTE_4|EN_CNTR;
	writeReg(eWRITE_MDR1, foo, 1);
	readReg(eREAD_MDR1, 1);
	if ( m_rgcRxBuf[1] != foo )
	{
		m_pLogger->log(LOG_ERR, "LS7366RGeneral::initLS7366RRegisters: unable to init register MDR1 of the encoder");
		return false;
	}
	m_pLogger->log(LOG_INFO, "LS7366RGeneral::initLS7366RRegisters: register MDR1 correctly initialized. Parameters: %s, %s, %s, %s.",
				   STRINGIFY(IDX_FLAG), STRINGIFY(CMP_FLAG), STRINGIFY(BYTE_4), STRINGIFY(EN_CNTR));


	return true;
}

int8_t LS7366RGeneral::getStatus(uSTRReg& uEncoderStatus)
{
	int8_t cRes = readReg(eREAD_STR, 1);
	if ( cRes )
	{
		m_pLogger->log(LOG_ERR, "LS7366RGeneral::resetCounter: unable to clear counter to 0");
		return ERR_NSH_ENCODER_COMMUNICATION;
	}
	uEncoderStatus.ucStatus = m_rgcRxBuf[1];
	m_pLogger->log(LOG_INFO, "LS7366RGeneral::getStatus: sign bit: %d", uEncoderStatus.structStatusReg.ucSignBit);
	m_pLogger->log(LOG_INFO, "LS7366RGeneral::getStatus: counter direction indicator: %d", uEncoderStatus.structStatusReg.ucCountDir);
	m_pLogger->log(LOG_INFO, "LS7366RGeneral::getStatus: power loss indicator: %d", uEncoderStatus.structStatusReg.ucPowerLoss);
	m_pLogger->log(LOG_INFO, "LS7366RGeneral::getStatus: count enable status: %d", uEncoderStatus.structStatusReg.ucCountEnable);
	m_pLogger->log(LOG_INFO, "LS7366RGeneral::getStatus: index latch: %d", uEncoderStatus.structStatusReg.ucLatchIdx);
	m_pLogger->log(LOG_INFO, "LS7366RGeneral::getStatus: compare (CNTR = DTR) latch: %d", uEncoderStatus.structStatusReg.ucCompareLatch);
	m_pLogger->log(LOG_INFO, "LS7366RGeneral::getStatus: borrow(CNTR underflow): %d", uEncoderStatus.structStatusReg.ucBorrowLatch);
	m_pLogger->log(LOG_INFO, "LS7366RGeneral::getStatus: carry (CNTR overflow): %d", uEncoderStatus.structStatusReg.ucCarryLatch);
	return cRes;
}
