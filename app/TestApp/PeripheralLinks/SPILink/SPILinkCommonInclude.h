/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SPILinkCommonInclude.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the SPI classes.
 @details

 ****************************************************************************
*/

#ifndef SPILINKCOMMONINCLUDE_H
#define SPILINKCOMMONINCLUDE_H


// Config keys - NSH reader
#define CFG_SPI_BASE_DIR_1			1
#define CFG_SPI_MODE_1				2
#define CFG_SPI_SPEED_1				3
#define CFG_SPI_BITS_PER_WORD_1		4
#define CFG_SPI_GPIO_IDX_1_1		5
#define CFG_SPI_GPIO_IDX_1_2		6

// Config keys - NSH motor
#define CFG_SPI_BASE_DIR_2			10
#define CFG_SPI_MODE_2				11
#define CFG_SPI_SPEED_2				12
#define CFG_SPI_BITS_PER_WORD_2		13
#define CFG_SPI_GPIO_IDX_2_1		14
#define CFG_MOTOR_CODED_MOVEMENT	15
#define CFG_SPI_GPIO_IDX_2_2		16

// Config keys - NSH encoder
#define CFG_SPI_BASE_DIR_3			20
#define CFG_SPI_MODE_3				21
#define CFG_SPI_SPEED_3				22
#define CFG_SPI_BITS_PER_WORD_3		23
#define CFG_SPI_GPIO_IDX_3			24


/*! *******************************************************************************************************************
 * SPI defines
 * ********************************************************************************************************************
 */
#define SPI_BUS_NAME_3				"/dev/spidev2.0" // "/dev/spidev3.0"


/*! *******************************************************************************************************************
 * NSH Reader management
 * ********************************************************************************************************************
 */
#define NSHR_DEF_SPI_BUS_NAME		"/dev/spidev2.0" // "/dev/spidev1.0"
#define NSHR_DEF_GPIO_IDX_DRDY		"1"
#define NSHR_DEF_GPIO_IDX_RESET		"2"
#define NSHR_DEF_SPI_SPEED_HZ		"1000000" //up to 500000
#define NSHR_DEF_SPI_MODE			"1"
#define NSHR_DEF_SPI_BITS_PER_WORD	"8"

/*! *******************************************************************************************************************
 * NSH Motor management
 * ********************************************************************************************************************
 */
#define NSHM_DEF_SPI_BUS_NAME			"/dev/spidev2.0"
#define NSHM_DEF_GPIO_IDX_BUSYLINE		"1"
#define NSHM_DEF_GPIO_IDX_FAULT_LINE	"4"
#define NSHM_DEF_SPI_SPEED_HZ			"1000000"
#define NSHM_DEF_SPI_MODE				"3"
#define NSHM_DEF_SPI_BITS_PER_WORD		"8"

/*! *******************************************************************************************************************
 * NSH Motor management
 * ********************************************************************************************************************
 */
#define NSHE_DEF_SPI_BUS_NAME		"/dev/spidev2.0"
#define NSHE_DEF_GPIO_IDX_BUSYLINE	"1"
#define NSHE_DEF_SPI_SPEED_HZ		"1000000"
#define NSHE_DEF_SPI_MODE			"0"
#define NSHE_DEF_SPI_BITS_PER_WORD	"8"




#endif // SPILINKCOMMONINCLUDE_H
