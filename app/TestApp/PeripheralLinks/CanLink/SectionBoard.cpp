#include "SectionBoard.h"

SectionBoard::SectionBoard(uint32_t ulIdMaster, uint32_t ulIdSection)
{
    m_ulIdMasterBoard = ulIdMaster;
    m_ulIdSectionBoard = ulIdSection;
    m_canLinkInterface = nullptr;
    m_pMsgQueueLocalAnsw = nullptr;
    m_dataPacketFromSection.stateRxMsg	= CAN_RX_WAIT_STX;
    m_dataPacketFromSection.idxDataBuff = 0;

}

SectionBoard::~SectionBoard()
{
    SAFE_DELETE(m_pMsgQueueLocalAnsw);
}

int SectionBoard::setCanLinkInterface(CanLinkInterface *m_canLink)
{
    if (m_canLink == nullptr)
    {
        log(LOG_ERR,"SectionBoard::setCanLinkInterface-->No CanLinkBoardPresent");
        return -1;
    }

    m_canLinkInterface = m_canLink;

    return 0;
}

int SectionBoard::openCreateMessageQueue(const char * smsgQueueAns)
{
    string mqueeName;

    /*!***************************************
     * MSG QUEUE LOCAL ANSW                  *
     *****************************************/
    SAFE_DELETE(m_pMsgQueueLocalAnsw);
    m_pMsgQueueLocalAnsw = new MessageQueue();
    /* Create message queue for answer from section board */
    bool bRet = m_pMsgQueueLocalAnsw->open(smsgQueueAns, O_RDWR);
    if ( bRet == false )
    {
        bRet = m_pMsgQueueLocalAnsw->create(smsgQueueAns, O_RDWR, LOCAL_MQ_CANBOARD_MAX_MESSAGES, MQ_CANBOARD_MAX_MSG_SIZE);
        if ( bRet == false)
        {
            log(LOG_ERR, "SectionBoard::init--> Unable to create receive MQ%s. Exit.", smsgQueueAns);
            return -1;
        }
    }

    /* message queue pre emptying. It performs a cleaning of remained buffers. */
    bRet = m_pMsgQueueLocalAnsw->empty();
    if ( bRet == false )
    {
        log(LOG_ERR, "SectionBoard::init--> Unable to PRE-EMPTY receive MQ%s. Exit.", smsgQueueAns);
        return -1;
    }

    return 0;
}

int SectionBoard::closeMessageQueue()
{
    /* Close and unlink message queue */
    if (m_pMsgQueueLocalAnsw != NULL)
    {
        bool bRet = m_pMsgQueueLocalAnsw->closeAndUnlink();
        if ( bRet == false )
        {
            log(LOG_ERR,"SectionBoard::closeMessageQueue--> error from mq_unlink");

            return -1;
        }
        else
        {
            log(LOG_INFO,"SectionBoard::closeMessageQueue--> receive m_pMsgQueueLocalAnsw unlinked");
        }

        SAFE_DELETE(m_pMsgQueueLocalAnsw);
    }

    return 0;
}

int SectionBoard::sendToStoreBuffer(can_frame receivedFrame)
{
    log(LOG_DEBUG_PARANOIC,"SectionBoard::sendToStoreBuffer--> send to canBuffer on section:%x", m_ulIdSectionBoard);
    int ret = m_storeBuffer.setData(&receivedFrame);

    return ret;
}

int SectionBoard::ack( void )
{
    int res;
    uint8_t buf[8];
    uint8_t numByteToSend = 3;

    buf[0] = STX;
    buf[1] = ACK_CAN; // ~ 0x7E
    buf[2] = ETX;
    buf[3] = '\0';


    res = m_canLinkInterface->send(m_ulIdMasterBoard, buf, numByteToSend);
    if ( res < 0 )
    {
        //Error
        log(LOG_ERR, "SectionBoardLink::sendMsgAck--> ACK not sent");
        res = -1;
    }
    else
    {
        log(LOG_DEBUG_PARANOIC, "SectionBoardLink::sendMsgAck");
        res = 0;
    }
    return res;
}

int SectionBoard::getInternalStatus( uint8_t &ubStatus, uint16_t usTimeoutMs )
{
    uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
    uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
    memset(pPayLoad, 0, sizeof(pPayLoad));
    memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

    // STX | d | 'i' | ETX
    memset(pPayLoad, 0, sizeof(pPayLoad));

    pPayLoad[0] = SECTION_GENERAL_CHAR;
    pPayLoad[1] = 'i';

    int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
    if ( res == 0 )
    {
        //STX | o | ‘i’ | f  | s0 | ETX
        ubStatus = pucAnsMsg[3];
        log(LOG_DEBUG_PARANOIC, "SectionBoard::getInternalStatus %x --> %i", m_ulIdSectionBoard, ubStatus);
    }
    else
    {
        log(LOG_ERR, "SectionBoard::getInternalStatus-->ERROR idSection", m_ulIdSectionBoard);
        return -1;
    }

    return 0;
}

string SectionBoard::getFirmwareVersion(enumSectionFirmwareVersion eType, uint16_t usTimeoutMs )
{
    uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
    uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
    string strFirmware;

    strFirmware.clear();
    memset(pPayLoad, 0, sizeof(pPayLoad));
    memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

    // STX | d | 'f' | flag | fw_type | fw_string | ETX
    pPayLoad[0] = SECTION_GENERAL_CHAR;
    pPayLoad[1] = 'f';
    pPayLoad[2] = eType;

    int res = sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
    if ( res == 0 )
    {
//        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::getFirmwareVersion %c --> Sent to 0x%x", m_section + SECTION_CHAR_A, m_pSectionBoardLink->getID());
//        m_pLogger->log(LOG_INFO, "SectionBoardGeneral::getFirmwareVersion %c --> Type: %d, Rel: %s", m_section + SECTION_CHAR_A, eType, pucAnsMsg);

        strFirmware.append((const char *)&pucAnsMsg[3]);
    }
    else
    {
//        m_pLogger->log(LOG_ERR, "SectionBoardGeneral::getFirmwareVersion %c -->ERROR", m_section + SECTION_CHAR_A);
    }

    return strFirmware;
}

int SectionBoard::sendCanMsg(uint8_t *pMsg, uint16_t nlen, uint8_t * pucAnsMsg, uint32_t usMillisecToWait)
{
        uint8_t		sInBuffer[MQ_CANBOARD_MAX_MSG_SIZE_BUFF + 1];
        uint8_t msgLen = nlen+2;
        memset(sInBuffer, 0, sizeof(sInBuffer));

        sInBuffer[0] = STX;
        memcpy(&sInBuffer[1], pMsg, nlen);
        sInBuffer[nlen+1] = ETX;

        if (msgLen > 8)
        {
            log(LOG_ERR, "SectionBoard::sendCanMsg-->test msg too long");
        }

        log(LOG_DEBUG, "SectionBoard::sendCanMsg --> %s sent ", sInBuffer);

        int ret = m_canLinkInterface->send( m_ulIdMasterBoard, sInBuffer, msgLen );
        if ( ret == 0 )
        {
            log(LOG_DEBUG_PARANOIC, "SectionBoard::sendCanMsg --> sent ");
        }
        else
        {
            log(LOG_ERR, "SectionBoard::sendCanMsg --> not sent ");
            return -1;
        }

        ret = waitAnswMsg(pMsg[ IDX_CAN_ORIGIN ], pMsg[ IDX_CAN_TYPE_MSG ], pucAnsMsg, usMillisecToWait);

        uint16_t usLen = strlen((char*)pucAnsMsg);
        pucAnsMsg[usLen] = 0;

        if ( ret == 0 )
        {
            //command accepted
            log(LOG_DEBUG_PARANOIC, "SectionBoard::sendCanMsg --> Received msgData %s", pucAnsMsg);
        }
        else
        {

            log(LOG_ERR, "SectionBoard::sendCanMsg --> cmd not accepted");
            return -1;
        }



        ret = waitAnswMsg(pMsg[IDX_CAN_ORIGIN], pMsg[IDX_CAN_TYPE_MSG], pucAnsMsg, usMillisecToWait);

        usLen = strlen((char*)pucAnsMsg);
        pucAnsMsg[usLen] = 0;

        if ( ret == 0 )
        {
            log(LOG_DEBUG_PARANOIC, "SectionBoard::sendCanMsg --> Received msgData %s", pucAnsMsg);
        }
        else
        {
            log(LOG_ERR, "SectionBoard::sendCanMsg --> cmd not executed %s", pucAnsMsg);
            return -1;
        }

    return 0;
}

int SectionBoard::waitAnswMsg( uint8_t devAddr, uint8_t cmdType, uint8_t * pBuffPayLoad, uint32_t ntimeToWait )
{
    char ppayLoad[MQ_CANBOARD_MAX_MSG_SIZE_BUFF];
    unsigned int nMsgPriority;
    int timeoutMsecs;

    if(pBuffPayLoad == 0)
    {
        // buffer not valid ? should not happen ...
        return -4;
    }

    timeoutMsecs = (int)ntimeToWait; // the parameter is in msecs

    int nNumBytesReceived = m_pMsgQueueLocalAnsw->receive((char *)ppayLoad, MQ_CANBOARD_MAX_MSG_SIZE_BUFF, &nMsgPriority, timeoutMsecs);
    if ( nNumBytesReceived == -1 )
    {
        if ( errno == ETIMEDOUT )
        {
            log(LOG_ERR, "SectionBoard::waitAnswMsg--> TIME-OUT on mq_timedreceive() MQ");
        }
        else
        {
            log(LOG_ERR, "SectionBoard::waitAnswMsg--> error from mq_timedreceive() MQ");
        }
        return -1;
    }

    log(LOG_DEBUG_PARANOIC, "SectionBoard::waitAnswMsg --> %s dev=%x cmd=%x", ppayLoad, devAddr, cmdType);


    if (( devAddr == ppayLoad[ IDX_CAN_ORIGIN ] ) && ( cmdType == ppayLoad[ IDX_CAN_TYPE_MSG ] ))
    {
        switch ( ppayLoad[ IDX_CAN_FLAG_MSG ] )
        {
            case CANSTD_ACCEPTED:
            case CANSTD_COMPLETED:
            {
                //command accepted or executed
            }
            break;

            case CANSTD_REFUSED:
            case CANSTD_BADFORMAT:
            default:
            {
                // command not accepted
                m_pLogger->log(LOG_ERR, "SectionBoard::waitAnswMsg ERROR --> %s dev=%c cmd=%c",
                               ppayLoad, devAddr, cmdType);
                return -1;
            }
            break;
        }
        return 0;
    }
    else
    {
        // answer not corresponding
        m_pLogger->log(LOG_ERR, "CanBoardBase::waitAnswMsg--> %s (dev=%c cmd=%c)",
                       pBuffPayLoad, devAddr, cmdType);
        return -2;
    }

    memcpy(pBuffPayLoad, ppayLoad,strlen(ppayLoad));

    return 0;
}

int SectionBoard::parseMsgFromLink( t_canFrame *pcanFrame)
{
    uint8_t	ubLenPayLoad = 0;
    uint8_t ubIdx = 0;
    bool bAckSent = false;

    m_pLogger->log(LOG_DEBUG, "SectionBoard::parseMsgFromLink-->IDrx:%x   IDcp:%x", pcanFrame->Frame.can_id, m_ulIdSectionBoard);
    /* check if the if packet corresponds with the configured section */
    if ( pcanFrame->Frame.can_id == m_ulIdSectionBoard )
    {
        ubLenPayLoad = pcanFrame->Frame.can_dlc;
        m_pLogger->log(LOG_DEBUG, "SectionBoard::parseMsgFromLink-->%s", pcanFrame->Frame.data);

        while ( ubIdx < ubLenPayLoad )
        {
            switch ( m_dataPacketFromSection.stateRxMsg )
            {
                case CAN_RX_WAIT_STX:
                {
                    if ( pcanFrame->Frame.data[ ubIdx ] == STX )
                    {
                        m_dataPacketFromSection.stateRxMsg = CAN_RX_RECV_BODY;
                    }
                }
                break;

                case CAN_RX_RECV_BODY:
                {
                    if (pcanFrame->Frame.data[ ubIdx ] == STX)
                    {
                        /* Reset of state machine */
                        m_dataPacketFromSection.stateRxMsg = CAN_RX_WAIT_STX;
                        m_dataPacketFromSection.idxDataBuff = 0;
                        memset(m_dataPacketFromSection.msgDataBuff, 0, sizeof(m_dataPacketFromSection.msgDataBuff));
                    }
                    else if ( pcanFrame->Frame.data[ ubIdx ] != ETX )
                    {
                        m_dataPacketFromSection.msgDataBuff[m_dataPacketFromSection.idxDataBuff] = pcanFrame->Frame.data[ubIdx];
                        m_dataPacketFromSection.idxDataBuff++;
                    }
                    else
                    {
                        /* ETX character received */
                        m_dataPacketFromSection.msgDataBuff[m_dataPacketFromSection.idxDataBuff] = '\0';
                        memcpy(m_dataPacketFromSection.time_msg, pcanFrame->time_msg, sizeof(pcanFrame->time_msg));

                        // check if the reply is 'I' -> message not correct or unknown
                        if((m_dataPacketFromSection.idxDataBuff == 1) &&
                           (m_dataPacketFromSection.msgDataBuff[0] == 'I') )
                        {
                            m_pLogger->log(LOG_ERR, "SectionBoard::parseMsgFromLink--> I reply");
                            // no need to dispatch the message (but send Ack)
                            ack();
                            bAckSent = true;
                        }
                        else if(bAckSent == false)
                        {
                            // before dispatching the message send Ack to Section to close the communication
                            ack();
                            bAckSent = true;

                            /* Send received data to section */
                            bool bres = m_pMsgQueueLocalAnsw->send( (char*)m_dataPacketFromSection.msgDataBuff, strlen((char*)m_dataPacketFromSection.msgDataBuff) , 0, 1000 );
                            if (bres != true)
                            {
                                m_pLogger->log(LOG_ERR, "SectionBoard::sendToMsgQueueAnsw--> unable to send on m_msgQueueAnsw");
                                return -1;
                            }
                        }

                        /* Reset of state machine */
                        m_dataPacketFromSection.stateRxMsg = CAN_RX_WAIT_STX;
                        m_dataPacketFromSection.idxDataBuff = 0;
                        memset(m_dataPacketFromSection.msgDataBuff, 0, sizeof(m_dataPacketFromSection.msgDataBuff));
                    }
                }
                break;

                default:
                    //Error
                    m_pLogger->log(LOG_ERR, "SectionBoardLink::parseMsgFromLink--> default state machine");
                break;
            }

            /* control the maximum idxdatabuff of msgDataBuff: if it excedes the maximum length retrieves with an error */
            if ( m_dataPacketFromSection.idxDataBuff >= sizeof(m_dataPacketFromSection.msgDataBuff) )
            {
                //Error
                m_pLogger->log(LOG_ERR, "SectionBoard::parseMsgFromLink-->Exceeded maximum data length");
                return -1;
            }

            ubIdx++;
        }
    }
    else
    {
        //Error
        m_pLogger->log(LOG_ERR, "SectionBoard::parseMsgFromLink --> Received ID not correct ");
        return -1;
    }

    /* send ack every received packet */
    if(bAckSent == false)
    {
        ack();
    }
    return 0;
}

void SectionBoard::beforeWorkerThread()
{
    log(LOG_DEBUG_PARANOIC,"SectionBoard--> START Thread");
}

void SectionBoard::afterWorkerThread(void)
{
    log(LOG_DEBUG_PARANOIC, "SectionBoard--> STOP Thread");
}

int SectionBoard::workerThread()
{

    can_frame canFrame;
    t_canFrame  canAppo;

    if (m_canLinkInterface == nullptr)
    {
        m_pLogger->log(LOG_ERR, "SectionBoard::workerThread--> canLinkBoard not present");
        return -1;
    }

    while( isRunning() )
    {                
        int ret = m_storeBuffer.getData(&canFrame);
        if (ret >= 0)
        {
            memcpy(&canAppo.Frame, &canFrame, sizeof(can_frame));
            parseMsgFromLink(&canAppo);
        }
        usleep(200);
    }
    return 0;
}
