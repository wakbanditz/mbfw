#ifndef CANLINKBOARD_H
#define CANLINKBOARD_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#include "CommonInclude.h"
#include "Log.h"
#include "Loggable.h"
#include "CanLinkInterface.h"
#include "MessageQueue.h"
#include "SectionBoard.h"

using namespace std;



class CanLinkBoard : public Loggable,
                     public Thread
{
    public:
        CanLinkBoard();

        /*! ***************************************************************************************************
         * @brief ~CanLinkBoard default destructor
         * ****************************************************************************************************
         */
        virtual ~CanLinkBoard();


        int initCanLinkBoard(bool nSectionAEnabled, bool bSectionBEnabled);

        int stopCanLinkBoard(void);

    protected:
        /*!
         * @brief	workerThread
         *
         *
         * @return 0 if success, -1 otherwise
         */
        int		workerThread( void );

        /*!
         * @brief	beforeWorkerThread	Function to be called before starting the thread
         */
        void	beforeWorkerThread( void );

        /*!
         * @brief	afterWorkerThread	Function to be called after ending the thread
         */
        void	afterWorkerThread( void );

    private:
    public:
        SectionBoard    *sectionA;
        SectionBoard    *sectionB;
    protected:
    private:
        CanLinkInterface    m_canLink;

        bool            m_bSectionAenabled;
        bool            m_bSectionBenabled;


};

#endif // CANLINKBOARD_H
