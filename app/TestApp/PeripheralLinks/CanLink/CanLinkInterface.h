#ifndef CANLINKINTERFACE_H
#define CANLINKINTERFACE_H


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#include "Thread.h"
#include "CommonInclude.h"
//#include "StaticSingleton.h"
#include "Log.h"
#include "Loggable.h"
#include "CanIfr.h"
#include "CanBuffer.h"
#include "MessageQueue.h"

using namespace std;

#define MQ_CAN_LINK_NAME_TESTAPP             "/mq-can-link-local-testapp"

class CanLinkInterface : public Loggable,
//                     public StaticSingleton<CanLinkInterface>,
                     public Thread

{
    public:
        CanLinkInterface();
        /*! ***************************************************************************************************
         * @brief ~CanLink default destructor
         * ****************************************************************************************************
         */
        virtual ~CanLinkInterface();

        int setInterface(bool nSectionAEnabled, bool bSectionBEnabled);

        int receive(t_canFrame &canFrame);

        int send(uint8_t ubId, uint8_t *ubBuff, uint8_t ubLen);

        int openMessageQueue(void);
        int closeMessageQueue(void);

    protected:
        /*!
         * @brief	workerThread
         *
         *
         * @return 0 if success, -1 otherwise
         */
        int		workerThread( void );

        /*!
         * @brief	beforeWorkerThread	Function to be called before starting the thread
         */
        void	beforeWorkerThread( void );

        /*!
         * @brief	afterWorkerThread	Function to be called after ending the thread
         */
        void	afterWorkerThread( void );

    private:


    public:
    protected:
    private:
            CanIfr          *canInterface;
            CanBuffer       canBufferRx;
            MessageQueue    *m_pMsgQueueLocal;
};

#endif // CANLINKINTERFACE_H
