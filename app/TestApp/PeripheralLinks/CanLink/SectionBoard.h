#ifndef SECTIONBOARD_H
#define SECTIONBOARD_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#include "CommonInclude.h"
#include "Loggable.h"
#include "CanIfr.h"
#include "Thread.h"
#include "MessageQueue.h"
#include "CanLinkInterface.h"
#include "CanBuffer.h"

#define MQ_CAN_LINK_NAME_SECTION_A             "/mq-can-link-sectionA"
#define MQ_CAN_LINK_NAME_SECTION_B             "/mq-can-link-sectionB"
#define SCT_MQ_CAN_LINK_NAME_A_TESTAPP_ANSW             "/mq-section-local-a-testapp-answ"
#define SCT_MQ_CAN_LINK_NAME_B_TESTAPP_ANSW             "/mq-section-local-b-testapp-answ"

using namespace std;

/* CAN defines */
#define CAN_SEND_FRAME_WITH_TIMESTAMP	//Only for logging of data timestamp when the socket send the message

#define CANLINKBOARD_TO_MQ_RX				12000

//CAN parameters
#define CANBOARDBASE_TIMEOUT_RECEIVE_MQ		10000

#define STX 0x02
#define ETX 0x03
#define ACK_CAN	0x7E

//Index fields of protocol message
#define IDX_CAN_ORIGIN      0
#define IDX_CAN_TYPE_MSG    1
#define IDX_CAN_NOTIFY_MSG  1
#define IDX_CAN_BODY_MSG    2
#define IDX_CAN_FLAG_MSG    2
#define IDX_CAN_DATA_MSG    3
#define IDX_CAN_ASYNC_MSG   2

//Type flag response
#define	CANSTD_ACCEPTED						'0'
#define	CANSTD_REFUSED						'1'
#define	CANSTD_BADFORMAT					'2'
#define	CANSTD_COMPLETED					'3'
#define	CANSTD_DONEWARNING					'4'
#define	CANSTD_HOMENOTFOUND					'5'
#define	CANSTD_MICROSTEPDRVERR				'6'
#define	CANSTD_MICRODRVCOMFAIL				'7'
#define	CANSTD_I2CFAILCOM					'8'
#define	CANSTD_QSPICFAILCOM					'9'
#define	CANSTD_I2C1FAILCOM					'A'
#define	CANSTD_MICROADERROR					'B'
#define	CANSTD_PCLEEPROMNERVERSET			'C'
#define	CANSTD_PCLEEPROMPARAMCORRUPT		'D'
#define	CANSTD_BALANCEEEPORMNEVERSET		'E'
#define	CANSTD_BALANCEEEPROMPARAMCORRUPT	'F'
#define	CANSTD_MOTORENCERR					'G'
#define	CANSTD_SCTDOOROPEN					'L'
#define	CANSTD_CHECKSUMERR					'M'
#define	CANSTD_CURRENTERR					'Q'
#define	CANSTD_HOMESTEPLOST					'R'
#define	CANSTD_HOMENOTDONE					'S'

typedef enum
{
    CAN_RX_WAIT_STX		= 0,
    CAN_RX_RECV_BODY	= 1,
    CAN_RX_RECV_ETX     = 2
} enumStateReceivedMsg;

typedef enum
{
    CAN_TX_START		= 0,
    CAN_TX_BODY			= 1
} enumStateTxMsg;

typedef struct
{
    uint8_t		msgDataBuff[DIM_SECTIONLINK_MSG_BUFFER_SIZE];
    uint16_t	idxDataBuff;
    char		time_msg[32];
    enumStateReceivedMsg	stateRxMsg;
} structRxMsgData;

typedef enum
{
    eBootloader						= '0',
    eApplication					= '1',
    eFPGAbitstream					= '2',
    eSelector						= '3'

}enumSectionFirmwareVersion;

#define SECTION_CHAR_A			'A'

#define SECTION_PUMP_DEVICE		0
#define SECTION_TOWER_DEVICE	1
#define SECTION_TRAY_DEVICE		2
#define SECTION_SPR_DEVICE		3
#define SECTION_PIB_DEVICE		4
#define SECTION_GENERAL_DEVICE	5

#define SECTION_PUMP_CHAR		(ZERO_CHAR + SECTION_PUMP_DEVICE)
#define SECTION_TOWER_CHAR		(ZERO_CHAR + SECTION_TOWER_DEVICE)
#define SECTION_TRAY_CHAR		(ZERO_CHAR + SECTION_TRAY_DEVICE)
#define SECTION_SPR_CHAR		(ZERO_CHAR + SECTION_SPR_DEVICE)
#define SECTION_PIB_CHAR		(ZERO_CHAR + SECTION_PIB_DEVICE)
#define SECTION_GENERAL_CHAR	(ZERO_CHAR + SECTION_GENERAL_DEVICE)

#define SECTION_PUMP_STRING		"Pump"
#define SECTION_TOWER_STRING	"Tower"
#define SECTION_TRAY_STRING		"Tray"
#define SECTION_SPR_STRING		"SPR"
#define SECTION_PIB_STRING		"PIB"
#define SECTION_GENERAL_STRING	"General"

#define DEVICE_NSH_STRING		"NSH"

#define DEFAULT_REQUEST_DELAY		(5 * 1000)	// default timeout on commands = 5 secs
#define DEFAULT_SETTING_DELAY		(1 * 1000)	// default timeout on commands = 5 secs
#define ABORT_REQUEST_DELAY         (5 * 60 * 1000)  // timeout for Abort = 5 mins

#define STRING_CURRENT_ENABLED	"ENABLED"
#define STRING_CURRENT_DISABLED	"DISABLED"

#define SECTION_DOOR_OPEN_STRING	"OPEN"
#define SECTION_DOOR_CLOSE_STRING	"CLOSED"

#define SLEEP_DISABLE_ENABLE		'1'
#define SLEEP_DISABLE_DISABLE		'0'

#define SET_DISABLE		'1'
#define SET_SLEEP		'0'

enum
{
    SECTION_EVENT_TEMPERATURE = 0,
    SECTION_EVENT_NONE
};

enum
{
    SECTION_ACTION_INIT = 0,
    SECTION_ACTION_REINIT,
    SECTION_ACTION_CMD,
    SECTION_ACTION_EXECUTE,
    SECTION_ACTION_FW_UPDATE,
    SECTION_ACTION_END,
    SECTION_ACTION_NONE
};

enum
{
    MONITOR_PRESSURE_NONE = -1,
    MONITOR_PRESSURE_STOP = 0,
    MONITOR_PRESSURE_START = 1,
    MONITOR_PRESSURE_PROTOCOL_START = 2
};

typedef enum
{
    eDoorUnknown		= -1,
    eDoorOpen	 		= 0,
    eDoorClosed			= 1
} eDoorStatus;

typedef enum
{
    eTemperatureUnknown	= -1,
    eTemperatureHigh 	= 0,
    eTemperatureLow		= 1,
    eTemperatureOk		= 2
} eTemperatureStatus;

typedef enum
{
    eStopAcq				= '0',
    eStartAcq				= '1'
} enumSectionPIBStartStateAcq;

typedef struct
{
    string strType;			//set if it is a calibration request or a position request
    string strComponent;
    string strMotor;
    string strLabel;
    string strValue;

} structSaveCalibration;

class SectionBoard : public Loggable,
                     public Thread
{
    public:

        SectionBoard(uint32_t ulIdMaster, uint32_t ulIdSection);
        virtual ~SectionBoard();

        int setCanLinkInterface(CanLinkInterface *m_canLink);

        int ack(void);

        int  getInternalStatus(uint8_t &ubStatus, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

        string  getFirmwareVersion(enumSectionFirmwareVersion eType, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

        int openCreateMessageQueue(const char * smsgQueueAns);

        int closeMessageQueue(void);

        int sendToStoreBuffer(can_frame receivedFrame);


    protected:
        /*!
         * @brief	workerThread
         *
         *
         * @return 0 if success, -1 otherwise
         */
        int		workerThread( void );

        /*!
         * @brief	beforeWorkerThread	Function to be called before starting the thread
         */
        void	beforeWorkerThread( void );

        /*!
         * @brief	afterWorkerThread	Function to be called after ending the thread
         */
        void	afterWorkerThread( void );


    private:
        int parseMsgFromLink( t_canFrame *pcanFrame);
        int sendCanMsg(uint8_t *pMsg, uint16_t nlen, uint8_t * pucAnsMsg, uint32_t usMillisecToWait);
        int waitAnswMsg( uint8_t devAddr, uint8_t cmdType, uint8_t * pBuffPayLoad, uint32_t ntimeToWait );



    private:
        uint32_t                m_ulIdSectionBoard;
        uint32_t				m_ulIdMasterBoard;
        structRxMsgData			m_dataPacketFromSection;
        CanLinkInterface        *m_canLinkInterface;
        MessageQueue            *m_pMsgQueueLocalAnsw;
        CanBuffer               m_storeBuffer;
};

#endif // SECTIONBOARD_H
