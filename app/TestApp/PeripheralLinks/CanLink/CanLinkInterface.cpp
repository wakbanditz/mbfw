#include "CanLinkInterface.h"

CanLinkInterface::CanLinkInterface()
{
    canInterface = nullptr;
    m_pMsgQueueLocal = nullptr;

    openMessageQueue();
}

CanLinkInterface::~CanLinkInterface()
{
    closeMessageQueue();

    SAFE_DELETE(canInterface);
    SAFE_DELETE(m_pMsgQueueLocal);
}

int CanLinkInterface::setInterface(bool nSectionAEnabled, bool bSectionBEnabled)
{
    uint16_t canIdListSFF[MAX_CAN_ID_LIST];
    uint16_t canIdSizeSFF = 0;
    uint16_t canIdListEFF[MAX_CAN_ID_LIST];
    uint16_t canIdSizeEFF = 0;

    if ( nSectionAEnabled == true )
    {
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_MASTER;	canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_1;canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_2;canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_3;canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_4;canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_5;canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_CHANNEL_6;canIdSizeSFF++;

        canIdListSFF[canIdSizeSFF] = CAN_SECTION_A_BOARD_ID_UPFW;canIdSizeSFF++;
    }

    if ( bSectionBEnabled == true )
    {
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_MASTER;	canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_1;canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_2;canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_3;canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_4;canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_5;canIdSizeSFF++;
        canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_CHANNEL_6;canIdSizeSFF++;

        canIdListSFF[canIdSizeSFF] = CAN_SECTION_B_BOARD_ID_UPFW;canIdSizeSFF++;
    }

    SAFE_DELETE(canInterface);
    canInterface = new CanIfr();
    canInterface->setInterfaceName("can0");
    canInterface->setLogger(getLogger());
    bool bInitOk = canInterface->init(canIdListSFF, canIdSizeSFF, canIdListEFF, canIdSizeEFF);
    if ( bInitOk == false )
    {
        log(LOG_ERR, "CanLinkInterface::setInterface--> initilization failed");
        return -1;
    }
    log(LOG_INFO, "CanLinkInterface::setInterface--> correctly initialized");

    return 0;
}

int CanLinkInterface::receive(t_canFrame &canFrame)
{

    char		sInBuffer[MQ_CANBOARD_MAX_MSG_SIZE_BUFF];
    memset(sInBuffer, 0, sizeof(sInBuffer));
    unsigned int nMsgPriority = 0;

    int nNumBytesReceived = m_pMsgQueueLocal->receive(sInBuffer, MQ_CANBOARD_MAX_MSG_SIZE_BUFF,
                                                     &nMsgPriority, SCT_MQ_TIMEOUT_RX);
    if ( nNumBytesReceived == -1 )
    {
       if ( errno == ETIMEDOUT )
       {
           log(LOG_DEBUG_IPER_PARANOIC, "CanLinkInterface::receive--> TIME-OUT on mq_timedreceive() MQ");
       }
       else
       {
           log(LOG_ERR, "CanLinkInterface::receive--> error from mq_timedreceive() MQ");
       }
       return -1;
    }
    else
    {

       //Message is available, get it
        canFrame = (t_canFrame&)sInBuffer;
        log(LOG_DEBUG, "CanLinkInterface::receive--> received %s", canFrame.Frame.data);

    }

    return nNumBytesReceived;
}

int CanLinkInterface::send(uint8_t ubId, uint8_t *ubBuff, uint8_t ubLen)
{
    int res = canInterface->sendFrame(ubId, ubBuff, ubLen);
    if (res < 0)
    {
        m_pLogger->log(LOG_ERR, "CanLinkInterface: send FAILED  %d %d", res, ubLen);
        return -1;
    }

    return 0;
}

int CanLinkInterface::openMessageQueue()
{

    /*!***************************************
     * MSG QUEUE ANSW                        *
     *****************************************/
    SAFE_DELETE(m_pMsgQueueLocal);
    m_pMsgQueueLocal = new MessageQueue();
    /* Create message queue for answer from section board */
    bool bRet = m_pMsgQueueLocal->open(MQ_CAN_LINK_NAME_TESTAPP, O_RDWR);
    if ( bRet == false )
    {
        bRet = m_pMsgQueueLocal->create(MQ_CAN_LINK_NAME_TESTAPP, O_RDWR, LOCAL_MQ_CANBOARD_MAX_MESSAGES, MQ_CANBOARD_MAX_MSG_SIZE);
        if ( bRet == false)
        {
            m_pLogger->log(LOG_ERR, "CanLinkInterface::init--> Unable to create receive MQ%s. Exit.", MQ_CAN_LINK_NAME_TESTAPP);
            return -1;
        }
    }

    /* message queue pre emptying. It performs a cleaning of remained buffers. */
    bRet = m_pMsgQueueLocal->empty();
    if ( bRet == false )
    {
        m_pLogger->log(LOG_ERR, "CanLinkInterface::init--> Unable to PRE-EMPTY receive MQ%s. Exit.", MQ_CAN_LINK_NAME_TESTAPP);
        return -1;
    }

    return 0;
}

int CanLinkInterface::closeMessageQueue()
{
    /* Close and unlink message queue */
    if (m_pMsgQueueLocal != NULL)
    {
        bool bRet = m_pMsgQueueLocal->closeAndUnlink();
        if ( bRet == false )
        {
            m_pLogger->log(LOG_ERR,"CanLinkInterface::closeMessageQueue--> error from mq_unlink");

            return -1;
        }
        else
        {
            m_pLogger->log(LOG_INFO,"CanLinkInterface::closeMessageQueue--> receive m_pMsgQueueLocal unlinked");
        }

        SAFE_DELETE(m_pMsgQueueLocal);
    }
    return 0;
}

void CanLinkInterface::beforeWorkerThread()
{
    m_pLogger->log(LOG_DEBUG_PARANOIC,"CanLinkInterface--> START Thread");
}

void CanLinkInterface::afterWorkerThread(void)
{
    m_pLogger->log(LOG_DEBUG_PARANOIC, "CanLinkInterface--> STOP Thread");
}

int CanLinkInterface::workerThread()
{
    can_frame  canFrame;
    int nNumRxByte;

    if (canInterface == nullptr)
    {
        log(LOG_ERR, "CanLinkInterface::workerThread-->canInterface not present");
        return -1;
    }

    while( isRunning() )
    {

        nNumRxByte = canInterface->readFrame(&canFrame);
        if ( nNumRxByte < 0 )
        {
            m_pLogger->log(LOG_ERR, "CanLinkInterface::workerThread-->ERR <%s>", strerror(errno));
        }
        else
        {
            bool bres = m_pMsgQueueLocal->send( (char*)&canFrame, sizeof(canFrame) , 0, 1000 );
            if (bres != true)
            {
                m_pLogger->log(LOG_ERR, "CanLinkInterface::workerThread--> unable to send on m_msgQueueAnsw");
                return -1;
            }
            m_pLogger->log(LOG_DEBUG_PARANOIC, "CanLinkInterface::workerThread--> received %s",canFrame.data);
        }

        usleep(200);
    }

    closeMessageQueue();
    return 0;
}


