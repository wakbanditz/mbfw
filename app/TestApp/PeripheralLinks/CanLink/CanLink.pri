INCLUDEPATH += $$PWD/../CanLink/
INCLUDEPATH += $$PWD/CanLink/
INCLUDEPATH += $$PWD/PeripheralLinks/CanLink/

HEADERS += \
    $$PWD/CanBuffer.h \
    $$PWD/CanLinkBoard.h \
    $$PWD/SectionBoard.h \
    $$PWD/CanLinkInterface.h

SOURCES += \
    $$PWD/CanBuffer.cpp \
    $$PWD/CanLinkBoard.cpp \
    $$PWD/SectionBoard.cpp \
    $$PWD/CanLinkInterface.cpp
