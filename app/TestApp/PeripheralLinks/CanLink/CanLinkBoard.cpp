#include "CanLinkBoard.h"

CanLinkBoard::CanLinkBoard()
{
    m_bSectionAenabled = false;
    m_bSectionBenabled = false;

    sectionA = nullptr;
    sectionB = nullptr;
}

CanLinkBoard::~CanLinkBoard()
{

}

int CanLinkBoard::initCanLinkBoard(bool nSectionAEnabled, bool bSectionBEnabled)
{
    int ret = 0;
    /*****************************************************************************
     * SET CAN INTERFACE
     * **************************************************************************/
    m_canLink.setLogger(getLogger());
    ret = m_canLink.setInterface(nSectionAEnabled,bSectionBEnabled);
    if (ret != 0)
    {
        log(LOG_ERR, "CanLinkBoard::initCanLinkBoard-->Unable to set interface");
        return -1;
    }
    ret = m_canLink.openMessageQueue();
    if (ret != 0)
    {
        log(LOG_ERR, "CanLinkBoard::initCanLinkBoard-->Unable to open MessageQueue");
        return -1;
    }
    if (!m_canLink.startThread())
    {
        log(LOG_ERR, "CanLinkBoard::initCanLinkBoard-->Unable to start thread");
        return -1;
    }

    /*****************************************************************************
     * SET CAN BOARD
     * **************************************************************************/

    /*****************************************************************************
     * SECTION BOARD
     * **************************************************************************/
    if (nSectionAEnabled == true)
    {
        SAFE_DELETE(sectionA);
        sectionA = new SectionBoard(CAN_SECTION_A_BOARD_ID_TX, CAN_SECTION_A_BOARD_ID_MASTER);
        sectionA->setLogger(getLogger());
        ret = sectionA->setCanLinkInterface(&m_canLink);
        if (ret != 0)
        {
            log(LOG_ERR, "CanLinkBoard::initCanLinkBoard-->Unable to set interface");
            return -1;
        }

        ret = sectionA->openCreateMessageQueue(SCT_MQ_CAN_LINK_NAME_A_TESTAPP_ANSW);
        if (ret != 0)
        {
            log(LOG_ERR, "CanLinkBoard::initCanLinkBoard-->Unable to open MessageQueue");
            return -1;
        }
        if (!sectionA->startThread())
        {
            log(LOG_ERR, "CanLinkBoard::initCanLinkBoard-->Unable to start thread");
            return -1;
        }
        sectionA->ack();

        m_bSectionAenabled = true;

        log(LOG_INFO, "CanLinkBoard::initCanLinkBoard-->Section A enabled");
    }


    if (bSectionBEnabled == true)
    {
        SAFE_DELETE(sectionB);
        sectionB = new SectionBoard(CAN_SECTION_B_BOARD_ID_TX, CAN_SECTION_B_BOARD_ID_MASTER);
        sectionB->setLogger(getLogger());
        ret = sectionB->setCanLinkInterface(&m_canLink);
        if (ret != 0)
        {
            log(LOG_ERR, "CanLinkBoard::initCanLinkBoard-->Unable to set interface");
            return -1;
        }

        ret = sectionB->openCreateMessageQueue(SCT_MQ_CAN_LINK_NAME_B_TESTAPP_ANSW);
        if (ret != 0)
        {
            log(LOG_ERR, "CanLinkBoard::initCanLinkBoard-->Unable to open MessageQueue");
            return -1;
        }
        if (!sectionB->startThread())
        {
            log(LOG_ERR, "CanLinkBoard::initCanLinkBoard-->Unable to start thread");
            return -1;
        }
        sectionB->ack();

        m_bSectionBenabled = true;

        log(LOG_INFO, "CanLinkBoard::initCanLinkBoard-->Section B enabled");
    }

    return 0;
}

int CanLinkBoard::stopCanLinkBoard()
{
    stopThread();

    if (m_bSectionAenabled == true)
    {
        sectionA->stopThread();
        sectionA->closeMessageQueue();

        SAFE_DELETE(sectionA);

    }

    if (m_bSectionBenabled == true)
    {
        sectionB->stopThread();
        sectionB->closeMessageQueue();

        SAFE_DELETE(sectionB);
    }

    m_canLink.stopThread();
    m_canLink.closeMessageQueue();

    return 0;
}

void CanLinkBoard::beforeWorkerThread()
{
    log(LOG_DEBUG_PARANOIC,"CanLinkBoard--> START Thread");
}

void CanLinkBoard::afterWorkerThread(void)
{
    log(LOG_DEBUG_PARANOIC, "CanLinkBoard--> STOP Thread");
}

int CanLinkBoard::workerThread()
{
    t_canFrame receivedFrame;

    if ((sectionA == nullptr) || (sectionB == nullptr))
    {
        log(LOG_ERR, "CanLinkBoard::workerThread--> received mq not present");
    }


    while( isRunning() )
    {
        if (m_canLink.receive(receivedFrame) > 0)
        {
            log(LOG_DEBUG_PARANOIC, "CanLinkBoard::workerThread--> ID:%i, %s to parse", receivedFrame.Frame.can_id,receivedFrame.Frame.data);

            if (receivedFrame.Frame.can_id == CAN_SECTION_A_BOARD_ID_MASTER)
            {

                sectionA->sendToStoreBuffer(receivedFrame.Frame);
            }
            else if (receivedFrame.Frame.can_id == CAN_SECTION_B_BOARD_ID_MASTER)
            {
                sectionB->sendToStoreBuffer(receivedFrame.Frame);
            }
            else
            {
            }
        }

        usleep(200);
    }
    return 0;
}
