#include <termios.h>
#include <stdio.h>
#include "UCD9090a.h"
#include "TestManager.h"
#include "Log.h"

#include "MainExecutor.h"

TestManager::TestManager()
{
	m_ubTestToExecute	= 0;
	m_ubState			= 0;
	m_config			= 0;
	m_ubTypeCmd			= 0;
}

TestManager::~TestManager()
{

}

void TestManager::initConfig(Config *pconfig)
{
	if (pconfig != NULL)
	{
		m_config = pconfig;
	}
	else
	{
		printf("ERR, No configurations");
	}
}

int TestManager::getkey(void)
{
	int character;
	struct termios orig_term_attr;
	struct termios new_term_attr;

	/* set the terminal to raw mode */
	tcgetattr(fileno(stdin), &orig_term_attr);
	memcpy(&new_term_attr, &orig_term_attr, sizeof(struct termios));
	new_term_attr.c_lflag &= ~(ECHO|ICANON);
	new_term_attr.c_cc[VTIME] = 0;
	new_term_attr.c_cc[VMIN] = 0;
	tcsetattr(fileno(stdin), TCSANOW, &new_term_attr);

	/* read a character from the stdin stream without blocking */
	/*   returns EOF (-1) if no character is available */
	character = fgetc(stdin);

	/* restore the original terminal attributes */
	tcsetattr(fileno(stdin), TCSANOW, &orig_term_attr);

	return character;
}

int TestManager::ctrlExit(void)
{
	int key;
	key = getkey();

	/* terminate loop on ESC (0x1B) or Ctrl-D (0x04) on STDIN */
	if (key == 0x1B || key == 0x04)
	{
		printf("ESC");
		return -1;
	}

	return 0;
}

int TestManager::setTest(void)
{
	printf("--< TEST >--\n");
	printf("a - Power Manager\n");
	printf("b - Gpio Expander Section\n");
	printf("c - Led Driver\n" );
	printf("d - Fan Controller\n");
	printf("e - EEprom\n");
	printf("f - Sections\n");
	printf("g - NSH\n");
	printf("h - Stepper Motor\n" );
	printf("i - Encoder Counter\n");
	printf("l - Camera\n");
	printf("m - Folder Manager\n");
	printf("n - Gpio Expander LED \n");
    printf("o - Codec Audio\n");
    printf("y - Master Update\n");
    printf("S - SleepMode \n");
	printf("x - End\n" );
    printf("z - shutdown\n");

	char sCmd[80];
	scanf("%s", sCmd);

	if (sCmd[0] == 'x')
	{
		return -1;
	}

	m_ubTestToExecute = sCmd[0];

	return 0;
}

int TestManager::setTypeCmd(void)
{
	printf("--< Type CMD >--\n");
	printf("a - Single Cmd\n");
	printf("b - Loop Cmd\n");
	printf("x - End\n" );

	char sCmd[80];
	scanf("%s", sCmd);

	if (sCmd[0] == 'x')
	{
		return -1;
	}

	m_ubTypeCmd = sCmd[0];

	return 0;
}

int TestManager::executeTest(void)
{
	int ret = 0;
	switch(m_ubTestToExecute)
	{
		case 'a':
		{
			// Power Manager
			string strPMEnabled(m_config->GetString(CFG_FW_CORE_POWER_MAN_ENABLE));
			if ( strPMEnabled.compare("true") != 0 )
			{
				log(LOG_INFO, "MainExec::executeTest: Power Manager not enabled");
				return -1;
			}
			else
			{
				ret = testPowerManager();
			}
		}
		break;

		case 'b':
		{
			// GPIO Expander
			string strGEEnabled(m_config->GetString(CFG_FW_CORE_GPIO_EXP_SEC_ENABLE));
			if (strGEEnabled.compare("true") != 0 )
			{
				log(LOG_INFO, "MainExec::executeTest: GPIO Expander not enabled");
				return -1;
			}
			else
			{
				ret = testGpioExpander();
			}
		}
		break;

		case 'c':
		{
			// Led Driver
			string strLedDriverEnabled(m_config->GetString(CFG_FW_CORE_LED_DRV_ENABLE));
			if (strLedDriverEnabled.compare("true") != 0)
			{
				log(LOG_INFO,"MainExec::executeTest: LED DRIVER not enabled");
				return -1;
			}
			else
			{
				ret = testLedDriver();
			}
		}
		break;

		case 'd':
		{
			// Fan Controller
			string strFanControllerEnabled(m_config->GetString(CFG_FW_CORE_FAN_CTRL_ENABLE));
			if (strFanControllerEnabled.compare("true") != 0)
			{
				log(LOG_INFO,"MainExec::executeTest: FAN CONTROLLER not enabled");
				return -1;
			}
			else
			{
				ret = testFanController();
			}
		}
		break;

		case 'e':
		{
			// EEprom
			string strEEEnabled(m_config->GetString(CFG_FW_CORE_EEPROM_ENABLE));
			if ( strEEEnabled.compare("true") != 0 )
			{
				log(LOG_INFO, "MainExec::executeTest: EEprom not enabled");
				return -1;
			}
			else
			{
				ret = testEeprom();
			}
		}
		break;

		case 'f':
		{
			// Sections
			string strSectEnabledA(m_config->GetString(CFG_FW_CORE_SCT_A_ENABLE));
			string strSectEnabledB(m_config->GetString(CFG_FW_CORE_SCT_B_ENABLE));
			if (( strSectEnabledA.compare("true") != 0 ) && ( strSectEnabledB.compare("true") != 0 ))
			{
				log(LOG_INFO, "MainExec::executeTest: sections not enabled");
				return -1;
			}
			else
			{
				ret = testSection();
			}		
		}
		break;

        case 'o':
		{
			// codec Audio
			string strCAudioEnabled(m_config->GetString(CFG_FW_CORE_AUDIO_CODEC_ENABLE));
			if ( strCAudioEnabled.compare("true") != 0 )
			{
				log(LOG_INFO, "MainExec::executeTest: Audio not enabled");
				return -1;
			}
			else
			{
				ret = testCodecAudio();
			}
		}
		break;

		case 'g':
		{
			// NSH
			string strNSHEnabled(m_config->GetString(CFG_FW_CORE_NSH_ENABLE));
			if ( strNSHEnabled.compare("true") != 0 )
			{
				log(LOG_INFO, "MainExec::executeTest: NSH not enabled");
				return -1;
			}
			else
			{
				ret = testNSH();
			}
		}
		break;

		case 'h':
		{
			// Stepper Motor
			string strMotorEnabled(m_config->GetString(CFG_FW_CORE_STEPPER_MOTOR_ENABLE));
			if ( strMotorEnabled.compare("true") != 0 )
			{
				log(LOG_INFO, "MainExec::executeTest: stepper motor not enabled");
				return -1;
			}
			else
			{
				ret = testMotor();
			}
		}
		break;

		case 'i':
		{
			// Encoder counter
			string strEncoderEnabled(m_config->GetString(CFG_FW_CORE_ENCODER_COUNTER_ENABLE));
			if ( strEncoderEnabled.compare("true") != 0 )
			{
				log(LOG_INFO, "MainExec::executeTest: encoder counter not enabled");
				return -1;
			}
			else
			{
				ret = testEncoderCounter();
			}
		}
		break;

		case 'l':
		{
			// Camera CR8062
			string strSectEnabled(m_config->GetString(CFG_FW_CORE_CAM_ENABLE));
			if ( strSectEnabled.compare("true") != 0 )
			{
				log(LOG_INFO, "MainExec::executeTest: camera not enabled");
				return -1;
			}
			else
			{
				ret = testCamera();
			}
		}
		break;

		case 'm':
		{
			// Sections
			string strFolderManEnabled(m_config->GetString(CFG_FW_CORE_FOLDER_MAN_ENABLE));
			if ( strFolderManEnabled.compare("true") != 0 )
			{
				log(LOG_INFO, "MainExec::executeTest: folder manager not enabled");
				return -1;
			}
			else
			{
				ret = testFolderManager();
			}
		}
		break;

		case 'n':
		{
			// GPIO Expander LED
			string strGELEDEnabled(m_config->GetString(CFG_FW_CORE_GPIO_EXP_LED_ENABLE));
			if (strGELEDEnabled.compare("true") != 0 )
			{
				log(LOG_INFO, "MainExec::executeTest: GPIO Expander LED not enabled");
				return -1;
			}
			else
			{
				ret = testGpioExpanderLed();
			}
		}
		break;

        case 'y':
        {
            ret = testMasterUpdate();
            if ( ! ret )
            {
				return -1;
			}
        }
        break;

    case 'S':
    {
        ret = testSleepMode();
        if ( ret == -1 )
        {
            return -1;
        }
    }
    break;

        case 'z':
            {
                // Power Manager
                printf("POWEROFF system\n"); fflush(stdout);
                string strPMEnabled(m_config->GetString(CFG_FW_CORE_POWER_MAN_ENABLE));
                if ( strPMEnabled.compare("true") != 0 )
                {
                    log(LOG_INFO, "MainExec::executeTest: Power Manager not enabled");
                    return -1;
                }
                else
                {
                    msleep(3000);
                    MainExecutor::getInstance()->shutdownInstrument();
                }
            }
            break;

		case 'x':
		default:
			return -1;
		break;
	}

	if (ret != 0)
	{
		return -1;
	}
	return 0;
}

int TestManager::testSleepMode()
{
    printf("a - Section Disable A\n");
    printf("b - Section Enable A\n");
    printf("c - Section Disable B\n");
    printf("d - Section Enable B\n");
    printf("e - Section Disable All\n");
    printf("f - Section Enable All\n");
    printf("g - NSH Off  \n");
    printf("h - NSH ON \n");
    printf("i - StatusLED/Camera Off \n");
    printf("l - StatusLED/Camera On \n");
    printf("m - Motor Off \n");
    printf("n - Motor On \n");
    printf("o - Disable ALL\n");
    printf("p - Enable ALL\n");
    printf("x - Exit \n");


    char sCmd[80];
    scanf("%s", sCmd);

    if (sCmd[0] == 'a')
    {
         MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, false);
         usleep(200);
         printf("SECTION A DISABLED \n");
    }
    else if (sCmd[0] == 'b')
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, true);
        usleep(200);
        printf("SECTION A ENABLED \n");
    }
    else if (sCmd[0] == 'c')
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, false);
        usleep(200);
       printf("SECTION B DISABLED \n");
    }
    else if (sCmd[0] == 'd')
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, true);
        usleep(200);
        printf("SECTION B ENABLED \n");
    }
    else if (sCmd[0] == 'e')
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, false);
        usleep(200);
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, false);
        usleep(200);
        printf("SECTIONs DISABLED \n");
    }
    else if (sCmd[0] == 'f')
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, true);
        usleep(200);
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, true);
        usleep(200);
        printf("SECTIONs ENABLED \n");
    }
    else if (sCmd[0] == 'g')
    {
        printf("RAIL 4  \n");
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setOperationOnlyMode(PM_UCD9090A_RAIL4);

        printf("Operation ON  \n");
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_ON);

        printf("Immediate OFF \n");
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_IMMEDIATE_OFF);


        printf("RAIL 5  \n");
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setOperationOnlyMode(PM_UCD9090A_RAIL5);

        printf("Operation ON  \n");
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_ON);

        printf("Immediate OFF \n");
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_IMMEDIATE_OFF);

        printf("NSH OFF \n");


    }
    else if (sCmd[0] == 'h')
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setControlPinOnlyMode(PM_UCD9090A_RAIL4);

        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setControlPinOnlyMode(PM_UCD9090A_RAIL5);

        printf("NSH ON \n");
    }
    else if (sCmd[0] == 'i')
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_10_FPWM1_GPIO5, false);
        usleep(200);
        printf("STATUS LED/CAMERA OFF \n");
    }
    else if (sCmd[0] == 'l')
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_10_FPWM1_GPIO5, true);
        usleep(200);
        printf("STATUS LED/CAMERA ON \n");
    }
    else if (sCmd[0] == 'm')
    {
        spiBoardLinkSingleton()->m_pNSHMotor->stopMotor();
        usleep(200);
        spiBoardLinkSingleton()->m_pNSHMotor->setHiZ();
        usleep(200);
        spiBoardLinkSingleton()->getMotorState();
        printf("MOTOR OFF \n");
    }
    else if (sCmd[0] == 'n')
    {
        spiBoardLinkSingleton()->m_pNSHMotor->stopMotor();
        usleep(200);
        spiBoardLinkSingleton()->getMotorState();
        printf("MOTOR ON \n");
    }
    else if (sCmd[0] == 'o')
    {
        printf("SLEEP TOTAL MODE \n");

        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, false);
        printf("SECTION A DISABLED \n");fflush(stdout);
        usleep(200);        
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, false);
        printf("SECTION B DISABLED \n");fflush(stdout);
        usleep(200);


        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setOperationOnlyMode(PM_UCD9090A_RAIL4);
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_ON);
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_IMMEDIATE_OFF);
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setOperationOnlyMode(PM_UCD9090A_RAIL5);
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_ON);
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_IMMEDIATE_OFF);
        printf("NSH OFF \n");fflush(stdout);
        usleep(200);

        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_10_FPWM1_GPIO5, false);
        printf("STATUS LED/CAMERA OFF \n");fflush(stdout);
        usleep(200);


        spiBoardLinkSingleton()->m_pNSHMotor->stopMotor();
        usleep(200);
        spiBoardLinkSingleton()->m_pNSHMotor->setHiZ();
        usleep(200);
        spiBoardLinkSingleton()->getMotorState();
        printf("MOTOR OFF \n");fflush(stdout);
        usleep(200);

    }
    else if (sCmd[0] == 'p')
    {
        printf("ENABLE ALL \n");

        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, true);
        printf("SECTION A ENABLED \n");fflush(stdout);
        usleep(200);    

        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, true);
        printf("SECTION B ENABLED \n");fflush(stdout);
        usleep(200);


        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setControlPinOnlyMode(PM_UCD9090A_RAIL4);
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setControlPinOnlyMode(PM_UCD9090A_RAIL5);
        printf("NSH ON \n");fflush(stdout);
        usleep(200);

        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_10_FPWM1_GPIO5, true);
        printf("STATUS LED/CAMERA ON \n");fflush(stdout);
        usleep(200);


        spiBoardLinkSingleton()->m_pNSHMotor->stopMotor();
        usleep(200);
        spiBoardLinkSingleton()->getMotorState();
        usleep(200);
        printf("MOTOR ON \n");fflush(stdout);

    }
    else
    {
        return -1;
    }

    return 0;
}

void TestManager::beforeWorkerThread()
{
	log(LOG_INFO,"TestManager: starting");
}

int TestManager::workerThread()
{
	int ret = 0;

	msleep(1000);

	while( isRunning() )
	{
		switch(m_ubState)
		{
			case 0:
				ret = setTest();
				if (ret != 0)
				{
					return 0;
				}
				else
				{
					m_ubState = 1;
				}
				break;

			case 1:
				ret = setTypeCmd();
				if (ret != 0)
				{
					m_ubState = 0;
				}
				else
				{
					m_ubState = 2;
				}
				break;

			case 2:
				ret = executeTest();
				if (ret != 0)
				{
					m_ubState = 0;
				}
				else
				{
					m_ubState = 2;
				}
				break;

			default:
				break;
		}

		usleep(100);
	}

	log(LOG_INFO,"TestManager::workerThread--> Stop Running Thread");

	for ( auto s:vstrThughLife)
		cout << "\r" << s << endl;

	return 0;
}

void TestManager::afterWorkerThread(void)
{
	log(LOG_INFO,"TestManager: closing");
}
