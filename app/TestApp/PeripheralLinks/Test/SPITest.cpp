#include "SPITest.h"


void testSPI()
{
	printf("\n\n***************************************************************************\n");
	printf("***************          WELCOME TO SPI WORLD           *******************\n");
	printf("***************************************************************************\n");
	printf("\n");
	printf("Press 1 if you want to test the NSH, 2 if you want to test the stepper motor\n 3 for the encoder counter. ");
	printf("If you are in the wrong place, press any other key to exit.\n");
	printf("\n");

	uint8_t usKey;
	cin >> usKey;
	if ( usKey == '1' )
	{
		testNSH();
	}
	else if ( usKey == '2' )
	{
		testMotor();
	}
	else if ( usKey == '3' )
	{
		testEncoderCounter();
	}

	return;
}



int testNSH()
{
	char sCmd[4];

	printf("\n--< NSH CMD LIST >--\n\n");
	printf("a - Get FW Version\n");
	printf("b - Get NSH Status\n");
	printf("c - Reset NSH\n");
	printf("d - Get Fluorescence Reading\n");
	printf("e - Get Air Reading\n");
	printf("f - Read solid standard RFU\n");
	printf("g - Get Last Raw Fluorescence Value\n");
	printf("h - Get Last Raw Reference Value\n");
	printf("i - Restore Default Parameter\n\n");

	scanf("%s", sCmd);

	string strFWversion;
	structNSHStatus NSHstatus;
	structNSHFluoRead fluoRead;
	structNSHLastRFV NSHRfvValues;
	structNSHLastReference NSHRefValues;

	uint8_t ucStripPos = 1;
	int32_t liTmpVal;

	switch (sCmd[0])
	{
		case 'a':
			printf("   a - App Version\n");
			printf("   b - Boot Version\n");
			printf("   c - Selector Version\n");
			printf("   d - Board HW Version\n\n");

			scanf("%s", sCmd);

			switch (sCmd[0])
			{
				case 'a':
					MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getFwVersion(eAppVersion, strFWversion);
				break;

				case 'b':
					MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getFwVersion(eBootVersion, strFWversion);
				break;

				case 'c':
					MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getFwVersion(eSelectorVersion, strFWversion);
				break;

				case 'd':
					MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getFwVersion(eBoardHwVersion, strFWversion);
				break;

				default:
					printf("Wrong key pressed. I'm sorry.\n");
				break;
			}
		break;

		case 'b':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getNSHStatus(&NSHstatus);
		break;

		case 'c':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.swReset();
		break;

		case 'd':
			printf("   a - Model Enabled\n");
			printf("   b - Model Disabled\n");

			scanf("%s", sCmd);

			if ( sCmd[0] == 'a' )
			{
				MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getNSHFluoRead(eNSHModelEnabled, &fluoRead);
			}
			else if ( sCmd[0] == 'b' )
			{
				MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getNSHFluoRead(eNSHModelDisabled, &fluoRead);
			}
			else
			{
				printf("Wrong key pressed. I'm sorry.\n");
			}
		break;

		case 'e':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.readAir(liTmpVal);
		break;

		case 'f':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.readSolidStandardRFU(liTmpVal);
		break;

		case 'g':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getLastFluoRawValuemV(&NSHRfvValues, ucStripPos);
		break;

		case 'h':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getLastRefRawValuemV(&NSHRefValues, ucStripPos);
		break;

		default:
			printf("Wrong key pressed. I'm sorry.\n");
			return -1;
		break;

	}

	return 0;
}


int testMotor(void)
{
	int32_t liPosition = 0;
	uint16_t liParamToBeGet = 0;
	uint16_t uliParamToBeSet = 0;
	uint8_t ucDir = 0;
	char sCmd[4];

	printf("\n--< MOTOR DRIVER CMD LIST >--\n\n");
	printf("a - Get Motor Status\n");
	printf("b - Set Register\n");
	printf("c - Get Register\n");
	printf("d - Move of Relative Steps\n");
	printf("e - Move To Absolute Position\n");
	printf("f - Is Sensor At Home?\n");
	printf("g - Search For Home\n");
	printf("h - Send Reset\n\n");

	scanf("%s", sCmd);

	switch (sCmd[0])
	{
		case 'a':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getMotorStatus();
		break;

		case 'b':
			printf("   a - Set Acceleration\n");
			printf("   b - Set Deceleration\n");
			printf("   c - Set Max Speed\n");
			printf("   d - Set Min Speed\n");
			printf("   e - Set Full Speed\n\n");

			scanf("%s", sCmd);

			switch (sCmd[0])
			{
				case 'a':
					printf("\nPlease insert an acceleration value between 15 and 59590 [step/s^2].\n\n");
					cin >> uliParamToBeSet;

					if ( uliParamToBeSet >= 15 && uliParamToBeSet <= 59590 )
					{
						MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->setAcceleration(uliParamToBeSet);
						printf("\nAcceleration value: %d.\n", uliParamToBeSet);
					}
					else
					{
						printf("\nWRONG INPUT VALUE!\n");
					}
				break;

				case 'b':
					printf("\nPlease insert a deceleration value between 15 and 59590 [step/s^2].\n\n");
					cin >> uliParamToBeSet;

					if (  uliParamToBeSet >= 15 && uliParamToBeSet <= 59590 )
					{
						MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->setDeceleration(uliParamToBeSet);
						printf("\nDeceleration value: %d.\n", uliParamToBeSet);
					}
					else
					{
						printf("\nWRONG INPUT VALUE!\n");
					}
				break;

				case 'c':
					printf("\nPlease insert max speed value between 16 and 15610 [step/s].\n\n");
					cin >> uliParamToBeSet;
					if ( uliParamToBeSet >= 16 && uliParamToBeSet <= 15610 )
					{
						MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->setMaxSpeed(uliParamToBeSet);
						printf("\nMax Speed value: %d.\n", uliParamToBeSet);
					}
					else
					{
						printf("\nWRONG INPUT VALUE!\n");
					}
				break;

				case 'd':
					printf("\nPlease insert min speed value between 0 and 976 [step/s].\n\n");
					cin >> uliParamToBeSet;
					if ( uliParamToBeSet > 0 && uliParamToBeSet <= 976 )
					{
						MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->setMinSpeed(uliParamToBeSet);
						printf("\nMin speed value: %d.\n", uliParamToBeSet);
					}
					else
					{
						printf("\nWRONG INPUT VALUE!\n");
					}
				break;

				case 'e':
					printf("\nPlease insert a full speed value between 8 and 15625.\n\n");
					cin >> uliParamToBeSet;
					if ( uliParamToBeSet >= 8 && uliParamToBeSet <= 15625 )
					{
						MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->setFullSpeed(uliParamToBeSet);
						printf("\nFull speed value: %d.\n", uliParamToBeSet);
					}
					else
					{
						printf("\nWRONG INPUT VALUE!\n");
					}
				break;

				default:
					printf("Wrong key pressed. I'm sorry.\n");
				break;
			}

		break;

		case 'c':
			printf("   a - Get Acceleration\n");
			printf("   b - Get Deceleration\n");
			printf("   c - Get Max Speed\n");
			printf("   d - Get Min Speed\n");
			printf("   e - Get Full Speed\n");
			printf("   f - Get Absolute Position\n\n");

			scanf("%s", sCmd);
			switch (sCmd[0])
			{
				case 'a':
					MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getAcceleration(liParamToBeGet);
					printf("\nAcceleration value: %d.\n", liParamToBeGet);
				break;

				case 'b':
					MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getDeceleration(liParamToBeGet);
					printf("\nDeceleration value: %d.\n", liParamToBeGet);
				break;

				case 'c':
					MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getMaxSpeed(liParamToBeGet);
					printf("\nMax Speed value: %d.\n", liParamToBeGet);
				break;

				case 'd':
					MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getMinSpeed(liParamToBeGet);
					printf("\nMin speed value: %d.\n", liParamToBeGet);
				break;

				case 'e':
					MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getFullSpeed(liParamToBeGet);
					printf("\nFull speed value: %d.\n", liParamToBeGet);
				break;

				case 'f':
					MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getAbsPosition((int&)liParamToBeGet);
					printf("\nAbsolute position value: %d.\n", liParamToBeGet);
				break;

				default:
					printf("Wrong key pressed. I'm sorry.\n");
				break;
			}

		break;

		case 'd':
			printf("Please insert steps number.\n");
			cin >> liPosition;
			printf("Please insert the direction of the motion. 0 for reverse, any other key for forward direction.\n");
			cin >> ucDir;

			if ( ucDir == 0)
				MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->moveRelativeSteps(eReverseDir, liPosition);
			else
				MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->moveRelativeSteps(eForwardDir, liPosition);

			printf("Motion completed.\n");
		break;

		case 'e':
			printf("Please insert the absolute position.\n");
			cin >> liPosition;
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->moveToAbsolutePosition(liPosition);
			printf("Motion completed.\n");
		break;

		case 'f':
			if ( MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->isSensorAtHome() )
				printf("Sensor is at home.\n");
			else
				printf("Sensor is not at home.\n");
		break;

		case 'g':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->searchForHome();
		break;

		case 'h':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->sendReset();
			printf("Reset sent with success.\n");
		break;

		default:
			printf("Wrong key pressed. I'm sorry.\n");
			return -1;
		break;
	}

	return 0;
}


class SPITestThread : public Thread
{

	protected:

		int workerThread()
		{
			char cKey = 0;
			cKey = cin.get();
			cKey = 0;
			while ( cKey != '\n' )
			{
				cKey = cin.get();
			}
			return 0;
		}

		void beforeWorkerThread()
		{
			printf("Start of testEncoder thread\n"); fflush(stdout);
		}

		void afterWorkerThread()
		{
			printf("\rEnd of testEncoder thread\n"); fflush(stdout);
		}

};


int testEncoderCounter()
{
	char sCmd[4];
	int liVal;
	uSTRReg uEncoderStatus;
	SPITestThread thread;

	printf("\n--< ENCODER COUNTER CMD LIST >--\n\n");
	printf("a - Read counter\n");
	printf("b - Reset counter\n");
	printf("c - Enable counter\n");
	printf("d - Disable counter\n");
	printf("e - Load value to counter register\n");
	printf("f - Get status\n");
	printf("g - Monitore dynamically the counter\n\n");

	scanf("%s", sCmd);

	switch (sCmd[0])
	{
		case 'a':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->readCounter(liVal);
			printf("Value Read: %d.\n", liVal);
		break;

		case 'b':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->resetCounter();
			printf("Done.\n");
		break;

		case 'c':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->enableCounter();
			printf("Done.\n");
		break;

		case 'd':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->disableCounter();
			printf("Done.\n");
		break;

		case 'e':
			printf("Please insert the value to be loaded.\n");
			cin >> liVal;
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->loadCounterWithValue(liVal);
			printf("Done.\n");
		break;

		case 'f':
			MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->getStatus(uEncoderStatus);
			printf("Done.\n");
		break;

		case 'g':
			printf("Press ENTER if you want to exit this test.\n");
			if ( !thread.startThread() )
			{
				printf("Impossible to start thread\n"); fflush(stdout);
				break;
			}

			while ( thread.isRunning() )
			{
				MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->readCounter(liVal);
				printf("\rValue Read: %d.", liVal); fflush(stdout);
			}
			thread.stopThread();
			printf("\nKey pressed.\n");
		break;

		default:
			printf("Wrong key pressed. I'm sorry.\n");
			return -1;
		break;
	}

	return 0;
}
