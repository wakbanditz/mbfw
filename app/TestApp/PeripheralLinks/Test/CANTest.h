#ifndef CANTEST_H
#define CANTEST_H

#include <vector>
#include <dirent.h>
#include <fstream>
#include <stdlib.h>     /* atoi */

#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "SectionBoardPIB.h"

#endif // CANTEST_H
