#include "CANTest.h"
#include "TestManager.h"
#include "FwUpgrade.h"

static uint8_t	subState = 0;
static uint8_t	subNumSection = 0;

int TestManager::testMotors( void )
{
	SectionCommonMessage*	pCommonMessage = NULL;
	char string[80];

	printf("\n--<LIST COMPONENT >--\n");

	printf("a - Tower\n");
	printf("b - Pump\n");
	printf("c - Tray\n");
	printf("d - SPR\n");
//	printf("e - PIB\n");
	scanf("%s", string);
	printf("\n");

	//choice of the component to move
	if (string[0] == 'a')
	{
		m_pLogger->log(LOG_INFO,"TOWER");
		pCommonMessage = &MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pTower->m_CommonMessage;
	}
	else if (string[0] == 'b')
	{
		m_pLogger->log(LOG_INFO,"PUMP");
		pCommonMessage = &MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pPump->m_CommonMessage;
	}
	else if (string[0] == 'c')
	{
		m_pLogger->log(LOG_INFO,"TRAY");
		pCommonMessage = &MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pTray->m_CommonMessage;
	}
	else if (string[0] == 'd')
	{
		m_pLogger->log(LOG_INFO,"SPR");
		pCommonMessage = &MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pSPR->m_CommonMessage;
	}
	else
	{
		return -1;
	}

	printf("\n--< CMD LIST TRAY >--\n");
	printf("H - searchHome\n");
	printf("M - moveByRelativeSteps\n");
	printf("c - moveToAbsolutePosition\n");
	printf("a - getAbsolutePosition\n");
	printf("P - moveToCodedPosition\n");
	printf("E - getMotorParameters\n");
	printf("g - getMotorConversionFactor\n");
	printf("o - getOffset\n");
	printf("O - setOffset\n");
	printf("U - setDirectOffset\n");

	printf("x - Exit\n");
	scanf("%s", string);
	printf("\n");

	switch (string[0])
	{
		case 'H':
			{
				m_pLogger->log(LOG_INFO,"HOME");
				pCommonMessage->searchHome(0);
			}
			break;

		case 'M':
			{
				printf("set number relative steps:\n");
				scanf("%s", string);
				uint16_t usRelSteps = atoi(&string[0]);

				pCommonMessage->setNumberRelativeSteps(usRelSteps,0);

				printf("d - Decrease\n");
				printf("i - Increase\n");
				scanf("%s", string);

				if (string[0] == 'd')
				{
					pCommonMessage->startMotor(eDecreasePosition, 0);
				}
				else
				{
					pCommonMessage->startMotor(eIncreasePosition, 0);
				}
			}
			break;

		case 'c':
			{
				printf("set absolute position:\n");
				scanf("%s", string);
				int16_t sAbsPos = atoi(&string[0]);

				pCommonMessage->moveToAbsolutePosition(sAbsPos, 0);
			}
			break;

		case 'a':
			int32_t slValue;
			pCommonMessage->getAbsolutePosition(slValue, 0);
			m_pLogger->log(LOG_INFO, "Get POS TRAY: %d\n", slValue);
			break;

		case 'P':
			{
				printf("set coded position:\n");
				scanf("%s", string);

				pCommonMessage->moveToCodedPosition(string[0],0);
			}
			break;

		case 'E':
			{
				uint8_t ubMicroRes;
				uint16_t usLCurr;
				uint16_t usHCurr;
				uint8_t ubRampFactor;
				pCommonMessage->getMotorParameters(ubMicroRes, usLCurr,	usHCurr, ubRampFactor, 0);
				m_pLogger->log(LOG_INFO, "MOTOR PARAMETERS: Microstep resolution: %d, Low current: %d, High current: %d, Ramp Factor: %d \n",
						ubMicroRes,usLCurr, usHCurr, ubRampFactor );
			}
			break;

		case 'g':
			{
				uint32_t ulMotorConvFactor = 0;
				pCommonMessage->getMotorConversionFactor(ulMotorConvFactor, 0);
				m_pLogger->log(LOG_INFO,"MOTOR CONVERSION FACTOR: %d\n", ulMotorConvFactor);

			}
			break;

		case 'o':
		{
			printf("--<GETOFFSET>--\n");
			printf("\n Insert offset position: \n");

			scanf("%s", string);
			int32_t	slOffset;
			pCommonMessage->getCalibration(string[0], slOffset, 0);
			m_pLogger->log(LOG_INFO,"Offset: %d\n", slOffset);
		}
		break;

		case 'O':
		{
			printf("--<SETOFFSET>--\n");
			printf("\n Insert offset position: \n");

			scanf("%s", string);
			pCommonMessage->saveCalibration(string[0], 0);
		}
		break;

		case 'U':
		{
			printf("--<SETDIRECTOFFSET>--\n");
			printf("coded offset position:\n");
			scanf("%s", string);
			uint8_t ubCodPos = string[0];
			printf("value offset position:\n");
			scanf("%s", string);
			int16_t sAbsPos = atoi(&string[0]);
			pCommonMessage->setDirectCalibration(ubCodPos, sAbsPos, 0);
		}
		break;

		case 'x':
		default:
			return -1;
		break;
	}

	return 0;
}

int TestManager::testFunction( void )
{
	char input[80];
	bool bLoopCmd = true;

	printf("\n--< CMD LIST >--\n");

	printf("a - FW Version\n");
	printf("b - get Voltages\n");
	printf("c - getSensors\n");
	printf("d - getTemperatures\n");
	printf("f - getPressure\n");
	printf("l - \n");
	printf("r - \n");
	printf("x - End Test\n");

	scanf("%s", input);
	printf("%s\n", input);

	switch (input[0])
	{
		case 'a':
		{
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					m_pLogger->log(LOG_INFO,"--<FW VERSION>--");
					MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getFirmwareVersion(eBootloader, 0);
					MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getFirmwareVersion(eApplication, 0);
					MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getFirmwareVersion(eSelector, 0);
					MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getFirmwareVersion(eFPGAbitstream, 0);
					msleep(1000);
				}
			}
			else
			{
				m_pLogger->log(LOG_INFO,"--<FW VERSION>--");
				MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getFirmwareVersion(eBootloader, 0);
				MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getFirmwareVersion(eApplication, 0);
				MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getFirmwareVersion(eSelector, 0);
				MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getFirmwareVersion(eFPGAbitstream, 0);
			}
		}
		break;

		case 'b':
		{

			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					m_pLogger->log(LOG_INFO,"--<FW GETVOLTAGES>--");
					uint16_t	usVoltageP3V3 = 0;
					uint16_t	usVoltageP24V = 0;
					uint16_t	usVoltageP5V = 0;
					uint16_t	usVoltageP1V2 = 0;
					MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getVoltages(0,
																									&usVoltageP24V,
																									&usVoltageP5V,
																									&usVoltageP3V3,
																									&usVoltageP1V2);
					m_pLogger->log(LOG_INFO,"SectionBoardGeneral::getVoltages--> 3V3=%d, 24V=%d, 5V=%d, 1V2=%d",
																								usVoltageP3V3,
																								usVoltageP24V,
																								usVoltageP5V,
																								usVoltageP1V2);
					msleep(1000);
				}
			}
			else
			{
				m_pLogger->log(LOG_INFO,"--<GETVOLTAGES>--");

				uint16_t	usVoltageP3V3 = 0;
				uint16_t	usVoltageP24V = 0;
				uint16_t	usVoltageP5V = 0;
				uint16_t	usVoltageP1V2 = 0;
				MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getVoltages(0,
																								&usVoltageP24V,
																								&usVoltageP5V,
																								&usVoltageP3V3,
																								&usVoltageP1V2);
				m_pLogger->log(LOG_INFO,"SectionBoardGeneral::getVoltages--> 3V3=%d, 24V=%d, 5V=%d, 1V2=%d",
																							usVoltageP3V3,
																							usVoltageP24V,
																							usVoltageP5V,
																							usVoltageP1V2);
			}
		}
		break;

		case 'c':
		{
				while (bLoopCmd == true)
				{
					m_pLogger->log(LOG_INFO,"--<GETSENSOR>--");
					MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getSensors(0);
					m_pLogger->log(LOG_INFO,"State Sensors HOME PUMP: %d",
								  MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getSensorPump() );
					m_pLogger->log(LOG_INFO,"State Sensors HOME TOWER: %d",
								   MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getSensorTower() );
					m_pLogger->log(LOG_INFO,"State Sensors HOME TRAY: %d",
								   MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getSensorTray() );
					bool homeSpr;
					MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pSPR->getSensor(homeSpr, 0);
					m_pLogger->log(LOG_INFO,"State Sensors HOME SPR: %d", homeSpr);

					if (m_ubTypeCmd == 'b')
					{
						int res = ctrlExit();
						if (res != 0)
						{
							bLoopCmd = false;
						}
						else
						{
							msleep(1000);
						}
					}
					else
					{
						bLoopCmd = false;
					}
			}
		}
		break;

		case 'd':
		{
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					m_pLogger->log(LOG_INFO,"--<GETTEMPERATURE>--");
					uint16_t usSPRTemp = 0;
					uint16_t usTrayTemp = 0;
					MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getTemperature(usSPRTemp, usTrayTemp, 0);
					m_pLogger->log(LOG_INFO,"SPRTemp:%d, TrayTemp:%d", usSPRTemp, usTrayTemp);
					msleep(1000);
				}
			}
			else
			{
				m_pLogger->log(LOG_INFO,"--<GETTEMPERATURE>--");
				uint16_t usSPRTemp = 0;
				uint16_t usTrayTemp = 0;
				MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pGeneral->getTemperature(usSPRTemp, usTrayTemp, 0);
				m_pLogger->log(LOG_INFO,"SPRTemp:%d, TrayTemp:%d", usSPRTemp, usTrayTemp);
			}
		}
		break;

		case 'f':
		{

			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					m_pLogger->log(LOG_INFO,"--<GETPRESSURE>--");
					int32_t slValPressure[6];
					MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pPIB->getPressure(
							slValPressure[0],
							slValPressure[1],
							slValPressure[2],
							slValPressure[3],
							slValPressure[4],
							slValPressure[5], 0);

					m_pLogger->log(LOG_INFO,"Press1:%d, Press2:%d, Press3:%d, Press4:%d, Press5:%d, Press6:%d,",
							slValPressure[0],
							slValPressure[1],
							slValPressure[2],
							slValPressure[3],
							slValPressure[4],
							slValPressure[5]);

					int32_t slADC[6];
					MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pPIB->getADCData(
							slADC[0],
							slADC[1],
							slADC[2],
							slADC[3],
							slADC[4],
							slADC[5], 0);

					m_pLogger->log(LOG_INFO,"ADC1:%d, ADC2:%d, ADC3:%d, ADC4:%d, ADC5:%d, ADC6:%d,",
							slADC[0],
							slADC[1],
							slADC[2],
							slADC[3],
							slADC[4],
							slADC[5]);
					msleep(1000);
				}
			}
			else
			{
				m_pLogger->log(LOG_INFO,"--<GETPRESSURE>--");
				int32_t slValPressure[6];
				MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pPIB->getPressure(
						slValPressure[0],
						slValPressure[1],
						slValPressure[2],
						slValPressure[3],
						slValPressure[4],
						slValPressure[5], 0);

				m_pLogger->log(LOG_INFO,"Press1:%d, Press2:%d, Press3:%d, Press4:%d, Press5:%d, Press6:%d,",
						slValPressure[0],
						slValPressure[1],
						slValPressure[2],
						slValPressure[3],
						slValPressure[4],
						slValPressure[5]);

				int32_t slADC[6];
				MainExecutor::getInstance()->m_sectionBoardLink[subNumSection].m_pPIB->getADCData(
						slADC[0],
						slADC[1],
						slADC[2],
						slADC[3],
						slADC[4],
						slADC[5], 0);

				m_pLogger->log(LOG_INFO,"ADC1:%d, ADC2:%d, ADC3:%d, ADC4:%d, ADC5:%d, ADC6:%d,",
						slADC[0],
						slADC[1],
						slADC[2],
						slADC[3],
						slADC[4],
						slADC[5]);
			}
		}
		break;

		case 'x':
		default:
		{
			return -1;
		}
		break;
	}

//SET VOLUME KFACTOR
//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->setVolumeKFactor(5, 1000);
//		uint32_t ulKfactor;
//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getVolumeKFactor(ulKfactor, 1000);
//		l->log(LOG_INFO, "Value of KFactor: %d", ulKfactor);

//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getNTCParameters(1000);
//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->enterMaintenanceState(1000);
//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->exitMaintenanceState(1000);
//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTableVersion(eAspirationLowThresholdTable, 1000);
//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTable(eAspirationLowThresholdTable, 0, 1000);


// PROVA SCRITTURA TABELLA
//		SectionEepromTable prova;
//		uint32_t ubBuff[36] = {
//		2001, 2201, 2401, 2601, 2701, 2801,
//		2901, 3001, 3101, 3101, 2301, 2301,
//		2301, 2301, 2301, 1901, 2301, 2301,
//		2301, 2001, 2301, 2101, 2201, 2301,
//		2401, 2501, 2701, 2301, 2301, 2301,
//		2301, 2301, 2301, 2301, 2301, 2301 };

//		for (int j=0; j<SECTION_EEPROM_TABLE_SIZE; j++)
//		{
//			for (int i=0; i<SECTION_EEPROM_TABLE_SIZE; i++)
//			{
//				prova.setValAspirationTsTable( eAspirationLowClinicThresholdTable, ubBuff[i], j, i );
//			}
//		}
//		uint8_t Version[6]={0,1,2,3,4,5};
//		prova.setVersion(eAspirationLowClinicThresholdTable, Version);
//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->sendTable(eAspirationLowClinicThresholdTable, eTableVersion, 0, prova, 1000);

//		msleep(1000);
//		for (int i = 0; i<SECTION_EEPROM_TABLE_SIZE; i++)
//		{
//			MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->sendTable(eAspirationLowClinicThresholdTable, eRow, i, prova, 3000);
//			msleep(1000);
//		}

//		msleep(1000);

//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTableVersion(eAspirationLowClinicThresholdTable, 1000);

//		msleep(1000);

//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTable(eAspirationLowClinicThresholdTable, 0, 1000);

//		msleep(1000);

//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTable(eAspirationLowClinicThresholdTable, 1, 1000);

//		msleep(1000);

//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTable(eAspirationLowClinicThresholdTable, 2, 1000);

//		msleep(1000);

//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTable(eAspirationLowClinicThresholdTable, 3, 1000);

//		msleep(1000);

//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTable(eAspirationLowClinicThresholdTable, 4, 1000);

//		msleep(1000);

//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTable(eAspirationLowClinicThresholdTable, 5, 1000);

// PROVA LETTURA TABELLA Threshold Low
//		for (int i ; i<36; i++)
//		{
//			MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTable(eAspirationLowThresholdTable, i, 1000);
//		}
//		msleep(2000);

// PROVA LETTURA TABELLA Volume
//		int32_t slVolumeProva[SECTION_EEPROM_TABLE_SIZE] = {
//			  0,  1,  2,  3,  4,  5,
//			  6,  7,  8, 10, 11, 13,
//			 15, 17, 20, 22, 27, 33,
//			 38, 44, 54, 65, 87,109,
//			151,201,251,301,351,400,
//			449,499,548,597,647,696
//		};

//		VALUE TABLE
//		for (int i=0; i<SECTION_EEPROM_TABLE_SIZE; i++)
//		{
//			prova.setValVolumes(slVolumeProva[i], i );
//		}

//		msleep(2000);

/* PROVA LETTURA TABELLA Threshold High */
//		for (int i ; i<36; i++)
//		{
//			MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTable(eAspirationHighThresholdTable, i, 1000);
//		}
//		msleep(2000);

// PROVA LETTURA TABELLA Speed
//		for (int i ; i<36; i++)
//		{
//			MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->getTable(eSpeedTable, i, 1000);
//		}
//		msleep(2000);

// PROVA RESTORE EEPROM STATE
//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->restoreDefaultEepromState(1000);
//		msleep(1000);

// READ EEPROM Parameter
//		MainExecutor::getInstance()->m_sectionBoard[SCT_B_ID].m_pGeneral->readEepromParameter()
	return 0;
}

int TestManager::testSection(void)
{
	char input[80];
	int res = 0;


	switch(subState)
	{
		case 0:

			printf("\n--< TEST SECTION >--\n");
			printf("Num Section [0,1]\n");
			printf("x to exit\n");
			scanf("%s", input);

			if ((input[0] == 'x') ||
				((input[0] != '0') && (input[0] != '1')))
			{
				return -1;
			}

			subNumSection = atoi(&input[0]);

			printf("1 - TestFunction\n");
			printf("2 - TestMotors\n");
			printf("3 - TestFwUpdate\n");
			scanf("%s", input);

			subState = atoi(&input[0]);

		break;

		case 1:

			res = testFunction();
			if (res != 0)
			{
				subState = 0;
			}

		break;

		case 2:

			res = testMotors();
			if (res != 0)
			{
				subState = 0;
			}

		break;

		case 3:
		{
			/*!**************************TEST FWUPGRADE*****************************************************************************/
				FwUpgrade fwUpgrade;
				fwUpgrade.setLogger(MainExecutor::getInstance()->getLogger());
				uint8_t	NumSectionToTest = subNumSection;
				fwUpgrade.setSectionLink(&(MainExecutor::getInstance()->m_sectionBoardLink[NumSectionToTest]));
				msleep(100);
				printf("TEST FWUPDATE\n");
				printf("Section %d\n", NumSectionToTest);

				string sourcePath = SCT_UPG_PATH;
                fwUpgrade.updateFirmwareCan(eApplicative, sourcePath,0,0);
				subState = 0;

			/*!*********************************************************************************************************************/
		}
		break;

		default:
			return -1;
		break;
	}
	return 0;
}
