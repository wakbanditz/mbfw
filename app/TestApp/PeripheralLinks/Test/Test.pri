INCLUDEPATH += $$PWD/../Test/

HEADERS += \
    $$PWD/CANTest.h \
    $$PWD/MasterUpdateTest.h \
    $$PWD/SPITest.h \
    $$PWD/USBTest.h \
    $$PWD/TestManager.h \
    $$PWD/FolderManagerTest.h

SOURCES += \
    $$PWD/I2CTest.cpp \
    $$PWD/CANTest.cpp \
    $$PWD/MasterUpdateTest.cpp \
    $$PWD/SPITest.cpp \
    $$PWD/USBTest.cpp \
    $$PWD/TestManager.cpp \
    $$PWD/FolderManagerTest.cpp

    

