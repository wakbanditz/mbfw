#include "I2CLinkBoard.h"
#include "UCD9090a.h"
#include "PCA9633.h"
#include "PCA9955B.h"
#include "ADT7470.h"
#include "AT24C256C.h"
#include "TLV320aic31xx.h"
#include "PCA9537.h"
#include "TestManager.h"
#include "MainExecutor.h"

#define ESC		27

int TestManager::testPowerManager( void )
{
	string	sID;
	bool	bLoopCmd = true;

	printf("\n--< CMD LIST >--\n");
	printf("a - printConf\n");
	printf("b - getGpioConfig\n");
	printf("c - readDeviceID\n" );
	printf("d - readMonitorConfig\n");
	printf("e - clear faults\n");
    printf("E - clear Logged faults\n");
	printf("f - setGPIO\n");
	printf("g - writeSeqConfig\n");
	printf("l - set&writeMonPinConfig\n");
	printf("m - UserRAM\n");
	printf("p - setPage \n");
	printf("s - storeDefaultAll\n");
	printf("r - resetSoft\n" );
	printf("t - readTemperature \n");
	printf("v - readVOUT\n");
    printf("u - readStatusREgister\n");
    printf("S - sleepMode\n");
	printf("x - Exit\n" );

	char sCmd[80];
	scanf("%s", sCmd);

	switch (sCmd[0])
	{
		case 'a':
			printf("a - printGpioConfig\n");
			printf("b - printRailConfig\n" );
			printf("c - printMonConfig\n" );
			scanf("%s", sCmd);

			if (sCmd[0] == 'a')
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->printGpioConfig();
			}
			else if (sCmd[0] == 'b')
			{
				printf("MON : 1==Voltage, 2==Temp \n" );
				MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->printRailConfig();
			}
			else if (sCmd[0] == 'c')
			{
				printf("MON : 1==Voltage, 2==Temp \n" );
				MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->printPinMonConfig();
			}
			else
			{
				printf("No print conf \n");
			}
			break;

		case 'b':

            /* GPIO CONFIG */
			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->getGpioConfig();
			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->printGpioConfig();

			break;

		case 'c':
			/* DEVICE ID */
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					sID.clear();
					MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readDeviceID(sID);
					printf(">>>>>Device ID: %s \n", sID.c_str());

					msleep(100);
				}
			}
			else
			{
				sID.clear();
				MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readDeviceID(sID);
				printf(">>>>>Device ID: %s \n", sID.c_str());

			}
			break;

		case 'd':
			/* MON CONFIG */
			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readMonitorConfig();
			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->printPinMonConfig();

			break;

		case 'e':
			/* CLEAR FAULTS */
			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeClearFaults();
			printf("RESET FAULTS done \n");
			break;

        case 'E':
            /* CLEAR FAULTS */
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeLoggedFaults();
            printf("CLEAR LOGGED FAULTS done \n");
            break;

		case 'f':
			uint8_t ubNumRail;
			uint8_t	ubDir;
			uint8_t ubVal;

			/* SET GPIO*/
			printf("SelectGPIO [0-22]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubNumRail = atoi((char*)&sCmd[0]);

			printf("Select Direction [1-Output 0-Input]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubDir = atoi((char*)&sCmd[0]);

			printf("Select Value [0-1]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubVal = atoi((char*)&sCmd[0]);


            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setGPIO(ubNumRail, (eUCD9090aGpioOutEnable)ubDir, (eUCD9090aGpioOutValue)ubVal);
			break;

		case 'g':
			uint8_t ubRail;
			stEnablePinConfig Pin;

			printf("Write SeqConfig  \n" );
			printf("Set Rail: ");
			scanf("%s", sCmd);
			printf("\n");
			ubRail = atoi((char*)&sCmd[0]);

			printf("ID ");
			scanf("%s", sCmd);
			Pin.ubPinIDDef = atoi((char*)&sCmd[0]);
			printf("\n");

			printf("Pol ");
			scanf("%s", sCmd);
			Pin.ubPolarity = atoi((char*)&sCmd[0]);
			printf("\n");

			printf("Mode ");
			scanf("%s", sCmd);
			Pin.ubMode = atoi((char*)&sCmd[0]);
			printf("\n");

			printf("Rail: %d", ubRail);
			printf("Cfg:  %d-%d-%d    \n",Pin.ubPinIDDef, Pin.ubPolarity, Pin.ubMode);

			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeSeqConfig(ubRail, Pin);
			break;

		case 'l':
			uint8_t ubNumPinId;
			uint8_t ubMonType;
			uint8_t ubPageByte;

			/* SET MON PIN CONFIG*/
			printf("Set PinID: <NumPinID>  \n" );
			scanf("%s", sCmd);
			ubNumPinId = atoi(sCmd);
			printf("Set Type: <Type>  \n" );
			scanf("%s", sCmd);
			ubMonType = atoi(sCmd);
			printf("Set Page: <Page>  \n" );
			scanf("%s", sCmd);
			ubPageByte = atoi(sCmd);

			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPinMonitorConfigRAM(ubNumPinId, ubMonType, ubPageByte);

			/* WRITE MON PIN CONFIG */
			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writePinMonitorConfig();
			break;

		case 'm':
			/* USER RAM */
			printf("USER RAM [r/w]: " );
			scanf("%s", sCmd);
			printf("\n");

			if (sCmd[0] == 'w')
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeUserRAM(0x01);
			}
			else
			{
				if (m_ubTypeCmd == 'b')
				{
					while (bLoopCmd == true)
					{
						int res = ctrlExit();
						if (res != 0)
						{
							bLoopCmd = false;
							break;
						}

						uint8_t ubVal;
						MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readUserRAM(ubVal);
						printf(">>>>>USER RAM: %d \n", ubVal);

						msleep(100);
					}
				}
				else
				{
					uint8_t ubVal;
					MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readUserRAM(ubVal);
					printf(">>>>>USER RAM: %d \n", ubVal);

				}
			}
			break;

		case 'p':
			uint8_t ubPage ;
			/* SET PAGE */
			printf("Select Page: " );
			scanf("%s", sCmd);
			printf("\n");

			ubPage = atoi(sCmd);
			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeSetPage(ubPage);
			break;

		case 'r':

			/* RESET SOFT */
			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeResetSoft();
			break;

		case 's':
			/* STORE DEFAULT ALL */
			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeStoreDefaultAll();
			break;

		case 't':
			/* TEMP_1 */
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					float fTemp;
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readTemperature(0, 1, fTemp);
					m_pLogger->log(LOG_INFO,"Read Temp1 Power Manager: %f °C", fTemp);
				}
				msleep(500);
			}
			else
			{
				float fTemp;
                MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readTemperature(0, 1, fTemp);
				m_pLogger->log(LOG_INFO,"Read Temp1 Power Manager: %f °C", fTemp);
			}
			break;

		case 'v':
			/* READ_VOUT */
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					float fV;
					MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readVout(fV);
					printf("Read Vout Power Manager: %f V", fV);
				}
				msleep(500);
			}
			else
			{
				float fV;
				MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readVout(fV);
				printf("Read Vout Power Manager: %f V", fV);
			}
			break;

        case 'u':
            {
                uint32_t ulTemp;
                MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readStatusRegister(ulTemp);
                printf("Read Status Register: 0x%X \n", ulTemp);

                //Test with GPI3-->shutdown pin
                if (ulTemp & MASK_MFR_STATUS_GPI3)
                {
                    printf("GPI3 Asserted\n");
                }
            }
            break;

        case 'S':
            {
                printf("a - Section Enable A\n");
                printf("b - Section Disable A\n");
                printf("c - Section Enable B\n");
                printf("d - Section Disable B\n");
                printf("e - Section Enable All\n");
                printf("f - Section Disable All\n");
                printf("g - NSH Off  \n");
                printf("h - NSH ON \n");
                scanf("%s", sCmd);

                if (sCmd[0] == 'a')
                {
                     MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, true);
                     usleep(200);
                     printf("POWER GOOD ON \n");
                }
                else if (sCmd[0] == 'b')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, false);
                    usleep(200);
                    printf("POWER GOOD OFF \n");
                }
                else if (sCmd[0] == 'c')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, true);
                    usleep(200);
                   printf("POWER GOOD ON \n");
                }
                else if (sCmd[0] == 'd')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, false);
                    usleep(200);
                    printf("POWER GOOD OFF \n");
                }
                else if (sCmd[0] == 'e')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, true);
                    usleep(200);
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, true);
                    usleep(200);
                    printf("POWER GOOD ON \n");
                }
                else if (sCmd[0] == 'f')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, false);
                    usleep(200);
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, false);
                    usleep(200);
                    printf("POWER GOOD OFF \n");
                }
                else if (sCmd[0] == 'g')
                {
                    printf("OPERATION ONLY MODE \n");

                    printf("RAIL 4  \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setOperationOnlyMode(PM_UCD9090A_RAIL4);

                    printf("Operation ON  \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_ON);

                    printf("Immediate OFF \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_IMMEDIATE_OFF);


                    printf("RAIL 5  \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setOperationOnlyMode(PM_UCD9090A_RAIL5);

                    printf("Operation ON  \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_ON);

                    printf("Immediate OFF \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_IMMEDIATE_OFF);

                }
                else if (sCmd[0] == 'h')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setControlPinOnlyMode(PM_UCD9090A_RAIL4);

                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setControlPinOnlyMode(PM_UCD9090A_RAIL5);

                    printf("CONTROL PIN MODE \n");
                }
            }
            break;

		case 'x':
		default:
			return -1;
			break;
	}

	return 0;
}

int TestManager::testGpioExpander( void )
{
	uint8_t ubGpio = 0;
	uint8_t ubBright = 0;
	uint8_t ubPWM = 0;
	uint8_t	ubAppo = 0;
	bool	bLoopCmd = true;

	printf("\n--< CMD LIST >--\n");
	printf("a - setPWM A\n" );
	printf("b - getPWM A\n" );
	printf("c - setLEDOUT A\n");
	printf("d - getLEDOUT A\n");
	printf("r - resetSoft A\n");
	printf("f - setPWM B\n" );
	printf("g - getPWM B\n" );
	printf("h - setLEDOUT B\n");
	printf("i - getLEDOUT B\n");
	printf("l - resetSoft B\n");
	printf("m - resetSectionA\n");
	printf("n - resetSectionB\n");
	printf("x - Exit\n" );

	char sCmd[80];
	scanf("%s", sCmd);

	switch(sCmd[0])
	{
		case 'a':
			printf("Select GPIO/LED[0-3]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubGpio = atoi((char*)&sCmd[0]);

			printf("Enter brightness[0-255]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubBright = atoi((char*)&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->writePWM(ubGpio, ubBright);
			break;

		case 'b':
			printf("Select GPIO/LED[0-3]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubPWM = atoi((char*)&sCmd[0]);
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->readPWM(ubPWM, ubBright);
					printf("Brightness:%d \n", ubBright);

					msleep(200);
				}
			}
			else
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->readPWM(ubPWM, ubBright);
				printf("Brightness:%d \n", ubBright);
			}
			break;

		case 'c':
			printf("Select GPIO/LED[0-3]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubGpio = atoi((char*)&sCmd[0]);

			printf("Enter setting[0-3]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubAppo = atoi((char*)&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->writeLedOut(ubGpio, ubAppo);
			break;

		case 'd':			
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->readLedOut(ubAppo);
					printf("LEDOUT state: %d \n", ubAppo);

					msleep(200);
				}
			}
			else
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->readLedOut(ubAppo);
				printf("LEDOUT state: %d \n", ubAppo);
			}
			break;

		case 'r':
			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->resetSoft();
			printf("RESET DONE \n");
			break;

		case 'f':
			printf("Select GPIO/LED[0-3]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubGpio = atoi((char*)&sCmd[0]);

			printf("Enter brightness[0-255]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubBright = atoi((char*)&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->writePWM(ubGpio, ubBright);
			break;

		case 'g':
			printf("Select GPIO/LED[0-3]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubPWM = atoi((char*)&sCmd[0]);
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->readPWM(ubPWM, ubBright);
					printf("Brightness:%d \n", ubBright);

					msleep(200);
				}
			}
			else
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->readPWM(ubPWM, ubBright);
				printf("Brightness:%d \n", ubBright);
			}
			break;

		case 'h':
			printf("Select GPIO/LED[0-3]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubGpio = atoi((char*)&sCmd[0]);

			printf("Enter setting[0-3]: ");
			scanf("%s", sCmd);
			printf("\n");

			ubAppo = atoi((char*)&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->writeLedOut(ubGpio, ubAppo);
			break;

		case 'i':
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->readLedOut(ubAppo);
					printf("LEDOUT state: %d \n", ubAppo);

					msleep(200);
				}
			}
			else
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->readLedOut(ubAppo);
				printf("LEDOUT state: %d \n", ubAppo);
			}
			break;

		case 'l':
			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->resetSoft();
			printf("RESET DONE \n");
			break;

		case 'm':
			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->writeLedOut(1);
			msleep(500);
			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->writeLedOut(0);
			printf("RESET SECTION A DONE \n");
			break;

		case 'n':
			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->writeLedOut(1);
			msleep(500);
			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->writeLedOut(0);
			printf("RESET SECTION B DONE \n");
			break;

		case 'x':
		default:
			return -1;
			break;
	}

	return 0;
}

int TestManager::testGpioExpanderLed( void )
{
//	uint8_t ubConf = 0x00;
//	bool	bLoopCmd = true;

	printf("\n--< CMD LIST >--\n");
	printf("a - write Config Register\n" );
	printf("b - write Output Register\n" );
	printf("c - read Output Register\n");
	printf("d - read Input Register\n");
	printf("e - read Configuration Register \n");
	printf("f - init\n");
	printf("x - Exit\n");

	char sCmd[80];
	scanf("%s", sCmd);

	switch(sCmd[0])
	{
		case 'a':
		{
			uint8_t ubConf = 0x00;
			printf("CONFIG Register: \n");
			scanf("%s", sCmd);
			printf("\n");

			ubConf = atoi(sCmd);

			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderLed->writeConfigurationRegister(ubConf);
			printf("Value 0x%X set\n", ubConf);
		}
		break;

		case 'b':
		{
			uint8_t ubConf = 0x00;
			printf("OUTPUT Register: \n");
			scanf("%s", sCmd);
			printf("\n");

			ubConf = atoi(sCmd);

			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderLed->writeOutputPortRegister(ubConf);
			printf("Value 0x%X set\n", ubConf);
		}
		break;

		case 'c':
		{
			uint8_t ubConf = 0x00;
			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderLed->readOutputPortRegister(ubConf);
			printf("OUTPUT PORT is 0x%X\n", ubConf);
		}
		break;

		case 'd':
		{
			uint8_t ubConf = 0x00;
			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderLed->readInputPortRegister(ubConf);
			printf("INPUT PORT is 0x%X\n", ubConf);
		}
		break;

		case 'e':
		{
			uint8_t ubConf = 0x00;
			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderLed->readConfigurationRegister(ubConf);
			printf("CONFIG REG is 0x%X\n", ubConf);
		}
		break;

		case 'f':
		{
			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderLed->writeConfigurationRegister(0xF0);

			MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderLed->writeOutputPortRegister(0xF8);

			printf("INIT DONE\n");
		}
		break;

		case 'x':
		default:
			return -1;
			break;
	}

	return 0;
}

int TestManager::testLedDriver(void)
{
	uint8_t ubLed = 0;
	uint8_t ubConf = 0;
	uint8_t ubState = 0;
	uint8_t	ubAppo = 0;
	uint8_t ubPeriod = 0;
	bool	bLoopCmd = true;

	printf("\n--< CMD LIST >--\n");
	printf("a - LEDOUT\n" );
	printf("b - PWM\n" );
	printf("c - IREF\n");
	printf("d - PWMALL\n");
	printf("f - IREFALL\n" );
	printf("g - GRPFREQ\n" );
	printf("h - GRPPWM\n" );
	printf("i - Enable Dim/Blnk Mode\n");
	printf("l - Set Blinking\n");
	printf("r - resetSoft\n");
    printf("p - clearError\n");
	printf("x - End Test\n" );

	char sCmd[80];
	scanf("%s", sCmd);

	switch(sCmd[0])
	{
		case 'a':
			printf("LEDOUT [w,r]: " );
			scanf("%s",sCmd);
			printf("\n");

			if (sCmd[0] == 'w')
			{
				printf("LED [0-16]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubLed = atoi(&sCmd[0]);

				printf("CONF [0-3]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubConf = atoi(&sCmd[0]);

				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(ubLed, ubConf);
			}
			else if (sCmd[0] == 'r')
			{
				printf("LED [0-16]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubLed = atoi(&sCmd[0]);

				if (m_ubTypeCmd == 'b')
				{
					while (bLoopCmd == true)
					{
						int res = ctrlExit();
						if (res != 0)
						{
							bLoopCmd = false;
							break;
						}

						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readLedOut(ubLed, ubState);
						printf("LEDOUT conf%d \n", ubState);

						msleep(200);
					}
				}
				else
				{
					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readLedOut(ubLed, ubState);
					printf("LEDOUT conf%d \n", ubState);
				}

			}
			else
			{

			}
			break;

		case 'b':
			printf("PWM [w,r]: " );
			scanf("%s",sCmd);
			printf("\n");

			if (sCmd[0] == 'w')
			{
				printf("LED [0-16]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubLed = atoi(&sCmd[0]);

				printf("BRIGHT [0-255]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubAppo = atoi(&sCmd[0]);

				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWM(ubLed, ubAppo);
			}
			else if (sCmd[0] == 'r')
			{
				printf("LED [0-16]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubLed = atoi(&sCmd[0]);

				if (m_ubTypeCmd == 'b')
				{
					while (bLoopCmd == true)
					{
						int res = ctrlExit();
						if (res != 0)
						{
							bLoopCmd = false;
							break;
						}

						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readPWM(ubLed, ubState);
						printf("Brightness: %d \n", ubState);

						msleep(200);
					}
				}
				else
				{
					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readPWM(ubLed, ubState);
					printf("Brightness: %d \n", ubState);
				}
			}
			else
			{

			}
			break;

		case 'c':
			printf("IREF [w,r]: " );
			scanf("%s",sCmd);
			printf("\n");

			if (sCmd[0] == 'w')
			{
				printf("LED [0-16]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubLed = atoi(&sCmd[0]);

				printf("CURR [0-100]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubAppo = atoi(&sCmd[0]);

				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIref(ubLed, ubAppo);
			}
			else if (sCmd[0] == 'r')
			{
				printf("LED [0-16]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubLed = atoi(&sCmd[0]);

				if (m_ubTypeCmd == 'b')
				{
					while (bLoopCmd == true)
					{
						int res = ctrlExit();
						if (res != 0)
						{
							bLoopCmd = false;
							break;
						}

						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readIref(ubLed, ubState);
						printf("Current: %d \n", ubState);

						msleep(200);
					}
				}
				else
				{
					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readIref(ubLed, ubState);
					printf("Current: %d \n", ubState);
				}
			}
			else
			{

			}
			break;

		case 'd':
			printf("PWMALL [w,r]: " );
			scanf("%s",sCmd);
			printf("\n");

			if (sCmd[0] == 'w')
			{
				printf("PWM [0-255]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubAppo = atoi(&sCmd[0]);

				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWMAll(ubAppo);
			}
			else if (sCmd[0] == 'r')
			{
				if (m_ubTypeCmd == 'b')
				{
					while (bLoopCmd == true)
					{
						int res = ctrlExit();
						if (res != 0)
						{
							bLoopCmd = false;
							break;
						}

						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readPWMAll( ubState);
						printf("PWM: %d \n", ubState);

						msleep(200);
					}
				}
				else
				{
					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readPWMAll( ubState);
					printf("PWM: %d \n", ubState);
				}
			}
			else
			{
			}
			break;

		case 'f':
			printf("IREFALL [w,r]: " );
			scanf("%s",sCmd);
			printf("\n");

			if (sCmd[0] == 'w')
			{
				printf("CURRENT [0-100]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubAppo = atoi(&sCmd[0]);

				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIrefAll(ubAppo);
			}
			else if (sCmd[0] == 'r')
			{
				if (m_ubTypeCmd == 'b')
				{
					while (bLoopCmd == true)
					{
						int res = ctrlExit();
						if (res != 0)
						{
							bLoopCmd = false;
							break;
						}

						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readIrefAll(ubState);
						printf("CURR: %d \n", ubState);

						msleep(200);
					}
				}
				else
				{
					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readIrefAll(ubState);
					printf("CURR: %d \n", ubState);
				}
			}
			else
			{
			}
			break;

		case 'g':
			printf("GRPFREQ [w,r]: " );
			scanf("%s",sCmd);
			printf("\n");

			if (sCmd[0] == 'w')
			{
				printf("FREQ [0-100]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubAppo = atoi(&sCmd[0]);

				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpFreq(ubAppo);
			}
			else if (sCmd[0] == 'r')
			{
				if (m_ubTypeCmd == 'b')
				{
					while (bLoopCmd == true)
					{
						int res = ctrlExit();
						if (res != 0)
						{
							bLoopCmd = false;
							break;
						}

						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readGrpFreq(ubState);
						printf("FREQ: %d \n", ubState);

						msleep(200);
					}
				}
				else
				{
					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readGrpFreq(ubState);
					printf("FREQ: %d \n", ubState);
				}
			}
			else
			{
			}
			break;

		case 'h':
			printf("GRPPWM [w,r]: " );
			scanf("%s",sCmd);
			printf("\n");

			if (sCmd[0] == 'w')
			{
				printf("PWM [0-100]: " );
				scanf("%s",sCmd);
				printf("\n");

				ubAppo = atoi(&sCmd[0]);

				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpPWM(ubAppo);
			}
			else if (sCmd[0] == 'r')
			{
				if (m_ubTypeCmd == 'b')
				{
					while (bLoopCmd == true)
					{
						int res = ctrlExit();
						if (res != 0)
						{
							bLoopCmd = false;
							break;
						}

						MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readGrpPWM(ubState);
						printf("PWM: %d \n", ubState);

						msleep(200);
					}
				}
				else
				{
					MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readGrpPWM(ubState);
					printf("PWM: %d \n", ubState);
				}
			}
			else
			{
			}
			break;

		case 'i':
			printf("1 - DIM Mode\n " );
			printf("2 - BLNK Mode\n " );
			scanf("%s",sCmd);
			printf("\n");

			if (sCmd[0] == '2')
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_BLNK_CTL);
				printf("Set blinking mode\n " );
			}
			else
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_DIM_CTL);
				printf("Set dimming mode\n " );
			}
			break;

		case 'l':
			printf("LED [0-16]: " );
			scanf("%s",sCmd);
			printf("\n");

			ubLed = atoi(&sCmd[0]);

            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIref(ubLed, 25);
			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWM(ubLed, 50);
			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpPWM(50);//dc 50%

			printf("Freq [1s-15s]" );
			scanf("%s",sCmd);
			printf("\n");

			ubAppo = atoi(&sCmd[0]);
			ubPeriod = (15.26*ubAppo)-1;
			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpFreq(ubPeriod);
			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(ubLed, 3);
			break;

		case 'r':

			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->resetSoft();
			MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_DIM_CTL);
			printf("RESET \n" );
			break;

        case 'p':

            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->clearError();

            printf("CLEAR ERROR \n" );
            break;

		case 'x':
		default:
			return -1;
		break;
	}

	return 0;
}

int TestManager::testFanController(void)
{
	uint8_t ubFan = 0;
	uint8_t	ubAppo = 0;
	uint16_t usRPM = 0;
	bool	bLoopCmd = true;

	printf("\n--< CMD LIST >--\n");
	printf("a - START\n" );
	printf("b - STOP\n" );
	printf("c - INFO\n");
	printf("d - set config register \n");
	printf("e - read TACH\n");
	printf("f - set CurrentDutyCycle\n" );
	printf("g - read CurrentDutyCycle\n" );
	printf("h - config\n");
	printf("i - PWM12 Configuration Register\n");
	printf("l - PWM34 Configuration Register\n");
	printf("m - Itr Mask Register 1\n");
	printf("n - Itr Mask Register 2\n");
	printf("o - Total init\n");
	printf("x - End Test\n" );

	char sCmd[80];
	scanf("%s", sCmd);

	switch(sCmd[0])
	{
		case 'a':
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->startFanController();
			printf("FAN CONTROLLER START\n" );
			break;

		case 'b':
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->stopFanController();
			printf("FAN CONTROLLER STOP\n" );
			break;

		case 'c':
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readDeviceIDRegister(ubAppo);
					printf("Device ID: %d\n", ubAppo);

					MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readCompanyIDNumber(ubAppo);
					printf("Company ID: %d\n", ubAppo);

					MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readRevisionNumber(ubAppo);
					printf("Revision Num: %d\n", ubAppo);

					msleep(200);
				}
			}
			else
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readDeviceIDRegister(ubAppo);
				printf("Device ID: %d\n", ubAppo);

				MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readCompanyIDNumber(ubAppo);
				printf("Company ID: %d\n", ubAppo);

				MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readRevisionNumber(ubAppo);
				printf("Revision Num: %d\n", ubAppo);
			}
			break;

		case 'd':
			printf("Configuration Register 1: " );
			scanf("%s",sCmd);
			printf("\n");

			ubFan = atoi(&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeConfigRegister1(ubFan);

			printf("Configuration Register 2: " );
			scanf("%s",sCmd);
			printf("\n");

			ubFan = atoi(&sCmd[0]);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeConfigRegister2(ubFan);
			break;

		case 'e':
			printf("FAN [0-3]: " );
			scanf("%s",sCmd);
			printf("\n");

			ubFan = atoi(&sCmd[0]);

			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readFan(ubFan, usRPM);
					printf("FAN SPEED: %d\n", usRPM);

					msleep(200);
				}
			}
			else
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readFan(ubFan, usRPM);
				printf("FAN SPEED: %d\n", usRPM);
			}
			break;

		case 'f':
			printf("FAN [0-3]: " );
			scanf("%s",sCmd);
			printf("\n");

			ubFan = atoi(&sCmd[0]);

			printf("DC [0-100]: " );
			scanf("%s",sCmd);
			printf("\n");

			ubAppo = atoi(&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(ubFan, ubAppo);

		break;

		case 'g':
			printf("FAN [0-3]: " );
			scanf("%s",sCmd);
			printf("\n");

			ubFan = atoi(&sCmd[0]);

			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readCurrentDutyCycle(ubFan, ubAppo);
					printf("Current DC %d \n", ubAppo);

					msleep(200);
				}
			}
			else
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readCurrentDutyCycle(ubFan, ubAppo);
				printf("Current DC %d \n", ubAppo);
			}
			break;

		case 'h':
			printf("CONFIG LOW FREQ \n");
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->configLowFrequency();

			break;

		case 'i':
			printf("PWM1/PWM2 configuration register: " );
			scanf("%s",sCmd);
			printf("\n");

			ubFan = atoi(&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writePWM12ConfigurationRegister(ubFan);
			break;

		case 'l':
			printf("PWM3/PWM4 configuration register: " );
			scanf("%s",sCmd);
			printf("\n");

			ubFan = atoi(&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writePWM34ConfigurationRegister(ubFan);
			break;

		case 'm':
			printf("Interrupt Mask Register 1: " );
			scanf("%s",sCmd);
			printf("\n");

			ubFan = atoi(&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeItrMaskReg1(ubFan);
			break;

		case 'n':
			printf("Interrupt Mask Register 2: " );
			scanf("%s",sCmd);
			printf("\n");

			ubFan = atoi(&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeItrMaskReg2(ubFan);
			break;

		case 'o':

			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeConfigRegister2(0xF0);
			msleep(1);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writePWM12ConfigurationRegister(0x30);
			msleep(1);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writePWM34ConfigurationRegister(0x30);
			msleep(1);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(0,0);
			msleep(1);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(1,0);
			msleep(1);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(2,0);
			msleep(1);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(3,0);
			msleep(1);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeConfigRegister1(0x61);
			msleep(1);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeItrMaskReg1(0x7F);
			msleep(1);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeItrMaskReg2(0x07);
			msleep(1);
			MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeConfigRegister2(0x70);

			printf("--<Total Init>--\n" );
			break;

		case 'x':
		default:
			return -1;
		break;
	}
	return 0;
}

int TestManager::testEeprom(void)
{
	uint16_t usStartAddress = 0x00;
	string strTxMsg("Nel mezzo del cammin di nostra vita "
			   "mi ritrovai per una selva oscura, "
			   "ché la diritta via era smarrita.\n"
			   "Ahi quanto a dir qual era è cosa dura "
			   "esta selva selvaggia e aspra e forte "
			   "che nel pensier rinova la paura!\n"
			   "Tant' è amara che poco è più morte; "
			   "ma per trattar del ben ch'i' vi trovai, "
			   "dirò de l'altre cose ch'i' v'ho scorte.\n"
			   "Io non so ben ridir com' i' v'intrai, "
			   "tant' era pien di sonno a quel punto "
               "che la verace via abbandonai.\n");

    uint16_t usMsgLen = strTxMsg.size();
    uint8_t rgRxBuff[500] = {0};
	uint8_t cWord;
	int8_t cRes;
    char sCmd[80];

	printf("\n--< CMD LIST >--\n\n");
	printf("a - Write one word(8 bit)\n" );
	printf("b - Read one word (8 bit)\n" );
	printf("c - Write the intro of the Divina Commedia\n");
	printf("d - Read the intro of the Divina Commedia\n");
    printf("e - Write\n");
    printf("f - Read\n");
    printf("g - Reset to default\n\n");
	scanf("%s", sCmd);

	switch(sCmd[0])
	{

		case 'a':
			printf("Insert the word: ");
			cin >> cWord;
			printf("\n");
			cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->writeWord(0x00, cWord);
			if ( cRes )
			{
				printf("\nUnable to write to EEPROM\n");
			}
		break;

		case 'b':
			cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->readWord(0x00, cWord);
			if ( cRes )
			{
				printf("\nUnable to read from EEPROM\n");
			}
			printf("Word read is: %c\n", cWord);
		break;

		case 'c':
			cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->write(usStartAddress, (uint8_t*)strTxMsg.c_str(), usMsgLen);
			if ( cRes )
			{
				printf("\nUnable to write to EEPROM\n");
			}
			printf("\n\nMessage written:\n");
			for (auto c : strTxMsg)
			{
				printf("%c", c);fflush(stdout);
			}

		break;

		case 'd':
			cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->read(usStartAddress, rgRxBuff, usMsgLen);
			if ( cRes )
			{
				printf("\nUnable to read from EEPROM\n");
			}
			printf("\n");
			for (auto c : rgRxBuff)
			{
				printf("%c", c);fflush(stdout);
			}
		break;

        case 'e':
        {
            printf("Insert starting address\n");
            int liAddr;
            cin >> liAddr;
            printf("Insert string\n");
            string strVal;
            cin >> strVal;
            int liSize = strVal.size();

            cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->write(liAddr, (uint8_t*)strVal.c_str(), liSize);
            if ( cRes )
            {
                printf("\nUnable to write %s to addr %d\n", strVal.c_str(), liAddr);
            }
            printf("\n\nMessage written:\n");
            for (auto c : strVal)
            {
                printf("%c", c);fflush(stdout);
            }
        }
        break;

        case 'f':
        {
            printf("Insert starting address\n");
            int liAddr;
            cin >> liAddr;
            printf("Insert num of bytes to be read\n");
            int liVal;
            cin >> liVal;
            string strVal("");
            strVal.resize(liVal);

            cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->read(liAddr, (uint8_t*)strVal.c_str(), liVal);
            if ( cRes )
            {
                printf("\nUnable to write %s to addr %d\n", strVal.c_str(), liAddr);
            }
            printf("\n");
            for (auto c : strVal)
            {
                printf("%c", c);fflush(stdout);
            }
        }
        break;

        case 'g':
			cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->resetEeprom();
			if ( cRes )
			{
				printf("\nUnable to read from EEPROM\n");
			}
			else
			{
				printf("\nEEPROM reset with success\n");
			}
                break;

		default:
			printf("Wrong key pressed. I'm sorry.\n");
			return -1;
		break;
	}

	return 0;
}

int TestManager::testCodecAudio(void)
{
	bool	bLoopCmd = true;
	long	lVolume = 0;
	long	lmin, lmax;
	uint8_t ubVol = 0;

	printf("\n--< CMD LIST >--\n");
	printf("a - read Vol\n" );
	printf("b - set Vol\n" );
	printf("c - Play Audio\n");
	printf("d - range Control\n");
	printf("x - End Test\n" );

	char sCmd[80];
	scanf("%s", sCmd);

	switch(sCmd[0])
	{
		case 'a':

			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->getSpeakerAnalogVolume(lVolume);
					m_pLogger->log(LOG_INFO,"Speaker Analog VolumeT:%d\n", lVolume );
					msleep(1000);
				}
			}
			else
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->getSpeakerAnalogVolume(lVolume);
				m_pLogger->log(LOG_INFO,"Speaker Analog VolumeT:%d\n", lVolume );
			}
			break;

		case 'b':
			printf("SET SPEAKER Vol[0-127]:\n");

			scanf("%s", sCmd);
			ubVol = atoi(&sCmd[0]);

			MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->setSpeakerAnalogVolume(ubVol);
			break;

		case 'c':
			printf("Play Audio\n");
            system("gst-play-1.0 /testfiles/Ring2.wav &");
			break;

		case 'd':
			if (m_ubTypeCmd == 'b')
			{
				while (bLoopCmd == true)
				{
					int res = ctrlExit();
					if (res != 0)
					{
						bLoopCmd = false;
						break;
					}

					MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->getSpeakerRange(MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->eTLV320_ANVOL,lmin,lmax);
					m_pLogger->log(LOG_INFO, "Lim Max:%d, Lim Min: %d", lmax, lmin);
					msleep(1000);
				}
			}
			else
			{
				MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->getSpeakerRange(MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->eTLV320_ANVOL,lmin,lmax);
				m_pLogger->log(LOG_INFO, "Lim Max:%d, Lim Min: %d", lmax, lmin);
			}
			break;

		case 'x':
		default:
			return -1;
			break;
	}
	return 0;
}



