/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    USBComHandler.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the USBComHandler class.
 @details

 ****************************************************************************
*/

#ifndef USBCOMHANDLER_H
#define USBCOMHANDLER_H

#include "HIDInterface.h"
#include "USBInterface.h"

#define MAX_SIZE	65


typedef enum
{
	eNoDevice = -1,
	eHIDKeyboard = 0,
	eHIDDevice = 1,
	eCDCDevice = 2
} enumDeviceType;

extern enumDeviceType eUSBType;

class USBComHandler
{
	public:

		/*! **********************************************************************************************************************
		 * @brief USBComHandler default constructor. Initialize all the members.
		 * ***********************************************************************************************************************
		 */
		USBComHandler();

		/*! **********************************************************************************************************************
		 * @brief ~USBComHandler default constructor. Initialize all the members.
		 * ***********************************************************************************************************************
		 */
		virtual ~USBComHandler();

		/*! **********************************************************************************************************************
		 * @brief setConfig set camera configuration (CDC, HID device or HID keyboard)
		 * @param eType device type
		 * ***********************************************************************************************************************
		 */
		void setConfig(enumDeviceType eType);

		/*! **********************************************************************************************************************
		 * @brief  getConfig get camera configuration (CDC, HID device or HID keyboard)
		 * @return device type
		 * ***********************************************************************************************************************
		 */
		enumDeviceType getConfig(void);

		/*! **********************************************************************************************************************
		 * @brief  sendCmd send message received, in different ways depending if the camera is in HID o CDC mode
		 * @param  strCmd command to be sent
		 * @return true in case of success, false otherwise
		 * ***********************************************************************************************************************
		 */
		bool sendCmd(const string& strCmd);


		/*! **********************************************************************************************************************
		 * @brief  sendACK send ack, in different ways depending if the camera is in HID o CDC mode
		 * @return true in case of success, false otherwise
		 * ***********************************************************************************************************************
		 */
		bool sendACK(void);

		/*! **********************************************************************************************************************
		 * @brief  readMsgReceived read message received, in different ways depending if the camera is in HID o CDC mode
		 * @param  liTimeoutMs is the timeout for the USB read
		 * @return number of bytes read or -1 in case of insuccess
		 * ***********************************************************************************************************************
		 */
		int32_t readMsgReceived(int32_t liTimeoutMs);

	protected:

		/*! **********************************************************************************************************************
		 * @brief virtual functions redefined
		 * ***********************************************************************************************************************
		 */
		virtual bool sendPktHID(const string& strCmd);
		virtual bool sendPktCDC(const string& strCmd);
		virtual bool sendAckHID();
		virtual bool sendAckCDC();
		virtual int32_t receivePktHID(int32_t liTimeoutMs);
		virtual int32_t receivePktCDC(int32_t liTimeoutMs);

    private:

        enumDeviceType*		m_pDevType;

};

#endif // USBCOMHANDLER_H
