/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    USBInterfaceCollector.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the USBInterfaceCollector class.
 @details

 ****************************************************************************
*/

#include "USBInterfaceCollector.h"

USBInterfaceCollector::USBInterfaceCollector()
{
	m_pLogger = NULL;
	m_pUSB0	  = NULL;
}

USBInterfaceCollector::~USBInterfaceCollector()
{
	SAFE_DELETE(m_pUSB0);
}


int8_t USBInterfaceCollector::createAndInitInterfaces(const char* sConfigFile)
{
	// read parameters from config file.
	if ( ! readConfigFile(sConfigFile) )
	{
		log(LOG_ERR, "USBInterfaceCollector::createAndInitInterfaces: Unable to read config file");
		return -1;
	}

	SAFE_DELETE(m_pUSB0);

	m_pUSB0 = new USBInterface();
	if ( !m_pUSB0 )
	{
		log(LOG_ERR, "USBInterfaceCollector::createAndInitInterfaces: unable to create USB interface, memory error");
		return -1;
	}

	if ( !m_pUSB0->initInterface(m_Config.GetString(CFG_USB_CDC_BASE_DIR_1), m_Config.GetInt(CFG_USB_CDC_BITS_PER_WORD_1),
								 m_Config.GetString(CFG_USB_CDC_PARITY_1), m_Config.GetInt(CFG_USB_CDC_HAS_RTS_1),
								 m_Config.GetInt(CFG_USB_CDC_HAS_XON_1)) )
	{
		log(LOG_ERR, "USBInterfaceCollector::createAndInitInterfaces: error initializing %s.", m_Config.GetString(CFG_USB_CDC_BASE_DIR_1) );
		return -1;
	}

	m_pUSB0->setLogger(getLogger());

	return 0;
}

USBInterface* USBInterfaceCollector::getInterfaceByIndex(enumUSBChannel eUSBChannel)
{
	USBInterface* pUSBifr = NULL;

	switch ( eUSBChannel )
	{
		case eUSB_0:	pUSBifr = m_pUSB0;	break;

		case eUSB_1:
		case eUSB_2:
		case eUSB_3:

		default:
			log(LOG_ERR, "SPIInterfaceCollector::getChannel: channel not available");
			return NULL;
		break;
	}

	return pUSBifr;
}

void USBInterfaceCollector::destroyInterfaces()
{
	SAFE_DELETE(m_pUSB0);
}


bool USBInterfaceCollector::readConfigFile(const char* sConfigFileName)
{

	// Reads configuration file
	config_key_t config_keys[] =
	{
		// Single SPI configuration keys
		{ CFG_USB_CDC_BASE_DIR_1, (char*)"usb.1.baseDir", (char*)"GENERAL", T_string, (char*)CAMERA_USB_BUS_NAME, DEFAULT_ACCEPTED},
		{ CFG_USB_CDC_BITS_PER_WORD_1, (char*)"usb.1.bitsPerWord", (char*)"GENERAL", T_int, (char*)CAMERA_USB_BITS_PER_WORD, DEFAULT_ACCEPTED},
		{ CFG_USB_CDC_PARITY_1, (char*)"usb.1.parity", (char*)"GENERAL", T_string, (char*)CAMERA_USB_PARITY, DEFAULT_ACCEPTED},
		{ CFG_USB_CDC_HAS_RTS_1, (char*)"usb.1.hasRts", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
		{ CFG_USB_CDC_HAS_XON_1, (char*)"usb.1.hasXon", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED},
	};

	int num_keys = sizeof(config_keys) / sizeof(config_key_t);

	if ( !m_Config.Init(config_keys, num_keys, sConfigFileName) )
	{
		char error[1024];
		Config::GetLastError(error, 1024);
		log(LOG_ERR, "Impossible to read parameter from <%s> (error: <%s>)", sConfigFileName, error);
		m_Config.Free();
		return false;
	}

	return true;

}
