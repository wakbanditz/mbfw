/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SprDetectionPreliminary.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SprDetectionPreliminary class.
 @details

 ****************************************************************************
*/

#include <algorithm>

#include "SprDetectionPreliminary.h"

#define SPR_WHITE_CHECK_PERC_THRESHOLD	20.0f
#define SPR_WHITE_CHECK_THRESHOLD		180
#define SPR_WHITE_CHECK_HALF_WIDTH		30
#define SPR_WHITE_CHECK_HALF_HEIGHT		25
#define SPR_IMAGE_CORNER_SIZE			75
#define SPR_IMAGE_MIN_WIDTH				150
#define SPR_IMAGE_MIN_HEIGHT			300
#define SPR_REMOVE_ARTEFACTS_STEP_SIZE	10
#define SPR_REMOVE_LABEL_LOW_LIMIT		199
#define SPR_REMOVE_LABEL_HIGH_LIMIT		299

SprDetectionPreliminary::SprDetectionPreliminary()
{
	m_Edge.liLowerEdge = 0;
	m_Edge.liUpperEdge = 0;
	m_pLogger = nullptr;
	m_bConfigOk = false;
	m_pvImage = nullptr;
}

SprDetectionPreliminary::~SprDetectionPreliminary()
{

}

int SprDetectionPreliminary::init(vector<unsigned char>* pvImage, Log* pLogger, uint32_t uliWidth, uint32_t uliHeight)
{
	if ( pvImage == nullptr )	return -1;
	if ( pLogger == nullptr )	return -1;

	m_pLogger = pLogger;
	m_pvImage = pvImage;
	setImageHeight(uliHeight);
	setImageWidth(uliWidth);

	m_bConfigOk = true;

	return 0;
}

int SprDetectionPreliminary::perform(eSprResults& eRes)
{
	if ( ! m_bConfigOk )	return -1;

	// Perform a check on the image size
	if ( m_nImageWidth < SPR_IMAGE_MIN_WIDTH || m_nImageHeight < SPR_IMAGE_MIN_HEIGHT )
	{
		eRes = eSprResults::eError;
		m_pLogger->log(LOG_ERR, "SprDetectionPreliminary::perform: the size of the image is small to "
								"contain the data matrix");
		return -1;
	}

	// Unknown yet
	eRes = eSprResults::eUnknown;

	float fWhitePerc = look4White();
	if ( fWhitePerc > SPR_WHITE_CHECK_PERC_THRESHOLD )
	{
		eRes = eSprResults::eGlareOnSpr;
		m_pLogger->log(LOG_DEBUG, "SprDetectionPreliminary::perform: found SPR");
		return 0;
	}

	// Remove corners that may have glares and introduce artefacts - 75x75 window
	removeCorners();

	// Find line presence
	int liLowEdge = 0;
	int liHighEdge = 0;
	int liRes = removeStripArtefacts(&liLowEdge, &liHighEdge);
	if ( liRes == -1 )
	{
		eRes = eSprResults::eError;
		m_pLogger->log(LOG_ERR, "SprDetectionPreliminary::perform: unable to remove artefacts");
		return -1;
	}

	// Filter until satisfied
	liRes = filter(liLowEdge, liHighEdge);
	if ( liRes == -1 )
	{
		eRes = eSprResults::eError;
		m_pLogger->log(LOG_ERR, "SprDetectionPreliminary::perform: unable to filter the image");
		return -1;
	}

	m_Edge.liLowerEdge = liLowEdge;
	m_Edge.liUpperEdge = liHighEdge;

	return 0;
}

const stripEdgesStruct& SprDetectionPreliminary::getEdges()
{
	return m_Edge;
}

float SprDetectionPreliminary::look4White()
{
	if ( m_nImageWidth < 2*SPR_WHITE_CHECK_HALF_WIDTH )		return -1;
	if ( m_nImageHeight < 2*SPR_WHITE_CHECK_HALF_HEIGHT )	return -1;

	float fWhitePerc = 0.0f;
	int xStart = m_nImageWidth / 2;
	int xStop = xStart + SPR_WHITE_CHECK_HALF_WIDTH;
	int yStart = m_nImageHeight / 2;
	int yStop = yStart + SPR_WHITE_CHECK_HALF_HEIGHT;

	for ( int i = xStart; i != xStop; ++i )
	{
		for ( int j = yStart; j != yStop; ++j )
		{
			if ( m_pvImage->at(yStart*m_nImageWidth+i) > SPR_WHITE_CHECK_THRESHOLD )
			{
				++fWhitePerc;
			}
		}
	}

	fWhitePerc *= 100;
	fWhitePerc /= (SPR_WHITE_CHECK_HALF_WIDTH*2 + SPR_WHITE_CHECK_HALF_HEIGHT*2);

	return fWhitePerc;
}

void SprDetectionPreliminary::removeCorners()
{
	unsigned int liEdge = SPR_IMAGE_CORNER_SIZE;
	unsigned char ucMin = UINT8_MAX;

	for ( unsigned int i = 0; i != liEdge; ++i )
	{
		for ( unsigned int j = 0; j != liEdge; ++j )
		{
			if ( m_pvImage->at(j*m_nImageWidth+i) < ucMin )
			{
				ucMin = m_pvImage->at(j*m_nImageWidth+i);
			}
		}
	}

	if ( ucMin >= UINT8_MAX-4 )	ucMin = UINT8_MAX-4;

	for ( unsigned int i = 0; i != liEdge; ++i )
	{
		for ( unsigned int j = 0; j != liEdge; ++j )
		{
			m_pvImage->at(j*m_nImageWidth+i) = ucMin + 4;
			m_pvImage->at(j*m_nImageWidth+(m_nImageWidth-i-1)) = ucMin + 4;
		}
	}
}

int SprDetectionPreliminary::removeStripArtefacts(int* pLowEdge, int* pHighEdge)
{
	if ( pLowEdge == nullptr )	return -1;
	if ( pHighEdge == nullptr )	return -1;

	// Check first in the left part of the image to check if the strip artefact is present
	// Note: it is important to temove it, because it is too white -> the next filtering
	// step leads undesired images.

	vector<uint32_t> vWave(m_nImageHeight);
	uint32_t rgLowLimits[5] = {};
	uint32_t rgHighLimits[5] = {};

	int liCnt = 0;
	int liStart = SPR_REMOVE_ARTEFACTS_STEP_SIZE - 1;
	int liEnd = 5 * SPR_REMOVE_ARTEFACTS_STEP_SIZE;

	for ( auto column = liStart; column < liEnd; column+=SPR_REMOVE_ARTEFACTS_STEP_SIZE )
	{
		unsigned int uliMaxVal = 0;
		unsigned int uliMaxPos = 0;
		for ( int i = 0; i < m_nImageHeight; ++i )
		{
			vWave[i] = m_pvImage->at(i*m_nImageWidth+column);
			if ( vWave[i] > uliMaxVal )
			{
				uliMaxVal = vWave[i];
				uliMaxPos = i;
			}
		}

		if ( uliMaxPos > SPR_REMOVE_LABEL_LOW_LIMIT && uliMaxPos < SPR_REMOVE_LABEL_HIGH_LIMIT )
		{
			liCnt++;
			unsigned int uliLimit = ( uliMaxPos > 30 ) ? uliMaxPos-30 : 0;
			for ( auto check = uliMaxPos; check > uliLimit; --check )
			{
				if ( vWave[check] <= vWave[check-1] && vWave[check] < uliMaxVal/2 )
				{
					int liIdx = (column-liStart) / SPR_REMOVE_ARTEFACTS_STEP_SIZE;
					rgLowLimits[liIdx] =  check;
					break;
				}
			}
			uliLimit = ( int(uliMaxPos) < m_nImageHeight-30 ) ? uliMaxPos+30 : m_nImageHeight;
			for ( auto check = uliMaxPos; check < uliLimit; ++check )
			{
				if ( vWave[check] <= vWave[check+1] && vWave[check] < uliMaxVal/2 )
				{
					int liIdx = (column-liStart) / SPR_REMOVE_ARTEFACTS_STEP_SIZE;
					rgHighLimits[liIdx] =  check;
					break;
				}
			}
		}
	}

	// If no foil has been found on the left, check on the right
	if ( liCnt < 3 )
	{
		liCnt = 0;
		fill(rgLowLimits, rgLowLimits+5, 0x00);
		fill(rgHighLimits, rgHighLimits+5, 0x00);
		int foo = 0;
		int liBegin = m_nImageWidth - SPR_REMOVE_ARTEFACTS_STEP_SIZE - 1;
		int liStop = m_nImageWidth - ( 5 * SPR_REMOVE_ARTEFACTS_STEP_SIZE + 2 );

		for ( auto column = liBegin; column > liStop; column-=SPR_REMOVE_ARTEFACTS_STEP_SIZE )
		{
			unsigned int uliMaxVal = 0;
			unsigned int uliMaxPos = 0;
			for ( int i = 0; i < m_nImageHeight; ++i )
			{
				vWave[i] = m_pvImage->at(i*m_nImageWidth+column);
				if ( vWave[i] > uliMaxVal )
				{
					uliMaxVal = vWave[i];
					uliMaxPos = i;
				}
			}

			if ( uliMaxPos > SPR_REMOVE_LABEL_LOW_LIMIT && uliMaxPos < SPR_REMOVE_LABEL_HIGH_LIMIT )
			{
				liCnt++;
				unsigned int uliLimit = ( uliMaxPos > 30 ) ? uliMaxPos-30 : 0;
				for ( auto check = uliMaxPos; check > uliLimit; --check )
				{
					if ( vWave[check] <= vWave[check-1] && vWave[check] < uliMaxVal/2 )
					{
						rgLowLimits[foo] =  check;
						break;
					}
				}
				uliLimit = ( int(uliMaxPos) < m_nImageHeight-30 ) ? uliMaxPos+30 : m_nImageHeight;
				for ( auto check = uliMaxPos; check < uliLimit; ++check )
				{
					if ( vWave[check] <= vWave[check+1] && vWave[check] < uliMaxVal/2 )
					{
						rgHighLimits[foo] =  check;
						break;
					}
				}
			}
			foo++;
		}
	}

	*pLowEdge = 0;
	*pHighEdge = 0;
	// It means that there is no foil
	if ( liCnt < 3 )
	{
		return 0;
	}

	int liCntLow = 0;
	int liCntHigh = 0;
	for ( int i = 0; i < 5; ++i )
	{
		if ( rgLowLimits[i] )
		{
			*pLowEdge += rgLowLimits[i];
			liCntLow++;
		}

		if ( rgHighLimits[i] )
		{
			*pHighEdge += rgHighLimits[i];
			liCntHigh++;
		}
	}

	if ( liCntLow != 0 && liCntHigh != 0 )
	{
		*pLowEdge /= liCntLow;
		*pHighEdge /= liCntHigh;

		// Get some margin
		*pLowEdge  -= 2;
		*pHighEdge += 2;
	}

	return 0;
}

int SprDetectionPreliminary::filter(int liLowEdge, int liHighEdge)
{
	vector<unsigned char> vTmpImage;
	vTmpImage.reserve(m_pvImage->size());
	if ( liLowEdge && liHighEdge )
	{
		copy(m_pvImage->begin(), m_pvImage->begin()+(liLowEdge)*m_nImageWidth, back_inserter(vTmpImage));
		copy(m_pvImage->begin()+(liHighEdge+1)*m_nImageWidth, m_pvImage->end(), back_inserter(vTmpImage));
	}
	else
	{
		vTmpImage = *m_pvImage;
	}

	sort(vTmpImage.begin(), vTmpImage.end());

	unsigned int liLowThresh = (vTmpImage.size()/100 > 0) ? vTmpImage[vTmpImage.size()/100-1] : vTmpImage[1];
	unsigned int liHighThresh = vTmpImage[99*vTmpImage.size()/100-1];
	// retry number with different filter parameter, to let the best image to be processed
	signed char cRetry = 5;
	liHighThresh -= cRetry;

	do
	{
		vTmpImage = *m_pvImage;

		size_t size = m_pvImage->size();
		// Adjust the image
		for ( size_t i = 0; i != size; ++i )
		{
			if ( vTmpImage[i] < liLowThresh )
			{
				vTmpImage[i] = 0;
			}
			else if ( vTmpImage[i] > liHighThresh )
			{
				vTmpImage[i] = MAX_PIXEL_VALUE;
			}
			else
			{
				float fFoo = MAX_PIXEL_VALUE * (vTmpImage[i] - liLowThresh );
				fFoo /= float( liHighThresh - liLowThresh );
				if ( round(fFoo) <= 110 )
				{
					vTmpImage[i] = 0;
				}
				else
				{
					vTmpImage[i] = round(fFoo);
				}
			}
		}

		uint8_t liMedianFilterMatrix[2] = {5,5};
		_opt_medFilt2(vTmpImage.data(), m_nImageWidth, m_nImageHeight, liMedianFilterMatrix);

		// Now the image is filtered, check if current filter is good enough
		// It has to all black in the spot chosen
		bool bWrongLightConditions = false;
		int yStart = 79;
		int yStop = (m_nImageHeight>yStart+100) ? yStart+100 : m_nImageHeight;
		int xStart = m_nImageWidth-SPR_WHITE_CHECK_HALF_WIDTH;

		for ( int i = yStart; i < yStop; ++i )
		{
			if ( bWrongLightConditions )	break;

			for ( int j = xStart; j < m_nImageWidth; ++j )
			{
				if ( vTmpImage[i*m_nImageWidth+j] > 0 )
				{
					bWrongLightConditions = true;
					break;
				}
			}
		}

		if ( ! bWrongLightConditions )	break;

		++liHighThresh;
	}
	while(--cRetry >= 0);

	*m_pvImage = vTmpImage;

	return 0;
}
