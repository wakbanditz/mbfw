/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SprDetectionLink.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SprDetectionLink class.
 @details

 ****************************************************************************
*/

#include "SprDetectionLink.h"

SprDetectionLink::SprDetectionLink()
{
	m_bConfigOk = false;
	m_nImageWidth = false;
	m_vImage.clear();
	m_pLogger = nullptr;
	m_pPreliminaryMan = nullptr;
	m_pEllipseMan = nullptr;
}

SprDetectionLink::~SprDetectionLink()
{
	SAFE_DELETE(m_pPreliminaryMan);
	SAFE_DELETE(m_pEllipseMan);
}

int SprDetectionLink::init(const unsigned char* pImage, Log* pLogger, uint32_t uliWidth, uint32_t uliHeight)
{
	m_bConfigOk = false;

	if ( pImage == nullptr )	return -1;
	if ( pLogger == nullptr )	return -1;

	m_pLogger = pLogger;

	SAFE_DELETE(m_pPreliminaryMan);
	m_pPreliminaryMan = new SprDetectionPreliminary();
	if ( m_pPreliminaryMan == nullptr )
	{
		m_pLogger->log(LOG_ERR, "SprDetectionLink::init: memory error");
		return -1;
	}

	SAFE_DELETE(m_pEllipseMan);
	m_pEllipseMan = new SprDetectionEllipse();
	if ( m_pEllipseMan == nullptr )
	{
		m_pLogger->log(LOG_ERR, "SprDetectionLink::init: memory error");
		return -1;
	}

	m_nImageWidth = uliWidth;
	m_vImage.assign(pImage, pImage+uliWidth*uliHeight);
	int liRes = m_pPreliminaryMan->init(&m_vImage, pLogger, uliWidth, uliHeight);
	if ( liRes )	return false;

	liRes = m_pEllipseMan->init(&m_vImage, pLogger, uliWidth, uliHeight);
	if ( liRes )	return false;

	m_bConfigOk = true;

	return 0;
}

int SprDetectionLink::perform(bool& bIsSprPresent)
{
	if ( ! m_bConfigOk )	return -1;

	eSprResults eCodeRes = eSprResults::eUnknown;
	bIsSprPresent = false;

	// Create a copy before m_vImage is changed from the 2 managers
	vector<unsigned char> vOriginalImage(m_vImage);

	int liRes = m_pPreliminaryMan->perform(eCodeRes);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "SprDetectionLink::perform(): unable to perform preliminary man");
		return -1;
	}

	if ( eCodeRes == eSprResults::eGlareOnSpr )
	{
		bIsSprPresent = true;
		return 0;
	}

	const stripEdgesStruct& Edge = m_pPreliminaryMan->getEdges();

	/*! Find ellipses coordinates **/
	liRes = m_pEllipseMan->perform(&Edge, eCodeRes);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "SprDetectionLink::perform(): unable to perform ellipse man");
		return -1;
	}

	if ( eCodeRes == eSprResults::eEllipseOutOfLimits || eCodeRes == eSprResults::eNoMatch )
	{
		bIsSprPresent = true;
		return 0;
	}

	const deque<point<int>>& vPointsMin = m_pEllipseMan->getIntCoordMinus();
	const deque<point<int>>& vPointsPlus = m_pEllipseMan->getIntCoordPlus();

	liRes = look4White(vOriginalImage, vPointsMin, vPointsPlus, eCodeRes);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "SprDetectionLink::perform(): unable to look for white into the image");
		return -1;
	}

	switch ( eCodeRes )
	{
		case eSprResults::eGlareOnSpr:
			bIsSprPresent = true;
		break;

		case eSprResults::eNoGlare:
			bIsSprPresent = false;
		break;

		default:
			m_pLogger->log(LOG_ERR, "SprDetectionLink::perform(): unexpected result code");
			return -1;
		break;
	}

	return 0;
}

bool SprDetectionLink::readBMP(unsigned char* prgData, string& strFileName)
{
	unsigned char nImageHeader[BMP_HEADER_SIZE];

	FILE* picture = fopen(strFileName.c_str(), "rb"); // rb means: read as a binary file
	if ( picture == nullptr )
	{
		return false;
	}

	if ( fread(nImageHeader, sizeof(unsigned char), BMP_HEADER_SIZE, picture) != BMP_HEADER_SIZE )
	{
		printf("Image not succesfully read. \nBe careful, the name can be wrong.\n");
		return false;
	}

	int size = getVal4((char*)&nImageHeader[2]);
	int bfOffBits = getVal4((char*)&nImageHeader[10]);
	int width = getVal4((char*)&nImageHeader[18]);
	int height = getVal4((char*)&nImageHeader[22]);

	unsigned char* raw_data  = new unsigned char[size];
	fread(raw_data, sizeof(unsigned char), size, picture);

	fclose(picture);

	raw_data += ( bfOffBits - BMP_HEADER_SIZE );

	// Need 2 take care about the padding
	int padBytes;

	if ( width%4 == 0 )
	{
		padBytes = 0;
	}
	else
	{
		padBytes = 4 - width%4;
	}

	width += padBytes;

	// fRead goes from left to right and from bottom to top, while Matlab and the camera
	// go from left to right and from top to bottom.

	int idx, counter = 0;
	for (int idxH = (height-1); idxH >= 0; --idxH)
	{
		for (int idxW = 0; idxW < width-padBytes; ++idxW)
		{
			idx = (idxH * width) + idxW;

			*(prgData+counter) = *(raw_data+idx);
			++counter;
		}
	}
	// this is actually needed because the delete has to have the correct start
	raw_data -=  (bfOffBits - BMP_HEADER_SIZE );
	delete[] raw_data;
	return true;
}

int SprDetectionLink::look4White(vector<unsigned char>& vOriginalImage, const deque<point<int>>& vPointsMin, const deque<point<int>>& vPointsPlus, eSprResults& eRes)
{
	if ( vPointsMin.empty() )		return -1;
	if ( vPointsPlus.empty() )		return -1;
	if ( vOriginalImage.empty() )	return -1;

	// Unknown yet
	eRes = eSprResults::eUnknown;

	size_t size = vPointsMin.size();
	int liCoordShouldBeChecked = size / 2;
	int liPixelsChecked = 0;
	int liPixelsWhite = 0;
	int liLastY = -1;

	for ( int i = 0; i <= liCoordShouldBeChecked; ++i )
	{
		// May happen that for float->int conversion, some ordinates are the same. Skip if happens
		if ( liLastY == vPointsMin[i].y )	continue;

		// Update variable
		liLastY = vPointsMin[i].y;

		// Being an ellipse, coordinates are specular
		for ( int k = vPointsMin[i].x; k <= vPointsMin[size-1-i].x; ++k )
		{
			liPixelsChecked++;
			if ( vOriginalImage[liLastY*m_nImageWidth+k] > 180 )
				liPixelsWhite++;
		}
	}

	size = vPointsPlus.size();
	liCoordShouldBeChecked = size / 2;
	liLastY = -1;
	for ( int i = 0; i <= liCoordShouldBeChecked; ++i )
	{
		// May happen that for float->int conversion, some ordinates are the same. Skip if happens
		if ( liLastY == vPointsPlus[i].y )	continue;

		// Update variable
		liLastY = vPointsPlus[i].y;

		// Being an ellipse, coordinates are specular
		for ( int k = vPointsPlus[i].x; k <= vPointsPlus[size-1-i].x; ++k )
		{
			liPixelsChecked++;
			if ( vOriginalImage[liLastY*m_nImageWidth+k] > 180 )
				liPixelsWhite++;
		}
	}

	float fRes = float(liPixelsWhite) * 100.0f / float(liPixelsChecked);

	if ( fRes > 25.0f )
	{
		eRes = eSprResults::eGlareInEllipse;
	}
	else
	{
		eRes = eSprResults::eNoGlare;
	}

	m_pLogger->log(LOG_DEBUG, "SprDetectionLink::look4White: percentage of white inside the area interpreted "
							 "as the holder [%.2f%%]", fRes);

	return 0;
}

int32_t SprDetectionLink::getVal4(char *p)
{
	union
	{
		uint32_t i;
		unsigned char c[4];
	} endiannessTest;

	endiannessTest.i = {0x01020304};

if ( endiannessTest.c[0] == 4 )
{
	// little endian
	endiannessTest.c[0]=*p++;
	endiannessTest.c[1]=*p++;
	endiannessTest.c[2]=*p++;
	endiannessTest.c[3]=*p++;
}
else
{
	// big endian
	endiannessTest.c[3]=*(p++);
	endiannessTest.c[2]=*(p++);
	endiannessTest.c[1]=*(p++);
	endiannessTest.c[0]=*(p++);
}

	return endiannessTest.i;
}
