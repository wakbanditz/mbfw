/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SprDetectionPreliminary.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SprDetectionPreliminary class.
 @details

 ****************************************************************************
*/

#ifndef SPRDETECTIONPRELIMINARY_H
#define SPRDETECTIONPRELIMINARY_H

#include "SprDetectionCommonInclude.h"
#include "Filters.h"


class SprDetectionPreliminary : public Filters
{

	public:

		SprDetectionPreliminary();

		virtual ~SprDetectionPreliminary();

		int init(vector<unsigned char>* pvImage, Log *pLogger, uint32_t uliWidth, uint32_t uliHeight);

		int perform(eSprResults& eRes);

		const stripEdgesStruct& getEdges(void);

	private:

		float look4White(void);

		void removeCorners(void);

		int removeStripArtefacts(int* pLowEdge, int* pHighEdge);

		int filter(int liLowEdge, int liHighEdge);

    private:

        Log* m_pLogger;

        bool m_bConfigOk;
        vector<unsigned char>* m_pvImage;
        stripEdgesStruct  m_Edge;
};

#endif // SPRDETECTIONPRELIMINARY_H
