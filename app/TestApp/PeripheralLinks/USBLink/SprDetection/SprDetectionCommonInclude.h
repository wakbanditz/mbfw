/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SprDetectionCommonInclude.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the SprDetectionCommonInclude classes.
 @details

 ****************************************************************************
*/

#ifndef SPRDETECTIONCOMMONINCLUDE_H
#define SPRDETECTIONCOMMONINCLUDE_H

#include <vector>
#include <string.h>
#include <deque>


#include "Log.h"
#include "CommonInclude.h"
#include "inttypes.h"


using namespace std;

enum class eSprResults { eUnknown, eGlareOnSpr, eEllipseOutOfLimits, eEllipseMatches, eNoMatch, eGlareInEllipse, eNoGlare, eError };

struct stripEdgesStruct
{
	int liLowerEdge;
	int liUpperEdge;
};

struct infoEllipseStruct
{
	float fAxisX; // semiax x
	float fAxisY; // semiax y
	float fCenterX;
	float fCenterY;
};

// Needed to be float or int
template <typename T>
struct point
{
  T x;
  T y;

  void set(T a, T b)
  {
	  this->x = a;
	  this->y = b;
  }
};


#endif // SPRDETECTIONCOMMONINCLUDE_H
