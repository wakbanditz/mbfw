/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SprDetectionLink.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SprDetectionLink class.
 @details

 ****************************************************************************
*/

#ifndef SPRDETECTION_H
#define SPRDETECTION_H

#include "SprDetectionPreliminary.h"
#include "SprDetectionEllipse.h"

class SprDetectionLink
{
	public:

		SprDetectionLink();

		virtual ~SprDetectionLink();

		int init(const unsigned char* pImage, Log *pLogger, uint32_t uliWidth, uint32_t uliHeight);

		int perform(bool& bIsSprPresent);

		bool readBMP(unsigned char* prgData, string& strFileName);

	private:

		int look4White(vector<unsigned char>& vOriginalImage, const deque<point<int>>& vPointsMin,
					   const deque<point<int>>& vPointsPlus, eSprResults& eRes);

		int32_t getVal4(char* p);

    private:

        bool		  m_bConfigOk;
        unsigned int  m_nImageWidth;

        vector<unsigned char>	 m_vImage;
        vector<unsigned char>*	 m_pOriginalImage;
        Log*					 m_pLogger;
        SprDetectionPreliminary* m_pPreliminaryMan;
        SprDetectionEllipse*	 m_pEllipseMan;

};

#endif // SPRDETECTIONLINK_H
