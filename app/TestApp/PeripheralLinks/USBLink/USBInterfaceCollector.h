/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    USBInterfaceCollector.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the USBInterfaceCollector class.
 @details

 ****************************************************************************
*/

#ifndef USBINTERFACECOLLECTOR_H
#define USBINTERFACECOLLECTOR_H

#include "USBInterface.h"
#include "USBLinkCommonInclude.h"
#include "CommonInclude.h"
#include "Config.h"

typedef enum
{

	eUSB_0 = 0,
	eUSB_1 = 1,
	eUSB_2 = 2,
	eUSB_3 = 3,

} enumUSBChannel; // Only one USB device is used, but can be added more...


class USBInterfaceCollector : public Loggable
{
	public:

		/*! **********************************************************************************************************************
		 * @brief USBInterfaceCollector default constructor.
		 * ***********************************************************************************************************************
		 */
		USBInterfaceCollector();

		/*! **********************************************************************************************************************
		 * @brief USBInterfaceCollector default destructor.
		 * ***********************************************************************************************************************
		 */
		virtual ~USBInterfaceCollector();

		/*! **********************************************************************************************************************
		 * @brief  createInterfaces create and initialize the USB interfaces. (1 at the moment).
		 * @return 0 in case of success, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t createAndInitInterfaces(const char* sConfigFile);

		/*! **********************************************************************************************************************
		 * @brief getInterface get the desired interface specifying eUSbChannel.
		 * @param eUSBChannel is the index of the interface desired.
		 * @return pointer to the USB interface in case of success, NULL otherwise.
		 * ***********************************************************************************************************************
		 */
		USBInterface* getInterfaceByIndex(enumUSBChannel eUSBChannel);

		/*! **********************************************************************************************************************
		 * @brief destroyInterfaces destroys all the interfaces.
		 * ***********************************************************************************************************************
		 */
		void destroyInterfaces(void);

	private:

		/*! **********************************************************************************************************************
		 * @brief  readConfigFile reads configuratin file and stores the info in m_Config.
		 * @param  sConfigFileName configuration file to be read.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool readConfigFile(const char* sConfigFileName);

    protected:

        USBInterface* m_pUSB0;

    private:

        Config m_Config;

};

#endif // USBINTERFACECOLLECTOR_H
