/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionZeroZero.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SampleDetectionZeroZero class.
 @details

 ****************************************************************************
*/

#ifndef SAMPLEDETECTIONZEROZERO_H
#define SAMPLEDETECTIONZEROZERO_H

#include "SampleDetectionBase.h"


class SampleDetectionZeroZero : public SampleDetectionBase
{
	public:

		/*! *************************************************************************************************
		 * @brief SampleDetectionHorizontal default constructor
		 * **************************************************************************************************
		 */
		SampleDetectionZeroZero();

		/*! *************************************************************************************************
		 * @brief ~SampleDetectionZeroZero virtual destructor
		 * **************************************************************************************************
		 */
		virtual ~SampleDetectionZeroZero();

		/*! *************************************************************************************************
		 * @brief  init initialization of the class
		 * @param  pLogger pointer to the logger
		 * @param  uliWidth width of the full image
		 * @param  uliHeight height of the full image
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool init(Log *pLogger, uint32_t uliWidth, uint32_t uliHeight);

        /** *************************************************************************************************
         * @brief  getCropRectangleFromZeroZero analyze the image width the pattern and get the crop rect
         * @param  pulData pointer to the image
         * @return true in case of success, false otherwise
         * **************************************************************************************************
         */
		bool getCropRectangleFromZeroZero_Otsu(uint32_t* pulData);

		/** *************************************************************************************************
		 * @brief  getCropRectangleFromZeroZero analyze the image width the pattern and get the crop rect
		 * @param  pulData pointer to the image
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getCropRectangleFromZeroZero(uint32_t* pulData);

		/*! *************************************************************************************************
		 * @brief  convertCropRectangleZeroZero apply fixed shift to cropRectangle
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool convertCropRectangleZeroZero(void);

		/*! *************************************************************************************************
		 * @brief clearAll reset all the class
		 * **************************************************************************************************
		 */
		void clearAll(void);

	private:

		/*! *************************************************************************************************
		 * @brief  checkRectangleCorrectness check if crop rectangle is acceptable
		 * @param  pulData pointer to the data
		 * @param  rgCropRectangle pointer to the crop rectangle to be checked
		 * @return center position if success, -1 otherwise
		 * **************************************************************************************************
		 */
		int32_t checkRectangleCorrectness(uint32_t* pulData, const uint32_t* rgCropRectangle);

		/*! *************************************************************************************************
		 * @brief  searchPatternFromImage2 get pattern coordinates from the image
		 * @param  pulData pointer to the data
		 * @param  uliImageWidth width of the image
		 * @param  uliImageHeight height of the image
		 * @param  uliCropRectangle cropRectangle found
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool searchPatternFromImage(uint32_t* pulData, uint32_t uliImageWidth, uint32_t uliImageHeight, uint32_t uliCropRectangle[4]);


		bool searchPatternFromImage_Otsu(uint32_t* pulData, int32_t uliImageWidth, int32_t uliImageHeight, uint32_t uliCropRectangle[4]);

		int otsuThresh(const uint32_t* pulData, size_t liSize, float& fOtsuThresh);

		int binarize(uint32_t* pulData, size_t liSize, const float fOtsuThresh);

    private:

        int				m_nImageSize;
        bool			m_bConfigOK;

};

#endif // SAMPLEDETECTIONZEROZERO_H
