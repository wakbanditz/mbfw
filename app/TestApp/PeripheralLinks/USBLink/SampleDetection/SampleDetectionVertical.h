/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionVertical.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SampleDetectionVertical class.
 @details

 ****************************************************************************
*/

#ifndef SAMPLEDETECTIONVERTICAL_H
#define SAMPLEDETECTIONVERTICAL_H

#include "SampleDetectionBase.h"

class SampleDetectionVertical : public SampleDetectionBase
{
	public:

		/*! *************************************************************************************************
		 * @brief SampleDetectionVertical default constructor
		 * **************************************************************************************************
		 */
		SampleDetectionVertical();

		/*! *************************************************************************************************
		 * @brief ~SampleDetectionVertical virtual destructor
		 * **************************************************************************************************
		 */
		virtual ~SampleDetectionVertical();

		// NO ZEROZERO!!!
		/*! *************************************************************************************************
		 * @brief  init initialize SampleDetectionVertical class
		 * @param  pLogger pointer to the logger
		 * @param  pPreliminaryCropRect pointer to the crop rectangle that comes out from the preliminary phase
		 * @param  cWellNum number of the well
         * @param liOriginalWidth the original width
         * @param liXShift
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool init(Log *pLogger, cropRectangle* pPreliminaryCropRect, int8_t cWellNum, int liOriginalWidth, int liXShift);

		/*! *************************************************************************************************
		 * @brief  verticalProcessingX0 vertical processing for X0 (X3 has wierd reflections on the lower part)
		 * @param  pCroppedImage pointer to the image cropped
		 * @param  cDiff vertical shift due to the eventual foil presence
		 * @param  fOutput output of the algorithm (-33.333 to 100)
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool verticalProcessingX0(const uint32_t* pCroppedImage, int8_t cDiff, float& fOutput);

		/*! *************************************************************************************************
		 * @brief  verticalProcessingX3 vertical processing for X3
		 * @param  pCroppedImage pointer to the image cropped
		 * @param  cDiff vertical shift due to the eventual foil presence
		 * @param  fOutput output of the algorithm (-33.333 to 100)
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool verticalProcessingX3(const uint32_t* pCroppedImage, int8_t cDiff, float& fOutput);

		/*! *************************************************************************************************
		 * @brief clearAll reset all the class
		 * **************************************************************************************************
		 */
		void clearAll(void);

	private:

		/*! *************************************************************************************************
		 * @brief  getLimits get the limits for vertical scan
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getLimits();

		/*! *************************************************************************************************
		 * @brief  scanImageAndCreateWaves function used to get waves from images
		 * @param  pulData pointer to the image
		 * @param  pulWaves pointer to the wave to be created
		 * @param  uliStartIndex start index from where getting the waves
		 * @param  uliStopIndex stop index from where getting the waves
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool scanImageAndCreateWaves(uint32_t* pulData, uint32_t* pulWaves, uint32_t uliStartIndex, uint32_t uliStopIndex, uint8_t ucWindowSize);

		/*! *************************************************************************************************
		 * @brief  findPeaks find all the peaks in the data passed
		 * @param  pulData pointer to data
		 * @param  uliSize size of the data to be considered
		 * @param  pPeaksStruct pointer to peaks struct where the result is stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool findPeaks(uint32_t* pulData, uint32_t uliSize, peaksStruct* pPeaksStruct);

		/*! *************************************************************************************************
		 * @brief  proximityCheck check if peaks are more or less in the expacted (relatively large) band
		 * @param  pLeftPeaksStruct struct with the left peaks
		 * @param  pCentralPeaksStruct struct with the central peaks
		 * @param  pRightPeaksStruct struct with the right peaks
		 * @param  cDiff vertical shift due to the eventual foil presence
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool proximityCheck(peaksStruct* pLeftPeaksStruct, peaksStruct* pCentralPeaksStruct, peaksStruct* pRightPeaksStruct, int8_t cDiff);

		/*! *************************************************************************************************
		 * @brief  peaksClosenessCheck check if the peaks are close one another
		 * @param  pLeftPeaksStruct struct with the left peaks
		 * @param  pCentralPeaksStruct struct with the central peaks
		 * @param  pRightPeaksStruct struct with the right peaks
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool peaksClosenessCheck(peaksStruct* pLeftPeaksStruct, peaksStruct* pCentralPeaksStruct, peaksStruct* pRightPeaksStruct);

		/*! *************************************************************************************************
		 * @brief  peaksAmplitudeCheck check if the amplitude of the peaks is similar
		 * @param  pLeftPeaksStruct struct with the left peaks
		 * @param  pCentralPeaksStruct struct with the central peaks
		 * @param  pRightPeaksStruct struct with the right peaks
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool peaksAmplitudeCheck(peaksStruct* pLeftPeaksStruct, peaksStruct* pCentralPeaksStruct, peaksStruct* pRightPeaksStruct);

		/*! *************************************************************************************************
		 * @brief  checkOtherPeaksPresenceFirstPart check if there are other peaks due to liquid
		 * @param  pPeaksStruct pointer to the peaks struct
		 * @param  liDoNotConsiderId skip that peak
		 * @return -1 * every_other_peak_found
		 * **************************************************************************************************
		 */
		int32_t checkOtherPeaksPresenceFirstPart(peaksStruct* pPeaksStruct, int32_t liDoNotConsiderId = -1);

		/*! *************************************************************************************************
		 * @brief  checkOtherPeaksPresenceLastPart check if there are other peaks due to liquid
		 * @param  pPeaksStruct pointer to the peaks struct
		 * @param  liDoNotConsiderId skip that peak
		 * @return -1 * every_other_peak_found
		 * **************************************************************************************************
		 */
		int32_t checkOtherPeaksPresenceLastPart(peaksStruct* pPeaksStruct, int32_t liDoNotConsiderId = -1);

    private:

        int32_t					m_nImageSize;
        int32_t					m_nInitialWidth;
        int32_t					m_nXShift;
        bool					m_bConfigOK;

        verticalScansLimits		m_VerticalLimits;
        centerProximity			m_CenterProx;
        differencePeaksPos		m_PeaksPos;
        differencePeaksAmpl		m_PeaksAmpl;

};

#endif // SAMPLEDETECTIONVERTICAL_H
