/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    Filters.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the Filters class.
 @details

 ****************************************************************************
*/

#ifndef FILTERS_H
#define FILTERS_H

#include <string>
#include <string.h>
#include <vector>
#include <math.h>

#include "inttypes.h"
#include "CR8062Include.h"

#define ROUND_TO_DECIMAL_FIGURES(x)				(((x)%DECIMAL_DIGITS_FACTOR == 0) ? (x) : ((x)+ DECIMAL_DIGITS_FACTOR/2))
#define FILTER_CANNY_MULTIPL_FACTOR				10
#define FILTER_HISTO_BINS_NUM					64
#define FILTER_CANNY_PERC_OF_PIXELS_NOT_EDGES	70
#define FILTER_CANNY_THRESHOLD_RATIO			0.4f
#define FILTER_FILL_SIZE						8


typedef enum
{
	eHorizontal,
	eVertical
}eCannyDirection;


typedef struct
{
	float  fLowThresh;
	float  fHighThresh;
} structCannyThresholds;


class Filters
{
	public:

		/*! **********************************************************************************************************************
		 * @brief Filters default constructor.
		 * ***********************************************************************************************************************
		 */
		Filters();

		/*! **********************************************************************************************************************
		 * @brief ~Filters virtual destructor of the class.
		 * ***********************************************************************************************************************
		 */
		virtual ~Filters();

		/*! **********************************************************************************************************************
		 * @brief setImageWidth assign value to member m_nImageWidth.
		 * @param liWidth value to be set.
		 * ***********************************************************************************************************************
		 */
		void setImageWidth(int32_t liWidth);

		/*! **********************************************************************************************************************
		 * @brief setImageHeight assign value to member m_nImageHeight.
		 * @param liHeight value to be set.
		 * ***********************************************************************************************************************
		 */
		void setImageHeight(int32_t liHeight);

		/*! **********************************************************************************************************************
		 * @brief  movingAverageFilter
		 * @param  pulRawData pointer to the data to be filtered - data change!.
		 * @param  liSize of the data to be filtered.
		 * @param  liWindowSize size of the window (1D).
		 * @return true if succeeded, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool movingAverageFilter(uint32_t* pulRawData, int32_t liSize, int32_t liWindowSize);


		/*! **********************************************************************************************************************
		 * @brief  sobel
		 * @param  pulRawData pointer to the data to be filtered - data change!.
		 * @param  uliWidth and @param uliHeight of the image where the filter will be applied
		 * @return true if succeeded, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool sobel(uint32_t* pulRawData, uint32_t uliWidth, uint32_t uliHeight);


		/*! **********************************************************************************************************************
		 * @brief  imAdjust maps the intensity values of the grayScale image saturating the bottom 1% and the top 1% of all pixel
		 *			 	    values. This is really slow compared to the others, because it has to sort all the elements
		 * @param  pulRawData pointer to the data to be filtered - data change!.
		 * @param  uliSize of the data.
		 * @return true if succeeded, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool imAdjust(uint32_t* pulRawData, uint32_t uliSize);


		/*! **********************************************************************************************************************
		 * @brief  medFilt2 performs median filtering of the image in 2 dimensions.
		 * @param  pulRawData pointer to the data to be filtered - data change!.
		 * @param  liRowWidth and @param liColumnHeight of the image to which the filter will be applied
		 * @param  liMedianFilterMatrix matrix with the filter size m x n.
		 * @return true if succeeded, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool medFilt2(uint32_t *pulRawData, int32_t liRowWidth, int32_t liColumnHeight, uint8_t liMedianFilterMatrix[2]);

		/*! **********************************************************************************************************************
		 * @brief  _opt_medFilt2 optimized version (for odd x odd filter matrices) of the previous algo, for unsigned char buff
		 * ***********************************************************************************************************************
		 */
		bool _opt_medFilt2(unsigned char* pucRawData, int32_t liRowWidth, int32_t liColumnHeight, uint8_t liMedianFilterMatrix[2]);

		/*! **********************************************************************************************************************
		 * @brief  medFilt1 performs median filtering of the image in 1 dimension.
		 * @param  pulRawData pointer to the data to be filtered - data change!.
		 * @param  liDataLength is the size of the image of the image to which the filter will be applied
		 * @param  ucFilterSize is the size of the filter (1D).
		 * @return true if succeeded, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool medFilt1(uint32_t *pulRawData, int32_t liDataLength, uint8_t ucFilterSize);

		/*! **********************************************************************************************************************
		 * @brief  cannyFilter Canny filter implemented exactly as Canny wrote in his papers and equal to the Matlab version.
		 * @param  pulRawData pointer to the raw image to be filtered.
		 * @param  pulEdgeData pointer to the filtered image (output). It's a 0-1 image, with the same size of the raw image.
		 * @param  uliRowWidth width of the image.
		 * @param  uliColumnHeight height of the image
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool cannyFilter(uint32_t* pulRawData, uint32_t* pulEdgeData, uint32_t uliRowWidth, uint32_t uliColumnHeight);

		/*! **********************************************************************************************************************
		 * @brief  highPassFilter set to 0 all the pixels with value lower than the one chosen.
		 * @param  pulRawData pointer to the raw image to be filtered.
		 * @param  liImageSize size of the image to be filtered.
		 * @param  uliExtreme threshold value. Pixels lower than the latter are set to 0.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool highPassFilter(uint32_t* pulRawData, uint32_t liImageSize, uint32_t uliExtreme = (100 * DECIMAL_DIGITS_FACTOR));

		/*! **********************************************************************************************************************
		 * @brief  customImadjust imadjust customized considering only a part of the image for the stratching of the values
		 * @param  pulRawData pointer to the raw image to be filtered.
		 * @param  y0 start of the area to be considered along y direction.
		 * @param  uliHeight height of the area to be considered.
		 * @param  uliWidth width of the area to be considered.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool customImadjust(uint32_t* pulRawData, uint32_t y0, uint32_t uliHeight, uint32_t uliWidth = (10+1));

		/*! *************************************************************************************************
		 * @brief  histCounts matlab function replaced, used to get the histogram of a certain data array
		 * @param  vData const reference to the data taken into account
		 * @param  liBinNum number of bins (divisions of the data values) we want to have
		 * @param  vOccurrences occurrence of values in each bin (output)
		 * @param  vEdges vector containing all the bin edges (output)
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool histCounts(const std::vector<uint32_t>& vData, int liBinNum, std::vector<uint32_t>& vOccurrences, std::vector<float>& vEdges);

		/** *************************************************************************************************
		 * @brief mode calculate mode of the array. If 2 values have the same occurrences num, than the lower one is returned.
		 * @param pData pointer to the data where the mode has to be evaluated
		 * @param uliDataLength size of the data
		 * @param pModeVal pointer to value where the mode will be stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool mode(uint32_t* pData, size_t uliDataLength, uint32_t* pModeVal);

	private:

		/*! **********************************************************************************************************************
		 * @brief  median
		 * @param  pulRawData pointer to the data where the median has to be found.
		 * @param  uliSize size of the array.
		 * @return median value.
		 * ***********************************************************************************************************************
		 */
		uint32_t median(uint32_t* pulRawData, uint32_t uliSize);
		unsigned char median(unsigned char* ucRawData, uint32_t uliSize);

		/*! **********************************************************************************************************************
		 * @brief  _opt_median super optimized median for 25
		 * @param  ucData pointer to the data where the median has to be found.
		 * @return median value.
		 * ***********************************************************************************************************************
		 */
		unsigned char _opt_median(unsigned char* ucData, int size);

		/*! **********************************************************************************************************************
		 * @brief swap
		 * @param a first element to be swapped.
		 * @param b second element to be swapped.
		 * ***********************************************************************************************************************
		 */
		void swap(uint32_t* a, uint32_t* b);

		/*! **********************************************************************************************************************
		 * @brief  partition function needed to be called into quickSort.
		 * @param  pulArr pointer to the data to be partitioned.
		 * @param  uliFirst first element.
		 * @param  uliLast last element.
		 * @return index.
		 * ***********************************************************************************************************************
		 */
		uint32_t partition(uint32_t* pulArr, uint32_t uliFirst, uint32_t uliLast);

		/*! **********************************************************************************************************************
		 * @brief quickSort sort the element of an array.
		 * @param pulArr pointer to the data to be sorted.
		 * @param uliFirst first element of the array.
		 * @param uliLast last element of the array.
		 * ***********************************************************************************************************************
		 */
		void quickSort(uint32_t* pulArr, uint32_t uliFirst, uint32_t uliLast);


		// Functions used for Canny filter
		// Look at matlab for descriptions
		/*! **********************************************************************************************************************
		 * @brief normalizeImage takes an image as input and normalize it (0-1)
		 * @param pfData pointer to the image.
		 * @param uliImageSize image length.
		 * ***********************************************************************************************************************
		 */
		void normalizeImage(float* pfData, uint32_t uliImageSize);

		/*! **********************************************************************************************************************
		 * @brief  smoothGradient creates 2 arrays from the image (that has to be normalized) where a derivative 8x8 gaussian
		 *		   filter is applied along X and Y direction.
		 * @param  pulRawData pointer to the data to be filtered.
		 * @param  rgGX array with the same size of pulRawData. Output along x axis.
		 * @param  rgGY array with the same size of pulRawData. Output along y axis.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool smoothGradient(float* pulRawData, float* rgGX, float* rgGY);

		// 1-D gradient
		/*! **********************************************************************************************************************
		 * @brief  gradient create an 1-D numerical gradient of the array passed as input.
		 * @param  pfInData data where the function has to be applied.
		 * @param  pfOutFilt pointer to an array of equal size of pfInData. It's the output of the function.
		 * @return true in case of success, fale otherwise.
		 * ***********************************************************************************************************************
		 */
		bool gradient(float* pfInData, float* pfOutFilt);

		// Note the 2 functions gives different outputs (only in sign, not magnitude).
		/*! **********************************************************************************************************************
		 * @brief  imageFilterA computes smoothed numerical gradient of the array. Can be horizontal or vertical).
		 * @param  pfData pointer to the array to be filtered.
		 * @param  pfFilteredData pointer to an array of equal size of pfData. It's the output of the function.
		 * @param  pfKernel kernel to be applied at the input.
		 * @param  eDir to decide the direction of the filter.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool imageFilterA(float* pfData, float* pfFilteredData, float* pfKernel, eCannyDirection eDir);

		/*! **********************************************************************************************************************
		 * @brief  imageFilterB computes smoothed numerical gradient of the array. Can be horizontal or vertical).
		 * @param  pfData pointer to the array to be filtered.
		 * @param  pfFilteredData pointer to an array of equal size of pfData. It's the output of the function.
		 * @param  pfKernel kernel to be applied at the input.
		 * @param  eDir to decide the direction of the filter.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool imageFilterB(float* pulData, float* pulFilteredData, float* pfKernel, eCannyDirection eDir);

		/*! **********************************************************************************************************************
		 * @brief elevArraysToPot computes square root of the sum of the squares of each elements of A and B.
		 * @param pArrA pointer to array A.
		 * @param pArrB pointer to array B.
		 * @param pArrRes pointer to the resulting array. They all have to be of the same size.
		 * @param uliLength size of the arrays.
		 * ***********************************************************************************************************************
		 */
		void elevArraysToPot(float* pArrA, float* pArrB, float* pArrRes, uint32_t uliLength);

		/*! **********************************************************************************************************************
		 * @brief  selectThresholds determine hysteresis thresholds limits.
		 * @param  prgMagn array in input.
		 * @param  uliMagnLength its length.
		 * @return structCannyThresholds, which contains low and high thresholds.
		 * ***********************************************************************************************************************
		 */
		structCannyThresholds selectThresholds(float* prgMagn, uint32_t uliMagnLength);

		/*! **********************************************************************************************************************
		 * @brief binarySearch allowes to find which range all the elements of the array passed as input belongs to.
		 * @param prgMagn pointer to the input array.
		 * @param rgiCounts pointer to an array where the counts are stored. Length = 64.
		 * @param liThresholdsTable64 pointer to the various thresholds.
		 * ***********************************************************************************************************************
		 */
		void binarySearch(const float* prgMagn, uint32_t* rgiCounts, float* liThresholdsTable64);

		/*! **********************************************************************************************************************
		 * @brief  cannyFindLocalMaxima function explained in its body.
		 * @param  ucDir direction can be a value from 0 to 4.
		 * @param  rgGX array with smoothGradient function applied along X.
		 * @param  rgGY array with smoothGradient function applied along Y.
		 * @param  rgMagnitude magnitude of rgGX and rgGY.
		 * @return vector of the index of the local maxima.
		 * ***********************************************************************************************************************
		 */
		bool cannyFindLocalMaxima(uint8_t ucDir, float* rgGX, float* rgGY, float* rgMagnitude, std::vector<int>& vIdxLocalMax);

		/*! **********************************************************************************************************************
		 * @brief  objectSelects selects objects in binary image. Used to keep only the pixel with value > lowThreshold and
		 *		   < highThresholds, but that are in contact with other pixels > highThreshold.
		 * @param  pData pointer to the image with high & low thresholds set to 255.
		 * @param  pMarker pointer to the output image.
		 * @param  vMaskIdx vector with idxs of the points whose value is > than the high threshold
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool objectSelects(float* pData, uint32_t* pMarker, std::vector<int> vMaskIdx);

		/*! **********************************************************************************************************************
		 * @brief  floodFill classical flood fill algorithm.
		 * @param  pliData array with only elements > lowThresholds = 255.
		 * @param  pImgMarker image with only the pixel with value > lowThreshold and < highThresholds set to 255, but that are in
		 *		   contact with other pixels > highThreshold.
		 * @param  liMaskId starting point of the algorithm.
		 * @param  vMaskIdChecked vector with the id of the points already checked.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool floodFill(float* pliData, uint32_t* pImgMarker, uint32_t liMaskId, std::vector<int>& vMaskIdChecked);

		/*! **********************************************************************************************************************
		 * @brief  find find presence of a value in a vector.
		 * @param  vVector vector to be parsed.
		 * @param  liValue value to be checked.
		 * @return true if the value is found, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool find(const std::vector<int>& vVector, int liValue);

    protected:

        int32_t	m_nImageWidth;
        int32_t m_nImageHeight;

    private:

        int32_t	m_nKernelSize;

};

#endif // FILTERS_H
