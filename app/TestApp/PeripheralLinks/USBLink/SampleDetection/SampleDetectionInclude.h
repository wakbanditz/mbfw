/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionInclude.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the SampleDetection classes.
 @details

 ****************************************************************************
*/

#ifndef STRUCTS_H
#define STRUCTS_H

#include <dirent.h>
#include <string.h>
#include <fstream>

#include "inttypes.h"
#include "CR8062Include.h"

// General
#define WELL_ZERO	0
#define WELL_THREE	3

// Zero Zero

#define HORIZONTAL_EDGE_WIDTH				( 70 * DECIMAL_DIGITS_FACTOR )
#define HORIZONTAL_MARGIN					3
#define VERTICAL_EDGE_WIDTH					( 3  * DECIMAL_DIGITS_FACTOR ) // this should be modified if used
#define VERTICAL_EDGE_WIDTH_2				( 60  * DECIMAL_DIGITS_FACTOR )
#define VERTICAL_MARGIN						3
#define VERTICAL_RATIO						1.5f

// image height = 160 pixels; pattern width around 20 pixels
// distance between pattern and vertical mark: around 50 pixels
// so we are chosing 60 to be sure!!
#define DISTANCE_BETWEEN_PATTERNS			60

#define DIFFERENCE_BETWEEN_PATTERNS			45

#define WIDTH_IMAGE_TO_BE_PROCESSED			45
#define WIDTH_IMAGE_TO_BE_PROCESSED_2		( WIDTH_IMAGE_TO_BE_PROCESSED / 2 )
#define HEIGHT_IMAGE_TO_BE_PROCESSED		40
#define HEIGHT_IMAGE_TO_BE_PROCESSED_2		( HEIGHT_IMAGE_TO_BE_PROCESSED / 2 )


// Vertical Algorithm
#define VER_DX								0
#define VER_DY								4
#define VER_DW								0
#define VER_DH								(-8)
#define VER_OFFSET_X3						3
#define VER_ADMITTED_DISPLACEMENT			3
#define VER_MIN_PEAK_AMPLITUDE_ADMITTED		( 20 * DECIMAL_DIGITS_FACTOR )
#define VER_SHIFT_TO_RETRY					4
#define VER_MAX_INTERPEAK_DISANCE			2
#define VER_MAX_INTERPEAK_AMPLITUDE			( 20 * DECIMAL_DIGITS_FACTOR )
#define VER_PEAKS_CLOSENESS_POS				5
#define VER_FOIL_RADIUS						24
#define VER_HALF_CROWN						2//3
#define VER_FOIL_THRESHOLD					75.0f

// Horizontal Algorithm
#define HOR_DX								1
#define HOR_DY								4
#define HOR_DW								0
#define HOR_DH								(-8)
#define HOR_MAX_OFFSET_FOR_EACH_RETRY		3
#define HOR_MAX_OFFSET_ADMITTED				8
#define HOR_POSITIVE_SCORE					100
#define HOR_NEGATIVE_SCORE					150

// Common defines
#define BANDS_WIDTH							1
#define BANDS_HEIGHT						24
#define BLACK_BAND_DISPLACEMENT_FROM_CENTER	8
#define LOWER_LIMIT_SCORE					(-33.333f)


#define HALVE_AND_ROUND_UP(x)				(((x)%2 == 0) ? ((x)/2) : (((x)+1)/2)) // only for integer - MATLAB round up x.5, here no.


using namespace std;

typedef struct verticalScansLimits_tag{
		int32_t xStartIndexLeftBand;
		int32_t xEndIndexLeftBand;
		int32_t xStartIndexCentralBand;
		int32_t xEndIndexCentralBand;
		int32_t xStartIndexRightBand;
		int32_t xEndIndexRightBand;
		int32_t yStartIndex;
		int32_t yEndIndex;
}verticalScansLimits;


typedef struct horizontalScansLimits_tag{
		int32_t yStartIndexHighBand;
		int32_t yEndIndexHighBand;
		int32_t yStartIndexCentralBand;
		int32_t yEndIndexCentralBand;
		int32_t yStartIndexLowBand;
		int32_t yEndIndexLowBand;
		int32_t xStartIndex;
		int32_t xEndIndex;
}horizontalScansLimits;


typedef struct cropRectangle_tag{
		uint32_t xStart;
		uint32_t yStart;
		uint32_t width;
		uint32_t height;
}cropRectangle;


typedef struct centerProximity_tag{
		uint32_t left;
		uint32_t center;
		uint32_t right;
}centerProximity;


typedef struct differencePeaksPos_tag{
		uint32_t left;
		uint32_t center;
		uint32_t right;
}differencePeaksPos;


typedef struct differencePeaksAmpl_tag{
		uint32_t left;
		uint32_t center;
		uint32_t right;
}differencePeaksAmpl;


typedef struct peaksStruct_tag{
	uint32_t peaksNum;
	int32_t centeredPeakId;
	vector <uint32_t> peaksAmplitude;
	vector <uint32_t> peaksPos;
}peaksStruct;


typedef struct outsidePeaks_tag{
	int8_t leftFirstPart;
	int8_t leftLastPart;
	int8_t centerFirstPart;
	int8_t centerLastPart;
	int8_t rightFirstPart;
	int8_t rightLastPart;
}outsidePeaks;


typedef struct idPeaksAssignment_tag{
	int8_t posPeakLeftScan;
	int8_t posPeakCentralScan;
	int8_t posPeakRightScan;
}idPeaksAssignment;

typedef struct possibleFoil_tag{
	int8_t cLeftLowLimit;
	int8_t cLeftHighLimit;
	int8_t cCenterLowLimit;
	int8_t cCenterHighLimit;
	int8_t cRightLowLimit;
	int8_t cRightHighLimit;
}possibleFoil;


typedef struct circumferenceCoefficients_tag{
	float a;
	float b;
	float c;
}circumferenceCoefficients;


typedef struct verticalProcessingOutput_tag{
	float output;
	float a;
	float b;
	float c;
}verticalProcessingOutput;


typedef struct bitCollection_tag{
	vector <uint32_t>  ulUpperBand;
	vector <uint32_t>  ulCentralBand;
	vector <uint32_t>  ulLowerBand;
} bitCollectionStruct;


typedef struct AVGstruct_tag{
	vector <int32_t>  ulUpperBand;
	vector <int32_t>  ulCentralBand;
	vector <int32_t>  ulLowerBand;
}AVGstruct;


typedef struct differenceStruct_tag{
	vector <int32_t>  ilUpperDifference;
	vector <int32_t>  ilLowerDifference;
	int32_t ilUpperIntegral;
	int32_t ilLowerIntegral;
}differenceStruct;


//Utility functions

int32_t getVal4(char *p);
int getdir (string strDir, vector<string> &svFiles);



#endif // STRUCTS_H
