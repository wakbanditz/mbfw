/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionVertical.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SampleDetectionVertical class.
 @details

 ****************************************************************************
*/

#include "SampleDetectionVertical.h"

#define CENTER_EYE_CABLED						12
#define CENTER_EYE_MAGIC_VALUE_1				1
#define CENTER_EYE_MAGIC_VALUE_7				7
#define CENTER_EYE_MAGIC_VALUE_10				10
#define CENTER_EYE_MAGIC_VALUE_13				13
#define CENTER_EYE_MAGIC_VALUE_24				24
#define CENTER_EYE_MAGIC_VALUE_29				29

SampleDetectionVertical::SampleDetectionVertical()
{
	m_nImageSize = -1;
	m_nXShift = 0;
	m_bConfigOK = false;

	memset(&m_VerticalLimits, 0x00, sizeof(verticalScansLimits));
	memset(&m_CenterProx, 0x00, sizeof(centerProximity));
	memset(&m_PeaksPos, 0x00, sizeof(differencePeaksPos));
	memset(&m_PeaksAmpl, 0x00, sizeof(differencePeaksAmpl));
}

SampleDetectionVertical::~SampleDetectionVertical()
{

}

bool SampleDetectionVertical::init(Log* pLogger, cropRectangle* pPreliminaryCropRect, int8_t cWellNum, int liOriginalWidth, int liXShift)
{
	m_bConfigOK = false;

	if ( ! setLogger(pLogger) )						return false;
	if ( ! setCropRectangle(pPreliminaryCropRect) )	return false;
	if ( ! setWellNumber(cWellNum) )				return false;

	setImageHeight(pPreliminaryCropRect->height);
	setImageWidth(pPreliminaryCropRect->width);
	m_nImageSize = m_nImageHeight * m_nImageWidth;
	m_nInitialWidth = liOriginalWidth;
	m_nXShift = liXShift;

	m_bConfigOK = true;

	return true;
}

bool SampleDetectionVertical::verticalProcessingX0(const uint32_t* pCroppedImage, int8_t cDiff, float& fOutput)
{
	if ( pCroppedImage == 0 )			return false;
	if ( m_bConfigOK == false )			return false;

	vector<uint32_t> vImFiltered(pCroppedImage, pCroppedImage+m_nImageSize);
	uint8_t rgcMedianFilterMatrix[2] = {4, 2};

	if ( ! pretreatImage(vImFiltered.data(), rgcMedianFilterMatrix, m_cropRectangle.width, m_cropRectangle.height) )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionPreliminary::performPreliminaryChecks: failed pretreating the image.");
		return false;
	}

	// Calculate vertical scans limits.
	getLimits();

	uint8_t cAVGWindowSize = 3;

	uint16_t usiArraySize = m_cropRectangle.height + 1;

	vector<uint32_t> vuiWavesCenter(usiArraySize);
	vector<uint32_t> vuiWavesRight(usiArraySize);
	vector<uint32_t> vuiWavesLeft(usiArraySize);

	// Previously there was a different scanImageAndCreateWaves for X3
	scanImageAndCreateWaves(vImFiltered.data(), vuiWavesLeft.data(), m_VerticalLimits.xStartIndexLeftBand, m_VerticalLimits.xEndIndexLeftBand, cAVGWindowSize);
	scanImageAndCreateWaves(vImFiltered.data(), vuiWavesCenter.data(), m_VerticalLimits.xStartIndexCentralBand, m_VerticalLimits.xEndIndexCentralBand, cAVGWindowSize);
	scanImageAndCreateWaves(vImFiltered.data(), vuiWavesRight.data(), m_VerticalLimits.xStartIndexRightBand, m_VerticalLimits.xEndIndexRightBand, cAVGWindowSize);

	peaksStruct leftPeaks;
	peaksStruct centralPeaks;
	peaksStruct rightPeaks;

	bool bRes = findPeaks(vuiWavesLeft.data(), usiArraySize, &leftPeaks);

	if ( bRes )
	{
		bRes = findPeaks(vuiWavesCenter.data(), usiArraySize, &centralPeaks);
	}
	if ( bRes )
	{
		bRes = findPeaks(vuiWavesRight.data(), usiArraySize, &rightPeaks);
	}
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionVertical::verticalProcessingX0: error in finding peaks waves");
		return false;
	}

	/*! ********************************************************************************
	 **** Check number 1: are there 3 peaks (one per scan) in the central position? ****
	********************************************************************************* **/
	// Reset centeredPeakId values
	leftPeaks.centeredPeakId = -1;
	centralPeaks.centeredPeakId = -1;
	rightPeaks.centeredPeakId = -1;

	bRes = proximityCheck(&leftPeaks, &centralPeaks, &rightPeaks, cDiff);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionVertical::verticalProcessingX0: error in checking the proximity of the peaks");
		return false;
	}

	uint32_t uliPartialScore = m_CenterProx.left + m_CenterProx.center + m_CenterProx.right;


	/*! *********************************************************************************
	 ******** Check number 2: Are the 3 central peaks close enough one another? *********
	********************************************************************************** **/

	if ( uliPartialScore == 3 )
	{
		peaksClosenessCheck(&leftPeaks, &centralPeaks, &rightPeaks);
	}


	/*! *********************************************************************************
	 ********* Check number 3: Are the 3 central peaks with similar amplitude? **********
	********************************************************************************** **/
	if ( uliPartialScore == 3 )
	{
		peaksAmplitudeCheck(&leftPeaks, &centralPeaks, &rightPeaks);
	}


	/*! *********************************************************************************
	 ************** Check number 4: Are there more peaks groups? Count them *************
	********************************************************************************** **/

	outsidePeaks otherPeaks;
	otherPeaks.leftFirstPart = checkOtherPeaksPresenceFirstPart(&leftPeaks);
	otherPeaks.centerFirstPart = checkOtherPeaksPresenceFirstPart(&centralPeaks);
	otherPeaks.rightFirstPart = checkOtherPeaksPresenceFirstPart(&rightPeaks);

	otherPeaks.leftLastPart = checkOtherPeaksPresenceLastPart(&leftPeaks);
	otherPeaks.centerLastPart = checkOtherPeaksPresenceLastPart(&centralPeaks);
	otherPeaks.rightLastPart = checkOtherPeaksPresenceLastPart(&rightPeaks);


	/*! *********************************************************************************
	 *********************************  Calculate output ********************************
	********************************************************************************** **/

	fOutput = uliPartialScore + m_PeaksPos.left + m_PeaksPos.center + m_PeaksPos.right;
	fOutput += ( m_PeaksAmpl.left + m_PeaksAmpl.center + m_PeaksAmpl.right );
	fOutput += ( otherPeaks.leftFirstPart + otherPeaks.centerFirstPart + otherPeaks.rightFirstPart );
	fOutput += ( otherPeaks.leftLastPart + otherPeaks.centerLastPart + otherPeaks.rightLastPart );
	fOutput *= (100.0f/9.0f); // to normalize the output to 100

	if ( fOutput < LOWER_LIMIT_SCORE )
	{
		fOutput = LOWER_LIMIT_SCORE;
	}

	return true;
}

bool SampleDetectionVertical::verticalProcessingX3(const uint32_t* pCroppedImage, int8_t cDiff, float& fOutput)
{
	if ( pCroppedImage == nullptr )		return false;
	if ( m_bConfigOK == false )			return false;

	// Calculate vertical scans limits.
	getLimits();

	uint8_t ucCenter = CENTER_EYE_CABLED + cDiff;
	int liLowLimit = ( ucCenter > CENTER_EYE_MAGIC_VALUE_10 ) ? ucCenter-CENTER_EYE_MAGIC_VALUE_10 : CENTER_EYE_MAGIC_VALUE_1;
	int liHighLimit = ( ucCenter < CENTER_EYE_MAGIC_VALUE_24 ) ? ucCenter+CENTER_EYE_MAGIC_VALUE_7 : CENTER_EYE_MAGIC_VALUE_29;

	vector<uint32_t> vImFiltered(pCroppedImage, pCroppedImage+m_nImageSize);

	if ( ! customImadjust(vImFiltered.data(), liLowLimit, liHighLimit-liLowLimit+1) )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionVertical::verticalProcessingX3: unable to apply customized imadjust filter");
		return false;
	}

	bool bRes = highPassFilter(vImFiltered.data(), m_nImageSize);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionVertical::verticalProcessingX3: unable to apply high pass filter");
		return false;
	}

	int liXStart = CENTER_EYE_MAGIC_VALUE_10 - 1;
	int liXStop = m_nImageWidth - CENTER_EYE_MAGIC_VALUE_10;
	int liYStop = m_nImageHeight / 2;

	if ( liXStart == liXStop )	return 0;

	vector<float> vContainer(liXStop - liXStart);

	for ( int i = liXStart; i != liXStop; i++ )
	{
		for ( int l = 0; l != liYStop; l++ )
		{
			if ( ( vImFiltered[l*m_nImageWidth+i] != 0 &&  vImFiltered[(l+1)*m_nImageWidth+i] != 0  ) &&  vImFiltered[(l+2)*m_nImageWidth+i] != 0 )
			{
				if ( l < 2 )
				{
					vContainer[i-liXStart] = (float)(rand()%(8000)) / 1000.0f;
					break;
				}
				else
				{
					vContainer[i-liXStart] = l;
					break;
				}
			}
		}
		if ( vContainer[i-liXStart] == 0 )
		{
			vContainer[i-liXStart] = (float)(rand()%(10000)) / 1000.0f;
		}
	}

	/*! *********************************************************************************
	 *********************************  Calculate output ********************************
	********************************************************************************** **/

	int liSize = vContainer.size();
	float fAVG = 0;
	for ( int i = 0; i != liSize; i++ )
	{
		fAVG += vContainer[i];
	}
	fAVG /= vContainer.size();

	float fSum = 0;
	for ( int i = 0; i != liSize; i++ )
	{
		fSum += (vContainer[i] > fAVG) ? (vContainer[i]-fAVG) : (fAVG-vContainer[i]);
	}

	fOutput = fSum / liSize;
	// < 0.25 is 100, than linear until 1 = 70, than linear 2 = 0.
	fOutput = ( fOutput <= 1 ) ? (fOutput * (-54.5f) + 117.5f) : (fOutput * (-58.3f) + 116.6f);

	if ( fOutput > 100.0 )
	{
		fOutput = 100;
	}
	else if ( fOutput < LOWER_LIMIT_SCORE )
	{
		fOutput = LOWER_LIMIT_SCORE;
	}

	return true;
}

void SampleDetectionVertical::clearAll()
{
	m_nImageSize = -1;
	m_bConfigOK = false;
	m_pLogger = 0;
	m_cWellNumber = -1;

	memset(&m_VerticalLimits, 0x00, sizeof(verticalScansLimits));
	memset(&m_CenterProx, 0x00, sizeof(centerProximity));
	memset(&m_PeaksPos, 0x00, sizeof(differencePeaksPos));
	memset(&m_PeaksAmpl, 0x00, sizeof(differencePeaksAmpl));
	memset(&m_cropRectangle, 0x00, sizeof(cropRectangle));
}

bool SampleDetectionVertical::getLimits()
{
	if ( m_bConfigOK == false )		return -1;

    // It is important to keep the original center in x, because it is the correct one. (label do not change the real position of the center!)
	uint32_t xCenter = m_nInitialWidth / 2 - 1;			// because the index starts from 0 and not from 1 like Matlab
	uint32_t yCenter = m_cropRectangle.height / 2 - 1;	// because the index starts from 0 and not from 1 like Matlab

	if ( xCenter < BLACK_BAND_DISPLACEMENT_FROM_CENTER+BANDS_WIDTH )							return -1;
	if ( xCenter+BLACK_BAND_DISPLACEMENT_FROM_CENTER+BANDS_WIDTH >= m_cropRectangle.width )		return -1;

	// For the moment the two limits are the same, but eventually they can be different.
	switch( getWellNumber() )
	{
		case 0:
			m_VerticalLimits.xStartIndexLeftBand = xCenter - BLACK_BAND_DISPLACEMENT_FROM_CENTER - BANDS_WIDTH;
			m_VerticalLimits.xEndIndexLeftBand = xCenter - BLACK_BAND_DISPLACEMENT_FROM_CENTER;
			m_VerticalLimits.xStartIndexCentralBand = xCenter - HALVE_AND_ROUND_UP(BANDS_WIDTH);
			m_VerticalLimits.xEndIndexCentralBand = xCenter + HALVE_AND_ROUND_UP(BANDS_WIDTH);
			m_VerticalLimits.xStartIndexRightBand = xCenter + BLACK_BAND_DISPLACEMENT_FROM_CENTER;
			m_VerticalLimits.xEndIndexRightBand = xCenter + BLACK_BAND_DISPLACEMENT_FROM_CENTER + BANDS_WIDTH;
			m_VerticalLimits.yStartIndex = ( yCenter >= HALVE_AND_ROUND_UP(BANDS_HEIGHT) ) ? yCenter - HALVE_AND_ROUND_UP(BANDS_HEIGHT) : 0;
			m_VerticalLimits.yEndIndex = (( yCenter + BANDS_HEIGHT / 2 ) < m_cropRectangle.width )?
										  ( yCenter + BANDS_HEIGHT / 2 ) : (m_cropRectangle.width - 1);
		break;

		case 3:
			m_VerticalLimits.xStartIndexLeftBand = xCenter - BLACK_BAND_DISPLACEMENT_FROM_CENTER - BANDS_WIDTH;
			m_VerticalLimits.xEndIndexLeftBand = xCenter - BLACK_BAND_DISPLACEMENT_FROM_CENTER;
			m_VerticalLimits.xStartIndexCentralBand = xCenter - HALVE_AND_ROUND_UP(BANDS_WIDTH);
			m_VerticalLimits.xEndIndexCentralBand = xCenter + HALVE_AND_ROUND_UP(BANDS_WIDTH);
			m_VerticalLimits.xStartIndexRightBand = xCenter + BLACK_BAND_DISPLACEMENT_FROM_CENTER;
			m_VerticalLimits.xEndIndexRightBand = xCenter + BLACK_BAND_DISPLACEMENT_FROM_CENTER + BANDS_WIDTH;
			m_VerticalLimits.yStartIndex = ( yCenter >= BANDS_HEIGHT/2 ) ? yCenter - BANDS_HEIGHT / 2 : 0;
			m_VerticalLimits.yEndIndex = (( yCenter + BANDS_HEIGHT / 2 ) < m_cropRectangle.width )?
										  ( yCenter + BANDS_HEIGHT / 2 ) : (m_cropRectangle.width - 1);
		break;

		default:
			m_pLogger->log(LOG_ERR, "SampleDetectionVertical::getLimits: wrong wellNumber set (%d)", getWellNumber());
			return false;
		break;
	}

	return true;
}

bool SampleDetectionVertical::scanImageAndCreateWaves(uint32_t* pulData, uint32_t* pulWaves, uint32_t uliStartIndex, uint32_t uliStopIndex, uint8_t ucWindowSize)
{
	if ( pulData == nullptr )				return false;
	if ( pulWaves == nullptr )				return false;
	if ( uliStartIndex > uliStopIndex )		return false;

	uint32_t uliImageHeight = m_cropRectangle.height;
	uint32_t uliImageWidth = m_cropRectangle.width;

	// Create Waves from pulData
	for (uint32_t y = 0; y < uliImageHeight; ++y)
	{
		for (uint32_t x = uliStartIndex; x <= uliStopIndex; ++x)
		{
			*(pulWaves + y) += *(pulData + uliImageWidth*y + x);
		}
		*(pulWaves + y) /= (uliStopIndex - uliStartIndex + 1);
	}

	if ( ucWindowSize != 0 )
	{
		movingAverageFilter(pulWaves, uliImageHeight, ucWindowSize);
	}

	return true;
}

bool SampleDetectionVertical::findPeaks(uint32_t* pulData, uint32_t uliSize, peaksStruct* pPeaksStruct)
{
	if ( pulData == nullptr )			return false;
	if ( pPeaksStruct == nullptr )		return false;

	pPeaksStruct->peaksNum = 0;
	pPeaksStruct->centeredPeakId = 0;
	pPeaksStruct->peaksAmplitude.clear();
	pPeaksStruct->peaksPos.clear();

	/*!*************************  DESCRIPTION  *******************************************
	 * A point is considered to be a peak if is greater than its two neighboring.
	 * A point is also considered to be a peak if it's the first of n equal value and the
	 * previous one and the following one are smaller.
	 * If the point is the last but one element of the array and is greater than the
	 * previous one and equal to the next one, it is also considered as peak.
	*************************************************************************************/

	for (uint32_t i = 1; i < (uliSize-1); ++i)
	{
		if ( (*(pulData+i) > *(pulData+i-1)) && (*(pulData+i) > *(pulData+i+1)) )
		{
			pPeaksStruct->peaksNum++;
			pPeaksStruct->peaksAmplitude.push_back(*(pulData+i));
			pPeaksStruct->peaksPos.push_back(i);
		}
		else if ( i < (uliSize-2) )
		{
			if ( (*(pulData+i) > *(pulData+i-1)) && (*(pulData+i) == *(pulData+i+1)) && (*(pulData+i) >= *(pulData+i+2)) )
			{
				pPeaksStruct->peaksNum++;
				pPeaksStruct->peaksAmplitude.push_back(*(pulData+i));
				pPeaksStruct->peaksPos.push_back(i);
			}
		}
		else if ( (*(pulData+i) > *(pulData+i-1)) && (*(pulData+i) == *(pulData+i+1)) )
		{
			pPeaksStruct->peaksNum++;
			pPeaksStruct->peaksAmplitude.push_back(*(pulData+i));
			pPeaksStruct->peaksPos.push_back(i);
		}
	}

	return true;
}

bool SampleDetectionVertical::proximityCheck(peaksStruct* pLeftPeaksStruct, peaksStruct* pCentralPeaksStruct, peaksStruct* pRightPeaksStruct, int8_t cDiff)
{
	if ( pLeftPeaksStruct == nullptr )						return false;
	if ( pCentralPeaksStruct == nullptr )					return false;
	if ( pRightPeaksStruct == nullptr )						return false;
	if ( getWellNumber() != 0 && getWellNumber() != 3 )		return false;

	uint32_t uliCenter = m_cropRectangle.height / 2 + cDiff;

	int32_t liCenterDisplacement;
	bool peakAlreadyFound = false;

	if ( pLeftPeaksStruct->peaksNum != 0 )
	{
		for (uint32_t i = 0; i < pLeftPeaksStruct->peaksNum; ++i)
		{
			liCenterDisplacement = pLeftPeaksStruct->peaksPos[i] - uliCenter;
			if ( (abs(liCenterDisplacement) <= VER_ADMITTED_DISPLACEMENT) && (pLeftPeaksStruct->peaksAmplitude[i] > VER_MIN_PEAK_AMPLITUDE_ADMITTED) )
			{
				// this control is needed in case there are 2 peaks in the central part
				if ( peakAlreadyFound == true )
				{
					if (pLeftPeaksStruct->peaksAmplitude[i] > pLeftPeaksStruct->peaksAmplitude[pLeftPeaksStruct->centeredPeakId])
						pLeftPeaksStruct->centeredPeakId = i;
				}
				else
				{
					pLeftPeaksStruct->centeredPeakId = i;
					m_CenterProx.left = 1;
					peakAlreadyFound = true;
				}
			}
		}
	}

	peakAlreadyFound = false;
	if ( pCentralPeaksStruct->peaksNum != 0 )
	{
		for (uint32_t i = 0; i < pCentralPeaksStruct->peaksNum; ++i)
		{
			liCenterDisplacement = pCentralPeaksStruct->peaksPos[i] - uliCenter;
			if ( (abs(liCenterDisplacement) <= VER_ADMITTED_DISPLACEMENT) && (pCentralPeaksStruct->peaksAmplitude[i] > VER_MIN_PEAK_AMPLITUDE_ADMITTED) )
			{
				// this control is needed in case there are 2 peaks in the central part
				if ( peakAlreadyFound == true )
				{
					if (pCentralPeaksStruct->peaksAmplitude[i] > pCentralPeaksStruct->peaksAmplitude[pCentralPeaksStruct->centeredPeakId])
						pCentralPeaksStruct->centeredPeakId = i;
				}
				else
				{
					pCentralPeaksStruct->centeredPeakId = i;
					m_CenterProx.center = 1;
					peakAlreadyFound = true;
				}
			}
		}
	}

	peakAlreadyFound = false;
	if ( pRightPeaksStruct->peaksNum != 0 )
	{
		for (uint32_t i=0; i<pRightPeaksStruct->peaksNum; ++i)
		{
			liCenterDisplacement = pRightPeaksStruct->peaksPos[i] - uliCenter;
			if ( (abs(liCenterDisplacement) <= VER_ADMITTED_DISPLACEMENT) && (pRightPeaksStruct->peaksAmplitude[i] > VER_MIN_PEAK_AMPLITUDE_ADMITTED) )
			{
				// this control is needed in case there are 2 peaks in the central part
				if ( peakAlreadyFound == true )
				{
					if (pRightPeaksStruct->peaksAmplitude[i] > pRightPeaksStruct->peaksAmplitude[pRightPeaksStruct->centeredPeakId])
						pRightPeaksStruct->centeredPeakId = i;
				}
				else
				{
					pRightPeaksStruct->centeredPeakId = i;
					m_CenterProx.right = 1;
					peakAlreadyFound = true;
				}
			}
		}
	}

	return true;
}

bool SampleDetectionVertical::peaksClosenessCheck(peaksStruct* pLeftPeaksStruct, peaksStruct* pCentralPeaksStruct, peaksStruct* pRightPeaksStruct)
{
	if ( pLeftPeaksStruct == nullptr )					return false;
	if ( pCentralPeaksStruct == nullptr )				return false;
	if ( pRightPeaksStruct == nullptr )					return false;
	if ( pLeftPeaksStruct->centeredPeakId == -1 )		return false;
	if ( pCentralPeaksStruct->centeredPeakId == -1 )	return false;
	if ( pRightPeaksStruct->centeredPeakId == -1 )		return false;

	int32_t liDifference;
	uint32_t uliPeaksPosAVG =  DECIMAL_DIGITS_FACTOR * (pLeftPeaksStruct->peaksPos[pLeftPeaksStruct->centeredPeakId]
							 + pCentralPeaksStruct->peaksPos[pCentralPeaksStruct->centeredPeakId]
							 + pRightPeaksStruct->peaksPos[pRightPeaksStruct->centeredPeakId] ) / 3;

	liDifference = uliPeaksPosAVG -  DECIMAL_DIGITS_FACTOR * pLeftPeaksStruct->peaksPos[pLeftPeaksStruct->centeredPeakId];

	if ( abs(liDifference) <= VER_MAX_INTERPEAK_DISANCE * DECIMAL_DIGITS_FACTOR )
		m_PeaksPos.left = 1;

	liDifference = uliPeaksPosAVG - DECIMAL_DIGITS_FACTOR * pCentralPeaksStruct->peaksPos[pCentralPeaksStruct->centeredPeakId];
	if ( abs(liDifference) <= VER_MAX_INTERPEAK_DISANCE * DECIMAL_DIGITS_FACTOR )
		m_PeaksPos.center = 1;

	liDifference = uliPeaksPosAVG - DECIMAL_DIGITS_FACTOR * pRightPeaksStruct->peaksPos[pRightPeaksStruct->centeredPeakId];
	if ( abs(liDifference) <= VER_MAX_INTERPEAK_DISANCE * DECIMAL_DIGITS_FACTOR )
		m_PeaksPos.right = 1;

	return true;
}

bool SampleDetectionVertical::peaksAmplitudeCheck(peaksStruct* pLeftPeaksStruct, peaksStruct* pCentralPeaksStruct, peaksStruct* pRightPeaksStruct)
{
	if ( pLeftPeaksStruct == nullptr )					return false;
	if ( pCentralPeaksStruct == nullptr )				return false;
	if ( pRightPeaksStruct == nullptr )					return false;
	if ( pLeftPeaksStruct->centeredPeakId == -1 )		return false;
	if ( pCentralPeaksStruct->centeredPeakId == -1 )	return false;
	if ( pRightPeaksStruct->centeredPeakId == -1 )		return false;

	int32_t liDifference;
	uint32_t uliPeaksAmplAVG = (pLeftPeaksStruct->peaksAmplitude[pLeftPeaksStruct->centeredPeakId]
							  + pCentralPeaksStruct->peaksAmplitude[pCentralPeaksStruct->centeredPeakId]
							  + pRightPeaksStruct->peaksAmplitude[pRightPeaksStruct->centeredPeakId] ) / 3;

	liDifference = uliPeaksAmplAVG - pLeftPeaksStruct->peaksAmplitude[pLeftPeaksStruct->centeredPeakId];

	if ( abs(liDifference) <= VER_MAX_INTERPEAK_AMPLITUDE )
		m_PeaksAmpl.left = 1;

	liDifference = uliPeaksAmplAVG - pCentralPeaksStruct->peaksAmplitude[pCentralPeaksStruct->centeredPeakId];
	if ( abs(liDifference) <= VER_MAX_INTERPEAK_AMPLITUDE )
		m_PeaksAmpl.center = 1;

	liDifference = uliPeaksAmplAVG - pRightPeaksStruct->peaksAmplitude[pRightPeaksStruct->centeredPeakId];
	if ( abs(liDifference) <= VER_MAX_INTERPEAK_AMPLITUDE  )
		m_PeaksAmpl.right = 1;

	return true;
}

int32_t SampleDetectionVertical::checkOtherPeaksPresenceFirstPart(peaksStruct* pPeaksStruct, int32_t liDoNotConsiderId)
{
	if ( pPeaksStruct->centeredPeakId == -1 )		return 0;
	if ( pPeaksStruct->peaksNum == 0 )				return 0;

	int32_t liPeaksContribute = 0;
	uint8_t ucPeaksCnt = 0;
	uint32_t uliMinAmplAdmitted = pPeaksStruct->peaksAmplitude[pPeaksStruct->centeredPeakId] * 2 / 3;

	// This means that there is at least one peak in the first part
	if ( pPeaksStruct->centeredPeakId > 0 )
	{
		for (int32_t i = 0; i < pPeaksStruct->centeredPeakId; ++i)
		{
			if ( pPeaksStruct->peaksAmplitude[i] > uliMinAmplAdmitted && i!=liDoNotConsiderId )
				++ucPeaksCnt;
		}
	}

	liPeaksContribute -= ucPeaksCnt;

	return liPeaksContribute;
}

int32_t SampleDetectionVertical::checkOtherPeaksPresenceLastPart(peaksStruct* pPeaksStruct, int32_t liDoNotConsiderId)
{
	if ( pPeaksStruct->centeredPeakId == -1 )		return 0;
	if ( pPeaksStruct->peaksNum == 0 )				return 0;

	int32_t liPeaksContribute = 0;
	uint8_t ucPeaksCnt = 0;
	uint32_t uliMinAmplAdmitted = pPeaksStruct->peaksAmplitude[pPeaksStruct->centeredPeakId] * 2 / 3;

	// This means that there is at least one peak in the first part
	if ( uint32_t(pPeaksStruct->centeredPeakId) < (pPeaksStruct->peaksNum-1) )
	{
		for (uint32_t i = (pPeaksStruct->centeredPeakId+1); i < pPeaksStruct->peaksNum; ++i)
		{
			if ( pPeaksStruct->peaksAmplitude[i] > uliMinAmplAdmitted  && (int)i != liDoNotConsiderId )
				++ucPeaksCnt;
		}
	}
	liPeaksContribute -= ucPeaksCnt;
	return liPeaksContribute;
}

