/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionBase.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SampleDetectionBase class.
 @details

 ****************************************************************************
*/

#include "SampleDetectionBase.h"


SampleDetectionBase::SampleDetectionBase()
{
	m_pLogger = 0;
	m_cWellNumber = -1;
	memset(&m_cropRectangle, 0x00, sizeof(cropRectangle));
}

SampleDetectionBase::~SampleDetectionBase()
{

}

bool SampleDetectionBase::setLogger(Log* pLogger)
{
	if ( pLogger == 0 )	return false;

	m_pLogger = pLogger;
	return true;
}

int8_t SampleDetectionBase::getWellNumber()
{
	if ( ( m_cWellNumber != WELL_ZERO ) && ( m_cWellNumber != WELL_THREE ) )
	{
		return -1;
	}

	return m_cWellNumber;
}

bool SampleDetectionBase::setWellNumber(int8_t cWellNum)
{
	if ( ( cWellNum != WELL_ZERO ) && ( cWellNum != WELL_THREE ) )
	{
		return false;
	}

	m_cWellNumber = cWellNum;
	return true;
}

bool SampleDetectionBase::setCropRectangle(const cropRectangle* pCropRectangle)
{
	if ( pCropRectangle == 0 )		return false;

	m_cropRectangle.xStart = pCropRectangle->xStart;
	m_cropRectangle.yStart = pCropRectangle->yStart;
	m_cropRectangle.width  = pCropRectangle->width;
	m_cropRectangle.height = pCropRectangle->height;
	return true;
}

cropRectangle* SampleDetectionBase::getCropRectangle()
{
	return (&m_cropRectangle);
}

bool SampleDetectionBase::pretreatImage(uint32_t* pulData, uint8_t rgcMedianFilterMatrix[], uint32_t uliWidth, uint32_t uliHeight)
{
	if ( pulData == NULL )	return false;

	uint32_t uliImageSize = uliWidth * uliHeight;

	if ( !sobel(pulData, uliWidth, uliHeight) )								return false;

	if ( !imAdjust(pulData, uliImageSize) )									return false;

	if ( !medFilt2(pulData, uliWidth, uliHeight, rgcMedianFilterMatrix) )	return false;

	return true;
}


