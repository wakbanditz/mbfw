/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionLink.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SampleDetectionLink class.
 @details

 ****************************************************************************
*/

#include <algorithm>

#include "SampleDetectionLink.h"

#define CROP_RECT_MAX_SHIFT_ACCEPT_X0	5
#define CROP_RECT_MAX_SHIFT_ACCEPT_Y0	5 // TODO define these 2 values
#define THRESHOLD_SCORE	70


SampleDetectionLink::SampleDetectionLink()
{
    m_pLogger = 0;
}

SampleDetectionLink::~SampleDetectionLink()
{

}

bool SampleDetectionLink::initZeroZero(Log *pLogger, uint32_t uliWidth, uint32_t uliHeight)
{
    m_pLogger = pLogger;
    bool bRes = m_ZeroZero.init(pLogger, uliWidth, uliHeight);
    return bRes;
}

bool SampleDetectionLink::analyzeZeroZero(uint32_t* pulData, uint8_t ucSlotNum, uint8_t ucWellNum, enumParamsType eType)
{
    if ( ucSlotNum > SCT_NUM_TOT_SLOTS*SCT_NUM_TOT_SECTIONS )
    {
        m_pLogger->log(LOG_ERR, "SampleDetectionLink::analyzeZeroZero: ucSlotNum [%d] exceed max value [%d]",
                                                           ucSlotNum, SCT_NUM_TOT_SLOTS*SCT_NUM_TOT_SECTIONS);
        return false;
    }

    bool bRes = m_ZeroZero.getCropRectangleFromZeroZero_Otsu(pulData);
//	bool bRes = m_ZeroZero.getCropRectangleFromZeroZero(pulData);
    if ( ! bRes )
    {
        m_pLogger->log(LOG_ERR, "SampleDetectionLink::analyzeZeroZero: unable to find cropRectangle of slot num [%d]");
        return false;
    }

    m_ZeroZero.convertCropRectangleZeroZero();

    /*! ******* Once the cropRectangle is calculated, it has to be stored in the apposite file ****** **/

    // update cropRectangle file
    bRes = setCropRectangleToFile(m_ZeroZero.getCropRectangle(), ucSlotNum, ucWellNum, eType);
    if ( ! bRes )	return false;

    // Check if a recalibration is needed
    if ( eType == eUpdateParams )
    {
        /*! I need to check if the new cropRectangle is too shifter compared to the calibration value **/

        cropRectangle Crop;
        bRes = getCropRectangleFromFile(&Crop, ucSlotNum, ucWellNum, eFactoryParams);
        if ( ! bRes )	return false;

        int liDiffX0 = int(Crop.xStart) - int(m_ZeroZero.getCropRectangle()->xStart);
        int liDiffY0 = int(Crop.yStart) - int(m_ZeroZero.getCropRectangle()->yStart);

        if ( abs(liDiffX0) > CROP_RECT_MAX_SHIFT_ACCEPT_X0 || abs(liDiffY0) > CROP_RECT_MAX_SHIFT_ACCEPT_Y0 )
        {
            m_pLogger->log(LOG_ERR, "SampleDetectionLink::analyzeZeroZero: recalibration needed, shift X [%d], shift Y [%d]",
                                                               liDiffX0, liDiffY0);
            return false;
        }
    }

    return true;
}

bool SampleDetectionLink::initAlgo(Log *pLogger, uint8_t ucSlotNum, int8_t cWellNum)
{
    cropRectangle CropRectangle;
    bool bRes = getCropRectangleFromFile(&CropRectangle, ucSlotNum, cWellNum);
    if ( ! bRes )
    {
        pLogger->log(LOG_ERR, "SampleDetectionLink::initAlgo: unable to get crop rectangle from file");
        return false;
    }

    // add 8 depending on the well, and pass to m_Preliminary.init
    CropRectangle.yStart += ( ( cWellNum == WELL_ZERO ) ? 8 : 8 );
    bRes = m_Preliminary.init(pLogger, &CropRectangle, cWellNum);
    if ( ! bRes )
    {
        return false;
    }

    m_pLogger = pLogger;
    return true;
}

bool SampleDetectionLink::sampleDetectionAlgorithm(const uint32_t* pulData, uint32_t uliImageWidth, float& fResult)
{
    fResult = 0;
    bool bRes;
    vector<uint32_t> vCroppedImage(m_Preliminary.m_nImageSize);

    /*! ********************************************************************************
     ******************        Start preliminary processing        *********************
     ******************************************************************************** **/

    bool bLiquid;
    bRes = m_Preliminary.performPreliminaryChecks((uint32_t*)pulData, vCroppedImage, uliImageWidth, bLiquid);
    if ( ! bRes )
    {
        m_pLogger->log(LOG_ERR, "SampleDetectionLink::sampleDetectionAlgorithm: error performing preliminary processing");
        return false;
    }
    if ( bLiquid )
    {
        return true;
    }

    /*! ********************************************************************************
     ********************        Start vertical processing        **********************
     ******************************************************************************** **/

    cropRectangle* preliminaryCropRectangle;
    preliminaryCropRectangle = m_Preliminary.getCropRectangle();
    m_Vertical.init(m_pLogger, preliminaryCropRectangle, m_Preliminary.getWellNumber(), m_Preliminary.m_nInitialWidth, m_Preliminary.getXShift());
    float fVerRes = 0;

    if ( m_Preliminary.getWellNumber() == WELL_ZERO )
    {
        bRes = m_Vertical.verticalProcessingX0(vCroppedImage.data(), m_Preliminary.getDiffVerX0(), fVerRes);
    }
    else if ( m_Preliminary.getWellNumber() == WELL_THREE )
    {
        bRes = m_Vertical.verticalProcessingX3(vCroppedImage.data(), m_Preliminary.getDiffVerX0(), fVerRes);
    }

    if ( ! bRes )
    {
        m_pLogger->log(LOG_ERR, "SampleDetectionLink::sampleDetectionAlgorithm: error performing vertical processing");
        return false;
    }
    m_pLogger->log(LOG_INFO, "SampleDetectionLink::sampleDetectionAlgorithm: vertical processing result: %.1f", fVerRes);


    /*! ********************************************************************************
     ********************        Start horizontal processing        ********************
     ******************************************************************************** **/

    m_Horizontal.init(m_pLogger, preliminaryCropRectangle, m_Preliminary.getWellNumber(), m_Preliminary.m_nInitialWidth);
    float fHorRes = 0;
    bRes = m_Horizontal.horizontalProcessing(vCroppedImage.data(), m_Preliminary.getDiff(), fHorRes);
    if ( ! bRes )
    {
        m_pLogger->log(LOG_ERR, "SampleDetectionLink::sampleDetectionAlgorithm: error performing horizontal processing");
        return false;
    }
    m_pLogger->log(LOG_INFO, "SampleDetectionLink::sampleDetectionAlgorithm: horizontal processing result: %.1f", fHorRes);

    fResult = ( fVerRes + fHorRes ) / 2;
    return true;
}

bool SampleDetectionLink::readBMP(uint32_t* prgData, string strFileName)
{
    unsigned char nImageHeader[BMP_HEADER_SIZE];

    FILE* picture = fopen(strFileName.c_str(), "rb"); // rb means: read as a binary file
    if ( picture == nullptr )	return false;

    if ( fread(nImageHeader, sizeof(unsigned char), BMP_HEADER_SIZE, picture) != BMP_HEADER_SIZE )
    {
        printf("Image not succesfully read. \nBe careful, the name can be wrong.\n");
        return false;
    }

    int size = getVal4((char*)&nImageHeader[2]);
    int bfOffBits = getVal4((char*)&nImageHeader[10]);
    int width = getVal4((char*)&nImageHeader[18]);
    int height = getVal4((char*)&nImageHeader[22]);

    unsigned char* raw_data  = new unsigned char[size];
    fread(raw_data, sizeof(unsigned char), size, picture);

    fclose(picture);

    raw_data += ( bfOffBits - BMP_HEADER_SIZE );

    // Need 2 take care about the padding
    int padBits;

    if ( width%4 == 0 )
    {
        padBits = 0;
    }
    else
    {
        padBits = 4 - width%4;
    }

    width += padBits;

    // fRead goes from left to right and from bottom to top, while Matlab and the camera
    // go from left to right and from top to bottom.

    int idx, counter = 0;
    for (int idxH = (height-1); idxH >= 0; --idxH)
    {
        for (int idxW = 0; idxW < width-padBits; ++idxW)
        {
            idx = (idxH * width) + idxW;

            *(prgData+counter) = *(raw_data+idx) *  DECIMAL_DIGITS_FACTOR;
            ++counter;
        }
    }
    // this is actually needed because the delete has to have the correct start
    raw_data -= ( bfOffBits - BMP_HEADER_SIZE );
    delete[] raw_data;

    return true;
}

void SampleDetectionLink::getCropRectangleZeroZero(cropRectangle* pCropRectangle)
{
	*pCropRectangle = *m_ZeroZero.getCropRectangle();

//	pCropRectangle->xStart = m_ZeroZero.getCropRectangle()->xStart;
//	pCropRectangle->yStart = m_ZeroZero.getCropRectangle()->yStart;
//	pCropRectangle->width = m_ZeroZero.getCropRectangle()->width;
//	pCropRectangle->height = m_ZeroZero.getCropRectangle()->height;
    return;
}

bool SampleDetectionLink::getCropRectangleFromFile(cropRectangle* pCropRectangle, uint8_t ucSlotNum, uint8_t ucWellNum, enumParamsType eType)
{
    string strFileName = ( eType == eUpdateParams ) ? CAMERA_UPD_CONFIG_FILENAME : CAMERA_DEF_CONFIG_FILENAME;
    ifstream file(strFileName);
    if ( ! file.good() )
    {
        return false;
    }

    bool bFound = false;
    string strTmp("");
    string strName("CropRectangle");
    strName += ( to_string(ucWellNum) + "_" + to_string(ucSlotNum) );

    while ( getline(file, strTmp) )
    {
        // Remove white spaces
        strTmp.erase(remove(strTmp.begin(), strTmp.end(), ' '), strTmp.end());

        // Do not consider if empty or commented
        if ( strTmp.empty() )		continue;
        if ( strTmp[0] == '#' )		continue;

        // issue: name0_1 and name0_11 are equal if we take only the first 7 chars
        if ( ! strName.compare(strTmp.substr(0, strTmp.find('='))) )
        {
            bFound = true;
            break;
        }
    }
    file.close();

    if ( ! bFound )
    {
        return false;
    }

    // strTmp is the wanted one has the format Name = X0 # Y0 # Width # Height
    strTmp.erase(0, strName.size()+1);
    vector<string> vValues(4);
    int i = 0;
    for ( char c : strTmp )
    {
        if ( c == '#' )
        {
            i++;
            if ( i == 4 ) // the fourth can be a # for a comment
            {
                break;
            }
            continue;
        }
        if ( isdigit(c) )	vValues[i].push_back(c);
    }

    // There have to be all the fields
    if ( i != 3 )
    {
        return false;
    }

    pCropRectangle->xStart = stoi(vValues[0]);
    pCropRectangle->yStart = stoi(vValues[1]);
    pCropRectangle->width = stoi(vValues[2]);
    pCropRectangle->height = stoi(vValues[3]);

    return true;
}

bool SampleDetectionLink::setCropRectangleToFile(cropRectangle* pCropRectangle, uint8_t ucSlotNum, uint8_t ucWellNum, enumParamsType eType)
{
    // if eUpdateParams then it has to be copied also in the other!! update it!!!

    string strFileName = ( eType == eUpdateParams ) ? CAMERA_UPD_CONFIG_FILENAME : CAMERA_DEF_CONFIG_FILENAME;
    ifstream inFile(strFileName);
    if ( ! inFile.good() )
    {
        return false;
    }

    string strName("CropRectangle");
    strName += ( to_string(ucWellNum) + "_" + to_string(ucSlotNum) );
    string strAllFile("");
    string strTmp("");
    bool bFound = false;

    while ( getline(inFile, strTmp) )
    {
        // perform the check only if the string is not empty and not a comment
        if ( ! strTmp.empty() )
        {
            if ( strTmp[0] != '#' )
            {
                string::size_type pos = strTmp.find('=');
                if (pos != std::string::npos)
                {
                    string strCompare = strTmp.substr(0, pos);
                    strCompare.erase(remove(strCompare.begin(), strCompare.end(), ' '), strCompare.end());

                    if ( ! strName.compare(strCompare) )
                    {
                        strTmp.assign(strName);
                        strTmp += ( " = " + to_string(pCropRectangle->xStart) );
                        strTmp += ( " # " + to_string(pCropRectangle->yStart) );
                        strTmp += ( " # " + to_string(pCropRectangle->width) );
                        strTmp += ( " # " + to_string(pCropRectangle->height) );
                        bFound = true;
                    }
                }
            }
        }
        strAllFile += (strTmp + "\n");
    }

    inFile.close();

    if ( ! bFound )
    {
        m_pLogger->log(LOG_INFO, "SampleDetectionLink::setCropRectangleToFile: unable to find %s in file %s",
                                   strName.c_str(), strFileName.c_str());
        return false;
    }

    // We have all the file updated in strAllFile. Now we have to copy it.

    switch ( eType )
    {
        case eFactoryParams:
        {
            strFileName = CAMERA_DEF_CONFIG_FILENAME;
            ofstream outFile;
            outFile.open(strFileName, std::ofstream::out | std::ofstream::trunc);
            if ( ! outFile.good() )
            {
                    m_pLogger->log(LOG_INFO, "SampleDetectionLink::setCropRectangleToFile: unable to open %s", strFileName.c_str());
                    return false;
            }
            outFile << strAllFile;
            outFile.close();
        }
        // there is no need of break. When the default are applied (production and calibration time)
        // we have to save the new values also in the other file.

        default:
        {
            strFileName = CAMERA_UPD_CONFIG_FILENAME;
            ofstream outFile;
            outFile.open(strFileName, std::ofstream::out | std::ofstream::trunc);
            if ( ! outFile.good() )
            {
                    m_pLogger->log(LOG_INFO, "SampleDetectionLink::setCropRectangleToFile: unable to open %s", strFileName.c_str());
                    return false;
            }
            outFile << strAllFile;
            outFile.close();
        }
        break;
    }

    return true;
}
