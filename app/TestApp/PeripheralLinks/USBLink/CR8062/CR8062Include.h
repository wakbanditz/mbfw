/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Include.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the CR8062 classes.
 @details

 ****************************************************************************
*/

#ifndef CODECORPSTRUCTDEFINITION_H
#define CODECORPSTRUCTDEFINITION_H

#include <vector>
#include <string>
#include <inttypes.h>

#include "CommonInclude.h"
#include "ErrorCodes.h"

/*! ***********************************************************************************
 * CodeCorp8062 defines.
 **************************************************************************************/

// Standard protocol messages
#define PROTO_HID_REPORT_NUMBER		0x00
#define PROTO_SOH					0x01
#define PROTO_START_OF_FRAME_C		0x43
#define PROTO_START_OF_FRAME_T		0x54
#define PROTO_PACKET_VERSION		0x31
#define PROTO_PACKET_PROTOCOL		0x00
#define PROTO_CONNECTION_ID			0x01
#define PROTO_PAYLOAD_CMD			0x02
#define PROTO_FLAGS					0x00
#define PROTO_TRANSACTION_NUMBER_0	0x00
#define PROTO_TRANSACTION_NUMBER_1	0x0E
#define PROTO_REQUEST_ID_0			0x80
#define	PROTO_REQUEST_ID_1			0x0E
#define PROTO_CRC_16_0				0xDE
#define PROTO_CRC_16_1				0x2E


// Connection protocol - Payload protocol
#define PROTO_CONNECTION_ACK		0x00
#define PROTO_DECODE_DATA			0x01
#define PROTO_CMD					0x02
#define PROTO_IMAGE_TRANSFER		0x03
#define PROTO_LOGGER				0x04


// Protocol Parser
#define PROTO_START					0
#define PROTO_TYPE_SELECT			1
#define PROTO_CONNECTION			2
#define PROTO_ACK					3
#define PROTO_DECODE				4
#define PROTO_COMMAND				5
#define PROTO_IMAGE					6
#define PROTO_LOG					7
#define PROTO_PAYLOAD				8
#define PROTO_LOG_PAYLOAD			9
#define PROTO_CRC					10
#define PROTO_STORE_IMAGE			11
#define PROTO_STORE_CODE			12


// Other messages related defines useful
#define MSG_RX_EMPTY_UNTIL_PAYLOAD	24
#define MSG_LENGTH_UNTIL_PAYLOAD	23
#define MSG_ACK_LENGTH				21
#define MSG_PAYLOAD_MAX_LENGTH		( 65535 - MSG_ACK_LENGTH )	// Max PacketLength - dest.Addr - sour.Addr - PT - Flags - Cmd -TN - reqID - CRC
#define MSG_PACKET_MAX_LENGTH		4096			// actually message are 4096B, change in future.
#define MSG_FIRST_PART_HEADER		6
#define MSG_FIXED_PART_LENGTH		( 25 - MSG_FIRST_PART_HEADER )
#define MSG_STANDARD_RX_LENGTH		85
#define MSG_CRC_BITS				16
#define MSG_CRC_CHAR_BITS			8
#define MSG_CRC_DIFF_BITS			( MSG_CRC_BITS - MSG_CRC_CHAR_BITS )

#define FOI_HIGH_DENSITY_FIELD		0x00
#define FOI_WIDE_FIELD				0x01
#define FOI_ENTIRE_IMAGE			0x02

// Image and parameters limits
#define MAX_WIDTH					1280
#define MAX_HEIGHT					960
#define MAX_IMAGE_SIZE				1229800	// Max image size equal to 1280*960 + 1000B of payload info+padding
#define MAX_ILLUMINATION			100
#define MAX_EXPOSURE				100000
#define MAX_GAIN					100

#define MAX_PIXEL_VALUE				255
#define DECIMAL_DIGITS_FACTOR		1000 // to calculate stuff with int, but still having 3 decimal digits
#define MAX_VALUE					( 255 * DECIMAL_DIGITS_FACTOR )

// Payload limits
#define DEF_FWUPDATE_PAYLOAD_SIZE	UINT8_MAX // btw 128 should be enough
#define DEF_READER_PAYLOAD_SIZE		INT16_MAX
#define DEF_CAMERA_PAYLOAD_SIZE		( MAX_IMAGE_SIZE + INT16_MAX ) // added to be sure everything goes well in case of bad msgs sent

// Max reading time for barcode and dataMatrix
#define MAX_LINEAR_BARCODE_READING_TIME_MSEC	eGetLinearBarcodeTimeOutMsec
#define MAX_DATA_MATRIX_READING_TIME_MSEC		eGetDataMatrixTimeOutMsec
#define MAX_RETRY_NUMBER						2


// Needed for the BMP header
#define BMP_HEADER_SIZE				54
#define MAGIC_MSB					'B'
#define MAGIC_LSB					'M'
#define CREATOR1					0x0
#define CREATOR2					0x0
#define HRES						0x0
#define VRES						0x0
#define PALETTE_NUM					256
#define PALETTE_LEN					(PALETTE_NUM*4)

// Common include for batch files
#define TARGET_NAME_SAMPLE0		"Sample0"
#define TARGET_NAME_SAMPLE3		"Sample3"
#define TARGET_NAME_BC			"BC"
#define TARGET_NAME_DATAMATRIX	"DataMatrix"
#define TARGET_NAME_BC_ABS		"ConeAbsence"

/*! ***********************************************************************************
 * CodeCorp8062 structures.
 **************************************************************************************/

#pragma pack(1)

// Operation Retvalues
//typedef enum
//{
//	eOk 			= ERR_NONE,
//	eCommError		= ERR_USB_PERIPH_COMMUNICATION,
//	eWriteError		= ERR_USB_WRITE_DUE_TO_SIGNAL,
//	eTimeout		= ERR_USB_PERIPH_TIMEOUT,
//	eErrReading		= ERR_CR8062_READING_CODE,
//	eErrPicture		= ERR_CR8062_TAKING_PICTURE,
//	eUncorrectParam = ERR_CR8062_IN_PARAMETERS,

//	eUndefined		= ERR_UNDEFINED
//} eCameraOperationResult;

enum class eBatchTarget { eNone, eSampleX0, eSampleX3, eDataMatrix, eLinearBarcode, eConeAbsence };

typedef enum
{
	eRAW = 1,
	eJPEG = 3,
	eInvalidFormat = -1
} eImageFormat;

typedef struct
{
	int liWidth;
	int liHeight;
	int liSize;
	eImageFormat eFormat;
} structInfoImage;


// Command Reading from Serial Timeouts
typedef enum
{
	eGetFwVersionTimeOutMsec	 = 1000,
	eGetLinearBarcodeTimeOutMsec = 1000,
	eGetDataMatrixTimeOutMsec	 = 1000,
	eGetAGCTimeOutMsec			 = 1000,
	eGetFoiTimeOutMsec			 = 1000,
	eGetContrWindTimeOutMsec	 = 1000,
	eGetRoiTimeOutMsec			 = 1000,
	eResetTimeOutMsec			 = 1000,
	eGetCropCoordTimeOutMsec	 = 1000,
	eGetImageTimeOutMsec		 = 2000,
	eGetReadyTimeOutMsec		 = 1000,
	eGetReady4FwUpdateTimeOutMsec= 100,
	eFwUpdateTimeOutMsec		 = 45000,
	eGetErrorsTimeOutMsec		 = 100,
	eSwitch2HIDTimeOutMsec		 = 2000,
	eSwitch2CDCTimeOutMsec		 = 2000,
	eFullSpeedTimeOutMsec		 = 400,
	eSavePlatformSettingsMsec	 = 400,
	eGetReady4VideosMsec		 = 400,
	eRestoreDefaultMsec			 = 400,
	eGetDefaultTimeOutMsec		 = 400,
	eFast						 = 200
} eCameraCmdTimeout;


// Header
typedef struct packetHeader_tag
{
	uint32_t startFrame:24;
	uint8_t  packetVersion;
	uint16_t packetLength;
	uint32_t destAddress;
	uint32_t sourceAddress;
	uint8_t  protocolType;
} packetHeader;


// Connection Protocol
typedef struct flag_tag
{
	uint8_t ACK:1;
	uint8_t reserved:5;
	uint8_t firstFragment:1;
	uint8_t fragmentIncluded:1;
} flag;


typedef union uFlag_tag
{
	flag	pkt;
	uint8_t data;
} uFlag;


typedef struct connectionProtocol_tag
{
	flag	 uFlag;
	uint8_t  payloadProtocol;
	uint16_t ACKnumber;
	uint16_t transactionNumber;
	uint32_t fragmentOffsetSize;
} connectionProtocol;


// Packet Protocol
typedef struct packetProtocol_tag
{
	uint8_t messageType;
	uint8_t messageSubType;
} packetProtocol;


// Payload Protocol Layer
typedef struct requestID_tag
{
	uint16_t ID:15;
	uint16_t originatorBit:1;
} requestID;


typedef struct payload_tag
{
	requestID uRequestID;
	std::vector<uint8_t> pktPayload;
} payload;


typedef uint16_t crc_t;


// BMP header
typedef struct BmpHeader_tag
{
	unsigned char magic[2];
	uint32_t filesz;
	uint16_t creator1;
	uint16_t creator2;
	uint32_t bmp_offset;
} BmpHeader;


typedef struct BmpInfoHeader_tag
{
	uint32_t header_sz;
	int32_t  width;
	int32_t  height;
	uint16_t nplanes;
	uint16_t bitspp;
	uint32_t compress_type;
	uint32_t bmp_bytesz;
	int32_t  hres;
	int32_t  vres;
	uint32_t ncolors;
	uint32_t nimpcolors;
} BmpInfoHeader;


typedef struct logAGCparameters_tag
{
	int liAttempNum;
	int liMultiplier;
	int liIndex;
	int liExposure;
	int liIllumination;
	int liGain;
} logAGCparameters;


#pragma pack()


/*! ***********************************************************************************
 * CodeCorp8062 AGC tables, used to modify the AGC behaviour when taking a picture.
 **************************************************************************************
 */

#define AGC_TABLE_SIZE			18 // 6 values per 3 parameters

static const uint32_t AGCTabX0[] =
{
	9200, 9200, 9200, 9200, 9200, 9200, 	// exposure time values
	20, 20, 20, 20, 20, 20,						// gain
	70, 70, 70, 70, 70, 70,						// illumination
};

static const uint32_t AGCTabX3[] =
{
	30000, 33200, 36400, 39600, 42800, 46000, 	// exposure time values
	70, 70, 70, 70, 70, 70,						// gain
	20, 20, 20, 20, 20, 20,						// illumination
};


/*! ***********************************************************************************
 * CodeCorp8062 tables for correctness check.
 **************************************************************************************
 */

typedef uint16_t crc_t;

static const crc_t crcTab[] =
{
	0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
	0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
	0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
	0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
	0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
	0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
	0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
	0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
	0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
	0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
	0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
	0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
	0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
	0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
	0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
	0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
	0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
	0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
	0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
	0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
	0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
	0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
	0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
	0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
	0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
	0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
	0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
	0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
	0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
	0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
	0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0,
};


#define CHECKSUM_BARCODE_MODULE		37

static const char checkSumTable[CHECKSUM_BARCODE_MODULE+1] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ+";




/*! ***********************************************************************************
 * CodeCorp8062 printf ROCKET and THUGH LIFE.
 **************************************************************************************
 */

const char rocket[] =
"           \n\
		   /^\\\n\
		   |-|\n\
		   | |\n\
		   |N|\n\
		   |A|\n\
		   |S|\n\
		   |A|\n\
		  /| |\\\n\
		 / | | \\\n\
		|  | |  |\n\
		 `-\"\"\"-`\n\
\n\
";


static std::vector <std::string> vstrThughLife =
{
".",
".",
".                                            @@@@@@@+ @@   @@ @@+    @@    ++++ ",
".                ++++++++++++                @@@@@@@+ @@   @@ @@+    @@   @@@@@@+",
".            +++++++++++++++++++             +++@@+++ @@   @@ @@+    @@  @@@++@@+",
".          +++++++++++++++++++++++              @@    @@   @@ @@+    @@ @@@    +  ",
".        +++++++++++++++++++++++++++            @@    @@   @@ @@+    @@ @@         ",
".       +++++++++++++++++++++++++++++           @@    @@@@@@@ @@+    @@ @@   @@@@@+",
".      +++++++++++++++++++++++++++++++          @@    @@@@@@@ @@+    @@ @@   @@@@@+",
".     +++++++++++++++++++++++++++++++++         @@    @@   @@ +@+   +@@ @@+     @@  ",
".     +++++++++++++++++++++++++++++++++         @@    @@   @@  @@+ +@@+ +@@+  +@@+  ",
".                   +++++                       @@    @@   @@  +@@@@@@   +@@@@@@@   ",
".                                               @@    @@   @@   +@@@+       @@@@+    ",
".                      ++                                                           ",
".                     +++                                                           ",
".  +                +++++                +",
".  +                +++++                +",
".  ++              +++++++              ++   ",
".  +++            ++++++++++           +++          @@     @@+ @@@@@@ @@@@@@           ",
".  +++++       +++++++++++++++       +++++          @@     @@+ @@@@@@ @@@@@@           ",
".  +++++++++++++++++++++++++++++++++++++++          @@     @@+ @@     @@               ",
".  +++++++++++++++++++++++++++ @++++++++++          @@     @@+ @@     @@               ",
".  ++++++++++++++++++++++++++ ++++++++++++          @@     @@+ @@@@@  @@@@@            ",
".   +++++++++++++++  +++  +@++++++++++++++          @@     @@+ @@@@@  @@@@@",
".     ++++++++++++++++++++@@@@@++++++++++           @@     @@+ @@+++  @@+++",
".        +++++++++++++++++@@@@@@+++++++             @@     @@+ @@     @@",
".          +++++++++++++++@@@@@@++                  @@++++ @@+ @@     @@++++",
".                   +++++  @@@@@@                   @@@@@@ @@+ @@     @@@@@@",
".                          +@@@@@@+                 @@@@@@ @@+ @@     @@@@@@",
".                            +@@@@@@+",
".                             +@@@@@@+",
".                              +@@@@@@+",
".                                @@@@@@+",
".                                 +@@@@+",
".                                   @@@+",
".                                     ++",
".",
"."
};



#endif // CODECORPSTRUCTDEFINITION_H
