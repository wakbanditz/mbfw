/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062FwUpdateManager.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CR8062FwUpdateManager class.
 @details

 ****************************************************************************
*/

#include "CR8062FwUpdateManager.h"
#include "CR8062Link.h"

#define PROTO_FW_UPGRADE_START					1
#define PROTO_FW_UPGRADE_SEND_FIRST_64B_PKT		2
#define PROTO_FW_UPGRADE_SEND_64B_PKT			3
#define PROTO_FW_UPGRADE_SEND_LAST_PKT			4
#define PROTO_FW_UPGRADE_SEND_NEW_PKT			5
#define PROTO_FW_UPGRADED						99
#define FW_UPGRADE_FIRST_PKT_SIZE_B				36
#define FW_UPGRADE_PKT_SIZE_B					63//37

#define FW_UPDATE_FIXED_PART_FIRST_PKT_LENGTH	29-MSG_FIRST_PART_HEADER
#define FW_UPDATE_FIXED_PART_PKT_LENGTH			27-MSG_FIRST_PART_HEADER

#define FW_FIRST_FRAGMENT_PKT					0xc0
#define FW_OTHER_FRAGMENT_PKT					0x80

#define FW_UPGRADE_MAX_TOTAL_PKT_SIZE			0xFFFF

#define SEND_PKT	0
#define PKT_SENT	1

#define PAYLOAD_LENGTH_FIRST_PACKET				65508
#define PAYLOAD_LENGTH_INTERMEDIATE_PACKETS		65514

#define FW_FORMAT_EXTENSION						".bin"

CR8062FwUpdateManager::CR8062FwUpdateManager(int liPayloadSize)
{
	setPayloadSize(liPayloadSize);
	m_siRequestId = -1;
	m_liFwSize = -1;
	m_liOffset = -1;
	m_nBytesSent = -1;
	m_usTransactionNum = 0;
}

CR8062FwUpdateManager::~CR8062FwUpdateManager()
{

}

bool CR8062FwUpdateManager::init(HIDInterface* pHIDIfr, Log* pLogger)
{
	m_bConfigOk = false;

	if ( pHIDIfr == NULL )	return false;
	if ( pLogger == NULL )	return false;
//	if ( pErr == NULL )		return false;

	m_pHIDIfr = pHIDIfr;
	m_pLogger = pLogger;
//	m_pCamErr = pErr;

	m_bConfigOk = true;

	return true;
}

bool CR8062FwUpdateManager::upgradeFirmware(string strFileName, long llength, uint32_t uliTimeoutMsec)
{
	/*!	* **********************************************************************************************
	 * Set FW update info:
	 * RDFSXBA0 = start write data from address 0.
	 * SZxxxxxxx = new fw size.
	 * CRxxxxx = crc of the data. ( CR-1 means no crc check )
	 * FM0 = format.
	 * RB1 = reboot after firmware is rewritten.
	 * ************************************************************************************************/

	m_liFwSize = llength;
	if ( m_liFwSize < 0 )		return ERR_ACCESSING_FILE;

	unsigned char* pFile;
	pFile = new unsigned char[m_liFwSize];
	readBinFile(strFileName, pFile, m_liFwSize);
	crc_t usCRC = createCRC(pFile, m_liFwSize+2);

	m_liOffset = 0;
	vector<vector<unsigned char>> vPackets;
	vPackets = file2Pkts(pFile, m_liFwSize);

	string strMsg;
	{
		// compose msg depending on filename (file format)
		int idx = 0;
		for ( idx = strFileName.size(); idx >= 0 ; idx-- )
		{
			if ( strFileName[idx] == '.' )	break;
		}

		string strExt(strFileName.substr(idx));

		stringstream ssMsg;

		if ( ! strExt.compare(FW_FORMAT_EXTENSION) )
		{
			ssMsg << "RDFSXBA0,SZ" << m_liFwSize << ",CR" << usCRC << ",FM0,RB1";
			ssMsg >> strMsg;
		}
		else
		{
			ssMsg << "RDFSXBA0,FN\"" << strFileName	<< "\",SZ" << m_liFwSize << ",CR" << usCRC << ",FM2,RB1";
			ssMsg >> strMsg;
		}
	}

	sendCmd(strMsg);
	msleep(10);
	if ( ! checkIfAccepted(uliTimeoutMsec) )
	{
		m_pLogger->log(LOG_INFO, "CR8062FwUpdateManager::upgradeFirmware: camera not ready to be upgraded.");
		delete[] pFile;
		return false;
	}

	uint8_t ucSwitchProt;
	int liTmpVal = -1;

	for ( unsigned int i = 0; i != vPackets.size(); i++ )
	{
		ucSwitchProt = SEND_PKT;
		int liBytesToSend = vPackets[i].size();
		int liBytesSent = 0;

		while ( ucSwitchProt != PKT_SENT )
		{
			if ( liBytesSent+FW_UPGRADE_PKT_SIZE_B < liBytesToSend )
			{
				sendCustomPkt(vPackets[i], FW_UPGRADE_PKT_SIZE_B, liBytesSent);
				liBytesSent += FW_UPGRADE_PKT_SIZE_B;
			}
			else
			{
				sendCustomPkt(vPackets[i], liBytesToSend-liBytesSent, liBytesSent);
				liBytesSent += (liBytesToSend-liBytesSent);
				ucSwitchProt = PKT_SENT;
			}
			if ( int(100 * (i+1)/vPackets.size() ) != liTmpVal )
			{
				liTmpVal = 100 * (i+1)/vPackets.size();
				m_pLogger->log(LOG_INFO, "CR8062FwUpdateManager::upgradeFirmware: Downloading: %d%%", 100 * (i+1)/vPackets.size());
			}
		}

		// It is only an ack, no need to parse it
		int liRes = readMsgReceived(uliTimeoutMsec);
		if ( liRes == -1 )
		{
			m_pLogger->log(LOG_ERR, "CR8062FwUpdateManager::upgradeFirmware: missing ack of pkt %d", i);
			return false;
		}
	}

	delete[] pFile;

	return true;
}

bool CR8062FwUpdateManager::waitForInstallationCompleted(uint32_t uliTimeoutMsec)
{
	// The msg we are receiving is > than 65B, therefore we need to perform 2 readings.
	// btw the second msg contains not relevant info, so we parse only the first msg.

	int32_t liMsgSize =	readMsgReceived(uliTimeoutMsec);

	if ( liMsgSize <= 0 )	return false;

	parseMsgForFWUpgrade();

	for (auto val : m_PayloadStruct.pktPayload)
	{
		if ( isdigit(val) )
		{
			switch( val )
			{
				case '0':
				break;

				default:
					return false;
				break;
			}
		}
	}

	readMsgReceived(10);

	return true;
}

int CR8062FwUpdateManager::readBinFile(string strFileName, unsigned char* rgcFile, int32_t liFileSize)
{
	// attempt to open the file in the proper mode.
	std::ifstream binFile(strFileName, std::ios::in | std::ios::binary);

	// if the file was successfully open
	if ( binFile )
	{
		// read data from the file until EOF is reached
		while ( binFile.read((char*)rgcFile, liFileSize) ) {};
		binFile.close();
		return 0;
	}

	m_pLogger->log(LOG_ERR, "CR8062Link::readBinFile: failed to open binary file.");
	return -1;
}

vector<vector<unsigned char>> CR8062FwUpdateManager::file2Pkts(const unsigned char *pBinFile, int liMsgLength)
{
	if ( pBinFile == 0 )	return {};

	// 65512 bytes in the first packet
	// 65514 bytes in the other packets
	int liPktNum = 1;
	liMsgLength -= PAYLOAD_LENGTH_FIRST_PACKET;
	int liBytesLeft = liMsgLength % PAYLOAD_LENGTH_INTERMEDIATE_PACKETS;
	liPktNum += ( liBytesLeft == 0 ) ? liMsgLength/PAYLOAD_LENGTH_INTERMEDIATE_PACKETS : liMsgLength/PAYLOAD_LENGTH_INTERMEDIATE_PACKETS + 1;

	vector<vector<unsigned char>> vPkts(liPktNum);
	int liOffset = 0;

	copyFirstHugePktToVector(vPkts[0], pBinFile);
	liOffset += PAYLOAD_LENGTH_FIRST_PACKET;

	for ( int i = 1; i != (liPktNum-1); i++ )
	{
		copyIntermediateHugePktToVector(vPkts[i], pBinFile+liOffset);
		liOffset += PAYLOAD_LENGTH_INTERMEDIATE_PACKETS;
	}

	copyLastHugePktToVector(vPkts[liPktNum-1], liBytesLeft, pBinFile+liOffset);

	return vPkts;
}

bool CR8062FwUpdateManager::copyFirstHugePktToVector(vector<unsigned char>& vFirstPkt, const unsigned char *pBinFile)
{
	// packet size without StartOfFrame, PacketVersion and Packet Length
	int32_t liMsgSize = 0xFFFF;

	vFirstPkt.push_back(PROTO_SOH);
	vFirstPkt.push_back(PROTO_START_OF_FRAME_C);
	vFirstPkt.push_back(PROTO_START_OF_FRAME_T);
	vFirstPkt.push_back(PROTO_PACKET_VERSION);
	vFirstPkt.push_back(liMsgSize >> MSG_CRC_CHAR_BITS);
	vFirstPkt.push_back(liMsgSize & 0xFF);
	vFirstPkt.push_back(0x0F); // ADDR DEST
	vFirstPkt.push_back(0xFF); // ADDR DEST
	vFirstPkt.push_back(0xFF); // ADDR DEST
	vFirstPkt.push_back(0xFF); // ADDR DEST
	vFirstPkt.push_back(0x40); // ADDR SOURCE
	vFirstPkt.push_back(0x00); // ADDR SOURCE
	vFirstPkt.push_back(0x00); // ADDR SOURCE
	vFirstPkt.push_back(0x00); // ADDR SOURCE
	vFirstPkt.push_back(PROTO_CONNECTION_ID);
	vFirstPkt.push_back(0xC0); // 11000000
	vFirstPkt.push_back(PROTO_PAYLOAD_CMD);
	vFirstPkt.push_back(0x00); // ACK
	vFirstPkt.push_back(0x00); // ACK
	vFirstPkt.push_back(0x00); // TRANS NUM
	vFirstPkt.push_back(0x01); // TRANS NUM

	m_liFwSize += 6; // needed because it is a workaround by codecorp
	// Total data size (not only payload)
	vFirstPkt.push_back((uint8_t)(m_liFwSize >> 24));
	vFirstPkt.push_back((uint8_t)(m_liFwSize >> 16));
	vFirstPkt.push_back((uint8_t)(m_liFwSize >> 8));
	vFirstPkt.push_back((uint8_t)m_liFwSize);
	m_liFwSize -= 6;

	vFirstPkt.push_back(0x80);
	vFirstPkt.push_back(0x19);

	string strCmd("RDFD");
	for ( auto c : strCmd )
	{
		vFirstPkt.push_back(c);
	}

	vFirstPkt.insert(vFirstPkt.end(), pBinFile, pBinFile+PAYLOAD_LENGTH_FIRST_PACKET);

	crc_t usiCrc = createCRCfromVector(vFirstPkt, liMsgSize);

	vFirstPkt.push_back(usiCrc >> MSG_CRC_CHAR_BITS);
	vFirstPkt.push_back(usiCrc & 0xFF);

	m_liOffset = PAYLOAD_LENGTH_INTERMEDIATE_PACKETS;
	return true;
}

bool CR8062FwUpdateManager::copyIntermediateHugePktToVector(vector<unsigned char> &vPkt, const unsigned char *pBinFile)
{
	// packet size without StartOfFrame, PacketVersion and Packet Length
	int32_t liMsgSize = 0xFFFF;

	vPkt.push_back(PROTO_SOH);
	vPkt.push_back(PROTO_START_OF_FRAME_C);
	vPkt.push_back(PROTO_START_OF_FRAME_T);
	vPkt.push_back(PROTO_PACKET_VERSION);
	vPkt.push_back(liMsgSize >> MSG_CRC_CHAR_BITS);
	vPkt.push_back(liMsgSize & 0xFF);
	vPkt.push_back(0x0F); // ADDR DEST
	vPkt.push_back(0xFF); // ADDR DEST
	vPkt.push_back(0xFF); // ADDR DEST
	vPkt.push_back(0xFF); // ADDR DEST
	vPkt.push_back(0x40); // ADDR SOURCE
	vPkt.push_back(0x00); // ADDR SOURCE
	vPkt.push_back(0x00); // ADDR SOURCE
	vPkt.push_back(0x00); // ADDR SOURCE
	vPkt.push_back(PROTO_CONNECTION_ID);
	vPkt.push_back(0x80); // 10000000
	vPkt.push_back(PROTO_PAYLOAD_CMD);
	vPkt.push_back(0x00); // ACK
	vPkt.push_back(0x00); // ACK
	vPkt.push_back(0x00); // TRANS NUM
	vPkt.push_back(0x01); // TRANS NUM
	vPkt.push_back((uint8_t)(m_liOffset >> 24));
	vPkt.push_back((uint8_t)(m_liOffset >> 16));
	vPkt.push_back((uint8_t)(m_liOffset >> 8));
	vPkt.push_back((uint8_t)m_liOffset);

	vPkt.insert(vPkt.end(), pBinFile, pBinFile+PAYLOAD_LENGTH_INTERMEDIATE_PACKETS);

	crc_t usiCrc = createCRCfromVector(vPkt, liMsgSize);
	vPkt.push_back(usiCrc >> MSG_CRC_CHAR_BITS);
	vPkt.push_back(usiCrc & 0xFF);

	m_liOffset += PAYLOAD_LENGTH_INTERMEDIATE_PACKETS;

	return true;
}

bool CR8062FwUpdateManager::copyLastHugePktToVector(vector<unsigned char> &vPkt, int liBytesLeft, const unsigned char *pBinFile)
{
	// packet size without StartOfFrame, PacketVersion and Packet Length
	int32_t liMsgSize = liBytesLeft + 21;

	vPkt.push_back(PROTO_SOH);
	vPkt.push_back(PROTO_START_OF_FRAME_C);
	vPkt.push_back(PROTO_START_OF_FRAME_T);
	vPkt.push_back(PROTO_PACKET_VERSION);
	vPkt.push_back(liMsgSize >> MSG_CRC_CHAR_BITS);
	vPkt.push_back(liMsgSize & 0xFF);
	vPkt.push_back(0x0F); // ADDR DEST
	vPkt.push_back(0xFF); // ADDR DEST
	vPkt.push_back(0xFF); // ADDR DEST
	vPkt.push_back(0xFF); // ADDR DEST
	vPkt.push_back(0x40); // ADDR SOURCE
	vPkt.push_back(0x00); // ADDR SOURCE
	vPkt.push_back(0x00); // ADDR SOURCE
	vPkt.push_back(0x00); // ADDR SOURCE
	vPkt.push_back(PROTO_CONNECTION_ID);
	vPkt.push_back(0x80); // 10000000
	vPkt.push_back(PROTO_PAYLOAD_CMD);
	vPkt.push_back(0x00); // ACK
	vPkt.push_back(0x00); // ACK
	vPkt.push_back(0x00); // TRANS NUM
	vPkt.push_back(0x01); // TRANS NUM
	vPkt.push_back((uint8_t)(m_liOffset >> 24));
	vPkt.push_back((uint8_t)(m_liOffset >> 16));
	vPkt.push_back((uint8_t)(m_liOffset >> 8));
	vPkt.push_back((uint8_t)m_liOffset);

	vPkt.insert(vPkt.end(), pBinFile, pBinFile+liBytesLeft);

	size_t size = liMsgSize;
	crc_t usiCrc = createCRCfromVector(vPkt, size);

	vPkt.push_back(usiCrc >> MSG_CRC_CHAR_BITS);
	vPkt.push_back(usiCrc & 0xFF);

	m_liOffset += liBytesLeft;

	return true;
}

bool CR8062FwUpdateManager::sendCustomPkt(const vector<unsigned char> &vPkt, int liMsgLength, int liOffset)
{
	unsigned char ucPkt[65] = {0};
	ucPkt[0] = 0x00;
	ucPkt[1] = liMsgLength;

	for ( int i = 0; i != liMsgLength; i++ )
	{
		ucPkt[i+2] = vPkt[i+liOffset];
	}

	if ( m_pHIDIfr->write(ucPkt) == -1 )
	{
		return false;
	}

	return true;
}

crc_t CR8062FwUpdateManager::createCRCfromVector(vector<unsigned char>& vBuff, size_t usiLength)
{
	crc_t usiCRC = 0;
	usiLength = usiLength - 2; // CRC bytes not to be considered
	int i = MSG_FIRST_PART_HEADER; // skip first part that has not to be considered

	while ( usiLength-- )
	{
		usiCRC = ( usiCRC << MSG_CRC_CHAR_BITS ) ^ crcTab[ (usiCRC >> MSG_CRC_DIFF_BITS ) ^ (vBuff[i++])];
	}

	return usiCRC;
}

bool CR8062FwUpdateManager::checkIfAccepted(uint32_t uliTimeoutMsec)
{
	// We are expecting 3 msgs: ack - response accepted - crc+rest of the msg
	msleep(10);
	int32_t liMsgSize = readMsgReceived(uliTimeoutMsec);
	parseMsgForFWUpgrade();
	liMsgSize = readMsgReceived(uliTimeoutMsec);
	parseMsgForFWUpgrade();

	if ( liMsgSize < 1 )
	{
		m_pLogger->log(LOG_ERR, "CR8062FwUpdateManager::checkIfAccepted: error reading from USB port, %s",
					   errno2CamOperationResult(m_liLastError).c_str());
		return false;
	}

	for (auto val : m_PayloadStruct.pktPayload)
	{
		if ( isdigit(val) )
		{
			switch( val )
			{
				case '0':
				break;

				default:
				{
					string strAns(m_PayloadStruct.pktPayload.begin(), m_PayloadStruct.pktPayload.end());
					int liErrCode = findErrorCode(strAns);
//					m_pCamErr->decodeNotifyEvent((-liErrCode));
					return false;
				}
				break;
			}
		}
	}

	// just to clean the buffer from the misterious msg
	readMsgReceived(uliTimeoutMsec);
	return true;
}

int CR8062FwUpdateManager::findErrorCode(string& strAnsw)
{
	string strDef("<Response Val=\"-");
	size_t pos = strAnsw.find(strDef);
	if ( pos == string::npos )
	{
		return -ERR_CR8062_IN_PARAMETERS;
	}

	string strTmp("");
	for ( size_t i = pos+strDef.size(); i < strAnsw.size(); i++ )
	{
		if ( isdigit(strAnsw[i]) )
		{
			strTmp.push_back(strAnsw[i]);
		}
		else
		{
			break;
		}
	}

	int liErrorCode = stoi(strTmp);

	switch (liErrorCode)
	{
		case 6:
			return -ERR_CR8062_IN_PARAMETERS;

		case ERR_CR8062_NOT_SUPPORTED_CMD:
			return -ERR_CR8062_NOT_SUPPORTED_CMD;

		case ERR_CR8062_FLASH_PROGR:
			return -ERR_CR8062_FLASH_PROGR;

		default:
			return -ERR_CR8062_IN_PARAMETERS;
	}
}
