/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Protocol.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CR8062Protocol class.
 @details

 ****************************************************************************
*/

#ifndef CR8062PROTOCOL_H
#define CR8062PROTOCOL_H

#include "USBComHandler.h"
#include "CR8062Include.h"
#include "iostream"
//#include "USBErrors.h"

typedef enum
{
	eNoReq = -1,
	eGet,
	eSet,
	eAction
} enumRequestType;


class CR8062Protocol : public USBComHandler
{
	public:

		/*! **********************************************************************************************************************
		 * @brief CodeCorpCR8062 default constructor. Initialize all the members.
		 * ***********************************************************************************************************************
		 */
		CR8062Protocol();

		/*! **********************************************************************************************************************
		 * @brief ~CodeCorpCR8062 virtual destructor of the class. Close Serial interface.
		 * ***********************************************************************************************************************
		 */
		virtual ~CR8062Protocol();

		/*! **********************************************************************************************************************
		 * @brief clearRxBuff
		 * ***********************************************************************************************************************
		 */
		void clearRxBuff();

//		/*! **********************************************************************************************************************
//		 * @brief  setCameraErrors set m_pCameraError
//		 * @param  pCameraError pointer to USBErrors instance
//		 * @return true in case of success, false otherwise
//		 * ***********************************************************************************************************************
//		 */
//		bool setCR8062Errors(USBErrors* pCameraError);

//		/*! **********************************************************************************************************************
//		 * @brief  getCamErr wrapper function used to get m_pCamErr instance
//		 * @return m_pCamErr
//		 * ***********************************************************************************************************************
//		 */
//		USBErrors* getCamErr(void);

	protected:

		/*! **********************************************************************************************************************
		 * @brief setPayloadSize set the size of the payload
		 * @param liSize size desired
		 * ***********************************************************************************************************************
		 */
		void setPayloadSize(int liSize);

		/*! **********************************************************************************************************************
		 * @brief  readAndParseMsg performs all the readings that are necessary to get everything the camera sends
		 * @param  eType type of request asked to the camera
		 * @param  liPayloadSize size of the payload
		 * @param  liTimeOutMs reading timeout in milliseconds
		 * @return payloadBytes or 0 in case of success, -error code otherwise
		 * ***********************************************************************************************************************
		 */
		int readAndParseMsg(enumRequestType eType, int& liPayloadSize,int liTimeOutMs);

		/*! **********************************************************************************************************************
		 * @brief  sendPkt* used to send a command to the camera in CDC or HID mode.
		 * @param  strCmd string associated to the command desired.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		bool sendPktCDC(const string& strCmd);
		bool sendPktHID(const string& strCmd);

		bool sendAckCDC(void);
		bool sendAckHID(void);

		/*! **********************************************************************************************************************
		 * @brief  parseMsgForFWUpgrade is used to parse a message coming from the CR8062 camera.
		 * @param  liImageSize is used as argument if an image is expected to be received.
		 * @return true if the parser operation is successful, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool parseMsgForFWUpgrade();

		/*! ***********************************************************************************************************
		 * @brief  receivePkt* read from USB port in CDC or HID mode
         * @param  liTimeoutMs reading timeout in milliseconds
		 * @return -1 upon error, 0 upon timeout, otherwise the number of bytes read
		 * ************************************************************************************************************
		 */
		int32_t receivePktCDC(int32_t liTimeoutMs);
		int32_t receivePktHID(int32_t liTimeoutMs);

		/*! **********************************************************************************************************************
		 * @brief  errno2CamOperationResult handle errno coming from serial communication with the camera.
		 * @param  liErrno errno ID.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		string errno2CamOperationResult(int32_t liErrno);

		/*! ***********************************************************************************************************
		 * @brief  createCRC is used to create CRC to complete the packet to be sent.
		 * @param  pucBuff pointer to the destination address of the packet.
		 * @param  usiLength = packetHeader.packetLength.
		 * @return the CRC bytes (16 bit).
		 * ************************************************************************************************************
		 */
		crc_t createCRC(const unsigned char* pucBuff, size_t usiLength);


	private:

		/*! **********************************************************************************************************
		 * @brief setFixedParameters set fixed parameter of the msg to be sent.
		 * @param pucCmdBuf pointer to the msg.
		 * ************************************************************************************************************
		 */
		void setFixedParameters(unsigned char* pucCmdBuf);

		/*! **********************************************************************************************************
		 * @brief  checkCRCforPacketLoss check if some packets are lost through CRC control.
		 * @param  pucRxBuf array of packet elements.
		 * @param  usiLength = packetHeader.packetLength.
		 * @return true if there are no packet lost, false otherwise.
		 * ************************************************************************************************************
		 */
		bool checkCRCforPacketLoss(const uint8_t * pucRxBuf, uint32_t usiLength = 0);

		/*! **********************************************************************************************************
		 * @brief parseHeader parse the header struct. (RX)
		 * @param pucRxBuf pointer to the message received.
		 * ************************************************************************************************************
		 */
		void  parseHeader(const unsigned char* pucRxBuf);

    protected:

        USBInterface*		m_pUSBIfr;
        HIDInterface*		m_pHIDIfr;
        Log*				m_pLogger;
//        USBErrors*			m_pCamErr;

        payload				m_PayloadStruct;
        int32_t				m_liLastError;
        uint8_t				m_rgcBuff[MSG_PACKET_MAX_LENGTH];
        int32_t				m_liPktReceived;
        uint16_t			m_usTransactionNum;
        bool				m_bConfigOk;

    private:

        int32_t				m_liByteReceived;
        int32_t				m_liByteCopied;
        crc_t				m_usiCrc;
        packetHeader		m_HeaderStruct;
        connectionProtocol	m_ProtocolStruct;

};

#endif // CR8062PROTOCOL_H
