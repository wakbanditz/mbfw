/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    USBComHandler.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the USBComHandler class.
 @details

 ****************************************************************************
*/

#include "USBComHandler.h"

enumDeviceType eUSBType = eNoDevice;

USBComHandler::USBComHandler()
{
	m_pDevType = &eUSBType;
}

USBComHandler::~USBComHandler()
{

}

void USBComHandler::setConfig(enumDeviceType eType)
{
	*m_pDevType = eType;
}

enumDeviceType USBComHandler::getConfig()
{
	return eUSBType;
}

bool USBComHandler::sendCmd(const string& strCmd)
{
	bool lbRes = false;

	if ( *m_pDevType == eCDCDevice )
	{
		lbRes = sendPktCDC(strCmd);
	}
	else if ( *m_pDevType == eHIDDevice)
	{
		lbRes = sendPktHID(strCmd);
	}

	return lbRes;
}

bool USBComHandler::sendACK()
{
	bool lbRes = false;

	if ( *m_pDevType == eCDCDevice )
	{
		lbRes = sendAckCDC();
	}
	else if ( *m_pDevType == eHIDDevice)
	{
		lbRes = sendAckHID();
	}

	return lbRes;
}

int32_t USBComHandler::readMsgReceived(int32_t liTimeoutMs)
{
	int liRes = -1;

	if ( *m_pDevType == eCDCDevice )
	{
		liRes = receivePktCDC(liTimeoutMs);
	}
	else if ( *m_pDevType == eHIDDevice)
	{
		liRes = receivePktHID(liTimeoutMs);
	}

	return liRes;
}

bool USBComHandler::sendPktHID(const string& /*strCmd*/)
{
	return true;
}

int32_t USBComHandler::receivePktHID(int32_t /*liTimeoutMs*/)
{
	return true;
}

bool USBComHandler::sendPktCDC(const string& /*strCmd*/)
{
	return true;
}

bool USBComHandler::sendAckHID()
{
	return true;
}

bool USBComHandler::sendAckCDC()
{
	return true;
}

int32_t USBComHandler::receivePktCDC(int32_t /*liTimeoutMs*/)
{
	return true;
}
