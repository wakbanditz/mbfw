/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    USBLinkBoard.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the USBLinkBoard class.
 @details

 ****************************************************************************
*/

#include "USBLinkBoard.h"

USBLinkBoard::USBLinkBoard()
{
	m_pCR8062 = nullptr;
//	m_pUSBErr = nullptr;
//	m_bIsUSBErrEnabled = false;
}

USBLinkBoard::~USBLinkBoard()
{
//	SAFE_DELETE(m_pUSBErr);
	SAFE_DELETE(m_pCR8062);
}

int8_t USBLinkBoard::createAndInitUSBInterfaces()
{
	m_USBIfrCollector.setLogger(getLogger());
	int8_t cRet = m_USBIfrCollector.createAndInitInterfaces(CONF_FILE);

//	SAFE_DELETE(m_pUSBErr);
//	m_pUSBErr = new USBErrors();
//	if ( m_pUSBErr == nullptr )
//	{
//		log(LOG_ERR, "USBLinkBoard::createAndInitUSBInterfaces: memory error");
//	}

//	// Init NSH errors handling
//	m_bIsUSBErrEnabled = false;
//	int liRes = m_pUSBErr->initCameraErr(m_pLogger);
//	if ( liRes )
//	{
//		string strFileName(CAMERA_ERROR_FILE_NAME);
//		log(LOG_ERR, "USBLinkBoard::createAndInitUSBInterfaces: unable to open %s", strFileName.c_str());
//	}
//	m_bIsUSBErrEnabled = true;

	return cRet;
}

int8_t USBLinkBoard::destroyUSBInterfaces()
{
	m_USBIfrCollector.destroyInterfaces();
	return 0;
}

int8_t USBLinkBoard::createAndInitCameraReader(void)
{
	SAFE_DELETE(m_pCR8062);
	m_pCR8062 = new CR8062Link();
	if ( ! m_pCR8062 )
	{
		log(LOG_ERR, "USBLinkBoard::createAndInitCameraReader: unable to create CR8062, memory error");
		return -1;
	}

//	// Set pointer to USBErrors class
//	bool bRes = m_pCR8062->m_Camera.setCR8062Errors(m_pUSBErr);
//	if ( ! bRes )
//	{
//		return -1;
//	}
//	m_pCR8062->m_Reader.setCR8062Errors(m_pUSBErr);

	// Init device
    bool bRes = m_pCR8062->init(getLogger(), m_USBIfrCollector.getInterfaceByIndex(eUSB_0));
	if ( bRes == false )
	{
		log(LOG_ERR, "USBLinkBoard::createAndInitCameraReader: unable to initialize CR8062");
		return -1;
	}

	return 0;
}

int8_t USBLinkBoard::destroyCameraReader()
{
	SAFE_DELETE(m_pCR8062);
	return 0;
}

//bool USBLinkBoard::isUSBErrEnabled()
//{
//	return m_bIsUSBErrEnabled;
//}

