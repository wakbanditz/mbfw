/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    USBLinkBoard.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the USBLinkBoard class.
 @details

 ****************************************************************************
*/
#ifndef USBLINKBOARD_H

#define USBLINKBOARD_H

#include "USBInterfaceCollector.h"
#include "CR8062Link.h"
//#include "USBErrors.h"
#include "CommonInclude.h"

class USBLinkBoard : public Loggable
{
	public:

		/*! **********************************************************************************************************************
		 * @brief USBLinkBoard default constructor.
		 * ***********************************************************************************************************************
		 */
		USBLinkBoard();

		/*! **********************************************************************************************************************
		 * @brief ~USBLinkBoard default destructor.
		 * ***********************************************************************************************************************
		 */
		virtual ~USBLinkBoard();

		/*! **********************************************************************************************************************
		 * @brief  createUSBInterfaces create all USB interfaces.
		 * @return 0 in case of successs, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t createAndInitUSBInterfaces(void);

		/*! **********************************************************************************************************************
		 * @brief  destroyUSBInterfaces destroy all USB interfaces.
		 * @return 0 in case of successs, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t destroyUSBInterfaces(void);

		/*! **********************************************************************************************************************
		 * @brief  createAndInitCameraReader create and initialize a camera and a reader object.
		 * @return 0 in case of successs, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t createAndInitCameraReader(void);

		/*! **********************************************************************************************************************
		 * @brief  destroyCameraReader object.
		 * @return 0 always.
		 * ***********************************************************************************************************************
		 */
		int8_t destroyCameraReader(void);

		/*! **********************************************************************************************************************
		 * @brief  isUSBErrEnabled
		 * @return true if enabled, false otherwise
		 * ***********************************************************************************************************************
		 */
		bool isUSBErrEnabled(void);

    public:

        CR8062Link* m_pCR8062;

//        // Shared between the reader and the camera
//        USBErrors	*m_pUSBErr;

    protected:

        USBInterfaceCollector m_USBIfrCollector;

    private:

//        bool m_bIsUSBErrEnabled;

};

#endif // USBLINKBOARD_H
