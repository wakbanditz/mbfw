/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    I2CBoardBase.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the I2CBoardBase class.
 @details

 ****************************************************************************
*/

#include "I2CBoardBase.h"

#include "I2CInterface.h"
#include "Log.h"


uint8_t I2CBoardBase::m_numI2CLinkBoard = 0;

I2CBoardBase::I2CBoardBase()
{
	m_pLogger	= NULL;
	m_pI2CIfr	= NULL;

	m_slDeviceAddress = -1;

	m_numI2CLinkBoard++;
}

I2CBoardBase::~I2CBoardBase()
{

}

int I2CBoardBase::init( Log * pLogger, I2CInterface * pI2CIfr )
{
	if ( ( pLogger == NULL ) ||
		 ( pI2CIfr == NULL ) )
	{
		return -1;
	}
	else
	{
		m_pLogger = pLogger;
		m_pI2CIfr = pI2CIfr;

		m_pLogger->log( LOG_INFO, "I2CLinkBoard::init-->Num I2C device initialized:%d", m_numI2CLinkBoard);
	}

	return 0;
}

int I2CBoardBase::setDeviceAddress( int32_t slDeviceAddress )
{
	if ( slDeviceAddress > 0 )
	{
		m_slDeviceAddress = slDeviceAddress;
	}
	else
	{
		m_pLogger->log(LOG_ERR, "I2CLinkBoard::setDeviceAddress-->ERR");
		return -1;
	}

	return 0;
}

int I2CBoardBase::getDeviceAddress( int32_t &slDeviceAddress )
{
	if ( m_slDeviceAddress >= 0 )
	{
		slDeviceAddress = m_slDeviceAddress;
	}
	else
	{
		m_pLogger->log(LOG_ERR, "I2CLinkBoard::getDeviceAddress-->ERR");
		return -1;
	}

	return 0;
}

int I2CBoardBase::write(uint8_t ubCmd, uint8_t *pubTxBuff, uint16_t usLen)
{
	if ( ( pubTxBuff == NULL )	||
		 ( usLen == 0) )
	{
		int32_t slDeviceAddress = -1;
		int ret = getDeviceAddress(slDeviceAddress);
		if ( ret == 0 )
		{
			bool bRet = m_pI2CIfr->singleWriteGeneral(slDeviceAddress, &ubCmd, 1);
			if ( bRet == false )
			{
				m_pLogger->log( LOG_ERR, "I2CBoardBase::write-->Error on write operation");
				return -1;
			}
		}
	}
	else
	{
		int32_t slDeviceAddress = -1;
		int ret = getDeviceAddress(slDeviceAddress);
		if ( ret == 0 )
		{
			bool bRet = m_pI2CIfr->multiWrite( slDeviceAddress, pubTxBuff, usLen, ubCmd);
			if ( bRet == false )
			{
				m_pLogger->log( LOG_ERR, "I2CBoardBase::write-->Error on write operation");
				return -1;
			}
		}
	}

    usleep(200);

	return 0;
}

int I2CBoardBase::writeCustomAdd(uint8_t ubAdd, uint8_t ubCmd)
{
	bool bRet = m_pI2CIfr->singleWriteGeneral(ubAdd, &ubCmd, 1);
	if ( bRet == false )
	{
		m_pLogger->log( LOG_ERR, "I2CBoardBase::writeCustomAdd-->Error on write operation");
		return -1;
	}
	return 0;
}

int I2CBoardBase::writeCustomAdd(uint8_t ubAdd, uint8_t ubCmd, uint8_t *pubTxBuff, uint16_t usLen)
{

	bool bRet = m_pI2CIfr->multiWrite( ubAdd, pubTxBuff, usLen, ubCmd);
	if ( bRet == false )
	{
		m_pLogger->log( LOG_ERR, "I2CBoardBase::writeCustomAdd-->Error on write operation");
		return -1;
	}

	return 0;
}

int I2CBoardBase::read(uint8_t ubCmd, uint8_t *pubRxBuff, uint16_t usLen)
{
	if ( ( pubRxBuff == NULL )	||
		 ( usLen == 0) )
	{
        m_pLogger->log( LOG_ERR, "I2CBoardBase::read-->ERR Parameters");
		return -1;
	}
	else
	{
		int32_t slDeviceAddress = -1;
		int ret = getDeviceAddress(slDeviceAddress);
		if ( ret == 0 )
		{
			bool bRet = m_pI2CIfr->multiRead( slDeviceAddress, pubRxBuff, usLen, ubCmd);
			if ( bRet == false )
			{
                m_pLogger->log( LOG_ERR, "I2CBoardBase::read-->Error on read operation");
				return -1;
			}
		}
	}

	return 0;
}






