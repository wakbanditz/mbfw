/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    UCD9090a.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the UCD9090a class.
 @details

 ****************************************************************************
*/

#include "UCD9090a.h"

#include "PmBusCmd.h"
#include <math.h>

//#define msleep(ms) usleep(ms*1000)

UCD9090a::UCD9090a()
{
	ubCurrentRail = 0;

	memset(stPinMon, 0, sizeof(stPinMon));
	memset(stGPIO, 0, sizeof(stGPIO));
	memset(stRail, 0, sizeof(stRail));

	stGPIO[0].ubName="FPWM1_GPIO5";
	stGPIO[1].ubName="FPWM2_GPIO6";
	stGPIO[2].ubName="FPWM3_GPIO7";
	stGPIO[3].ubName="FPWM4_GPIO8";
	stGPIO[4].ubName="FPWM5_GPIO9";
	stGPIO[5].ubName="FPWM6_GPIO10";
	stGPIO[6].ubName="FPWM7_GPIO11";
	stGPIO[7].ubName="FPWM8_GPIO12";
	stGPIO[8].ubName="GPI1_PWM1";
	stGPIO[9].ubName="GPI2_PWM2";
	stGPIO[10].ubName="GPIO14";
	stGPIO[11].ubName="TDO_GPIO19";
	stGPIO[12].ubName="TCK_GPIO18";
	stGPIO[13].ubName="TMS_GPIO21";
	stGPIO[14].ubName="TDI_GPIO20";
	stGPIO[15].ubName="GPIO1";
	stGPIO[16].ubName="GPIO2";
	stGPIO[17].ubName="GPIO3";
	stGPIO[18].ubName="GPIO4";
	stGPIO[19].ubName="GPIO13";
	stGPIO[20].ubName="GPIO15";
	stGPIO[21].ubName="GPIO16";
	stGPIO[22].ubName="GPIO17";

    for (int i = 0; i < UCD9090A_NUM_RAILS_MAX; i++)
	{
        stRail[i].unNumRail = i + 1;
	}

}

UCD9090a::~UCD9090a()
{

}

void UCD9090a::printGpioConfig( void )
{

	m_pLogger->log(LOG_INFO," GPIO CONFIG");
    for ( int i = 0; i < UCD9090A_MAX_NUM_GPIO; i++)
	{
        m_pLogger->log(LOG_INFO, "< GPIO: %s, PinID: %i, DIR: %d, EN_PIN: %d, RAIL: %d >",
                       stGPIO[i].ubName.c_str(), i, stGPIO[i].ubDir, stGPIO[i].ubEnablePin, stGPIO[i].ubEnableRail);
	}
	m_pLogger->log(LOG_INFO,">--------------------------<");

}


void UCD9090a::printRailConfig( void )
{
	m_pLogger->log(LOG_INFO," RAIL CONFIG");
	for ( int i=0; i<UCD9090A_NUM_RAILS_MAX; i++)
	{
		m_pLogger->log(LOG_INFO, "< Num RAIL: %d, MON_1: %d, MON_2: %d, MON_3: %d, EnablePinID: %d >",
                       stRail[i].unNumRail, stRail[i].stMonPin[0].ubMonitorType, stRail[i].stMonPin[1].ubMonitorType,
                        stRail[i].stMonPin[2].ubMonitorType, stRail[i].stEnalePin.ubPinIDDef);
	}
	m_pLogger->log(LOG_INFO,">--------------------------<");

}

void UCD9090a::printPinMonConfig( void )
{
	m_pLogger->log(LOG_INFO," MON CONFIG");
	for ( int i=0; i<UCD9090A_MAX_NUM_MON_PINS; i++)
	{
		m_pLogger->log(LOG_INFO, "< NumPinMon: %d, MonType: %d, Page:%d >", i, stPinMon[i].ubMonitorType, stPinMon[i].ubPageByte);

	}
	m_pLogger->log(LOG_INFO,">--------------------------<");

}

/* *****************************************************************************************
 *
 * SET operations
 *
 * ******************************************************************************************/

int UCD9090a::setGPIO(uint8_t ubNumGpio, eUCD9090aGpioOutEnable ubDir, eUCD9090aGpioOutValue ubValue )
{
	int res = writeSelectGPIO( ubNumGpio );
	if ( res != 0 )
	{
		return -1;
	}

	res = writeConfigGPIO( ubDir, ubValue );
	if ( res != 0 )
	{
		return -1;
	}

	return 0;
}

int UCD9090a::setPinMonitorConfigRAM( uint8_t ubNumPin, uint8_t ubMonitorType, uint8_t ubPageByte)
{
	if ((ubNumPin >= UCD9090A_MAX_NUM_MON_PINS)	||
		(ubMonitorType > 4)					||
		(ubPageByte >= UCD9090A_NUM_RAILS_MAX))
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::setPinMonitorConfig--> Parameters out of range ");
		return -1;
	}

	stPinMon[ubNumPin].ubMonitorType = ubMonitorType;
	stPinMon[ubNumPin].ubPageByte = ubPageByte;


	printPinMonConfig();

	return 0;
}

int UCD9090a::setOperationOnlyMode( uint8_t ubRail )
{

    //select the rail to read
    int res = writeSetPage( ubRail );
    if ( res != 0 )
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::setOperationOnlyMode--> Failed to set the rail ");
        return -1;
    }

    res = writeOnOffConfig(ON_OFF_OPERATION_ONLY);
    if ( res != 0 )
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::setOperationOnlyMode--> Failed to set operation only mode ");
        return -1;
    }
    else
    {
        m_pLogger->log(LOG_DEBUG, "UCD9090a::setOperationOnlyMode--> Set operation only mode ");
    }

    return 0;
}

int UCD9090a::setControlPinOnlyMode( uint8_t ubRail )
{
    //select the rail to read
    int res = writeSetPage( ubRail );
    if ( res != 0 )
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::setControlPinOnlyMode--> Failed to set the rail ");
        return -1;
    }

    res = writeOnOffConfig(ON_OFF_CONTROL_PIN);
    if ( res != 0 )
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::setControlPinOnlyMode--> Failed to set control pin only mode ");
        return -1;
    }
    else
    {
        m_pLogger->log(LOG_DEBUG, "UCD9090a::setControlPinOnlyMode--> Set control pin only mode ");
    }

    return 0;
}

int UCD9090a::setPowerGoodMode( uint8_t ubPin, bool bval )
{
    uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
    uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
    memset(pubRxBuff, 0, sizeof(pubRxBuff));
    memset(pubTxBuff, 0, sizeof(pubTxBuff));

    int res = read( PMBUS_MFR_GPO_CONFIG, pubRxBuff, GPO_CONFIG_BYTE_COUNT );
    if (res != 0)
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::setPowerGoodMode--> GPO read register failed ");
        return -1;
    }
    else
    {
        pubTxBuff[0] = GPO_CONFIG_BYTE_COUNT;
        memcpy( &pubTxBuff[1], &pubRxBuff[1], GPO_CONFIG_BYTE_COUNT );
        pubTxBuff[1] = ubPin;

        //TODO to make general: this is only for one specific GPO in PAGE7
        if ( bval == true )
        {
            pubTxBuff[13] = PM_OPERATION_IMMEDIATE_OFF;
        }
        else
        {
            pubTxBuff[13] = PM_OPERATION_ON;
        }

        //number of bytes to write is 1 more than read bytes
        res = write( PMBUS_MFR_GPO_CONFIG, pubTxBuff, GPO_CONFIG_BYTE_COUNT + 1 );
        if ( res != 0 )
        {
            m_pLogger->log(LOG_ERR, "UCD9090a::setPowerGoodMode--> write FAILED ");
            return -1;
        }
    }

    return 0;
}

int UCD9090a::reloadButton()
{
    int res = setGPIO(19, GPIO_OUT_ENABLE_OK, GPIO_OUT_VALUE_LOW);
    if ( res != 0 )
    {
        return -1;
    }

    res = setGPIO(19, GPIO_OUT_ENABLE_OK, GPIO_OUT_VALUE_HIGH);
    if ( res != 0 )
    {
        return -1;
    }
    return 0;
}

/* *****************************************************************************************
 *
 * GET operations
 *
 * ******************************************************************************************/

int UCD9090a::getGpioConfig()
{
    for (int idxGpio = 0; idxGpio < UCD9090A_MAX_NUM_GPIO; idxGpio++)
	{
		uint8_t	ubReg;
		readGPIO( idxGpio, ubReg );

		msleep(10);
	}


    for (int idxPage = 0; idxPage < UCD9090A_NUM_RAILS_MAX; idxPage++)
	{
		readSeqConfig((eUCD9090aRail)idxPage);

		msleep(10);
	}
	return 0;
}

int UCD9090a::getMonitorConfig(uint8_t ubNumMonPin, uint8_t &ubMonType, uint8_t &ubPageByte)
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if ( ubNumMonPin < UCD9090A_MAX_NUM_MON_PINS )
	{
		ubMonType   = stPinMon[ubNumMonPin].ubMonitorType;
		ubPageByte  = stPinMon[ubNumMonPin].ubPageByte;
	}
	else
	{
		m_pLogger->log(LOG_DEBUG_PARANOIC,"UCD9090a::getMonitorConfig-->Failed");
		return -1;
	}

	return 0;
}

int UCD9090a::getSeqConfig( eUCD9090aRail eRail, stEnablePinConfig &stPinConfig )
{
	if ( eRail >= UCD9090A_NUM_RAILS_MAX)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::getSeqConfig-->Num rail out of limit ");
		return -1;
	}

	memcpy( &stPinConfig, &stRail[eRail].stEnalePin, sizeof(stPinConfig));

	return 0;
}

/* *****************************************************************************************
 *
 * WRITE operations
 *
 * ******************************************************************************************/

int UCD9090a::writeResetSoft( void )
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	int res = write(PMBUS_MFR_SOFT_RESET, pubTxBuff, 0);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::resetSoft-->Failed");
		return -1;
	}

	return 0;
}

int UCD9090a::writeClearFaults( void )
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	int res = write(PMBUS_CLEAR_FAULTS, pubTxBuff, 0);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::writeClearFaults-->Failed");
		return -1;
	}

	return 0;
}

int UCD9090a::writeLoggedFaults( void )
{
    uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
    memset(pubTxBuff, 0, sizeof(pubTxBuff));

    pubTxBuff[0] = 12;
    int res = write(PMBUS_MFR_LOGGED_FAULTS, pubTxBuff, 13);
    if (res != 0)
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::writeClearFaults-->Failed");
        return -1;
    }

    return 0;
}

int UCD9090a::writeUserRAM(uint8_t ubVal)
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubVal;

	int res = write(PMBUS_MFR_USER_RAM, pubTxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::writeUserRAM-->Failed");
		return -1;
	}

	return 0;
}

int UCD9090a::writeStoreDefaultAll( void )
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	int res = write(PMBUS_STORE_DEFAULT_ALL, pubTxBuff, 0);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::writeStoreDefaultAll-->Failed");
		return -1;
	}

	return 0;
}

int UCD9090a::writeSetPage(uint8_t ubPage)
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubPage;

	int res = write(PMBUS_PAGE, pubTxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::setPage--> Impossible to set related page");
		return -1;
	}

	ubCurrentRail = ubPage;

	return 0;
}

int UCD9090a::writeVoutMode( uint8_t ubExp )
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];

	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubExp;

	int res = write(PMBUS_VOUT_MODE, pubTxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::writeVoutMode--> Impossible to set related page");
		return -1;
	}

	return 0;
}

int UCD9090a::writeSelectGPIO( uint8_t ubNumGpio )
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubNumGpio >= UCD9090A_MAX_NUM_GPIO)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::writeSelectGPIO--> GPIO read OutOfRange ");
		return -1;
	}

	pubTxBuff[0] = ubNumGpio;

	int res = write( PMBUS_MFR_GPIO_SELECT, pubTxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::writeVoutMode--> GPIO[%d] selection failed ", ubNumGpio);
		return -1;
	}

	return 0;
}

int UCD9090a::writeConfigGPIO(eUCD9090aGpioOutEnable ubDir, eUCD9090aGpioOutValue ubValue)
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] |= GPIO_ENABLE;
	if ( ubDir == GPIO_OUT_ENABLE_OK )
	{
		pubTxBuff[0] |= GPIO_OUT_ENABLE;

		if ( ubValue == GPIO_OUT_VALUE_HIGH )
		{
			pubTxBuff[0] |= GPIO_VALUE;
		}
	}

	int res = write( PMBUS_MFR_GPIO_CONFIG, pubTxBuff, 1 );
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::writeVoutMode--> GPIO config failed ");
		return -1;
	}

	return 0;
}

int UCD9090a::writePinMonitorConfig( void )
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	// CMD | BYTE_COUNT | Mon 1 Type and Page | Mon 2 Type and Page | ..... | Mon N Type and Page
	//Max number of monitor pins + 1 byte fro bytecount
	//Pin 7-5	Mon Type
	//Pin 4-0	Page Byte
	pubTxBuff[0] =  UCD9090A_MAX_NUM_MON_PINS;

	for (int i = 0; i<UCD9090A_MAX_NUM_MON_PINS; i++ )
	{
		uint8_t ubMonConf = 0;
		ubMonConf =	(stPinMon[i].ubMonitorType << 5) | stPinMon[i].ubPageByte;

		pubTxBuff[i+1] = ubMonConf;
	}

	int res = write( PMBUS_MFR_MONITOR_CONFIG, pubTxBuff, UCD9090A_MAX_NUM_MON_PINS+1);
	if ( res != 0 )
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::writeVoutMode--> failed ");
		return -1;
	}

	return 0;
}

int UCD9090a::writeSeqConfig(uint8_t eRail, stEnablePinConfig &stPinConfig)
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));
	uint8_t ubLen = UCD9090A_SEQ_CONFIG_CMD_NUMBYTE;

	if ( eRail >= UCD9090A_NUM_RAILS_MAX)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readSeqConfig-->Num rail out of limit ");
		return -1;
	}

	//select the rail to read
	int res = writeSetPage( eRail );
	if ( res != 0 )
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readSeqConfig--> Failed to set the rail ");
		return -1;
	}

	pubTxBuff[0] = ubLen;
	pubTxBuff[1] = (stPinConfig.ubPinIDDef << 3) | (stPinConfig.ubPolarity << 2) | stPinConfig.ubMode;
	//TODO config sequences

	res = write( PMBUS_MFR_SEQ_CONFIG , pubTxBuff, UCD9090A_SEQ_CONFIG_CMD_NUMBYTE+1);
	if ( res != 0 )
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::writeSeqConfig--> failed ");
		return -1;
	}

	return 0;
}

int UCD9090a::writeVoutCommand(uint8_t ubSetPage, float fVCmd)
{
	uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	int res = writeSetPage( ubSetPage );
	if ( res != 0 )
	{
		return -1;
	}


	int8_t ubExp;
	res = readVoutMode(ubExp);
	if ( res != 0 )
	{
		return -1;
	}

	int16_t ssMolt = exp2(ubExp);

	float fVal = ssMolt*fVCmd;
	uint16_t usVal = (uint16_t)fVal;

	pubTxBuff[0] = (usVal & 0xFF00) >> 8;
	pubTxBuff[1] = usVal & 0x00FF;

	res = write( PMBUS_VOUT_COMMAND, pubTxBuff, 2);
	if ( res != 0 )
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::writeVoutCommand--> failed ");
		return -1;
	}

	return 0;
}

int UCD9090a::writeOnOffConfig( uint8_t ubReg )
{
    int res;
    uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
    memset(pubTxBuff, 0, sizeof(pubTxBuff));

    pubTxBuff[0] = ubReg;

    res = write( PMBUS_ON_OFF_CONFIG, pubTxBuff, 1 );
    if ( res != 0 )
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::writeOnOffConfig--> failed ");
        return -1;
    }
    return 0;
}

int UCD9090a::writeOperation( uint8_t ubVal )
{
    int res;
    uint8_t pubTxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
    memset(pubTxBuff, 0, sizeof(pubTxBuff));

    pubTxBuff[0] = ubVal;

    res = write( PMBUS_OPERATION, pubTxBuff, 1 );
    if ( res != 0 )
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::writeOperation--> failed ");
        return -1;
    }

    return 0;
}
/* *****************************************************************************************
 *
 * Read operations
 *
 * ******************************************************************************************/
int UCD9090a::readPage(uint8_t &ubCurrPage)
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(PMBUS_PAGE, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readPage--> Failed");
	}
	else
	{
		ubCurrPage = pubRxBuff[0];
	}

	return 0;
}

int UCD9090a::readDeviceID(string &pubDeviceID)
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PMBUS_MFR_DEVICE_ID, pubRxBuff, 27);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readDeviceID--> Failed");
	}
	else
	{
		pubDeviceID = (char*)&pubRxBuff[0];
	}

	return 0;
}

int UCD9090a::readVoutMode( int8_t &ubExp )
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];

	linear11_t mode;

	memset(pubRxBuff, 0, sizeof(pubRxBuff));


	int res = read(PMBUS_VOUT_MODE, pubRxBuff, 1);
	if ( res == 0)
	{
		mode.mantissa = pubRxBuff[0] & 0x1F;
		ubExp = ~mode.mantissa + 1;

	}
	else
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readVoutMode--> Failed");
		return -1;
	}

	return 0;
}

int UCD9090a::readVout(float &fVout)
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];

	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int8_t ubExp;
	int res = readVoutMode(ubExp);
	if (res == 0)
	{
		res = read(PMBUS_READ_VOUT, pubRxBuff, 2);
		if ( res == 0)
		{
			uint16_t usVout;
			usVout = pubRxBuff[1] << 8;
			usVout |= pubRxBuff[0];

			int16_t ssDiv = exp2(ubExp);
			fVout = (float)usVout/ssDiv;
		}
		else
		{
			m_pLogger->log(LOG_ERR, "UCD9090a::readVout--> Impossible to read Vout");
			return -1;
		}
	}
	else
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readVout--> Impossible to read VoutMode");
		return -1;
	}


	return 0;
}

int UCD9090a::readTemperature(uint8_t ubPage, uint8_t index, float & fTemp)
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];

	uint16_t usTemp = 0;

    // change device page if necessary (0 -> direct reading of internal sensor)
    if(ubPage > 0)
    {
        if(writeSetPage( ubPage ) != 0 )
        {
            return -1;
        }
    }

	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = 0;
	switch(index)
	{
		case 1 :
			res = read(PMBUS_READ_TEMPERATURE_1, pubRxBuff, 2);
		break;
		case 2 :
			res = read(PMBUS_READ_TEMPERATURE_2, pubRxBuff, 2);
		break;
		case 3 :
			res = read(PMBUS_READ_TEMPERATURE_3, pubRxBuff, 2);
		break;

	}
	if ( res == 0)
	{

		usTemp = pubRxBuff[1] << 8;
		usTemp |= pubRxBuff[0];

        m_pLogger->log(LOG_DEBUG_PARANOIC, "UCD9090a::readTemperature--> index %d usTemp %d", index, usTemp);

        fTemp = conversionLinear112Float(usTemp);
	}
	else
	{
        m_pLogger->log(LOG_ERR, "UCD9090a::readTemperature--> Error");
        return -1;
	}

	return 0;
}

int UCD9090a::readGPIO(uint8_t ubNumGPIO, uint8_t &ubGpioReg)
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];

	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = writeSelectGPIO( ubNumGPIO );
	if ( res != 0 )
	{
		return -1;
	}

	res = readRegGPIO(ubGpioReg);
	if (res != 0)
	{
		return -1;
	}

	//VALUE
	stGPIO[ubNumGPIO].ubValue = (ubGpioReg & 0x04) >> 2;

	//DIRECTION
	uint8_t ubGPIOdir= (ubGpioReg & 0x02) >> 1;
	if (ubGPIOdir == GPIO_OUT_ENABLE_NOK)
	{
		stGPIO[ubNumGPIO].ubDir = GPIO_CONF_INPUT;
	}
	else
	{
		stGPIO[ubNumGPIO].ubDir = GPIO_CONF_OUTPUT;
	}


	return 0;
}

int UCD9090a::readRegGPIO( uint8_t &ubGpioReg )
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubGpioReg >= UCD9090A_MAX_NUM_GPIO)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readRegGPIO--> GPIO read OutOfRange ");
		return -1;
	}


	int res = read( PMBUS_MFR_GPIO_CONFIG, pubRxBuff, 1 );
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readRegGPIO--> GPIO read config failed ");
		return -1;
	}
	else
	{
		//bit 7-4	Reserved
		//bit 3		Status
		//bit 2		Out_Value
		//bit 1		Out_Enable
		//bit 0		Enable
		ubGpioReg = pubRxBuff[0];
	}

	return 0;
}

int UCD9090a::readMonitorConfig( void )
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read( PMBUS_MFR_MONITOR_CONFIG, pubRxBuff, (UCD9090A_MAX_NUM_MON_PINS+1) );
	if ( res != 0 )
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readMonitorConfig--> Failed ");
		return -1;
	}
	else
	{
		clearRailConfig();

		for (int i = 0; i<UCD9090A_MAX_NUM_MON_PINS; i++)
		{
			stPinMon[i].ubMonitorType	= pubRxBuff[i+1] >> 5;
			stPinMon[i].ubPageByte		= pubRxBuff[i+1] & 0x1F;

			//Save Rail Configurations
			uint8_t ubNumCurrRail = stPinMon[i].ubPageByte;
			uint8_t ubNumCurrMonPin = stRail[ubNumCurrRail].ubNumPinMon;

			stRail[ubNumCurrRail].stMonPin[ubNumCurrMonPin].ubMonitorType = pubRxBuff[i+1] >> 5;
			stRail[ubNumCurrRail].stMonPin[ubNumCurrMonPin].ubPageByte = pubRxBuff[i+1] & 0x1F;

			stRail[ubNumCurrRail].ubNumPinMon++;
		}
	}

	return 0;
}

int UCD9090a::readSeqConfig( eUCD9090aRail eRail )
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if ( eRail >= UCD9090A_NUM_RAILS_MAX)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readSeqConfig-->Num rail out of limit ");
		return -1;
	}

	//select the rail to read
	int res = writeSetPage( eRail );
	if ( res != 0 )
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readSeqConfig--> Failed to set the rail ");
		return -1;
	}


	res = read( PMBUS_MFR_SEQ_CONFIG, pubRxBuff, UCD9090A_SEQ_CONFIG_CMD_NUMBYTE+1 );
	if ( res != 0 )
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readSeqConfig--> Failed ");
		return -1;
	}
	else
	{
		//stRail Configuration, PinEnable in particular
		//bit 7-3	Pin ID
		//bit 2		Polarity (0 Active Low, 1 Active High)
		//bit 1,0	Mode	 (0=Unused, 1=Input, 2=Actively Driven Output, 3=Open-Drain Output)
		stRail[eRail].stEnalePin.ubMode		= pubRxBuff[1] & 0x03;
		stRail[eRail].stEnalePin.ubPolarity = (pubRxBuff[1] & 0x04) >> 2;
		stRail[eRail].stEnalePin.ubPinIDDef = (pubRxBuff[1] & 0xF8) >> 3;

		//To keep in mind the type of the pin read
		if (stRail[eRail].stEnalePin.ubMode == 2)
		{
			stGPIO[stRail[eRail].stEnalePin.ubPinIDDef].ubEnablePin = GPIO_CONF_ENABLE_PIN_OK;
			stGPIO[stRail[eRail].stEnalePin.ubPinIDDef].ubEnableRail = eRail+1;
		}
		else
		{
			stGPIO[stRail[eRail].stEnalePin.ubPinIDDef].ubEnablePin = 0;
			stGPIO[stRail[eRail].stEnalePin.ubPinIDDef].ubEnableRail = 0;
		}
	}

	//TODO Read other configuration of sequence

	return 0;
}

int UCD9090a::readUserRAM(uint8_t &ubVal)
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read( PMBUS_MFR_USER_RAM, pubRxBuff, 1 );
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readUserRAM-->ERR");
		return -1;
	}
	else
	{
		ubVal = pubRxBuff[0];
	}

	return 0;
}

int UCD9090a::readVoutCommand( uint8_t ubPage, float &fVoutCmd )
{
	uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = writeSetPage( ubPage );
	if ( res != 0 )
	{
		return -1;
	}


	int8_t ubExp;
	res = readVoutMode( ubExp );
	if (res == 0)
	{
        res = read( PMBUS_VOUT_COMMAND, pubRxBuff, 2);
		if ( res == 0)
		{
			uint16_t usVout;
			usVout = pubRxBuff[0] << 8;
			usVout |= pubRxBuff[1];

            int16_t ssDiv = exp2(ubExp);
            fVoutCmd = (float)usVout / ssDiv;

            m_pLogger->log(LOG_DEBUG, "UCD9090a::readVoutCommand--> usVout %d ebExp %d ssDiv %d", usVout, ubExp, ssDiv);
        }
		else
		{
			m_pLogger->log(LOG_ERR, "UCD9090a::readVoutCommand--> Impossible to readVoutCommand");
			return -1;
		}
	}
	else
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::readVoutCommand--> Impossible to readVoutCommand");
		return -1;
	}

	return 0;
}

int UCD9090a::readStatusRegister(uint32_t &ubVal)
{
    uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
    memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read( PMBUS_MFR_STATUS, pubRxBuff, 5 );
    if (res != 0)
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::readUserRAM-->ERR");
        return -1;
    }
    else
    {
        ubVal = 0;
        ubVal = pubRxBuff[1] << 24 | pubRxBuff[2] << 16 | pubRxBuff[3] << 8 | pubRxBuff[4];
    }

    return 0;
}

int UCD9090a::readOnOffConfig(uint8_t &ubReg)
{
    uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
    memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PMBUS_ON_OFF_CONFIG, pubRxBuff, 1);
    if (res != 0)
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::readOnOffConfig--> Failed");
    }
    else
    {
        ubReg = pubRxBuff[0];
    }

    return 0;
}

int UCD9090a::readOperation(uint8_t &ubReg)
{
    uint8_t pubRxBuff[UCD9090A_MAX_MSG_SIZE_BUFF];
    memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PMBUS_OPERATION, pubRxBuff, 1);
    if (res != 0)
    {
        m_pLogger->log(LOG_ERR, "UCD9090a::readOperation--> Failed");
    }
    else
    {
        ubReg = pubRxBuff[0];
    }

    return 0;
}

/*!*********************************************************************************************
 * Private methods
 * **********************************************************************************************/

float UCD9090a::conversionLinear112Float(uint16_t usReadTemp )
{
	linear11_t linearTemp;
	float		fTemp = 0;

	linearTemp.base = usReadTemp & 0x7FF;
	linearTemp.mantissa = usReadTemp >> 11;

	//CA2
	int16_t ssMant = ~linearTemp.mantissa + 1;
	int16_t ssDiv = exp2(ssMant);

	if (ssDiv > 0)
	{
		fTemp = ((double)linearTemp.base) / ssDiv;
	}
	else
	{
		m_pLogger->log(LOG_ERR, "UCD9090a::conversionLinear112Float-->Unable to convert Linear11 value to float value");
	}

	return fTemp;
}


int UCD9090a::clearRailConfig( void )
{
	memset(stPinMon, 0, sizeof(stPinMon));
	memset(stRail, 0, sizeof(stRail));
	for (int i=0; i<UCD9090A_NUM_RAILS_MAX; i++)
	{
		stRail[i].unNumRail = i+1;
	}

	return 0;
}
