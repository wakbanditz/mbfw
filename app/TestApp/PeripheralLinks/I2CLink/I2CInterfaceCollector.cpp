/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    I2CInterfaceCollector.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the I2CInterfaceCollector class.
 @details

 ****************************************************************************
*/

#include "I2CInterfaceCollector.h"
#include "I2CInterface.h"
#include "Log.h"

I2CInterfaceCollector::I2CInterfaceCollector()
{

}

I2CInterfaceCollector::~I2CInterfaceCollector()
{
	SAFE_DELETE(m_pI2C2);
	SAFE_DELETE(m_pI2C3);
}

int I2CInterfaceCollector::createInterfaces( void )
{
	m_pI2C2 = new I2CInterface();
	m_pI2C3 = new I2CInterface();

    m_pI2C2->setLogger( getLogger() );
	bool bRes = m_pI2C2->init("/dev/i2c-1", false, false);
	if ( bRes == false )
	{
		log(LOG_ERR, "I2CInterfaceCollector::createInterfaces: unable to init /dev/i2c-1");
		return -1;
	}


    m_pI2C3->setLogger( getLogger() );
	bRes = m_pI2C3->init("/dev/i2c-2", false, false);
	if ( bRes == false )
	{
		log(LOG_ERR, "I2CInterfaceCollector::createInterfaces: unable to init /dev/i2c-2");
		return -1;
	}


	return 0;
}

I2CInterface * I2CInterfaceCollector::getInterfaceByIndex(enumI2CInterface ubNumI2CChannel )
{
	I2CInterface * i2cIfr = NULL;
	switch( ubNumI2CChannel )
	{
		case eI2C_2:
			i2cIfr = m_pI2C2;
		break;

		case eI2C_3:
			i2cIfr = m_pI2C3;
		break;

		case eI2C_0:
		case eI2C_1:
		case eI2C_4:
		default:
			log(LOG_ERR, "I2CChannel::getChannel-->no channel available");
			return NULL;
		break;
	}

	return i2cIfr;
}

void I2CInterfaceCollector::destroyInterfaces( void )
{
	SAFE_DELETE(m_pI2C2);
	SAFE_DELETE(m_pI2C3);
}
