/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PCA9955b.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the PCA9952 class.
 @details

 ****************************************************************************
*/

#include "PCA9955B.h"
#include "math.h"
/*!***************************************************************************************************************
 *
 * Methods
 *
 *****************************************************************************************************************/
PCA9955b::PCA9955b()
{
	//Set configuration of LEDOUT Group
	setConfLedGrp();
}

PCA9955b::~PCA9955b()
{
	/* Nothing to do yet */
}

int PCA9955b::resetSoft()
{

    int ret = writeCustomAdd(PCALED_SWRST_ADD, PCALED_SWRST_REG);
	if ( ret != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::resetSoft-->Unable to reset device");
		return -1;
	}
	return 0;
}

int PCA9955b::config(uint8_t ubDmBlinkState)
{
	int res = writeMode1(0);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::config-->Unable to set MODE2 register");
		return -1;
	}

    if (ubDmBlinkState == PCALED_MODE2_BLNK_CTL)
	{
        res = writeMode2(0x21);
        m_pLogger->log( LOG_DEBUG, "PCA9955b::config-->Blink mode on");
	}
	else
	{
        res = writeMode2(0x01);
        m_pLogger->log( LOG_DEBUG, "PCA9955b::config-->Dim mode on");
	}

	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::config-->Unable to set MODE2 register");
		return -1;
	}

	return 0;
}

int PCA9955b::clearError()
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
    memset(pubTxBuff, 0, sizeof(pubTxBuff));

    uint8_t ubMode1RegTmp = 0;

    int res = readMode2(ubMode1RegTmp);
    if ( res != 0 )
    {
        m_pLogger->log( LOG_ERR, "PCA9955b::writeClearError-->Unable to write");
        return -1;
    }
    else
    {
        ubMode1RegTmp = ubMode1RegTmp |  PCALED_BIT_CLRERR;
        res = writeMode2(ubMode1RegTmp);
        if ( res != 0 )
        {
            m_pLogger->log( LOG_ERR, "PCA9955b::writeClearError-->Unable to write");
            return -1;
        }
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::writeClearError-->Write register");
    }

    return 0;
}

/*!****************************************************************************************
 *
 * WRITE operations
 *
 ******************************************************************************************/
int PCA9955b::writeMode1(uint8_t ubMode)
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubMode;

    int res = write(PCALED_MODE1, pubTxBuff, 1);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeMode1-->Unable to write on Mode1 register");
		return -1;
	}
	else
	{
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::writeMode1-->Write on Mode1 register");
	}
	return 0;
}

int PCA9955b::writeMode2(uint8_t ubMode)
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubMode;

    int res = write(PCALED_MODE2, pubTxBuff, 1);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeMode2-->Unable to write on Mode2 register");
		return -1;
	}
	else
	{
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::writeMode2-->Write on Mode2 register");
	}
	return 0;
}

int PCA9955b::clearErrorMode2()
{
    uint8_t ubModeReg;

    if(readMode2(ubModeReg) == -1)
    {
        m_pLogger->log(LOG_ERR, "PCA9955b::clearErrorMode2-->Unable to read Mode2 register");
        return -1;
    }

    // set CLRERR bit = 1
    ubModeReg |= 0x10;

    if(writeMode2(ubModeReg) == -1)
    {
        m_pLogger->log(LOG_ERR, "PCA9955b::clearErrorMode2-->Unable to write Mode2 register");
        return -1;
    }
    return 0;
}

int PCA9955b::writeLedOut(uint8_t ubNumLed, uint8_t ubState)
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

    if (ubNumLed >= PCALED_MAX_NUM_LED)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeLedOut-->Num LED out of range");
		return -1;
	}

/* !!!! NON FUNGE -> sempre errore
    // at each setting ON of the led let's check the error flag
    if(readErrorBit(ubNumLed) == true)
    {
        m_pLogger->log( LOG_ERR, "PCA9955b::writeLedOut-->Error flag ");
        return -1;
    } */

    uint8_t pubRxBuff;
	int res = read(m_tableLedGrp[ubNumLed].ubAddLedGrp, &pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeLedOut-->Unable to read register");
		return -1;
	}

	uint8_t ubTmp = 0;
	ubTmp = pubRxBuff & (~m_tableLedGrp[ubNumLed].ubMaskLedGrp);
	pubTxBuff[0] = ubTmp | (ubState << m_tableLedGrp[ubNumLed].ubPosLedGrp);

	res = write(m_tableLedGrp[ubNumLed].ubAddLedGrp, pubTxBuff, 1);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeLedOut-->Unable to write ");
		return -1;
	}
	else
	{
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::writeLedOut-->Write ");
	}

	return 0;
}

int PCA9955b::clearLedOut()
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
    memset(pubTxBuff, 0, sizeof(pubTxBuff));


    int res = write(PCALED_LEDOUT0, pubTxBuff, 1);
    if ( res != 0 )
    {
        m_pLogger->log( LOG_ERR, "PCA9955b::clearLedOut-->Unable to reset LED0 ");
        return -1;
    }
    else
    {
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::clearLedOut-->Reset LED0 ");
    }

    res = write(PCALED_LEDOUT1, pubTxBuff, 1);
    if ( res != 0 )
    {
        m_pLogger->log( LOG_ERR, "PCA9955b::clearLedOut-->Unable to reset LED1 ");
        return -1;
    }
    else
    {
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::clearLedOut-->Reset LED1 ");
    }

    res = write(PCALED_LEDOUT2, pubTxBuff, 1);
    if ( res != 0 )
    {
        m_pLogger->log( LOG_ERR, "PCA9955b::clearLedOut-->Unable to reset LED2 ");
        return -1;
    }
    else
    {
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::clearLedOut-->Reset LED2 ");
    }

    res = write(PCALED_LEDOUT3, pubTxBuff, 1);
    if ( res != 0 )
    {
        m_pLogger->log( LOG_ERR, "PCA9955b::clearLedOut-->Unable to reset LED3 ");
        return -1;
    }
    else
    {
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::clearLedOut-->Reset LED3 ");
    }

    return 0;
}

int PCA9955b::writeGrpPWM(uint8_t ubDutyCycle)
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubDutyCycle > 100)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeGrpPWM-->Value of duty cycle out of range");
		return -1;
	}

    uint8_t ubNumDig = ((uint16_t)ubDutyCycle * 255) / 100;
	pubTxBuff[0] = ubNumDig;

    int res = write(PCALED_GRPPWM, pubTxBuff, 1);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeGrpPWM-->Unable to write");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9952::writeGrpPWM-->Write register");
	}
	return 0;
}

int PCA9955b::writeGrpFreq(uint8_t ubFreq)
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubFreq > 100)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeGrpFreq-->Param out of range");
		return -1;
	}

    uint8_t ubNumDig = ((uint16_t)ubFreq * 255) / 100;

	pubTxBuff[0] = ubNumDig;

    int res = write(PCALED_GRPFREQ, pubTxBuff, 1);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeGrpFreq-->Unable to write");
		return -1;
	}
	else
	{
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::writeGrpFreq-->Write register");
	}
	return 0;
}

int PCA9955b::writePWM(uint8_t ubNumLed, uint8_t ubBrightness)
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

    uint8_t ubNumPWMAdd = ubNumLed + PCALED_PWM0;

	if (ubBrightness > 100)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writePWM-->% Param out of range");
		return -1;
	}

    uint8_t ubNumDig = ((uint16_t)ubBrightness * 255) / 100;

	pubTxBuff[0] = ubNumDig;

	int res = write(ubNumPWMAdd, pubTxBuff, 1);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writePWM-->Unable to write");
		return -1;
	}
	else
	{
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::writePWM-->Write register");
	}
	return 0;
}

int PCA9955b::writeIref(uint8_t ubNumLed, uint8_t ubCurrent)
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

    uint8_t ubNumIrefAdd = ubNumLed + PCALED_IREF0;

	if (ubCurrent > 100)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeIref-->% Param out of range");
		return -1;
	}

    uint8_t ubNumDig = ((uint16_t)ubCurrent * 255) / 100;

	pubTxBuff[0] = ubNumDig;

	int res = write(ubNumIrefAdd, pubTxBuff, 1);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeIref-->Unable to write");
		return -1;
	}
	else
	{
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::writeIref-->Write register");
	}
	return 0;
}

int PCA9955b::writeOffset(uint8_t ubOffset)
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubOffset;

    int res = write(PCALED_OFFSET, pubTxBuff, 1);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeOffset-->Unable to write");
		return -1;
	}
	else
	{
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::writeOffset-->Write register");
	}
	return 0;
}

int PCA9955b::writePWMAll(uint8_t ubBrightness)
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubBrightness > 100)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writePWMAll-->% Param out of range");
		return -1;
	}

    uint8_t ubNumDig = ((uint16_t)ubBrightness * 255) / 100;

	pubTxBuff[0] = ubNumDig;

    int res = write(PCALED_PWMALL, pubTxBuff, 1);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writePWMAll-->Unable to write");
		return -1;
	}
	else
	{
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::writePWMAll-->Write register");
	}
	return 0;
}

int PCA9955b::writeIrefAll(uint8_t ubCurrent)
{
    uint8_t pubTxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubCurrent > 100)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeIrefAll-->% Param out of range");
		return -1;
	}

    uint8_t ubNumDig = ((uint16_t)ubCurrent * 255) / 100;

	pubTxBuff[0] = ubNumDig;

    int res = write(PCALED_IREFALL, pubTxBuff, 1);
	if ( res != 0 )
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::writeIrefAll-->Unable to write");
		return -1;
	}
	else
	{
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9955b::writeIrefAll-->Write register");
	}
	return 0;
}

/*!****************************************************************************************
 *
 * READ operations
 *
 ******************************************************************************************/
int PCA9955b::readMode1(uint8_t &ubModeReg)
{
    uint8_t pubRxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PCALED_MODE1, pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::readMode1-->Unable to read on Mode1 register");
		return -1;
	}
	else
	{
		ubModeReg = pubRxBuff[0];
	}
	return 0;
}

int PCA9955b::readMode2(uint8_t &ubModeReg)
{
    uint8_t pubRxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PCALED_MODE2, pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::readMode2-->Unable to read on Mode1 register");
		return -1;
	}
	else
	{
		ubModeReg = pubRxBuff[0];
	}
	return 0;
}

bool PCA9955b::readErrorBit(uint8_t ubNumLed)
{
    uint8_t ubModeReg;

    readMode2(ubModeReg);

    // the error bit is read from Mode2 register bit 6
    if(ubModeReg & 0x40)
    {
        // an error has been detected: let's check the specific register of the led
        uint8_t pubRxBuff;
        int res = read(m_tableLedGrp[ubNumLed].ubErrorLedGrp, &pubRxBuff, 1);
        if (res != 0)
        {
            m_pLogger->log(LOG_ERR, "PCA9955b::readErrorBit-->Unable to read register");
            return true;
        }

        if(pubRxBuff & m_tableLedGrp[ubNumLed].ubMaskLedGrp)
        {
            m_pLogger->log(LOG_ERR, "PCA9955b::readErrorBit led %d error %d", ubNumLed, pubRxBuff);
            return true;
        }
    }

    return false;
}

int PCA9955b::readLedOut(uint8_t ubNumLed, uint8_t &ubState)
{
    uint8_t pubRxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    if (ubNumLed >= PCALED_MAX_NUM_LED)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::readLedOut-->Num LED out of range");
		return -1;
	}

	int res = read(m_tableLedGrp[ubNumLed].ubAddLedGrp, pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::readLedOut-->Unable to read register");
		return -1;
	}
	else
	{
		ubState = (pubRxBuff[0] & m_tableLedGrp[ubNumLed].ubMaskLedGrp) >> m_tableLedGrp[ubNumLed].ubPosLedGrp;
	}
	return 0;
}

int PCA9955b::readPWM(uint8_t ubNumLed, uint8_t &ubBrightness)
{
    uint8_t pubRxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    if (ubNumLed >= PCALED_MAX_NUM_LED)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::readPWM-->Num Led out of range");
		return -1;
	}

    uint8_t	ubNumPWMAdd = ubNumLed + PCALED_PWM0;
	int res = read(ubNumPWMAdd, pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::readPWM-->Unable to read PWM 0x%x register", ubNumPWMAdd);
		return -1;
	}
	else
	{
		float fAppo = pubRxBuff[0];
        uint8_t ubNumPerc =  round(((fAppo * 100) / 255));

		ubBrightness = ubNumPerc;
	}
	return 0;
}

int PCA9955b::readPWMAll(uint8_t &ubBrightness)
{
    uint8_t pubRxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PCALED_PWMALL, pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::readPWMAll-->Unable to read register");
		return -1;
	}
	else
	{

		float fAppo = pubRxBuff[0];
        uint8_t ubNumPerc =  round(((fAppo * 100) / 255));

		ubBrightness = ubNumPerc;
	}
	return 0;
}

int PCA9955b::readIref(uint8_t ubNumLed, uint8_t &ubIref)
{
    uint8_t pubRxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    if (ubNumLed >= PCALED_MAX_NUM_LED)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::readPWM-->Num Led out of range");
		return -1;
	}

    uint8_t	ubNumIrefAdd = ubNumLed + PCALED_IREF0;
	int res = read(ubNumIrefAdd, pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::readIref-->Unable to read IREF 0x%x register", ubNumIrefAdd);
		return -1;
	}
	else
	{
		float fAppo = pubRxBuff[0];
        uint8_t ubNumPerc =  round(((fAppo * 100) / 255));

		ubIref = ubNumPerc;
	}
	return 0;
}

int PCA9955b::readIrefAll(uint8_t &ubIref)
{
    uint8_t pubRxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PCALED_IREFALL, pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log( LOG_ERR, "PCA9955b::readIrefAll-->Unable to read register" );
		return -1;
	}
	else
	{
		float fAppo = pubRxBuff[0];
        uint8_t ubNumPerc =  round(((fAppo * 100) / 255));

		ubIref = ubNumPerc;
	}
	return 0;
}

int PCA9955b::readOffset(uint8_t &ubOffset)
{
    uint8_t pubRxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PCALED_OFFSET, pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log(LOG_ERR, "PCA9955b::readOffset-->Unable to read register");
		return -1;
	}
	else
	{
		ubOffset = pubRxBuff[0];
	}
	return 0;
}

int PCA9955b::readGrpFreq(uint8_t &ubGrpFreq)
{
    uint8_t pubRxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PCALED_GRPFREQ, pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log(LOG_ERR, "PCA9955b::readGrpFreq-->Unable to read register");
		return -1;
	}
	else
	{
		float fAppo = pubRxBuff[0];
        uint8_t ubNumPerc =  round(((fAppo * 100) / 255));

		ubGrpFreq = ubNumPerc;
	}
	return 0;
}

int PCA9955b::readGrpPWM(uint8_t &ubGrpPwm)
{
    uint8_t pubRxBuff[PCALED_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PCALED_GRPPWM, pubRxBuff, 1);
	if (res != 0)
	{
        m_pLogger->log(LOG_ERR, "PCA9955b::readGrpPWM-->Unable to read register");
		return -1;
	}
	else
	{
		float fAppo = pubRxBuff[0];
        uint8_t ubNumPerc =  round(((fAppo * 100) / 255));

		ubGrpPwm = ubNumPerc;
	}
	return 0;
}

void PCA9955b::setConfLedGrp(void)
{
    m_tableLedGrp[0].ubAddLedGrp	= PCALED_LEDOUT0;
    m_tableLedGrp[0].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_A;
    m_tableLedGrp[0].ubPosLedGrp	= PCALED_LEDOUT_POS_A;

    m_tableLedGrp[1].ubAddLedGrp	= PCALED_LEDOUT0;
    m_tableLedGrp[1].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_B;
    m_tableLedGrp[1].ubPosLedGrp	= PCALED_LEDOUT_POS_B;

    m_tableLedGrp[2].ubAddLedGrp	= PCALED_LEDOUT0;
    m_tableLedGrp[2].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_C;
    m_tableLedGrp[2].ubPosLedGrp	= PCALED_LEDOUT_POS_C;

    m_tableLedGrp[3].ubAddLedGrp	= PCALED_LEDOUT0;
    m_tableLedGrp[3].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_D;
    m_tableLedGrp[3].ubPosLedGrp	= PCALED_LEDOUT_POS_D;

    m_tableLedGrp[4].ubAddLedGrp	= PCALED_LEDOUT1;
    m_tableLedGrp[4].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_A;
    m_tableLedGrp[4].ubPosLedGrp	= PCALED_LEDOUT_POS_A;

    m_tableLedGrp[5].ubAddLedGrp	= PCALED_LEDOUT1;
    m_tableLedGrp[5].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_B;
    m_tableLedGrp[5].ubPosLedGrp	= PCALED_LEDOUT_POS_B;

    m_tableLedGrp[6].ubAddLedGrp	= PCALED_LEDOUT1;
    m_tableLedGrp[6].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_C;
    m_tableLedGrp[6].ubPosLedGrp	= PCALED_LEDOUT_POS_C;

    m_tableLedGrp[7].ubAddLedGrp	= PCALED_LEDOUT1;
    m_tableLedGrp[7].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_D;
    m_tableLedGrp[7].ubPosLedGrp	= PCALED_LEDOUT_POS_D;

    m_tableLedGrp[8].ubAddLedGrp	= PCALED_LEDOUT2;
    m_tableLedGrp[8].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_A;
    m_tableLedGrp[8].ubPosLedGrp	= PCALED_LEDOUT_POS_A;

    m_tableLedGrp[9].ubAddLedGrp	= PCALED_LEDOUT2;
    m_tableLedGrp[9].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_B;
    m_tableLedGrp[9].ubPosLedGrp	= PCALED_LEDOUT_POS_B;

    m_tableLedGrp[10].ubAddLedGrp	= PCALED_LEDOUT2;
    m_tableLedGrp[10].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_C;
    m_tableLedGrp[10].ubPosLedGrp	= PCALED_LEDOUT_POS_C;

    m_tableLedGrp[11].ubAddLedGrp	= PCALED_LEDOUT2;
    m_tableLedGrp[11].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_D;
    m_tableLedGrp[11].ubPosLedGrp	= PCALED_LEDOUT_POS_D;

    m_tableLedGrp[12].ubAddLedGrp	= PCALED_LEDOUT3;
    m_tableLedGrp[12].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_A;
    m_tableLedGrp[12].ubPosLedGrp	= PCALED_LEDOUT_POS_A;

    m_tableLedGrp[13].ubAddLedGrp	= PCALED_LEDOUT3;
    m_tableLedGrp[13].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_B;
    m_tableLedGrp[13].ubPosLedGrp	= PCALED_LEDOUT_POS_B;

    m_tableLedGrp[14].ubAddLedGrp	= PCALED_LEDOUT3;
    m_tableLedGrp[14].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_C;
    m_tableLedGrp[14].ubPosLedGrp	= PCALED_LEDOUT_POS_C;

    m_tableLedGrp[15].ubAddLedGrp	= PCALED_LEDOUT3;
    m_tableLedGrp[15].ubMaskLedGrp	= PCALED_LEDOUT_MASK_LED_D;
    m_tableLedGrp[15].ubPosLedGrp	= PCALED_LEDOUT_POS_D;
}
