/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PCA9537.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the PCA9537 class.
 @details

 ****************************************************************************
*/

#include "PCA9537.h"

PCA9537::PCA9537()
{

}

PCA9537::~PCA9537()
{
	/* Nothing to do yet */
}

/*!****************************************************************************************
 *
 * WRITE operations
 *
 ******************************************************************************************/
int PCA9537::writeConfigurationRegister(uint8_t ubConf)
{
	uint8_t pubTxBuff[PCA9537_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubConf;

	int res = write(PCA9537_CONF_REG, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9537::writeConfigurationRegister-->Unable to write on CONFIG register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9537::writeConfigurationRegister-->Write on CONFIG register");
	}
	return 0;
}

int PCA9537::writeOutputPortRegister(uint8_t ubConf)
{
	uint8_t pubTxBuff[PCA9537_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubConf;

	int res = write(PCA9537_OUTPUT_REG, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9537::writeOutputPortRegister-->Unable to write on OUTPUT register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9537::writeOutputPortRegister-->Write on OUTPUT register");
	}
	return 0;
}

int PCA9537::writePolarityInversionRegister(uint8_t ubConf)
{
	uint8_t pubTxBuff[PCA9537_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubConf;

	int res = write(PCA9537_POL_INV_REG, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9537::writePolarityInversionRegister-->Unable to write on POLARITY register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9537::writePolarityInversionRegister-->Write on POLARITY register");
	}
	return 0;
}

/*!****************************************************************************************
 *
 * READ operations
 *
 ******************************************************************************************/
int PCA9537::readConfigurationRegister(uint8_t &ubConf)
{
	uint8_t pubRxBuff[PCA9537_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(PCA9537_CONF_REG, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log( LOG_ERR, "PCA9537::readConfigurationRegister-->Unable to read Configuration register");
		return -1;
	}
	else
	{
		ubConf = pubRxBuff[0];
	}

	return 0;
}

int PCA9537::readInputPortRegister(uint8_t &ubConf)
{
	uint8_t pubRxBuff[PCA9537_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(PCA9537_INPUT_REG, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log( LOG_ERR, "PCA9537::readInputPortRegister-->Unable to read Input register");
		return -1;
	}
	else
	{
		ubConf = pubRxBuff[0];
	}

	return 0;
}

int PCA9537::readPolarityInversionRegister(uint8_t &ubConf)
{
	uint8_t pubRxBuff[PCA9537_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(PCA9537_POL_INV_REG, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log( LOG_ERR, "PCA9537::readPolarityInversionRegister-->Unable to read Polarity Inversion register");
		return -1;
	}
	else
	{
		ubConf = pubRxBuff[0];
	}

	return 0;
}

int PCA9537::readOutputPortRegister(uint8_t &ubConf)
{
	uint8_t pubRxBuff[PCA9537_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(PCA9537_OUTPUT_REG, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log( LOG_ERR, "PCA9537::readOutputPortRegister-->Unable to read Output register");
		return -1;
	}
	else
	{
		ubConf = pubRxBuff[0];
	}

	return 0;
}

