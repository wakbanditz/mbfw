/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    AT24C256C.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the AT24C256C class.
 @details

 ****************************************************************************
*/

#include "AT24C256C.h"

#define AT24C256C_BIT_PER_WORD		8
#define AT24C256C_MEMORY_SIZE_BYTES	32768
#define AT24C256C_BYTE_PER_PAGE		64
#define AT24C256C_PAGES_NUM			512



AT24C256C::AT24C256C()
{

}

AT24C256C::~AT24C256C()
{

}

int8_t AT24C256C::writeWord(uint16_t usStartAddress, uint8_t ucWord)
{
	if ( usStartAddress + 1 >= AT24C256C_MEMORY_SIZE_BYTES )		return -1;

	int32_t slDeviceAddress = -1;
	if ( getDeviceAddress(slDeviceAddress) )	return -1;

	bool bRes = m_pI2CIfr->multiWrite(slDeviceAddress, &ucWord, 1, usStartAddress);
	if ( bRes == false )
	{
		m_pLogger->log( LOG_ERR, "AT24C256C::write: error on write operation");
		return -1;
	}
	return 0;
}

int8_t AT24C256C::write(uint16_t usStartAddress, uint8_t* pBuff, uint16_t usMsgLen)
{
	if ( pBuff == 0 )			return -1;
	if ( usMsgLen == 0)			return -1;
	if ( usStartAddress + usMsgLen > AT24C256C_MEMORY_SIZE_BYTES )		return -1;

	int32_t slDeviceAddress = -1;
	if ( getDeviceAddress(slDeviceAddress) )	return -1;

    int liPageSpace = AT24C256C_BYTE_PER_PAGE - ( usStartAddress % AT24C256C_BYTE_PER_PAGE );
	int liBytesSent = 0;
	int liBytesLeft = usMsgLen;
	bool bRes = false;
	uint8_t ucState = ( usMsgLen < liPageSpace ) ? eWriteBuff : eWriteUntilEndPage;

	while ( liBytesSent != usMsgLen )
	{
		switch(ucState)
		{
			case eWriteBuff:
                // Check if eeprom is ready
                bRes = waitUntilReady(slDeviceAddress, usStartAddress);
                if ( ! bRes )
                {
                    m_pLogger->log( LOG_ERR, "AT24C256C::write: eeprom not ready");
                    return -1;
                }

				liBytesLeft = usMsgLen - liBytesSent;
				bRes = m_pI2CIfr->multiWrite(slDeviceAddress, pBuff, liBytesLeft, usStartAddress);
				if ( bRes == false )
				{
					m_pLogger->log( LOG_ERR, "AT24C256C::write: error on write operation");
					return -1;
				}
				liBytesSent += liBytesLeft;
                msleep(5);
			break;

			case eWriteUntilEndPage:
                // Check if eeprom is ready
                bRes = waitUntilReady(slDeviceAddress, usStartAddress);
                if ( ! bRes )
                {
                    m_pLogger->log( LOG_ERR, "AT24C256C::write: eeprom not ready");
                    return -1;
                }

				bRes = m_pI2CIfr->multiWrite(slDeviceAddress, pBuff, liPageSpace, usStartAddress);
				if ( bRes == false )
				{
                    m_pLogger->log( LOG_ERR, "AT24C256C::write: error on write until end of page operation");
					return -1;
				}
				liBytesSent = liPageSpace;
				usStartAddress += liPageSpace;
				pBuff += liPageSpace;
                ucState = ( usMsgLen - liBytesSent >= AT24C256C_BYTE_PER_PAGE ) ? eWriteAllThePage : eWriteBuff;
			break;

			case eWriteAllThePage:
                // Check if eeprom is ready
                bRes = waitUntilReady(slDeviceAddress, usStartAddress);
                if ( ! bRes )
                {
                    m_pLogger->log( LOG_ERR, "AT24C256C::write: eeprom not ready");
                    return -1;
                }

				bRes = m_pI2CIfr->multiWrite(slDeviceAddress, pBuff, AT24C256C_BYTE_PER_PAGE, usStartAddress);
				if ( bRes == false )
				{
                    m_pLogger->log( LOG_ERR, "AT24C256C::write: error on write all page operation, startAddress %d", usStartAddress);
					return -1;
				}
				liBytesSent += AT24C256C_BYTE_PER_PAGE;
				usStartAddress += AT24C256C_BYTE_PER_PAGE;
				pBuff += AT24C256C_BYTE_PER_PAGE;
                ucState = ( usMsgLen - liBytesSent >= AT24C256C_BYTE_PER_PAGE ) ? eWriteAllThePage : eWriteBuff;
			break;
		}
	}
	return 0;
}

int8_t AT24C256C::readWord(uint16_t usStartAddress, uint8_t& ucWordRead)
{
	if ( usStartAddress + 1 >= AT24C256C_MEMORY_SIZE_BYTES )		return -1;

	int32_t slDeviceAddress = -1;
	if ( getDeviceAddress(slDeviceAddress) )	return -1;

	bool bRet = m_pI2CIfr->multiRead(slDeviceAddress, &ucWordRead, 1, usStartAddress);
	if ( bRet == false )
	{
		m_pLogger->log( LOG_ERR, "EEpromLink::read-->Error on read operation");
		return -1;
	}

	return 0;
}


int8_t AT24C256C::read(uint16_t usStartAddress, uint8_t *pBuff, uint16_t usMsgLen)
{
	if ( pBuff == 0 )		return -1;
	if ( usMsgLen == 0)		return -1;
	if ( usStartAddress + usMsgLen >= AT24C256C_MEMORY_SIZE_BYTES )		return -1;

	int32_t slDeviceAddress = -1;
	if ( getDeviceAddress(slDeviceAddress) )	return -1;

	bool bRet = m_pI2CIfr->multiRead(slDeviceAddress, pBuff, usMsgLen, usStartAddress);
	if ( bRet == false )
	{
		m_pLogger->log( LOG_ERR, "EEpromLink::read-->Error on read operation");
		return -1;
	}

	return 0;
}

int8_t AT24C256C::eraseEepromArea(uint16_t usStartAddress, uint16_t usAreaLen)
{
	return write(usStartAddress, (uint8_t*)0xFF, usAreaLen);
}

int8_t AT24C256C::resetEeprom()
{
    return write(0x00, (uint8_t*)0xFF, AT24C256C_MEMORY_SIZE_BYTES);
}

bool AT24C256C::waitUntilReady(int32_t slDeviceAddress, uint16_t usStartAddress)
{
    uint16_t usSleep = 0;
    bool bRes = false;
    uint8_t ucWord = 0;

    // Perform a better check on time.. timer TODO
    // max writing time 5ms
    while ( usSleep < 5000 && ! bRes )
    {
        m_pLogger->log(LOG_DEBUG_PARANOIC, "AT24C256C::pollingAck: waiting for ack");
        usleep(200);
        usSleep += 200;
        bRes = m_pI2CIfr->multiWriteGeneral(slDeviceAddress,  &ucWord, 0, usStartAddress, sizeof(usStartAddress), false);
    }

    m_pLogger->log(LOG_DEBUG_PARANOIC, "AT24C256C::pollingAck: ack received");

    return (usSleep < 5000 || bRes );
}
