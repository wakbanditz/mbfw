/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    UCD9090a.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the UCD9090a class.
 @details

 ****************************************************************************
*/

#ifndef UCD9090A_H
#define UCD9090A_H

#include "I2CBoardBase.h"
#include "PmBusCmd.h"

/*!
 * @brief The UCD9090a class
 * The number of configurable rails is a maximum of 10. The number of configurable GPIs is
 * a maximum of 8. The number of configurable Boolean Logic GPOs is a maximum of 10.
 * The number of monitor pins is 11.
 * The class consits of read, write, set and get functions. Read and write functions are
 * related to the data exchange directly with the i2c device commands, instead get and set functions
 * are used to allow a specific behavoiur that can be composed by two or more write and read functions.
 *
 */
class UCD9090a : public I2CBoardBase
{
	public:

		UCD9090a();
		virtual ~UCD9090a();

		/*!
		 * @brief printGpioConfig Print the status of GPIOs configuration
		 */
		void printGpioConfig( void );

		/*!
		 * @brief printRailConfig
		 */
		void printRailConfig( void );

		/*!
		 * @brief printPinMonConfig
		 */
		void printPinMonConfig( void );

		/*!*******************************************************************************************************
		 *
		 * SET operations
		 *
		 *********************************************************************************************************/
		/*!
		 * @brief setGPIO	set NumGpio with the configuration present in ubDir and ubValue param.
		 *					The function in turn calls SelectGpio and ConfigGpio functions.
		 * @param ubNumGpio
		 * @param ubDir
		 * @param ubValue
		 * @return 0 if success | -1 otherwise
		 */
		int setGPIO( uint8_t ubNumGpio, eUCD9090aGpioOutEnable ubDir, eUCD9090aGpioOutValue ubValue );

		/*!
		 * @brief setPinMonitorConfig	Set the pin monitor configuration with the type and the page. Setting is storaged in
		 *								struct stPin, member of the class.
		 * @param ubNumPin				[0-11]
		 * @param ubMonitorType			[0-4]
		 * @param ubPageByte			[0-10]
		 * @return 0 if success | -1 otherwise
		 */
		int setPinMonitorConfigRAM( uint8_t ubNumPin, uint8_t ubMonitorType, uint8_t ubPageByte );

        /*!
         * @brief setOperationOnlyMode
         * @param ubRail
         * @return
         */
        int setOperationOnlyMode( uint8_t ubRail );

        /*!
         * @brief setControlPinOnlyMode
         * @param ubRail
         * @return
         */
        int setControlPinOnlyMode( uint8_t ubRail );

        /*!
         * @brief setControlPinOnlyMode
         * @param ubPin
         * @param bval
         * @return
         */
        int setPowerGoodMode(uint8_t ubPin, bool bval );

        /*!
         * \brief reloadButton
         * \return
         */
        int reloadButton();

		/*!*******************************************************************************************************
		 *
		 * GET operations
		 *
		 *********************************************************************************************************/
		/*!
		 * @brief getGpioConfig It asks to UCD9090a the whole configuration of the GPIOs using readGPIO and readSeqConfig functions.
		 *						The configuration is storaged in stGPIO array struct.
		 * @return 0 if success | -1 otherwise
		 */
		int getGpioConfig( void );

		/*!
		 * @brief getMonitorConfig	Retrieve the configuration of monitor pins that is saved in a related RAM struct stMonitorConfig.
		 *							It is necessary to invoke readMonitorConfig before to call getMonitorConfig function.
		 * @param ubNumMonPin		[in]	number of selected monitor pin to read
		 * @param ubMonType			[out]
		 * @param ubPageByte		[out]
		 * @return	0 if success | -1 otherwise
		 */
		int getMonitorConfig(uint8_t ubNumMonPin, uint8_t &ubMonType, uint8_t &ubPageByte);

		/*!
		 * @brief getSeqConfig		Retrieve the status of sequence rail previously read with readSeqConfig.
		 * @param eRail
         * @param stPinMonConfig
		 * @return 0 if success | -1 otherwise
		 */
		int getSeqConfig( eUCD9090aRail eRail, stEnablePinConfig &stPinMonConfig );

		/*!*******************************************************************************************************
		 *
		 * WRITE operations
		 *
		 *********************************************************************************************************/

		/*!
		 * @brief resetSoft	This Write-Only Send Byte command restarts the controller firmware. Any active voltage outputs are
		 *					turned off before the firmware restarts.
		 * @return 0 if success | -1 otherwise
		 */
		int writeResetSoft( void );

		/*!
		 * @brief writeClearFaults	This function is used for clear the flag in the faults register
         * @return 0 if success | -1 otherwise
		 */
		int writeClearFaults( void );

        /**
         * @brief writeLoggedFaults It cleans the logged fault s register of the device
         * @return 0 if success | -1 otherwise
         */
        int writeLoggedFaults( void );

		/*!
		 * @brief writeUserRAM  Allows the user to write a byte value into a RAM location of the device. This RAM
		 *						value is reset to a known value (0) when the devie is reset.
         * @param ubVal
		 * @return 0 if success | -1 otherwise
		 */
		int writeUserRAM( uint8_t ubVal );

		/*!
		 * @brief writeStoreDefaultAll It stores the current configuration of UCD9090A in NV memory.
		 * @return 0 if success | -1 otherwise
		 */
		int writeStoreDefaultAll( void );

		/*!
		 * @brief setPage set the page to config or to read.
		 * @param ubPage
		 * @return 0 if success | -1 otherwise
		 */
		int writeSetPage( uint8_t ubPage );

		/*!
		 * @brief writeVoutMode Indicates the data format used for all commands related to output voltage (all LINEAR16 commands). The command includes a 3-
								bit Mode field and a 5-bit Parameter field. In UCD90xxx, the Mode field is a read-only and is fixed to 000b(linear data format, described in Section 2.1).
								The Parameter field is the exponent value and can be modified. The exponent is reported in the bottom 5 bits of the VOUT_MODE parameter.
		 * @param ubExp
		 * @return 0 if success | -1 otherwise
		 */
		int writeVoutMode( uint8_t ubExp );

		/*!
		 * @brief writeSelectGPIO The function is used to select a specific gpio.
		 * @param ubNumGpio
		 * @return 0 if success | -1 otherwise
		 */
		int writeSelectGPIO( uint8_t ubNumGpio );

		/*!
		 * @brief writeConfigGPIO Write the configuration of a gpio previously setted by SelectGPIO command.
		 * @param ubDir
		 * @param ubValue
		 * @return 0 if success | -1 otherwise
		 */
		int writeConfigGPIO( eUCD9090aGpioOutEnable ubDir, eUCD9090aGpioOutValue ubValue );

		/*!
		 * @brief writePinMonitorConfig		Write on the UCD9090A the stPin configuration still present in RAM.
		 * @return 0 if success | -1 otherwise
		 */
		int writePinMonitorConfig( void );

		/*!
		 * @brief writeSeqConfig	The function writes in the PMBUS_MFR_SEQ_CONFIG register the configuration
		 *							of a selected rail. In the pin monitor config parameter  there is the
		 *							configuration of enable pin configuration.
		 * @param eRail
		 * @param stPinMonConfig
		 * @return 0 if success | -1 otherwise
		 */
		int writeSeqConfig(uint8_t eRail, stEnablePinConfig &stPinMonConfig );

		/*!
		 * @brief writeVoutCommand	The function first switch the rail where the setting whould be done, then
		 *							after that digital voltage has been calculated the VOUT_COMMAND set is sent.
		 * @param  fVCmd
		 * @param  ubSetPage
		 * @return 0 if success | -1 otherwise
		 */
		int writeVoutCommand( uint8_t ubSetPage, float fVCmd );

        /*!
         * @brief writeOnOffConfig The ON_OFF_CONFIG command configures the combination of CONTROL pin input
                                    and serial bus commands needed to turn the unit on and off.
         * @param ubReg [IN]        Setting of the register
         * @return 0 if success | -1 otherwise
         */
        int writeOnOffConfig( uint8_t ubReg);

        /*!
         * \brief writeOperation
         * \param ubVal
         * \return
         */
        int writeOperation( uint8_t ubVal );

		/* TODO
		 * VOUT_OV_FAULT_LIMIT , VOUT_OV_WARN_LIMIT   , VOUT_MARGIN_HIGH , VOUT_MARGIN_LOW , VOUT_UV_WARN_LIMIT ,
		 * VOUT_UV_FAULT_LIMIT , POWER_GOOD_ON , POWER_GOOD_OFF
		 * */


		/*!*******************************************************************************************************
		 *
		 * READ operations
		 *
		 *********************************************************************************************************/

		/*!
		 * @brief readPage		Retrieve the current page set on chip.
		 * @param ubCurrPage
		 * @return 0 if success | -1 otherwise
		 */
		int readPage( uint8_t &ubCurrPage );

		/*!
		 * @brief readDeviceID Retrieve the device ID, the string contains the model and the FW version of the device.
		 * @param pubDeviceID
		 * @return 0 if success | -1 otherwise
		 */
		int readDeviceID( string &pubDeviceID );

		/*!
		 * @brief readVoutMode Read the exponenent used for liner conversion of the current rail
		 * @param ubExp
		 * @return 0 if success | -1 otherwise
		 */
		int readVoutMode( int8_t &ubExp );

		/*!
		 * @brief readVout Read the Vout of the current rail
		 * @param fVout
		 * @return 0 if success | -1 otherwise
		 */
		int readVout( float &fVout );

		/*!
		 * @brief readTemperature1
		 * @param index index of the sensor to read
         * @param ubPage page index (0-3)
		 * @param fTemp (output) the temperature value
		 * @return 0 if success | -1 otherwise
		 */
        int readTemperature( uint8_t ubPage, uint8_t index, float &fTemp );

		/*!
		 * @brief readGPIO		Read the configuration of a selected GPIO of the chip. The function switches
		 *						the GPIO before the request of a ubGpioReg.
		 * @param ubNumGPIO
		 * @param ubGpioReg
		 * @return 0 if success | -1 otherwise
		 */
		int readGPIO( uint8_t ubNumGPIO, uint8_t &ubGpioReg );

		/*!
		 * @brief readRegGPIO	The function read the configuration of the GPIO currently set on chip.
		 * @param ubGpioReg
		 * @return 0 if success | -1 otherwise
		 */
		int readRegGPIO( uint8_t &ubGpioReg );

		/*!
		 * @brief readMonitorConfig	Read the configuration of all the monitor pins. The command answers with the configuration
		 *							of all the rails, in particular the the type of the monitoring(e.g.: current, temperature, voltage etc..)
		 *							and the page of the rail(for the UCD9090 there are only 10 pages).
		 * @return 0 if success | -1 otherwise
		 */
		int readMonitorConfig(void);

		/*!
		 * @brief readSeqConfig	This Read/Write Block command reads the sequences dependencies and enable pin for a given rail and
		 *						it stores the information in the stRail structure.
		 * @param eRail
		 * @return 0 if success | -1 otherwise
		 */
		int readSeqConfig( eUCD9090aRail eRail );

		/*!
		 * @brief readUserRAM	Read the RAM variable in the device for the ack of the reset.
		 * @param ubVal
		 * @return 0 if success | -1 otherwise
		 */
		int readUserRAM( uint8_t &ubVal );

		/*!
		 * @brief readVoutCommand Read the exponent for the Vout convertion of a selected rail.
         * @param ubPage page index
         * @param fVoutCmd [out] voltage value
		 * @return 0 if success | -1 otherwise
		 */
		int readVoutCommand( uint8_t ubPage, float &fVoutCmd );

        /*!
         * @brief readStatusRegister    Read status register
         * @param ubVal         Status register
         * @return 0 if success | -1 otherwise
         */
        int readStatusRegister(uint32_t &ubVal);

        /*!
         * @brief readOnOffConfig
         * @param ubReg [OUT]
         * @return 0 if success | -1 otherwise
         */
        int readOnOffConfig( uint8_t &ubReg);

        /*!
         * \brief readOperation
         * \param ubReg
         * \return
         */
        int readOperation(uint8_t &ubReg);

	private:
		/*!
		 * @brief conversionLinear112Float
		 * @param usReadTemp
		 * @return 0 if success | -1 otherwise
		 */
		float conversionLinear112Float( uint16_t usReadTemp );

		/*!
		 * @brief clearRailConfig	It performs the clean of a RAM structures of the class.
		 * @return 0 if success | -1 otherwise
		 */
		int clearRailConfig( void );


    private:

        uint8_t			ubCurrentRail;
        stRailConfig	stRail[UCD9090A_NUM_RAILS_MAX];
        stPinMonConfig	stPinMon[UCD9090A_MAX_NUM_MON_PINS];
        stGPIOPinConfig	stGPIO[UCD9090A_MAX_NUM_GPIO];

};

#endif // UCD9090A_H
