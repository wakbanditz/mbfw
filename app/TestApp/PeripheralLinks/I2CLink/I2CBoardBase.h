/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    I2CBoardBase.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the I2CBoardBase class.
 @details

 ****************************************************************************
*/

#ifndef I2CBOARDBASE_H
#define I2CBOARDBASE_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include "Thread.h"
#include "Mutex.h"
#include "CommonInclude.h"
#include "I2CInterfaceCollector.h"


class I2CInterface;
class Log;

using namespace std;
/*!
 * @brief The I2CBoardBase class	The class is a base for the management of I2C devices attached to the Master board.
 *									It contains I2CInterface and the Logger of the Fw System.
 */
class I2CBoardBase
{
	public:

		I2CBoardBase();
		virtual ~I2CBoardBase();

		/*!
		 * @brief init	Initialization of the logger object and I2CInterface for data write and read.
		 * @param pLogger
		 * @param pI2CIfr
		 * @return 0 if success, -1 otherwise
		 */
		int		init( Log * pLogger, I2CInterface * pI2CIfr);

		/*!
		 * @brief setDeviceAddress	The function sets the I2C device address.
		 * @param slDeviceAddress
		 * @return 0 if success, -1 otherwise
		 */
		int		setDeviceAddress( int32_t slDeviceAddress );

		/*!
		 * @brief getDeviceAddress The function retrieves the specific device address.
		 * @param slDeviceAddress
		 * @return 0 if success, -1 otherwise
		 */
		int		getDeviceAddress( int32_t &slDeviceAddress );

	protected:

		/*!
		 * @brief write
		 * @param ubCmd
		 * @param pubTxBuff
		 * @param usLen
		 * @return 0 if success, -1 otherwise
		 */
		int write(uint8_t ubCmd, uint8_t *pubTxBuff, uint16_t usLen);

		/*!
		 * @brief writeCustomAdd	The function allows to send
		 * @param ubAdd
		 * @param ubCmd
		 * @param pubTxBuff
		 * @param usLen
		 * @return 0 if success, -1 otherwise
		 */
		int writeCustomAdd(uint8_t ubAdd, uint8_t ubCmd, uint8_t *pubTxBuff, uint16_t usLen);

		/*!
		 * @brief writeCustomAdd
		 * @param ubAdd
		 * @param ubCmd
		 * @return 0 if success, -1 otherwise
		 */
		int writeCustomAdd(uint8_t ubAdd, uint8_t ubCmd);

		/*!
		 * @brief read
		 * @param ubCmd
		 * @param pubRxBuff
		 * @param usLen
		 * @return 0 if success, -1 otherwise
		 */
		int read(uint8_t ubCmd, uint8_t *pubRxBuff, uint16_t usLen);

    public:

        static uint8_t	m_numI2CLinkBoard; //Object counter

    protected:

        Log				* m_pLogger;
        I2CInterface	* m_pI2CIfr;

        int32_t			m_slDeviceAddress;

};

#endif // I2CBOARDBASE_H

