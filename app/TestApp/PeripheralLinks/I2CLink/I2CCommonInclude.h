/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    I2CCommonInclude.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the I2C classes.
 @details

 ****************************************************************************
*/

#ifndef I2CCOMMONINCLUDE_H
#define I2CCOMMONINCLUDE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*! **************************************************************************************
 * ADT7470 Fan controller
 *****************************************************************************************
 */
#define ADT7470_MAX_MSG_SIZE_BUFF		16

// ADT7470 registers
#define ADT7470_REG_BASE_ADDR				0x20
#define ADT7470_REG_TEMP_BASE_ADDR			0x20
#define ADT7470_REG_TEMP_MAX_ADDR			0x29
#define ADT7470_REG_FAN_BASE_ADDR			0x2A
#define ADT7470_REG_FAN_MAX_ADDR			0x31
#define ADT7470_REG_PWM_BASE_ADDR			0x32
#define ADT7470_REG_PWM_MAX_ADDR			0x35
#define ADT7470_REG_PWM_MAX_BASE_ADDR		0x38
#define ADT7470_REG_PWM_MAX_MAX_ADDR		0x3B
#define ADT7470_REG_CFG1					0x40
#define ADT7470_REG_CFG2					0x74
#define ADT7470_REG_PULSES					0x43
#define	ADT7470_FSPD_MASK					0x04
#define ADT7470_REG_ALARM1					0x41
#define ADT7470_REG_ALARM2					0x42
#define ADT7470_REG_TEMP_LIMITS_BASE_ADDR	0x44
#define ADT7470_REG_TEMP_LIMITS_MAX_ADDR	0x57
#define ADT7470_REG_FAN_MIN_BASE_ADDR		0x58
#define ADT7470_REG_FAN_MIN_MAX_ADDR		0x5F
#define ADT7470_REG_FAN_MAX_BASE_ADDR		0x60
#define ADT7470_REG_FAN_MAX_MAX_ADDR		0x67
#define ADT7470_REG_PWM_CFG_BASE_ADDR		0x68
#define ADT7470_REG_PWM12_CFG				0x68
#define ADT7470_REG_PWM34_CFG				0x69
#define	ADT7470_REG_PWM_MIN_BASE_ADDR		0x6A
#define ADT7470_REG_PWM_MIN_MAX_ADDR		0x6D
#define ADT7470_REG_PWM_TEMP_MIN_BASE_ADDR	0x6E
#define ADT7470_REG_PWM_TEMP_MIN_MAX_ADDR	0x71
#define ADT7470_REG_ACOUSTICS12				0x75
#define ADT7470_REG_ACOUSTICS34				0x76
#define ADT7470_REG_DEVICE					0x3D
#define ADT7470_REG_VENDOR					0x3E
#define ADT7470_REG_REVISION				0x3F
#define ADT7470_REG_ALARM1_MASK				0x72
#define ADT7470_REG_ALARM2_MASK				0x73
#define ADT7470_REG_PWM_AUTO_TEMP_BASE_ADDR	0x7C
#define ADT7470_REG_PWM_AUTO_TEMP_MAX_ADDR	0x7D
#define ADT7470_REG_MAX_ADDR				0x81

#define ADT7470_TEMP_COUNT					10
#define ADT7470_TEMP_REG(x)					(ADT7470_REG_TEMP_BASE_ADDR + (x))
#define ADT7470_TEMP_MIN_REG(x)				(ADT7470_REG_TEMP_LIMITS_BASE_ADDR + ((x) * 2))
#define ADT7470_TEMP_MAX_REG(x)				(ADT7470_REG_TEMP_LIMITS_BASE_ADDR + ((x) * 2) + 1)

#define ADT7470_FAN_COUNT					4
#define ADT7470_REG_FAN(x)					(ADT7470_REG_FAN_BASE_ADDR + ((x) * 2))
#define ADT7470_REG_FAN_MIN(x)				(ADT7470_REG_FAN_MIN_BASE_ADDR + ((x) * 2))
#define ADT7470_REG_FAN_MAX(x)				(ADT7470_REG_FAN_MAX_BASE_ADDR + ((x) * 2))

#define ADT7470_PWM_COUNT					4
#define ADT7470_REG_PWM(x)					(ADT7470_REG_PWM_BASE_ADDR + (x))
#define ADT7470_REG_PWM_MAX(x)				(ADT7470_REG_PWM_MAX_BASE_ADDR + (x))
#define ADT7470_REG_PWM_MIN(x)				(ADT7470_REG_PWM_MIN_BASE_ADDR + (x))
#define ADT7470_REG_PWM_TMIN(x)				(ADT7470_REG_PWM_TEMP_MIN_BASE_ADDR + (x))
#define ADT7470_REG_PWM_CFG(x)				(ADT7470_REG_PWM_CFG_BASE_ADDR + ((x) / 2))
#define ADT7470_REG_PWM_AUTO_TEMP(x)		(ADT7470_REG_PWM_AUTO_TEMP_BASE_ADDR + ((x) / 2))

#define ALARM2(x)							((x) << 8)

#define ADT7470_VENDOR						0x41
#define ADT7470_DEVICE						0x2F

// Mask
typedef enum
{
	eADT7470_R1T_ALARM		= 0x01,
	eADT7470_R2T_ALARM		= 0x02,
	eADT7470_R3T_ALARM		= 0x04,
	eADT7470_R4T_ALARM		= 0x08,
	eADT7470_R5T_ALARM		= 0x10,
	eADT7470_R6T_ALARM		= 0x20,
	eADT7470_R7T_ALARM		= 0x40,
	eADT7470_OOL_ALARM		= 0x80
} eADT7470ItrReg1;

typedef enum
{
	eADT7470_R8T_ALARM		= 0x01,
	eADT7470_R9T_ALARM		= 0x02,
	eADT7470_R10T_ALARM		= 0x04,
	eADT7470_FAN1_ALARM		= 0x10,
	eADT7470_FAN2_ALARM		= 0x20,
	eADT7470_FAN3_ALARM		= 0x40,
	eADT7470_FAN4_ALARM		= 0x80
} eADT7470ItrReg2;

//#define	ADT7470_R1T_ALARM					0x01
//#define	ADT7470_R2T_ALARM					0x02
//#define	ADT7470_R3T_ALARM					0x04
//#define	ADT7470_R4T_ALARM					0x08
//#define	ADT7470_R5T_ALARM					0x10
//#define	ADT7470_R6T_ALARM					0x20
//#define	ADT7470_R7T_ALARM					0x40
//#define	ADT7470_OOL_ALARM					0x80
//#define	ADT7470_R8T_ALARM					0x01
//#define	ADT7470_R9T_ALARM					0x02
//#define	ADT7470_R10T_ALARM					0x04
//#define	ADT7470_FAN1_ALARM					0x10
//#define	ADT7470_FAN2_ALARM					0x20
//#define	ADT7470_FAN3_ALARM					0x40
//#define	ADT7470_FAN4_ALARM					0x80
#define	ADT7470_PWM2_AUTO_MASK				0x40
#define	ADT7470_PWM1_AUTO_MASK				0x80
#define	ADT7470_PWM_AUTO_MASK				0xC0
#define	ADT7470_PWM3_AUTO_MASK				0x40
#define	ADT7470_PWM4_AUTO_MASK				0x80

// datasheet only mentions a revision 2
#define ADT7470_REVISION					0x02

// "all temps" according to hwmon sysfs interface spec
#define ADT7470_PWM_ALL_TEMPS				0x3FF

// How often do we reread sensors values? (In jiffies)
#define SENSOR_REFRESH_INTERVAL				(5 * HZ)

// How often do we reread sensor limit values? (In jiffies)
#define LIMIT_REFRESH_INTERVAL				(60 * HZ)

// Wait at least 200ms per sensor for 10 sensors
#define TEMP_COLLECTION_TIME				2000

// auto update thing won't fire more than every 2s
#define AUTO_UPDATE_INTERVAL				2000

// datasheet says to divide this number by the fan reading to get fan rpm
#define FAN_PERIOD_TO_RPM(x)				((90000 * 60) / (x))
#define FAN_RPM_TO_PERIOD					FAN_PERIOD_TO_RPM
#define FAN_PERIOD_INVALID					65535
#define FAN_DATA_VALID(x)					((x) && (x) != FAN_PERIOD_INVALID)


/*! ***************************************************************************************
 * PCA9633
 *****************************************************************************************
 */
#define PCA9633_MAX_MSG_SIZE_BUFF		16
#define PCA9633_MAX_NUM_LED				4

//PCA9633 REG
#define PCA9633_MODE1					0x00
#define PCA9633_MODE2					0x01
#define PCA9633_PWM0					0x02
#define PCA9633_PWM1					0x03
#define PCA9633_PWM2					0x04
#define PCA9633_PWM3					0x05
#define PCA9633_GRPPWM					0x06
#define PCA9633_GRPFREQ					0x07
#define PCA9633_LEDOUT					0x08

//PCA9633 LEDOUT
#define PCA9633_LEDOUT_MASK_LED0		0x03
#define PCA9633_LEDOUT_MASK_LED1		0x0C
#define PCA9633_LEDOUT_MASK_LED2		0x30
#define PCA9633_LEDOUT_MASK_LED3		0xC0

//PCA9633 SWRST
#define PCA9633_SWRST_ADD				0x03
#define PCA9633_SWRST_REG				0xA5
#define PCA9633_SWRST_VAL				0x5A

//PCA9633 MODE2 register bit
#define PCA9633_BIT_DMBLNK				0x05
#define PCA9633_BIT_INVRT				0x04
#define PCA9633_BIT_OUTDRV				0x02


#define PCA9633_LEDOUT_OFF				0x00
#define PCA9633_LEDOUT_PWM_ALL			0xAA
#define PCA9633_LEDOUT_ON				0x01
#define PCA9633_LEDOUT_PWM_ON			0x02

/*! ***************************************************************************************
 * PCA9955b
 ******************************************************************************************
 */
typedef struct
{
	uint8_t	ubAddLedGrp;
	uint8_t	ubMaskLedGrp;
	uint8_t ubPosLedGrp;
    uint8_t ubErrorLedGrp;

} PCA9955b_LedStruct;

//Common defines
#define PCALED_MAX_MSG_SIZE_BUFF		16
#define PCALED_MAX_NUM_LED				16

#ifdef PCA9952_REG_CONF
#define PCA9952_GRPPWM                                 0x08
#define PCA9952_GRPFREQ                                0x09

#define PCA9952_PWM0                                   0x0A
#define PCA9952_PWM1                                   0x0B
#define PCA9952_PWM2                                   0x0C
#define PCA9952_PWM3                                   0x0D
#define PCA9952_PWM4                                   0x0E
#define PCA9952_PWM5                                   0x0F
#define PCA9952_PWM6                                   0x10
#define PCA9952_PWM7                                   0x11
#define PCA9952_PWM8                                   0x12
#define PCA9952_PWM9                                   0x13
#define PCA9952_PWM10                                  0x14
#define PCA9952_PWM11                                  0x15
#define PCA9952_PWM12                                  0x16
#define PCA9952_PWM13                                  0x17
#define PCA9952_PWM14                                  0x18
#define PCA9952_PWM15                                  0x19

#define PCA9952_IREF0                                  0x22
#define PCA9952_IREF1                                  0x23
#define PCA9952_IREF2                                  0x24
#define PCA9952_IREF3                                  0x25
#define PCA9952_IREF4                                  0x26
#define PCA9952_IREF5                                  0x27
#define PCA9952_IREF6                                  0x28
#define PCA9952_IREF7                                  0x29
#define PCA9952_IREF8                                  0x2A
#define PCA9952_IREF9                                  0x2B
#define PCA9952_IREF10                                 0x2C
#define PCA9952_IREF11                                 0x2D
#define PCA9952_IREF12                                 0x2E
#define PCA9952_IREF13                                 0x2F
#define PCA9952_IREF14                                 0x30
#define PCA9952_IREF15                                 0x31

#define PCA9952_OFFSET                                 0x3A

#define PCA9952_PWMALL                                 0x42
#define PCA9952_IREFALL                                0x43
#define PCA9952_EFLAG0                                 0x44
#define PCA9952_EFLAG1                                 0x45
#else
//PCA9955 REG
#define PCALED_MODE1					0x00
#define PCALED_MODE2					0x01
#define PCALED_LEDOUT0					0x02
#define PCALED_LEDOUT1					0x03
#define PCALED_LEDOUT2					0x04
#define PCALED_LEDOUT3					0x05

#define PCALED_GRPPWM					0x06
#define PCALED_GRPFREQ					0x07

#define PCALED_PWM0					0x08
#define PCALED_PWM1					0x09
#define PCALED_PWM2					0x0A
#define PCALED_PWM3					0x0B
#define PCALED_PWM4					0x0C
#define PCALED_PWM5					0x0D
#define PCALED_PWM6					0x0E
#define PCALED_PWM7					0x0F
#define PCALED_PWM8					0x10
#define PCALED_PWM9					0x11
#define PCALED_PWM10					0x12
#define PCALED_PWM11					0x13
#define PCALED_PWM12					0x14
#define PCALED_PWM13					0x15
#define PCALED_PWM14					0x16
#define PCALED_PWM15					0x17

#define PCALED_IREF0					0x18
#define PCALED_IREF1					0x19
#define PCALED_IREF2					0x1A
#define PCALED_IREF3					0x1B
#define PCALED_IREF4					0x1C
#define PCALED_IREF5					0x1D
#define PCALED_IREF6					0x1E
#define PCALED_IREF7					0x1F
#define PCALED_IREF8					0x20
#define PCALED_IREF9					0x21
#define PCALED_IREF10					0x22
#define PCALED_IREF11					0x23
#define PCALED_IREF12					0x24
#define PCALED_IREF13					0x25
#define PCALED_IREF14					0x26
#define PCALED_IREF15					0x27

#define PCALED_OFFSET					0x3F

#define PCALED_PWMALL					0x44
#define PCALED_IREFALL					0x45
#define PCALED_EFLAG0					0x46
#define PCALED_EFLAG1					0x47
#define PCALED_EFLAG2					0x48
#define PCALED_EFLAG3					0x49
#endif

//PCA9955B LEDOUT
#define PCALED_LEDOUT_MASK_LED_A        0x03
#define PCALED_LEDOUT_MASK_LED_B        0x0C
#define PCALED_LEDOUT_MASK_LED_C        0x30
#define PCALED_LEDOUT_MASK_LED_D        0xC0

#define PCALED_LEDOUT_POS_A             0
#define PCALED_LEDOUT_POS_B             2
#define PCALED_LEDOUT_POS_C             4
#define PCALED_LEDOUT_POS_D             6


//PCA9955B SWRST
#define PCALED_SWRST_ADD				0x00
#define PCALED_SWRST_REG				0x06

//PCA9955B MODE2 register bit
#define PCALED_BIT_DMBLNK				0x05
#define PCALED_BIT_INVRT				0x04
#define PCALED_BIT_OUTDRV				0x02
#define PCALED_BIT_CLRERR				0x10

#define PCALED_MODE2_BLNK_CTL			0x01
#define PCALED_MODE2_DIM_CTL			0x02

/*! ***************************************************************************************
 * PCA9537
 ******************************************************************************************
 */
#define PCA9537_MAX_MSG_SIZE_BUFF		16

//PCA9537 register
#define PCA9537_INPUT_REG				0x00
#define PCA9537_OUTPUT_REG				0x01
#define PCA9537_POL_INV_REG				0x02
#define PCA9537_CONF_REG				0x03

/*! ***************************************************************************************
 * UCD9090A
 ******************************************************************************************
 */
#endif // I2CCOMMONINCLUDE_H
