/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PCA9955b.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the PCA9955b class.
 @details

 ****************************************************************************
*/

#ifndef PCA9955_H
#define PCA9955_H

#include <I2CBoardBase.h>
#include "I2CCommonInclude.h"

#define OUTPUT_LED_GREEN_0    0
#define OUTPUT_LED_GREEN_1    1
#define OUTPUT_LED_GREEN_2    2
#define OUTPUT_LED_YELLOW_0   3
#define OUTPUT_LED_YELLOW_1   4
#define OUTPUT_LED_YELLOW_2   5
#define OUTPUT_LED_RED_0      6
#define OUTPUT_LED_RED_1      7
#define OUTPUT_LED_RED_2      8

#define OUTPUT_LED_DIS_OSC  9
/*!
 * @brief The PCA9952 class Status LED driver board implementation
 */
class PCA9955b : public I2CBoardBase
{
	public:
        PCA9955b();
        virtual ~PCA9955b();

		/*!
		 * @brief resetSoft		It performs a reset SW of the device. It initializes the registers to
		 *						their default state causing the outputs to be set HIGH.
		 *						This command is different from the others: it is able to reset the device
		 *						if the address sent is 0x00. After, the master has to send data byte 0x06.
		 *						Only by sending this sequence PCA9633 cam acknowledge the command.
		 *
		 * @return 0 if success | -1 otherwise
		 */
		int resetSoft( void );

		/*!
		 * @brief config		It performs the initializations of the MODE registers. In particular, it initializes
		 *						all the bits of MODE1 and MODE2 registers with a definite values.
		 *						MODE1 is set in sleep mode, without subaddress and not All Call i2c-bus address.
		 *						MODE2 is set in dimming control.
         * @param ubDmBlinkState blinking mode
		 * @return 0 if success | -1 otherwise
		 */
		int config(uint8_t ubDmBlinkState);

        /*!
         * @brief writeClearError   The function is used to clear error bit in mode2 register
         * @return 0 if success | -1 otherwise
         */
        int clearError();

        /*!****************************************************************************************
		 *
		 * WRITE operations
		 *
		 ******************************************************************************************/
		/*!
		 * @brief writeMode1	Write on Mode1 configuration register. Default value is 0x09.
		 *
		 * @param ubMode
		 * @return 0 if success | -1 otherwise
		 */
		int writeMode1(uint8_t ubMode);

		/*!
		 * @brief writeMode2	Write on Mode2 configuration register. Default value is 0x05.
		 * @param ubMode
		 * @return 0 if success | -1 otherwise
		 */
		int writeMode2(uint8_t ubMode);

        /*!
         * @brief clearErrorMode2	Write on Mode2 configuration register setting the CLRERR bit = 1
         * @return 0 if success | -1 otherwise
         */
        int clearErrorMode2();


		/*!
		 * @brief writeLedOut	It sets the output state control of a specific eLed driver.
		 *						- 0 -> Led driver is off
		 *						- 1 -> Led driver is fully on (individual brightness and group dimming/blinking
		 *								not controlled).
		 *						- 2 -> Led driver brightness can be controlled through its PWMx register.
		 *						- 3 -> Led driver brightness and group diiming/blinking can be controlled through its
		 *							   PWM register and GRPPWM registers.
         * @param ubLed			[0-PCALED_MAX_NUM_LED]
		 * @param ubState		[0-3]
		 * @return 0 if success | -1 otherwise
		 */
		int writeLedOut( uint8_t ubLed, uint8_t ubState );

        /*!
         * @brief clearLedOut	It sets the output state control of all eLed driver to 0 (off).
         * @return 0 if success | -1 otherwise
         */
        int clearLedOut();


		/*!
		 * @brief writeGrpPWM	GRPPWM is used as a global brightness control allowing the LED outputs to be dimmed with the
		 *						same value.
		 *						General brightness for the 4 outputs is controlled through 256 linear steps from 00h
		 *						(0 % duty cycle = LED output off) to FFh (99.6 % duty cycle = maximum brightness).
		 *						Applicable to LED outputs programmed with LDRx = 11 (LEDOUT register).
		 *						Duty cycle = GDC[7:0]/256
		 * @param ubDutyCycle	[0-100]
		 * @return 0 if success | -1 otherwise
		 */
		int writeGrpPWM(uint8_t ubDutyCycle);

		/*!
		 * @brief writeGrpFreq	GRPFREQ is used to program the global blinking period when DMBLNK bit (MODE2
		 *						register) is equal to 1. Value in this register is a ‘Don’t care’ when DMBLNK = 0.
		 *						Blinking period is controlled through 256 linear steps from 00h (67 ms, frequency 15 Hz)
		 *						to FFh (16.8 s).
		 *						global blinking period = (GFRQ[7:0] + 1)/15.26
		 * @param ubFreq		[0-100]
		 * @return 0 if success | -1 otherwise
		 */
		int writeGrpFreq(uint8_t ubFreq);

		/*!
		 * @brief writePWM		It writes the desired brightness in the related PWM register.
		 *						A 31.25 kHz fixed frequency signal is used for each output. Duty cycle is controlled through
		 *						256 linear steps from 00h (0 % duty cycle = LED output off) to FFh.
		 *						duty cycle=IDC[7:0]/256
         * @param ubNumLed		[0-PCALED_MAX_NUM_LED]
		 * @param ubBrightness	[0-100]
		 * @return 0 if success | -1 otherwise
		 */
		int writePWM(uint8_t ubNumLed, uint8_t ubBrightness );

		/*!
		 * @brief writeIref		The functions set the IREF register of related LED. These registers reflect the gain
		 *						settings for output current for LED0 to LED15.
         * @param ubNumLed		[0-PCALED_MAX_NUM_LED]
		 * @param ubCurrent		[0-100]
		 * @return 0 if success | -1 otherwise
		 */
		int writeIref(uint8_t ubNumLed, uint8_t ubCurrent);

		/*!
		 * @brief writeOffset	OFFSET control register bits [3:0] determine the delay used between the turn-on times.
		 * @param ubOffset		[0-15]
		 * @return 0 if success | -1 otherwise
		 */
		int writeOffset(uint8_t ubOffset);

		/*!
		 * @brief writePWMAll	When programmed, the value in this register will be used for PWM duty cycle for all the
		 *						LEDn outputs and will be reflected in PWM0 through PWM15 registers.
		 *						Write to any of the PWM0 to PWM15 registers will overwrite the value in corresponding
		 *						PWMn register programmed by PWMALL.
		 * @param ubBrightness	[0-100]
		 * @return 0 if success | -1 otherwise
		 */
		int writePWMAll(uint8_t ubBrightness);

		/*!
		 * @brief writeIrefAll	The output current setting for all outputs is held in this register. When this register is
		 *						written to or updated, all LEDn outputs will be set to a current corresponding to this
		 *						register value.
		 *						Write to IREF0 to IREF15 will overwrite the output current settings.
		 * @param ubCurrent		[0-100]
		 * @return 0 if success | -1 otherwise
		 */
		int writeIrefAll(uint8_t ubCurrent);

		/*!****************************************************************************************
		 *
		 * READ operations
		 *
		 ******************************************************************************************/
		/*!
		 * @brief readMode1
		 * @param ubModeReg
		 * @return 0 if success | -1 otherwise
		 */
		int readMode1( uint8_t &ubModeReg );

		/*!
		 * @brief readMode2
		 * @param ubModeReg
		 * @return 0 if success | -1 otherwise
		 */
		int readMode2( uint8_t &ubModeReg);

		/*!
		 * @brief readLedOut	Read whole register LEDOUT
         * @param ubNumLed
		 * @param ubState
		 * @return 0 if success | -1 otherwise
		 */
		int readLedOut(uint8_t ubNumLed, uint8_t &ubState );

		/*!
		 * @brief readPWM		Read Brightness set in the related PWM register.
         * @param ubNumLed
		 * @param ubBrightness
		 * @return 0 if success | -1 otherwise
		 */
		int readPWM(uint8_t ubNumLed, uint8_t &ubBrightness );

		/*!
		 * @brief readPWMAll
		 * @param ubBrightness
		 * @return 0 if success | -1 otherwise
		 */
		int readPWMAll(uint8_t &ubBrightness);

		/*!
		 * @brief readIref
		 * @param ubNumLed
		 * @param ubIref
		 * @return 0 if success | -1 otherwise
		 */
		int readIref(uint8_t ubNumLed, uint8_t &ubIref);

		/*!
		 * @brief readIrefAll
		 * @param ubIref
		 * @return 0 if success | -1 otherwise
		 */
		int readIrefAll(uint8_t &ubIref);

		/*!
		 * @brief reafOffset
		 * @param ubOffset
		 * @return 0 if success | -1 otherwise
		 */
		int readOffset(uint8_t &ubOffset);

		/*!
		 * @brief readGrpFreq
		 * @param ubGrpFreq
		 * @return 0 if success | -1 otherwise
		 */
		int readGrpFreq(uint8_t &ubGrpFreq);

		/*!
		 * @brief readGrpPWM
		 * @param ubGrpPwm
		 * @return 0 if success | -1 otherwise
		 */
		int readGrpPWM(uint8_t &ubGrpPwm);

        /*!
         * @brief readErrorBit checks if an error has been set in Mode2 register
         * @param ubNumLed the led index
         * @return 0 if success | -1 otherwise
         */
        bool readErrorBit(uint8_t ubNumLed);

	private:
		/*!
		 * @brief setConfLedGrp
		 */
		void setConfLedGrp(void);

    private:
        PCA9955b_LedStruct	m_tableLedGrp[PCALED_MAX_NUM_LED];

};

#endif // PCA9955_H
