/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    I2CLinkBoard.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the I2CLinkBoard class.
 @details

 ****************************************************************************
*/

#ifndef I2CLINKBOARD_H
#define I2CLINKBOARD_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include "Thread.h"
#include "Mutex.h"
#include "CommonInclude.h"
#include "I2CInterfaceCollector.h"

class UCD9090a;
class PCA9633;
class PCA9955b;
class ADT7470;
class AT24C256C;
class TLV320AIC31xx;
class PCA9537;

#define BLK_TIME_LED    1.2
/*!
 * @brief The I2CLinkBoard class
 * The class reflects the I2C section composition inside the master board. It contains the I2C channels where
 * the devices should be connected. In particular power manager(UCD9090A), fan controller(ADT7470), eeprom(AT24C256C)
 * are connected to I2C3 channel.
 * Audio codec, led/gpio expander(PCA9633) and led driver are connected to I2C2.
 * All devices are public objects of the class in order to make them easier to call.
 */
class I2CLinkBoard : public Loggable
{
	public:

		/*!
		 * @brief I2CLinkBoard void constructor
		 */
		I2CLinkBoard();

		/*!
		 * @brief ~I2CLinkBoard default destructor
		 */
		virtual ~I2CLinkBoard();

		/*!
		 * @brief setChannels	set the I2C channels present in the carrier board
		 * @return 0 if success | -1 otherwise
		 */
		int createI2CInterfaces(void);

		/*!
		 * @brief destroyI2CInterfaces	deallocates I2C interfaces by calling the m_I2CIfrCollector destroyInterfaces()
		 *								method
		 */
		void destroyI2CInterfaces(void);

		/*!
		 * @brief initPowerManager	initialize the PowerManager by calling its init() method and by setting its
		 *							I2C address
		 * @return 0 if success | -1 otherwise
		 */
		int createAndInitPowerManager(void);

		/*!
		 * @brief destroyI2CDevice
		 * @return 0 if success | -1 otherwise
		 */
		int destroyPowerManager(void);

		/*!
		 * @brief createAndInitGpioExpander	initialize the GpioExpander by calling its init() method and by setting its
		 *							I2C address
		 * @return 0 if success | -1 otherwise
		 */
		int createAndInitGpioExpanderA(void);

		/*!
		 * @brief destroyGpioExpander
		 * @return 0 if success | -1 otherwise
		 */
		int destroyGpioExpanderA(void);

		/*!
		 * @brief createAndInitGpioExpander	initialize the GpioExpander by calling its init() method and by setting its
		 *							I2C address
		 * @return 0 if success | -1 otherwise
		 */
		int createAndInitGpioExpanderB(void);

		/*!
		 * @brief destroyGpioExpander
		 * @return 0 if success | -1 otherwise
		 */
		int destroyGpioExpanderB(void);

        /*!
         * @brief reset the Section Board via hardware
         * @param section identifier
         * @return 0 if success | -1 otherwise
         */
        int resetSectionBoard(uint8_t section);

		/*!
		 * @brief createLedDriver
         * @param bExpLedEnabled if GPIOExpendarLED is present or not
		 * @return 0 if success | -1 otherwise
		 */
        int createLedDriver(bool bExpLedEnabled);

		/*!
		 * @brief	destroyLedDriver
		 * @return	0 if success | -1 otherwise
		 */
		int destroyLedDriver(void);

		/*!
		 * @brief	createAndInitFanController
		 * @return	0 if success | -1 otherwise
		 */
		int createAndInitFanController(void);

		/*!
		 * @brief	destroyFanController
		 * @return	0 if success | -1 otherwise
		 */
		int destroyFanController(void);

		/*!
		 * @brief	createAndInitEEprom
		 * @return	0 if success | -1 otherwise
		 */
		int createAndInitEEprom(void);

		/*!
		 * @brief	destroyEeprom
		 * @return	0 if success | -1 otherwise
		 */
		int destroyEeprom(void);

		/*!
		 * @brief createAndInitCodecAudio
		 * @return 0 if success | -1 otherwise
		 */
		int createAndInitCodecAudio(void);

		/*!
		 * @brief destroyCodecAudio
		 * @return 0 if success | -1 otherwise
		 */
		int destroyCodecAudio(void);

		/*!
		 * @brief createAndInitGpioExpanderLed
		 * @return 0 if success | -1 otherwise
		 */
		int createAndInitGpioExpanderLed(void);

		/*!
		 * @brief destroyGpioExpanderLed
		 * @return 0 if success | -1 otherwise
		 */
		int destroyGpioExpanderLed(void);

    public:

        UCD9090a		*m_pPowerManager;
        PCA9633			*m_pGpioExpanderA;
        PCA9633			*m_pGpioExpanderB;
        PCA9955b			*m_pLedDriver;
        ADT7470			*m_pFanController;
        AT24C256C		*m_pEeprom;
        TLV320AIC31xx	*m_pCodecAudio;
        PCA9537			*m_pGpioExpanderLed;

    private:

        I2CInterfaceCollector m_I2CIfrCollector;

};

#endif // I2CLINKBOARD_H
