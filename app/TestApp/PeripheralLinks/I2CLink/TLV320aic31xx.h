/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    TLV320aic31xx.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the TLV320aic31xx class.
 @details

 ****************************************************************************
*/

#ifndef TLV320AIC31XX_H
#define TLV320AIC31XX_H

#include <I2CBoardBase.h>
#include <alsa/asoundlib.h>

#define TLV320_MAX_SIZE_BUFF	64

class TLV320AIC31xx : public I2CBoardBase
{
	public:
		typedef enum
		{
			eTLV320_DAC = 0,
			eTLV320_ANVOL = 1,
			eTLV320_DVOL = 2
		}eTLV320_CONTROLS;

	public:
		TLV320AIC31xx();
		virtual ~TLV320AIC31xx();

		int setCardName(uint8_t *ubName);

		int setSpeakerAnalogVolume(uint8_t ubVolume);

		int setDACGain(uint8_t ubVal);

		int setSpeakerDriverGain(uint8_t ubVal);

		int getSpeakerAnalogVolume(long &lVolume);

		int getDACGain(uint8_t &ubVal);

		int getSpeakerDriverGain(uint8_t &bVal);

		int getSpeakerRange(eTLV320_CONTROLS eTypeCtrl, long &lmin, long &lmax);

	private:

		snd_mixer_elem_t *selectControl(snd_mixer_t *handle, uint8_t *ubControl);

		int setSndMixerHandle(void);

    private:
        uint8_t		m_ubCardName[TLV320_MAX_SIZE_BUFF];
        uint8_t		m_ubControlSpeakerDriverName[TLV320_MAX_SIZE_BUFF];
        uint8_t		m_ubControlSpeakerAnalogName[TLV320_MAX_SIZE_BUFF];
        uint8_t		m_ubControlDACName[TLV320_MAX_SIZE_BUFF];

        snd_mixer_t			 *m_pHandle;

};

#endif // TLV320AIC31XX_H
