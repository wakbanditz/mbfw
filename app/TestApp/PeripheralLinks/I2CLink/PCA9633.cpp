/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PCA9633.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the PCA9633 class.
 @details

 ****************************************************************************
*/

#include "PCA9633.h"
#include "math.h"

/*!***************************************************************************************************************
 * Defines
 *****************************************************************************************************************/


/*!***************************************************************************************************************
 * Methods
 *****************************************************************************************************************/
PCA9633::PCA9633()
{
	/* Nothing to do yet */
}

PCA9633::~PCA9633()
{
	/* Nothing to do yet */
}

int PCA9633::resetSoft(void)
{
	uint8_t pubTxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = PCA9633_SWRST_VAL;

	int ret = writeCustomAdd(PCA9633_SWRST_ADD, PCA9633_SWRST_REG, pubTxBuff, 1);
	if ( ret != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9633::resetSoft-->Unable to reset device");
		return -1;
	}

	return 0;
}

int PCA9633::config(void)
{
	int res = writeMode1(0);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9633::config-->Unable config PCA9633");
		return -1;
	}

	res = writeMode2(0);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9633::config-->Unable config PCA9633");
		return -1;
	}

	return 0;
}

/*!****************************************************************************************
 *
 * WRITE operations
 *
 ******************************************************************************************/
int PCA9633::writeMode1(uint8_t ubMode)
{
	uint8_t pubTxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubMode;

	int res = write(PCA9633_MODE1, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writeMode1-->Unable to write on Mode1 register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9633::writeMode1-->Write on Mode1 register");
	}

	return 0;
}

int PCA9633::writeMode2(uint8_t ubMode)
{
	uint8_t pubTxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubMode;

	int res = write(PCA9633_MODE2, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writeMode2-->Unable to write on Mode2 register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9633::writeMode2-->Write on Mode2 register");
	}

	return 0;
}

int PCA9633::writePWM(uint8_t ePwm, uint8_t ubBrightness)
{
	uint8_t pubTxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	uint8_t	ubNumPWMAdd = (uint8_t)ePwm + 2;

	if (ubBrightness > 100)
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writePWM-->Param out of range");
		return -1;
	}

	uint8_t ubNumDig = ((uint16_t)ubBrightness*255)/100;
	pubTxBuff[0] = ubNumDig;

	int res = write(ubNumPWMAdd, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writePWM-->Unable to write on PWM 0x%x register", ubNumPWMAdd);
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9633::writePWM-->Write on PWM register");
	}
	return 0;
}

int PCA9633::writeLedOut(uint8_t ubState)
{
	uint8_t pubTxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubState;

	int res = write(PCA9633_LEDOUT, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writePWM-->Unable to write on LEDOUT register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9633::writePWM-->Write on LEDOUT register");
	}
	return 0;
}

int PCA9633::writeLedOut(uint8_t eLed, uint8_t ubState)
{
	uint8_t pubTxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (eLed >= PCA9633_MAX_NUM_LED)
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writeLedOut-->Num LED out of range");
		return -1;
	}

	//Read the current state of the register
	uint8_t	ubStateOld = 0;
	int res = readLedOut(ubStateOld);
	if (res != 0)
	{
		return -1;
	}

	switch (eLed)
	{
		case eLED0:
			ubStateOld &= ~PCA9633_LEDOUT_MASK_LED0;
		break;

		case eLED1:
			ubState = ubState << 2;
			ubStateOld &= ~PCA9633_LEDOUT_MASK_LED1;
		break;

		case eLED2:
			ubState = ubState << 4;
			ubStateOld &= ~PCA9633_LEDOUT_MASK_LED2;
		break;

		case eLED3:
			ubState = ubState << 6;
			ubStateOld &= ~PCA9633_LEDOUT_MASK_LED3;
		break;

		default:
			m_pLogger->log( LOG_ERR, "PCA9633::writePWM-->Unable to write on LEDOUT register");
			return -1;
		break;

	}

	ubState = ubState | ubStateOld;
	pubTxBuff[0] = ubState;

	res = write(PCA9633_LEDOUT, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writePWM-->Unable to write on LEDOUT register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9633::writePWM-->Write on LEDOUT register");
	}
	return 0;
}

int PCA9633::writeGrpPWM(uint8_t ubDutyCycle)
{
	uint8_t pubTxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubDutyCycle > 100)
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writeGrpPWM-->Param out of range");
		return -1;
	}

	uint8_t ubNumDig = ((uint16_t)ubDutyCycle*255)/100;
	pubTxBuff[0] = ubNumDig;

	int res = write(PCA9633_GRPPWM, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writeGrpPWM-->Unable to write on GRPPWM register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9633::writeGrpPWM-->Write on GRPPWM register");
	}

	return 0;
}

int PCA9633::writeGrpFreq(uint8_t ubFreq)
{
	uint8_t pubTxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubFreq > 100)
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writeGrpFreq-->Param out of range");
		return -1;
	}

	uint8_t ubNumDig = ((uint16_t)ubFreq*255)/100;
	pubTxBuff[0] = ubNumDig;

	int res = write(PCA9633_GRPFREQ, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "PCA9633::writeGrpFreq-->Unable to write on GRPFREQ register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9633::writeGrpFreq-->Write on GRPFREQ register");
	}

	return 0;
}

/*!****************************************************************************************
 *
 * READ operations
 *
 ******************************************************************************************/
int PCA9633::readMode1(uint8_t &ubModeReg)
{
	uint8_t pubRxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(PCA9633_MODE1, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log( LOG_ERR, "PCA9633::readMode1-->Unable to read on Mode1 register");
		return -1;
	}
	else
	{
		ubModeReg = pubRxBuff[0];
	}

	return 0;
}

int PCA9633::readMode2(uint8_t &ubModeReg)
{
	uint8_t pubRxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(PCA9633_MODE1, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log( LOG_ERR, "PCA9633::readMode2-->Unable to read on Mode2 register");
		return -1;
	}
	else
	{
		ubModeReg = pubRxBuff[0];
	}

	return 0;
}

int PCA9633::readPWM(uint8_t ePwm, uint8_t &ubBrightness)
{
	uint8_t pubRxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	uint8_t	ubNumPWMAdd = (uint8_t)ePwm + 2;

	int res = read(ubNumPWMAdd, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log( LOG_ERR, "PCA9633::readPWM-->Unable to read PWM 0x%x register", ubNumPWMAdd);
		return -1;
	}
	else
	{
		float fAppo = pubRxBuff[0];
		uint8_t ubNumPerc =  round(((fAppo*100)/255));

		ubBrightness = ubNumPerc;
	}

	return 0;
}

int PCA9633::readLedOut(uint8_t &ubState)
{
	uint8_t pubRxBuff[PCA9633_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(PCA9633_LEDOUT, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log( LOG_ERR, "PCA9633::readLedOut-->Unable to read LEDOUT register");
		return -1;
	}
	else
	{
		ubState = pubRxBuff[0];
	}

	return 0;
}




