/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    I2CInterfaceCollector.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the I2CInterfaceCollector class.
 @details

 ****************************************************************************
*/

#ifndef I2CCHANNEL_H
#define I2CCHANNEL_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#include "CommonInclude.h"
#include "I2CInterface.h"
#include "Loggable.h"
#include "Mutex.h"


typedef enum
{
	eI2C_0		= 0,
	eI2C_1		= 1,
	eI2C_2		= 2,
	eI2C_3		= 3,
	eI2C_4		= 4,

} enumI2CInterface;

/*!
 * @brief	The I2CChannel class	Class for the management of I2C Interface.
 *									It allows the opening and the closing of the I2C interface of the operative system.
 */
class I2CInterfaceCollector : public Loggable
{
	public:

		I2CInterfaceCollector();
		virtual ~I2CInterfaceCollector();

		/*!
		 * @brief setChannels
		 * @return 0 if success, -1 otherwise
		 */
		int createInterfaces( void );
		/*!
		 * @brief closeChannels Delete all the I2CInterface instantiated.
		 */
		void destroyInterfaces( void );

		/*!
		 * @brief getChannel retrive the i2c channel requested
		 * @param ubNumI2CChannel
		 * @return I2CInterface,  NULL if there is a problem
		 */
		I2CInterface *getInterfaceByIndex(enumI2CInterface ubNumI2CChannel);

    private:


        I2CInterface	* m_pI2C2;
        I2CInterface	* m_pI2C3;

};

#endif // I2CCHANNEL_H
