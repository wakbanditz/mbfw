#include "MainExecutor.h"
#include <vector>
#include <stdint.h>
#include <string>

using namespace std;

MainExecutor::MainExecutor()
{
    m_bConfigIsOk = false;

    bPowerManager   = false;
    bGPIOExpSection = false;
    bLedDriver      = false;
    bFanController  = false;
    bEEprom         = false;
    bCodecAudio     = false;
    bCamera         = false;
    bSectionA       = false;
    bSectionB       = false;

}

MainExecutor::~MainExecutor()
{

}

bool MainExecutor::initConfigurator(void)
{
    char sConfigFileName[1024];
    char sError[1024];

    /* ********************************************************************************************
     * READS CONFIGURATION FILE
     * ********************************************************************************************
     */
    config_key_t config_keys[] =
    {
        { CFG_TEST_APP_LOG_ENABLE, (char*)"log.TestApp.enable", (char*)"GENERAL", T_int, (char*)"1", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_LOG_FILE, (char*)"log.TestApp.file", (char*)"GENERAL", T_string, (char*)"stdout", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_LOG_LEVEL, (char*)"log.TestApp.level", (char*)"GENERAL", T_int, (char*)"6", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_LOG_NUM_FILES, (char*)"log.TestApp.numFiles", (char*)"GENERAL", T_int, (char*)"5", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_SCT_A_ENABLE, (char*)"TestApp.sectionA.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_SCT_B_ENABLE, (char*)"TestApp.sectionB.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_WEB_ENABLE, (char*)"TestApp.webServer.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_GPIO_IF_ENABLE, (char*)"TestApp.gpioInterface.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_CAM_ENABLE, (char*)"TestApp.camera.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_POWER_MAN_ENABLE, (char*)"TestApp.powerManager.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_EEPROM_ENABLE, (char*)"TestApp.eeprom.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_FAN_CTRL_ENABLE, (char*)"TestApp.fanController.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_AUDIO_CODEC_ENABLE, (char*)"TestApp.audioCodec.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_GPIO_EXP_SEC_ENABLE, (char*)"TestApp.gpioExpanderSection.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_LED_DRV_ENABLE, (char*)"TestApp.ledDriver.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_NSH_ENABLE, (char*)"TestApp.nsh.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_STEPPER_MOTOR_ENABLE, (char*)"TestApp.stepperMotor.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_ENCODER_COUNTER_ENABLE, (char*)"TestApp.encoder.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
        { CFG_TEST_APP_GPIO_EXP_LED_ENABLE, (char*)"TestApp.gpioExpanderLed.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED}
    };
    int num_keys = sizeof(config_keys) / sizeof(config_key_t);

    strcpy(sConfigFileName, CONF_FILE);
    if(sConfigFileName[0] == 0)
    {
        printf("Configuration file name NOT valid");
        return false;
    }

    if ( !m_configurator.Init(config_keys, num_keys, sConfigFileName) )
    {
        Config::GetLastError(sError, 1024);
        printf("Impossible to read configuration file <%s> (error: %s)", sConfigFileName, sError);
        return false;
    }
    m_bConfigIsOk = true;
    return true;
}

bool MainExecutor::initLogger(void)
{
    m_pLogger = &m_logger;
    int liLogEnable = m_configurator.GetInt(CFG_TEST_APP_LOG_ENABLE);
    bool bEnable = (liLogEnable > 0 ? true : false);
    m_pLogger->enable(bEnable);
    m_pLogger->setAppName(PROGRAM_NAME);
    m_pLogger->setFile(m_configurator.GetString(CFG_TEST_APP_LOG_FILE));
    m_pLogger->setLevel(m_configurator.GetInt(CFG_TEST_APP_LOG_LEVEL));
    m_pLogger->setMaxNumFiles(m_configurator.GetInt(CFG_TEST_APP_LOG_NUM_FILES));
    return true;
}

bool MainExecutor::initGpioInterface(void)
{
    if ( ! m_bConfigIsOk )
    {
        log(LOG_ERR, "MainExec::initGpioInterface: fw not configured");
        return false;
    }

    string strEnabled(m_configurator.GetString(CFG_TEST_APP_GPIO_IF_ENABLE));
    if ( strEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::initGpioInterface: GPIOs not enabled");
        return true;
    }

    m_gpioInterface.setLogger(getLogger());
    if( ! m_gpioInterface.init(CONF_FILE) )
    {
        log(LOG_ERR, "MainExec::initGpioInterface: error, unable to initialize camera gpioInterface");
        return false;
    }
    log(LOG_INFO, "MainExec::initGpioInterface: done");
    return true;
}

void MainExecutor::closeGpioInterface(void)
{
    if ( ! m_bConfigIsOk )
    {
        log(LOG_ERR, "MainExec::closeGpioInterface: fw not configured");
        return;
    }
    string strEnabled(m_configurator.GetString(CFG_TEST_APP_GPIO_IF_ENABLE));
    if ( strEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::closeGpioInterface: GPIOs not enabled");
        return;
    }
    m_gpioInterface.closeInterface();
}

bool MainExecutor::initI2CPeripherals( void )
{

    if ( ! m_bConfigIsOk )
    {
        log(LOG_ERR, "MainExec::initI2CPeripherals: fw not configured");
        return false;
    }

    /* ****************************************************************************************************************
     * Set logger in I2C board
     * ****************************************************************************************************************
     */
    m_i2cBoard.setLogger(getLogger());

    /* ****************************************************************************************************************
     * I2C interfaces initialization
     * ****************************************************************************************************************
     */
    if ( m_i2cBoard.createI2CInterfaces() != 0 )
    {
        log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize I2C channels");
        return false;
    }

    /* ****************************************************************************************************************
     * I2C devices initialization
     * ****************************************************************************************************************
     */
    // Power Manager
    string strPMEnabled(m_configurator.GetString(CFG_TEST_APP_POWER_MAN_ENABLE));
    if ( strPMEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::initI2CPeripherals: Power Manager not enabled");
    }
    else
    {
        if ( m_i2cBoard.createAndInitPowerManager() != 0 )
        {
            log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize PowerManager");
            return false;
        }
        log(LOG_INFO, "MainExec::initI2CPeripherals: Power Manager enabled");
        bPowerManager = true;
    }

    // GPIO Expander SECTION
    string strGEEnabled(m_configurator.GetString(CFG_TEST_APP_GPIO_EXP_SEC_ENABLE));
    if (strGEEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::initI2CPeripherals: GPIO Expander not enabled");
    }
    else
    {

            if ( m_i2cBoard.createAndInitGpioExpanderA() != 0 )
            {
                log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize GPIO Expander A");
                return false;
            }
            log(LOG_INFO, "MainExec::initI2CPeripherals: GPIO Expander A enabled");

            if ( m_i2cBoard.createAndInitGpioExpanderB() != 0 )
            {
                log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize GPIO Expander B");
                return false;
            }
            log(LOG_INFO, "MainExec::initI2CPeripherals: GPIO Expander B enabled");
            bGPIOExpSection = true;
    }

    // Status Led Driver
    string strLedDriverEnabled(m_configurator.GetString(CFG_TEST_APP_LED_DRV_ENABLE));
    if (strLedDriverEnabled.compare("true") != 0)
    {
        log(LOG_INFO,"MainExec::initI2CPeripherals: LED DRIVER not enabled");
    }
    else
    {
        bool bGEExpLedEnabled = false;
        // GPIO Expander LED
        string strGELEDEnabled(m_configurator.GetString(CFG_TEST_APP_GPIO_EXP_LED_ENABLE));
        if (strGELEDEnabled.compare("true") != 0 )
        {
            log(LOG_INFO, "MainExec::initI2CPeripherals: GPIO Expander LED not enabled");
        }
        else
        {
            if ( m_i2cBoard.createAndInitGpioExpanderLed() != 0 )
            {
                log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize GPIO Expander LED");
                return false;
            }
            bGEExpLedEnabled = true;
            log(LOG_INFO, "MainExec::initI2CPeripherals: GPIO Expander LED enabled");

        }

        if ( m_i2cBoard.createLedDriver(bGEExpLedEnabled) != 0 )
        {
            log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize Led Driver");
            return false;
        }
        log(LOG_INFO, "MainExec::initI2CPeripherals: Led Driver enabled");
        bLedDriver = true;
    }

    // Fan Controller
    string strFanControllerEnabled(m_configurator.GetString(CFG_TEST_APP_FAN_CTRL_ENABLE));
    if (strFanControllerEnabled.compare("true") != 0)
    {
        log(LOG_INFO,"MainExec::initI2CPeripherals: FAN CONTROLLER not enabled");
    }
    else
    {
        if ( m_i2cBoard.createAndInitFanController() != 0 )
        {
            log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize Fan Controller");
            return false;
        }
        log(LOG_INFO, "MainExec::initI2CPeripherals: Fan Controller enabled");
        bFanController = true;
    }

    // EEprom
    string strEEEnabled(m_configurator.GetString(CFG_TEST_APP_EEPROM_ENABLE));
    if ( strEEEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::initI2CPeripherals: EEprom not enabled");
    }
    else
    {
        if ( m_i2cBoard.createAndInitEEprom() != 0 )
        {
            log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize EEprom");
            return false;
        }
        log(LOG_INFO, "MainExec::initI2CPeripherals: EEprom enabled");
        bEEprom = true;
    }

    // Codec AUDIO
    string strCAudioEnabled(m_configurator.GetString(CFG_TEST_APP_AUDIO_CODEC_ENABLE));
    if ( strCAudioEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::initI2CPeripherals: Audio not enabled");
    }
    else
    {
        if ( m_i2cBoard.createAndInitCodecAudio() != 0 )
        {
            log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize Audio");
            return false;
        }
        log(LOG_INFO, "MainExec::initI2CPeripherals: Audio enabled");
        bCodecAudio = true;
    }

    log(LOG_INFO, "MainExec::initI2CPeripherals: done");
    return true;
}

void MainExecutor::stopI2CPeripherals(void)
{
    if ( ! m_bConfigIsOk )
    {
        log(LOG_ERR, "MainExec::initI2CPeripherals: fw not configured");
        return;
    }

    // Power Manager
    string strPMEnabled(m_configurator.GetString(CFG_TEST_APP_POWER_MAN_ENABLE));
    if ( strPMEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::stopI2CPeripherals: Power Manager not enabled");
    }
    else
    {
        // Close here PowerManager
        m_i2cBoard.destroyPowerManager();
        log(LOG_INFO, "MainExec::stopI2CPeripherals: Power Manager correctly closed");
    }

    //GPIO Expander
    string strGEEnabled(m_configurator.GetString(CFG_TEST_APP_GPIO_EXP_SEC_ENABLE));
    if (strGEEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::stopI2CPeripherals: GPIO Expander not enabled");
    }
    else
    {
        m_i2cBoard.destroyGpioExpanderA();
        log(LOG_INFO, "MainExec::stopI2CPeripherals: GPIO Expander correctly closed");

    }

    // Led Driver
    string strLedDriverEnabled(m_configurator.GetString(CFG_TEST_APP_LED_DRV_ENABLE));
    if (strLedDriverEnabled.compare("true") != 0)
    {
        log(LOG_INFO,"MainExec::stopI2CPeripherals: LED DRIVER not enabled");
    }
    else
    {
        m_i2cBoard.destroyLedDriver();
        log(LOG_INFO, "MainExec::stopI2CPeripherals: LED Driver correctly closed");
    }

    // Fan Controller
    string strFanControllerEnabled(m_configurator.GetString(CFG_TEST_APP_FAN_CTRL_ENABLE));
    if (strFanControllerEnabled.compare("true") != 0)
    {
        log(LOG_INFO,"MainExec::stopI2CPeripherals: FAN CONTROLLER not enabled");
    }
    else
    {
        m_i2cBoard.destroyFanController();
        log(LOG_INFO, "MainExec::stopI2CPeripherals: FAN CONTROLLER correctly closed");
    }

    // EEprom
    string strEEEnabled(m_configurator.GetString(CFG_TEST_APP_EEPROM_ENABLE));
    if ( strEEEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::stopI2CPeripherals: EEprom not enabled");
    }
    else
    {
        m_i2cBoard.destroyEeprom();
        log(LOG_INFO, "MainExec::stopI2CPeripherals: EEprom correctly closed");
    }

    // CodecAUDIO
    string strCAudioEnabled(m_configurator.GetString(CFG_TEST_APP_AUDIO_CODEC_ENABLE));
    if ( strCAudioEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::stopI2CPeripherals: Audio not enabled");
    }
    else
    {
        m_i2cBoard.destroyCodecAudio();
        log(LOG_INFO, "MainExec::stopI2CPeripherals: Audio correctly closed");
    }

    //GPIO Expander
    string strGELEDEnabled(m_configurator.GetString(CFG_TEST_APP_GPIO_EXP_LED_ENABLE));
    if (strGELEDEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::stopI2CPeripherals: GPIO Expander LED not enabled");
    }
    else
    {
        m_i2cBoard.destroyGpioExpanderLed();
        log(LOG_INFO, "MainExec::stopI2CPeripherals: GPIO Expander LED correctly closed");

    }

    // I2C Interfaces
     m_i2cBoard.destroyI2CInterfaces();

}

bool MainExecutor::initSPIPeripherals(void)
{
    if ( ! m_bConfigIsOk )
    {
        log(LOG_ERR, "MainExec::initSPIPeripherals: fw not configured");
        return false;
    }

    string strNSHREnabled(m_configurator.GetString(CFG_TEST_APP_NSH_ENABLE));
    string strNSHMEnabled(m_configurator.GetString(CFG_TEST_APP_STEPPER_MOTOR_ENABLE));
    string strNSHEEnabled(m_configurator.GetString(CFG_TEST_APP_ENCODER_COUNTER_ENABLE));

    int ucDeviceEnabled = 0;

    if ( ! strNSHREnabled.compare("true") )		ucDeviceEnabled |= 1;
    if ( ! strNSHEEnabled.compare("true") )		ucDeviceEnabled |= (1 << 1);
    if ( ! strNSHMEnabled.compare("true") )		ucDeviceEnabled |= (1 << 2);

    if ( ucDeviceEnabled )
    {
        // GPIO interface is needed!
        string strEnabled(m_configurator.GetString(CFG_TEST_APP_GPIO_IF_ENABLE));
        if ( strEnabled.compare("true") != 0 )
        {
            log(LOG_ERR, "MainExec::initSPIPeripherals: gpio interface not initialized");
            return false;
        }

        m_SPIBoard.setLogger(getLogger());
        if ( m_SPIBoard.createAndInitSPIInterfaces() != 0 )
        {
            log(LOG_ERR, "MainExec::initSPIPeripherals: error, unable to initialize SPI channels");
            return false;
        }
    }
    else
    {
        log(LOG_INFO, "MainExec::initSPIPeripherals: no SPI peripheral is enabled");
        return true;
    }

    if ( ucDeviceEnabled & 1 )
    {
        if ( m_SPIBoard.createAndInitNSHReader(&m_gpioInterface) != 0 )
        {
            log(LOG_ERR, "MainExec::initSPIPeripherals: error, unable to initialize NSH reader");
            return false;
        }
        log(LOG_INFO, "MainExec::initSPIPeripherals: NSH Reader enabled");
    }
    else
    {
        log(LOG_INFO, "MainExec::initSPIPeripherals: NSH reader not enabled");
    }

    if ( ucDeviceEnabled & (1 << 1) )
    {
        if ( m_SPIBoard.createAndInitNSHEncoder() != 0 )
        {
            log(LOG_ERR, "MainExec::initSPIPeripherals: error, unable to initialize NSH encoder");
            return false;
        }
        log(LOG_INFO, "MainExec::initSPIPeripherals: NSH encoder enabled");
    }
    else
    {
        log(LOG_INFO, "MainExec::initSPIPeripherals: NSH encoder not enabled");
    }

    if ( ucDeviceEnabled & (1 << 2) )
    {
        if ( m_SPIBoard.createAndInitNSHMotor(&m_gpioInterface) != 0 )
        {
            log(LOG_ERR, "MainExec::initSPIPeripherals: error, unable to initialize NSH motor");
            return false;
        }
        log(LOG_INFO, "MainExec::initSPIPeripherals: NSH motor enabled");
    }
    else
    {
        log(LOG_INFO, "MainExec::initSPIPeripherals: NSH motor not enabled");
    }

    log(LOG_INFO, "MainExec::initSPIPeripherals: done");
    return true;
}

void MainExecutor::stopSPIPeripherals(void)
{
    if ( ! m_bConfigIsOk )
    {
        log(LOG_ERR, "MainExec::stopSPIPeripherals: fw not configured");
        return;
    }

    string strNSHREnabled(m_configurator.GetString(CFG_TEST_APP_NSH_ENABLE));
    if ( strNSHREnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH reader not enabled");
    }
    else
    {
        m_SPIBoard.destroyNSHReader();
        log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH reader correctly closed");
    }

    string strNSHMEnabled(m_configurator.GetString(CFG_TEST_APP_STEPPER_MOTOR_ENABLE));
    if ( strNSHMEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH motor not enabled");
    }
    else
    {
        m_SPIBoard.destroyNSHMotor();
        log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH motor correctly closed");
    }

    string strNSHEEnabled(m_configurator.GetString(CFG_TEST_APP_ENCODER_COUNTER_ENABLE));
    if ( strNSHEEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH encoder not enabled");
    }
    else
    {
        m_SPIBoard.destroyNSHEncoder();
        log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH encoder correctly closed");
    }

    // SPI Interfaces
     m_SPIBoard.destroySPIInterfaces();
     log(LOG_INFO, "MainExec::stopSPIPeripherals: done.");
}

bool MainExecutor::initUSBPeripherals(void)
{
    if ( ! m_bConfigIsOk )
    {
        log(LOG_ERR, "MainExec::initUSBPeripherals: fw not configured");
        return false;
    }

    /* ****************************************************************************************************************
     * Set logger in USB board
     * ****************************************************************************************************************
     */
    m_USBBoard.setLogger(getLogger());

    /* ****************************************************************************************************************
     * USB interfaces initialization
     * ****************************************************************************************************************
     */
    if ( m_USBBoard.createAndInitUSBInterfaces() != 0 )
    {
        log(LOG_ERR, "MainExec::initUSBPeripherals: error, unable to create USB interface");
        return false;
    }

    /* ****************************************************************************************************************
     * Camera initialization
     * ****************************************************************************************************************
     */
    string strCamEnabled(m_configurator.GetString(CFG_TEST_APP_CAM_ENABLE));
    if ( strCamEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::initUSBPeripherals: camera not enabled");
        return true;
    }
    else
    {
        if ( m_USBBoard.createAndInitCameraReader() != 0 )
        {
            log(LOG_ERR, "MainExec::initUSBPeripherals: unable to initialize Camera device");
            return false;
        }
        log(LOG_INFO, "MainExec::initUSBPeripherals: camera enabled");
        bCamera = true;
    }

    return true;
}

void MainExecutor::stopUSBPeripherals(void)
{
    if ( ! m_bConfigIsOk )
    {
        log(LOG_ERR, "MainExec::stopUSBPeripherals: fw not configured");
        return;
    }

    string strEnabled(m_configurator.GetString(CFG_TEST_APP_CAM_ENABLE));
    if ( strEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::stopUSBPeripherals: camera not enabled");
        return;
    }
    else
    {
        m_USBBoard.destroyCameraReader();
        log(LOG_INFO, "MainExec::stopUSBPeripherals: camera correctly closed");
    }

    m_USBBoard.destroyUSBInterfaces();
    log(LOG_INFO, "MainExec::stopUSBPeripherals: done.");
}

bool MainExecutor::initCANPeripherals(void)
{
    if ( ! m_bConfigIsOk )
    {
        log(LOG_ERR, "MainExec::initCANPeripherals: fw not configured");
        return false;
    }

    m_CANLinkBoard.setLogger(getLogger());

    //Section A
    string strSecAEnabled(m_configurator.GetString(CFG_TEST_APP_SCT_A_ENABLE));
    if ( strSecAEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::initCANPeripherals--> section A not enabled");
        return false;
    }
    else
    {
        bSectionA = true;
        log(LOG_DEBUG, "MainExec::initCANPeripherals--> SecA enabled ");
    }

    //Section B
    string strSecBEnabled(m_configurator.GetString(CFG_TEST_APP_SCT_B_ENABLE));
    if ( strSecBEnabled.compare("true") != 0 )
    {
        log(LOG_INFO, "MainExec::initCANPeripherals--> section B not enabled");
        return false;
    }
    else
    {
        bSectionB = true;
        log(LOG_DEBUG, "MainExec::initCANPeripherals--> SecB enabled ");
    }

    int res = m_CANLinkBoard.initCanLinkBoard(bSectionA, bSectionB);
    if (res != 0)
    {
        log(LOG_ERR, "MainExec::initCANPeripherals-->error init CanLinkBoard");
        return false;
    }

    if (!m_CANLinkBoard.startThread())
    {
        log(LOG_ERR, "MainExec::initCANPeripherals-->error start thread");
        return false;
    }

    uint8_t ubstato;
    m_CANLinkBoard.sectionA->getInternalStatus(ubstato);
    printf("%i", ubstato);
    return true;
}

void MainExecutor::stopCANPeripherals()
{
    if (bSectionA || bSectionB)
    {
        int res = m_CANLinkBoard.stopCanLinkBoard();
        if (res != 0)
        {
            log(LOG_ERR, "MainExec::stopCANPeripherals-->error stop CanLinkBoard");
        }
        log(LOG_INFO, "MainExec::stopCANPeripherals-->stop CAN Peripheral Link");
    }
}

bool MainExecutor::isPowerManagerEnabled()
{
    return bPowerManager;
}

bool MainExecutor::isGPIOExpSectionEnabled()
{
    return bGPIOExpSection;
}

bool MainExecutor::isLedDriverEnabled()
{
    return bLedDriver;
}

bool MainExecutor::isFanControllerEnabled()
{
    return bFanController;
}

bool MainExecutor::isCodecAudioEnabled()
{
    return bCodecAudio;
}

bool MainExecutor::isEEPromEnabled()
{
    return bEEprom;
}

bool MainExecutor::isCameraEnabled()
{
    return bCamera;
}

bool MainExecutor::isSectionAEnabled()
{
    return bSectionA;
}

bool MainExecutor::isSectionBEnabled()
{
    return bSectionB;
}
