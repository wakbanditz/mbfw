#ifndef SECTIONTEST_H
#define SECTIONTEST_H

#include "TestManagerBase.h"

class SectionTest : public TestManagerBase
{
    public:
        SectionTest();

        virtual ~SectionTest();

        int setSection(uint8_t ubVal);

        int actionCmd(uint8_t ubCmd);

    private:
         uint8_t m_ubSection;
};

#endif // SECTIONTEST_H
