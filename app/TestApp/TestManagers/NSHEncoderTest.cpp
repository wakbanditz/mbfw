#include "NSHEncoderTest.h"
#include "MainExecutor.h"

NSHEncoderTest::NSHEncoderTest()
{
    vecMenu.clear();

    vecMenu.push_back("a - Read counter\n");
    vecMenu.push_back("b - Reset counter\n");
    vecMenu.push_back("c - Enable counter\n");
    vecMenu.push_back("d - Disable counter\n");
    vecMenu.push_back("e - Load value to counter register\n");
    vecMenu.push_back("f - Get status\n");
    vecMenu.push_back("g - Monitore dynamically the counter\n\n");
}

NSHEncoderTest::~NSHEncoderTest()
{

}

int NSHEncoderTest::actionCmd(uint8_t ubCmd)
{
    int liVal;
    uSTRReg uEncoderStatus;
//    SPITestThread thread;
    switch (ubCmd)
    {
        case 'a':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->readCounter(liVal);
            printf("Value Read: %d.\n", liVal);
        break;

        case 'b':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->resetCounter();
            printf("Done.\n");
        break;

        case 'c':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->enableCounter();
            printf("Done.\n");
        break;

        case 'd':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->disableCounter();
            printf("Done.\n");
        break;

        case 'e':
            printf("Please insert the value to be loaded.\n");
            cin >> liVal;
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->loadCounterWithValue(liVal);
            printf("Done.\n");
        break;

        case 'f':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->getStatus(uEncoderStatus);
            printf("Done.\n");
        break;

//        case 'g':
//            printf("Press ENTER if you want to exit this test.\n");
//            if ( !thread.startThread() )
//            {
//                printf("Impossible to start thread\n"); fflush(stdout);
//                break;
//            }

//            while ( thread.isRunning() )
//            {
//                MainExecutor::getInstance()->m_SPIBoard.m_pNSHEncoder->readCounter(liVal);
//                printf("\rValue Read: %d.", liVal); fflush(stdout);
//            }
//            thread.stopThread();
//            printf("\nKey pressed.\n");
//        break;

        default:
            printf("Wrong key pressed. I'm sorry.\n");
            return -1;
        break;
    }
    return 0;
}
