INCLUDEPATH += $$PWD/TestManagers/

SOURCES += \
        $$PWD/CameraTest.cpp \
        $$PWD/CodecAudioTest.cpp \
        $$PWD/EEpromTest.cpp \
        $$PWD/FanControllerTest.cpp \
        $$PWD/GPIOExpSectionTest.cpp \
        $$PWD/LedDriverTest.cpp \
        $$PWD/NSHEncoderTest.cpp \
        $$PWD/NSHMotorTest.cpp \
        $$PWD/NSHReaderTest.cpp \
        $$PWD/PowerManagerTest.cpp \
        $$PWD/SectionTest.cpp \
        $$PWD/SelectorTest.cpp \
        $$PWD/TestManagerBase.cpp

HEADERS += \
    $$PWD/CameraTest.h \
    $$PWD/CodecAudioTest.h \
    $$PWD/EEpromTest.h \
    $$PWD/FanControllerTest.h \
    $$PWD/GPIOExpSectionTest.h \
    $$PWD/LedDriverTest.h \
    $$PWD/NSHEncoderTest.h \
    $$PWD/NSHMotorTest.h \
    $$PWD/NSHReaderTest.h \
    $$PWD/PowerManagerTest.h \
    $$PWD/SectionTest.h \
    $$PWD/SelectorTest.h \
    $$PWD/TestManagerBase.h
