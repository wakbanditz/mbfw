#include "CodecAudioTest.h"
#include "MainExecutor.h"
#include "TLV320aic31xx.h"

CodecAudioTest::CodecAudioTest()
{
    vecMenu.clear();

    vecMenu.push_back("a - read Vol\n" );
    vecMenu.push_back("b - set Vol\n" );
    vecMenu.push_back("c - Play Audio\n");
    vecMenu.push_back("d - range Control\n");
    vecMenu.push_back("x - Exit\n" );
}

CodecAudioTest::~CodecAudioTest()
{

}

int CodecAudioTest::actionCmd(uint8_t ubCmd)
{
    long	lVolume = 0;
    long	lmin, lmax;
    uint8_t ubVol = 0;

    switch(ubCmd)
    {
        case 'a':
            MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->getSpeakerAnalogVolume(lVolume);
            log(LOG_INFO,"Speaker Analog VolumeT:%d", lVolume );
            break;

        case 'b':
            printf("SET SPEAKER Vol[0-127]:\n");
            decodeCmd();
            ubVol = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->setSpeakerAnalogVolume(ubVol);
            log(LOG_INFO,"Set Speaker Analog VolumeT:%d", ubVol );
            break;

        case 'c':
            printf("Play Audio\n");
            system("gst-play-1.0 /testfiles/Ring2.wav &");
            break;

        case 'd':
            MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->getSpeakerRange(MainExecutor::getInstance()->m_i2cBoard.m_pCodecAudio->eTLV320_ANVOL,lmin,lmax);
            log(LOG_INFO, "Lim Max:%d, Lim Min: %d", lmax, lmin);
            break;

        case 'x':
        default:
            return -1;
            break;
    }

    return 0;
}
