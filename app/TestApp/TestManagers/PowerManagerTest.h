#ifndef POWERMANAGERTEST_H
#define POWERMANAGERTEST_H

#include "TestManagerBase.h"
#include "CommonInclude.h"

class PowerManagerTest : public TestManagerBase
{
    public:
        PowerManagerTest();

        virtual ~PowerManagerTest();

        int actionCmd(uint8_t ubCmd);
};

#endif // POWERMANAGERTEST_H
