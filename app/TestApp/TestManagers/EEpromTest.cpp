#include "EEpromTest.h"
#include "MainExecutor.h"
#include "AT24C256C.h"


EEpromTest::EEpromTest()
{
    vecMenu.clear();

    vecMenu.push_back("a - Write one word(8 bit)\n" );
    vecMenu.push_back("b - Read one word (8 bit)\n" );
    vecMenu.push_back("c - Write the intro of the Divina Commedia\n");
    vecMenu.push_back("d - Read the intro of the Divina Commedia\n");
    vecMenu.push_back("e - Write\n");
    vecMenu.push_back("f - Read\n");
    vecMenu.push_back("g - Reset to default\n\n");
    vecMenu.push_back("x - Exit \n");
}

EEpromTest::~EEpromTest()
{

}

int EEpromTest::actionCmd(uint8_t ubCmd)
{
    uint16_t usStartAddress = 0x00;
    string strTxMsg("Nel mezzo del cammin di nostra vita "
               "mi ritrovai per una selva oscura, "
               "ché la diritta via era smarrita.\n"
               "Ahi quanto a dir qual era è cosa dura "
               "esta selva selvaggia e aspra e forte "
               "che nel pensier rinova la paura!\n"
               "Tant' è amara che poco è più morte; "
               "ma per trattar del ben ch'i' vi trovai, "
               "dirò de l'altre cose ch'i' v'ho scorte.\n"
               "Io non so ben ridir com' i' v'intrai, "
               "tant' era pien di sonno a quel punto "
               "che la verace via abbandonai.\n");

    uint16_t usMsgLen = strTxMsg.size();
    uint8_t rgRxBuff[500] = {0};
    uint8_t cWord;
    int8_t cRes;

    switch(ubCmd)
    {

        case 'a':
            printf("Insert the word: ");
            cin >> cWord;
            printf("\n");
            cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->writeWord(0x00, cWord);
            if ( cRes )
            {
                printf("\nUnable to write to EEPROM\n");
            }
        break;

        case 'b':
            cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->readWord(0x00, cWord);
            if ( cRes )
            {
                printf("\nUnable to read from EEPROM\n");
            }
            printf("Word read is: %c\n", cWord);
        break;

        case 'c':
            cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->write(usStartAddress, (uint8_t*)strTxMsg.c_str(), usMsgLen);
            if ( cRes )
            {
                printf("\nUnable to write to EEPROM\n");
            }
            printf("\n\nMessage written:\n");
            for (auto c : strTxMsg)
            {
                printf("%c", c);fflush(stdout);
            }

        break;

        case 'd':
            cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->read(usStartAddress, rgRxBuff, usMsgLen);
            if ( cRes )
            {
                printf("\nUnable to read from EEPROM\n");
            }
            printf("\n");
            for (auto c : rgRxBuff)
            {
                printf("%c", c);fflush(stdout);
            }
        break;

        case 'e':
        {
            printf("Insert starting address\n");
            int liAddr;
            cin >> liAddr;
            printf("Insert string\n");
            string strVal;
            cin >> strVal;
            int liSize = strVal.size();

            cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->write(liAddr, (uint8_t*)strVal.c_str(), liSize);
            if ( cRes )
            {
                printf("\nUnable to write %s to addr %d\n", strVal.c_str(), liAddr);
            }
            printf("\n\nMessage written:\n");
            for (auto c : strVal)
            {
                printf("%c", c);fflush(stdout);
            }
        }
        break;

        case 'f':
        {
            printf("Insert starting address\n");
            int liAddr;
            cin >> liAddr;
            printf("Insert num of bytes to be read\n");
            int liVal;
            cin >> liVal;
            string strVal("");
            strVal.resize(liVal);

            cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->read(liAddr, (uint8_t*)strVal.c_str(), liVal);
            if ( cRes )
            {
                printf("\nUnable to write %s to addr %d\n", strVal.c_str(), liAddr);
            }
            printf("\n");
            for (auto c : strVal)
            {
                printf("%c", c);fflush(stdout);
            }
        }
        break;

        case 'g':
            cRes = MainExecutor::getInstance()->m_i2cBoard.m_pEeprom->resetEeprom();
            if ( cRes )
            {
                printf("\nUnable to read from EEPROM\n");
            }
            else
            {
                printf("\nEEPROM reset with success\n");
            }
                break;

        default:
            printf("Wrong key pressed. I'm sorry.\n");
            return -1;
        break;
    }

    return 0;
}
