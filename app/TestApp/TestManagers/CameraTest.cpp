#include "CameraTest.h"
#include "MainExecutor.h"

CameraTest::CameraTest()
{
    vecMenu.clear();

    // General commands
    vecMenu.push_back("a - Set Contrast Window\n");
    vecMenu.push_back("b - Set AGC Parameters Manually\n");
    vecMenu.push_back("c - Enable or Disable AGC Log\n");
    vecMenu.push_back("d - Get AGC Log\n");
    vecMenu.push_back("e - Clear AGC Log\n");

    // Camera commands
    vecMenu.push_back("f - Get Firmware Version\n");
    vecMenu.push_back("g - Set Picture FOI\n");
    vecMenu.push_back("h - Reconfigure AGC Curves\n");
    vecMenu.push_back("i - Take Picture and Save it\n");

    // Reader commands
    vecMenu.push_back("j - Set decoding timeout\n");
    vecMenu.push_back("k - Set ROI\n");
    vecMenu.push_back("l - Get Linear Barcode\n");
    vecMenu.push_back("m - Get Data Matrix\n");

    // Reset
    vecMenu.push_back("n - Factory Reset\n\n");
}

CameraTest::~CameraTest()
{

}

int CameraTest::actionCmd(uint8_t ubCmd)
{

    uint32_t sParam[4];
    uint32_t rgliTable[AGC_TABLE_SIZE];
    vector<logAGCparameters> vAGCParams;
    vector<unsigned char> vcImage;
    string str;
    char foo;

    switch (ubCmd)
    {
        case 'a':
            printf("\nPlease insert x0 coordinate of contrast window.\n");
            scanf("%d", &sParam[0]);
            printf("Please insert y0 coordinate of contrast window.\n");
            scanf("%d", &sParam[1]);
            printf("Please insert contrast window's width.\n");
            scanf("%d", &sParam[2]);
            printf("Please insert contrast window's height.\n");
            scanf("%d", &sParam[3]);

            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setContrastWindow(sParam[0], sParam[1], sParam[2], sParam[3]) == ERR_CR8062_IN_PARAMETERS )
            {
                printf("\nWRONG INPUT VALUE!\n");
                break;
            }
            printf("\nContrast window correctly set!\n");
        break;

        case 'b':
            printf("\nPlease insert illumination value (1 to 100).\n");
            scanf("%d", &sParam[0]);
            printf("Please insert exposition time value (1 to 70000).\n");
            scanf("%d", &sParam[1]);
            printf("Please insert gain value (1 to 100).\n");
            scanf("%d", &sParam[2]);

            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setAGCparam(sParam[0], sParam[1], sParam[2]) == ERR_CR8062_IN_PARAMETERS )
            {
                printf("\nWRONG INPUT VALUE!\n");
                break;
            }
            printf("\nContrast window correctly set!\n");
            MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setAGCnormalMode(); // needed to set the AGC in MONO
        break;

        case 'c':
            printf("\nPress 1 to enable logging, or the other keys to disable it.\n");
            scanf("%d", &sParam[0]);
            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.enableLogAGCparameters(sParam[0]) == ERR_USB_PERIPH_COMMUNICATION )
            {
                printf("\nUSB communication error!\n");
                break;
            }
            ( sParam[0] == 1 ) ? printf("\nLog Enabled.\n") : printf("\nLog Disabled.\n");

        break;

        case 'd':
            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.getLogAGCparameters(vAGCParams) == ERR_USB_PERIPH_COMMUNICATION )
            {
                printf("\nUSB communication error!\n");
                break;
            }
        break;

        case 'e':
            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.clearLogAGCparameters() == ERR_USB_PERIPH_COMMUNICATION )
            {
                printf("\nUSB communication error!\n");
                break;
            }
            printf("\nLOG cleared.\n");
        break;

        case 'f':
            if (  MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.getFWversion(str) == ERR_USB_PERIPH_COMMUNICATION )
            {
                printf("\nUSB communication error!\n");
                break;
            }
        break;

        case 'g':
            printf("\nPress 0 for HD fiels, 1 for Wide Field, 2 for entire image.\n");
            scanf("%d", &sParam[0]);

            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setPictureFOI(sParam[0]) == ERR_CR8062_IN_PARAMETERS )
            {
                printf("\nWRONG INPUT VALUE!\n");
                break;
            }
            printf("\nFOI correctly set.\n");
        break;

        case 'h':
            printf("\nPress 0 to avoid inserting all the values and use default, otherwise all the other keys are accepted.\n");
            scanf(" %c", &foo);
            if ( foo == '0' )
            {
                if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.reconfigureAGCCurves(NULL, 0) == ERR_USB_PERIPH_COMMUNICATION )
                {
                    printf("\nUSB communication error!!\n");
                    break;
                }
                printf("\nAGC correctly reconfigured.\n");
                break;
            }
            printf("\nPlease insert exposure time @0 (1 to 70000).\n");
            scanf("%d", &rgliTable[0]);
            printf("Please insert exposure time @20 (1 to 70000).\n");
            scanf("%d", &rgliTable[1]);
            printf("Please insert exposure time @40 (1 to 70000).\n");
            scanf("%d", &rgliTable[2]);
            printf("Please insert exposure time @60 (1 to 70000).\n");
            scanf("%d", &rgliTable[3]);
            printf("Please insert exposure time @80 (1 to 70000).\n");
            scanf("%d", &rgliTable[4]);
            printf("Please insert exposure time @100 (1 to 70000).\n");
            scanf("%d", &rgliTable[5]);
            printf("Please insert gain @0 (1 to 100).\n");
            scanf("%d", &rgliTable[6]);
            printf("Please insert gain @20 (1 to 100).\n");
            scanf("%d", &rgliTable[7]);
            printf("Please insert gain @40 (1 to 100).\n");
            scanf("%d", &rgliTable[8]);
            printf("Please insert gain @60 (1 to 100).\n");
            scanf("%d", &rgliTable[9]);
            printf("Please insert gain @80 (1 to 100).\n");
            scanf("%d", &rgliTable[10]);
            printf("Please insert gain @100 (1 to 100).\n");
            scanf("%d", &rgliTable[11]);
            printf("Please insert illumination @0 (1 to 100).\n");
            scanf("%d", &rgliTable[12]);
            printf("Please insert illumination @20 (1 to 100).\n");
            scanf("%d", &rgliTable[13]);
            printf("Please insert illumination @40 (1 to 100).\n");
            scanf("%d", &rgliTable[14]);
            printf("Please insert illumination @60 (1 to 100).\n");
            scanf("%d", &rgliTable[15]);
            printf("Please insert illumination @80 (1 to 100).\n");
            scanf("%d", &rgliTable[16]);
            printf("Please insert illumination @100 (1 to 100).\n");
            scanf("%d", &rgliTable[17]);

            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.reconfigureAGCCurves(rgliTable, 0) == ERR_USB_PERIPH_COMMUNICATION )
            {
                printf("\nUSB communication error!!\n");
                break;
            }
            printf("\nAGC correctly reconfigured.\n");
        break;

        case 'i':
        {
            printf("\nPlease insert x0.\n");
            scanf("%d", &sParam[0]);
            printf("Please insert y0.\n");
            scanf("%d", &sParam[1]);
            printf("Please insert image's width.\n");
            scanf("%d", &sParam[2]);
            printf("Please insert image's height.\n");
            scanf("%d", &sParam[3]);

            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setCropCoordinates(sParam[0], sParam[1], sParam[2], sParam[3]) == ERR_CR8062_IN_PARAMETERS )
            {
                printf("\nUnable to set crop coordinates, error!!\n");
                break;
            }

            structInfoImage InfoImage;
            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.getPicture(vcImage, &InfoImage) == ERR_CR8062_IN_PARAMETERS )
            {
                printf("\nWRONG INPUT VALUE!\n");
                break;
            }

            if ( vcImage.empty() )
            {
                printf("Unable to get picture.\n");
                break;
            }

            printf("\nSave picture as:\n");
            cin >> str;

            // Save image
            MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.saveBMPimage(vcImage.data(), sParam[2]*sParam[3], sParam[2], sParam[3], str);
            vcImage.clear();
            printf("\nPicture saved with success.\n");
        }
        break;

        case 'j':
            printf("\nPlease insert decoding time (ms).\n");
            scanf("%d", &sParam[0]);

            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Reader.setDecodingTimeMsec(sParam[0]) == ERR_CR8062_IN_PARAMETERS )
            {
                printf("\nWRONG INPUT VALUE!\n");
                break;
            }
            printf("\nDecoding timeout correctly set!\n");
        break;

        case 'k':
            printf("\nPlease insert ROI x0.\n");
            scanf("%d", &sParam[0]);
            printf("Please insert ROI y0.\n");
            scanf("%d", &sParam[1]);
            printf("Please insert ROI's width.\n");
            scanf("%d", &sParam[2]);
            printf("Please insert ROI's height.\n");
            scanf("%d", &sParam[3]);

            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Reader.setROI(sParam[0], sParam[1], sParam[2], sParam[3]) == ERR_CR8062_IN_PARAMETERS )
            {
                printf("\nWRONG INPUT VALUE!\n");
                break;
            }
            printf("\nROI correctly set!\n");
        break;

        case 'l':
            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Reader.getLinearBarcode(str) == ERR_CR8062_READING_BARCODE )
            {
                printf("\nError reading code!\n");
                break;
            }
        break;

        case 'm':
            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Reader.getDataMatrix(str) == ERR_CR8062_READING_DATAMATRIX )
            {
                printf("\nError reading code!\n");
                break;
            }
        break;

        case 'n':

            if ( MainExecutor::getInstance()->m_USBBoard.m_pCR8062->resetCamera() == false )
            {
                printf("\nUSB communication error!\n");
                break;
            }
            printf("\nReset successful!\n");
        break;

        default:
            printf("Wrong key pressed. I'm sorry.\n");
            return -1;
        break;

    }
    return 0;
}
