#include "PowerManagerTest.h"
#include "MainExecutor.h"
#include "UCD9090a.h"

PowerManagerTest::PowerManagerTest()
{
    vecMenu.clear();
    vecMenu.push_back("a - printConf\n");
    vecMenu.push_back("b - getGpioConfig\n");
    vecMenu.push_back("c - readDeviceID\n" );
    vecMenu.push_back("d - readMonitorConfig\n");
    vecMenu.push_back("e - clear faults\n");
    vecMenu.push_back("E - clear Logged faults\n");
    vecMenu.push_back("f - setGPIO\n");
    vecMenu.push_back("g - writeSeqConfig\n");
    vecMenu.push_back("l - set&writeMonPinConfig\n");
    vecMenu.push_back("m - UserRAM\n");
    vecMenu.push_back("p - setPage \n");
    vecMenu.push_back("s - storeDefaultAll\n");
    vecMenu.push_back("r - resetSoft\n" );
    vecMenu.push_back("t - readTemperature \n");
    vecMenu.push_back("v - readVOUT\n");
    vecMenu.push_back("u - readStatusREgister\n");
    vecMenu.push_back("S - sleepMode\n");
    vecMenu.push_back("x - Exit\n" );
}

PowerManagerTest::~PowerManagerTest()
{

}

int PowerManagerTest::actionCmd(uint8_t ubCmd)
{
    string sID;

    switch (ubCmd)
    {
        case 'a':
            printf("a - printGpioConfig\n");
            printf("b - printRailConfig\n" );
            printf("c - printMonConfig\n" );
            decodeCmd();

            if (sCmd[0] == 'a')
            {
                MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->printGpioConfig();
            }
            else if (sCmd[0] == 'b')
            {
                printf("MON : 1==Voltage, 2==Temp \n" );
                MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->printRailConfig();
            }
            else if (sCmd[0] == 'c')
            {
                printf("MON : 1==Voltage, 2==Temp \n" );
                MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->printPinMonConfig();
            }
            else
            {
                printf("No print conf \n");
            }
            break;

        case 'b':

            /* GPIO CONFIG */
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->getGpioConfig();
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->printGpioConfig();

            break;

        case 'c':
            /* DEVICE ID */
            sID.clear();
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readDeviceID(sID);
            m_pLogger->log(LOG_INFO, ">>>>>Device ID: %s", sID.c_str());
            break;

        case 'd':
            /* MON CONFIG */
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readMonitorConfig();
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->printPinMonConfig();
            break;

        case 'e':
            /* CLEAR FAULTS */
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeClearFaults();
            m_pLogger->log(LOG_INFO, "RESET FAULTS done");
            break;

        case 'E':
            /* CLEAR FAULTS */
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeLoggedFaults();
            m_pLogger->log(LOG_INFO, "CLEAR LOGGED FAULTS done");
            break;

        case 'f':
            uint8_t ubNumRail;
            uint8_t	ubDir;
            uint8_t ubVal;

            /* SET GPIO*/
            printf("SelectGPIO [0-22]: ");
            decodeCmd();
            ubNumRail = atoi(sCmd);

            printf("Select Direction [1-Output 0-Input]: ");
            decodeCmd();
            ubDir = atoi(sCmd);

            printf("Select Value [0-1]: ");
            decodeCmd();
            ubVal = atoi(sCmd);


            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setGPIO(ubNumRail, (eUCD9090aGpioOutEnable)ubDir, (eUCD9090aGpioOutValue)ubVal);
            break;

        case 'g':
            uint8_t ubRail;
            stEnablePinConfig Pin;

            printf("Write SeqConfig  \n" );
            printf("Set Rail: \n");
            decodeCmd();
            ubRail = atoi(sCmd);

            printf("ID \n");
            decodeCmd();
            Pin.ubPinIDDef = atoi(sCmd);

            printf("Pol \n");
            decodeCmd();
            Pin.ubPolarity = atoi(sCmd);

            printf("Mode \n");
            decodeCmd();
            Pin.ubMode = atoi(sCmd);


            printf("Rail: %d", ubRail);
            printf("Cfg:  %d-%d-%d    \n",Pin.ubPinIDDef, Pin.ubPolarity, Pin.ubMode);

            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeSeqConfig(ubRail, Pin);
            break;

        case 'l':
            uint8_t ubNumPinId;
            uint8_t ubMonType;
            uint8_t ubPageByte;

            /* SET MON PIN CONFIG*/
            printf("Set PinID: <NumPinID>  \n" );
            decodeCmd();
            ubNumPinId = atoi(sCmd);
            printf("Set Type: <Type>  \n" );
            decodeCmd();
            ubMonType = atoi(sCmd);
            printf("Set Page: <Page>  \n" );
            decodeCmd();
            ubPageByte = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPinMonitorConfigRAM(ubNumPinId, ubMonType, ubPageByte);

            /* WRITE MON PIN CONFIG */
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writePinMonitorConfig();
            break;

        case 'm':
            /* USER RAM */
            printf("USER RAM [r/w]: " );
            decodeCmd();
            if (sCmd[0] == 'w')
            {
                MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeUserRAM(0x01);
            }
            else
            {
                uint8_t ubVal;
                MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readUserRAM(ubVal);
                m_pLogger->log(LOG_INFO, "USER RAM: %d",ubVal);

            }
            break;

        case 'p':
            uint8_t ubPage ;
            /* SET PAGE */
            printf("Select Page: " );
            decodeCmd();

            ubPage = atoi(sCmd);
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeSetPage(ubPage);
            m_pLogger->log(LOG_INFO,"Set PAGE %d", ubPage);
            break;

        case 'r':

            /* RESET SOFT */
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeResetSoft();
            m_pLogger->log(LOG_INFO,"Reset Soft done");
            break;

        case 's':
            /* STORE DEFAULT ALL */
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeStoreDefaultAll();
            m_pLogger->log(LOG_INFO,"STORE default");
            break;

        case 't':
            /* TEMP_1 */
            float fTemp;
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readTemperature(0, 1, fTemp);
            m_pLogger->log(LOG_INFO,"Read Temp1 Power Manager: %f °C", fTemp);
            break;

        case 'v':
            /* READ_VOUT */
            float fV;
            MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readVout(fV);
            m_pLogger->log(LOG_INFO,"Read Vout Power Manager: %f V", fV);
            break;

        case 'u':
            {
                uint32_t ulTemp;
                MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readStatusRegister(ulTemp);
                m_pLogger->log(LOG_INFO,"Read Status Register: 0x%X \n", ulTemp);

                //Test with GPI3-->shutdown pin
                if (ulTemp & MASK_MFR_STATUS_GPI3)
                {
                    printf("GPI3 Asserted\n");
                }
            }
            break;

        case 'S':
            {
                printf("a - Section Disable A\n");
                printf("b - Section Enable A\n");
                printf("c - Section Disable B\n");
                printf("d - Section Enable B\n");
                printf("e - Section Disable All\n");
                printf("f - Section Enable All\n");
                printf("g - NSH Off  \n");
                printf("h - NSH ON \n");
                printf("i - StatusLED/Camera Off \n");
                printf("l - StatusLED/Camera On \n");
//                printf("m - Motor Off \n");
//                printf("n - Motor On \n");
//                printf("o - Disable ALL\n");
//                printf("p - Enable ALL\n");
                printf("x - Exit \n");

                decodeCmd();

                if (sCmd[0] == 'a')
                {
                     MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, false);
                     usleep(200);
                     printf("SECTION A DISABLED \n");
                }
                else if (sCmd[0] == 'b')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, true);
                    usleep(200);
                    printf("SECTION A ENABLED \n");
                }
                else if (sCmd[0] == 'c')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, false);
                    usleep(200);
                   printf("SECTION B DISABLED \n");
                }
                else if (sCmd[0] == 'd')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, true);
                    usleep(200);
                    printf("SECTION B ENABLED \n");
                }
                else if (sCmd[0] == 'e')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, false);
                    usleep(200);
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, false);
                    usleep(200);
                    printf("SECTIONs DISABLED \n");
                }
                else if (sCmd[0] == 'f')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_14_FPWM5_GPIO9, true);
                    usleep(200);
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_27_TCK_GPIO18, true);
                    usleep(200);
                    printf("SECTIONs ENABLED \n");
                }
                else if (sCmd[0] == 'g')
                {
                    printf("RAIL 4  \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setOperationOnlyMode(PM_UCD9090A_RAIL4);

                    printf("Operation ON  \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_ON);

                    printf("Immediate OFF \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_IMMEDIATE_OFF);


                    printf("RAIL 5  \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setOperationOnlyMode(PM_UCD9090A_RAIL5);

                    printf("Operation ON  \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_ON);

                    printf("Immediate OFF \n");
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->writeOperation(PM_OPERATION_IMMEDIATE_OFF);

                    printf("NSH OFF \n");


                }
                else if (sCmd[0] == 'h')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setControlPinOnlyMode(PM_UCD9090A_RAIL4);

                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setControlPinOnlyMode(PM_UCD9090A_RAIL5);

                    printf("NSH ON \n");
                }
                else if (sCmd[0] == 'i')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_10_FPWM1_GPIO5, false);
                    usleep(200);
                    printf("STATUS LED/CAMERA OFF \n");
                }
                else if (sCmd[0] == 'l')
                {
                    MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->setPowerGoodMode(GPO_PIN_10_FPWM1_GPIO5, true);
                    usleep(200);
                    printf("STATUS LED/CAMERA ON \n");
                }
                else
                {
                    return -1;
                }
            }
            break;

        case 'x':
        default:
            return -1;
            break;
    }

    return 0;
}
