#ifndef EEPROMTEST_H
#define EEPROMTEST_H

#include "TestManagerBase.h"

class EEpromTest : public TestManagerBase
{
public:
    EEpromTest();

    virtual ~EEpromTest();

    int actionCmd(uint8_t ubCmd);
};

#endif // EEPROMTEST_H
