#include "SectionTest.h"
#include "CanLinkBoard.h"
#include "SectionBoard.h"
#include "MainExecutor.h"

SectionTest::SectionTest()
{
    vecMenu.clear();

    vecMenu.push_back("a - FW Version\n");
    vecMenu.push_back("b - get Status\n");
    vecMenu.push_back("c - ACK\n");
    vecMenu.push_back("d - \n");
    vecMenu.push_back("f - \n");
    vecMenu.push_back("l - \n");
    vecMenu.push_back("r - \n");
    vecMenu.push_back("x - End Test\n");

    m_ubSection = SCT_A_ID;
}

SectionTest::~SectionTest()
{

}

int SectionTest::setSection(uint8_t ubVal)
{
    if (ubVal > SCT_B_ID)
    {
        log(LOG_ERR,"SectionTest::setSection--> Section not in list");
        return -1;
    }
    m_ubSection = ubVal;
    return 0;
}

int SectionTest::actionCmd(uint8_t ubCmd)
{
    SectionBoard *section = nullptr;

    if (m_ubSection == SCT_A_ID)
    {
        section = MainExecutor::getInstance()->m_CANLinkBoard.sectionA;
    }
    else if (m_ubSection == SCT_B_ID)
    {
        section = MainExecutor::getInstance()->m_CANLinkBoard.sectionB;
    }
    else
    {

    }

    switch (ubCmd)
    {
        case 'a':
        {
            m_pLogger->log(LOG_INFO,"--<FW VERSION>--");
            string sBoot = section->getFirmwareVersion(eBootloader, 0);
            string sApp = section->getFirmwareVersion(eApplication, 0);
            string sSel = section->getFirmwareVersion(eSelector, 0);
            string sFPGA = section->getFirmwareVersion(eFPGAbitstream, 0);

            printf("Boot: %s\n",sBoot.c_str());
            printf("Application: %s\n",sApp.c_str());
            printf("Selector: %s\n",sSel.c_str());
            printf("FPGA: %s\n",sFPGA.c_str());

        }
        break;

        case 'b':
        {
            m_pLogger->log(LOG_INFO,"--<GET STATUS>--");
            uint8_t ubStatus;
            section->getInternalStatus(ubStatus);
            printf("STATUS %i\n", ubStatus);
        }
        break;

        case 'c':
        {
            m_pLogger->log(LOG_INFO,"--<ACK>--");
    //        uint8_t ubStatus;
            section->ack();
    //        printf("STATUS %i\n", ubStatus);
        }
        break;

        case 'x':
        default:
        {
            return -1;
        }
        break;
    }
    return 0;
}
