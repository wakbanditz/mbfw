#ifndef NSHENCODERTEST_H
#define NSHENCODERTEST_H

#include "TestManagerBase.h"

class NSHEncoderTest : public TestManagerBase
{
    public:
        NSHEncoderTest();

        virtual ~NSHEncoderTest();

        int actionCmd(uint8_t ubCmd);
};

#endif // NSHENCODERTEST_H
