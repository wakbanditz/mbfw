#ifndef FANCONTROLLERTEST_H
#define FANCONTROLLERTEST_H

#include "TestManagerBase.h"

class FanControllerTest : public TestManagerBase
{
    public:
        FanControllerTest();

        virtual ~FanControllerTest();

        int actionCmd(uint8_t ubCmd);
};

#endif // FANCONTROLLERTEST_H
