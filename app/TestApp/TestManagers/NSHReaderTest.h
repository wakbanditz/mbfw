#ifndef NSHREADERTEST_H
#define NSHREADERTEST_H

#include "TestManagerBase.h"

class NSHReaderTest : public TestManagerBase
{
public:
    NSHReaderTest();

    virtual ~NSHReaderTest();

    int actionCmd(uint8_t ubCmd);
};

#endif // NSHREADERTEST_H
