#ifndef CODECAUDIOTEST_H
#define CODECAUDIOTEST_H

#include "TestManagerBase.h"

class CodecAudioTest : public TestManagerBase
{
    public:
        CodecAudioTest();

        virtual ~CodecAudioTest();

        int actionCmd(uint8_t ubCmd);
};

#endif // CODECAUDIOTEST_H
