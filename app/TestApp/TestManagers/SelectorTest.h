#ifndef SELECTORTEST_H
#define SELECTORTEST_H

#include "TestManagerBase.h"
#include "CommonInclude.h"


class SelectorTest : public TestManagerBase
{
    public:
        SelectorTest();

        virtual ~SelectorTest();

        int actionCmd(uint8_t ubCmd);
};

#endif // SELECTORTEST_H
