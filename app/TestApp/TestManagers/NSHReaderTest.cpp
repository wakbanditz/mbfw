#include "NSHReaderTest.h"
#include "MainExecutor.h"
#include "NSHReaderLink.h"

NSHReaderTest::NSHReaderTest()
{
    vecMenu.clear();

    vecMenu.push_back("a - Get FW Version\n");
    vecMenu.push_back("b - Get NSH Status\n");
    vecMenu.push_back("c - Reset NSH\n");
    vecMenu.push_back("d - Get Fluorescence Reading\n");
    vecMenu.push_back("e - Get Air Reading\n");
    vecMenu.push_back("f - Read solid standard RFU\n");
    vecMenu.push_back("g - Get Last Raw Fluorescence Value\n");
    vecMenu.push_back("h - Get Last Raw Reference Value\n");
    vecMenu.push_back("i - Restore Default Parameter\n\n");
}

NSHReaderTest::~NSHReaderTest()
{

}

int NSHReaderTest::actionCmd(uint8_t ubCmd)
{
    string strFWversion;
    structNSHStatus NSHstatus;
    structNSHFluoRead fluoRead;
    structNSHLastRFV NSHRfvValues;
    structNSHLastReference NSHRefValues;

    uint8_t ucStripPos = 1;
    int32_t liTmpVal;

    switch (ubCmd)
    {
        case 'a':
            printf("   a - App Version\n");
            printf("   b - Boot Version\n");
            printf("   c - Selector Version\n");
            printf("   d - Board HW Version\n\n");

            decodeCmd();

            switch (sCmd[0])
            {
                case 'a':
                    MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getFwVersion(eAppVersion, strFWversion);
                break;

                case 'b':
                    MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getFwVersion(eBootVersion, strFWversion);
                break;

                case 'c':
                    MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getFwVersion(eSelectorVersion, strFWversion);
                break;

                case 'd':
                    MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getFwVersion(eBoardHwVersion, strFWversion);
                break;

                default:
                    printf("Wrong key pressed. I'm sorry.\n");
                break;
            }
        break;

        case 'b':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getNSHStatus(&NSHstatus);
        break;

        case 'c':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.swReset();
        break;

        case 'd':
            printf("   a - Model Enabled\n");
            printf("   b - Model Disabled\n");

            decodeCmd();

            if ( sCmd[0] == 'a' )
            {
                MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getNSHFluoRead(eNSHModelEnabled, &fluoRead);
            }
            else if ( sCmd[0] == 'b' )
            {
                MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getNSHFluoRead(eNSHModelDisabled, &fluoRead);
            }
            else
            {
                printf("Wrong key pressed. I'm sorry.\n");
            }
        break;

        case 'e':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.readAir(liTmpVal);
        break;

        case 'f':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.readSolidStandardRFU(liTmpVal);
        break;

        case 'g':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getLastFluoRawValuemV(&NSHRfvValues, ucStripPos);
        break;

        case 'h':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.getLastRefRawValuemV(&NSHRefValues, ucStripPos);
        break;

        default:
            printf("Wrong key pressed. I'm sorry.\n");
            return -1;
        break;

    }

    return 0;
}
