#include "LedDriverTest.h"
#include "MainExecutor.h"
#include "PCA9955B.h"

LedDriverTest::LedDriverTest()
{
    vecMenu.clear();

    vecMenu.push_back("a - LEDOUT\n");
    vecMenu.push_back("b - PWM\n");
    vecMenu.push_back("c - IREF\n");
    vecMenu.push_back("d - PWMALL\n");
    vecMenu.push_back("e - LEDGRP\n");
    vecMenu.push_back("f - IREFALL\n");
    vecMenu.push_back("g - GRPFREQ\n");
    vecMenu.push_back("i - Enable Dim/Blnk Mode\n");
    vecMenu.push_back("l - Set Blinking\n");
    vecMenu.push_back("r - resetSoft\n");
    vecMenu.push_back("p - clearError\n");
    vecMenu.push_back("o - init\n");
    vecMenu.push_back("x - End Test\n" );
}

LedDriverTest::~LedDriverTest()
{

}

int LedDriverTest::actionCmd(uint8_t ubCmd)
{
    uint8_t ubLed = 0;
    uint8_t ubConf = 0;
    uint8_t ubState = 0;
    uint8_t	ubAppo = 0;
    uint8_t ubPeriod = 0;

    switch(ubCmd)
    {
        case 'a':
            printf("LEDOUT [w,r]: " );
            decodeCmd();

            if (sCmd[0] == 'w')
            {
                printf("LED [0-16]: " );
                decodeCmd();
                ubLed = atoi(sCmd);

                printf("CONF [0-3]: " );
                decodeCmd();
                ubConf = atoi(sCmd);

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(ubLed, ubConf);
            }
            else if (sCmd[0] == 'r')
            {
                    printf("LED [0-16]: " );
                    decodeCmd();
                    ubLed = atoi(sCmd);
                    MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readLedOut(ubLed, ubState);
                    log(LOG_INFO, "LEDOUT conf:0x%X \n", ubState);
            }
            else
            {

            }
            break;

        case 'b':
            printf("PWM [w,r]: " );
            decodeCmd();

            if (sCmd[0] == 'w')
            {
                printf("LED [0-16]: " );
                decodeCmd();
                ubLed = atoi(sCmd);

                printf("BRIGHT [0-255]: " );
                decodeCmd();
                ubAppo = atoi(sCmd);

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWM(ubLed, ubAppo);
            }
            else if (sCmd[0] == 'r')
            {
                printf("LED [0-16]: " );
                decodeCmd();
                ubLed = atoi(sCmd);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readPWM(ubLed, ubState);
                log(LOG_INFO, "Brightness conf:0x%X \n", ubState);

            }
            else
            {

            }
            break;

        case 'c':
            printf("IREF [w,r]: " );
            decodeCmd();

            if (sCmd[0] == 'w')
            {
                printf("LED [0-16]: " );
                decodeCmd();

                ubLed = atoi(sCmd);

                printf("CURR [0-100]: " );
                decodeCmd();

                ubAppo = atoi(sCmd);

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIref(ubLed, ubAppo);
            }
            else if (sCmd[0] == 'r')
            {
                printf("LED [0-16]: " );
                decodeCmd();

                ubLed = atoi(sCmd);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readIref(ubLed, ubState);
                log(LOG_INFO, "Current %d", ubState);
            }
            else
            {

            }
            break;

        case 'd':
            printf("PWMALL [w,r]: " );
            decodeCmd();

            if (sCmd[0] == 'w')
            {
                printf("PWM [0-100]: " );
                decodeCmd();
                ubAppo = atoi(sCmd);

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWMAll(ubAppo);
            }
            else if (sCmd[0] == 'r')
            {
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readPWMAll( ubState);
                log(LOG_INFO, "PWM %d", ubState);
            }
            else
            {
            }
            break;

    case 'e':
            printf("LED [G,Y,R]: " );
            decodeCmd();
            ubLed = sCmd[0];

            printf("CONF [0-3]: " );
            decodeCmd();
            ubConf = atoi(sCmd);

            if (ubLed == 'G')
            {
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_GREEN_0, ubConf);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_GREEN_1, ubConf);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_GREEN_2, ubConf);
            }
            else if (ubLed == 'Y')
            {
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_YELLOW_0, ubConf);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_YELLOW_1, ubConf);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_YELLOW_2, ubConf);
            }
            else if (ubLed == 'R')
            {
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_RED_0, ubConf);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_RED_1, ubConf);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_RED_2, ubConf);
            }
            else
            {}

            break;

        case 'f':
            printf("IREFALL [w,r]: " );
            decodeCmd();

            if (sCmd[0] == 'w')
            {
                printf("CURRENT [0-100]: " );
                decodeCmd();
                ubAppo =atoi(sCmd);

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIrefAll(ubAppo);
            }
            else if (sCmd[0] == 'r')
            {
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readIrefAll(ubState);
                printf("CURR: %d \n", ubState);
                log(LOG_INFO, "CURRENT %d", ubState);
            }
            else
            {
            }
            break;

        case 'g':
            printf("GRPFREQ [w,r]: " );
                decodeCmd();

            if (sCmd[0] == 'w')
            {
                printf("FREQ [0-100]: " );
                decodeCmd();

                ubAppo = atoi(sCmd);

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpFreq(ubAppo);
            }
            else if (sCmd[0] == 'r')
            {

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readGrpFreq(ubState);
                log(LOG_INFO, "FREQ %d", ubState);

            }
            else
            {
            }
            break;

        case 'h':
            printf("GRPPWM [w,r]: " );
                decodeCmd();

            if (sCmd[0] == 'w')
            {
                printf("PWM [0-100]: " );
                decodeCmd();

                ubAppo = atoi(sCmd);

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpPWM(ubAppo);
            }
            else if (sCmd[0] == 'r')
            {

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->readGrpPWM(ubState);
                log(LOG_INFO, "GRPPWM %d", ubState);

            }
            else
            {
            }
            break;

        case 'i':
            printf("1 - DIM Mode\n " );
            printf("2 - BLNK Mode\n " );
            decodeCmd();

            if (sCmd[0] == '2')
            {
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_BLNK_CTL);
                log(LOG_INFO,"Set blinking mode\n " );
            }
            else
            {
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_DIM_CTL);
                log(LOG_INFO,"Set dimming mode\n " );
            }
            break;

        case 'l':
            printf("LED [0-16]: " );
            decodeCmd();

            ubLed = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIref(ubLed, 25);
            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWM(ubLed, 50);
            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpPWM(50);//dc 50%

            printf("Freq [1s-15s]" );
            decodeCmd();

            ubAppo = atoi(sCmd);
            ubPeriod = (15.26*ubAppo)-1;
            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpFreq(ubPeriod);
            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(ubLed, 3);
            break;

        case 'r':

            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->resetSoft();
            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_DIM_CTL);
            log(LOG_INFO,"RESET \n" );
            break;

        case 'p':

            MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->clearError();

            log(LOG_INFO,"CLEAR ERROR \n" );
            break;

        case 'o':
            {
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->resetSoft();
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_BLNK_CTL);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIrefAll(40);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->clearLedOut();

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeIref(OUTPUT_LED_DIS_OSC, 1);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_DIS_OSC, 1);

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWMAll(50);


//                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWM(OUTPUT_LED_GREEN_0, 50);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpPWM(50);


//                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWM(OUTPUT_LED_YELLOW_0, 50);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpPWM(50);


//                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writePWM(OUTPUT_LED_RED_0, 50);
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpPWM(50);

                uint8_t ubPeriod = 0;
                ubPeriod = (15.26 * BLK_TIME_LED) - 1;
                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeGrpFreq(ubPeriod);

                MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->clearErrorMode2();
            }
            break;

        case 'x':
        default:
            return -1;
        break;
    }

    return 0;
}
