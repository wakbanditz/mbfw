#ifndef LEDDRIVERTEST_H
#define LEDDRIVERTEST_H

#include "TestManagerBase.h"

class LedDriverTest : public TestManagerBase
{
    public:
        LedDriverTest();

        virtual ~LedDriverTest();

        int actionCmd(uint8_t ubCmd);
};

#endif // LEDDRIVERTEST_H
