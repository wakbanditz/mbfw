#include "GPIOExpSectionTest.h"
#include "MainExecutor.h"
#include "PCA9633.h"

GPIOExpSectionTest::GPIOExpSectionTest()
{
    vecMenu.clear();

    vecMenu.push_back("a - setPWM A\n" );
    vecMenu.push_back("b - getPWM A\n" );
    vecMenu.push_back("c - setLEDOUT A\n");
    vecMenu.push_back("d - getLEDOUT A\n");
    vecMenu.push_back("r - resetSoft A\n");
    vecMenu.push_back("f - setPWM B\n" );
    vecMenu.push_back("g - getPWM B\n" );
    vecMenu.push_back("h - setLEDOUT B\n");
    vecMenu.push_back("i - getLEDOUT B\n");
    vecMenu.push_back("l - resetSoft B\n");
    vecMenu.push_back("m - resetSectionA\n");
    vecMenu.push_back("n - resetSectionB\n");
    vecMenu.push_back("x - Exit\n" );
}

GPIOExpSectionTest::~GPIOExpSectionTest()
{

}

int GPIOExpSectionTest::actionCmd(uint8_t ubCmd)
{
    uint8_t ubGpio = 0;
    uint8_t ubBright = 0;
    uint8_t ubPWM = 0;
    uint8_t	ubAppo = 0;

    switch(ubCmd)
    {
        case 'a':
            printf("Select GPIO/LED[0-3]: ");
            decodeCmd();
            ubGpio = atoi(sCmd);

            printf("Enter brightness[0-255]: ");
            decodeCmd();

            ubBright = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->writePWM(ubGpio, ubBright);
            break;

        case 'b':
            printf("Select GPIO/LED[0-3]: ");
            decodeCmd();

            ubPWM = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->readPWM(ubPWM, ubBright);
            printf("Brightness:%d \n", ubBright);
            break;

        case 'c':
            printf("Select GPIO/LED[0-3]: ");
            decodeCmd();
            ubGpio = atoi(sCmd);

            printf("Enter setting[0-3]: ");
            decodeCmd();
            ubAppo = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->writeLedOut(ubGpio, ubAppo);
            break;

        case 'd':

            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->readLedOut(ubAppo);
            printf("LEDOUT state: %d \n", ubAppo);
            break;

        case 'r':
            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->resetSoft();
            printf("RESET DONE \n");
            break;

        case 'f':
            printf("Select GPIO/LED[0-3]: ");
            decodeCmd();
            ubGpio = atoi(sCmd);

            printf("Enter brightness[0-255]: ");
            decodeCmd();
            ubBright = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->writePWM(ubGpio, ubBright);
            break;

        case 'g':
            printf("Select GPIO/LED[0-3]: ");
            decodeCmd();
            ubPWM = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->readPWM(ubPWM, ubBright);
            printf("Brightness:%d \n", ubBright);
            break;

        case 'h':
            printf("Select GPIO/LED[0-3]: ");
            decodeCmd();
            ubGpio = atoi(sCmd);

            printf("Enter setting[0-3]: ");
            decodeCmd();
            ubAppo = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->writeLedOut(ubGpio, ubAppo);
            break;

        case 'i':
            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->readLedOut(ubAppo);
            printf("LEDOUT state: %d \n", ubAppo);
            break;

        case 'l':
            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->resetSoft();
            printf("RESET DONE \n");
            break;

        case 'm':
            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->writeLedOut(1);
            msleep(500);
            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderA->writeLedOut(0);
            printf("RESET SECTION A DONE \n");
            break;

        case 'n':
            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->writeLedOut(1);
            msleep(500);
            MainExecutor::getInstance()->m_i2cBoard.m_pGpioExpanderB->writeLedOut(0);
            printf("RESET SECTION B DONE \n");
            break;

        case 'x':
        default:
            return -1;
            break;
    }
    return 0;
}
