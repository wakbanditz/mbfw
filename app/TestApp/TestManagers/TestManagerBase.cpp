#include "TestManagerBase.h"
#include "CommonInclude.h"

TestManagerBase::TestManagerBase()
{
    m_ubCmd = 0;
}

TestManagerBase::~TestManagerBase()
{

}

void TestManagerBase::decodeCmd()
{    
    scanf("%s", sCmd);

    m_ubCmd = sCmd[0];

    log(LOG_DEBUG_PARANOIC,"Input command: %s", sCmd);
}

void TestManagerBase::showMainMenu(void)
{
    for(unsigned int i = 0; i < vecMenu.size(); i++)
    {
        // check if the error is activated
        string pstrMenu = vecMenu.at(i);
        printf("%s",pstrMenu.c_str());
    }
}

void TestManagerBase::afterWorkerThread( void )
{
    m_pLogger->log(LOG_DEBUG,"TestManagerBase: thread END");
}

void TestManagerBase::beforeWorkerThread( void )
{
    m_pLogger->log(LOG_DEBUG,"TestManagerBase: thread START");
}

int TestManagerBase::workerThread( void )
{
    while( isRunning() )
    {
        showMainMenu();

        decodeCmd();

        int res = actionCmd(m_ubCmd);
        if (res == -1)
        {
            m_pLogger->log(LOG_INFO,"TestManagerBase: Exit Thread");
            break;
        }

        usleep(2);
    }
    return 0;
}
