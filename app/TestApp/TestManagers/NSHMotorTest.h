#ifndef NSHMOTORTEST_H
#define NSHMOTORTEST_H

#include "TestManagerBase.h"

class NSHMotorTest : public TestManagerBase
{
    public:
        NSHMotorTest();

        virtual ~NSHMotorTest();

        int actionCmd(uint8_t ubCmd);
};

#endif // NSHMOTORTEST_H
