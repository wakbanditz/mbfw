#ifndef TESTMANAGERBASE_H
#define TESTMANAGERBASE_H

#include <stdint.h>
#include <vector>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Thread.h"
#include "Log.h"
#include "Loggable.h"
#include "Mutex.h"

using namespace std;

class TestManagerBase : public Thread,
                        public Loggable
{
    public:

        /*!
         * @brief Contructor
         */
        TestManagerBase();

        /*!
         * @brief destructor
         */
        virtual ~TestManagerBase();

        void init(void);

//        virtual void setMainMenu(void)=0;

//        virtual void showMainMenu(void)=0;

        virtual int actionCmd(uint8_t ubCmd)=0;


    protected:

        void showMainMenu(void);
        void decodeCmd(void);
        /*!
         * @brief	workerThread
         *
         *
         * @return 0 if success, -1 otherwise
         */
        int		workerThread( void );

        /*!
         * @brief	beforeWorkerThread	Function to be called before starting the thread
         */
        void	beforeWorkerThread( void );

        /*!
         * @brief	afterWorkerThread	Function to be called after ending the thread
         */
        void	afterWorkerThread( void );


    private:



    public:
    protected:
        vector<string>      vecMenu;

        char                sCmd[20];

    private:
        uint8_t m_ubCmd;

};

#endif // TESTMANAGERBASE_H
