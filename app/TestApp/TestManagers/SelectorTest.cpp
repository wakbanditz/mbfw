#include "SelectorTest.h"
#include "PowerManagerTest.h"
#include "LedDriverTest.h"
#include "GPIOExpSectionTest.h"
#include "FanControllerTest.h"
#include "CodecAudioTest.h"
#include "EEpromTest.h"
#include "NSHReaderTest.h"
#include "NSHMotorTest.h"
#include "NSHEncoderTest.h"
#include "CameraTest.h"
#include "SectionTest.h"
#include "MainExecutor.h"

SelectorTest::SelectorTest()
{
    vecMenu.clear();

    vecMenu.push_back("--< TEST >--\n");
    if (MainExecutor::getInstance()->isPowerManagerEnabled())
    vecMenu.push_back("a - Power Manager\n");

    if (MainExecutor::getInstance()->isGPIOExpSectionEnabled())
    vecMenu.push_back("b - Gpio Expander Section\n");

    if (MainExecutor::getInstance()->isLedDriverEnabled())
    vecMenu.push_back("c - Led Driver\n" );

    if (MainExecutor::getInstance()->isFanControllerEnabled())
    vecMenu.push_back("d - Fan Controller\n");

    if (MainExecutor::getInstance()->isEEPromEnabled())
    vecMenu.push_back("e - EEprom\n");

    if (MainExecutor::getInstance()->isCodecAudioEnabled())
    vecMenu.push_back("f - Codec Audio\n");

    if (MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled())
    vecMenu.push_back("g - NSH Reader\n");

    if (MainExecutor::getInstance()->m_SPIBoard.isNSHMotorEnabled())
    vecMenu.push_back("h - NSH Motor\n" );

    if (MainExecutor::getInstance()->m_SPIBoard.isNSHEncoderEnabled())
    vecMenu.push_back("i - NSH Encoder\n");

    if (MainExecutor::getInstance()->isCameraEnabled())
    vecMenu.push_back("l - Camera\n");

    if (MainExecutor::getInstance()->isSectionAEnabled())
    vecMenu.push_back("m - Section A\n");

    if (MainExecutor::getInstance()->isSectionBEnabled())
    vecMenu.push_back("n - Section B\n");



    vecMenu.push_back("x - Exit\n" );
}

SelectorTest::~SelectorTest()
{

}

int SelectorTest::actionCmd(uint8_t ubCmd)
{

    switch (ubCmd) {
        case 'a':
            {
                PowerManagerTest* powerManager = new PowerManagerTest();
                powerManager->setLogger(getLogger());
                powerManager->startThread();
                while (powerManager->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(powerManager);
            }
            break;

        case 'b':
            {
                GPIOExpSectionTest* gpioExpSec = new GPIOExpSectionTest();
                gpioExpSec->setLogger(getLogger());
                gpioExpSec->startThread();
                while (gpioExpSec->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(gpioExpSec);
            }
            break;

        case 'c':
            {
                LedDriverTest* ledDriver = new LedDriverTest();
                ledDriver->setLogger(getLogger());
                ledDriver->startThread();
                while (ledDriver->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(ledDriver);
            }
            break;

        case 'd':
            {
                FanControllerTest* fanController = new FanControllerTest();
                fanController->setLogger(getLogger());
                fanController->startThread();
                while (fanController->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(fanController);
            }
            break;

        case 'e':
            {
                EEpromTest* eeprom = new EEpromTest();
                eeprom->setLogger(getLogger());
                eeprom->startThread();
                while (eeprom->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(eeprom);
            }
            break;

        case 'f':
            {
                CodecAudioTest* codecAudio = new CodecAudioTest();
                codecAudio->setLogger(getLogger());
                codecAudio->startThread();
                while (codecAudio->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(codecAudio);
            }
            break;

        case 'g':
            {
                NSHReaderTest* readerNSH = new NSHReaderTest();
                readerNSH->setLogger(getLogger());
                readerNSH->startThread();
                while (readerNSH->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(readerNSH);
            }
            break;

        case 'h':
            {
                NSHMotorTest* motorNSH = new NSHMotorTest();
                motorNSH->setLogger(getLogger());
                motorNSH->startThread();
                while (motorNSH->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(motorNSH);
            }
            break;

        case 'i':
            {
                NSHEncoderTest* encoderNSH = new NSHEncoderTest();
                encoderNSH->setLogger(getLogger());
                encoderNSH->startThread();
                while (encoderNSH->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(encoderNSH);
            }
            break;

        case 'l':
            {
                CameraTest* camera = new CameraTest();
                camera->setLogger(getLogger());
                camera->startThread();
                while (camera->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(camera);
            }
            break;

        case 'm':
            {
                SectionTest* sectionA = new SectionTest();
                sectionA->setLogger(getLogger());
                sectionA->setSection(SCT_A_ID);
                sectionA->startThread();
                while (sectionA->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(sectionA);
            }
            break;

        case 'n':
            {
                SectionTest* sectionB = new SectionTest();
                sectionB->setLogger(getLogger());
                sectionB->setSection(SCT_B_ID);
                sectionB->startThread();
                while (sectionB->isRunning())
                {
                    //stay in loop until the thread is stopped
                    usleep(200);
                }
                SAFE_DELETE(sectionB);
            }
            break;


        default:
            return -1;
            break;
    }



    return 0;
}
