#include "FanControllerTest.h"
#include "MainExecutor.h"
#include "ADT7470.h"

FanControllerTest::FanControllerTest()
{
    vecMenu.clear();

    vecMenu.push_back("a - START\n" );
    vecMenu.push_back("b - STOP\n" );
    vecMenu.push_back("c - INFO\n");
    vecMenu.push_back("d - set config register \n");
    vecMenu.push_back("e - read TACH\n");
    vecMenu.push_back("f - set CurrentDutyCycle\n" );
    vecMenu.push_back("g - read CurrentDutyCycle\n" );
    vecMenu.push_back("h - config\n");
    vecMenu.push_back("i - PWM12 Configuration Register\n");
    vecMenu.push_back("l - PWM34 Configuration Register\n");
    vecMenu.push_back("m - Itr Mask Register 1\n");
    vecMenu.push_back("n - Itr Mask Register 2\n");
    vecMenu.push_back("o - Total init\n");
    vecMenu.push_back("x - End Test\n" );
}

FanControllerTest::~FanControllerTest()
{

}

int FanControllerTest::actionCmd(uint8_t ubCmd)
{
    uint8_t   ubFan = 0;
    uint8_t   ubAppo = 0;
    uint16_t  usRPM = 0;

    switch(ubCmd)
    {
        case 'a':
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->startFanController();
            log(LOG_INFO,"FAN CONTROLLER START" );
            break;

        case 'b':
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->stopFanController();
            log(LOG_INFO,"FAN CONTROLLER STOP" );
            break;

        case 'c':
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readDeviceIDRegister(ubAppo);
            log(LOG_INFO,"Device ID: %d\n", ubAppo);

            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readCompanyIDNumber(ubAppo);
            log(LOG_INFO,"Company ID: %d\n", ubAppo);

            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readRevisionNumber(ubAppo);
            log(LOG_INFO,"Revision Num: %d\n", ubAppo);
            break;

        case 'd':
            printf("Configuration Register 1: " );
            decodeCmd();
            ubFan = atoi(sCmd);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeConfigRegister1(ubFan);
            log(LOG_INFO,"CFG Reg 1: 0x%X", ubFan);

            printf("Configuration Register 2: " );
            decodeCmd();
            ubFan = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeConfigRegister2(ubFan);
            log(LOG_INFO,"CFG Reg 2: 0x%X", ubFan);
            break;

        case 'e':
            printf("FAN [0-3]: " );
            decodeCmd();
            ubFan = atoi(sCmd);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readFan(ubFan, usRPM);
            log(LOG_INFO,"FAN SPEED: %d\n", usRPM);
            break;

        case 'f':
            printf("FAN [0-3]: " );
            decodeCmd();
            ubFan = atoi(sCmd);

            printf("DC [0-100]: " );
            decodeCmd();
            ubAppo = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(ubFan, ubAppo);
            log(LOG_INFO,"FAN SPEED: %d\n", ubAppo);
            break;

        case 'g':
            printf("FAN [0-3]: " );
            decodeCmd();
            ubFan = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readCurrentDutyCycle(ubFan, ubAppo);
            log(LOG_INFO,"Current DC %d \n", ubAppo);
            break;

        case 'h':
            printf("CONFIG LOW FREQ \n");
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->configLowFrequency();
            log(LOG_INFO,"Config LOW Frequency");
            break;

        case 'i':
            printf("PWM1/PWM2 configuration register: " );
            decodeCmd();
            ubFan = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writePWM12ConfigurationRegister(ubFan);
            log(LOG_INFO,"write PWM1/PWM2 configuration register 0x%X", ubFan);
            break;

        case 'l':
            printf("PWM3/PWM4 configuration register: " );
            decodeCmd();
            ubFan = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writePWM34ConfigurationRegister(ubFan);
            log(LOG_INFO,"write PWM3/PWM4 configuration register 0x%X", ubFan);
            break;

        case 'm':
            printf("Interrupt Mask Register 1: " );
            decodeCmd();
            ubFan = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeItrMaskReg1(ubFan);
            log(LOG_INFO,"write Itr Mask register 1 0x%X", ubFan);
            break;

        case 'n':
            printf("Interrupt Mask Register 2: " );
            decodeCmd();
            ubFan = atoi(sCmd);

            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeItrMaskReg2(ubFan);
            log(LOG_INFO,"write Itr Mask register 2 0x%X", ubFan);
            break;

        case 'o':

            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeConfigRegister2(0xF0);
            msleep(1);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writePWM12ConfigurationRegister(0x30);
            msleep(1);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writePWM34ConfigurationRegister(0x30);
            msleep(1);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(0,0);
            msleep(1);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(1,0);
            msleep(1);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(2,0);
            msleep(1);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(3,0);
            msleep(1);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeConfigRegister1(0x61);
            msleep(1);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeItrMaskReg1(0x7F);
            msleep(1);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeItrMaskReg2(0x07);
            msleep(1);
            MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeConfigRegister2(0x70);

            log(LOG_INFO,"--<Total Init>--");
            break;

        case 'x':
        default:
            return -1;
        break;
    }
    return 0;
}
