#ifndef GPIOEXPSECTIONTEST_H
#define GPIOEXPSECTIONTEST_H

#include "TestManagerBase.h"

class GPIOExpSectionTest : public TestManagerBase
{
    public:
        GPIOExpSectionTest();

        virtual ~GPIOExpSectionTest();

        int actionCmd(uint8_t ubCmd);
};

#endif // GPIOEXPSECTIONTEST_H
