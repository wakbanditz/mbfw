#ifndef CAMERATEST_H
#define CAMERATEST_H

#include "TestManagerBase.h"

class CameraTest : public TestManagerBase
{
    public:
        CameraTest();

        virtual ~CameraTest();

        int actionCmd(uint8_t ubCmd);
};

#endif // CAMERATEST_H
