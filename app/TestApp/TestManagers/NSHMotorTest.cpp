#include "NSHMotorTest.h"
#include "MainExecutor.h"
#include "NSHMotorGeneral.h"

NSHMotorTest::NSHMotorTest()
{
    vecMenu.clear();

    vecMenu.push_back("a - Get Motor Status\n");
    vecMenu.push_back("b - Set Register\n");
    vecMenu.push_back("c - Get Register\n");
    vecMenu.push_back("d - Move of Relative Steps\n");
    vecMenu.push_back("e - Move To Absolute Position\n");
    vecMenu.push_back("f - Is Sensor At Home?\n");
    vecMenu.push_back("g - Search For Home\n");
    vecMenu.push_back("h - Send Reset\n\n");
}

NSHMotorTest::~NSHMotorTest()
{

}

int NSHMotorTest::actionCmd(uint8_t ubCmd)
{
    int32_t liPosition = 0;
    uint16_t liParamToBeGet = 0;
    uint16_t uliParamToBeSet = 0;
    uint8_t ucDir = 0;

    switch (ubCmd)
    {
        case 'a':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getMotorStatus();
        break;

        case 'b':
            printf("   a - Set Acceleration\n");
            printf("   b - Set Deceleration\n");
            printf("   c - Set Max Speed\n");
            printf("   d - Set Min Speed\n");
            printf("   e - Set Full Speed\n\n");

            decodeCmd();

            switch (ubCmd)
            {
                case 'a':
                    printf("\nPlease insert an acceleration value between 15 and 59590 [step/s^2].\n\n");
                    cin >> uliParamToBeSet;

                    if ( uliParamToBeSet >= 15 && uliParamToBeSet <= 59590 )
                    {
                        MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->setAcceleration(uliParamToBeSet);
                        printf("\nAcceleration value: %d.\n", uliParamToBeSet);
                    }
                    else
                    {
                        printf("\nWRONG INPUT VALUE!\n");
                    }
                break;

                case 'b':
                    printf("\nPlease insert a deceleration value between 15 and 59590 [step/s^2].\n\n");
                    cin >> uliParamToBeSet;

                    if (  uliParamToBeSet >= 15 && uliParamToBeSet <= 59590 )
                    {
                        MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->setDeceleration(uliParamToBeSet);
                        printf("\nDeceleration value: %d.\n", uliParamToBeSet);
                    }
                    else
                    {
                        printf("\nWRONG INPUT VALUE!\n");
                    }
                break;

                case 'c':
                    printf("\nPlease insert max speed value between 16 and 15610 [step/s].\n\n");
                    cin >> uliParamToBeSet;
                    if ( uliParamToBeSet >= 16 && uliParamToBeSet <= 15610 )
                    {
                        MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->setMaxSpeed(uliParamToBeSet);
                        printf("\nMax Speed value: %d.\n", uliParamToBeSet);
                    }
                    else
                    {
                        printf("\nWRONG INPUT VALUE!\n");
                    }
                break;

                case 'd':
                    printf("\nPlease insert min speed value between 0 and 976 [step/s].\n\n");
                    cin >> uliParamToBeSet;
                    if ( uliParamToBeSet > 0 && uliParamToBeSet <= 976 )
                    {
                        MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->setMinSpeed(uliParamToBeSet);
                        printf("\nMin speed value: %d.\n", uliParamToBeSet);
                    }
                    else
                    {
                        printf("\nWRONG INPUT VALUE!\n");
                    }
                break;

                case 'e':
                    printf("\nPlease insert a full speed value between 8 and 15625.\n\n");
                    cin >> uliParamToBeSet;
                    if ( uliParamToBeSet >= 8 && uliParamToBeSet <= 15625 )
                    {
                        MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->setFullSpeed(uliParamToBeSet);
                        printf("\nFull speed value: %d.\n", uliParamToBeSet);
                    }
                    else
                    {
                        printf("\nWRONG INPUT VALUE!\n");
                    }
                break;

                default:
                    printf("Wrong key pressed. I'm sorry.\n");
                break;
            }

        break;

        case 'c':
            printf("   a - Get Acceleration\n");
            printf("   b - Get Deceleration\n");
            printf("   c - Get Max Speed\n");
            printf("   d - Get Min Speed\n");
            printf("   e - Get Full Speed\n");
            printf("   f - Get Absolute Position\n\n");

            decodeCmd();
            switch (sCmd[0])
            {
                case 'a':
                    MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getAcceleration(liParamToBeGet);
                    printf("\nAcceleration value: %d.\n", liParamToBeGet);
                break;

                case 'b':
                    MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getDeceleration(liParamToBeGet);
                    printf("\nDeceleration value: %d.\n", liParamToBeGet);
                break;

                case 'c':
                    MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getMaxSpeed(liParamToBeGet);
                    printf("\nMax Speed value: %d.\n", liParamToBeGet);
                break;

                case 'd':
                    MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getMinSpeed(liParamToBeGet);
                    printf("\nMin speed value: %d.\n", liParamToBeGet);
                break;

                case 'e':
                    MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getFullSpeed(liParamToBeGet);
                    printf("\nFull speed value: %d.\n", liParamToBeGet);
                break;

                case 'f':
                    MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getAbsPosition((int&)liParamToBeGet);
                    printf("\nAbsolute position value: %d.\n", liParamToBeGet);
                break;

                default:
                    printf("Wrong key pressed. I'm sorry.\n");
                break;
            }

        break;

        case 'd':
            printf("Please insert steps number.\n");
            cin >> liPosition;
            printf("Please insert the direction of the motion. 0 for reverse, any other key for forward direction.\n");
            cin >> ucDir;

            if ( ucDir == 0)
                MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->moveRelativeSteps(eReverseDir, liPosition);
            else
                MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->moveRelativeSteps(eForwardDir, liPosition);

            printf("Motion completed.\n");
        break;

        case 'e':
            printf("Please insert the absolute position.\n");
            cin >> liPosition;
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->moveToAbsolutePosition(liPosition);
            printf("Motion completed.\n");
        break;

        case 'f':
            if ( MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->isSensorAtHome() )
                printf("Sensor is at home.\n");
            else
                printf("Sensor is not at home.\n");
        break;

        case 'g':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->searchForHome();
        break;

        case 'h':
            MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->sendReset();
            printf("Reset sent with success.\n");
        break;

        default:
            printf("Wrong key pressed. I'm sorry.\n");
            return -1;
        break;
    }
    return 0;
}
