INCLUDEPATH += $$PWD/../MainExecutor
INCLUDEPATH += $$PWD/../WorkflowManager
INCLUDEPATH += $$PWD/../WorkflowManager/Services
INCLUDEPATH += $$PWD/../InstrumentInfo
INCLUDEPATH += $$PWD/../SectionBoardManager
INCLUDEPATH += $$PWD/../PowerOffManager
INCLUDEPATH += $$PWD/../NSHBoardManager
INCLUDEPATH += $$PWD/../CameraBoardManager

HEADERS += $$PWD/MainExecutor.h
SOURCES += $$PWD/MainExecutor.cpp
