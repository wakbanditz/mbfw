/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    MainExecutor.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the MainExecutor class.
 @details

 ****************************************************************************
*/

#ifndef MAINEXECUTOR_H
#define MAINEXECUTOR_H

#include "CommonInclude.h"
#include "StaticSingleton.h"

#include "Log.h"
#include "Loggable.h"
#include "EventRecorder.h"

#include "Config.h"

#include "SectionBoardLink.h"
#include "SectionBoardManager.h"
#include "WebServerDaemon.h"
#include "HttpPageContainer.h"
#include "ProtocolValidator.h"

#include "CR8062Include.h"
#include "CameraBoardManager.h"
#include "CR8062Link.h"
#include "USBLinkBoard.h"
#include "SampleDetectionLink.h"
#include "NSHBoardManager.h"
#include "SPILinkBoard.h"
#include "I2CLinkBoard.h"

#include "OperationManager.h"
#include "ServiceCollector.h"
#include "LoginManager.h"
#include "StateMachine.h"

#include "SchedulerThread.h"

#include "PowerOffManager.h"
#include "InstrumentInfo.h"
#include "MasterBoardManager.h"

#include "ProtocolInfo.h"
#include "RunwlInfo.h"
#include "ReadSecInfo.h"
#include "PressureSettings.h"
#include "THRunProtocol.h"

#include "FWUpdateManager.h"

class TestManager;

class MainExecutor	:	public StaticSingleton<MainExecutor>,
						public Loggable
{
	public:

		/*! ****************************************************************************************************
		 * @brief MainExecutor void constructor
		 * *****************************************************************************************************
		 */
		MainExecutor();

		/*! ***************************************************************************************************
		 * @brief ~MainExecutor default destructor
		 * ****************************************************************************************************
		 */
		virtual ~MainExecutor();

		/*! *************************************************************************************************
		 * @brief	initConfigurator	initializes configuration structure
		 * @return	true upon succesfull initialization, false otherwise
		 * ************************************************************************************************
		 */
		bool initConfigurator(void);

		/*! *************************************************************************************************
		 * @brief	initLogger	initializes the logger
		 * @return	true upon succesfull initialization, false otherwise
		 * **********************************************************************************************
		 */
		bool initLogger(void);

		/*! **********************************************************************************************
		 * @brief	initTimeValues	records the date+time and startup time
		 * @return	true upon succesfull initialization, false otherwise
		 * ***********************************************************************************************
		 */
		bool initTimeValues();

		/*! *******************************************************************************************
		 * @brief	setTimeValues	calculates the ratio clocks vs seconds to be used when setting the error time
		 * @return	true upon succesfull initialization, false otherwise
		 * *********************************************************************************************
		 */
		bool setTimeValues();

		/*! *********************************************************************************************
		 * @brief	initLoginManager	initializes the login manager
		 * @return	true upon succesfull initialization, false otherwise
		 * ************************************************************************************************
		 */
		bool initLoginManager();

		/*! **********************************************************************************************
		 * @brief	initStateMachine	initializes the state machine
		 * @return	true upon succesfull initialization, false otherwise
		 * **********************************************************************************************
		 */
		bool initStateMachine();

		/*! **********************************************************************************************
		 * @brief	initScheduler	initializes the scheduler
		 * @return	true upon succesfull initialization, false otherwise
		 * ***********************************************************************************************
		 */
		bool initScheduler();

		/*! *************************************************************************************************
		 * @brief	closeScheduler	terminate the scheduler
		 * @return	true upon succesfull initialization, false otherwise
		 * *********************************************************************************************
		 */
		bool closeScheduler();

		/*! *******************************************************************************************
		 * @brief	initInstrumentInfo	initializes the Instrument Info container
		 * @return	true upon succesfull initialization, false otherwise
		 * *********************************************************************************************
		 */
		bool initInstrumentInfo();

		/*! ***********************************************************************************************************
		 * @brief stopInstrumentInfo	stop Instrument Info
		 * ************************************************************************************************************
		 */
		bool stopInstrumentInfo();

		/*! ********************************************************************************************
		 * @brief	idleStateMachine	sets the whole state machine in IDLE
		 * @return	true upon succesfull initialization, false otherwise
		 * **********************************************************************************************
		 */
		bool idleStateMachine();

		/*! *********************************************************************************************
		 * @brief initIPCConfig	provides IPC structures configuration on the OS, for instance the default number
		 *						of message queue managed by the kernel
		 * @return true	upon succesfull configuration, false otherwise
		 * *******************************************************************************************
		 */
		bool initIPCConfig( void );

		/*! ******************************************************************************************
		 * @brief initWebServer initialization of http pages manager and web server daemon. Initialization of http
		 *						page manager consists in the insertion of the pages currently managed by the server
		 *						as per specification
		 * @return true if the server server correctly started to listen to web connections, false otherwise
		 * **********************************************************************************************
		 */
		bool initWebServer(void);

		/*! *******************************************************************************************
		 * @brief stopWebServer	stops the web server daemon and clears the page list inside the http manager
		 * ********************************************************************************************
		 */
		void stopWebServer(void);

		/*! *******************************************************************************************
		 * @brief initGpioInterface initializes the gpioInterface object by calling its init() method
		 * @return true upon succesfull initialization, false otherwise
		 * ********************************************************************************************
		 */
		bool initGpioInterface(void);

		/*! ************************************************************************************************
		 * @brief closeGpioInterface close the GPIO interface by calling it reset() method
		 * ********************************************************************************************
		 */
		void closeGpioInterface(void);

		/*! ****************************************************************************************
         * @brief initSectionBoards set CAN IDs, initialize MsgQueues, start the thread of each section
         * @return true if success, false otherwise
		 * *******************************************************************************************
		 */
		bool initSectionBoards(void);

		/*! ******************************************************************************************
		 * @brief stopSectionBoards
		 * *********************************************************************************************
		 */
		void stopSectionBoards(void);

		/*! ***********************************************************************************************
		 * @brief initSectionBoardsManagers
		 * @return
		 * ************************************************************************************************
		 */
		bool initSectionBoardManagers(void);

		/*! *********************************************************************************************
		 * @brief stopSectionBoardManagers
		 * ************************************************************************************************
		 */
		void stopSectionBoardManagers(void);

		/*! ***********************************************************************************************
		 * @brief initNSHBoardManager
		 * @return
		 * ************************************************************************************************
		 */
		bool initNSHBoardManager(void);

		/*! **********************************************************************************************
		 * @brief stopNSHBoardManager
		 * ************************************************************************************************
		 */
		void stopNSHBoardManager(void);

		/*! ***********************************************************************************************
		 * @brief initCameraBoardManager
		 * @return
		 * ************************************************************************************************
		 */
		bool initCameraBoardManager(void);

		/*! **********************************************************************************************
		 * @brief stopCameraBoardManager
		 * ************************************************************************************************
		 */
		void stopCameraBoardManager(void);

		/*! ***********************************************************************************************
		 * @brief initMasterBoardManager
		 * @return true if succesfull false otherwise
		 * **********************************************************************************************
		 */
		bool initMasterBoardManager(void);

		/*! *********************************************************************************************
		 * @brief stopMasterBoardManager
		 * *******************************************************************************************
		 */
		void stopMasterBoardManager(void);

		/*! **********************************************************************************************
		 * @brief  initUSBPeripherals initializes USB and the camera.
		 * @return true upon succesfull initialization, false otherwise.
		 * ************************************************************************************************
		 */
		bool initUSBPeripherals(void);

		/*! *******************************************************************************************
		 * @brief stopCamera close USB channel and free resources.
		 *******************************************************************************************
		 */
		void stopUSBPeripherals(void);

		/*! *********************************************************************************************
         * @brief initI2CPeripherals
		 * @return
		 * *************************************************************************************************
		 */
		bool initI2CPeripherals( void );

		/*! ***********************************************************************************************
		 * @brief stopI2CPeripherals close I2C peripherals and free resources
		 * ********************************************************************************************
		 */
		void stopI2CPeripherals(void);

		/*! *************************************************************************************************
		 * @brief  initSPIPeripherals initialize SPI peripheral and all the devices SPI related.
		 * @return true upon succesfull initialization, false otherwise.
		 * ********************************************************************************************
		 */
		bool initSPIPeripherals(void);

		/*! ***********************************************************************************************
		 * @brief stopSPIPeripherals close SPI peripheral and free resources.
		 * ************************************************************************************************
		 */
		void stopSPIPeripherals(void);

		/*! *********************************************************************************************
		 * @brief  initOperationManager initializes the m_operationManager object by calling its init() method
		 * @return true upon succesfull initialization, false otherwise
		 * *********************************************************************************************
		 */
		bool initOpManagerSrvCollector(void);

		/*! **********************************************************************************************
		 * @brief stopOperationManager	stop the OperationManager
		 * *********************************************************************************************
		 */
		void stopOpManagerSrvCollector(void);

		/*! ****************************************************************************************************
		 * @brief initTestManager	initialize test manager
		 * ************************************************************************************************
		 */
		bool initTestManager(void);

		/*! *************************************************************************************************
		 * @brief checkTestManager	control if test manager is still running
		 * *************************************************************************************************
		 */
		bool checkTestManager(void);

		/*! 	**************************************************************************************
		 * @brief stopTestManager	stop test manager
		 * ***********************************************************************************************
		 */
		void stopTestManager(void);

		/*!		****************************************************************************************
		 * @brief initPowerOffManager
		 * ************************************************************************************************************
		 */
		bool initPowerOffManager( void );

		/*!
		***********************************************************************************************************
		 * @brief stopPowerOffManager
		 * ************************************************************************************************************
		 */
		bool stopPowerOffManager( void );

		/*! ***********************************************************************************************************
		 * @brief initProtocolThread	init protocol thread
		 * ************************************************************************************************************
		 */
		bool initProtocolThread(void);

		/*!
		***********************************************************************************************************
		 * @brief stopProtocolThread	stop protocol thread
		 * ************************************************************************************************************
		 */
		void stopProtocolThread(void);

		/*!
		***********************************************************************************************************
		 * @brief startupInstrument	executes the instrument startup
		 * ************************************************************************************************************
		 */
		bool firstStartupInstrument(void);

		/*! ***********************************************************************************************************
		 * @brief initEventRecorder	init event recorder
		 * ************************************************************************************************************
		 */
		bool initEventRecorder();

		/*! ***********************************************************************************************************
		 * @brief stopEventRecorder	stop event recorder
		 * ************************************************************************************************************
		 */
		bool stopEventRecorder();

		/*!
		***********************************************************************************************************
		 * @brief shutdownInstrument	close the software
		 * ************************************************************************************************************
		 */
		void shutdownInstrument();

		/*!
		***********************************************************************************************************
		 * @brief getShutdown get the flag that command shutdown
		 * ************************************************************************************************************
		 */
		bool getShutdown();

		/*!
		***********************************************************************************************************
		 * @brief readLastShutdownInstrument read the shutdown file to check if the last shutdown was regular
		 * ************************************************************************************************************
		 */
		void readLastShutdownInstrument();

		/*!
		***********************************************************************************************************
		 * @brief updateApplicative	close the software
		 * ************************************************************************************************************
		 */
		void updateApplicative();

		/*!
		***********************************************************************************************************
		 * @brief getApplicativeUpdate get the flag that command update
		 * ************************************************************************************************************
		 */
		bool getApplicativeUpdate();

		/*!
		***********************************************************************************************************
		 * @brief reinitInstrument start the init procedure on devices enabled
		 * ************************************************************************************************************
		 */
		bool reinitInstrument();

		/*!
		***********************************************************************************************************
		 * @brief saveDisableState save to file the DISABLED status of devices
		 * ************************************************************************************************************
		 */
		bool saveDisableState();

		/*!
		***********************************************************************************************************
		 * @brief restoreDisableState read from file the DISABLED status of devices
		 * ************************************************************************************************************
		 */
		bool restoreDisableState();

		/*!
		***********************************************************************************************************
		 * @brief isSectionEnabled	checks according to configuration if a specific section is enabled
		 * @param section the section identifier
		 * @return true if enabled, false otherwise
		 * ************************************************************************************************************
		 */
		bool isSectionEnabled(int section);

		/*!
		***********************************************************************************************************
		 * @brief  setRootPolicy set root to 755. This is needed to get the log through apache2 from the SSW
		 * @return -1 in case of error, different otherwise
		 * ************************************************************************************************************
		 */
		int setRootPolicy(void);

	private:

		/*!
		***********************************************************************************************************
		 * @brief areSectionsEnabled	checks according to configuration if both sections are enabled
		 * @return true if enabled, false otherwise
		 * ************************************************************************************************************
		 */
		bool areSectionsEnabled();

		/*!
		***********************************************************************************************************
		 * @brief isNSHEnabled	checks according to configuration if NSH is enabled
		 * @return true if enabled, false otherwise
		 * ************************************************************************************************************
		 */
		bool isNSHEnabled(void);

		/*!
		***********************************************************************************************************
		 * @brief isCameraEnabled	checks according to configuration if Camera is enabled
		 * @return true if enabled, false otherwise
		 * ************************************************************************************************************
		 */
		bool isCameraEnabled(void);

    public:

        Log m_logger;
        EventRecorder m_eventRecorder;

        // Configurator
        bool m_bConfigIsOk;
        Config m_configurator;

        // Web server
        WebServerDaemon m_webServerDaemon;
        HttpPageContainer m_httpPageContainer;
        ProtocolValidator m_protocolValidator;

        // Gpio Interface
        GpioInterface m_gpioInterface;

        // Section boards "high level" management
        SectionBoardManager m_sectionBoardManager[SCT_NUM_TOT_SECTIONS];

        // NSH "high level" management
        NSHBoardManager m_NSHBoardManager;

        // MasterBoard "high level" management
        MasterBoardManager m_MasterBoardManager;

        // Section boards communication management
        SectionBoardLink m_sectionBoardLink[SCT_NUM_TOT_SECTIONS];

        // Camera boards "high level" management
        CameraBoardManager m_CameraBoardManager;

        //PowerOff management
        PowerOffManager	m_powerOffManager;

        // USB board
        USBLinkBoard m_USBBoard;
        SampleDetectionLink m_sampleDetector;

        // I2CBoard
        I2CLinkBoard m_i2cBoard;

        // SPI Board
        SPILinkBoard m_SPIBoard;

        // Instrument internal management
        OperationManager m_operationManager;
        ServiceCollector m_serviceCollector;

        // Test for peripheral devices
        TestManager	*m_pTestApp;

        // Login manager
        LoginManager m_loginManager;
        // State Machine
        StateMachine m_stateMachine;
        // instrument info container
        InstrumentInfo m_instrumentInfo;
        // Scheduling thread
        SchedulerThread m_SchedulerThread;
        // Protocol, Settings and Runwl Info structure
        ProtocolInfo m_protocolInfo[SCT_NUM_TOT_SECTIONS];
        RunwlInfo m_runwlInfo[SCT_NUM_TOT_SECTIONS];
		ReadSecInfo m_readSecInfo[SCT_NUM_TOT_SECTIONS];
        PressureSettings m_pressureSettings[SCT_NUM_TOT_SECTIONS * SCT_NUM_TOT_SLOTS];
        THRunProtocol m_THRunProtocol;
        THReplyProtocol m_THReplyProtocol;

   private :

        time_t m_startSeconds;

        bool m_bShutdown;
        bool m_bApplUpdate;
        bool m_bTestManagerActive;

};


/*!
***********************************************************************************************************
 * @brief stateMachineSingleton	to get the pointer to the only stateMachine object
 * @return the stateMachine pointer
 * ************************************************************************************************************
 */
StateMachine * stateMachineSingleton();

/*! ***********************************************************************************************************
 * @brief protocolSerialize		wrapper function to call the serialize of the only object protocolValidator
 * @param pCmd					OutgoingCommand pointer that must be serialized into an XML buffer string
 * @param strOutString [out]	the serialized string form of the command
 * @return						true upon succesful serialization, false otherwise
 * ************************************************************************************************************
 */
bool protocolSerialize(OutgoingCommand* pCmd, std::string& strOutString);

/*! ***********************************************************************************************************
 * @brief protocolDataSerialize	wrapper function to call the serializeDataToXML of the only object protocolValidator
 * @param section				the involved section
 * @param strip					the involved strip
 * @param strDataType			the data type string identifier
 * @param strOutString [out]	the serialized string form of the command
 * @return						true upon succesful serialization, false otherwise
 *								(in this case strOutString is empty)
 * ************************************************************************************************************
 */
bool protocolDataSerialize(uint8_t section, uint8_t strip, std::string strDataType, std::string& strOutString);

/*!
***********************************************************************************************************
 * @brief operationSingleton	to get the pointer to the only operationManager object
 * @return the operationManager pointer
 * ************************************************************************************************************
 */
OperationManager * operationSingleton();

/*!
***********************************************************************************************************
 * @brief infoSingleton	to get the pointer to the only instrumentInfo object
 * @return the instrumentInfo pointer
 * ************************************************************************************************************
 */
InstrumentInfo * infoSingleton();

/*!
***********************************************************************************************************
 * @brief sectionBoardManagerSingleton	to get the pointer to the only sectionBoardManager object per section
 * @param section the section index
 * @return the requested pointer
 * ************************************************************************************************************
 */
SectionBoardManager * sectionBoardManagerSingleton(int section);

/*!
***********************************************************************************************************
 * @brief nshBoardManagerSingleton	to get the pointer to the only nshBoardManager object
 * @return the requested pointer
 * *********************************************************************************************************
 */
NSHBoardManager * nshBoardManagerSingleton();

/*!
***********************************************************************************************************
 * @brief nshReaderGeneralSingleton	to get the pointer to the only nshGeneral object
 * @return the requested pointer
 * *********************************************************************************************************
 */
NSHReaderGeneral * nshReaderGeneralSingleton();

/*!
***********************************************************************************************************
 * @brief masterBoardManagerSingleton	to get the pointer to the only MasterBoardManager object
 * @return the requested pointer
 * *********************************************************************************************************
 */
MasterBoardManager * masterBoardManagerSingleton();

/*!
***********************************************************************************************************
 * @brief cameraBoardManagerSingleton	to get the pointer to the only cameraBoardManager object
 * @return the requested pointer
 * ************************************************************************************************************
 */
CameraBoardManager * cameraBoardManagerSingleton();

/*!
***********************************************************************************************************
 * @brief spiBoardLinkSingleton	to get the pointer to the only SPILinkBoard object
 * @return the requested pointer
 * ************************************************************************************************************
 */
SPILinkBoard* spiBoardLinkSingleton();

/*!
***********************************************************************************************************
 * @brief usbBoardLinkSingleton	to get the pointer to the only USBLinkBoard object
 * @return the requested pointer
 * ************************************************************************************************************
 */
USBLinkBoard* usbBoardLinkSingleton();

/*!
***********************************************************************************************************
 * @brief usbCameraSingleton	to get the pointer to the only CR8062Camera object
 * @return the requested pointer
 * ************************************************************************************************************
 */
CR8062Camera* usbCameraSingleton();

/*!
***********************************************************************************************************
 * @brief usbReaderSingleton	to get the pointer to the only CR8062Reader object
 * @return the requested pointer
 * ************************************************************************************************************
 */
CR8062Reader* usbReaderSingleton();

/**
***********************************************************************************************************
 * @brief sectionBoardGeneralSingleton	to get the pointer to the only sectionBoardGeneral object per section
 * @param section the section index
 * @return the requested pointer
 * ************************************************************************************************************
 */
SectionBoardGeneral * sectionBoardGeneralSingleton(int section);

/*!
***********************************************************************************************************
 * @brief sectionBoardLinkSingleton	to get the pointer to the only sectionBoardLink object per section
 * @param section the section index
 * @return the requested pointer
 * ************************************************************************************************************
 */
SectionBoardLink * sectionBoardLinkSingleton(int section);

/*!
***********************************************************************************************************
 * @brief protocolInfoPointer	to get the pointer to the protocolInfo object by index
 * @param index the protocol info index
 * @return the protocolInfo pointer
 * ************************************************************************************************************
 */
ProtocolInfo * protocolInfoPointer(int index);

/*!
***********************************************************************************************************
 * @brief runwlInfoPointer	to get the pointer to the runwlInfo object by index
 * @param index the protocol runwl index
 * @return the runwlInfo pointer
 * ************************************************************************************************************
 */
RunwlInfo * runwlInfoPointer(int index);

/*!
***********************************************************************************************************
 * @brief pressureSettingsPointer	to get the pointer to the pressureSettings object by index
 * @param section the section index
 * @param strip the strip index
 * @return the pressureSettings pointer
 * ************************************************************************************************************
 */
PressureSettings * pressureSettingsPointer(uint8_t section, uint8_t strip);

/*!
***********************************************************************************************************
 * @brief readSecInfoPointer	to get the pointer to the ReadSecInfo object
 * @param index the section index
 * @return the ReadSecInfo pointer
 * ************************************************************************************************************
 */
ReadSecInfo* readSecInfoPointer(int index);

/*!
***********************************************************************************************************
 * @brief schedulerThreadPointer	to get the pointer to the SchedulerThread object
 * @return the SchedulerThread pointer
 * ************************************************************************************************************
 */
SchedulerThread * schedulerThreadPointer();

/*!
***********************************************************************************************************
 * @brief protocolThreadPointer	to get the pointer to the THRunProtocol object
 * @return the THRunProtocol pointer
 * ************************************************************************************************************
 */
THRunProtocol * protocolThreadPointer();

/*!
***********************************************************************************************************
 * @brief protocolReplyThreadPointer	to get the pointer to the THReplyProtocol object
 * @return the THReplyProtocol pointer
 * ************************************************************************************************************
 */
THReplyProtocol * protocolReplyThreadPointer();

/*! ***********************************************************************************************************
 * @brief getMasterErrorInfoFromCode to search in the MasterBoard error list the one with the requested code and
 *				return the corresponding info (don't care about active status)
 * @param code the error code
 * @return the string formatted as timestamp#code#category#description#component
 * ************************************************************************************************************
 */
std::string getMasterErrorInfoFromCode(uint16_t code);

/*!
***********************************************************************************************************
 * @brief requestVidasEp	to wake up the VidasEp thread and send the VidasEp message to PC
 * @return 0 if successfull
 * ************************************************************************************************************
 */
int requestVidasEp();

/*!
***********************************************************************************************************
 * @brief requestStartButton	to send the notification the StartButton has been pressed
 * @param section the involved section
 * @return 0 if successfull
 * ************************************************************************************************************
 */
int requestStartButton(uint8_t section);

/*!
***********************************************************************************************************
 * @brief requestModuleLed	to ask the MasterBoard thread to set the led status
 * @return true if successfull
 * ************************************************************************************************************
 */
bool requestModuleLed();

/*! ***********************************************************************************************************
 * @brief changeDeviceStatus to set the status of a specific device: the function is responsible to request
 *				the vidasep message to gateway
 * @param device the device id according to eDeviceId enum
 * @param status the device status according to eDeviceStatus enum
 * @return	true upon succesfull setting, false otherwise
 * ************************************************************************************************************
 */
bool changeDeviceStatus(eDeviceId device, eDeviceStatus status);

/*! ***********************************************************************************************************
 * @brief checkErrorsOnSection call the checkErrorsStatus of SectionBoardManager
 * @param section the section index
 * @return	the return value of the SectionBoardManager function
 * ************************************************************************************************************
 */
int checkErrorsOnSection(uint8_t section);

/*! ***********************************************************************************************************
 * @brief resetSectionBoard to reset via hardware the section board
 * @param section the section to reset
 * @return	0 upon succesfull setting, value != 0 otherwise
 * ************************************************************************************************************
 */
int resetSectionBoard(uint8_t section);

/*! ***********************************************************************************************************
 * @brief registerErrorEvent call the EventRecorder object to record the error event on file
 * @param section the section index
 * @param strDevice the device string
 * @param strCode the error code
 * @param liLevel the error level
 * @param strDescription the error description
 * @param enabled true if errro detectin is enabled
 * @param active true if error was already active
 * @return true upon succesfull writing, false otherwise
 * ************************************************************************************************************
 */
bool registerErrorEvent(uint8_t section, const string& strDevice,
					   const string& strCode, int liLevel,
					   const string& strDescription, bool enabled, bool active);

/*! ***********************************************************************************************************
 * @brief registerGenericEvent call the EventRecorder object to record an event on file
 * @param strEvent the event string
 * @return	true upon succesfull writing, false otherwise
 * ************************************************************************************************************
 */
bool registerGenericEvent(std::string strEvent);

/*! ***********************************************************************************************************
 * @brief registerCommandEvent call the EventRecorder object to record a command on file
 * @param strEvent the command string
 * @return	true upon succesfull writing, false otherwise
 * ************************************************************************************************************
 */
bool registerCommandEvent(const char * strEvent);

/*! ***********************************************************************************************************
 * @brief registerTemperatures call the EventRecorder object to record the actual temperatures
 * @return	true upon succesfull writing, false otherwise
 * ************************************************************************************************************
 */
bool registerTemperatures();

/*! ***********************************************************************************************************
 * @brief registerVoltages call the EventRecorder object to record the actual voltages
 * @return	true upon succesfull writing, false otherwise
 * ************************************************************************************************************
 */
bool registerVoltages();

/*! ***********************************************************************************************************
 * @brief registerFans call the EventRecorder object to record the actual fans
 * @return	true upon succesfull writing, false otherwise
 * ************************************************************************************************************
 */
bool registerFans();

/*! ***********************************************************************************************************
 * @brief registerSolidStandardReadings call the EventRecorder object to record the result of the NSH autocalibration
 * @param strStatus the description string
 * @param liFluoVal the measure value
 * @param liGoldenSS the reference value
 * @return	true upon succesfull writing, false otherwise
 * ************************************************************************************************************
 */
bool registerSolidStandardReadings(std::string strStatus, int liFluoVal, int liGoldenSS);

/*! ***********************************************************************************************************
 * @brief registerOPTCalibration call the EventRecorder object to record the result of the OPT calibration
 * @param strSection the section used
 * @param strSlot the slot used
 * @param strTarget the target value
 * @param strGain the OPT gain
 * @param strValue the read value
 * @param strID the OPT id
 * @return	true upon succesfull writing, false otherwise
 * ************************************************************************************************************
 */
bool registerOPTCalibration(std::string strSection, std::string strSlot, std::string strTarget, std::string strGain,
                            std::string strValue, std::string strID);

/*!
***********************************************************************************************************
 * @brief getMsecsFromStart	read from the system the msecs elapsed since startup
 * @return the msecs value
 * ************************************************************************************************************
 */
uint64_t getClockFromStart();

/*!
***********************************************************************************************************
 * @brief getFormattedTime	read the system date-time and return it formatted
 * @param bSeparator if true -> 2020-02-02T12:05:27 otherwise 20200202120527
 * @return the formatted string
 * ************************************************************************************************************
 */
std::string getFormattedTime(bool bSeparator);

/*!
***********************************************************************************************************
 * @brief getFormattedTimeFromSeconds	receive the time in seconds and return it formatted
 * @param seconds the input seconds value
 * @param bSeparator if true -> 2020-02-02T12:05:27 otherwise 20200202120527
 * @return the formatted string
 * ************************************************************************************************************
 */
std::string getFormattedTimeFromSeconds(uint64_t seconds, bool bSeparator);

/*!
***********************************************************************************************************
 * @brief getFormattedTime	read the system date-time and return it formatted as
 *  2020-02-02T12:05:27.324
 * @return the formatted string
 * ************************************************************************************************************
 */
std::string getVidasepTimestamp();

/*! ***********************************************************************************************************
 * @brief showTime  Test function to visualize UTC time and formatted timestamp
 * ***********************************************************************************************************
 */
void showTime(void);

/*! ************************************************************************************************************
 * @brief getSecondsTime	read from the system the current date-time and convert it to secs
 * @return the secs value
 * ************************************************************************************************************
 */
time_t getSecondsTime();


#endif // MAINEXECUTOR_H
