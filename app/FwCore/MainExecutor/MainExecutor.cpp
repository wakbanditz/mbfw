/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    MainExecutor.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the MainExecutor class.
 @details

 ****************************************************************************
*/

#include <sys/resource.h>

#include <time.h>
#include <fstream>
#include <ctime>
#include <unistd.h>


#include "MainExecutor.h"

#include "HttpPageProcessCommand.h"
#include "WebServerAndProtocolInclude.h"

#include "TestManager.h"
#include "THRunProtocol.h"
#include "THVidasEp.h"

#define SHUTDOWN_FILE		"shutdown.cfg"
#define DISABLED_SETUP_FILE	"disabled.cfg"

static bool writeShutdownFile(bool status);
static bool readShutdownFile();
static std::string fromTimeStructToTimeString(struct tm * timeNow, bool bSeparator);
static bool registerStateMachine();

// ----------------------------------------------------------------------------- //

MainExecutor::MainExecutor()
{
	m_bConfigIsOk = false;
	m_bShutdown = false;
	m_bApplUpdate = false;
    m_bTestManagerActive = false;
}

MainExecutor::~MainExecutor()
{

}

bool MainExecutor::initConfigurator(void)
{
	char sConfigFileName[1024];
	char sError[1024];

	/* ********************************************************************************************
	 * READS CONFIGURATION FILE
	 * ********************************************************************************************
	 */
	config_key_t config_keys[] =
	{
		{ CFG_FW_CORE_LOG_ENABLE, (char*)"log.FwCore.enable", (char*)"GENERAL", T_int, (char*)"1", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_LOG_FILE, (char*)"log.FwCore.file", (char*)"GENERAL", T_string, (char*)"stdout", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_LOG_LEVEL, (char*)"log.FwCore.level", (char*)"GENERAL", T_int, (char*)"6", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_LOG_NUM_FILES, (char*)"log.FwCore.numFiles", (char*)"GENERAL", T_int, (char*)"5", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_SCT_A_ENABLE, (char*)"fwCore.sectionA.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_SCT_B_ENABLE, (char*)"fwCore.sectionB.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_WEB_ENABLE, (char*)"fwCore.webServer.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_GPIO_IF_ENABLE, (char*)"fwCore.gpioInterface.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_CAM_ENABLE, (char*)"fwCore.camera.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_POWER_MAN_ENABLE, (char*)"fwCore.powerManager.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_EEPROM_ENABLE, (char*)"fwCore.eeprom.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_FAN_CTRL_ENABLE, (char*)"fwCore.fanController.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_AUDIO_CODEC_ENABLE, (char*)"fwCore.audioCodec.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_GPIO_EXP_SEC_ENABLE, (char*)"fwCore.gpioExpanderSection.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_LED_DRV_ENABLE, (char*)"fwCore.ledDriver.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_NSH_ENABLE, (char*)"fwCore.nsh.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_STEPPER_MOTOR_ENABLE, (char*)"fwCore.stepperMotor.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_ENCODER_COUNTER_ENABLE, (char*)"fwCore.encoder.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_OPER_MAN_ENABLE, (char*)"fwCore.operationManager.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_TESTMANAGER_ENABLE, (char*)"fwCore.testManager.enable", (char*)"GENERAL", T_string, (char*)"false", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_EVENTRECORDER_ENABLE, (char*)"fwCore.eventRecorder.enable", (char*)"GENERAL", T_string, (char*)"false", DEFAULT_ACCEPTED},
		{ CFG_FW_CORE_GPIO_EXP_LED_ENABLE, (char*)"fwCore.gpioExpanderLed.enable", (char*)"GENERAL", T_string, (char*)"true", DEFAULT_ACCEPTED}
	};
	int num_keys = sizeof(config_keys) / sizeof(config_key_t);

	strcpy(sConfigFileName, CONF_FILE);
	if(sConfigFileName[0] == 0)
	{
		printf("Configuration file name NOT valid");
		return false;
	}

	if ( !m_configurator.Init(config_keys, num_keys, sConfigFileName) )
	{
		Config::GetLastError(sError, 1024);
		printf("Impossible to read configuration file <%s> (error: %s)", sConfigFileName, sError);
		return false;
	}
	m_bConfigIsOk = true;
	return true;
}

bool MainExecutor::initLogger(void)
{
	m_pLogger = &m_logger;
	int liLogEnable = m_configurator.GetInt(CFG_FW_CORE_LOG_ENABLE);
	bool bEnable = (liLogEnable > 0 ? true : false);
	m_pLogger->enable(bEnable);
	m_pLogger->setAppName(PROGRAM_NAME);
	m_pLogger->setFile(m_configurator.GetString(CFG_FW_CORE_LOG_FILE));
	m_pLogger->setLevel(m_configurator.GetInt(CFG_FW_CORE_LOG_LEVEL));
	m_pLogger->setMaxNumFiles(m_configurator.GetInt(CFG_FW_CORE_LOG_NUM_FILES));
	return true;
}

bool MainExecutor::initEventRecorder()
{
	bool bEnable = false;

	string strEnabled(m_configurator.GetString(CFG_FW_CORE_EVENTRECORDER_ENABLE));
	if ( strEnabled.compare("true") == 0 )
	{
		bEnable = true;
	}

	m_eventRecorder.initEventRecorder(bEnable, APPLOGS_FOLDER);
	return true;
}

bool MainExecutor::stopEventRecorder()
{
	m_eventRecorder.stopEventRecorder();
	return true;
}

void MainExecutor::shutdownInstrument()
{
	// save flag on file
	writeShutdownFile(true);

	// stop main loop
	m_bShutdown = true;
}

bool MainExecutor::getShutdown()
{
	return m_bShutdown;
}

void MainExecutor::readLastShutdownInstrument()
{
	// read flag saved on file
	bool status = readShutdownFile();
	if(status == false)
	{
		// last shutdown not correct
		registerGenericEvent("Last Shutdown not correct");
		// set the error and send it to gateway
		m_MasterBoardManager.setErrorFromCode(ERR_SHUTDOWN_NOT_CORRECT, true);
	}

	// by default set shutdown failed
	writeShutdownFile(false);
}

void MainExecutor::updateApplicative()
{
	m_bApplUpdate = true;
}

bool MainExecutor::getApplicativeUpdate()
{
	return m_bApplUpdate;
}

bool MainExecutor::initTimeValues()
{
	time_t t = time(NULL);
	struct tm timeNow = *localtime(&t);

	log(LOG_INFO, "MainExec::start time : %04d-%02d-%02dT%02d:%02d:%02d",
		timeNow.tm_year + 1900, timeNow.tm_mon + 1, timeNow.tm_mday,
		timeNow.tm_hour, timeNow.tm_min, timeNow.tm_sec);

	m_startSeconds = mktime(&timeNow);

	uint64_t uptime_clocks = getClockFromStart();

	m_instrumentInfo.setStartTime(&timeNow, uptime_clocks);
	return true;
}

bool MainExecutor::setTimeValues()
{
	time_t totalSeconds2 = getSecondsTime();
	uint64_t timeClock_2 = getClockFromStart();

	float cklRatio = ((float)(timeClock_2 - m_instrumentInfo.getClockStart()) / (float)(totalSeconds2 - m_startSeconds));

	m_instrumentInfo.setClockRatio(cklRatio);
	return  true;
}

bool MainExecutor::initLoginManager()
{
	return m_loginManager.init();
}

bool MainExecutor::initInstrumentInfo()
{
	return m_instrumentInfo.init();
}

bool MainExecutor::stopInstrumentInfo()
{
	return m_instrumentInfo.stop();
}

bool MainExecutor::initStateMachine()
{
	return m_stateMachine.init();
}

bool MainExecutor::initScheduler()
{
	m_SchedulerThread.setLogger(getLogger());
	return m_SchedulerThread.startThread();
}

bool MainExecutor::closeScheduler()
{
	m_SchedulerThread.stopThread();
	log(LOG_INFO, "MainExec::stopScheduler: done");
	return true;
}

bool MainExecutor::idleStateMachine()
{
	return m_stateMachine.idle();
}

bool MainExecutor::initIPCConfig( void )
{
	struct rlimit rlim;
	memset(&rlim, 0, sizeof(rlim));
	rlim.rlim_cur = RLIM_INFINITY;
	rlim.rlim_max = RLIM_INFINITY;

	int res = setrlimit(RLIMIT_MSGQUEUE, &rlim);
	if ( res != 0)
	{
		log(LOG_ERR, "MainExec::initIPCConfig: <%s>", strerror(errno));
		return false;
	}

	return true;
}

bool MainExecutor::initWebServer(void)
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::initSectionBoards: fw not configured");
		return false;
	}

	string strWebEnabled(m_configurator.GetString(CFG_FW_CORE_WEB_ENABLE));
	if ( strWebEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::initWebServer: web server not enabled");
		return true;
	}

	/* ****************************************************************************************************************
	 * XmlProtocol validator initialization
	 * ****************************************************************************************************************
	 */
	m_protocolValidator.setLogger(getLogger());
	if ( !m_protocolValidator.init() )
	{
		log(LOG_ERR, "MainExec::initWebServer: error, unable to start xerces XML parser");
		return false;
	}

	/* ****************************************************************************************************************
	 * HttpPageContainer initialization
	 * ****************************************************************************************************************
	 */
	m_httpPageContainer.setLogger(getLogger());

    std::string serialNumber;
    serialNumber.assign(infoSingleton()->getInstrumentSerialNumber());
    if(serialNumber.empty() == true)
    {
        serialNumber.assign(WEB_HEADER_SERIAL_NUMBER_VALUE);
    }

    HttpPageProcessCommand* p = new HttpPageProcessCommand(WEB_PAGE_PROCESS_COMMAND_NAME, serialNumber);
	p->setLogger(getLogger());


	m_httpPageContainer.addPage(*p);

	/* ****************************************************************************************************************
	 * WebServerDaemon initialization
	 * ****************************************************************************************************************
	 */

    /* ********************************************************************************************
     * READS CONFIGURATION FILE
     * ********************************************************************************************
     */
    m_webServerDaemon.setLogger(getLogger());

    std::string strListenAddress;
    uint16_t intListenPort;

    strListenAddress.assign(WEB_SERVER_DEFAULT_LISTEN_ADDRESS);
    intListenPort = WEB_SERVER_DEFAULT_LISTEN_PORT; // 29092

    char sConfigFileName[1024] = CONF_FILE;
    char sError[1024];

    Config config;
    config_key_t config_keys[] =
    {
        { CFG_WEB_ADDRESS_DEFAULT, (char*)"webServer.address", (char*)"GENERAL", T_string, (char*)WEB_SERVER_DEFAULT_LISTEN_ADDRESS, DEFAULT_ACCEPTED},
        { CFG_WEB_PORT_DEFAULT, (char*)"webServer.port", (char*)"GENERAL", T_int, (char*)"0", DEFAULT_ACCEPTED}
    };

    int num_keys = sizeof(config_keys) / sizeof(config_key_t);
    if ( !config.Init(config_keys, num_keys, sConfigFileName) )
    {
        Config::GetLastError(sError, 1024);
        log(LOG_ERR, "Impossible to read configuration file <%s> (error: %s)", sConfigFileName, sError);
    }

    strListenAddress.assign(config.GetString(CFG_WEB_ADDRESS_DEFAULT));
    if ( strListenAddress.compare(WEB_SERVER_DEFAULT_LISTEN_ADDRESS) == 0 )
    {
        // all address are enabled -> empty string
        strListenAddress.clear();
    }
    intListenPort = config.GetInt(CFG_WEB_PORT_DEFAULT);
    log(LOG_INFO, "MainExec::initWebServer: listen on %s port %d", strListenAddress.c_str(), intListenPort);

    if ( !m_webServerDaemon.init(strListenAddress,  intListenPort) )
	{
		log(LOG_ERR, "MainExec::initWebServer: error, unable to initialize webServerDaemon");
		return false;
	}
	m_webServerDaemon.setConnectionProcessorFunction(HttpPageContainer::processConnection);
	m_webServerDaemon.setProcessorData((void*)&m_httpPageContainer);
	if ( !m_webServerDaemon.startThread() )
	{
		log(LOG_ERR, "MainExec::initWebServer: error, unable to start WebServerDaemon");
		return false;
	}
	log(LOG_INFO, "MainExec::initWebServer: done");
	return true;
}

void MainExecutor::stopWebServer(void)
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::stopWebServer: fw not configured");
		return;
	}
	string strWebEnabled(m_configurator.GetString(CFG_FW_CORE_WEB_ENABLE));
	if ( strWebEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::stopWebServer: web server not enabled");
		return;
	}
	m_webServerDaemon.stopThread();
	m_httpPageContainer.removeAll();
	log(LOG_INFO, "MainExec::stopWebServer: done");
}

bool MainExecutor::initGpioInterface(void)
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::initGpioInterface: fw not configured");
		return false;
	}

	string strEnabled(m_configurator.GetString(CFG_FW_CORE_GPIO_IF_ENABLE));
	if ( strEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::initGpioInterface: GPIOs not enabled");
		return true;
	}

	m_gpioInterface.setLogger(getLogger());
	if( ! m_gpioInterface.init(CONF_FILE) )
	{
		log(LOG_ERR, "MainExec::initGpioInterface: error, unable to initialize camera gpioInterface");
		return false;
	}
	log(LOG_INFO, "MainExec::initGpioInterface: done");
	return true;
}

void MainExecutor::closeGpioInterface(void)
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::closeGpioInterface: fw not configured");
		return;
	}
	string strEnabled(m_configurator.GetString(CFG_FW_CORE_GPIO_IF_ENABLE));
	if ( strEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::closeGpioInterface: GPIOs not enabled");
		return;
	}
	m_gpioInterface.closeInterface();
}

bool MainExecutor::initSectionBoards(void)
{
	int iRet = 0;
	bool bStartThread = true;

	// Initialize Section A
	iRet += m_sectionBoardLink[SCT_A_ID].initLogger(getLogger());
    iRet += m_sectionBoardLink[SCT_A_ID].setSectionBoardNumber(SCT_A_ID);
	iRet += m_sectionBoardLink[SCT_A_ID].setIDs(SCT_A_ID, CAN_SECTION_A_BOARD_ID_MASTER, CAN_SECTION_A_BOARD_ID_TX,
												CAN_SECTION_A_BOARD_ID_UPFW, CAN_MASTER_TO_SECTION_A_UPFW_TX );

#ifndef __DEBUG_PROTOCOL
    iRet += m_sectionBoardLink[SCT_A_ID].initQueueTx(SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_A,		SCT_IDX_MQ_GPF_CANLINK);
	iRet += m_sectionBoardLink[SCT_A_ID].initQueueTx(SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_UPFW_A, SCT_IDX_MQ_BPF_CANLINK);
	iRet += m_sectionBoardLink[SCT_A_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_A, 0);
	iRet += m_sectionBoardLink[SCT_A_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX1_A, 1);
	iRet += m_sectionBoardLink[SCT_A_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX2_A, 2);
	iRet += m_sectionBoardLink[SCT_A_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX3_A, 3);
	iRet += m_sectionBoardLink[SCT_A_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX4_A, 4);
	iRet += m_sectionBoardLink[SCT_A_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX5_A, 5);
	iRet += m_sectionBoardLink[SCT_A_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX6_A, 6);
	iRet += m_sectionBoardLink[SCT_A_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_UPFW_A, 7);
#endif
	iRet += m_sectionBoardLink[SCT_A_ID].initSectionParts();
#ifndef __DEBUG_PROTOCOL
	iRet += m_sectionBoardLink[SCT_A_ID].setCanLinkUpFw(SCT_MQ_CAN_LINK_NAME_A_UPFW_BIN_ANSW);
#endif

	//Check initialization of Section boards
	if ( iRet != 0 )
	{
		log(LOG_ERR, "MainExec::initSectionBoards: impossible to init section board A");
		bStartThread = false;
	}

	iRet = 0;
	// Initialize Section B
	iRet += m_sectionBoardLink[SCT_B_ID].initLogger(getLogger());
    iRet += m_sectionBoardLink[SCT_B_ID].setSectionBoardNumber(SCT_B_ID);
	iRet += m_sectionBoardLink[SCT_B_ID].setIDs(SCT_B_ID, CAN_SECTION_B_BOARD_ID_MASTER, CAN_SECTION_B_BOARD_ID_TX,
												CAN_SECTION_B_BOARD_ID_UPFW, CAN_MASTER_TO_SECTION_B_UPFW_TX);

#ifndef __DEBUG_PROTOCOL
    iRet += m_sectionBoardLink[SCT_B_ID].initQueueTx(SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_B,		SCT_IDX_MQ_GPF_CANLINK);
	iRet += m_sectionBoardLink[SCT_B_ID].initQueueTx(SCT_MQ_CAN_LINK_TO_MANAGER_NAME_TX_UPFW_B, SCT_IDX_MQ_BPF_CANLINK);
	iRet += m_sectionBoardLink[SCT_B_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_B, 0);
	iRet += m_sectionBoardLink[SCT_B_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX1_B, 1);
	iRet += m_sectionBoardLink[SCT_B_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX2_B, 2);
	iRet += m_sectionBoardLink[SCT_B_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX3_B, 3);
	iRet += m_sectionBoardLink[SCT_B_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX4_B, 4);
	iRet += m_sectionBoardLink[SCT_B_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX5_B, 5);
	iRet += m_sectionBoardLink[SCT_B_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_AUX6_B, 6);
	iRet += m_sectionBoardLink[SCT_B_ID].initQueueRx(SCT_MQ_CAN_MANAGER_TO_LINK_NAME_RX_UPFW_B, 7);
#endif

	iRet += m_sectionBoardLink[SCT_B_ID].initSectionParts();
#ifndef __DEBUG_PROTOCOL
    iRet += m_sectionBoardLink[SCT_B_ID].setCanLinkUpFw(SCT_MQ_CAN_LINK_NAME_B_UPFW_BIN_ANSW);
#endif

	//Check initialization of Section boards
	if ( iRet != 0 )
	{
		log(LOG_ERR, "MainExec::initSectionBoards: impossible to init section board B");
		bStartThread = false;
	}

	if(!bStartThread) return false;

#ifndef __DEBUG_PROTOCOL

	for ( int i = 0; i < SCT_NUM_TOT_SECTIONS; i++ )
	{
		if ( !m_sectionBoardLink[i].startThread() )
		{

			log(LOG_ERR, "MainExec::initSectionBoards: impossible to start receiver thread on SCT %d", i);
			return false;
		}

        m_sectionBoardLink[i].setEnabled(isSectionEnabled(i));

		if(isSectionEnabled(i))
		{
			m_sectionBoardLink[i].sendMsgAck();
		}
	}
#endif

	return true;
}

void MainExecutor::stopSectionBoards(void)
{
	for ( int i = 0; i < SCT_NUM_TOT_SECTIONS; i++ )
	{
		m_sectionBoardLink[i].destroySectionParts();
		m_sectionBoardLink[i].stopReceiveMsgFromManagerThread();
		m_sectionBoardLink[i].closeAndUnlinkMsgQueue();
	}

	log(LOG_INFO, "MainExec::stopSectionBoards: done");
}

bool MainExecutor::initSectionBoardManagers()
{
	m_sectionBoardManager[SCT_A_ID].setLogger(getLogger());
	m_sectionBoardManager[SCT_A_ID].setSectionId(SCT_A_ID);
	m_sectionBoardManager[SCT_A_ID].setSectionLink(&m_sectionBoardLink[SCT_A_ID]);

	m_sectionBoardManager[SCT_B_ID].setLogger(getLogger());
	m_sectionBoardManager[SCT_B_ID].setSectionId(SCT_B_ID);
	m_sectionBoardManager[SCT_B_ID].setSectionLink(&m_sectionBoardLink[SCT_B_ID]);

	log(LOG_INFO, "MainExec::initSectionManagers: done");
	return true;
}

void MainExecutor::stopSectionBoardManagers()
{
	// TODO to be completed

	log(LOG_INFO, "MainExec::stopSectionManagers: done");
}

bool MainExecutor::initNSHBoardManager()
{
	m_NSHBoardManager.setLogger(getLogger());
	m_NSHBoardManager.initNSHBoardManager();
	log(LOG_INFO, "MainExec::initNSHBoardManager: done");

	return true;
}

void MainExecutor::stopNSHBoardManager()
{
	log(LOG_INFO, "MainExec::stopNSHBoardManager: done");
}

bool MainExecutor::initCameraBoardManager()
{
	m_CameraBoardManager.setLogger(getLogger());
	m_CameraBoardManager.initCameraBoardManager();
	log(LOG_INFO, "MainExec::initCameraBoardManager: done");

	return true;
}

void MainExecutor::stopCameraBoardManager()
{
	log(LOG_INFO, "MainExec::stopCameraBoardManager: done");
}

void MainExecutor::stopMasterBoardManager()
{
    m_MasterBoardManager.stopMasterBoardManager();
	log(LOG_INFO, "MainExec::stopMasterBoardManager: done");
}

bool MainExecutor::initMasterBoardManager()
{
	m_MasterBoardManager.setLogger(getLogger());
	m_MasterBoardManager.initMasterBoardManager();
    log(LOG_INFO, "MainExec::initMasterBoardManager: done");
    return true;
}

bool MainExecutor::initUSBPeripherals(void)
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::initUSBPeripherals: fw not configured");
		return false;
	}

	/* ****************************************************************************************************************
	 * Set logger in USB board
	 * ****************************************************************************************************************
	 */
	m_USBBoard.setLogger(getLogger());

	/* ****************************************************************************************************************
	 * USB interfaces initialization
	 * ****************************************************************************************************************
	 */
	if ( m_USBBoard.createAndInitUSBInterfaces() != 0 )
	{
		log(LOG_ERR, "MainExec::initUSBPeripherals: error, unable to create USB interface");
		return false;
	}

	/* ****************************************************************************************************************
	 * Camera initialization
	 * ****************************************************************************************************************
	 */
	string strCamEnabled(m_configurator.GetString(CFG_FW_CORE_CAM_ENABLE));
	if ( strCamEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::initUSBPeripherals: camera not enabled");
		return true;
	}
	else
	{
		if ( m_USBBoard.createAndInitCameraReader() != 0 )
		{
			log(LOG_ERR, "MainExec::initUSBPeripherals: unable to initialize Camera device");
			return false;
		}
		log(LOG_INFO, "MainExec::initUSBPeripherals: camera enabled");
	}

	return true;
}

void MainExecutor::stopUSBPeripherals(void)
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::stopUSBPeripherals: fw not configured");
		return;
	}

	string strEnabled(m_configurator.GetString(CFG_FW_CORE_CAM_ENABLE));
	if ( strEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::stopUSBPeripherals: camera not enabled");
		return;
	}
	else
	{
		m_USBBoard.destroyCameraReader();
		log(LOG_INFO, "MainExec::stopUSBPeripherals: camera correctly closed");
	}

	m_USBBoard.destroyUSBInterfaces();
	log(LOG_INFO, "MainExec::stopUSBPeripherals: done.");
}

bool MainExecutor::initI2CPeripherals( void )
{

	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::initI2CPeripherals: fw not configured");
		return false;
	}

	/* ****************************************************************************************************************
	 * Set logger in I2C board
	 * ****************************************************************************************************************
	 */
	m_i2cBoard.setLogger(getLogger());

	/* ****************************************************************************************************************
	 * I2C interfaces initialization
	 * ****************************************************************************************************************
	 */
	if ( m_i2cBoard.createI2CInterfaces() != 0 )
	{
		log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize I2C channels");
		return false;
	}

	/* ****************************************************************************************************************
	 * I2C devices initialization
	 * ****************************************************************************************************************
	 */
	// Power Manager
	string strPMEnabled(m_configurator.GetString(CFG_FW_CORE_POWER_MAN_ENABLE));
	if ( strPMEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::initI2CPeripherals: Power Manager not enabled");
	}
	else
	{
		if ( m_i2cBoard.createAndInitPowerManager() != 0 )
		{
			log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize PowerManager");
			return false;
		}
		log(LOG_INFO, "MainExec::initI2CPeripherals: Power Manager enabled");
	}

	// GPIO Expander SECTION
	string strGEEnabled(m_configurator.GetString(CFG_FW_CORE_GPIO_EXP_SEC_ENABLE));
	if (strGEEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::initI2CPeripherals: GPIO Expander not enabled");
	}
	else
	{
		if (isSectionEnabled(SCT_A_ID))
		{
			if ( m_i2cBoard.createAndInitGpioExpanderA() != 0 )
			{
				log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize GPIO Expander A");
				return false;
			}
			log(LOG_INFO, "MainExec::initI2CPeripherals: GPIO Expander A enabled");
		}

		if (isSectionEnabled(SCT_B_ID))
		{
			if ( m_i2cBoard.createAndInitGpioExpanderB() != 0 )
			{
				log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize GPIO Expander B");
				return false;
			}
			log(LOG_INFO, "MainExec::initI2CPeripherals: GPIO Expander B enabled");
		}
	}

	// Status Led Driver
	string strLedDriverEnabled(m_configurator.GetString(CFG_FW_CORE_LED_DRV_ENABLE));
	if (strLedDriverEnabled.compare("true") != 0)
	{
		log(LOG_INFO,"MainExec::initI2CPeripherals: LED DRIVER not enabled");
	}
	else
    {
        bool bGEExpLedEnabled = false;
        // GPIO Expander LED
        string strGELEDEnabled(m_configurator.GetString(CFG_FW_CORE_GPIO_EXP_LED_ENABLE));
        if (strGELEDEnabled.compare("true") != 0 )
        {
            log(LOG_INFO, "MainExec::initI2CPeripherals: GPIO Expander LED not enabled");
        }
        else
        {
            if ( m_i2cBoard.createAndInitGpioExpanderLed() != 0 )
            {
                log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize GPIO Expander LED");
                return false;
            }
            bGEExpLedEnabled = true;
            log(LOG_INFO, "MainExec::initI2CPeripherals: GPIO Expander LED enabled");

        }

        if ( m_i2cBoard.createLedDriver(bGEExpLedEnabled) != 0 )
        {
            log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize Led Driver");
            return false;
        }
        log(LOG_INFO, "MainExec::initI2CPeripherals: Led Driver enabled");
    }

	// Fan Controller
	string strFanControllerEnabled(m_configurator.GetString(CFG_FW_CORE_FAN_CTRL_ENABLE));
	if (strFanControllerEnabled.compare("true") != 0)
	{
		log(LOG_INFO,"MainExec::initI2CPeripherals: FAN CONTROLLER not enabled");
	}
	else
	{
		if ( m_i2cBoard.createAndInitFanController() != 0 )
		{
			log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize Fan Controller");
			return false;
		}
		log(LOG_INFO, "MainExec::initI2CPeripherals: Fan Controller enabled");
	}

	// EEprom
	string strEEEnabled(m_configurator.GetString(CFG_FW_CORE_EEPROM_ENABLE));
	if ( strEEEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::initI2CPeripherals: EEprom not enabled");
	}
	else
	{
		if ( m_i2cBoard.createAndInitEEprom() != 0 )
		{
			log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize EEprom");
			return false;
		}
		log(LOG_INFO, "MainExec::initI2CPeripherals: EEprom enabled");
	}

	// Codec AUDIO
	string strCAudioEnabled(m_configurator.GetString(CFG_FW_CORE_AUDIO_CODEC_ENABLE));
	if ( strCAudioEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::initI2CPeripherals: Audio not enabled");
	}
	else
	{
		if ( m_i2cBoard.createAndInitCodecAudio() != 0 )
		{
			log(LOG_ERR, "MainExec::initI2CPeripherals: error, unable to initialize Audio");
			return false;
		}
		log(LOG_INFO, "MainExec::initI2CPeripherals: Audio enabled");
	}

	log(LOG_INFO, "MainExec::initI2CPeripherals: done");
	return true;
}

void MainExecutor::stopI2CPeripherals(void)
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::initI2CPeripherals: fw not configured");
		return;
	}

	// Power Manager
	string strPMEnabled(m_configurator.GetString(CFG_FW_CORE_POWER_MAN_ENABLE));
	if ( strPMEnabled.compare("true") != 0 )
	{		
		log(LOG_INFO, "MainExec::stopI2CPeripherals: Power Manager not enabled");
	}
	else
	{
		// Close here PowerManager
		m_i2cBoard.destroyPowerManager();
		log(LOG_INFO, "MainExec::stopI2CPeripherals: Power Manager correctly closed");
	}

	//GPIO Expander
	string strGEEnabled(m_configurator.GetString(CFG_FW_CORE_GPIO_EXP_SEC_ENABLE));
	if (strGEEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::stopI2CPeripherals: GPIO Expander not enabled");
	}
	else
	{
		m_i2cBoard.destroyGpioExpanderA();
		log(LOG_INFO, "MainExec::stopI2CPeripherals: GPIO Expander correctly closed");

	}

	// Led Driver
	string strLedDriverEnabled(m_configurator.GetString(CFG_FW_CORE_LED_DRV_ENABLE));
	if (strLedDriverEnabled.compare("true") != 0)
	{
		log(LOG_INFO,"MainExec::stopI2CPeripherals: LED DRIVER not enabled");
	}
	else
	{
		m_i2cBoard.destroyLedDriver();
		log(LOG_INFO, "MainExec::stopI2CPeripherals: LED Driver correctly closed");
	}

	// Fan Controller
	string strFanControllerEnabled(m_configurator.GetString(CFG_FW_CORE_FAN_CTRL_ENABLE));
	if (strFanControllerEnabled.compare("true") != 0)
	{
		log(LOG_INFO,"MainExec::stopI2CPeripherals: FAN CONTROLLER not enabled");
	}
	else
	{
		m_i2cBoard.destroyFanController();
		log(LOG_INFO, "MainExec::stopI2CPeripherals: FAN CONTROLLER correctly closed");
	}

	// EEprom
	string strEEEnabled(m_configurator.GetString(CFG_FW_CORE_EEPROM_ENABLE));
	if ( strEEEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::stopI2CPeripherals: EEprom not enabled");
	}
	else
	{
		m_i2cBoard.destroyEeprom();
		log(LOG_INFO, "MainExec::stopI2CPeripherals: EEprom correctly closed");
	}

	// CodecAUDIO
	string strCAudioEnabled(m_configurator.GetString(CFG_FW_CORE_AUDIO_CODEC_ENABLE));
	if ( strCAudioEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::stopI2CPeripherals: Audio not enabled");
	}
	else
	{
		m_i2cBoard.destroyCodecAudio();
		log(LOG_INFO, "MainExec::stopI2CPeripherals: Audio correctly closed");
	}

	//GPIO Expander
	string strGELEDEnabled(m_configurator.GetString(CFG_FW_CORE_GPIO_EXP_LED_ENABLE));
	if (strGELEDEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::stopI2CPeripherals: GPIO Expander LED not enabled");
	}
	else
	{
		m_i2cBoard.destroyGpioExpanderLed();
		log(LOG_INFO, "MainExec::stopI2CPeripherals: GPIO Expander LED correctly closed");

	}

	// I2C Interfaces
	 m_i2cBoard.destroyI2CInterfaces();

}

bool MainExecutor::initSPIPeripherals(void)
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::initSPIPeripherals: fw not configured");
		return false;
	}

	string strNSHREnabled(m_configurator.GetString(CFG_FW_CORE_NSH_ENABLE));
	string strNSHMEnabled(m_configurator.GetString(CFG_FW_CORE_STEPPER_MOTOR_ENABLE));
	string strNSHEEnabled(m_configurator.GetString(CFG_FW_CORE_ENCODER_COUNTER_ENABLE));

	int ucDeviceEnabled = 0;

	if ( ! strNSHREnabled.compare("true") )		ucDeviceEnabled |= 1;
	if ( ! strNSHEEnabled.compare("true") )		ucDeviceEnabled |= (1 << 1);
	if ( ! strNSHMEnabled.compare("true") )		ucDeviceEnabled |= (1 << 2);

	if ( ucDeviceEnabled )
	{
		// GPIO interface is needed!
		string strEnabled(m_configurator.GetString(CFG_FW_CORE_GPIO_IF_ENABLE));
		if ( strEnabled.compare("true") != 0 )
		{
			log(LOG_ERR, "MainExec::initSPIPeripherals: gpio interface not initialized");
			return false;
		}

		m_SPIBoard.setLogger(getLogger());
		if ( m_SPIBoard.createAndInitSPIInterfaces() != 0 )
		{
			log(LOG_ERR, "MainExec::initSPIPeripherals: error, unable to initialize SPI channels");
			return false;
		}
	}
	else
	{
		log(LOG_INFO, "MainExec::initSPIPeripherals: no SPI peripheral is enabled");
		return true;
	}

	if ( ucDeviceEnabled & 1 )
	{
		if ( m_SPIBoard.createAndInitNSHReader(&m_gpioInterface) != 0 )
		{
			log(LOG_ERR, "MainExec::initSPIPeripherals: error, unable to initialize NSH reader");
			return false;
		}
		log(LOG_INFO, "MainExec::initSPIPeripherals: NSH Reader enabled");
	}
	else
	{
		log(LOG_INFO, "MainExec::initSPIPeripherals: NSH reader not enabled");
	}

	if ( ucDeviceEnabled & (1 << 1) )
	{
		if ( m_SPIBoard.createAndInitNSHEncoder() != 0 )
		{
			log(LOG_ERR, "MainExec::initSPIPeripherals: error, unable to initialize NSH encoder");
			return false;
		}
		log(LOG_INFO, "MainExec::initSPIPeripherals: NSH encoder enabled");
	}
	else
	{
		log(LOG_INFO, "MainExec::initSPIPeripherals: NSH encoder not enabled");
	}

	if ( ucDeviceEnabled & (1 << 2) )
	{
		if ( m_SPIBoard.createAndInitNSHMotor(&m_gpioInterface) != 0 )
		{
			log(LOG_ERR, "MainExec::initSPIPeripherals: error, unable to initialize NSH motor");
			return false;
		}
		log(LOG_INFO, "MainExec::initSPIPeripherals: NSH motor enabled");
	}
	else
	{
		log(LOG_INFO, "MainExec::initSPIPeripherals: NSH motor not enabled");
	}

	log(LOG_INFO, "MainExec::initSPIPeripherals: done");
	return true;
}

void MainExecutor::stopSPIPeripherals(void)
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::stopSPIPeripherals: fw not configured");
		return;
	}

	string strNSHREnabled(m_configurator.GetString(CFG_FW_CORE_NSH_ENABLE));
	if ( strNSHREnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH reader not enabled");
	}
	else
	{
		m_SPIBoard.destroyNSHReader();
		log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH reader correctly closed");
	}

	string strNSHMEnabled(m_configurator.GetString(CFG_FW_CORE_STEPPER_MOTOR_ENABLE));
	if ( strNSHMEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH motor not enabled");
	}
	else
	{
		m_SPIBoard.destroyNSHMotor();
		log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH motor correctly closed");
	}

	string strNSHEEnabled(m_configurator.GetString(CFG_FW_CORE_ENCODER_COUNTER_ENABLE));
	if ( strNSHEEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH encoder not enabled");
	}
	else
	{
		m_SPIBoard.destroyNSHEncoder();
		log(LOG_INFO, "MainExec::stopSPIPeripherals: NSH encoder correctly closed");
	}

	// SPI Interfaces
	 m_SPIBoard.destroySPIInterfaces();
	 log(LOG_INFO, "MainExec::stopSPIPeripherals: done.");
}

bool MainExecutor::initOpManagerSrvCollector(void)
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExec::initOperationManager: fw not configured");
		return false;
	}

	string strEnabled(m_configurator.GetString(CFG_FW_CORE_OPER_MAN_ENABLE));
	if ( strEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::initOperationManager: operation manager not enabled");
		return true;
	}

	m_operationManager.setLogger(getLogger());
	if ( !m_operationManager.init() )
	{
		log(LOG_ERR, "MainExec::initOperationManager: error, unable to initialize OperationManager");
		return false;
	}

	m_serviceCollector.setLogger(getLogger());
	if ( ! m_serviceCollector.startAll() )
	{
		log(LOG_ERR, "MainExec::initOperationManager: error, unable to spawn some thread in the ServiceCollector");
		return false;
	}

	log(LOG_INFO, "MainExec::initOperationManager: done");
	
	return true;
}

void MainExecutor::stopOpManagerSrvCollector(void)
{
	m_serviceCollector.stopAll();
	m_operationManager.stop();
	log(LOG_INFO, "MainExec::stopOperationManager: done");
}

bool MainExecutor::initTestManager(void)
{
	string strTestManagerEnabled(m_configurator.GetString(CFG_FW_CORE_TESTMANAGER_ENABLE));
	if ( strTestManagerEnabled.compare("true") != 0 )
	{
		log(LOG_INFO, "MainExec::initTestManager: test Manager not enabled");
	}
	else
	{
		SAFE_DELETE(m_pTestApp);
		m_pTestApp = new TestManager();
		if (m_pTestApp == 0)
		{
            log(LOG_ERR, "MainExecutor::initTestManager: unable to create TestManager, memory error");
			return false;
		}

		m_pTestApp->initConfig(&m_configurator);
		m_pTestApp->setLogger(m_pLogger);
		if (!m_pTestApp->startThread())
		{
			log(LOG_ERR, "MainExecutor::initTestManager: error, unable to start TestManager");
			return false;
		}
        m_bTestManagerActive = true;
		log(LOG_INFO, "MainExec::initTestManager: done");
	}

	return true;
}

bool MainExecutor::checkTestManager(void)
{
    if ( m_bTestManagerActive == true )
	{
        if ( (m_pTestApp == 0) || (!m_pTestApp->isRunning()) )
		{
			log(LOG_INFO, "Main: TestApp is not more running...");
			return false;
		}
	}
	return true;
}

void MainExecutor::stopTestManager(void)
{
    if (m_bTestManagerActive == true)
	{
        if (m_pTestApp)
        {
            m_pTestApp->stopThread();
            SAFE_DELETE(m_pTestApp);
        }
	}
}

bool MainExecutor::initProtocolThread()
{
	bool bRetValue = true;

	m_THRunProtocol.setLogger(getLogger());
	if ( ! m_THRunProtocol.isRunning() )
	{
		bRetValue = m_THRunProtocol.startThread();
	}

    if(bRetValue == true)
    {
        m_THReplyProtocol.setLogger(getLogger());
        if ( ! m_THReplyProtocol.isRunning() )
        {
            bRetValue = m_THReplyProtocol.startThread();
        }
    }

    return bRetValue;
}

void MainExecutor::stopProtocolThread()
{
	if ( m_THRunProtocol.isRunning() )
	{
		m_THRunProtocol.stopThread();
	}
    if ( m_THReplyProtocol.isRunning() )
    {
        m_THReplyProtocol.stopThread();
    }
    msleep(500);
	if ( m_THRunProtocol.isStopped() )
	{
		log(LOG_INFO, "THRunProtocol thread closed");
	}
    if ( m_THReplyProtocol.isStopped() )
    {
        log(LOG_INFO, "THReplyProtocol thread closed");
    }
}

bool MainExecutor::firstStartupInstrument()
{
	log(LOG_INFO, "MainExecutor:instrument startup");

	// start initialization threads for Sections A and B
	for ( int i = 0; i < SCT_NUM_TOT_SECTIONS; i++ )
	{
		if(isSectionEnabled(i))
		{
			m_sectionBoardManager[i].setSectionAction(SECTION_ACTION_INIT);
		}
	}

	if(isNSHEnabled() == true)
	{
		m_NSHBoardManager.setNSHAction(eNSHActionInit, 0);
	}

	if(isCameraEnabled() == true)
	{
		m_CameraBoardManager.setCameraAction(eCameraActionInit, 0);
	}

	m_MasterBoardManager.setMasterAction(eMasterActionInit);

	return true;
}

bool MainExecutor::reinitInstrument()
{
	// start initialization threads for Sections A and B
	log(LOG_INFO, "MainExecutor:instrument reinit");

	if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) != eStatusDisable)
	{
		m_sectionBoardManager[SCT_A_ID].setSectionAction(SECTION_ACTION_INIT);
	}
	if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) != eStatusDisable)
	{
		m_sectionBoardManager[SCT_B_ID].setSectionAction(SECTION_ACTION_INIT);
	}
	if(stateMachineSingleton()->getDeviceStatus(eIdNsh) != eStatusDisable)
	{
		m_NSHBoardManager.setNSHAction(eNSHActionInit, 0);
	}
	if(stateMachineSingleton()->getDeviceStatus(eIdCamera) != eStatusDisable)
	{
		m_CameraBoardManager.setCameraAction(eCameraActionInit, 0);
	}

	return true;
}

bool MainExecutor::areSectionsEnabled()
{
	if ( ! m_bConfigIsOk )
	{
		log(LOG_ERR, "MainExecutor: fw not configured");
		return false;
	}

	if(isSectionEnabled(SCT_A_ID) == false && isSectionEnabled(SCT_B_ID) == false)
	{
		log(LOG_INFO, "MainExecutor: sections not enabled");
		return false;
	}

	return true;
}

bool MainExecutor::isSectionEnabled(int section)
{
	string strSectEnabled;

	if(section == SCT_A_ID)
		strSectEnabled.assign(m_configurator.GetString(CFG_FW_CORE_SCT_A_ENABLE));
	else
		strSectEnabled.assign(m_configurator.GetString(CFG_FW_CORE_SCT_B_ENABLE));

	if ( strSectEnabled.compare("true") != 0 )
	{
		if(section == SCT_A_ID)
			stateMachineSingleton()->setDeviceStatus(eIdSectionA, eStatusDisable);
		else
			stateMachineSingleton()->setDeviceStatus(eIdSectionB, eStatusDisable);
	}

	// the status can be disabled due to SW setup or due to instrument settings

	if(section == SCT_A_ID &&
		stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusDisable)
	{
		return false;
	}
	else if(section == SCT_B_ID &&
		stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusDisable)
	{
		return false;
	}

	return true;
}

int MainExecutor::setRootPolicy()
{
	string strFunc("chmod -R 755 /home/");
	return system(strFunc.c_str());
}

bool MainExecutor::initPowerOffManager( void )
{
	// Power Manager
	string strPMEnabled(m_configurator.GetString(CFG_FW_CORE_POWER_MAN_ENABLE));
	if ( strPMEnabled.compare("true") != 0 )
	{
        log(LOG_INFO, "MainExec::initPowerOffManager--> Power Manager not enabled");
        return false;
	}

    int res = -1;
	m_powerOffManager.setLogger(getLogger());
    res = m_powerOffManager.initPMManager(CONF_FILE, &m_gpioInterface);
    if ( res != 0)
    {
        log(LOG_ERR, "MainExecutor::initPowerOffManager--> error init");
        return false;
    }

    if ( !m_powerOffManager.startThread() )
	{
		log(LOG_ERR, "MainExecutor::initPowerOffManager--> error start thread");
		return false;
	}

	log(LOG_INFO, "MainExecutor::initPowerOffManager--> PowerOffManager correctly initialized ");
	return true;
}

bool MainExecutor::isNSHEnabled(void)
{
	string strNSHREnabled(m_configurator.GetString(CFG_FW_CORE_NSH_ENABLE));

	if ( strNSHREnabled.compare("true") != 0)
	{
		stateMachineSingleton()->setDeviceStatus(eIdNsh, eStatusDisable);
	}

	// the status can be disabled due to SW setup or due to instrument settings
	if ( stateMachineSingleton()->getDeviceStatus(eIdNsh) == eStatusDisable )
	{
		return false;
	}

	return true;
}

bool MainExecutor::isCameraEnabled(void)
{
	string strCameraEnabled(m_configurator.GetString(CFG_FW_CORE_CAM_ENABLE));

	if ( strCameraEnabled.compare("true") != 0)
	{
		stateMachineSingleton()->setDeviceStatus(eIdCamera, eStatusDisable);
	}

	// the status can be disabled due to SW setup or due to instrument settings
	if ( stateMachineSingleton()->getDeviceStatus(eIdCamera) == eStatusDisable )
	{
		return false;
	}

	return true;
}

bool MainExecutor::stopPowerOffManager( void )
{
	m_powerOffManager.stopThread();

	log(LOG_INFO, "MainExecutor::stopPowerOffManager--> stop thread");

	return true;
}

bool MainExecutor::saveDisableState()
{
	std::string strWrite;
	std::ofstream outFile;

	outFile.open(DISABLED_SETUP_FILE, ios_base::out);

	if(stateMachineSingleton()->getDeviceStatus(eIdNsh) == eStatusDisable)
	{
		strWrite.assign(XML_TAG_NSH_NAME);
		strWrite.append("=");
		strWrite.append(XML_ATTR_STATUS_DISABLED);
		strWrite.append("\r\n");
		outFile.write(strWrite.c_str(), sizeof(strWrite));
	}
	if(stateMachineSingleton()->getDeviceStatus(eIdCamera) == eStatusDisable)
	{
		strWrite.assign(XML_TAG_CAMERA_NAME);
		strWrite.append("=");
		strWrite.append(XML_ATTR_STATUS_DISABLED);
		outFile.write(strWrite.c_str(), sizeof(strWrite));
	}
	if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusDisable)
	{
		strWrite.assign(XML_TAG_SECTIONA_NAME);
		strWrite.append("=");
		strWrite.append(XML_ATTR_STATUS_DISABLED);
		outFile.write(strWrite.c_str(), sizeof(strWrite));
	}
	if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusDisable)
	{
		strWrite.assign(XML_TAG_SECTIONB_NAME);
		strWrite.append("=");
		strWrite.append(XML_ATTR_STATUS_DISABLED);
		outFile.write(strWrite.c_str(), sizeof(strWrite));
	}
	outFile.close();

	return true;
}

bool MainExecutor::restoreDisableState()
{
	std::string strRead, strDevice, strStatus;
	std::ifstream inFile;

	inFile.open(DISABLED_SETUP_FILE);
	if(inFile.fail())
	{
		//File does not exist -> no settings
		return true;
	}

	while(!inFile.eof())
	{
		getline(inFile, strRead);
		if(!strRead.empty())
		{
			// lines beginning with # are comments
			if(strRead.front() != '#')
			{
				size_t posStr = 0;
				if((posStr = strRead.find("=")) != std::string::npos)
				{
					// extract the value for the login / password
					posStr = strRead.find("=");
					strDevice = strRead.substr(0, posStr);
					strStatus = strRead.substr(posStr + 1);

					// assign disabled status
					if(strStatus.compare(XML_ATTR_STATUS_DISABLED) == 0)
					{
						if(strDevice.compare(XML_TAG_NSH_NAME) == 0)
						{
							stateMachineSingleton()->setDeviceStatus(eIdNsh, eStatusDisable);
						}
						else if(strDevice.compare(XML_TAG_CAMERA_NAME) == 0)
						{
							stateMachineSingleton()->setDeviceStatus(eIdCamera, eStatusDisable);
						}
						else if(strDevice.compare(XML_TAG_SECTIONA_NAME) == 0)
						{
							stateMachineSingleton()->setDeviceStatus(eIdSectionA, eStatusDisable);
						}
						else if(strDevice.compare(XML_TAG_SECTIONB_NAME) == 0)
						{
							stateMachineSingleton()->setDeviceStatus(eIdSectionB, eStatusDisable);
						}
					}
				}
			}
		}
	}
	inFile.close();

	return true;
}

// --------- wrapper functions --------------------------------------- //
StateMachine * stateMachineSingleton()
{
	return &MainExecutor::getInstance()->m_stateMachine;
}

bool changeDeviceStatus(eDeviceId device, eDeviceStatus status)
{
	if(stateMachineSingleton()->setDeviceStatus(device, status))
	{
		requestVidasEp();
		return true;
	}
	return false;
}

bool protocolSerialize(OutgoingCommand* pCmd, std::string& strOutString)
{
	return MainExecutor::getInstance()->m_protocolValidator.serialize(pCmd, strOutString);
}

bool protocolDataSerialize(uint8_t section, uint8_t strip, std::string strDataType, std::string& strOutString)
{
	return MainExecutor::getInstance()->m_protocolValidator.serializeDataToXML(section, strip, strDataType, strOutString);
}

OperationManager * operationSingleton()
{
	return &MainExecutor::getInstance()->m_operationManager;
}

InstrumentInfo * infoSingleton()
{
	return &MainExecutor::getInstance()->m_instrumentInfo;
}

SectionBoardManager * sectionBoardManagerSingleton(int section)
{
	return &MainExecutor::getInstance()->m_sectionBoardManager[section];
}

SectionBoardGeneral * sectionBoardGeneralSingleton(int section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return MainExecutor::getInstance()->m_sectionBoardManager[section].m_pSectionLink->m_pGeneral;
}

SectionBoardLink * sectionBoardLinkSingleton(int section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return &MainExecutor::getInstance()->m_sectionBoardLink[section];
}

NSHBoardManager * nshBoardManagerSingleton()
{
	return &MainExecutor::getInstance()->m_NSHBoardManager;
}

NSHReaderGeneral * nshReaderGeneralSingleton()
{
	return &MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH;
}

MasterBoardManager * masterBoardManagerSingleton()
{
	return &MainExecutor::getInstance()->m_MasterBoardManager;
}

CameraBoardManager * cameraBoardManagerSingleton()
{
	return &MainExecutor::getInstance()->m_CameraBoardManager;
}

SPILinkBoard* spiBoardLinkSingleton()
{
	return &MainExecutor::getInstance()->m_SPIBoard;
}

USBLinkBoard* usbBoardLinkSingleton()
{
	return &MainExecutor::getInstance()->m_USBBoard;
}

CR8062Camera* usbCameraSingleton()
{
	return &MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera;
}

CR8062Reader* usbReaderSingleton()
{
	return &MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Reader;
}

ProtocolInfo * protocolInfoPointer(int index)
{
	if(index >= SCT_NUM_TOT_SECTIONS) return 0;

	return &MainExecutor::getInstance()->m_protocolInfo[index];
}

RunwlInfo * runwlInfoPointer(int index)
{
	if(index >= SCT_NUM_TOT_SECTIONS) return 0;

	return &MainExecutor::getInstance()->m_runwlInfo[index];
}

ReadSecInfo* readSecInfoPointer(int index)
{
	if ( index >= SCT_NUM_TOT_SECTIONS ) return 0;

	return &MainExecutor::getInstance()->m_readSecInfo[index];
}

PressureSettings * pressureSettingsPointer(uint8_t section, uint8_t strip)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;
	if(strip >= SCT_NUM_TOT_SLOTS) return 0;

	return &MainExecutor::getInstance()->m_pressureSettings[section * SCT_NUM_TOT_SECTIONS + strip];
}

SchedulerThread * schedulerThreadPointer()
{
	return &MainExecutor::getInstance()->m_SchedulerThread;
}

THRunProtocol * protocolThreadPointer()
{
	return &MainExecutor::getInstance()->m_THRunProtocol;
}

THReplyProtocol * protocolReplyThreadPointer()
{
    return &MainExecutor::getInstance()->m_THReplyProtocol;
}

std::string getMasterErrorInfoFromCode(uint16_t code)
{
	std::string strError;

    // explicitly set the Component
    strError.assign(XML_TAG_INSTRUMENT_NAME);
    strError.append(HASHTAG_SEPARATOR);
    strError.append(MainExecutor::getInstance()->m_MasterBoardManager.setErrorFromCode(code));
	return strError;
}

bool requestModuleLed()
{
	return masterBoardManagerSingleton()->setMasterAction(eMasterActionLed);
}

int requestVidasEp()
{
	// everytime a VidasEp is requested record the state machine condition
	registerStateMachine();

	return THVidasEp::getInstance()->sendHttpData();
}

int requestStartButton(uint8_t section)
{
    // the StartButton has been pressed:
    // check the section status: if door open OR in PreProcessing or Processing the operation is not notified
    if(section >= SCT_NUM_TOT_SECTIONS) return -1;

    eDeviceId deviceId;
    if(section == SCT_A_ID)
    {
        deviceId = eIdSectionA;
    }
    else
    {
        deviceId  = eIdSectionB;
    }

    if(stateMachineSingleton()->getDeviceStatus(deviceId) == eStatusIdle  &&
       sectionBoardGeneralSingleton(section)->isDoorClosed() == true)
    {
        /* !!! TODO : send notify to SW */
        return 0;
    }

    return -1;
}

int checkErrorsOnSection(uint8_t section)
{
    return sectionBoardManagerSingleton(section)->requestCheckErrorsStatus();
}

bool registerErrorEvent(uint8_t section, const string& strDevice,
					   const string& strCode, int liLevel,
					   const string& strDescription, bool enabled, bool active)
{
	std::vector<std::string> vectorMsg;

	switch(section)
	{
		case SCT_A_ID:
			vectorMsg.push_back(XML_TAG_SECTIONA_NAME);
		break;
		case SCT_B_ID:
			vectorMsg.push_back(XML_TAG_SECTIONB_NAME);
		break;
		case NSH_ID:
			vectorMsg.push_back(XML_TAG_NSH_NAME);
		break;
		case CAMERA_ID:
			vectorMsg.push_back(XML_TAG_CAMERA_NAME);
		break;
		case INSTRUMENT_ID:
			vectorMsg.push_back(XML_TAG_MASTERBOARD_NAME);
		break;
		default:
			vectorMsg.push_back("");
		break;
	}

	vectorMsg.push_back(strDevice);
	vectorMsg.push_back(strCode);
	vectorMsg.push_back(to_string(liLevel));
	vectorMsg.push_back(strDescription);
	if(enabled)
	{
		vectorMsg.push_back("enabled");
	}
	else
	{
		vectorMsg.push_back("disabled");
	}
	if(active)
	{
		vectorMsg.push_back("active");
	}
	else
	{
		vectorMsg.push_back("not active");
	}

	return MainExecutor::getInstance()->m_eventRecorder.writeEvent(RECORDER_ERRORS_INDEX, &vectorMsg);
}

bool registerStateMachine()
{
	std::string strTmp;
	std::vector<std::string> vectorMsg;

	strTmp.assign(XML_TAG_SECTIONA_NAME);
	strTmp.append("=");
	strTmp.append(stateMachineSingleton()->getDeviceStatusAsString(eIdSectionA));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONB_NAME);
	strTmp.append("=");
	strTmp.append(stateMachineSingleton()->getDeviceStatusAsString(eIdSectionB));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_NSH_NAME);
	strTmp.append("=");
	strTmp.append(stateMachineSingleton()->getDeviceStatusAsString(eIdNsh));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_CAMERA_NAME);
	strTmp.append("=");
	strTmp.append(stateMachineSingleton()->getDeviceStatusAsString(eIdCamera));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_MASTERBOARD_NAME);
	strTmp.append("=");
	strTmp.append(stateMachineSingleton()->getDeviceStatusAsString(eIdModule));
	vectorMsg.push_back(strTmp);

	return MainExecutor::getInstance()->m_eventRecorder.writeEvent(RECORDER_STATE_INDEX, &vectorMsg);
}

bool registerCommandEvent(const char * strEvent)
{
	std::vector<std::string> vectorMsg;

	vectorMsg.push_back(strEvent);

	return MainExecutor::getInstance()->m_eventRecorder.writeEvent(RECORDER_COMMANDS_INDEX, &vectorMsg);
}

bool registerGenericEvent(string strEvent)
{
	std::vector<std::string> vectorMsg;

	vectorMsg.push_back(strEvent);

	return MainExecutor::getInstance()->m_eventRecorder.writeEvent(RECORDER_EVENTS_INDEX, &vectorMsg);
}

bool registerTemperatures()
{
	std::string strTmp;
	std::vector<std::string> vectorMsg;

	strTmp.assign(XML_TAG_SECTIONA_NAME);
	strTmp.append(" ");
	strTmp.append(XML_TAG_SPR_NAME);
	strTmp.append("=");
	strTmp.append(to_string(sectionBoardGeneralSingleton(SCT_A_ID)->getIntTemperatureSPR()));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONA_NAME);
	strTmp.append(" ");
	strTmp.append(XML_TAG_TRAY_NAME);
	strTmp.append("=");
	strTmp.append(to_string(sectionBoardGeneralSingleton(SCT_A_ID)->getIntTemperatureTray()));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONB_NAME);
	strTmp.append(" ");
	strTmp.append(XML_TAG_SPR_NAME);
	strTmp.append("=");
	strTmp.append(to_string(sectionBoardGeneralSingleton(SCT_B_ID)->getIntTemperatureSPR()));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONB_NAME);
	strTmp.append(" ");
	strTmp.append(XML_TAG_TRAY_NAME);
	strTmp.append("=");
	strTmp.append(to_string(sectionBoardGeneralSingleton(SCT_B_ID)->getIntTemperatureTray()));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_MASTERBOARD_NAME);
	strTmp.append(" ");
        strTmp.append(XML_TAG_INSTRUMENT_NAME);
	strTmp.append("=");
        strTmp.append(to_string(masterBoardManagerSingleton()->getInternalTemperature()));
	vectorMsg.push_back(strTmp);
/*
	strTmp.assign(XML_TAG_MASTERBOARD_NAME);
	strTmp.append(" ");
	strTmp.append(XML_TAG_SECTIONB_NAME);
	strTmp.append("=");
	strTmp.append(to_string(masterBoardManagerSingleton()->getNtcValue(eNtcSectionB)));
	vectorMsg.push_back(strTmp);
*/
	strTmp.assign(XML_TAG_MASTERBOARD_NAME);
	strTmp.append(" ");
	strTmp.append("Supply");
	strTmp.append("=");
	strTmp.append(to_string(masterBoardManagerSingleton()->getNtcValue(eNtcSupply)));
	vectorMsg.push_back(strTmp);

	return MainExecutor::getInstance()->m_eventRecorder.writeEvent(RECORDER_TEMPERATURE_INDEX, &vectorMsg);
}


bool registerVoltages()
{
	std::string strTmp;
	std::vector<std::string> vectorMsg;

	strTmp.assign(XML_TAG_SECTIONA_NAME);
	strTmp.append(" ");
	strTmp.append(SENSOR_ID_NAME_24V);
	strTmp.append("=");
	strTmp.append(to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_A_ID)->getVoltage24V() * 1000. / 100.)));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONA_NAME);
	strTmp.append(" ");
	strTmp.append(SENSOR_ID_NAME_5V);
	strTmp.append("=");
	strTmp.append(to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_A_ID)->getVoltage5V() * 1000. / 100.)));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONA_NAME);
	strTmp.append(" ");
	strTmp.append(SENSOR_ID_NAME_3V3);
	strTmp.append("=");
	strTmp.append(to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_A_ID)->getVoltage3V3() * 1000. / 100.)));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONA_NAME);
	strTmp.append(" ");
	strTmp.append(SENSOR_ID_NAME_1V2);
	strTmp.append("=");
	strTmp.append(to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_A_ID)->getVoltage1V2() * 1000. / 100.)));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONB_NAME);
	strTmp.append(" ");
	strTmp.append(SENSOR_ID_NAME_24V);
	strTmp.append("=");
	strTmp.append(to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_B_ID)->getVoltage24V() * 1000. / 100.)));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONB_NAME);
	strTmp.append(" ");
	strTmp.append(SENSOR_ID_NAME_5V);
	strTmp.append("=");
	strTmp.append(to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_B_ID)->getVoltage5V() * 1000. / 100.)));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONB_NAME);
	strTmp.append(" ");
	strTmp.append(SENSOR_ID_NAME_3V3);
	strTmp.append("=");
	strTmp.append(to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_B_ID)->getVoltage3V3() * 1000. / 100.)));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_SECTIONA_NAME);
	strTmp.append(" ");
	strTmp.append(SENSOR_ID_NAME_1V2);
	strTmp.append("=");
	strTmp.append(to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_A_ID)->getVoltage1V2() * 1000. / 100.)));
	vectorMsg.push_back(strTmp);

	// !!!! TODO add masterboard voltages

	return MainExecutor::getInstance()->m_eventRecorder.writeEvent(RECORDER_VOLTAGES_INDEX, &vectorMsg);
}

bool registerFans()
{
	std::string strTmp;
	std::vector<std::string> vectorMsg;

	strTmp.assign(XML_TAG_MASTERBOARD_NAME);
	strTmp.append(" ");
	strTmp.append(XML_TAG_SECTIONA_NAME);
	strTmp.append(" ");
	strTmp.append("PWM");
	strTmp.append("=");
	strTmp.append(to_string(masterBoardManagerSingleton()->getFanPower(eFanSectionA)));
	strTmp.append(" ");
	strTmp.append(SENSOR_UNIT_RPM);
	strTmp.append("=");
	strTmp.append(to_string(masterBoardManagerSingleton()->getFanRpm(eFanSectionA)));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_MASTERBOARD_NAME);
	strTmp.append(" ");
	strTmp.append(XML_TAG_SECTIONB_NAME);
	strTmp.append(" ");
	strTmp.append("PWM");
	strTmp.append("=");
	strTmp.append(to_string(masterBoardManagerSingleton()->getFanPower(eFanSectionB)));
	strTmp.append(" ");
	strTmp.append(SENSOR_UNIT_RPM);
	strTmp.append("=");
	strTmp.append(to_string(masterBoardManagerSingleton()->getFanRpm(eFanSectionB)));
	vectorMsg.push_back(strTmp);

	strTmp.assign(XML_TAG_MASTERBOARD_NAME);
	strTmp.append(" ");
	strTmp.append("Supply");
	strTmp.append(" ");
	strTmp.append("PWM");
	strTmp.append("=");
	strTmp.append(to_string(masterBoardManagerSingleton()->getFanPower(eFanSupply)));
	strTmp.append(" ");
	strTmp.append(SENSOR_UNIT_RPM);
	strTmp.append("=");
	strTmp.append(to_string(masterBoardManagerSingleton()->getFanRpm(eFanSupply)));
	vectorMsg.push_back(strTmp);

	return MainExecutor::getInstance()->m_eventRecorder.writeEvent(RECORDER_FAN_INDEX, &vectorMsg);
}

bool registerSolidStandardReadings(std::string strStatus, int liFluoVal, int liGoldenSS)
{
    std::string strTmp;
    std::vector<std::string> vectorMsg;

    strTmp.assign("Result=");
    strTmp.append(strStatus);
    vectorMsg.push_back(strTmp);

    strTmp.assign("Measured=");
    strTmp.append(to_string(liFluoVal));
    vectorMsg.push_back(strTmp);

    strTmp.assign("Reference=");
    strTmp.append(to_string(liGoldenSS));
    vectorMsg.push_back(strTmp);

    return MainExecutor::getInstance()->m_eventRecorder.writeEvent(RECORDER_READINGS_INDEX, &vectorMsg);
}

bool registerOPTCalibration(std::string strSection, std::string strSlot, std::string strTarget,
                            std::string strGain,  std::string strValue, std::string strID)
{
    std::string strTmp;
    std::vector<std::string> vectorMsg;

    strTmp.assign("Slot=");
    strTmp.append(strSection);
    strTmp.append(strSlot);
    vectorMsg.push_back(strTmp);

    strTmp.assign("Reference=");
    strTmp.append(strTarget);
    vectorMsg.push_back(strTmp);

    strTmp.assign("Measured=");
    strTmp.append(strValue);
    vectorMsg.push_back(strTmp);

    strTmp.assign("Gain=");
    strTmp.append(strGain);
    vectorMsg.push_back(strTmp);

    strTmp.assign("ID=");
    strTmp.append(strID);
    vectorMsg.push_back(strTmp);


    return MainExecutor::getInstance()->m_eventRecorder.writeEvent(RECORDER_READINGS_INDEX, &vectorMsg);
}

int resetSectionBoard(uint8_t section)
{
    return MainExecutor::getInstance()->m_i2cBoard.resetSectionBoard(section);
}


// ------------------ utilities functions ----------------------------

std::string getFormattedTimeFromSeconds(uint64_t seconds, bool bSeparator)
{
	time_t t = (time_t) seconds;

	struct tm timeSec = *localtime(&t);

	return fromTimeStructToTimeString(&timeSec, bSeparator);
}


time_t getSecondsTime()
{
	time_t t = time(NULL);
	struct tm timeNow = *localtime(&t);

	time_t seconds = mktime(&timeNow);
	return seconds;
}

uint64_t getClockFromStart()
{
	clock_t timeClock = clock();
	return (uint64_t)(timeClock);
}

std::string getFormattedTime(bool bSeparator)
{
	time_t t = time(NULL);
	struct tm timeNow = *localtime(&t);

	return fromTimeStructToTimeString(&timeNow, bSeparator);
}

std::string fromTimeStructToTimeString(struct tm * timeNow, bool bSeparator)
{
	char bufferTmp[64];
	std::string strTime;

	strTime.clear();

	if(bSeparator == false)
	{
		sprintf(bufferTmp, "%04d%02d%02d%02d%02d%02d",
			timeNow->tm_year + 1900, timeNow->tm_mon + 1, timeNow->tm_mday,
			timeNow->tm_hour, timeNow->tm_min, timeNow->tm_sec);
	}
	else
	{
		sprintf(bufferTmp, "%04d-%02d-%02dT%02d:%02d:%02d",
			timeNow->tm_year + 1900, timeNow->tm_mon + 1, timeNow->tm_mday,
            timeNow->tm_hour, timeNow->tm_min, timeNow->tm_sec);
	}
	strTime.assign(bufferTmp);
	return strTime;
}

std::string getVidasepTimestamp()
{
    struct timeval curr_time;
    int curr_msec;
    char bufferTmp[64];
    std::string strTime;

    strTime.clear();

    gettimeofday(&curr_time, NULL);
    curr_msec = curr_time.tv_usec / 1000;

    time_t t = time(NULL);
    struct tm timeNow = *localtime(&t);

    sprintf(bufferTmp, "%04d-%02d-%02dT%02d:%02d:%02d.%03d",
        timeNow.tm_year + 1900, timeNow.tm_mon + 1, timeNow.tm_mday,
        timeNow.tm_hour, timeNow.tm_min, timeNow.tm_sec, curr_msec);

    strTime.assign(bufferTmp);

    return strTime;
}

void showTime(void)
{
	time_t mytime;

	struct tm * ptm;

	time( &mytime ); // Get local time in time_t
	ptm = gmtime ( &mytime ); // Find out UTC time
	time_t utctime = mktime(ptm); // Get UTC time as time_t

//	struct timeval timeStamp;
//	gettimeofday(&timeStamp, NULL);
//	time_t curr_sec = timeStamp.tv_sec;

	printf("\nLocal time %X (%s) and UTC Time %X (%s)", (uint32_t)mytime,  ctime(&mytime),
														(uint32_t)utctime, ctime(&utctime));

//	uint32_t timeval_s = curr_sec;
//	printf("\n timeval %X", timeval_s);
}


static bool writeShutdownFile(bool status)
{
	FILE* fp;

	fp = fopen(SHUTDOWN_FILE, "w");
	if ( fp == NULL )
	{
		return false;
	}

	if(status)
	{
		fprintf(fp, "Shutdown = OK\n");
	}
	else
	{
		fprintf(fp, "Shutdown = KO\n");
	}

	fflush(fp);
	fclose(fp);
	return true;
}

static bool readShutdownFile()
{
	FILE* fp;
	char buffer[128];
	char * pch = 0;

	buffer[0] = 0;

	fp = fopen(SHUTDOWN_FILE, "r");
	if ( fp != NULL )
	{
		fread(buffer, sizeof(char), sizeof(buffer) - 1, fp);
		fclose(fp);
	}

	// if last shutdown not correct ...
	if(buffer[0])
	{
		pch = strstr(buffer, "KO");
	}

	if(buffer[0] == 0 || pch != 0)
		return false;

	return true;
}
