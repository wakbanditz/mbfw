/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    StateMachine.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the StateMachine class.
 @details

 ****************************************************************************
*/

#include <vector>
#include <string>

#include "StateMachine.h"
#include "CommonInclude.h"

/*! ***********************************************************************************************************
 * @brief the StateMachine admits the following transitions for the device:
 * INIT -> IDLE (at startup end)
 * IDLE -> ERROR (message asynchronous received from the board)
 * IDLE -> ERROR (command completed with error)
 * IDLE -> PROTOCOL (protocol command received)
 * PROTOCOL -> IDLE (protocol execution completed)
 * PROTOCOL -> PROTOCOL (command executed while in protocol)
 * PROTOCOL -> ERROR (protocol execution completed with error)
 * ERROR -> DISABLE (command Disable->Disable received)
 * IDLE -> DISABLE (command Disable->Disable received)
 * DISABLE -> IDLE (command Disable->Enable received)
 * DISABLE -> INIT (command Disable->Init received)
 * IDLE -> SLEEP (command Sleep->On received)
 * SLEEP -> INIT (command Sleep->Off received)
 * IDLE -> PREPROCESSING (command ReadSec received)
 * PREPROCESSING -> IDLE (command ReadSec completed)
 * PREPROCESSING -> ERROR (command ReadSec completed with error)
 *
 * when a command is in execution the state is IDLE but the device is locked
 * ************************************************************************************************************
 */


StateMachine::StateMachine()
{
	m_statusModule = eStatusUnknown;
	m_statusSectionA = eStatusUnknown;
	m_statusSectionB = eStatusUnknown;
	m_statusNsh = eStatusUnknown;
	m_statusCamera = eStatusUnknown;

	m_statusTargetModule = eStatusUnknown;
	m_statusTargetSectionA = eStatusUnknown;
	m_statusTargetSectionB = eStatusUnknown;
	m_statusTargetNsh = eStatusUnknown;
	m_statusTargetCamera = eStatusUnknown;

	// all device set to UNLOCK by default
	setDeviceLock(eIdSectionA, eLockOff);
	setDeviceLock(eIdSectionB, eLockOff);
	setDeviceLock(eIdNsh, eLockOff);
	setDeviceLock(eIdCamera, eLockOff);
	setDeviceLock(eIdModule, eLockOff);
}

bool StateMachine::init()
{
	// all device set to INIT by default
	setDeviceStatus(eIdSectionA, eStatusInit);
	setDeviceStatus(eIdSectionB, eStatusInit);
	setDeviceStatus(eIdNsh, eStatusInit);
	setDeviceStatus(eIdCamera, eStatusInit);

	return (getDeviceStatus(eIdModule) == eStatusInit);
}

bool StateMachine::idle()
{
	setDeviceStatus(eIdSectionA, eStatusIdle);
	setDeviceStatus(eIdSectionB, eStatusIdle);
	setDeviceStatus(eIdNsh, eStatusIdle);
	setDeviceStatus(eIdCamera, eStatusIdle);

	return (getDeviceStatus(eIdModule) == eStatusIdle);
}

bool StateMachine::setDeviceStatus(eDeviceId device, eDeviceStatus status)
{
	switch(device)
	{
		case eIdModule :
			m_statusModule = status;
		break;
		case eIdSectionA :
			m_statusSectionA = status;
		break;
		case eIdSectionB :
			m_statusSectionB = status;
		break;
		case eIdNsh :
			m_statusNsh = status;
		break;
		case eIdCamera :
			m_statusCamera = status;
		break;
		default:
			return false;
		break;
	}

	// if the status of a subdevice has been set -> update the module state
	updateModuleStatus();

	return true;
}

bool StateMachine::setDeviceTargetStatus(eDeviceId device, eDeviceStatus status)
{
	switch(device)
	{
		case eIdModule :
			m_statusTargetModule = status;
		break;
		case eIdSectionA :
			m_statusTargetSectionA = status;
		break;
		case eIdSectionB :
			m_statusTargetSectionB = status;
		break;
		case eIdNsh :
			m_statusTargetNsh = status;
		break;
		case eIdCamera :
			m_statusTargetCamera = status;
		break;
		default:
			return false;
		break;
	}
	return true;
}

bool StateMachine::setDeviceStatusToTarget(eDeviceId device)
{
	eDeviceStatus status;

	switch(device)
	{
		case eIdModule :
			status = m_statusTargetModule;
		break;
		case eIdSectionA :
			status = m_statusTargetSectionA;
		break;
		case eIdSectionB :
			status = m_statusTargetSectionB;
		break;
		case eIdNsh :
			status = m_statusTargetNsh;
		break;
		case eIdCamera :
			status = m_statusTargetCamera;
		break;
		default:
			return false;
		break;
	}

	// clear the target (no more necessary)
	setDeviceTargetStatus(device, eStatusUnknown);

	if(status == eStatusNull || status == eStatusUnknown)
		return false;

	// if current status is Error -> no overwrite
	if(getDeviceStatus(device) == eStatusError)
		return false;

	// if the actual status is the same of the target -> no need to update
	if(getDeviceStatus(device) == status)
		return false;

	// and copy it to the current state
	return setDeviceStatus(device, status);
}

eDeviceStatus StateMachine::getDeviceStatus(eDeviceId device)
{
	eDeviceStatus status = eStatusUnknown;

	switch(device)
	{
		case eIdModule :
			status = m_statusModule;
		break;
		case eIdSectionA :
			status = m_statusSectionA;
		break;
		case eIdSectionB :
			status = m_statusSectionB;
		break;
		case eIdNsh :
			status = m_statusNsh;
		break;
		case eIdCamera :
			status = m_statusCamera;
		break;
		default:
		break;
	}

	return status;
}

std::string StateMachine::getDeviceStatusAsString(eDeviceId device)
{
	std::string strStatus;

	eDeviceStatus status = getDeviceStatus(device);

	strStatus = getStatusAsString(status);

	return strStatus;
}

bool StateMachine::checkDeviceStatusCongruent(eDeviceId device, unsigned short maskStates)
{
	unsigned short maskTmp = 0x0000;

	switch(device)
	{
		case eIdModule :
			maskTmp = (m_statusModule & maskStates);
		break;
		case eIdSectionA :
			maskTmp = (m_statusSectionA & maskStates);
		break;
		case eIdSectionB :
			maskTmp = (m_statusSectionB & maskStates);
		break;
		case eIdNsh :
			maskTmp = (m_statusNsh & maskStates);
		break;
		case eIdCamera :
			maskTmp = (m_statusCamera & maskStates);
		break;
		default:
		break;
	}

	return (maskTmp != 0);
}

bool StateMachine::checkDeviceListStatusCongruent(std::vector<eDeviceId> deviceList, unsigned short maskStates)
{
	uint8_t i;

	for(i = 0; i < deviceList.size(); i++)
	{
		if(!checkDeviceStatusCongruent(deviceList.at(i), maskStates))
			return false;
	}
	return true;
}

eDeviceStatus StateMachine::executeCommand(eDeviceId device)
{

	// when a command has to be executed if the device is in Protocol it stays in Protocol

	eDeviceStatus status = getDeviceStatus(device);
	eDeviceStatus statusTarget = status;
/*  BUSY STATE is not considered as a real state (the lock prevents multiple commands on same device)
	switch (status)
	{
		case eStatusDisable:
		case eStatusSleep:
		case eStatusError:
		case eStatusIdle:
			statusTarget = eStatusBusy;
		break;
		default:
		break;
	}
*/
	if(!setDeviceStatus(device, statusTarget))
		return eStatusUnknown;

	return statusTarget;
}

bool StateMachine::updateModuleStatus()
{
	// check if at least one module is SLEEP and the not sleeping are disabled ...
	uint8_t sleeping = 0, disabled = 0;

	if(m_statusSectionA == eStatusSleep)		sleeping++;
	else if(m_statusSectionA == eStatusDisable)	disabled++;

	if(m_statusSectionB == eStatusSleep)		sleeping++;
	else if(m_statusSectionB == eStatusDisable)	disabled++;

	if(m_statusNsh == eStatusSleep)			sleeping++;
	else if(m_statusNsh == eStatusDisable)	disabled++;

	if(m_statusCamera == eStatusSleep)			sleeping++;
	else if(m_statusCamera == eStatusDisable)	disabled++;


	if(sleeping > 0 && (sleeping + disabled) == 4 )
	{
		// all devices (not disabled) are in sleep -> the module is in sleep
		m_statusModule = eStatusSleep;
	}
	else if(m_statusSectionA == eStatusDisable &&
		m_statusSectionB  == eStatusDisable )
	{
		// both sections are disabled -> the module is disabled
		m_statusModule = eStatusDisable;
	}
	else if(m_statusNsh == eStatusDisable)
	{
		// NSH is disabled -> the module is disabled
		m_statusModule = eStatusDisable;
	}
	else if(m_statusSectionA == eStatusInit ||
			m_statusSectionB == eStatusInit ||
			m_statusNsh == eStatusInit ||
			m_statusCamera == eStatusInit)
	{
		// at least one device is in init -> the module is in init
		m_statusModule = eStatusInit;
	}
	else if(m_statusSectionA == eStatusProtocol ||
			m_statusSectionB == eStatusProtocol)
	{
		// at least one section is in protocol -> the module is in Protocol
		m_statusModule = eStatusProtocol;
	}
	else if ( m_statusSectionA == eStatusReadsec ||
			  m_statusSectionB == eStatusReadsec ||
			  m_statusNsh == eStatusReadsec ||
			  m_statusCamera == eStatusReadsec )
	{
		m_statusModule = eStatusReadsec;
	}
	else
	{
		// default status -> idle
		m_statusModule = eStatusIdle;
	}
	return true;
}

bool StateMachine::isDeviceDisabled(eDeviceId device)
{
	return (getDeviceStatus(device) == eStatusDisable);
}

bool StateMachine::isDeviceSleeping(eDeviceId device)
{
	return (getDeviceStatus(device) == eStatusSleep);
}

bool StateMachine::isDeviceAbleToAcceptCommands(eDeviceId device)
{
#ifdef __DEBUG_PROTOCOL
	return true;
#endif

	if(isDeviceDisabled(device) || isDeviceSleeping(device))
		return false;

	return true;
}

bool StateMachine::isDeviceInError(eDeviceId device)
{
	return (getDeviceStatus(device) == eStatusError);
}

std::string StateMachine::getStatusAsString(eDeviceStatus status)
{
	std::string strStatus;
	uint i = 0, mask = 0;

	strStatus.clear();

	std::vector<std::string>::const_iterator strIter;
	for(i = 0, strIter = global_strDeviceStatus.begin(); strIter != global_strDeviceStatus.end(); i++, strIter++)
	{
		mask = (0x01 << i);
		if(status == mask)
		{
			strStatus.assign(strIter->c_str());
			break;
		}
	}
	return strStatus;
}


bool StateMachine::setDeviceLock(eDeviceId device, eDeviceLockStatus status)
{
	// if the request is to lock a device already locked -> failed
	eDeviceLockStatus* pLockDevice;

	switch(device)
	{
		case eIdModule :
			pLockDevice = &m_lockModule;
		break;
		case eIdSectionA :
			pLockDevice = &m_lockSectionA;
		break;
		case eIdSectionB :
			pLockDevice = &m_lockSectionB;
		break;
		case eIdNsh :
			pLockDevice = &m_lockNsh;
		break;
		case eIdCamera :
			pLockDevice = &m_lockCamera;
		break;
		default:
			return false;
		break;
	}

	if(status == eLockOn && *pLockDevice == eLockOn)
		return false;

	*pLockDevice = status;
	return true;
}

bool StateMachine::setDeviceListLockOn(std::vector<eDeviceId> deviceList, bool bForceLock)
{
	uint8_t i = 0;

	for(i = 0; i < deviceList.size(); i++)
	{
        if(!setDeviceLock(deviceList.at(i), eLockOn) && bForceLock == false)
		{
            // the lock has failed (and the force flag is disabled - default situation) -> restore
			break;
		}
	}

	if(i < deviceList.size())
	{
		// the previous loop has stopped somewhere -> clear the locks set
		uint8_t j;
		for(j = 0; j < i; j++)
		{
			setDeviceLock(deviceList.at(j), eLockOff);
		}
		return false;
	}

	return true;
}

eDeviceLockStatus StateMachine::getDeviceLock(eDeviceId device)
{
	eDeviceLockStatus status = eLockOff;

	switch(device)
	{
		case eIdModule :
			status = m_lockModule;
		break;
		case eIdSectionA :
			status = m_lockSectionA;
		break;
		case eIdSectionB :
			status = m_lockSectionB;
		break;
		case eIdNsh :
			status = m_lockNsh;
		break;
		case eIdCamera :
			status = m_lockCamera;
		break;
		default:
		break;
	}

	return status;
}


