/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OperationManager.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OperationManager class.
 @details

 ****************************************************************************
*/

#include <unistd.h>
#include <string.h>

#include "OperationManager.h"
#include "CommonInclude.h"
#include "Operation.h"

OperationManager::OperationManager()
{
	m_liMaxOperations = WF_MQ_OPMAN_MAX_NUM_MESSAGES;
	m_liRunningOperations = 0;
}

OperationManager::~OperationManager()
{
	m_operationMsgQueue.closeAndUnlink();
}

bool OperationManager::init(void)
{
	/* ****************************************************************************************************************
	 * Initializes operation msgQueue
	 * ****************************************************************************************************************
	 */
	m_operationMsgQueue.enableDebug(false);
	bool bRet = m_operationMsgQueue.create(WF_MQ_OPMAN_TO_POSTMAN_NAME,
										   O_WRONLY,
										   WF_MQ_OPMAN_MAX_NUM_MESSAGES,
										   WF_MQ_OPMAN_MAX_MSG_SIZE);
	if ( bRet == false )
	{
		log(LOG_ERR, "OperationManager::init: unable to create message queue. Exit.");
		return false;
	}

	log(LOG_INFO, "OperationManager::init: operation queue \"%s\" correctly initialized", WF_MQ_OPMAN_TO_POSTMAN_NAME);

	/* ****************************************************************************************************************
	  *	MESSAGE QUEUE PRE EMPTYING
	  * This server can have been respawned, so before starting the normal real-time service
	  * it is necessary to perform a cleaning of the remained buffers
	  * ***************************************************************************************************************
	  */
	bRet = m_operationMsgQueue.empty();
	if ( bRet == false )
	{
		log(LOG_ERR, "OperationManager::init: unable to PRE-EMPTY message queue. Exit.");
		return false;
	}

	/* ****************************************************************************************************************
	 *	Postman thread initialization
	 * ****************************************************************************************************************
	 */
	m_postman.setLogger(getLogger());
	bool b = m_postman.startThread();
	if ( b != true )
	{
		log(LOG_ERR, "OperationManager::init: error creating postman thread");
		return false;
	}
	log(LOG_INFO, "OperationManager::init: postman thread correctly created and started");
	return true;
}

bool OperationManager::stop(void)
{

	/* ****************************************************************************************************************
	 * Stop Postman
	 * ****************************************************************************************************************
	 */
	while ( m_postman.isRunning() )
	{
		m_postman.stopThread();
		usleep(100000);
	}
	log(LOG_INFO, "OperationManager::stop: postman thread correctly stopped");

	/* ****************************************************************************************************************
	 * Close and unlink the message queue
	 * ****************************************************************************************************************
	 */
	bool bRet = m_operationMsgQueue.closeAndUnlink();
	if ( bRet == false )
	{
		log(LOG_ERR, "OperationManager::stop: error from mq_unlink()");
	}
	else
	{
		log(LOG_INFO, "OperationManager::stop: message queue unlinked.");
	}

	return bRet;

}

Mutex tmpMutex;

bool OperationManager::sendMsgToPostman(Operation* pOperation)
{
	tmpMutex.lock();
	// Retrieve number of messages in the queue
	int liMsgCount = m_operationMsgQueue.retrieveCurrentNumMessages();
	if ( liMsgCount >= WF_MQ_OPMAN_MAX_NUM_MESSAGES )
	{
		log(LOG_ERR, "OperationManager::sendMsgToPostman: number of messages in queue is too high (%d)", liMsgCount);
		tmpMutex.unlock();
		return false;
	}
	log(LOG_DEBUG_PARANOIC, "OperationManager::sendMsgToPostman: number of messages in queue = %d", liMsgCount);

	// Send message on the queue
	int liBufSize = WF_MQ_OPMAN_MAX_MSG_SIZE-1;
	char pOutBuffer[WF_MQ_OPMAN_MAX_MSG_SIZE-1];
	memset(pOutBuffer, 0x00, WF_MQ_OPMAN_MAX_MSG_SIZE-1);

	*(Operation**)pOutBuffer = pOperation;

	bool bRetvalue = m_operationMsgQueue.send(pOutBuffer, liBufSize);
	if ( ! bRetvalue )
	{
		log(LOG_ERR, "OperationManager::sendMsgToPostman: unable to send message");
	}
	else
	{
		int liMsgCount = m_operationMsgQueue.retrieveCurrentNumMessages();
		log(LOG_DEBUG, "OperationManager::sendMsgToPostman: sent numMsg=%d", liMsgCount);
	}
	tmpMutex.unlock();
	return bRetvalue;
}

bool OperationManager::handleOperation(Operation* pOperation, eOperationStatus liStatus)
{
	if ( ( liStatus == eOperationWaitForBirth ) && ( m_liRunningOperations >= m_liMaxOperations ) )
	{
		log(LOG_ERR, "OperationManager::handleOperation: reached maximum number of parallel operations");
		return false;
	}

	m_operationListMutex.lock();

	/* ********************************************************************************************
	 * Do some checks on the operation
	 * ********************************************************************************************
	 */
	if ( pOperation == NULL )
	{
		log(LOG_ERR, "OperationManager::handleOperation: empty operation");
		m_operationListMutex.unlock();
		return false;
	}

	if  ( liStatus == eOperationWaitForBirth )
	{
		m_liRunningOperations++;
		log(LOG_DEBUG_PARANOIC, "OperationManager::eOperationWaitForBirth %d", m_liRunningOperations);
	}
	else if ( liStatus == eOperationWaitForDeath )
	{
		if (m_liRunningOperations > 0)
		{
			m_liRunningOperations--;
		}
		log(LOG_DEBUG_PARANOIC, "OperationManager::eOperationWaitForDeath %d %p", m_liRunningOperations, pOperation);
	}

	/* ********************************************************************************************
	 * Mark operation and send it to PostMan
	 * ********************************************************************************************
	 */
	pOperation->setStatus(liStatus);
	bool bRetValue = sendMsgToPostman(pOperation);

	m_operationListMutex.unlock();

	return bRetValue;

}

bool OperationManager::addOperation(Operation* pOperation)
{
	return handleOperation(pOperation, eOperationWaitForBirth);
}

bool OperationManager::removeOperation(Operation* pOperation)
{
	return handleOperation(pOperation, eOperationWaitForDeath);
}

bool OperationManager::isAvailable(void)
{
	if ( m_liRunningOperations >= m_liMaxOperations )
	{
		return false;
	}
	return true;
}
