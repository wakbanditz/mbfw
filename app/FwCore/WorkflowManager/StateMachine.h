/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    StateMachine.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the StateMachine class.
 @details

 ****************************************************************************
*/

#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "StatusInclude.h"

class StateMachine
{
	public:

		/*! ***********************************************************************************************************
		 * @brief StateMachine void constructor
		 * ************************************************************************************************************
		 */
		StateMachine();

		/*! ***********************************************************************************************************
		 * @brief init StateMachine initialization
		 * @return	true upon succesfull initialization, false otherwise
		 * ************************************************************************************************************
		 */
		bool init();

		/*! ***********************************************************************************************************
		 * @brief idle sets the whole StateMachine in IDLE state (debug)
		 * @return	true upon succesfull initialization, false otherwise
		 * ************************************************************************************************************
		 */
		bool idle();

		/*! ***********************************************************************************************************
		 * @brief setDeviceStatus to set the status of a specific device
		 * @param device the device id according to eDeviceId enum
		 * @param status the device status according to eDeviceStatus enum
		 * @return	true upon succesfull setting, false otherwise
		 * NOTE: do not call this function directly but use the changeDeviceStatus in MainExecutor (in order to
		 * send also the VidasEp to gateway)
		 * ************************************************************************************************************
		 */
		bool setDeviceStatus(eDeviceId device, eDeviceStatus status);

		/*! ***********************************************************************************************************
		 * @brief setDeviceTargetStatus to set the target status of a specific device to be set when called
		 * @param device the device id according to eDeviceId enum
		 * @param status the device status according to eDeviceStatus enum
		 * @return	true upon succesfull setting, false otherwise
		 * ************************************************************************************************************
		 */
		bool setDeviceTargetStatus(eDeviceId device, eDeviceStatus status);

		/*! ***********************************************************************************************************
		 * @brief setDeviceStatusToTarget to set the status of a specific device to the target previously set
		 * @param device the device id according to eDeviceId enum
		 * @return	true upon succesfull setting, false otherwise
		 * ************************************************************************************************************
		 */
		bool setDeviceStatusToTarget(eDeviceId device);

		/*! ***********************************************************************************************************
		 * @brief getDeviceStatus to get the status of a specific device
		 * @param device the device id according to eDeviceId enum
		 * @return the device status according to eDeviceStatus enum
		 * ************************************************************************************************************
		 */
		eDeviceStatus getDeviceStatus(eDeviceId device);

		/*! ***********************************************************************************************************
		 * @brief getDeviceStatusAsString to get the status of a specific device as a string
		 * @param device the device id according to eDeviceId enum
		 * @return the device string status according to global_strDeviceStatus
		 * ************************************************************************************************************
		 */
		std::string getDeviceStatusAsString(eDeviceId device);

		/*! ***********************************************************************************************************
		 * @brief getStatusAsString to get the string corresponding to a specific status
		 * @param status the status id according to eDeviceStatus enum
		 * @return the string status according to global_strDeviceStatus
		 * ************************************************************************************************************
		 */
		std::string getStatusAsString(eDeviceStatus status);

		/*! ***********************************************************************************************************
		 * @brief setDeviceLock to lock/unlock a specific device
		 * @param device the device id according to eDeviceId enum
		 * @param status the lock/unlock according to eDeviceLockStatus enum
		 * @return	true upon succesfull setting, false otherwise
		 * ************************************************************************************************************
		 */
		bool setDeviceLock(eDeviceId device, eDeviceLockStatus status);

		/*! ***********************************************************************************************************
		 * @brief setDeviceListLockOn to lock a list of device
		 * @param deviceList the device id according to eDeviceId enum
         * @param bForceLock        if true even if the device is locked the action is allowed
         * @return	true if all devices of the list are locked, false otherwise (devices lock unchanged)
		 * ************************************************************************************************************
		 */
        bool setDeviceListLockOn(std::vector<eDeviceId> deviceList, bool bForceLock = false);

		/*! ***********************************************************************************************************
		 * @brief getDeviceLock to get the lock status of a specific device
		 * @param device the device id according to eDeviceId enum
		 * @return the lock status according to eDeviceLockStatus enum
		 * ************************************************************************************************************
		 */
		eDeviceLockStatus getDeviceLock(eDeviceId device);

		/*! ***********************************************************************************************************
		 * @brief checkDeviceStatusCongruent function called by the workflow of each command to verify if the current
		 *				status of a specific device (including the module) is congruent with the command mask that
		 *				defines the states in which the command is allowed
		 * @param device the device id according to eDeviceId enum
		 * @param maskStates the bitmask to be compared
		 * @return	true upon succesfull check, false otherwise
		 * ************************************************************************************************************
		 */
		bool checkDeviceStatusCongruent(eDeviceId device, unsigned short maskStates);

		/*! ***********************************************************************************************************
		 * @brief checkDeviceListStatusCongruent function calling the checkDeviceStatusCongruent for all the
		 *					devices in list
		 * @param deviceList the list of device ids according to eDeviceId enum
		 * @param maskStates the bitmask to be compared
		 * @return	true upon succesfull check, false otherwise
		 * ************************************************************************************************************
		 */
		bool checkDeviceListStatusCongruent(std::vector<eDeviceId> deviceList, unsigned short maskStates);

		/*! ***********************************************************************************************************
		 * @brief executeCommand the specific device is going to execute a comand so it performs the transition:
		 *				IDLE -> BUSY or ERROR->BUSY or PROTOCOL->PROTOCOL or DISABLE->BUSY
		 *
		 * @param device the device id according to eDeviceId enum
		 * @return the new status of the device
		 * ************************************************************************************************************
		 */
		eDeviceStatus executeCommand(eDeviceId device);

		/*! ***********************************************************************************************************
		 * @brief isDeviceDisabled check if the specific device is in DISABLE status
		 *
		 * @param device the device id according to eDeviceId enum
		 * @return true if DISABLED, false otherwise
		 * ************************************************************************************************************
		 */
		bool isDeviceDisabled(eDeviceId device);

		/*! ***********************************************************************************************************
		 * @brief isDeviceSleeping check if the specific device is in SLEEP status
		 *
		 * @param device the device id according to eDeviceId enum
		 * @return true if SLEEP, false otherwise
		 * ************************************************************************************************************
		 */
		bool isDeviceSleeping(eDeviceId device);

		/*! ***********************************************************************************************************
		 * @brief isDeviceAbleToAcceptCommands check if the specific device is in SLEEP or DISABLE status
		 *
		 * @param device the device id according to eDeviceId enum
		 * @return false if SLEEP or DISABLE, true otherwise
		 * ************************************************************************************************************
		 */
		bool isDeviceAbleToAcceptCommands(eDeviceId device);

		/*! ***********************************************************************************************************
		 * @brief isDeviceInError check if the specific device is in ERROR status
		 *
		 * @param device the device id according to eDeviceId enum
		 * @return true if ERROR, false otherwise
		 * ************************************************************************************************************
		 */
		bool isDeviceInError(eDeviceId device);

	private:

		/*! ***********************************************************************************************************
		 * @brief updateModuleStatus to update the status of the global module when the status of a single
		 *					subdevice is changed
		 * @return	true upon succesfull setting, false otherwise
		 * ************************************************************************************************************
		 */
		bool updateModuleStatus();


	private:

		eDeviceStatus m_statusModule, m_statusSectionA, m_statusSectionB, m_statusNsh, m_statusCamera;
		eDeviceStatus m_statusTargetModule, m_statusTargetSectionA, m_statusTargetSectionB,
						m_statusTargetNsh, m_statusTargetCamera;
		eDeviceLockStatus  m_lockModule, m_lockSectionA, m_lockSectionB, m_lockNsh, m_lockCamera;
};

#endif // STATEMACHINE_H
