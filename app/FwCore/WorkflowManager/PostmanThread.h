/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PostmanThread.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the PostmanThread class.
 @details

 ****************************************************************************
*/

#ifndef PORTERTHREAD_H
#define PORTERTHREAD_H

#include "Thread.h"
#include "MessageQueue.h"
#include "Operation.h"
#include "Loggable.h"
#include <list>

using namespace std;

class PostmanThread :	public Thread,
						public Loggable
{

	public:

		PostmanThread();
		virtual ~PostmanThread();

	protected:

		int	workerThread(void);

	private:

		bool removeOpFromList(Operation * pOp);


    private:

        list<Operation*> m_operationList;
        MessageQueue m_operationMsgQueue;

};

#endif // PORTERTHREAD_H
