/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THMonitorPressure.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the THMonitorPressure class.
 @details

 ****************************************************************************
*/

#include "THMonitorPressure.h"
#include "OpMonitorPressure.h"
#include "MainExecutor.h"
#include "OutCmdMonitorPressure.h"
#include "MonitorPressurePayload.h"
#include "WebServerAndProtocolInclude.h"
#include "UdpClient.h"
#include "RunwlInfo.h"

#include "CommonInclude.h"

#define SEM_MONITOR_PRESSURE_SYNCH_NAME			"/sem-monitorpressure-synch"
#define SEM_MONITOR_PRESSURE_SYNCH_INIT_VALUE		0

#define SEM_SEND_PRESSURE_SYNCH_NAME			"/sem-sendpressure-synch"
#define SEM_SEND_PRESSURE_SYNCH_INIT_VALUE		0

const uint8_t raw_header_chars[] = {0x00, 0x01, 0x91, 0x00, 0x00, 0x00, 0x00, 0x00 };
const uint8_t raw_footer_chars[] = {0x00, 0x01, 0x93, 0x00, 0x00, 0x00, 0x00, 0x00 };

#define RAW_DATA_HEADER	0x00000091
#define RAW_DATA_FOOTER	0x00000093

#define RAW_DATA_ID		0x94

static Mutex m_mutexPressure;

// ----------------------------------------------------------------------------------- //

THMonitorPressure::THMonitorPressure()
{
	m_synchSem.create(SEM_MONITOR_PRESSURE_SYNCH_NAME, SEM_MONITOR_PRESSURE_SYNCH_INIT_VALUE);
	m_synchSem.enableErrorPrint(true);

	for(uint8_t i = 0; i < SCT_NUM_TOT_SECTIONS; i++)
	{
		m_sectionAction[i] = MONITOR_PRESSURE_STOP;
		m_pOperation[i] = 0;
		clearDataVectors(i);
		resetChannelsStructure(i, SCT_NUM_TOT_SLOTS + 1);
		m_sectionChangeAction[i] = false;
	}

	m_monitorInProgress = false;
}

THMonitorPressure::~THMonitorPressure()
{
	clearDataVectors(SCT_A_ID);
	clearDataVectors(SCT_B_ID);
}

bool THMonitorPressure::isMonitorActive()
{
	for(int i = 0; i < SCT_NUM_TOT_SECTIONS; i++)
	{
		if(m_sectionAction[i] == MONITOR_PRESSURE_START ||
			m_sectionAction[i] == MONITOR_PRESSURE_PROTOCOL_START )
			return true;
	}

	return false;
}

int THMonitorPressure::getSectionAction(int section)
{
	return m_sectionAction[section];
}

int THMonitorPressure::startRawMonitoring(OpMonitorPressure* pOp)
{
	// this is the perform called by external command

	if ( pOp == 0 )
	{
		log(LOG_ERR, "THMonitorPressure::perform: trying to start with null operation");
		return eOperationError;
	}

	if(pOp->getAction() != MONITOR_PRESSURE_START )
	{
		// the command is not congruent
		return eOperationError;
	}

	if(pOp->getSection() == SCT_NONE_ID)
	{
		// error in calling the function
		return eOperationError;
	}

	if ( m_pOperation[pOp->getSection()] != 0 )
	{
		log(LOG_ERR, "THMonitorPressure::perform: BAD! trying to start with an already running operation");
		return eOperationError;
	}

	uint8_t section = pOp->getSection();

	m_pOperation[section] = pOp;

	// it may happen that the section is already acquiring data because of protocol execution
	// and the service software asks for sending data: in this case the action of the protocol is kept
	if(m_sectionAction[section] == MONITOR_PRESSURE_STOP)
	{
		m_sectionAction[section] = pOp->getAction();
		m_sectionChangeAction[section] = true;
	}
	else
	{
        // the monitoring was already activated by the protocol ->
		// clear the data array so to start sending from now on
		m_mutexPressure.lock();
		for(uint8_t i = 0; i < SCT_NUM_TOT_SLOTS; i++)
		{
			m_dataPressureChannel[section][i].clear();
            m_dataPressureRawChannel[section][i].clear();
		}
		m_mutexPressure.unlock();
	}

	if(m_monitorInProgress == false)
	{
		// the thread is not running -> unlock the semaphore
		int liRetVal =  m_synchSem.release();
		if ( liRetVal == SEM_FAILURE )
		{
			log(LOG_ERR, "THMonitorPressure::perform: unable to unlock thread synch semaphore for op=<%p:%s>",
				pOp, pOp->getName().c_str());
			return eOperationError;
		}
	}

    if(THSendPressure::getInstance()->isSendingActive() == false)
	{
		// unlock the thread that sends data
        std::string strWebUrl, strCommandId;
        strWebUrl.assign(m_pOperation[section]->getWebUrl());
        strCommandId = m_pOperation[section]->getID();

        log(LOG_INFO, "THMonitorPressure::perform: start sending to %s %s", strWebUrl.c_str(), strCommandId.c_str());

        if(THSendPressure::getInstance()->startSending(this, strWebUrl, strCommandId) == false)
		{
			log(LOG_ERR, "THMonitorPressure::perform: unable to start sending for op=<%p:%s>",
				pOp, pOp->getName().c_str());
			return eOperationError;
		}
	}

	log(LOG_INFO, "THMonitorPressure::perform: starting with operation <%p:%s>", pOp, pOp->getName().c_str());

	// it may happen that the section is already acquiring data because of protocol execution
	// and the service software asks for sending data: in this case the reply is immediately sent
	if(m_sectionAction[section] == MONITOR_PRESSURE_PROTOCOL_START)
	{
		// send the command reply
		sendReply(section);
	}

	return eOperationRunning;
}

int THMonitorPressure::stopRawMonitoring(OpMonitorPressure* pOp)
{
	// this is the perform called by external command

	if ( pOp == 0 )
	{
		log(LOG_ERR, "THMonitorPressure::stopMonitoring: trying to start with null operation");
		return eOperationError;
	}

	if(pOp->getAction() != MONITOR_PRESSURE_STOP )
	{
		// the command is not congruent
		return eOperationError;
	}

	if(pOp->getSection() == SCT_NONE_ID)
	{
		// error in calling the function
		return eOperationError;
	}

	if ( m_pOperation[pOp->getSection()] != 0)
	{
		log(LOG_ERR, "THMonitorPressure::stopMonitoring: BAD! trying to stop with a running operation");
		return eOperationError;
	}

	uint8_t section = pOp->getSection();

	m_pOperation[section] = pOp;

	// it may happen that the section is already acquiring data because of protocol execution
	// and the service software asks for sending data: in this case the action of the protocol is kept
	if(m_sectionAction[section] == MONITOR_PRESSURE_START)
	{
        // send the command
        sectionBoardLinkSingleton(section)->enablePressureAcquisition(eStopAcq, 1000);
        log(LOG_INFO, "THMonitorPressure::stopSending");
    }
	else
	{
		// in this case we keep the acquisition ongoing (protocol) but stop data sending
        if(THSendPressure::getInstance()->isSendingActive() == true)
		{
            log(LOG_INFO, "THMonitorPressure::stopSending in Protocol");
            THSendPressure::getInstance()->stopSending();
		}
		// and send the command reply
		sendReply(section);
	}

	return eOperationRunning;
}

int THMonitorPressure::startProtocolMonitoring(uint8_t section)
{
	// this is the start called by protocol execution

	m_sectionAction[section] = MONITOR_PRESSURE_PROTOCOL_START;
	m_sectionChangeAction[section] = true;
	m_pOperation[section] = 0;

	// open (clearing them) the output treated data file:
	// data are saved only if acquisition is performed during a protocol
	for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
	{
		char nameFile[64];
		sprintf(nameFile, "%s%s%c%c%s", RUN_FOLDER, PROTO_TREATED_PREFIX, SECTION_CHAR_A + section,
				ZERO_CHAR + channel + 1, PROTO_TREATED_SUFFIX);
		m_pfile[section][channel].open(nameFile, ios_base::out | ios_base::binary);

		sprintf(nameFile, "%s%s%c%c%s", RUN_FOLDER, PROTO_ELABORATED_PREFIX, SECTION_CHAR_A + section,
				ZERO_CHAR + channel + 1, PROTO_ELABORATED_SUFFIX);
		remove(nameFile);
	}


	if(m_monitorInProgress == false)
	{
		// the thread is not running -> unlock the semaphore
		int liRetVal =  m_synchSem.release();
		if ( liRetVal == SEM_FAILURE )
		{
			log(LOG_ERR, "THMonitorPressure:: unable to unlock semaphore for protocol on section %c", SECTION_CHAR_A + section);
			return eOperationError;
		}
	}

	log(LOG_INFO, "THMonitorPressure:: protocol starting on section %c", SECTION_CHAR_A + section);
	return eOperationRunning;
}

bool THMonitorPressure::isActionPending(uint8_t section)
{
	return m_sectionChangeAction[section];
}

int THMonitorPressure::stopProtocolMonitoring(uint8_t section)
{
	// this is the stop called by protocol execution

	if(m_sectionAction[section] == MONITOR_PRESSURE_PROTOCOL_START)
	{
		m_sectionAction[section] = MONITOR_PRESSURE_STOP;
		m_sectionChangeAction[section] = true;
		m_pOperation[section] = 0;
	}
	return eOperationRunning;
}

void THMonitorPressure::beforeWorkerThread(void)
{
	log(LOG_INFO, "THMonitorPressure::started");
}

int THMonitorPressure::workerThread(void)
{
	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "THMonitorPressure::th: unlocked but wait returned %d", liSynchRet);
		}

		// signal the thread is running
		m_monitorInProgress = true;

		log(LOG_INFO, "THMonitorPressure::th: unlocked, monitor active");

		bool monitorActive = true;
		while(monitorActive)
		{
			uint8_t section;

			for(section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
			{
                if(m_sectionAction[section] == MONITOR_PRESSURE_PROTOCOL_START ||
                    m_sectionAction[section] == MONITOR_PRESSURE_START)
                {
                    for(uint8_t channel = 1; channel < SCT_NUM_TOT_SLOTS + 1; channel++)
					{
						uint8_t dataCan[MAX_LEN_PAYLOAD_PACK];

						// attention: here the channel index is 1-6 ....
						if(sectionBoardLinkSingleton(section)->getStoredData(channel,
																			 dataCan,
																			 MAX_LEN_PAYLOAD_PACK))
						{
							// ... while here is 0-5
							decodeMsgPressure(section, dataCan, channel - 1);
						}
                    }
				}
				usleep(2);

				// check if a operation has to be peformed on the section
                executeAction(section);
			}

            if(m_monitorInProgress == false)
			{
				monitorActive = false;	// no data read from buffers and acquisition stopped -> end it
                log(LOG_INFO, "THMonitorPressure: no more data");
            }
		}

		// stop also the sending thread
        if(THSendPressure::getInstance()->isSendingActive() == true)
		{
			THSendPressure::getInstance()->stopSending();
            // clear remaining data ?
            m_mutexPressure.lock();
            for(uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
            {
                for(uint8_t channel = 1; channel < SCT_NUM_TOT_SLOTS + 1; channel++)
                {
                    m_dataPressureChannel[section][channel].clear();
                    m_dataCounterChannel[section][channel] = 1;
                }
            }
            m_mutexPressure.unlock();
        }

		log(LOG_INFO, "THMonitorPressure: done");
	}
	return 0;
}

void THMonitorPressure::afterWorkerThread(void)
{
	m_synchSem.closeAndUnlink();
	log(LOG_INFO, "THMonitorPressure::stopped");
}

bool THMonitorPressure::executeAction(uint8_t section)
{
	bool bRunThread = true;

	if(m_sectionChangeAction[section] == false)
	{
		return bRunThread;		// no action to do
	}

	if(m_sectionAction[section] == MONITOR_PRESSURE_STOP)
	{
		// offline
        // sectionBoardLinkSingleton(section)->enablePressureAcquisition(eStopAcq, 1000);

        // close the data file
        closeRawDataFiles(section);
	}
	else
	{
		// clear data arrays of all channels
		clearDataVectors(section);
		// reset the status machine of all channels
		resetChannelsStructure(section, SCT_NUM_TOT_SLOTS + 1);
		// reset the data buffer
		sectionBoardLinkSingleton(section)->clearAuxData();

		// the start command to section is different according to the context (protocol or offline)
		if(m_sectionAction[section] == MONITOR_PRESSURE_START)
		{
			// offline
			sectionBoardLinkSingleton(section)->enablePressureAcquisition(eStartAcq, 1000);
		}
	}
	// send the last empty reply to signal the start/stop command is executed
	sendReply(section);

	// signal the thread to stop (if both sections disabled)
	uint8_t i;
	for(i = 0; i < SCT_NUM_TOT_SECTIONS; i++)
	{
		if(m_sectionAction[i] == MONITOR_PRESSURE_START ||
			m_sectionAction[i] == MONITOR_PRESSURE_PROTOCOL_START )
			break;
	}
	if(i == SCT_NUM_TOT_SECTIONS)
	{
		bRunThread = false;
        m_monitorInProgress = false;
        log(LOG_INFO, "THMonitorPressure: going to stop thread");
    }

	m_sectionChangeAction[section] = false;

	return bRunThread;
}

bool THMonitorPressure::sendReply(uint8_t section)
{
	bool bSerializeResult = true;

	// these operations have to be performed only if the operation is valid
	// (i.e. when the start / stop command has just been sent and it's waiting for the reply)

	if(m_pOperation[section])
	{
		// set the status as SUCCESFULL so that the reply is composed with all the parameters
		OutCmdMonitorPressure cmd;
		cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
		cmd.setUsage(m_pOperation[section]->getUsage());
		cmd.setID(m_pOperation[section]->getID());

		// in this message no payload attached as it's simply the reply to command

		/* ********************************************************************************************
		 * Send the OutgoingCommand to the client
		 * ********************************************************************************************
		 */
		std::string strOutCommand;
		bSerializeResult = protocolSerialize(&cmd, strOutCommand);
		if ( bSerializeResult == false )
		{
			log(LOG_ERR, "THMonitorPressure: unable to serialize OutCmdMonitorPressure");
		}
		else
		{
			m_pOperation[section]->sendCommandReply(strOutCommand);
		}

/* as the command can be sent even during a protocol execution no interact with state machine
		m_pOperation[section]->restoreStateMachine();
*/

		/* ********************************************************************************************
		 * Remove the operation from the OperationManager list
		 * ********************************************************************************************
		 */
		operationSingleton()->removeOperation(m_pOperation[section]);
		m_pOperation[section] = 0;
	}

	return bSerializeResult;
}

void THMonitorPressure::closeRawDataFiles(uint8_t section)
{
	for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
	{
		if ( m_pfile[section][channel].is_open() )
		{
			m_pfile[section][channel].close();
		}
	}
}

void THMonitorPressure::clearDataVectors(uint8_t section)
{
	for(uint8_t i = 0; i < SCT_NUM_TOT_SLOTS; i++)
	{
		m_dataPressureChannel[section][i].clear();
		m_dataPressureElabChannel[section][i].clear();
		m_dataCounterChannel[section][i] = 1;
		m_dataElabCounterChannel[section][i] = 0;
        m_dataPressureRawChannel[section][i].clear();
	}
}

unsigned int THMonitorPressure::getDataVectorSize(uint8_t section, uint8_t channel)
{
	return m_dataPressureChannel[section][channel].size();
}

void THMonitorPressure::resetChannelsStructure(uint8_t section, uint8_t index)
{
	uint8_t i, start, end;

	start = index;
	end = index + 1;
	if(index ==	SCT_NUM_TOT_SLOTS + 1)
	{
		start = 0;
		end = SCT_NUM_TOT_SLOTS;
	}

	for(i = start; i < end; i++)
	{
		m_dataPacketFromChannel[section][i].stateRxMsg = CH_CAN_RX_WAIT_STX;
        m_bChannelInProgress[section][i] = false;
    }
}

bool THMonitorPressure::decodeMsgPressure(uint8_t section, uint8_t * dataCan, uint8_t channel)
{
	bool bRet = true;

	switch ( m_dataPacketFromChannel[section][channel].stateRxMsg )
	{
		case CH_CAN_RX_WAIT_STX:
		{
			if ( memcmp(dataCan, raw_header_chars, MAX_LEN_PAYLOAD_PACK) == 0 )
			{
                // pressure raw data NOT in protocol
                log(LOG_INFO, "Pressure:headerRaw %d %d", section, channel);
                m_bChannelInProgress[section][channel] = true;

                // if all 8 chars are valid for comparison -> raw data packet NOT in protocol
				m_mutexPressure.lock();

				m_dataPacketFromChannel[section][channel].stateRxMsg = CH_CAN_RX_RECV_RAW_BODY;

				m_dataPressureChannel[section][channel].push_back(m_dataCounterChannel[section][channel]);
				// we assign a fake value (higher than the maximum value from ADC 24 bits)
				m_dataPressureChannel[section][channel].push_back(RAW_DATA_HEADER_ADC_VALUE);
				// increase counter
				++m_dataCounterChannel[section][channel];

				m_mutexPressure.unlock();

			}
			else if ( memcmp(dataCan, raw_header_chars, 3) == 0 )
			{
                log(LOG_INFO, "Pressure:headerTreat %d %d", section, channel);
				// raw data packet during protocol (only first 3 chars)

                m_mutexPressure.lock();

				m_dataPacketFromChannel[section][channel].stateRxMsg = CH_CAN_RX_RECV_RAW_BODY;

                m_dataPressureRawChannel[section][channel].clear();
                // when the header is found the second part of the candata identifies the
				// info on the phase that is starting:
				// we compose 2 values: in the fist one we assign a fake value (higher than the maximum
				// value from ADC 24 bits) + the first info (cmd)
				// and in a second value we add the other info (cycle_counter, well, volume_char, speed_char)

				m_dataPressureChannel[section][channel].push_back(m_dataCounterChannel[section][channel]);
				uint32_t value = RAW_DATA_HEADER_ADC_VALUE;
				value += uint32_t(dataCan[3]);
				m_dataPressureChannel[section][channel].push_back(value);
                // transfer data to protocol array  ( -> file)
                m_dataPressureRawChannel[section][channel].push_back(value);
                // increase counter
				++m_dataCounterChannel[section][channel];

				m_dataPressureChannel[section][channel].push_back(m_dataCounterChannel[section][channel]);
				value = 0;
				value += uint32_t(dataCan[4] << 24) + uint32_t(dataCan[5] << 16) +
							uint32_t(dataCan[6] << 8) + uint32_t(dataCan[7]);
				m_dataPressureChannel[section][channel].push_back(value);
                // transfer data to protocol array  ( -> file)
                m_dataPressureRawChannel[section][channel].push_back(value);
                // increase counter
				++m_dataCounterChannel[section][channel];

				// we add also a third value that is the time (counted from protocol start)
				// at which the acquisition is starting
				m_dataPressureChannel[section][channel].push_back(m_dataCounterChannel[section][channel]);
                value = protocolThreadPointer()->getSectionFromStartMseconds(section);
				m_dataPressureChannel[section][channel].push_back(value);
                // transfer data to protocol array  ( -> file)
                m_dataPressureRawChannel[section][channel].push_back(value);
                // increase counter
				++m_dataCounterChannel[section][channel];

				m_mutexPressure.unlock();

			}
			else if( memcmp(dataCan, elab_header_chars, MAX_LEN_PAYLOAD_PACK) == 0 )
			{
                log(LOG_INFO, "Pressure:headerElab %d %d", section, channel);
				m_mutexPressure.lock();

				// elaborated Data packet
				m_dataPacketFromChannel[section][channel].stateRxMsg = CH_CAN_RX_RECV_ELAB_BODY;
				// store the received bytes
				for(uint8_t i = 0; i < MAX_LEN_PAYLOAD_PACK; i++)
				{
					m_dataPressureElabChannel[section][channel].push_back(dataCan[i]);
				}

				m_mutexPressure.unlock();
				// set the first counter to be received
				m_dataElabCounterChannel[section][channel] = 1;
			}
		}
		break;

		case CH_CAN_RX_RECV_RAW_BODY:
		{
			if ( memcmp(dataCan, raw_footer_chars, MAX_LEN_PAYLOAD_PACK) == 0 )
			{
                log(LOG_INFO, "Pressure:footerRaw %d %d (%d)", section, channel, m_dataCounterChannel[section][channel]);
				m_mutexPressure.lock();

				// if the monitor is started from Service no header/footer data are sent back
				m_dataPressureChannel[section][channel].push_back(m_dataCounterChannel[section][channel]);
				// the value is set = 0 to identify it
				m_dataPressureChannel[section][channel].push_back(0);
                // increase counter
                ++m_dataCounterChannel[section][channel];
                // if in protocol we store data also in other array and transfer it to file
                if(m_sectionAction[section] == MONITOR_PRESSURE_PROTOCOL_START)
                {
                    // transfer data to protocol array  ( -> file)
                    m_dataPressureRawChannel[section][channel].push_back(0);
                    // transfer data to file
                    saveRawDataOnFile(section, channel);
                    // if the data are not sent to gateway (due to command MONITORPRESSSURE)
                    // we can remove them (in case the command is received sending will start from next block)
                    if(THSendPressure::getInstance()->isSendingActive() == false)
                    {
                        // remove all values and ..
                        m_dataPressureChannel[section][channel].clear();
                        // ... reset counter
                        m_dataCounterChannel[section][channel] = 1;
                    }

                }

				m_mutexPressure.unlock();

				// Reset of state machine
				resetChannelsStructure(section, channel);

                // if no more channels are in progress (Note: we are NOT in protocol)  -> stop it
                uint8_t i = 0;
                for(i = 0; i < SCT_NUM_TOT_SLOTS; i++)
                {
                    if(m_bChannelInProgress[section][i] == true)
                    {
                        break;
                    }
                }
                if(i == SCT_NUM_TOT_SLOTS && m_sectionAction[section] == MONITOR_PRESSURE_START)
                {
                    m_sectionAction[section] = MONITOR_PRESSURE_STOP;
                    m_sectionChangeAction[section] = true;
                    log(LOG_INFO, "THMonitorPressure: going to stop %c", SECTION_CHAR_A + section);
                }
            }
			else
			{
				// convert the received bytes to uint32_t
				if(dataCan[2] == RAW_DATA_ID)
				{
					m_mutexPressure.lock();

					// frame counter + 32 bit value
					// NOTE: the frame counter coming from section is not taken into account and
					// replaced with a local global counter
					// m_dataPressureChannel[section][channel].push_back(uint32_t(dataCan[3]));
					m_dataPressureChannel[section][channel].push_back(m_dataCounterChannel[section][channel]);

					uint32_t value = ( (dataCan[4] << 24) |
										(dataCan[5] << 16) |
										(dataCan[6] <<  8) |
										(dataCan[7]) );
					m_dataPressureChannel[section][channel].push_back(value);

                    // if in protocol we store data also in other array and transfer it to file
                    if(m_sectionAction[section] == MONITOR_PRESSURE_PROTOCOL_START)
                    {
                        // transfer data to protocol array  ( -> file)
                        m_dataPressureRawChannel[section][channel].push_back(value);
                    }

                    // increase counter
					++m_dataCounterChannel[section][channel];

                    m_mutexPressure.unlock();
                }
			}
		}
		break;

		case CH_CAN_RX_RECV_ELAB_BODY:
		{
            if ( dataCan[0] == 0xFF )
            {
                log(LOG_INFO, "Pressure:footerElab %d %d", section, channel);
				// final footer
				m_mutexPressure.lock();
				// store the received bytes
				for(uint8_t i = 0; i < MAX_LEN_PAYLOAD_PACK; i++)
				{
					m_dataPressureElabChannel[section][channel].push_back(dataCan[i]);
				}
				// save data on file
				saveElabDataOnFile(section, channel);
                m_mutexPressure.unlock();
                // Reset of state machine
				resetChannelsStructure(section, channel);
			}
			else
			{
				// first check if the counter received (first byte of packet) corresponds to the expected one
				if(dataCan[0] != m_dataElabCounterChannel[section][channel])
				{
					// errors: packet order not congruent -> abort the whole structure
                    log(LOG_ERR, "THMonitorPressure: wrong packet %d %d (%d vs %d) ", section, channel,
                            dataCan[0],	m_dataElabCounterChannel[section][channel]);

                    m_dataPressureElabChannel[section][channel].clear();
					// Reset of state machine
					resetChannelsStructure(section, channel);
				}
				else
				{
					m_mutexPressure.lock();
					// store the received bytes (excluding the counter)
					for(uint8_t i = 1; i < MAX_LEN_PAYLOAD_PACK; i++)
					{
						m_dataPressureElabChannel[section][channel].push_back(dataCan[i]);
					}
					m_mutexPressure.unlock();
					// increase and recirculate the counter
					if(++m_dataElabCounterChannel[section][channel] > 0xFE)
					{
						m_dataElabCounterChannel[section][channel] = 1;
					}
				}
			}

		}
		break;

		default:
			//Error
			m_pLogger->log(LOG_ERR, "THMonitorPressure::decodeMsgPressure--> default state machine");
			bRet = false;
		break;
	}

	return bRet;
}

void THMonitorPressure::saveElabDataOnFile(uint8_t section, uint8_t channel)
{
	// data are saved only if acquisition is performed during a protocol
	if(m_sectionAction[section] != MONITOR_PRESSURE_PROTOCOL_START) return;

	char nameFile[64];
	sprintf(nameFile, "%s%s%c%c%s", RUN_FOLDER, PROTO_ELABORATED_PREFIX, SECTION_CHAR_A + section, ZERO_CHAR + channel + 1, PROTO_ELABORATED_SUFFIX);

	ofstream pfile;
	pfile.open(nameFile, ios_base::app | ios_base::binary);

	if ( pfile.is_open() )
	{
        uint32_t counter;
        for(counter = 0; counter < m_dataPressureElabChannel[section][channel].size(); counter++)
        {
            pfile.write( reinterpret_cast<char*>(&m_dataPressureElabChannel[section][channel][counter]),
                                            sizeof(uint8_t) );
        }
        pfile.flush();
		pfile.close();
	}
    m_dataPressureElabChannel[section][channel].clear();
}

void THMonitorPressure::saveRawDataOnFile(uint8_t section, uint8_t channel)
{
    if(m_dataPressureRawChannel[section][channel].size() == 0) return;

	if ( m_pfile[section][channel].is_open() )
	{
        uint32_t counter;
        for(counter = 0; counter < m_dataPressureRawChannel[section][channel].size(); counter++)
        {
            m_pfile[section][channel].write( reinterpret_cast<char*>(&m_dataPressureRawChannel[section][channel][counter]),
                                            sizeof(uint32_t) );
        }
        m_pfile[section][channel].flush();
	}

    m_dataPressureRawChannel[section][channel].clear();
}

THSendPressure::THSendPressure()
{
	m_synchSem.create(SEM_SEND_PRESSURE_SYNCH_NAME, SEM_SEND_PRESSURE_SYNCH_INIT_VALUE);
	m_synchSem.enableErrorPrint(true);

	m_sendInProgress = false;
	m_strWebUrl.clear();
    m_strCommandId.clear();
	m_udpConnection = false;

	/* ********************************************************************************************
	 * READS CONFIGURATION FILE
	 * ********************************************************************************************
	 */
	char sConfigFileName[1024] = CONF_FILE;
	char sError[1024];

	Config config;
	config_key_t config_keys[] =
	{
		{ CFG_FW_CORE_PRESSURE_UDP_ENABLE, (char*)"pressure.sendUdp.enable",
											(char*)"GENERAL", T_string, (char*)"false", DEFAULT_ACCEPTED},

	};
	int num_keys = sizeof(config_keys) / sizeof(config_key_t);
	if ( !config.Init(config_keys, num_keys, sConfigFileName) )
	{
		Config::GetLastError(sError, 1024);
		log(LOG_ERR, "Impossible to read configuration file <%s> (error: %s)", sConfigFileName, sError);
		return;
	}

	string strUdpEnabled(config.GetString(CFG_FW_CORE_PRESSURE_UDP_ENABLE));
	if ( strUdpEnabled.compare("true") == 0 )
	{
		// use udp to send data
		m_udpConnection = true;
	}
}

THSendPressure::~THSendPressure()
{

}

bool THSendPressure::startSending(THMonitorPressure * pMonitorPressure, std::string strWebUrl, std::string strCommandId)
{
	if(pMonitorPressure == 0 || strWebUrl.empty())
	{
		return false;
	}

	int liRetVal =  m_synchSem.release();
	if ( liRetVal == SEM_FAILURE )
	{
		log(LOG_ERR, "THSendPressure:: unable to unlock thread synch semaphore");
		return false;
	}

	m_pMonitorPressure = pMonitorPressure;
	m_sendInProgress = true;
	m_strWebUrl.assign(strWebUrl);
    m_strCommandId.assign(strCommandId);

	log(LOG_INFO, "THSendPressure:: starting");
	return true;
}

void THSendPressure::stopSending()
{
	m_sendInProgress = false;
}

bool THSendPressure::isSendingActive()
{
	return m_sendInProgress;
}

int THSendPressure::workerThread()
{
	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "THSendPressure::th: unlocked but wait returned %d", liSynchRet);
		}

		log(LOG_INFO, "THSendPressure::th: unlocked sending section");

		bool connUdpValid = false;
		UdpClient * pUdpClient;
		if(m_udpConnection == true)
		{
			pUdpClient = new UdpClient();
			pUdpClient->setLogger(getLogger());
			connUdpValid = pUdpClient->openSocket();
		}

		uint8_t section;

		while(true)
		{
			unsigned int dataSizeTot = 0;
			unsigned int dataSizeSingle = 0;
			for(section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
			{
				// data are sent from all channels at the same time: checking how many for channel 1 it's enough ....
				dataSizeSingle = m_pMonitorPressure->getDataVectorSize(section, 0);
//                log(LOG_INFO, "THSendPressure 1: size %d %d", section, dataSizeSingle);

				if(m_udpConnection == true)
				{
					for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
					{
						// fill the local structure removing items from the main one
						if(m_pMonitorPressure->getDataVectorSize(section, channel) > 0)
						{
							m_mutexPressure.lock();

							uint32_t index = m_pMonitorPressure->m_dataPressureChannel[section][channel].front();
							m_pMonitorPressure->m_dataPressureChannel[section][channel].pop_front();
							uint32_t value = m_pMonitorPressure->m_dataPressureChannel[section][channel].front();
							m_pMonitorPressure->m_dataPressureChannel[section][channel].pop_front();

							m_mutexPressure.unlock();

							if(connUdpValid == true)
							{
								char udpMessage[32];

								sprintf(udpMessage, "%01d%01d%03d%010d", section, channel, index, value);
								pUdpClient->sendMessage(udpMessage, strlen(udpMessage));
								log(LOG_DEBUG_PARANOIC, "THSendPressure: udp %s", udpMessage);
							}
						}
					}
				}
				else
				{
                    if(dataSizeSingle > (10 * 2))	// ex 10 to slow down the rate
					{
						// 250 data are collected in 1 sec (sampling time = 4 msecs): for each value also the index is stored
                        sendHttpData(section, (10 * 2));
					}
					else if(dataSizeSingle > 0 && m_sendInProgress == false)
					{
						// the acquisition is stopped -> send last data
                        sendHttpData(section, dataSizeSingle);
					}
				}
				dataSizeTot += dataSizeSingle;
			}

			if(dataSizeTot == 0 && m_sendInProgress == false)
			{
				break;	// no data read from buffers and acquisition stopped -> end it
			}

			usleep(1);

		}

		if(m_udpConnection == true)
		{
			SAFE_DELETE(pUdpClient);
		}

		m_pMonitorPressure = 0;
		log(LOG_INFO, "THSendPressure: done");
	}
	return 0;
}

void THSendPressure::beforeWorkerThread()
{
	log(LOG_INFO, "THSendPressure::started");
}

void THSendPressure::afterWorkerThread()
{
	m_synchSem.closeAndUnlink();
	log(LOG_INFO, "THSendPressure::stopped");
}

bool THSendPressure::sendHttpData(uint8_t section, uint32_t dataSize)
{
	std::string strUsage;

	strUsage.assign(WEB_SERVICE_CONNECTION);

	Operation * pOperation = new Operation("SendPressure");
	pOperation->setUsage(strUsage);
	pOperation->setWebResponseUrl(m_strWebUrl);
    pOperation->setID(m_strCommandId);
	pOperation->setLogger(MainExecutor::getInstance()->getLogger());

	// set the status as SUCCESFULL so that the reply is composed with all the parameters
    OutCmdMonitorPressure * pCmd = new OutCmdMonitorPressure();
    pCmd->setStatus(XML_ATTR_STATUS_SUCCESSFULL);
    pCmd->setUsage(strUsage);
    pCmd->setID(m_strCommandId);

	// build the payload of the asynchrounous reply
	std::deque<uint32_t> dataPressureChannel;

	MonitorPressurePayload * pPressurePayload = new MonitorPressurePayload();
	pPressurePayload->setSection(section);

	for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
	{
		dataPressureChannel.clear();

		m_mutexPressure.lock();

        std::deque<uint32_t>::iterator dataIterator;
        dataIterator = m_pMonitorPressure->m_dataPressureChannel[section][channel].begin();

		// fill the local structure removing items from the main one
		if(m_pMonitorPressure->m_dataPressureChannel[section][channel].size() < dataSize)
		{
            dataPressureChannel.assign(dataIterator, m_pMonitorPressure->m_dataPressureChannel[section][channel].end());
            m_pMonitorPressure->m_dataPressureChannel[section][channel].erase(dataIterator,
						m_pMonitorPressure->m_dataPressureChannel[section][channel].end() );
		}
		else
		{
//            log(LOG_INFO, "THSendPressure 2: size %d %d", channel, m_pMonitorPressure->getDataVectorSize(section, channel));
            dataPressureChannel.assign(dataIterator, dataIterator + dataSize);
//            log(LOG_INFO, "THSendPressure 2: step");
            m_pMonitorPressure->m_dataPressureChannel[section][channel].erase(dataIterator, dataIterator + dataSize );
//            log(LOG_INFO, "THSendPressure 2: size %d %d", channel, m_pMonitorPressure->getDataVectorSize(section, channel));
        }

		m_mutexPressure.unlock();

		pPressurePayload->assignPressureData(channel, &dataPressureChannel);
	}
	// and assign it to the outcommand so that the serialize will extract the info
    pCmd->setPayload((Payload *)pPressurePayload);
    // Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************************************************
	 */
	std::string strOutCommand;
    bool bSerializeResult = protocolSerialize(pCmd, strOutCommand);
    if ( bSerializeResult == false )
	{
		log(LOG_ERR, "THSendPressure: unable to serialize OutCmdMonitorPressure");
	}
	else
	{
        if(pOperation->sendCommandReply(strOutCommand) == false)
        {
            log(LOG_ERR, "THSendPressure: error sending data");
        }
	}

//    log(LOG_INFO, "THSendPressure 3: pressure sent");
    SAFE_DELETE(pOperation);
//    log(LOG_INFO, "THSendPressure 3: pressure step");
    SAFE_DELETE(pCmd);
//    log(LOG_INFO, "THSendPressure 3: pressure end");
    return bSerializeResult;
}

