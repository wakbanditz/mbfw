/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THAutoLogout.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the THLogout class.
 @details

 ****************************************************************************
*/

#include "THAutoLogout.h"
#include "MainExecutor.h"

#include "CommonInclude.h"

#define SEM_LOGOUT_SYNCH_NAME			"/sem-logout-synch"
#define SEM_LOGOUT_SYNCH_INIT_VALUE		0

#define LOGOUT_SLEEP_SECS		1
#define LOGOUT_TIMEOUT_SECS		120	// 2 mins timeout (no hello command received)

THAutoLogout::THAutoLogout()
{
	m_synchSem.create(SEM_LOGOUT_SYNCH_NAME, SEM_LOGOUT_SYNCH_INIT_VALUE);
	m_synchSem.enableErrorPrint(true);

	m_cntCommercial = 0;
	m_cntService = 0;
	m_bThreadActive = false;
}

THAutoLogout::~THAutoLogout()
{

}

int THAutoLogout::startConnection(std::string strUsage)
{
	bool bStartThread = false;

	log(LOG_INFO, "THAutoLogout::perform: starting with operation %s", strUsage.c_str());

	if(strcmp(strUsage.c_str(), WEB_COMMERCIAL_CONNECTION) == 0)
	{
		// commercial connection: is it already active ? (should not happen ...)
		if(m_cntCommercial == 0)
		{
			m_cntCommercial = 1;
			bStartThread = true;
		}
	}
	else if(strcmp(strUsage.c_str(), WEB_SERVICE_CONNECTION) == 0)
	{
		// commercial connection: is it already active ? (should not happen ...)
		if(m_cntService == 0)
		{
			m_cntService = 1;
			bStartThread = true;
		}
	}
	else
	{
		log(LOG_ERR, "THAutoLogout::perform: unknown connection %s", strUsage.c_str());
		return 1;
	}

	// if requested and necessary (the thread could be already running due to the other connection) -> unlock it
	if(bStartThread && m_bThreadActive == false)
	{
		int liRetVal =  m_synchSem.release();
		if ( liRetVal == SEM_FAILURE )
		{
			log(LOG_ERR, "THAutoLogout::perform: unable to unlock thread synch semaphore");
			return 1;
		}

		log(LOG_INFO, "THAutoLogout::perform: starting ");
	}
	return 0;
}

void THAutoLogout::beforeWorkerThread(void)
{
	log(LOG_INFO, "THAutoLogout::started");
}

int THAutoLogout::workerThread(void)
{
	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "THAutoLogout::th: unlocked but wait returned %d", liSynchRet);
		}

		if(m_cntCommercial == 0 && m_cntService == 0)
		{
			log(LOG_WARNING, "THAutoLogout::th: unlocked but no connection active");
			continue;
		}

		m_bThreadActive = true;
		log(LOG_INFO, "THAutoLogout::th: unlocked, Service = %d, Commercial = %d ", m_cntService, m_cntCommercial);

		while(m_cntCommercial != 0 || m_cntService != 0)
		{
			sleep(LOGOUT_SLEEP_SECS);

			// check timeouts
			if(m_cntCommercial != 0)
			{
				m_cntCommercial++;
				if(m_cntCommercial > LOGOUT_TIMEOUT_SECS)
				{
					// no hello received -> disconnect the Commercial
					log(LOG_INFO, "THAutoLogout::th: disconnect Commercial = %d ", m_cntCommercial);
					MainExecutor::getInstance()->m_loginManager.removeLoginByUsage(WEB_COMMERCIAL_CONNECTION);
					m_cntCommercial = 0;
				}
			}

			if(m_cntService != 0)
			{
				m_cntService++;
				if(m_cntService > LOGOUT_TIMEOUT_SECS)
				{
					// no hello received -> disconnect the Service
					log(LOG_INFO, "THAutoLogout::th: disconnect Service = %d ", m_cntService);
					MainExecutor::getInstance()->m_loginManager.removeLoginByUsage(WEB_SERVICE_CONNECTION);
					m_cntService = 0;
				}
			}
		}

		m_bThreadActive = false;
		log(LOG_INFO, "THAutoLogout:th: done");
	}
	return 0;
}

void THAutoLogout::afterWorkerThread(void)
{
	m_synchSem.closeAndUnlink();
	log(LOG_INFO, "THAutoLogout::stopped");
}

void THAutoLogout::stopConnection(std::string strUsage)
{
    log(LOG_INFO, "THAutoLogout::stopping timeout on %s", strUsage.c_str());

	if(strcmp(strUsage.c_str(), WEB_COMMERCIAL_CONNECTION) == 0)
	{
		m_cntCommercial = 0;
	}
	else if(strcmp(strUsage.c_str(), WEB_SERVICE_CONNECTION) == 0)
	{
		m_cntService = 0;
	}
}

void THAutoLogout::resetConnection(std::string strUsage)
{
	log(LOG_DEBUG_PARANOIC, "THAutoLogout::reset timeout on %s", strUsage.c_str());

	if(strcmp(strUsage.c_str(), WEB_COMMERCIAL_CONNECTION) == 0)
	{
		if(m_cntCommercial > 0) m_cntCommercial = 1;
	}
	else if(strcmp(strUsage.c_str(), WEB_SERVICE_CONNECTION) == 0)
	{
		if(m_cntService > 0)	m_cntService = 1;
	}
}
