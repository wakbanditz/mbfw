/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THVidasEp.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the THVidasEp class.
 @details

 ****************************************************************************
*/

#ifndef THVIDASEP_H
#define THVIDASEP_H

#include "Thread.h"
#include "StaticSingleton.h"
#include "Loggable.h"
#include "Semaphore.h"

class THVidasEp :	public StaticSingleton<THVidasEp>,
						public Thread,
						public Loggable
{
	public:

		/*! ***********************************************************************************************************
		 * @brief THVidasEp default constructor
		 * ***********************************************************************************************************
		 */
		THVidasEp();

		/*! ***********************************************************************************************************
		 * @brief ~THVidasEp default destructor
		 * ***********************************************************************************************************
		 */
		virtual ~THVidasEp();

		/*! ***********************************************************************************************************
		 * @brief sendHttpData to unlock the thread and send back data
		 * ***********************************************************************************************************
		 */
		int sendHttpData(std::string strUsage = "", std::string strWebUrl = "", std::string strId = "");

	protected:

		/*! ***********************************************************************************************************
		 * @brief workerThread neverending thread normally waiting on the semaphore : when the semaphore it's released
		 *		the thread executes the ReadSection procedure and, finally, compose the asynchronous reply, restore
		 *		the state machine and destroys the operation
		 * @return 0 when thread ends
		 * ***********************************************************************************************************
		 */
		int workerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions before the start of the thread
		 * ***********************************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions after the end of the thread
		 * ***********************************************************************************************************
		 */
		void afterWorkerThread(void);

    private:
        Semaphore m_synchSem;

        uint8_t m_cntThreadActive;

        std::string m_strUsage, m_strWebUrl, m_strId;

};

#endif // THVIDASEP_H
