/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THAutoLogout.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the THAutoLogout class.
 @details

 ****************************************************************************
*/

#ifndef THAUTOLOGOUT_H
#define THAUTOLOGOUT_H


#include "Thread.h"
#include "StaticSingleton.h"
#include "Loggable.h"
#include "Semaphore.h"


class THAutoLogout :	public StaticSingleton<THAutoLogout>,
						public Thread,
						public Loggable
{
	public:

		/*! ***********************************************************************************************************
		 * @brief THAutoLogout default constructor
		 * ***********************************************************************************************************
		 */
		THAutoLogout();

		/*! ***********************************************************************************************************
		 * @brief ~THAutoLogout default destructor
		 * ***********************************************************************************************************
		 */
		virtual ~THAutoLogout();

		/*! ***********************************************************************************************************
		 * @brief perform checks the context of the procedure is correct and release the semaphore to unlock the thread
		 * @param strUsage the string identifying the connection (SERVICE or COMMERCIAL)
		 * @return 0 if succesfull, 1 otherwise
		 * ***********************************************************************************************************
		 */
		int startConnection(std::string strUsage);

		/*! ***********************************************************************************************************
		 * @brief stop the check on timeout of the specified connection
		 * @param strUsage the string identifying the connection (SERVICE or COMMERCIAL)
		 * ***********************************************************************************************************
		 */
		void stopConnection(std::string strUsage);

		/*! ***********************************************************************************************************
		 * @brief reset the timeout of the specified connection (when a Hello is received)
		 * @param strUsage the string identifying the connection (SERVICE or COMMERCIAL)
		 * ***********************************************************************************************************
		 */
		void resetConnection(std::string strUsage);

	protected:

		/*! ***********************************************************************************************************
		 * @brief workerThread neverending thread normally waiting on the semaphore : when the semaphore it's released
		 *		the thread executes the ReadSection procedure and, finally, compose the asynchronous reply, restore
		 *		the state machine and destroys the operation
		 * @return 0 when thread ends
		 * ***********************************************************************************************************
		 */
		int workerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions before the start of the thread
		 * ***********************************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions after the end of the thread
		 * ***********************************************************************************************************
		 */
		void afterWorkerThread(void);

    private:
        Semaphore m_synchSem;
        // timeout counters (in secs) on the 2 possibile connections
        uint8_t m_cntCommercial, m_cntService;

        bool m_bThreadActive;


};

#endif // THAUTOLOGOUT_H
