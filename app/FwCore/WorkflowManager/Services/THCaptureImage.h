/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THCaptureImage.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the THCaptureImage class.
 @details

 ****************************************************************************
*/

#ifndef THCAPTUREIMAGE_H
#define THCAPTUREIMAGE_H

#include "StaticSingleton.h"
#include "Thread.h"
#include "Loggable.h"
#include "Semaphore.h"
#include "OpCaptureImage.h"
#include "CaptureImagePayload.h"

class Operation;
class OpCaptureImage;

class THCaptureImage: public StaticSingleton<THCaptureImage>,
					public Thread,
					public Loggable
{
	private:

        vector<vector<unsigned char>> m_vData;

		Semaphore			m_synchSem;
		OpCaptureImage*		m_pOperationStop;
		OpCaptureImage*		m_pOperationExecute;
		structCameraParams	m_StructParameters;
		string				m_strAction;
		string				m_strWebUrl;

		int	 m_nPictureIdx;
		bool m_bEnableRoi;

	public:

		THCaptureImage();
		virtual ~THCaptureImage();
		int stopTakingPictures(OpCaptureImage* pOp);
		int perform(OpCaptureImage* pOp, structCameraParams* pParams);

	protected:

		int workerThread(void);
		void beforeWorkerThread(void);
		void afterWorkerThread(void);
		CaptureImagePayload* buildPayload(void);

	private:

		bool sendHttpPictures(void);
        bool performStateMachine(int liIdx);
		bool prepareToRequestedSettings(void);
		bool setLight(void);
		bool setFormat(void);
		bool setQuality(void);
		bool setCoordinates(void);
		bool customization(int liIll);
        int addBMPheader(const structInfoImage* pImage, int liIdx);
        void swapY(vector<unsigned char>& vImage, int liImageWidth, int liImageHeight, int liTimes = 0);
        void sendRejected(OpCaptureImage* pOp);
};



#endif // THCAPTUREIMAGE_H
