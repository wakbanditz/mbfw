/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THReadSec.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the THReadSec class.
 @details

 ****************************************************************************
*/

#include "THReadSec.h"
#include "OpReadSec.h"
#include "MainExecutor.h"
#include "OutCmdReadSec.h"
#include "WebServerAndProtocolInclude.h"

#include "CommonInclude.h"

stripReadSection_tag m_stripReadSection[SCT_NUM_TOT_SLOTS];


#define SEM_READSEC_SYNCH_NAME			"/sem-readsec-synch"
#define SEM_READSEC_SYNCH_INIT_VALUE		0

THReadSec::THReadSec()
{
	m_synchSem.create(SEM_READSEC_SYNCH_NAME, SEM_READSEC_SYNCH_INIT_VALUE);
	m_synchSem.enableErrorPrint(true);
	m_pOperation = 0;

	int i;

	for(i = 0; i < SCT_NUM_TOT_SLOTS; i++)
	{
		m_stripReadSection[i].m_bEnabled = false;
		m_stripReadSection[i].m_bStripCheck = false;
		m_stripReadSection[i].m_bSprCheck = false;
		m_stripReadSection[i].m_bSubstrateCheck = false;
//		m_stripReadSection[i].m_smpDetection = -1;
	}
	m_sectionRead = -1;
}

THReadSec::~THReadSec()
{

}

int THReadSec::perform(OpReadSec* pOp)
{
	if ( pOp == 0 )
	{
		log(LOG_ERR, "THReadSec::perform: trying to start with null operation");
		return eOperationError;
	}

	if ( m_pOperation != 0 )
	{
		log(LOG_ERR, "THReadSec::perform: BAD! trying to start with an already running operation");
		return eOperationError;
	}
	m_pOperation = pOp;

	int liRetVal =  m_synchSem.release();
	if ( liRetVal == SEM_FAILURE )
	{
		log(LOG_ERR, "THReadSec::perform: unable to unlock thread synch semaphore for op=<%p:%s>",
			pOp, pOp->getName().c_str());
		return eOperationError;
	}

	log(LOG_INFO, "THReadSec::perform: starting with operation <%p:%s>", pOp, pOp->getName().c_str());
	return eOperationRunning;
}

void THReadSec::beforeWorkerThread(void)
{
	log(LOG_INFO, "THReadSec::started");
}

int THReadSec::workerThread(void)
{
	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "THReadSec::th: unlocked but wait returned %d", liSynchRet);
		}
		if ( m_pOperation == 0 )
		{
			log(LOG_ERR, "THReadSec::th: BAD unlocked but wait returned %d", liSynchRet);
			continue;
		}
		log(LOG_INFO, "THReadSec::th: unlocked, starting with operation <%p:%s>",
			m_pOperation, m_pOperation->getName().c_str());


		memcpy(m_stripReadSection, m_pOperation->getDataStructure(), sizeof(stripReadSection_tag) * SCT_NUM_TOT_SLOTS);
		m_sectionRead = m_pOperation->getSection();

		// !!!! TODO set LEd of Section and Module

		// analyze the parameters and call accordingly the scheduler
		// -------------------------------------------------------------------------------------
		t_ErrorData ErrorData;
		ErrorData.iErrorCode = SCHED_SUCCESS;
		int iFailureCode = SCHED_ERROR;
		log(LOG_INFO, "MAIN: start scheduling two dummy events");
		schedulerThreadPointer()->problemInit(ErrorData);
		if ( ErrorData.iErrorCode != 0 )
		{
			schedulerThreadPointer()->closeAndAbort();
			log(LOG_ERR, "MAIN: Scheduler initialization error :%d", ErrorData.iErrorCode);
		}
		else
		{
			uint16_t usiSectionId = SCT_A_ID;
			struct precedence SLastPrec;
			SLastPrec = schedulerThreadPointer()->getLastPrec();
			iFailureCode = schedulerThreadPointer()->addDummyEvent((uint8_t)E_DUMMY_1,
																		 usiSectionId, SLastPrec.section_id,
																	  SLastPrec.slot_id, 0, ErrorData, false, true);
			if ( iFailureCode == SCHED_SUCCESS )
			{
				SLastPrec = schedulerThreadPointer()->getLastPrec();
				iFailureCode = schedulerThreadPointer()->addDummyEvent((uint8_t)E_DUMMY_2,
																			 usiSectionId, SLastPrec.section_id,
																			SLastPrec.slot_id, THReadSec_completedOperation,
																			ErrorData, false, true);
			}
			else
			{
				log(LOG_ERR, "MAIN: Scheduler dummy_1 scheduling error :%d", ErrorData.iErrorCode);
			}

			if ( iFailureCode == SCHED_SUCCESS )
			{
				schedulerThreadPointer()->closeAndRun(ErrorData);
			}
			else
			{
				log(LOG_ERR, "MAIN: Scheduler dummy_2 scheduling error :%d", ErrorData.iErrorCode);
			}

		}
		log(LOG_INFO, "READSEC: finished scheduling two dummy events");
		// ---------------------------------------------------------------------------------

		// wait for the scheduler to unlock the thread when actions completed
		liSynchRet = m_synchSem.wait();



		// set the status as SUCCESFULL so that the reply is composed with all the parameters
		OutCmdReadSec cmd;
		cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
		cmd.setUsage(m_pOperation->getUsage());
		cmd.setID(m_pOperation->getID());

		// !?! TODO build the payload

		/* ********************************************************************************************
		 * Build the OutgoingCommand to the client
		 * ********************************************************************************************
		 */
		std::string strOutCommand;
		bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);

		/* ********************************************************************************************
		 * Access the state machine
		 * ********************************************************************************************
		 */
		m_pOperation->restoreStateMachine();

		/* ********************************************************************************************
		 * Send the OutgoingCommand to the client
		 * ********************************************************************************************
		 */
		if ( ! bSerializeResult )
		{
			log(LOG_ERR, "THReadSec::th: unable to serialize OutCmdReadSec");
		}
		else
		{
			m_pOperation->sendCommandReply(strOutCommand);
		}

		/* ********************************************************************************************
		 * Remove the operation from the OperationManager list
		 * ********************************************************************************************
		 */
		operationSingleton()->removeOperation(m_pOperation);
		m_pOperation = 0;
		log(LOG_INFO, "THReadSec:th: done");
	}
	return 0;
}

void THReadSec::afterWorkerThread(void)
{
	m_synchSem.closeAndUnlink();
	log(LOG_INFO, "THReadSec::stopped");
}

int THReadSec::completedOperation(int section, int slot, int event)
{
	(void)section;
	(void)slot;
	(void)event;

	if ( m_pOperation == 0 )
	{
		log(LOG_ERR, "THReadSec::completed: BAD! trying to unlock with no running operation");
		return 1;
	}

	int liRetVal =  m_synchSem.release();
	if ( liRetVal == SEM_FAILURE )
	{
		log(LOG_ERR, "THReadSec::completed: unable to unlock thread synch semaphore for op=<%p:%s>",
			m_pOperation, m_pOperation->getName().c_str());
		return 2;
	}

	log(LOG_INFO, "THReadSec::completed: continue with operation <%p:%s>",
			m_pOperation, m_pOperation->getName().c_str());
	return 0;
}

int THReadSec_completedOperation(int section, int slot, int event)
{
	return THReadSec::getInstance()->completedOperation(section, slot, event);
}
