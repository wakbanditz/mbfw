/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ServiceCollector.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the ServiceCollector class.
 @details

 ****************************************************************************
*/

#ifndef SERVICECOLLECTOR_H
#define SERVICECOLLECTOR_H

#include "Loggable.h"

/*! *******************************************************************************************************************
 * @brief The ServiceCollector class	is just an organizer to spawn and close a set of threads that tipically
 *										are static singletons
 * ********************************************************************************************************************
 */
class ServiceCollector : public Loggable
{
	public:

		/*! ***********************************************************************************************************
		 * @brief ServiceCollector void constructor does nothing
		 * ************************************************************************************************************
		 */
		ServiceCollector();

		/*! ***********************************************************************************************************
		 * @brief ~ServiceCollector	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~ServiceCollector();

		/*! ***********************************************************************************************************
		 * @brief startAll		perform the spawning of a set of threads that are static singletons
		 * @return				true if all the threads have been correctly started, false if at least one has failed
		 *						to start
		 * ************************************************************************************************************
		 */
		bool startAll(void);

		/*! ***********************************************************************************************************
		 * @brief stopAll		perform the stop of a set of threads by calling their stopThread method
		 * ************************************************************************************************************
		 */
		void stopAll(void);
};

#endif // SERVICECOLLECTOR_H
