#ifndef THGETPICTURE_H
#define THGETPICTURE_H

#include "StaticSingleton.h"
#include "Thread.h"
#include "Loggable.h"
#include "Semaphore.h"
#include "OpGetPicture.h"
#include "GetPicturePayload.h"

class Operation;
class OpGetPicture;

class THGetPicture: public StaticSingleton<THGetPicture>,
					public Thread,
					public Loggable
{
	private:


		vector<vector<unsigned char>> m_vData;

		Semaphore			m_synchSem;
		OpGetPicture*		m_pOperationStop;
		OpGetPicture*		m_pOperationExecute;
		structParameters	m_StructParameters;
		string				m_strWebUrl;

		int	 m_nPictureIdx;
		bool m_bHaveSameQuality;

	public:

		THGetPicture();

		virtual ~THGetPicture();

		int stopTakingPictures(OpGetPicture* pOp);

		int perform(OpGetPicture* pOp, structParameters* pParams);

	protected:

		int workerThread(void);

		void beforeWorkerThread(void);

		void afterWorkerThread(void);

		GetPicturePayload* buildPayload(void);

	private:

		void buildPayloadAndReply();

		bool sendHttpPictures();
};



#endif // THGETPICTURE_H
