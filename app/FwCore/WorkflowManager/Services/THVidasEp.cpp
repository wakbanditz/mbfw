/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THVidasEp.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the THVidasEp class.
 @details

 ****************************************************************************
*/

#include "THVidasEp.h"
#include "MainExecutor.h"
#include "OpVidasEp.h"

#include "CommonInclude.h"

#define SEM_VIDASEP_SYNCH_NAME			"/sem-vidasep-synch"
#define SEM_VIDASEP_SYNCH_INIT_VALUE		0

THVidasEp::THVidasEp()
{
	m_synchSem.create(SEM_VIDASEP_SYNCH_NAME, SEM_VIDASEP_SYNCH_INIT_VALUE);
	m_synchSem.enableErrorPrint(true);

	m_cntThreadActive = 0;

	m_strUsage.clear();
	m_strWebUrl.clear();
	m_strId.clear();
}

THVidasEp::~THVidasEp()
{

}

int THVidasEp::sendHttpData(std::string strUsage, std::string strWebUrl, std::string strId)
{
	// store if the reply has to be sent to a specific address (coming from an external command)
	m_strUsage.assign(strUsage);
	m_strWebUrl.assign(strWebUrl);
	m_strId.assign(strId);

	// if requested and necessary (the thread could be already running due to the other request) -> unlock it
	if(m_cntThreadActive == 0)
	{
		// increase the counter of request
		m_cntThreadActive++;
		log(LOG_INFO, "THVidasEp::perform: starting %d %s", m_cntThreadActive, m_strUsage.c_str());

		int liRetVal =  m_synchSem.release();
		if ( liRetVal == SEM_FAILURE )
		{
			m_strUsage.clear();
			m_strWebUrl.clear();
			m_strId.clear();
			m_cntThreadActive = 0;
			log(LOG_ERR, "THVidasEp::perform: unable to unlock thread synch semaphore");
			return 1;
		}
		return 0;
	}

	// increase the counter of request
	m_cntThreadActive++;
	log(LOG_INFO, "THVidasEp::perform: updating %d %s", m_cntThreadActive, m_strUsage.c_str());

	return 0;
}

void THVidasEp::beforeWorkerThread(void)
{
	log(LOG_INFO, "THVidasEp::started");
}

int THVidasEp::workerThread(void)
{
	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "THVidasEp::th: unlocked but wait returned %d", liSynchRet);
		}

		if(m_cntThreadActive == 0)
		{
			log(LOG_WARNING, "THVidasEp::th: unlocked but no request active");
			continue;
		}

		log(LOG_INFO, "THVidasEp::th: unlocked");

		while(m_cntThreadActive != 0)
		{
			// create a "fake" operation to compose message and send it back

			if(m_strUsage.empty() == false && m_strWebUrl.empty() == false)
			{
				OpVidasEp * pOperation = new OpVidasEp;
				pOperation->setLogger(MainExecutor::getInstance()->getLogger());
				pOperation->setUsage(m_strUsage);
				pOperation->setWebResponseUrl(m_strWebUrl);
				pOperation->setID(m_strId);
				pOperation->compileSendOutCmd();
				SAFE_DELETE(pOperation);
			}
			else
			{
				// in this case the message is requested by the instrument itself: send it
				// to all connection active
				vector<pair<string, string>>  loginList;

				MainExecutor::getInstance()->m_loginManager.getLoginActiveList(loginList);

				// first check if there are some open connection and if there are listening servers
				for (size_t i = 0; i < loginList.size(); i++)
				{
					string strUsage;
					strUsage.assign(loginList.at(i).first);

					string strWebUrl;
					strWebUrl.assign(loginList.at(i).second);

                    m_pLogger->log(LOG_INFO,"THVidasEp::sendHttpData--> Send to %s %s connection",
								   strWebUrl.c_str(),
								   strUsage.c_str());

					OpVidasEp * pOperation = new OpVidasEp;
					pOperation->setLogger(MainExecutor::getInstance()->getLogger());
					pOperation->setUsage(strUsage);
					pOperation->setWebResponseUrl(strWebUrl);
					pOperation->setID("");
					pOperation->compileSendOutCmd();
					SAFE_DELETE(pOperation);
				}
			}

			m_cntThreadActive--;
		}

		log(LOG_INFO, "THVidasEp:th: done");
	}
	return 0;
}

void THVidasEp::afterWorkerThread(void)
{
	m_synchSem.closeAndUnlink();
	log(LOG_INFO, "THVidasEp::stopped");
}

