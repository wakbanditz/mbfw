/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THCaptureImage.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the THCaptureImage class.
 @details

 ****************************************************************************
*/

#include "THCaptureImage.h"
#include "MainExecutor.h"
#include "WebServerAndProtocolInclude.h"
#include "CommonInclude.h"
#include "OutCmdCaptureImage.h"

#define SEM_GETPIC_SYNCH_NAME			"/sem-getPic-synch"
#define SEM_GETPIC_SYNCH_INIT_VALUE		0

#define PICTURE_FORMAT_JPEG		"JPEG"
#define PICTURE_FORMAT_BMP		"RAW"
#define PICTURE_FOCUS_NONE		"NONE"
#define PICTURE_FOCUS_SAMPLE	"DEFAULT"
#define PICTURE_FOCUS_PATTERN	"TRAY"

#define PICTURE_CUSTOM_ILL_THR	20
#define PICTURE_ILL_STEP_SIZE	5
#define PICTURE_DEFAULT_GAIN	19
#define PICTURE_DEFAULT_EXP		20000

#define PICTURE_ERR_SIZE		40000
#define PICTURE_ERR_HEIGHT		2000
#define PICTURE_ERR_WIDTH		2000



THCaptureImage::THCaptureImage()
{
	m_synchSem.create(SEM_GETPIC_SYNCH_NAME, SEM_GETPIC_SYNCH_INIT_VALUE);
	m_synchSem.enableErrorPrint(true);
	m_pOperationStop = nullptr;
    m_pOperationExecute = nullptr;
	m_nPictureIdx = 0;
	m_bEnableRoi = false;
	m_strWebUrl.clear();
}

THCaptureImage::~THCaptureImage()
{

}

int THCaptureImage::stopTakingPictures(OpCaptureImage* pOp)
{
	if ( pOp == 0 )
	{
		log(LOG_ERR, "THCaptureImage::stopTakingPictures: trying to start with null operation");
		return eOperationError;
	}

	if ( m_pOperationStop != 0)
	{
		log(LOG_ERR, "THCaptureImage::stopTakingPictures: BAD! trying to start with a running operation");
		return eOperationError;
	}

	if ( pOp->getAction().compare(STOP_TAKING_PICTURES) )
	{
		// the command is not congruent
		return eOperationError;
	}

	m_pOperationStop = pOp;
	m_strAction = pOp->getAction();

	return eOperationRunning;
}

int THCaptureImage::perform(OpCaptureImage* pOp, structCameraParams* pParams)
{
	if ( pOp == 0 )
	{
		log(LOG_ERR, "THCaptureImage::perform: trying to start with null operation");
		return eOperationError;
	}
	if ( pParams == 0 )
	{
		log(LOG_ERR, "THCaptureImage::perform: trying to start with null parameters");
		return eOperationError;
	}
	if ( m_pOperationExecute != 0 )
	{
		log(LOG_ERR, "THCaptureImage::perform: BAD! trying to start with an already running operation");
		return eOperationError;
	}

	m_pOperationExecute = pOp;
    m_strWebUrl.assign(m_pOperationExecute->getWebUrl());
	m_strAction = pOp->getAction();

	// copy the parameters to the struct member
	m_StructParameters = *pParams;
	m_nPictureIdx = 0;

	int liRetVal =  m_synchSem.release();
	if ( liRetVal == SEM_FAILURE )
	{
		log(LOG_ERR, "THCaptureImage::perform: unable to unlock thread synch semaphore for op=<%p:%s>", pOp, pOp->getName().c_str());
		return eOperationError;
	}

	log(LOG_INFO, "THCaptureImage::perform: starting with operation <%p:%s>", pOp, pOp->getName().c_str());
	return eOperationRunning;
}

int THCaptureImage::workerThread()
{
    while ( isRunning() )
    {
        int liSynchRet = m_synchSem.wait();
        if( liSynchRet != SEM_SUCCESS )
        {
            log(LOG_WARNING, "THCaptureImage::th: unlocked but wait returned %d", liSynchRet);
        }

        bool bLoopIsActive = true;

        /*!************************************ PREPARE THE CAMERA TO REQUESTED SETTINGS *************************************************/
		if ( m_StructParameters.vPredefinedRoi.front().empty() )
        {
            bool bRes = prepareToRequestedSettings();
            if ( ! bRes )
            {
                sendRejected(m_pOperationExecute);
                bLoopIsActive = false;
            }
        }
        else if ( m_StructParameters.liPicturesNum == 1 )
        {
            string strTarget = m_StructParameters.vPredefinedRoi.front();
            int liRes = usbBoardLinkSingleton()->m_pCR8062->uploadCalibrBatchFile(strTarget);
            if ( liRes )
            {
                sendRejected(m_pOperationExecute);
                bLoopIsActive = false;
            }
			m_StructParameters.vFormat.front() = (usbCameraSingleton()->getImageFormat() == eRAW) ? "RAW" : "JPEG";
        }
        /*!*****************************************************************************************************************************/

        while ( bLoopIsActive )
        {
            if ( ! m_strAction.compare("STOP") )
			{
                int liRes = usbCameraSingleton()->restoreDefaultAfterVideo();
                if ( liRes != ERR_NONE )
                {
                    log(LOG_ERR, "THCaptureImage::perform: unable to restore default params after video mode");
					sendRejected(m_pOperationStop);
                }
				else
				{
					m_nPictureIdx++;
					sendHttpPictures();
					m_pOperationStop->restoreStateMachine();	// Remove stop op
				}

				// Stop the execution immediately
				bLoopIsActive = false;
				m_pOperationExecute->restoreStateMachine();	// Remove exec op
                break;
            }

            // Start the loop
            m_vData.clear();
            m_vData.resize(m_StructParameters.liPicturesNum);
            bool bRes = true;
            for ( int i = 0; i != m_StructParameters.liPicturesNum; i++ )
            {
                bRes = performStateMachine(i);
                if ( !bRes )
                {
                    bLoopIsActive = false;
                    sendRejected(m_pOperationExecute);
                    break;
                }
            }
            if ( ! bRes )   break;

            /*! *******************************************************************
            I have the data of the 2 pictures now, I need to packet and send them.
            ******************************************************************** **/
            m_nPictureIdx++;
            sendHttpPictures();

            if ( ! m_strAction.compare("ONESHOT") )
            {
                // Stop the execution after one shot
                bLoopIsActive = false;
                break;
            }
        }

        /* ********************************************************************************************
         * Access the state machine
         * ********************************************************************************************
         */
		if ( m_pOperationExecute != nullptr )
		{
			operationSingleton()->removeOperation(m_pOperationExecute); // remove exec op
		}
		if ( m_pOperationStop != nullptr )
		{
			operationSingleton()->removeOperation(m_pOperationStop); // remove stop op
		}

        m_pOperationStop = nullptr;
        m_pOperationExecute = nullptr;

        log(LOG_INFO, "THCaptureImage:th: done");
    }

    return 0;
}

void THCaptureImage::beforeWorkerThread()
{
	log(LOG_INFO, "THCaptureImage::started");
}

void THCaptureImage::afterWorkerThread()
{
	m_synchSem.closeAndUnlink();
	log(LOG_INFO, "THCaptureImage::stopped");
}

bool THCaptureImage::sendHttpPictures()
{
    string strUsage;

    strUsage.assign(WEB_SERVICE_CONNECTION);

    Operation* pOperation = new Operation("OpGetPicture");
    pOperation->setUsage(strUsage);
    pOperation->setWebResponseUrl(m_strWebUrl);
    pOperation->setLogger(MainExecutor::getInstance()->getLogger());

    // set the status as SUCCESFULL so that the reply is composed with all the parameters
    OutCmdCaptureImage cmd;
    cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
    cmd.setUsage(m_pOperationExecute->getUsage());
    cmd.setID(m_pOperationExecute->getID());

	CaptureImagePayload* pCaptureImagePayload = new CaptureImagePayload();
	pCaptureImagePayload->setGetPictureAttr(m_nPictureIdx, m_StructParameters, m_vData);

    // Assign the payload to the outcommand to let the serializer will extract the info
    cmd.setPayload((Payload*)pCaptureImagePayload);

    /* ********************************************************************************************
     * Send the OutgoingCommand to the client
     * ********************************************************************************************
     */
    string strOutCommand;
    bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
    if ( ! bSerializeResult )
    {
        log(LOG_ERR, "THReadSec::th: unable to serialize OutCmdReadSec");
    }
    else
    {
        pOperation->sendCommandReply(strOutCommand);
    }

    SAFE_DELETE(pOperation);
    return bSerializeResult;
}

bool THCaptureImage::performStateMachine(int liIdx)
{
	enum class eOperation { eStart, eCaptureImage, eRecover, eEnd};
    eOperation eState = eOperation::eStart;
	structInfoImage InfoImage;

    while ( eState != eOperation::eEnd )
    {
        switch(eState)
        {
			case eOperation::eStart:
					eState = eOperation::eCaptureImage;
            break;

            case eOperation::eCaptureImage:
			{
                int liRes = usbCameraSingleton()->getPicture(m_vData[liIdx], &InfoImage);
                if ( liRes != ERR_NONE )
                {
                    log(LOG_ERR, "THCaptureImage::performStateMachine: unable to get picture");
                    eState = eOperation::eRecover;
					break;
				}

				if ( m_bEnableRoi )
				{
					usbCameraSingleton()->showROI(m_vData[liIdx], &InfoImage);
				}
                // add only if image is raw
                addBMPheader(&InfoImage, liIdx);
                eState = eOperation::eEnd;
            }
            break;

            case eOperation::eRecover:
            {
				m_pLogger->log(LOG_WARNING, "THCaptureImage::performStateMachine: trying to recover");
				bool bRes = true;
				for ( int i = 0; i != 2; ++i )
				{
					bRes = usbBoardLinkSingleton()->m_pCR8062->bringToLife();
					if ( bRes )		break;
					msleep(4000);
				}

				if ( bRes )
				{
					m_pLogger->log(LOG_WARNING, "THCaptureImage::performStateMachine: first recover step performed");
				}
				else
				{
					m_pLogger->log(LOG_ERR, "THCaptureImage::performStateMachine: unable to recover");
					break;
				}

                usbCameraSingleton()->sendCmd("RDFWGVS");
				msleep(100);
                int liBytesReceived;
                do
                {
                    // clean buffer
					liBytesReceived = usbCameraSingleton()->readMsgReceived(50);
                }
                while ( liBytesReceived > 0 );
				log(LOG_WARNING, "THCaptureImage::performStateMachine: recover action fully performed");

				m_vData[liIdx].clear();
				m_vData[liIdx].resize(PICTURE_ERR_SIZE);
				for ( int i = 0; i != PICTURE_ERR_SIZE; i+=2 )
				{
					m_vData[liIdx].at(i) = 0;
					m_vData[liIdx].at(i+1) = 255;
				}
				structInfoImage info = {PICTURE_ERR_WIDTH, PICTURE_ERR_HEIGHT, PICTURE_ERR_SIZE, eRAW};
				addBMPheader(&info, liIdx);
				eState = eOperation::eEnd;
            }
            break;

            default:
                eState = eOperation::eEnd;
            break;
        }
    }

    return true;
}

bool THCaptureImage::prepareToRequestedSettings()
{
    /** ***********************************************************************************************************
    Fill light field if empty with default values.
    ************************************************************************************************************ **/
    bool bRes = setLight();
    if ( ! bRes )
    {
        log(LOG_ERR, "THCaptureImage::prepareToRequestedSettings: unable to set light");
        return false;
    }

    /** ***********************************************************************************************************
    If video then get ready and start.
    ************************************************************************************************************ **/

    if ( ! m_strAction.compare("START") )
    {
		int liQuality = ( m_StructParameters.vQuality.front() == -1 ) ? 30 : m_StructParameters.vQuality.front();
		int liRes = usbCameraSingleton()->getReady4Videos(liQuality);
        return (liRes == ERR_NONE) ? true : false;
    }

    /*! ***********************************************************************************************************
    Take care of picture mode - Set image format if not already in that mode.
    ************************************************************************************************************ **/
    bRes = setFormat();
    if ( ! bRes )
    {
        log(LOG_ERR, "THCaptureImage::prepareToRequestedSettings: unable to set the camera in JPEG Mode");
        return false;
    }

    /** ***********************************************************************************************************
    Fill quality or focus field if empty with default values and check.
    ************************************************************************************************************ **/
    bRes = setQuality();
    if ( ! bRes )
    {
        log(LOG_ERR, "THCaptureImage::prepareToRequestedSettings: unable to set quality");
        return false;
    }

    bRes = setCoordinates();
    if ( ! bRes )
    {
        log(LOG_ERR, "THCaptureImage::prepareToRequestedSettings: unable to set coordinates");
        return false;
    }

    return true;
}

bool THCaptureImage::setLight()
{
    int liRes = ERR_NONE;
	m_bEnableRoi = false;

	int liIll = m_StructParameters.vLight.front();
	if ( liIll == -1 )						liIll = 100;
	if ( liIll < PICTURE_CUSTOM_ILL_THR )	customization(liIll);

	liRes = usbCameraSingleton()->setAGCparam(liIll, PICTURE_DEFAULT_EXP, PICTURE_DEFAULT_GAIN);
    return ( liRes == ERR_NONE );
}

bool THCaptureImage::setFormat()
{
    int liRes = ERR_NONE;

    eImageFormat eFormat = usbCameraSingleton()->getImageFormat();

    if ( m_StructParameters.vFormat.empty() )
    {
        if ( eFormat != eJPEG )
        {
            liRes = usbCameraSingleton()->setImageMode(eJPEG);
        }
        m_StructParameters.vFormat.front() = PICTURE_FORMAT_JPEG;
    }
    else if ( ! m_StructParameters.vFormat.front().compare(PICTURE_FORMAT_BMP) )
    {
        if ( eFormat != eRAW )
        {
            liRes = usbCameraSingleton()->setImageMode(eRAW);
        }
    }
    else
    {
        if ( eFormat != eJPEG )
        {
            liRes = usbCameraSingleton()->setImageMode(eJPEG);
        }
    }

    return ( liRes == ERR_NONE );
}

bool THCaptureImage::setQuality()
{
	int liRes = ERR_NONE;

	if ( usbCameraSingleton()->getImageFormat() == eJPEG )
	{
		int liQual = m_StructParameters.vQuality.front();
		if ( liQual == -1 )						liQual = 100;
		if ( liQual < PICTURE_CUSTOM_ILL_THR )	customization(liQual);

		liRes = usbCameraSingleton()->setJPEGQuality(m_StructParameters.vQuality.front());
	}

	return ( liRes == ERR_NONE );
}

bool THCaptureImage::setCoordinates()
{
	int liRes = ERR_NONE;

    if ( m_StructParameters.vrgCoordinates.empty() ) // default
    {
        for ( int i = 0; i < m_StructParameters.liPicturesNum; i++ )
        {
            m_StructParameters.vrgCoordinates.resize(m_StructParameters.liPicturesNum);
            m_StructParameters.vrgCoordinates[i].liTop = 0;
            m_StructParameters.vrgCoordinates[i].liBottom = MAX_HEIGHT;
            m_StructParameters.vrgCoordinates[i].liLeft = 0;
            m_StructParameters.vrgCoordinates[i].liRight = MAX_WIDTH;
		}
        liRes = usbCameraSingleton()->disableCrop();
    }
	else
	{
        uint16_t x0 = m_StructParameters.vrgCoordinates.front().liLeft;
        uint16_t y0 = m_StructParameters.vrgCoordinates.front().liTop;
        uint16_t usiWidth = m_StructParameters.vrgCoordinates.front().liRight - x0;
        uint16_t usiHeight = m_StructParameters.vrgCoordinates.front().liBottom - y0;;
        liRes = usbCameraSingleton()->setCropCoordinates(x0, y0, usiWidth, usiHeight);
    }

	return ( liRes == ERR_NONE );
}

bool THCaptureImage::customization(int liIll)
{
	if ( liIll >= PICTURE_CUSTOM_ILL_THR )	return false;

	int liStepNum = liIll / PICTURE_ILL_STEP_SIZE;

	switch (liStepNum)
	{
		case 0:
			m_bEnableRoi = true; // TODO SET ILLUMINATION!!!!
		break;

		case 1:
		case 2:
		case 3:
		break;

		default:
			return false;
		break;
	}

	return true;
}

int THCaptureImage::addBMPheader(const structInfoImage* pImage, int liIdx)
{
	if ( usbCameraSingleton()->getImageFormat() != eRAW )	return -1;
	if ( pImage == nullptr )								return -1;
	if ( liIdx > int(m_vData.size()-1) )					return -1;

    /* ************************************************************************
     * Raw picture from the camera is:     What bmp image has to be like:
     *      0------------->                     ------------->n
     *      -------------->                     -------------->
     *      -------------->n                    0------------->
     * For this reason y-axis have to be inverted
     * ************************************************************************/
    swapY(m_vData[liIdx], pImage->liWidth, pImage->liHeight);

	// Take care of padding
	unsigned int uliBufLength = 0;
	int padBytes = 0;
	if ( pImage->liWidth % 4 != 0 )
	{
		padBytes = 4 - ( pImage->liWidth % 4 );
		uliBufLength = pImage->liHeight * (pImage->liWidth + padBytes);
	}

	unsigned char rgcBuff[BMP_HEADER_SIZE + PALETTE_LEN];
	BmpHeader sBmpHeader;
	BmpInfoHeader sBmpInfoHeader;

	sBmpHeader.magic[0] = MAGIC_MSB;
	sBmpHeader.magic[1] = MAGIC_LSB;
	sBmpHeader.creator1 = CREATOR1;
	sBmpHeader.creator2 = CREATOR2;
	sBmpHeader.bmp_offset = sizeof(sBmpHeader) + sizeof(sBmpInfoHeader) + PALETTE_LEN;
	sBmpHeader.filesz = sBmpHeader.bmp_offset + uliBufLength;
	sBmpInfoHeader.header_sz = sizeof(sBmpInfoHeader);
	sBmpInfoHeader.width = pImage->liWidth;
	sBmpInfoHeader.height = pImage->liHeight;
	sBmpInfoHeader.nplanes = 1;
	sBmpInfoHeader.bitspp = 8;
	sBmpInfoHeader.compress_type = 0;
	sBmpInfoHeader.bmp_bytesz = uliBufLength;
	sBmpInfoHeader.hres = HRES;
	sBmpInfoHeader.vres = VRES;
	sBmpInfoHeader.ncolors = 0;
	sBmpInfoHeader.nimpcolors = 0;

	memcpy(rgcBuff, &sBmpHeader, sizeof(sBmpHeader));
	memcpy(&rgcBuff[14], &sBmpInfoHeader, sizeof(sBmpInfoHeader));

	uint32_t uliValPalette = 0;
	uint32_t uliOffset = BMP_HEADER_SIZE;
	for ( int idx = 0; idx < PALETTE_NUM; ++idx )
	{
		uliValPalette = (idx << 16) + (idx << 8) + idx;
		memcpy(rgcBuff+uliOffset, &uliValPalette, sizeof(uint32_t));
		uliOffset += sizeof(uint32_t);
	}

	// add padding
	if ( padBytes != 0 )
	{
		vector<unsigned char> vPadding(padBytes);
		for ( int i = 0; i < pImage->liHeight; ++i )
		{
			auto idx = m_vData[liIdx].begin() + i*(pImage->liWidth+padBytes) + pImage->liWidth;
			m_vData[liIdx].insert(idx, vPadding.begin(), vPadding.end());
		}
	}
	m_vData[liIdx].insert(m_vData[liIdx].begin(), rgcBuff, rgcBuff+BMP_HEADER_SIZE+PALETTE_LEN);

    return 0;
}

// tail recursion, no stack allocated
void THCaptureImage::swapY(vector<unsigned char>& vImage, int liImageWidth, int liImageHeight, int liTimes)
{
    if ( liTimes >= liImageHeight/2)    return;

    auto begin = vImage.begin() + liTimes * liImageWidth;
    auto end = begin + liImageWidth;
    auto beginLast = vImage.end() - liImageWidth*(liTimes+1);
    swap_ranges(begin, end, beginLast);

    return swapY(vImage, liImageWidth, liImageHeight, ++liTimes);
}

void THCaptureImage::sendRejected(OpCaptureImage *pOp)
{
    string strUsage;
    strUsage.assign(WEB_SERVICE_CONNECTION);

    pOp->setUsage(strUsage);
    pOp->setWebResponseUrl(m_strWebUrl);
    pOp->setLogger(MainExecutor::getInstance()->getLogger());

    // set the status as SUCCESFULL so that the reply is composed with all the parameters
    OutCmdCaptureImage cmd;
    cmd.setStatus(XML_ATTR_STATUS_REJECTED);
    cmd.setUsage(m_pOperationExecute->getUsage());
    cmd.setID(m_pOperationExecute->getID());

    /* ********************************************************************************************
     * Send the OutgoingCommand to the client
     * ********************************************************************************************
     */
    string strOutCommand;
    bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
    if ( ! bSerializeResult )
    {
        log(LOG_ERR, "THReadSec::th: unable to serialize OutCmdReadSec");
    }
    else
    {
        pOp->sendCommandReply(strOutCommand);
    }

	// clear state machine
	pOp->restoreStateMachine();

    return;
}
