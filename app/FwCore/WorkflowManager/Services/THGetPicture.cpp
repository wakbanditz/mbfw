#include "THGetPicture.h"
#include "MainExecutor.h"
#include "OutCmdGetPicture.h"
#include "WebServerAndProtocolInclude.h"
#include "GetPicturePayload.h"
#include "CommonInclude.h"

#define SEM_GETPIC_SYNCH_NAME			"/sem-getPic-synch"
#define SEM_GETPIC_SYNCH_INIT_VALUE		0

#define TEST

THGetPicture::THGetPicture()
{
	m_synchSem.create(SEM_GETPIC_SYNCH_NAME, SEM_GETPIC_SYNCH_INIT_VALUE);
	m_synchSem.enableErrorPrint(true);
	m_pOperationStop = nullptr;
	m_pOperationExecute = nullptr;
	m_nPictureIdx = 0;
	m_strWebUrl.clear();
}

THGetPicture::~THGetPicture()
{

}

int THGetPicture::stopTakingPictures(OpGetPicture* pOp)
{
	if ( pOp == 0 )
	{
		log(LOG_ERR, "THGetPicture::stopTakingPictures: trying to start with null operation");
		return eOperationError;
	}

	if ( m_pOperationStop != 0)
	{
		log(LOG_ERR, "THGetPicture::stopTakingPictures: BAD! trying to start with a running operation");
		return eOperationError;
	}

	if ( pOp->getAction().compare(STOP_TAKING_PICTURES) )
	{
		// the command is not congruent
		return eOperationError;
	}

	m_pOperationStop = pOp;
	m_StructParameters.strAction = pOp->getAction();

	return eOperationRunning;
}

#ifdef TEST

int THGetPicture::perform(OpGetPicture* pOp, structParameters* pParams)
{
	if ( pOp == 0 )
	{
		log(LOG_ERR, "THGetPicture::perform: trying to start with null operation");
		return eOperationError;
	}
	if ( pParams == 0 )
	{
		log(LOG_ERR, "THGetPicture::perform: trying to start with null parameters");
		return eOperationError;
	}
//	if ( m_bLoopIsActive == true )
//	{
//		log(LOG_ERR, "THGetPicture::perform: BAD! trying to start with an already running operation");
//		return eOperationError;
//	}
	if ( m_pOperationExecute != 0 )
	{
		log(LOG_ERR, "THGetPicture::perform: BAD! trying to start with an already running operation");
		return eOperationError;
	}

	m_pOperationExecute = pOp;
	m_pOperationExecute->getWebResponseUrl(m_strWebUrl);

	// copy the parameters to the struct member
	m_StructParameters.rgImageQuality[0] = pParams->rgImageQuality[0];
	m_StructParameters.rgImageQuality[1] = pParams->rgImageQuality[1];
	m_StructParameters.liImagesNum = pParams->liImagesNum;
	m_StructParameters.strAction = pParams->strAction;
	m_StructParameters.StructCoordinates[0] = pParams->StructCoordinates[0];
	m_StructParameters.StructCoordinates[1] = pParams->StructCoordinates[1];

	m_nPictureIdx = 0;

	/*!************************************ PREPARE THE CAMERA TO OPTIMAL SETTINGS *************************************************/

	// Set JPEG image format
	int8_t cRes = MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setJPEGMode();
	if ( cRes != ERR_NONE )
	{
		log(LOG_ERR, "THGetPicture::perform: unable to set the camera in JPEG Mode");
		return eOperationError;
	}

	cRes = MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.disableCrop();
	if ( cRes != ERR_NONE )
	{
		log(LOG_ERR, "THGetPicture::perform: unable to disable crop");
		return eOperationError;
	}

	cRes = MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setJPEGQuality(m_StructParameters.rgImageQuality[0]);
	if ( cRes != ERR_NONE )
	{
		log(LOG_ERR, "THGetPicture::perform: unable to set JPEG Quality");
		return eOperationError;
	}
	m_bHaveSameQuality = true;

	/*!*****************************************************************************************************************************/

	int liRetVal =  m_synchSem.release();
	if ( liRetVal == SEM_FAILURE )
	{
		log(LOG_ERR, "THGetPicture::perform: unable to unlock thread synch semaphore for op=<%p:%s>", pOp, pOp->getName().c_str());
		return eOperationError;
	}

	log(LOG_INFO, "THGetPicture::perform: starting with operation <%p:%s>", pOp, pOp->getName().c_str());
	return eOperationRunning;
}

#else

int THGetPicture::perform(OpGetPicture* pOp, structParameters* pParams)
{
	if ( pOp == 0 )
	{
		log(LOG_ERR, "THGetPicture::perform: trying to start with null operation");
		return eOperationError;
	}
	if ( pParams == 0 )
	{
		log(LOG_ERR, "THGetPicture::perform: trying to start with null parameters");
		return eOperationError;
	}
	if ( m_pOperationExecute != 0 )
	{
		log(LOG_ERR, "THGetPicture::perform: BAD! trying to start with an already running operation");
		return eOperationError;
	}

	m_pOperationExecute = pOp;
	m_pOperationExecute->getWebResponseUrl(m_strWebUrl);

	// copy the parameters to the struct member
	m_StructParameters.rgImageQuality[0] = pParams->rgImageQuality[0];
	m_StructParameters.rgImageQuality[1] = pParams->rgImageQuality[1];
	m_StructParameters.liImagesNum = pParams->liImagesNum;
	m_StructParameters.strAction = pParams->strAction;
	m_StructParameters.StructCoordinates[0] = pParams->StructCoordinates[0];
	m_StructParameters.StructCoordinates[1] = pParams->StructCoordinates[1];

	m_nPictureIdx = 0;

	/*!************************************ PREPARE THE CAMERA TO OPTIMAL SETTINGS *************************************************/

	// Set JPEG image format
	int8_t cRes = MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setJPEGMode();
	if ( cRes != ERR_NONE )
	{
		log(LOG_ERR, "THGetPicture::perform: unable to set the camera in JPEG Mode");
		return eOperationError;
	}

	// Set general info - crop coordinates and quality
	if ( m_StructParameters.liImagesNum == ONLY_ONE_IMAGE )
	{
		/*! ***********************************************************************************************************
		If we are dealing with only one image there is no need to sent the crop coordinates every time.
		************************************************************************************************************ **/
		uint16_t x0 = m_StructParameters.StructCoordinates[0].liLeft;
		uint16_t y0 = m_StructParameters.StructCoordinates[0].liBottom;
		uint16_t usiWidth = m_StructParameters.StructCoordinates[0].liRight - m_StructParameters.StructCoordinates[0].liLeft;
		uint16_t usiHeight = m_StructParameters.StructCoordinates[0].liBottom - m_StructParameters.StructCoordinates[0].liTop;;
		cRes = MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setCropCoordinates(x0, y0, usiWidth, usiHeight);

		if ( cRes != ERR_NONE )
		{
			log(LOG_ERR, "THGetPicture::perform: unable to set cropCoordinates");
			return eOperationError;
		}
	}
	if ( (m_StructParameters.liImagesNum == ONLY_ONE_IMAGE) || (m_StructParameters.rgImageQuality[0] == m_StructParameters.rgImageQuality[1]) )
	{
		/*! ***********************************************************************************************************
		If we are dealing with only one image or two images with the same quality there is no need to sent the crop
		coordinates every time.
		************************************************************************************************************ **/

		cRes = MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setJPEGQuality(m_StructParameters.rgImageQuality[0]);
		if ( cRes != ERR_NONE )
		{
			log(LOG_ERR, "THGetPicture::perform: unable to set cropCoordinates");
			return eOperationError;
		}
		m_bHaveSameQuality = true;
	}

	/*!*****************************************************************************************************************************/

	int liRetVal =  m_synchSem.release();
	if ( liRetVal == SEM_FAILURE )
	{
		log(LOG_ERR, "THGetPicture::perform: unable to unlock thread synch semaphore for op=<%p:%s>", pOp, pOp->getName().c_str());
		return eOperationError;
	}

	log(LOG_INFO, "THGetPicture::perform: starting with operation <%p:%s>", pOp, pOp->getName().c_str());
	return eOperationRunning;
}

#endif

#ifdef TEST
int THGetPicture::workerThread()
{
	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "THGetPicture::th: unlocked but wait returned %d", liSynchRet);
		}

		m_pOperationExecute->restoreStateMachine();
		bool bLoopIsActive = true;
		bool bChange = false;

		while ( bLoopIsActive )
		{
			// Start the loop
			m_vData.clear();
			m_vData.resize(m_StructParameters.liImagesNum);
			MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.getPicture(m_vData[0]);

			// Add second image
			if ( m_vData.size() > 1 )
			{
				string strFileName = ( bChange ) ? "2.jpg" : "4.jpg";
				bChange = !bChange;
				struct stat statbuf1;
				if ( stat(strFileName.c_str(), &statbuf1) == -1)	return -1;
				int liFileSize = statbuf1.st_size;

				ifstream file;
				file.open(strFileName, ios::in | ios::binary);
				if ( ! file.good() )
				{
					log(LOG_ERR, "Unable to open file %s", strFileName.c_str());
					return false;
				}

				m_vData[1].resize(liFileSize);
				// read data from the file until EOF is reached
				while ( file.read((char*)m_vData[1].data(), liFileSize) ) {};
				file.close();
			}

			/*! *******************************************************************
			I have the data of the 2 pictures now, I need to packet and send them.
			******************************************************************** **/
			m_nPictureIdx++;
			sendHttpPictures();

			if ( ! m_StructParameters.strAction.compare("ONESHOT") ||  ! m_StructParameters.strAction.compare("STOP") )
			{				
				m_pOperationStop->restoreStateMachine(); // Remove stop op

				// Reply - this only in case if STOP, not one shot, to be modified
				OutCmdGetPicture cmd;
				cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
				cmd.setUsage(m_pOperationStop->getUsage());
				cmd.setID(m_pOperationStop->getID());
				string strOutCommand;
				protocolSerialize(&cmd, strOutCommand);
				m_pOperationStop->sendCommandReply(strOutCommand);
				bLoopIsActive = false;
			}
		}

		operationSingleton()->removeOperation(m_pOperationExecute); // remove exec op
		operationSingleton()->removeOperation(m_pOperationStop); // remove stop op

		m_pOperationStop = nullptr;
		m_pOperationExecute = nullptr;

		log(LOG_INFO, "THGetPicture:th: done");
	}
	return 0;
}

#else
int THGetPicture::workerThread()
{
	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "THGetPicture::th: unlocked but wait returned %d", liSynchRet);
		}

		m_pOperationExecute->restoreStateMachine();
		bool bLoopIsActive = true;

		while ( bLoopIsActive )
		{
			if ( ! m_StructParameters.strAction.compare("STOP") )
			{
				m_pOperationStop->restoreStateMachine(); // Remove stop op

				// Reply - this only in case if STOP, not one shot, to be modified
				OutCmdGetPicture cmd;
				cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
				cmd.setUsage(m_pOperationStop->getUsage());
				cmd.setID(m_pOperationStop->getID());
				string strOutCommand;
				protocolSerialize(&cmd, strOutCommand);
				m_pOperationStop->sendCommandReply(strOutCommand);

				// Stop the execution immediatly
				bLoopIsActive = false;
				break;
			}

			// Start the loop
			m_vData.clear();
			m_vData.resize(m_StructParameters.liImagesNum);
			for ( int i = 0; i != m_StructParameters.liImagesNum; i++ )
			{
				// Set coordinates and image quality when needed
				if ( m_StructParameters.liImagesNum > ONLY_ONE_IMAGE )
				{
					uint16_t x0 = m_StructParameters.StructCoordinates[i].liLeft;
					uint16_t y0 = m_StructParameters.StructCoordinates[i].liBottom;
					uint16_t usiWidth = m_StructParameters.StructCoordinates[i].liRight - m_StructParameters.StructCoordinates[i].liLeft;
					uint16_t usiHeight = m_StructParameters.StructCoordinates[i].liBottom -m_StructParameters.StructCoordinates[i].liTop;
					int8_t cRes = MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setCropCoordinates(x0, y0, usiWidth, usiHeight);
					if ( cRes != ERR_NONE )
					{
						log(LOG_ERR, "THGetPicture::perform: unable to set cropCoordinates");
						return eOperationError;
					}
				}

				if ( ! m_bHaveSameQuality )
				{
					int8_t cRes = MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.setJPEGQuality(m_StructParameters.rgImageQuality[0]);
					if ( cRes != ERR_NONE )
					{
						log(LOG_ERR, "THGetPicture::perform: unable to set cropCoordinates");
						return eOperationError;
					}
				}

				MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.getPicture(m_vData[i]);
			}

			/*! *******************************************************************
			I have the data of the 2 pictures now, I need to packet and send them.
			******************************************************************** **/
			m_nPictureIdx++;
			sendHttpPictures();

			if ( ! m_StructParameters.strAction.compare("ONESHOT") )
			{
				// Stop the execution after one shot
				bLoopIsActive = false;
				break;
			}
		}

		/* ********************************************************************************************
		 * Access the state machine
		 * ********************************************************************************************
		 */
		operationSingleton()->removeOperation(m_pOperationExecute); // remove exec op
		operationSingleton()->removeOperation(m_pOperationStop); // remove stop op

		m_pOperationStop = nullptr;
		m_pOperationExecute = nullptr;

		log(LOG_INFO, "THGetPicture:th: done");
	}
	return 0;
}
#endif

void THGetPicture::beforeWorkerThread()
{
	log(LOG_INFO, "THGetPicture::started");
}

void THGetPicture::afterWorkerThread()
{
	m_synchSem.closeAndUnlink();
	log(LOG_INFO, "THGetPicture::stopped");
}

void THGetPicture::buildPayloadAndReply()
{
	OutCmdGetPicture cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);

	GetPicturePayload* pGetPicturePayload = new GetPicturePayload();
	pGetPicturePayload->setGetPictureAttr(m_nPictureIdx, m_StructParameters.liImagesNum, m_StructParameters.rgImageQuality,
										  m_StructParameters.StructCoordinates, m_vData);

	// Assign the payload to the outcommand to let the serializer will extract the info
	cmd.setPayload((Payload*)pGetPicturePayload);

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************************************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		log(LOG_ERR, "THReadSec::th: unable to serialize OutCmdReadSec");
	}
	else
	{
		m_pOperationExecute->sendCommandReply(strOutCommand);
	}
}

bool THGetPicture::sendHttpPictures(void)
{
	string strUsage;

	strUsage.assign(WEB_SERVICE_CONNECTION);

	Operation* pOperation = new Operation("OpGetPicture");
	pOperation->setUsage(strUsage);
	pOperation->setWebResponseUrl(m_strWebUrl);
	pOperation->setLogger(MainExecutor::getInstance()->getLogger());

	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdGetPicture cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);

	GetPicturePayload* pGetPicturePayload = new GetPicturePayload();
	pGetPicturePayload->setGetPictureAttr(m_nPictureIdx, m_StructParameters.liImagesNum, m_StructParameters.rgImageQuality,
										  m_StructParameters.StructCoordinates, m_vData);

	// Assign the payload to the outcommand to let the serializer will extract the info
	cmd.setPayload((Payload*)pGetPicturePayload);

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************************************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		log(LOG_ERR, "THReadSec::th: unable to serialize OutCmdReadSec");
	}
	else
	{
		pOperation->sendCommandReply(strOutCommand);
	}

	SAFE_DELETE(pOperation);
	return bSerializeResult;
}
