/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THMonitorPressure.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the THMonitorPressure class.
 @details

 ****************************************************************************
*/

#ifndef THMONITORPRESSURE_H
#define THMONITORPRESSURE_H

#include <deque>
#include <fstream>

#include "Thread.h"
#include "StaticSingleton.h"
#include "Loggable.h"
#include "Semaphore.h"

#include "CommonInclude.h"

#define MAX_CHANNEL_BUFFER_SIZE	256

typedef enum
{
	CH_CAN_RX_WAIT_STX		= 0,
    CH_CAN_RX_RECV_RAW_BODY = 1,
    CH_CAN_RX_RECV_ELAB_BODY = 2,
} enumStateChannelMsg;

typedef struct
{
	enumStateChannelMsg	stateRxMsg;
} structChannelMsgData;


class Operation;
class OpMonitorPressure;

class THMonitorPressure :	public StaticSingleton<THMonitorPressure>,
							public Thread,
							public Loggable
{
	public:

		std::deque<uint32_t> m_dataPressureChannel[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];
		std::deque<uint8_t> m_dataPressureElabChannel[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];
        std::deque<uint32_t> m_dataPressureRawChannel[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];

		/*! ***********************************************************************************************************
		 * @brief THMonitorPressure default constructor
		 * ***********************************************************************************************************
		 */
		THMonitorPressure();

		/*! *************************************************************************************
		 * @brief ~THMonitorPressure default destructor
		 * ************************************************************************************************
		 */
		virtual ~THMonitorPressure();

		/*! *********************************************************************************************
		 * @brief perform checks the context of the procedure is correct and release the semaphore to
		 *			 unlock the thread
		 * @param pOp the pointer to the  operation object
		 * @return eRunning in case of success, eError otherwise (see eOperationStatus enum)
		 * ********************************************************************************************
		 */
		int startRawMonitoring(OpMonitorPressure* pOp);

		/*! ***********************************************************************************************
		 * @brief getAction to get the action active (if any)
		 * @param section the index of the section
		 * @return the active action of the section
		 * **********************************************************************************************
		 */
		int getSectionAction(int section);

		/*! *********************************************************************************************
		 * @brief stopMonitoring send the command to section to stop acquisition and blocks the thread
		 * @param pOp the pointer to the  operation object
		 * @return 0 if success
		 * *******************************************************************************************
		 */
		int stopRawMonitoring(OpMonitorPressure* pOp);

		/*! *********************************************************************************************
		 * @brief stopProtocolMonitoring stop the data collection on the requested section during protocol
		 * @param section the section index
		 * @return 0 if success
		 * *******************************************************************************************
		 */
		int stopProtocolMonitoring(uint8_t section);

		/*! *********************************************************************************************
		 * @brief startProtocolMonitoring start the data collection on the requested section during protocol
		 * @param section the section index
		 * @return 0 if success
		 * *******************************************************************************************
		 */
		int startProtocolMonitoring(uint8_t section);

		/*! **********************************************************************************************
		 * @brief getDataVectorSize to get the number of elements stored in the pressure data array
		 * @param channel the index
		 * @param section the index of the section
		 * @return the size of array
		 * ******************************************************************************************
		 */
		unsigned int getDataVectorSize(uint8_t section, uint8_t channel);

		/*! **********************************************************************************************
		 * @brief isMonitorActive to get if at least one section is acquiring pressure
		 * @return true if monitoring is ongoing, false otherwise
		 * ******************************************************************************************
		 */
		bool isMonitorActive();

		/*! **********************************************************************************************
		 * @brief isActionPending to understand if actions are executed
		 * @param section the index of the section
		 * @return true if an action has to be completed, false otherwise
		 * ******************************************************************************************
		 */
		bool isActionPending(uint8_t section);


	protected:

		/*! ***************************************************************************************
		 * @brief workerThread neverending thread normally waiting on the semaphore : when the
		 *		semaphore it's released	the thread executes the ReadSection procedure and, finally,
		 *		 compose the asynchronous reply, restore the state machine and destroys the operation
		 * @return 0 when thread ends
		 * ***********************************************************************************************
		 */
		int workerThread(void);

		/*! *********************************************************************************************
		 * @brief beforeWorkerThread to perform actions before the start of the thread
		 * ************************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! *********************************************************************************************
		 * @brief beforeWorkerThread to perform actions after the end of the thread
		 * **********************************************************************************************
		 */
		void afterWorkerThread(void);

	private:

		/*! ***********************************************************************************************
		 * @brief sendReply send the http reply
		 * @param section the index of the section
		 * @return true if success
		 * *********************************************************************************************
		 */
		bool sendReply(uint8_t section);

		/*! ***********************************************************************************************
		 * @brief resetChannelsStructure reset the structure for the data collection of each channel
		 * @section the index of the section
		 * @param index the channel to be reset (if SCT_NUM_TOT_SLOTS + 1 -> all)
		 * ************************************************************************************************
		 */
		void resetChannelsStructure(uint8_t section, uint8_t index);

		/*! **********************************************************************************************
		 * @brief decodeMsgPressure build the pressure message per the specific channel managing
		 *  its status machine
		 * @param index the associated channel
		 * @param section the index of the section
		 * @param dataCan the array of 8 bytes read from the can bus
		 * @return true if ok, false in case of error
		 * ************************************************************************************************
		 */
		bool decodeMsgPressure(uint8_t section, uint8_t * dataCan, uint8_t channel);

		/*! ********************************************************************************************
		 * @brief clearDataVectors clear the arrays containing the pressure data
		 * @param section the index of the section
		 * *****************************************************************************************
		 */
		void clearDataVectors(uint8_t section);

		/*! ********************************************************************************************
		 * @brief executeAction executes the action linked to the operation and send the Succesfull reply
		 * @param section the index of the section
		 * @return false if no sections are working, true if at least one is stil working
		 * *****************************************************************************************
		 */
		bool executeAction(uint8_t section);

		/*! ********************************************************************************************
		 * @brief saveElabDataOnFile save the data elaborated to a binary file (name ElaboratePressure.bin)
		 * @param section the index of the section
		 * @param channel the index of the channel
		 * *****************************************************************************************
		 */
		void saveElabDataOnFile(uint8_t section, uint8_t channel);

		/*! ********************************************************************************************
		 * @brief saveRawDataOnFile save the raw data to a binary file (name RawPressureXY.bin)
		 *	where X = section, Y = channel
		 * @param section the index of the section
		 * @param channel the index of channel
		 * *****************************************************************************************
		 */
        void saveRawDataOnFile(uint8_t section, uint8_t channel);

		/*! ********************************************************************************************
		 * @brief closeRawDataFiles close all the RawPressureXY files
		 * @param section the index of the section
		 * *****************************************************************************************
		 */
        void closeRawDataFiles(uint8_t section);

    private:
        Semaphore m_synchSem;
        OpMonitorPressure * m_pOperation[SCT_NUM_TOT_SECTIONS];

        int m_sectionAction[SCT_NUM_TOT_SECTIONS];
        bool m_sectionChangeAction[SCT_NUM_TOT_SECTIONS];

        bool m_monitorInProgress;
        bool m_udpConnection;

        structChannelMsgData m_dataPacketFromChannel[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];

        ofstream m_pfile[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];

        uint32_t m_dataCounterChannel[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];
        uint8_t m_dataElabCounterChannel[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];
        bool m_bChannelInProgress[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];


};

class THSendPressure :	public StaticSingleton<THSendPressure>,
						public Thread,
						public Loggable
{
	public:

		/*! **********************************************************************************************
		 * @brief THSendPressure default constructor
		 *  **************************************************************************************
		 */
		THSendPressure();

		/*! *********************************************************************************************
		 * @brief ~THSendPressure default destructor
		 * *******************************************************************************************
		 */
		virtual ~THSendPressure();

		/*! ********************************************************************************************
		 * @brief startSending unlock the thread in order to start the sending mechanism
		 * @param pMonitorPressure pointer to the MonitorPressure thread so to retrieve data
		 * @param strWebUrl the address where to send the data
         * @param strCommandId the CommandId of the MonitorPressure command to be added at each data sending
		 * @return true if success, false otherwise
		 * **********************************************************************************************
		 */
        bool startSending(THMonitorPressure * pMonitorPressure, std::string strWebUrl, std::string strCommandId);

		/*! **********************************************************************************************
		 * @brief stopSending to stop the thread
		 * ************************************************************************************************
		 */
		void stopSending();

		/*! **********************************************************************************************
		 * @brief isSendingActive to retrieve the status of the thread
		 * ************************************************************************************************
		 */
		bool isSendingActive();

	protected:

		/*! ***********************************************************************************************
		 * @brief workerThread neverending thread normally waiting on the semaphore : when the semaphore
		 *	 it's released the thread executes the ReadSection procedure and, finally, compose the
		 *   asynchronous reply, restore the state machine and destroys the operation
		 * @return 0 when thread ends
		 * **********************************************************************************************
		 */
		int workerThread(void);

		/*! **********************************************************************************************
		 * @brief beforeWorkerThread to perform actions before the start of the thread
		 * ******************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! ***************************************************************************************
		 * @brief beforeWorkerThread to perform actions after the end of the thread
		 * ***************************************************************************************
		 */
		void afterWorkerThread(void);

	private:

		/*! ***************************************************************************************
		 * @brief sendHttpData build the payload compose the http message and sends it to pc
		 * @param dataSize the number of elements in the http message
		 * @param section the section index
		 * @return true if ok, false in case of error
		 * *********************************************************************************************
		 */
		bool sendHttpData(uint8_t section, uint32_t dataSize);

    private:
        Semaphore m_synchSem;

        bool m_sendInProgress;
        std::string m_strWebUrl, m_strCommandId;
        bool m_udpConnection;

        THMonitorPressure * m_pMonitorPressure;

};

#endif // THMONITORPRESSURE_H
