/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ServiceCollector.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the ServiceCollector class.
 @details

 ****************************************************************************
*/

#include <unistd.h>
#include "ServiceCollector.h"
#include "CommonInclude.h"

#include "THReadSec.h"
#include "THMonitorPressure.h"
#include "THCaptureImage.h"
#include "THAutoLogout.h"
#include "THVidasEp.h"

ServiceCollector::ServiceCollector()
{

}

ServiceCollector::~ServiceCollector()
{

}

bool ServiceCollector::startAll(void)
{
	bool bRetValue = true;

	THReadSec::getInstance()->setLogger(getLogger());
	if ( ! THReadSec::getInstance()->isRunning() )
	{
		bRetValue = THReadSec::getInstance()->startThread();
	}
	if ( ! bRetValue )	return false;

	THMonitorPressure::getInstance()->setLogger(getLogger());
	if ( !THMonitorPressure::getInstance()->isRunning() )
	{
		bRetValue = THMonitorPressure::getInstance()->startThread();
	}
	if ( ! bRetValue )	return false;

	THSendPressure::getInstance()->setLogger(getLogger());
	if ( ! THSendPressure::getInstance()->isRunning() )
	{
		bRetValue = THSendPressure::getInstance()->startThread();
	}
	if ( ! bRetValue )	return false;

	THAutoLogout::getInstance()->setLogger(getLogger());
	if ( !THAutoLogout::getInstance()->isRunning() )
	{
		bRetValue = THAutoLogout::getInstance()->startThread();
	}
	if ( ! bRetValue )	return false;

	THCaptureImage::getInstance()->setLogger(getLogger());
	if ( ! THCaptureImage::getInstance()->isRunning() )
	{
		bRetValue = THCaptureImage::getInstance()->startThread();
	}
	if ( ! bRetValue )	return false;

	THVidasEp::getInstance()->setLogger(getLogger());
	if ( ! THVidasEp::getInstance()->isRunning() )
	{
		bRetValue = THVidasEp::getInstance()->startThread();
	}
	if ( ! bRetValue )	return false;

	return bRetValue;
}

void ServiceCollector::stopAll(void)
{
	if ( THReadSec::getInstance()->isRunning() )
	{
		THReadSec::getInstance()->stopThread();
	}
	if ( THMonitorPressure::getInstance()->isRunning() )
	{
		THMonitorPressure::getInstance()->stopThread();
	}
	if ( THSendPressure::getInstance()->isRunning() )
	{
		THSendPressure::getInstance()->stopThread();
	}
	if ( THAutoLogout::getInstance()->isRunning() )
	{
		THAutoLogout::getInstance()->stopThread();
	}
	if ( THCaptureImage::getInstance()->isRunning() )
	{
		THCaptureImage::getInstance()->stopThread();
	}
	if ( THVidasEp::getInstance()->isRunning() )
	{
		THVidasEp::getInstance()->stopThread();
	}

	msleep(500);

	if ( THReadSec::getInstance()->isStopped() )
	{
		log(LOG_INFO, "ServiceCollector::stopAll: ReadSec thread closed");
	}
	if ( THMonitorPressure::getInstance()->isStopped() )
	{
		log(LOG_INFO, "ServiceCollector::stopAll: THMonitorPressure thread closed");
	}
	if ( THSendPressure::getInstance()->isStopped() )
	{
		log(LOG_INFO, "ServiceCollector::stopAll: THSendPressure thread closed");
	}
	if ( THAutoLogout::getInstance()->isStopped() )
	{
		log(LOG_INFO, "ServiceCollector::stopAll: THAutoLogout thread closed");
	}
	if ( THCaptureImage::getInstance()->isStopped() )
	{
		log(LOG_INFO, "ServiceCollector::stopAll: THGetPicture thread closed");
	}
	if ( THVidasEp::getInstance()->isStopped() )
	{
		log(LOG_INFO, "ServiceCollector::stopAll: THVidasEp thread closed");
	}

}

