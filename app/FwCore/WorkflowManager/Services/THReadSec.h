/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THReadSec.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the THReadSec class.
 @details

 ****************************************************************************
*/

#ifndef THREADSEC_H
#define THREADSEC_H


#include "Thread.h"
#include "StaticSingleton.h"
#include "Loggable.h"
#include "Semaphore.h"

class Operation;
class OpReadSec;

class THReadSec :		public StaticSingleton<THReadSec>,
						public Thread,
						public Loggable
{
	public:

		/*! ***********************************************************************************************************
		 * @brief THReadSec default constructor
		 * ***********************************************************************************************************
		 */
		THReadSec();

		/*! ***********************************************************************************************************
		 * @brief ~THReadSec default destructor
		 * ***********************************************************************************************************
		 */
		virtual ~THReadSec();

		/*! ***********************************************************************************************************
		 * @brief perform checks the context of the procedure is correct and release the semaphore to unlock the thread
		 * @param pOp the pointer to the ReadSec operation object
		 * @return eRunning in case of success, eError otherwise (see eOperationStatus enum)
		 * ***********************************************************************************************************
		 */
		int perform(OpReadSec* pOp);

		/*! ***********************************************************************************************************
		 * @brief completedOperation called by the scheduler to unlock the thread when the procedure final event
		 *			has been executed
		 * @param section the section involved in the scheduler event
		 * @param slot the slot involved in the scheduler event
		 * @param event the event involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int completedOperation(int section, int slot, int event);

	protected:

		/*! ***********************************************************************************************************
		 * @brief workerThread neverending thread normally waiting on the semaphore : when the semaphore it's released
		 *		the thread executes the ReadSection procedure and, finally, compose the asynchronous reply, restore
		 *		the state machine and destroys the operation
		 * @return 0 when thread ends
		 * ***********************************************************************************************************
		 */
		int workerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions before the start of the thread
		 * ***********************************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions after the end of the thread
		 * ***********************************************************************************************************
		 */
		void afterWorkerThread(void);

    private:
        Semaphore m_synchSem;
        OpReadSec * m_pOperation;

        int m_sectionRead;

};

/*! ***********************************************************************************************************
 * @brief THReadSec_completedOperation called by the scheduler to unlock the thread when the procedure final event
 *			has been executed : wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the slot involved in the scheduler event
 * @param event the event involved in the scheduler event
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THReadSec_completedOperation(int section, int slot, int event);

#endif // THREADSEC_H
