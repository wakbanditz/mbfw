/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PostmanThread.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the PostmanThread class.
 @details

 ****************************************************************************
*/

#include <string>
#include <unistd.h>
#include <string.h>

#include "CommonInclude.h"

#include "PostmanThread.h"
#include "Operation.h"
#include "MainExecutor.h"

static Mutex m_mutexOp;

PostmanThread::PostmanThread()
{

}

PostmanThread::~PostmanThread()
{

}

int PostmanThread::workerThread(void)
{

	eOperationStatus nOpStatus = eOperationError;

	/* ****************************************************************************************************************
	 * Open message queue for sending messages to the server
	 * ****************************************************************************************************************
	 */
	bool bRet = false;
	int liCnt = 0;
	while ( bRet == false )
	{
		bRet = m_operationMsgQueue.open(WF_MQ_OPMAN_TO_POSTMAN_NAME, O_RDONLY);
		if ( bRet == false )
		{
			log(LOG_DEBUG,
				"Postman: waiting to open msg queue \"%s\" t=%d sec",
				WF_MQ_OPMAN_TO_POSTMAN_NAME,
				liCnt);
			liCnt++;
			sleep(1);
		}
		else
		{
			break;
		}
	}
	m_operationMsgQueue.enableDebug(true);
	log(LOG_INFO, "Postman: started with queue \"%s\"", WF_MQ_OPMAN_TO_POSTMAN_NAME);
	m_operationList.clear();

	unsigned int uliMsgPriority;
	int liRecBufSize = WF_MQ_OPMAN_MAX_MSG_SIZE + 1;
	int liExpectedRecNumBytes = WF_MQ_OPMAN_MAX_MSG_SIZE - 1;
	char pInBuffer[liRecBufSize];

	// empty queue ?
	int liEmptyBytes = 1;
	while(liEmptyBytes > 0)
	{
        liEmptyBytes = m_operationMsgQueue.receive(pInBuffer, liRecBufSize, &uliMsgPriority, 10);
	}

	while ( isRunning() )
	{

		/* ************************************************************************************************************
		 * Wait for a new Operation to be handled
		 * ************************************************************************************************************
		 */
		nOpStatus = eOperationError;
		Operation* pOp = 0;
		memset(pInBuffer, 0x00, liRecBufSize);

		int liRecNumBytes = m_operationMsgQueue.receive(pInBuffer, liRecBufSize, &uliMsgPriority, 0);
		if ( liRecNumBytes == -1 )
		{
			if ( errno == ETIMEDOUT )
			{
				log(LOG_DEBUG, "Postman: TIME-OUT on receive");
			}
			else
			{
				log(LOG_ERR, "Postman: error from on receive");
			}
			// The following lines are necessary if the receive() is interrupted
			// by a signal before the timeout has expired
			if ( isRunning() )
			{
				continue;
			}
			else
			{
				log(LOG_INFO, "PostMan: received thread interruption");
				break;
			}
		}
		else if ( ( liRecNumBytes > 0 ) && ( liRecNumBytes != liExpectedRecNumBytes ) )
		{
			log(LOG_WARNING, "PostMan: received msg with size=%d != expected=%d", liRecNumBytes, liExpectedRecNumBytes);
			continue;
		}	
		pOp = *(Operation**)pInBuffer;

		// the operation must have a valid name
		if ( pOp == 0 || pOp->getName().empty())
		{
			continue;
		}

		/* ************************************************************************************************************
		 * Retrieve the type of activity to do on the Operation
		 * ************************************************************************************************************
		 */

		nOpStatus = pOp->getStatus();
		log(LOG_DEBUG, "Postman: <- received operation <%p:%s:%d>", pOp, pOp->getName().c_str(), nOpStatus);
		if ( nOpStatus == eOperationWaitForBirth )
		{
			/* ********************************************************************************************************
			 * Operation spawning
			 * ********************************************************************************************************
			 */
			m_mutexOp.lock();
			m_operationList.push_back(pOp);
			m_mutexOp.unlock();

			int liRetValue = pOp->perform();
			if ( liRetValue == eOperationRunning )
			{
				log(LOG_INFO, "Postman: STARTED op=<%p:%s>, operationList.size()=%d",
					pOp, pOp->getName().c_str(), m_operationList.size());
			}
			else
			{
				if ( liRetValue == eOperationWaitForDeath )
				{
					log(LOG_INFO,
						"Postman: CLOSING op=<%p:%s>, operationList.size()=%d",
						pOp, pOp->getName().c_str(), m_operationList.size());
				}
				else
				{
					log(LOG_INFO,
						"Postman: ERROR STARTING op=<%p:%s> err=%d",
						pOp, pOp->getName().c_str(), liRetValue);
				}
				// if the perform return immediately the WaitForDeath / Error it means the operation
				// is completed so it must be removed from the OperationManager (and not here)
				operationSingleton()->removeOperation(pOp);
				//m_operationList.remove(pOp);
				//SAFE_DELETE(pOp);
			}
		}
		else if ( nOpStatus == eOperationWaitForDeath )
		{
			/* ********************************************************************************************************
			 * Operation deleting because it is done
			 * ********************************************************************************************************
			 */
			log(LOG_INFO, "Postman: CLOSING op=<%p:%s>, operationList.size()=%d", pOp, pOp->getName().c_str(), m_operationList.size());

			m_mutexOp.lock();
			removeOpFromList(pOp);
			m_mutexOp.unlock();

			SAFE_DELETE(pOp);
			log(LOG_INFO, "Postman: CLOSED operationList.size()=%d", m_operationList.size());
		}
		else
		{
			/* ********************************************************************************************************
			 * Action unknown on the Operation
			 * ********************************************************************************************************
			 */
			log(LOG_ERR, "Postman: Action not managed");

			m_mutexOp.lock();
			bool bDelete = removeOpFromList(pOp);
			m_mutexOp.unlock();

			if(bDelete)	SAFE_DELETE(pOp);
			log(LOG_ERR, "Postman: Action removed");
		}
	}

	return 0;
}

bool PostmanThread::removeOpFromList(Operation * pOp)
{
	if(pOp == 0) return false;

	if(m_operationList.size() == 0) return false;

    log(LOG_DEBUG_PARANOIC, "Postman: remove from list");

	m_operationList.remove(pOp);

	return true;
}
