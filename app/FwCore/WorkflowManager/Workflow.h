/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    Workflow.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the Workflow class.
 @details

 ****************************************************************************
*/

#ifndef WORKFLOW_H
#define WORKFLOW_H

#include "StatusInclude.h"
#include "ErrorCodes.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

class IncomingCommand;
class OutgoingCommand;
class Operation;

/*! *******************************************************************************************************************
 * @brief The Workflow class	represents a workflow initiated after the reception of an IncomingCommand.
 *								Every IncomingCommand has a corresponding Workflow that must be initialized with
 *								the IncomingCommand instance (using the setIncomingCommand routine) that contains
 *								the parameters useful to the Workflow itself in order to take decisions.
 * ********************************************************************************************************************
 */
class Workflow : public Loggable
{

	public:

		/*! ***********************************************************************************************************
		 * @brief Workflow	void constructor
		 * ************************************************************************************************************
		 */
		Workflow();

		/*! ***********************************************************************************************************
		 * @brief ~Workflow	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~Workflow();

		/*! ***********************************************************************************************************
		 * @brief setIncomingCommand	initialize the IncomingCommand internal pointer. The IncomingCommand
		 *								pointer is used to access the input parameters from which the Workflow take
		 *								decision and change behavior
		 * @param pCmd					pointer to an IncomingCommand object
		 * ************************************************************************************************************
		 */
		void setIncomingCommand(IncomingCommand* pCmd);

		/*! ***********************************************************************************************************
		 * @brief setup					retrieve a synchronous OutgoingCommand to be sent to the client
		 *								(i.e. accepted/rejected or a generic synchronous answer) and an optional
		 *								Operation to be performed and destroyed later.
		 *								This routine is made of:
		 *									- a mandatory instantiation of an OutgoingCommand to be sent to the client
		 *									- a mandatory access to the checkAcceptance() method to verify whether the
		 *									  IncomingCommand can be performed or not;
		 *									- an optional schedulation sequence trial (accessing the Scheduler) whom
		 *									  result consists in the acceptance or not of the IncomingCommand and,
		 *									  if yes, in a mandatory access to the StateMachine::setState() method;
		 *									- the instantiation of an optional Operation to be queued to the
		 *									  OperationManager (feature used when a Workflow is made of a sequence
		 *									  of actions that can be executed without the need of the Scheduler, e.g.
		 *									  every maintenance command);
		 * @param[out] pCmd				synchronous OutgoingCommand to be sent to the client that must be destroyed by
		 *								the caller
		 * @param[out] pOp				optional Operation to be performed and destroyed later by the caller
		 * ************************************************************************************************************
		 */
		virtual void setup(OutgoingCommand* &pCmd, Operation* &pOp);


		/*! ***********************************************************************************************************
		 * @brief decodeTargetStatus	decode the string in input (read from the command file and stored in
		 *									IncomingCommand) associating it to a device status that will be activated
		 *									at the end of the workflow ; if BACK the device will go back to the
		 *									previous state
		 * @param strRead		transition status string
		 * @return				the decoded target status (eUnknown in case of previous, eNull in case of error)
		 * ************************************************************************************************************
		 */
		eDeviceStatus decodeTargetStatus(const std::string strRead);

		/*! ***********************************************************************************************************
		 * @brief setStateMachine	to lock, get and store current status, set execution state for all the devices in
		 *							list
		 * @param deviceList		the list of devices to be managed
         * @param bForceLock        if true even if the device is locked the action is allowed
		 * @return				0 if success, an error code otherwise
		 * ************************************************************************************************************
		 */
        int setStateMachine(std::vector<eDeviceId> deviceList, bool bForceLock = false);

    protected:


        /*! ***********************************************************************************************************
         * @brief checkAcceptance		routine to access the state machine and provide permits of execution of the
         *								Workflow
         * @param liErrorCode			the error code (if = 0 no error)
         * @return						true upon correct execution of the routine, false otherwise
         * ************************************************************************************************************
         */
        virtual bool checkAcceptance(int& liErrorCode);

        /*! ***********************************************************************************************************
         * @brief getOutgoingCommand	build the OutgoingCommand associated to the Workflow, that is the
         *								accepted / rejected response
         * @return						pointer to an OutgoingCommand that will be used and destroyed by somebody else
         * ************************************************************************************************************
         */
        virtual OutgoingCommand* getOutgoingCommand(void);

        /*! ***********************************************************************************************************
         * @brief getOperation			build the Operationt associated to the Workflow, if any, from the
         *								IncomingCommand passed as parameter
         * @return						pointer to an Operation that will be performed and destroyed by somebody else
         * ***************************SectionA**************************************************************************
         */
        virtual Operation* getOperation(void);

    protected:

        bool m_bHasOperation;
        IncomingCommand* m_pIncomingCommand;
        std::vector<eDeviceId> m_deviceList;

};

#endif // WORKFLOW_H
