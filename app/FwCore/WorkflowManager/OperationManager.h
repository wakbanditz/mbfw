/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OperationManager.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OperationManager class.
 @details

 ****************************************************************************
*/

#ifndef OPERATIONMANAGER_H
#define OPERATIONMANAGER_H


#include "PostmanThread.h"
#include "Mutex.h"
#include "Loggable.h"

class Operation;

class OperationManager : public Loggable
{

	public:

		/*! ***********************************************************************************************************
		 * @brief OperationManager	void constructor (void internal members)
		 * ************************************************************************************************************
		 */
		OperationManager();

		/*! ***********************************************************************************************************
		 * @brief ~OperationManager	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OperationManager();

		/*! ***********************************************************************************************************
		 * @brief init				initializes operation message queue and start the PostMan thread
		 * @return					true upon successfull initialization (msg and PostMan thread), false otherwise
		 * ************************************************************************************************************
		 */
		bool init(void);

		/*! ***********************************************************************************************************
		 * @brief stop				stop the PostMan thread, close and unlink the message queue
		 * @return					true if the message queue has been correctly unlinked
		 * ************************************************************************************************************
		 */
		bool stop(void);

		/*! ***********************************************************************************************************
		 * @brief addOperation		take a pointer to an operation, mark it as eWaitForBirth and insert it
		 *							in the queue, PostMan thread will pop it and will spawn it
		 * @param pOperation		pointer to an operation to be spawned
		 * @return					true if the Operation has been correctly pushed in the queue
		 * ************************************************************************************************************
		 */
		bool addOperation(Operation* pOperation);

		/*! ***********************************************************************************************************
		 * @brief removeOperation	take a pointer to an operation, mark it as eWaitForDeath and insert it
		 *							in the queue, PostMan thread will pop it and will destroy it
		 * @param pOperation		pointer to an operation to be closed
		 * @return					true if the Operation has been correctly pushed in the queue
		 * ************************************************************************************************************
		 */
		bool removeOperation(Operation* pOperation);

		/*! ***********************************************************************************************************
		 * @brief isAvailable		check whether the maximum number of parallel operations has been reached
		 * @return					true if it is possible to add an operation in the queue, false otherwise
		 * ************************************************************************************************************
		 */
		bool isAvailable(void);

    private:

        Mutex m_operationListMutex;
        PostmanThread m_postman;
        MessageQueue m_operationMsgQueue;
        int m_liMaxOperations;
        int m_liRunningOperations;

    private:

        bool sendMsgToPostman(Operation* pOperation);
        bool handleOperation(Operation* pOperation, eOperationStatus liStatus);

};

#endif // OPERATIONMANAGER_H
