/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    LoginManager.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the LoginManager class.
 @details

 ****************************************************************************
*/

#include <string>
#include <iostream>
#include <fstream>

#include "CommonInclude.h"
#include "LoginManager.h"
#include "MainExecutor.h"

/* ***************************************************************************************************
 * The login is composed of login + password + connection url
 * in the external file the possible combination login + password are stored
 * when a login command is received first the sw checks if the login / password received correspond to a pair
 * read from the file: if not --> refuse it
 * then it checks if a login is already active on the same url: if yes --> refuse it
 *  (only one login x url but many url with the same login)
 * now it can add the couple login + url to the list
 * when a logout command is received the corresponding url is removed from the active list
 *
 * */

LoginManager::LoginManager()
{
	m_loginActiveList.clear();
	m_loginSetupList.clear();
}

LoginManager::~LoginManager()
{
	m_loginActiveList.clear();
	m_loginSetupList.clear();
}

bool LoginManager::init()
{
	std::string strRead, strId, strPwd;
	std::ifstream inFile;

	setLogger(MainExecutor::getInstance()->getLogger());

    inFile.open(LOGIN_SETUP_FILE);
    if(inFile.fail())
	{
		//File does not exist code here
		printf("LoginManager::init-->%s",strerror(errno));
		return false;
	}

	while(!inFile.eof())
	{
		getline(inFile, strRead);
		if(!strRead.empty())
		{
			// lines beginning with # are comments
			if(strRead.front() != '#')
			{
				size_t posStr = 0;

				if((posStr = strRead.find("=")) != std::string::npos)
				{
					// extract the value for the login / password
					posStr = strRead.find("=");
					strPwd = strRead.substr(posStr + 1);
					strId = strRead.substr(0, posStr);
					m_loginSetupList.emplace_back(strId, strPwd);
				}
			}
		}
	}
	inFile.close();
	// if no values have been added use the default
	if(m_loginSetupList.size() == 0)
	{
		m_loginSetupList.emplace_back(WEB_VALID_LOGIN_ID_VALUE, WEB_VALID_LOGIN_PWD_VALUE);
	}
	return true;
}

bool LoginManager::addLogin(std::string strUsage, std::string strWebUrl)
{
	// if there already is a connection on this URL -> refuse it (the check should heve been already done ...)
	if(isLoginUrlActive(strUsage, strWebUrl) != -1)
		return false;
	m_loginActiveList.emplace_back(strUsage, strWebUrl);
	return true;
}

bool LoginManager::removeLogin(std::string strUsage, std::string strWebUrl)
{
	int posIndex = isLoginUrlActive(strUsage, strWebUrl);

	if(posIndex == -1)
		return false;

	m_loginActiveList.erase(m_loginActiveList.begin() + posIndex);
	return true;
}

bool LoginManager::removeLoginByUsage(std::__cxx11::string strUsage)
{
	std::vector<std::pair<std::string, std::string>>::const_iterator loginIterator;
	int index;

	for(index = 0, loginIterator = m_loginActiveList.begin();
		loginIterator != m_loginActiveList.end();
		index++, loginIterator++)
	{
		if(strcmp(strUsage.c_str(), loginIterator->first.c_str()) == 0)
		{
			m_loginActiveList.erase(m_loginActiveList.begin() + index);
			return true;
		}
	}
	return false;
}

bool LoginManager::checkLoginValid(std::string strLogin, std::string strPassword)
{
	std::vector<std::pair<std::string, std::string>>::const_iterator loginIterator;

	for(loginIterator = m_loginSetupList.begin(); loginIterator != m_loginSetupList.end(); loginIterator++)
	{
		if(strcmp(strLogin.c_str(), loginIterator->first.c_str()) == 0)
		{
			if(strcmp(strPassword.c_str(), loginIterator->second.c_str()) == 0)
				return true;
		}
	}

	return false;
}

int LoginManager::isLoginUrlActive(std::__cxx11::string strUsage, std::__cxx11::string strWebUrl)
{
	std::vector<std::pair<std::string, std::string>>::const_iterator loginIterator;
	int index;

	for(index = 0, loginIterator = m_loginActiveList.begin(); loginIterator != m_loginActiveList.end();
				index++, loginIterator++)
	{
		if(strcmp(strWebUrl.c_str(), loginIterator->second.c_str()) == 0 &&
		   strcmp(strUsage.c_str(), loginIterator->first.c_str()) == 0)
			return index;

		//it should be only a COMMERCIAL connection login
		if ((strUsage.compare("COMMERCIAL") == 0) && (loginIterator->first.compare("COMMERCIAL") == 0))
		{
			return index;
		}
	}

	return -1;
}

bool LoginManager::isLoginAcceptable(std::__cxx11::string strUsage, std::__cxx11::string strWebUrl)
{
	if(isLoginUrlActive(strUsage, strWebUrl) != -1)
	{
		// it's a login already active -> no change, accept it
		return true;
	}

	std::vector<std::pair<std::string, std::string>>::const_iterator loginIterator;
	int index;

	for(index = 0, loginIterator = m_loginActiveList.begin(); loginIterator != m_loginActiveList.end();
				index++, loginIterator++)
	{
		if(strcmp(strUsage.c_str(), loginIterator->first.c_str()) == 0)
		{
			// a login of the same Usage is active -> refuse the new one
			return false;
		}
	}

	return true;
}

std::__cxx11::string LoginManager::getUrlActiveByUsage(std::__cxx11::string strUsage)
{
	std::vector<std::pair<std::string, std::string>>::const_iterator loginIterator;
	std::string retString;

	retString.clear();
	for(loginIterator = m_loginActiveList.begin(); loginIterator != m_loginActiveList.end(); loginIterator++)
	{
		if(strcmp(strUsage.c_str(), loginIterator->first.c_str()) == 0)
		{
			retString.assign(loginIterator->second);
		}
	}

	return retString;
}

int LoginManager::getLoginActiveList(vector<pair<string, string>> & loginList)
{
	loginList = m_loginActiveList;

	return m_loginActiveList.size();
}
