/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    Operation.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the Operation class.
 @details

 ****************************************************************************
*/

#include "Operation.h"
#include "OperationManager.h"
#include "MainExecutor.h"
#include "Log.h"
#include "WebServerAndProtocolInclude.h"
#include "HttpConnection.h"

Operation::Operation(string strName)
{
	m_strName.assign(strName);
	m_liStatus = eOperationError;
	m_strWebResponseUrl.clear();
	m_strUsage.clear();
	m_strID.clear();
	m_pLogger = 0;
	m_errorCode = 0;
    m_bPersistent = false;
}

Operation::~Operation()
{
}

string Operation::getName(void) const
{
	return m_strName;
}

eOperationStatus Operation::getStatus(void) const
{
	return m_liStatus;
}

void Operation::setStatus(eOperationStatus liStatus)
{
	m_liStatus = liStatus;
}

int Operation::perform(void)
{
	return eOperationRunning;
}

void Operation::setWebResponseUrl(string& strUrl)
{
	m_strWebResponseUrl.assign(strUrl);
}

void Operation::setPersistent(bool status)
{
    m_bPersistent = status;
}

bool Operation::getPersistent()
{
    return m_bPersistent;
}

void Operation::setUsage(string strUsage)
{
	m_strUsage.assign(strUsage);
}

std::string Operation::getUsage()
{
	return m_strUsage;
}

string Operation::getWebUrl()
{
	return m_strWebResponseUrl;
}

void Operation::setID(const string& strId)
{
	m_strID.assign(strId);
}

string Operation::getID() const
{
	return m_strID;
}

void Operation::setErrorCode(uint16_t code)
{
	m_errorCode = code;
}

uint16_t Operation::getErrorCode()
{
	return m_errorCode;
}


void Operation::setLogger(Log* pLogger)
{
	m_pLogger = pLogger;
}

bool Operation::sendCommandReply(string& strCommandReply)
{
    m_pLogger->log(LOG_DEBUG_PARANOIC, "Operation::sendCommandReply--> URL %s, DATA %s ",
				   m_strWebResponseUrl.c_str(),
				   strCommandReply.c_str());

	HttpConnection connection("");

    std::string serialNumber;
    serialNumber.assign(infoSingleton()->getInstrumentSerialNumber());
    if(serialNumber.empty() == true)
    {
        serialNumber.assign(WEB_HEADER_SERIAL_NUMBER_VALUE);
    }

	connection.appendHeader(WEB_HEADER_CONTENT_TYPE, WEB_HEADER_APPLICATION_XML_TYPE);
    connection.appendHeader(WEB_HEADER_SERIAL_NUMBER_NAME, serialNumber);
	connection.appendHeader(WEB_HEADER_ACCEPT_TYPE, WEB_HEADER_APPLICATION_XML_TYPE);
	connection.appendHeader(WEB_HEADER_CONNECTION_TYPE, WEB_HEADER_CONNECTION_KEEPALIVE);
	connection.appendHeader(WEB_HEADER_EXPECT_TYPE, "");

	HttpResponse r = connection.post(m_strWebResponseUrl, strCommandReply);

    // log the reply
	logCommandReply(r, strCommandReply);

	return (r.code == HTTP_RET_CODE_OK);
}

void Operation::logCommandReply(HttpResponse reply, string& strCommandReply)
{
	if ( m_pLogger)
	{
		std::string strName;
		strName.assign(m_strName);

		m_pLogger->log(LOG_DEBUG, strName.append(": response").c_str());
		strName.assign(m_strName);
		m_pLogger->log(LOG_DEBUG, strName.append(": HEADERS").c_str());
		strName.assign(m_strName);

		for(HeaderFields::const_iterator it = reply.headers.begin(); it != reply.headers.end(); ++it)
		{
			m_pLogger->log(LOG_DEBUG, "                 \"%s\"=\"%s\"", it->first.c_str(), it->second.c_str());
		}
		m_pLogger->log(LOG_DEBUG, strName.append(": BODY:").c_str());

		m_pLogger->log(LOG_DEBUG, strCommandReply.c_str());

		strName.assign(m_strName);
		strName.append(": REPLY: ");
		strName.append(std::to_string(reply.code));
		m_pLogger->log(LOG_DEBUG, strName.c_str());
	}
}

void Operation::setDevicesList(vector<eDeviceId> deviceList)
{
	m_deviceList = deviceList;
}

void Operation::restoreStateMachine()
{
	bool bChange = false;

	for(int i = 0; i < (int)m_deviceList.size(); i++)
	{
		// set the device status to the target (end of operation)
		bChange |= stateMachineSingleton()->setDeviceStatusToTarget(m_deviceList.at(i));
		// unlock the device
		stateMachineSingleton()->setDeviceLock(m_deviceList.at(i), eLockOff);
	}

	// send VidasEp after change of status (if any)
	if(bChange)
	{
		requestVidasEp();
	}

	m_pLogger->log(LOG_DEBUG, "Operation::restoreStateMachine");
}

int Operation::closeOperation()
{
	/* ********************************************************************************************
	 * Access the state machine
	 * ********************************************************************************************
	 */
	restoreStateMachine();

	/* ********************************************************************************************
	 * Send the reply
	 * ********************************************************************************************
	 */
	compileSendOutCmd();

	/* ********************************************************************************************
	 * Remove the current Operation from list
	 * ********************************************************************************************
	 */
	operationSingleton()->removeOperation(this);

	return 0;
}

void Operation::addFieldCmd(vector<structOpCmdStruct>& cmd, const string& strName, uint8_t ubVal)
{
	structOpCmdStruct	cmdStruct;

	cmdStruct.strNameCmd.assign(strName);
	cmdStruct.ubCmd = ubVal;

	cmd.push_back(cmdStruct);
}

int Operation::setCodedPositions(vector<structOpCmdStruct>& cmd, const string strPos, uint8_t &ubPosCoded)
{
	for (size_t i = 0; i < cmd.size(); i++)
	{
		if (cmd[i].strNameCmd.compare(strPos) == 0)
		{
			ubPosCoded = cmd[i].ubCmd;
			return 0;
		}
	}

	return -1;
}

int Operation::executeCmd(int lNum )
{
	(void)(lNum);

	return 0;
}

int Operation::compileSendOutCmd()
{
	return 0;
}

int Operation::notifyCompleted(int lNum, bool bSuccess )
{
	(void)(lNum);
	(void)(bSuccess);

	return 0;
}

