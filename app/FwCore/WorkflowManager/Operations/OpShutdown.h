/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpShutdown.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpShutdown class.
 @details

 ****************************************************************************
*/

#ifndef OPSHUTDOWN_H
#define OPSHUTDOWN_H

#include "Operation.h"
#include "CommonInclude.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SHUTDOWN command
 * ********************************************************************************************************************
 */

class OpShutdown : public Operation
{

	public:

		/*! ***********************************************************************************************************
		 * @brief OpShutdown default constructor
		 * ***********************************************************************************************************
		 */
		OpShutdown();

		/*! ***********************************************************************************************************
		 * @brief ~OpShutdown default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpShutdown();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

	private :

		bool m_sectionRunning[SCT_NUM_TOT_SECTIONS];

};

#endif // OPSHUTDOWN_H
