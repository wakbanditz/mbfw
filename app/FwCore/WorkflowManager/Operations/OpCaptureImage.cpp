﻿/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpCaptureImage.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpCaptureImage class.
 @details

 ****************************************************************************
*/

#include <iterator>

#include "OpCaptureImage.h"
#include "THCaptureImage.h"
#include "SectionInclude.h"
#include "SectionBoardTray.h"
#include "SectionBoardSPR.h"
#include "OutCmdCaptureImage.h"

#define TRAY_OUT_CODED_COMMAND			"Out"
#define SPR_LOAD_DM_CODED_COMMAND		"Load"

#define WHITE					255
#define PICTURE_CUSTOM_ILL_THR	20
#define PICTURE_ILL_STEP_SIZE	5
#define PICTURE_DEFAULT_GAIN	19
#define PICTURE_DEFAULT_EXP		20000
#define PICTURE_DEFAULT_ILL		100
#define PICTURE_ILL_VALUE_TO_SKIP_AGC	5
#define PICTURE_DEFAULT_QUALITY	100
#define PICTURE_FOCUS_PATTERN	"TRAY"
#define MAX_IMAGE_NUM			2
#define PICTURE_DEF_ERR_SIZE	40000


OpCaptureImage::OpCaptureImage() : Operation("OpCaptureImage")
{
	m_nSectionId = -1;
	m_nCurrImageId = 0;
	m_isTimeToHomeMotors = true;
	m_bEnableRoi[0] = false;
	m_bEnableRoi[1] = false;
	m_vData.resize(MAX_IMAGE_NUM);
}

OpCaptureImage::~OpCaptureImage()
{

}

void OpCaptureImage::setParameters(string strAction, vector<vector<tuple<string, string, string>>>& tMotor, structCameraParams& pStruct)
{
    m_vMotorTuple = tMotor;
    m_strAction = strAction;
    m_CameraParams = pStruct;
}

int OpCaptureImage::perform()
{
    m_pLogger->log(LOG_NOTICE, "OpCaptureImage::perform: before, action %s", m_strAction.c_str());

    for ( int i = m_nCurrImageId; i != m_CameraParams.liPicturesNum; ++i )
    {
        while ( m_vMotorTuple[i].size() )
		{
            // Parse the first element
            if ( ! get<1>(m_vMotorTuple[i].front()).compare(XML_TAG_SECTIONA_NAME) )
            {
                m_nSectionId = SCT_A_ID;
                return sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
            }
            else if ( ! get<1>(m_vMotorTuple[i].front()).compare(XML_TAG_SECTIONB_NAME) )
            {
                m_nSectionId = SCT_B_ID;
                return sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
            }
            else if ( ! get<1>(m_vMotorTuple[i].front()).compare(XML_TAG_NSH_NAME) )
            {
                    // TODO: remove with the new SSW version!!
                if ( m_CameraParams.vPredefinedRoi[i].empty() )
                {
                    string strPredefined = get<2>(m_vMotorTuple[i].front());
                    string strCamera("Camera");
                    size_t pos = strPredefined.find(strCamera);
                    if ( pos != string::npos )
                    {
						strPredefined.erase(pos, strCamera.size());
						m_CameraParams.vPredefinedRoi[i] = strPredefined;
                    }
                }
                return nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
            }
            else
            {
                m_vMotorTuple[i].erase(m_vMotorTuple[i].begin());
            }
        }

        // If there is no motor to be moved, then start acquiring
        if ( m_strAction.compare("ONESHOT") )
        {
            m_pLogger->log(LOG_NOTICE, "OpCaptureImage::perform: before thread", m_strAction.c_str());
            if ( ! getAction().compare(STOP_TAKING_PICTURES) )
            {
                // the thread is working -> let's stop it
                return THCaptureImage::getInstance()->stopTakingPictures(this);
            }
            else
            {
                return THCaptureImage::getInstance()->perform(this, &m_CameraParams);
            }
        }
        else
        {
            return cameraBoardManagerSingleton()->setCameraAction(eCameraActionExecute, (Operation*)this);
        }
	}

    restoreStateMachine();
    int8_t cResult = compileSendOutCmd();
    if ( cResult == eOperationEnabledForDeath )
    {
        operationSingleton()->removeOperation(this);
		m_pLogger->log(LOG_INFO, "OpCaptureImage::perform-->operation removed");
    }
    else
    {
		m_pLogger->log(LOG_ERR, "OpCaptureImage::perform-->NO operation removal");
    }
    return 0;
}

int OpCaptureImage::executeCmd(int lNum)
{
	int liRes = -1;

	switch ( lNum )
	{
		case SCT_A_ID:
		case SCT_B_ID:	//action to perform
			liRes = executeMoveSection(lNum);
		break;

		case NSH_ID:
			liRes = executeMoveNSH();
		break;

		case CAMERA_ID:
			liRes = executeCamera();
		break;
	}

	if ( liRes != 0 )
	{
		m_pLogger->log(LOG_ERR, "OpCaptureImage::executeCmd-->execute not performed");
	}

	if ( ! m_vMotorTuple[m_nCurrImageId].empty() )
	{
		string strLastDevice(get<1>(m_vMotorTuple[m_nCurrImageId].front()));
		// remove element after movement
		if ( ! m_vMotorTuple[m_nCurrImageId].empty() )
		{
			m_vMotorTuple[m_nCurrImageId].erase(m_vMotorTuple[m_nCurrImageId].begin());
		}

		// If the new component is the same as the previous one, than the manager is busy.
		// Since the action is execute, if the function returns 0, it is called again and
		// The next item will be parsed.
		if ( ! m_vMotorTuple[m_nCurrImageId].empty() )
		{
			if ( ! strLastDevice.compare(get<1>(m_vMotorTuple[m_nCurrImageId].front())) )
			{
				return 0;
			}
		}
	}

	liRes = perform();
	if ( liRes != eOperationRunning )
	{
		return -1;
	}

	m_pLogger->log(LOG_INFO, "OpCaptureImage::executeCmd-->OpCaptureImage executed");
	return liRes; // to stop the loop
}

structCameraParams* OpCaptureImage::getParams()
{
	return &m_CameraParams;
}

string OpCaptureImage::getAction(void)
{
	return m_strAction;
}

int OpCaptureImage::executeMoveSection(int liSecNum)
{
	// perform home at the end
	if ( m_isTimeToHomeMotors )
	{
        sectionBoardLinkSingleton(liSecNum)->m_pTray->m_CommonMessage.searchHome();
		sectionBoardLinkSingleton(liSecNum)->m_pSPR->m_CommonMessage.moveToAbsolutePosition(10, TH_MOVE_CMD_TIMEOUT);
        sectionBoardLinkSingleton(liSecNum)->m_pSPR->m_CommonMessage.searchHome();
		m_isTimeToHomeMotors = false;
	}

	SectionCommonMessage* pMotor = nullptr;
	string strCoded("");

	//choice of the component to move
	if ( get<0>(m_vMotorTuple[m_nCurrImageId].front()).compare(SECTION_TRAY_STRING) == 0 )
	{
		strCoded = sectionBoardLinkSingleton(liSecNum)->m_pTray->getCodedFromLabel(get<2>(m_vMotorTuple[m_nCurrImageId].front()));
		pMotor = &sectionBoardLinkSingleton(liSecNum)->m_pTray->m_CommonMessage;
	}
	else if ( get<0>(m_vMotorTuple[m_nCurrImageId].front()).compare(SECTION_SPR_STRING) == 0 )
	{
		strCoded = sectionBoardLinkSingleton(liSecNum)->m_pSPR->getCodedFromLabel(get<2>(m_vMotorTuple[m_nCurrImageId].front()));
		pMotor = &sectionBoardLinkSingleton(liSecNum)->m_pSPR->m_CommonMessage;
	}
	else
	{
		m_pLogger->log(LOG_WARNING, "OpCaptureImage::executeMoveSection: Motor %s not in list",
					   get<0>(m_vMotorTuple[m_nCurrImageId].front()).c_str());
		return 0;
	}

	char ubPosCode = *strCoded.c_str();
	int liRes = pMotor->moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
	if ( liRes )
	{
		return -1;
	}

	return 0;
}

int OpCaptureImage::executeMoveNSH()
{
	// Move
	string strStepsVal("");
	bool bRes = nshBoardManagerSingleton()->m_pConfig->getItemValue(get<2>(m_vMotorTuple[m_nCurrImageId].front()), strStepsVal);
	if ( ! bRes )
	{
		return -1;
	}

	int liSteps = stoi(strStepsVal);
	bRes = spiBoardLinkSingleton()->move(eAbsPos, liSteps);
	if ( ! bRes )
	{
		return -1;
	}

	return 0;
}

int OpCaptureImage::executeCamera()
{
    enum class eOperation { eStart, eSetLight, eSetFormat, eSetQuality, eSetCoordinates, eSendBatch, eCaptureImage, eRecover, eEnd};
    eOperation eState = eOperation::eStart;
    structInfoImage InfoImage;
    int liRes = 0;

    while ( eState != eOperation::eEnd )
    {
        switch(eState)
        {
            case eOperation::eStart:
                eState = m_CameraParams.vPredefinedRoi[m_nCurrImageId].empty() ? eOperation::eSetLight : eOperation::eSendBatch;
            break;

            case eOperation::eSetLight:
            {
				// the flag is cleare to let the masterboard know that next time a batch file has to be sent, than also the general parameters are sent
                usbBoardLinkSingleton()->m_pCR8062->clearBatchFlag();
                int liIll = ( m_CameraParams.vLight[m_nCurrImageId] == -1 ) ? PICTURE_DEFAULT_ILL : m_CameraParams.vLight[m_nCurrImageId];
                customization(&liIll);
				if ( liIll == PICTURE_ILL_VALUE_TO_SKIP_AGC )
				{
					eState = eOperation::eSetFormat;
					break;
				}
                liRes = usbCameraSingleton()->setAGCparam(liIll, PICTURE_DEFAULT_EXP, PICTURE_DEFAULT_GAIN);
                if ( liRes != ERR_NONE )
                {
					// update to new id
					m_nCurrImageId++;
                    m_pLogger->log(LOG_ERR, "OpCaptureImage::executeCamera: unable to set light");
					if ( liRes == ERR_CR8062_IN_PARAMETERS ) return -1;
                }
                msleep(5);
                eState = ( liRes == ERR_NONE ) ? eOperation::eSetFormat : eOperation::eRecover;
            }
            break;

            case eOperation::eSetFormat:
            {
                eImageFormat eDesiredFormat = eJPEG;
                if ( m_CameraParams.vFormat[m_nCurrImageId].empty() || ! m_CameraParams.vFormat[m_nCurrImageId].compare("RAW") )
                {
                    eDesiredFormat = eRAW;
                }

                if ( usbCameraSingleton()->getImageFormat() != eDesiredFormat )
                {
                    liRes = usbCameraSingleton()->setImageMode(eDesiredFormat);
                    if ( liRes != ERR_NONE )
                    {
                        m_pLogger->log(LOG_ERR, "OpCaptureImage::executeCamera: unable to set image mode");
                    }
                }
                msleep(5);
                eState = ( liRes == ERR_NONE ) ? eOperation::eSetQuality : eOperation::eRecover;
            }
            break;

            case eOperation::eSetQuality:
            {
                if ( usbCameraSingleton()->getImageFormat() == eJPEG )
                {
                    int liQuality = ( m_CameraParams.vQuality[m_nCurrImageId] == -1 ) ? PICTURE_DEFAULT_QUALITY : m_CameraParams.vQuality[m_nCurrImageId];
                    liRes = usbCameraSingleton()->setJPEGQuality(liQuality);
                    if ( liRes != ERR_NONE )
                    {
						// update to new id
						m_nCurrImageId++;
                        m_pLogger->log(LOG_ERR, "OpCaptureImage::performStateMachine: unable to set quality");
						if ( liRes == ERR_CR8062_IN_PARAMETERS ) return -1;
                    }
                }
                msleep(5);
                eState = ( liRes == ERR_NONE ) ? eOperation::eSetCoordinates : eOperation::eRecover;
            }
            break;

            case eOperation::eSetCoordinates:
				if ( ! m_CameraParams.vCropEnabled[m_nCurrImageId] ||
					 ( m_CameraParams.vrgCoordinates[m_nCurrImageId].liTop == 0 &&
					   m_CameraParams.vrgCoordinates[m_nCurrImageId].liBottom == 0 &&
					   m_CameraParams.vrgCoordinates[m_nCurrImageId].liLeft == 0 &&
					   m_CameraParams.vrgCoordinates[m_nCurrImageId].liRight == 0 ) )
				{
                    if ( liRes == ERR_NONE )
                    {
                        liRes = usbCameraSingleton()->disableCrop();
                    }
                }
                else
                {
                    uint16_t x0 = m_CameraParams.vrgCoordinates[m_nCurrImageId].liLeft;
                    uint16_t y0 = m_CameraParams.vrgCoordinates[m_nCurrImageId].liTop;
                    uint16_t usiWidth = m_CameraParams.vrgCoordinates[m_nCurrImageId].liRight - x0;
                    uint16_t usiHeight = m_CameraParams.vrgCoordinates[m_nCurrImageId].liBottom - y0;;
                    liRes = usbCameraSingleton()->setCropCoordinates(x0, y0, usiWidth, usiHeight);
                }

                if ( liRes != ERR_NONE )
                {
					// update to new id
					m_nCurrImageId++;
                    m_pLogger->log(LOG_ERR, "OpCaptureImage::performStateMachine: unable to set crop coordinates");
					if ( liRes == ERR_CR8062_IN_PARAMETERS ) return -1;
                }
                msleep(5);
                eState = ( liRes == ERR_NONE ) ? eOperation::eCaptureImage : eOperation::eRecover;
            break;

            case eOperation::eSendBatch:
                if ( ! m_CameraParams.vFocus[m_nCurrImageId].empty() &&
					 ! m_CameraParams.vFocus[m_nCurrImageId].compare(PICTURE_FOCUS_PATTERN) )
                {
                    size_t pos = m_CameraParams.vPredefinedRoi[m_nCurrImageId].find("_");
					if ( pos == string::npos )
					{
						// update to new id
						m_nCurrImageId++;
						return -1;
					}

                    string tmp("");
                    for ( size_t i = 0; i != pos; ++i )
                    {
                        if ( isdigit(m_CameraParams.vPredefinedRoi[m_nCurrImageId][i]) )
                            tmp.push_back(m_CameraParams.vPredefinedRoi[m_nCurrImageId][i]);
                    }
                    eBatchTarget eTarget;
                    if ( ! tmp.compare("0") )	eTarget = eBatchTarget::eSampleX0;
					else						eTarget = eBatchTarget::eSampleX3;

                    tmp.clear();
                    for ( size_t i = pos; i != m_CameraParams.vPredefinedRoi[m_nCurrImageId].size(); ++i )
                    {
                        if ( isdigit(m_CameraParams.vPredefinedRoi[m_nCurrImageId][i]) )
                            tmp.push_back(m_CameraParams.vPredefinedRoi[m_nCurrImageId][i]);
                    }
                    uint8_t ucSlot = stoi(tmp);
                    liRes = usbBoardLinkSingleton()->m_pCR8062->uploadBatchFile(ucSlot, eTarget);
					// It may happen that the general batch is not updated. The Image has to be RAW before applying algorithm
					if ( usbCameraSingleton()->getImageFormat() != eRAW )			liRes = usbCameraSingleton()->setImageMode(eRAW);
                }
                else
                {
					// Bigger coordinates
					liRes = usbBoardLinkSingleton()->m_pCR8062->uploadCalibrBatchFile(m_CameraParams.vPredefinedRoi[m_nCurrImageId]);
                    if ( liRes != ERR_NONE )
                    {
                        m_pLogger->log(LOG_ERR, "OpCaptureImage::performStateMachine: unable to send batch file");
                    }
                    msleep(10);
					if ( ! m_CameraParams.vCropEnabled[m_nCurrImageId] )
					{
						liRes = usbCameraSingleton()->disableCrop();
						if ( liRes != ERR_NONE )	m_pLogger->log(LOG_ERR, "OpCaptureImage::executeCamera: unable to disable crop");
						liRes = usbCameraSingleton()->setImageMode(eJPEG);
						if ( liRes != ERR_NONE )	m_pLogger->log(LOG_ERR, "OpCaptureImage::executeCamera: unable to set image mode");
						liRes = usbCameraSingleton()->setJPEGQuality(50);
						if ( liRes != ERR_NONE )	m_pLogger->log(LOG_ERR, "OpCaptureImage::executeCamera: unable to set image quality");
					}
                }
                eState = ( liRes == ERR_NONE ) ? eOperation::eCaptureImage : eOperation::eRecover;
            break;

            case eOperation::eCaptureImage:
            {
				m_CameraParams.vFormat[m_nCurrImageId] = ( usbCameraSingleton()->getImageFormat() == eRAW ) ? "RAW" : "JPEG";
				m_pLogger->log(LOG_ERR, "OpCaptureImage::performStateMachine: getting picture");
				int liRes = usbCameraSingleton()->getPicture(m_vData[m_nCurrImageId], &InfoImage);
				if ( liRes != ERR_NONE || m_vData[m_nCurrImageId].empty() )
				{
					m_pLogger->log(LOG_ERR, "OpCaptureImage::performStateMachine: unable to get picture");
					eState = eOperation::eRecover;
					break;
				}
				m_CameraParams.vFormat[m_nCurrImageId] = ( InfoImage.eFormat == eRAW ) ? "RAW" : "JPEG";

				if ( ! m_CameraParams.vFocus[m_nCurrImageId].empty() &&
					 ! m_CameraParams.vFocus[m_nCurrImageId].compare(PICTURE_FOCUS_PATTERN) )
				{
					SampleDetectionLink detector;
					bool bRes = detector.initZeroZero(m_pLogger, InfoImage.liWidth, InfoImage.liHeight);
					if ( ! bRes )
					{
						m_pLogger->log(LOG_ERR, "OpCaptureImage::performStateMachine: unable to init sample detection algorithm");
						eState = eOperation::eRecover;
						break;
					}

					vector<uint32_t> vImage(m_vData[m_nCurrImageId].begin(), m_vData[m_nCurrImageId].end());
					usbBoardLinkSingleton()->m_pCR8062->ruotate180degree(vImage.data(), InfoImage.liWidth, InfoImage.liHeight);
					for ( auto it = vImage.begin(); it != vImage.end(); ++it )
					{
						(*it) *= DECIMAL_DIGITS_FACTOR;
					}

					uint8_t ucSlotNum = -1;
					uint8_t ucWellNum = -1;
					string strSample("Sample");
					auto pos = m_CameraParams.vPredefinedRoi[m_nCurrImageId].find(strSample);
					if ( pos == string::npos )
					{
						m_pLogger->log(LOG_ERR, "OpCaptureImage::performStateMachine: wrong predefined roy with TRAY pos [%s]",
								m_CameraParams.vPredefinedRoi[m_nCurrImageId].c_str());
						eState = eOperation::eRecover;
						break;
					}
					ucWellNum = (m_CameraParams.vPredefinedRoi[m_nCurrImageId][pos+strSample.size()]=='0') ? 0 : 3;

					strSample = strSample + to_string(ucWellNum) + "_";
					string strSection("");
					ucSlotNum = stoi(m_CameraParams.vPredefinedRoi[m_nCurrImageId].substr(pos+strSample.size()));

					enumParamsType eParamType = ( ! this->getUsage().compare("SERVICE") ) ? eFactoryParams : eUpdateParams;
					bRes = detector.analyzeZeroZero(vImage.data(), ucSlotNum, ucWellNum, eParamType);
					if ( ! bRes )
					{
						m_pLogger->log(LOG_ERR, "OpCaptureImage::performStateMachine: unable to apply sample detection algorithm");
						eState = eOperation::eRecover;
						break;
					}

					// Show calculated crop rectangle
					cropRectangle rect;
					detector.getCropRectangleZeroZero(&rect);
					// Just to see if is centered (but for the algo has to be shifted of 1 pixel)
					rect.xStart--;
					drawCropRectangle(m_vData[m_nCurrImageId], &InfoImage, rect, true);
				}

				// ROI is useful during calibration.
				if ( m_bEnableRoi[m_nCurrImageId] )
				{
					usbCameraSingleton()->showROI(m_vData[m_nCurrImageId], &InfoImage);
				}
				// add only if image is raw
				addBMPheader(&InfoImage, m_nCurrImageId);
				eState = eOperation::eEnd;
            }
            break;

            case eOperation::eRecover:
            {
                m_pLogger->log(LOG_WARNING, "OpCaptureImage::performStateMachine: trying to recover");
                msleep(4000);
                bool bRes = usbBoardLinkSingleton()->m_pCR8062->bringToLife();
                if ( bRes )
                {
                    m_pLogger->log(LOG_WARNING, "OpCaptureImage::performStateMachine: recover action performed");
                    // Adding this fake image the user understands something went wrong
                    m_vData[m_nCurrImageId].resize(PICTURE_DEF_ERR_SIZE);
                    fill(m_vData[m_nCurrImageId].begin(), m_vData[m_nCurrImageId].end(), 255); // fill of white
                    for ( int i = 0; i != PICTURE_DEF_ERR_SIZE/20; ++i )
                    {
                        m_vData[m_nCurrImageId].at(i) = 0;
                        m_vData[m_nCurrImageId].at(PICTURE_DEF_ERR_SIZE-1-i) = 0;
                    }
                    structInfoImage info = {200, 200, PICTURE_DEF_ERR_SIZE, eRAW};
                    addBMPheader(&info, m_nCurrImageId);
                }
                else
                {
                    m_pLogger->log(LOG_ERR, "OpCaptureImage::performStateMachine: unable to recover");
                }
                eState = eOperation::eEnd;
            }
            break;

            default:
				eState = eOperation::eEnd;
            break;
        }
    }

    m_nCurrImageId++;
    return 0;
}

bool OpCaptureImage::customization(int* liIll)
{
    if ( *liIll >= PICTURE_CUSTOM_ILL_THR )	return false;

    int liStepNum = *liIll / PICTURE_ILL_STEP_SIZE;

    switch (liStepNum)
    {
        case 0:
			*liIll = PICTURE_DEFAULT_ILL;
			m_bEnableRoi[m_nCurrImageId] = true;
			m_CameraParams.vLight[m_nCurrImageId] = PICTURE_DEFAULT_ILL;
        break;

		// Use last curves the AGC has in memory
		case 1:
			*liIll = PICTURE_ILL_VALUE_TO_SKIP_AGC;
			m_CameraParams.vLight[m_nCurrImageId] = PICTURE_DEFAULT_ILL;
		break;

		case 2: // for future use
        case 3:
        default:
			*liIll = PICTURE_DEFAULT_ILL;
			m_CameraParams.vLight[m_nCurrImageId] = PICTURE_DEFAULT_ILL;
        break;
    }

    return true;
}

int OpCaptureImage::addBMPheader(const structInfoImage* pImage, int liIdx)
{
    if ( pImage->eFormat != eRAW )							return -1;
    if ( pImage == nullptr )								return -1;
    if ( liIdx > int(m_vData.size()-1) )					return -1;

    /* ************************************************************************
     * Raw picture from the camera is:     What bmp image has to be like:
     *      0------------->                     ------------->n
     *      -------------->                     -------------->
     *      -------------->n                    0------------->
     * For this reason y-axis have to be inverted
     * ************************************************************************/
    swapY(m_vData[liIdx], pImage->liWidth, pImage->liHeight);

    // Take care of padding
    unsigned int uliBufLength = 0;
    int padBytes = 0;
    if ( pImage->liWidth % 4 != 0 )
    {
            padBytes = 4 - ( pImage->liWidth % 4 );
            uliBufLength = pImage->liHeight * (pImage->liWidth + padBytes);
    }

    unsigned char rgcBuff[BMP_HEADER_SIZE + PALETTE_LEN];
    BmpHeader sBmpHeader;
    BmpInfoHeader sBmpInfoHeader;

    sBmpHeader.magic[0] = MAGIC_MSB;
    sBmpHeader.magic[1] = MAGIC_LSB;
    sBmpHeader.creator1 = CREATOR1;
    sBmpHeader.creator2 = CREATOR2;
    sBmpHeader.bmp_offset = sizeof(sBmpHeader) + sizeof(sBmpInfoHeader) + PALETTE_LEN;
    sBmpHeader.filesz = sBmpHeader.bmp_offset + uliBufLength;
    sBmpInfoHeader.header_sz = sizeof(sBmpInfoHeader);
    sBmpInfoHeader.width = pImage->liWidth;
    sBmpInfoHeader.height = pImage->liHeight;
    sBmpInfoHeader.nplanes = 1;
    sBmpInfoHeader.bitspp = 8;
    sBmpInfoHeader.compress_type = 0;
    sBmpInfoHeader.bmp_bytesz = uliBufLength;
    sBmpInfoHeader.hres = HRES;
    sBmpInfoHeader.vres = VRES;
    sBmpInfoHeader.ncolors = 0;
    sBmpInfoHeader.nimpcolors = 0;

    memcpy(rgcBuff, &sBmpHeader, sizeof(sBmpHeader));
    memcpy(&rgcBuff[14], &sBmpInfoHeader, sizeof(sBmpInfoHeader));

    uint32_t uliValPalette = 0;
    uint32_t uliOffset = BMP_HEADER_SIZE;
    for ( int idx = 0; idx < PALETTE_NUM; ++idx )
    {
		uliValPalette = (idx << 16) + (idx << 8) + idx;
		memcpy(rgcBuff+uliOffset, &uliValPalette, sizeof(uint32_t));
		uliOffset += sizeof(uint32_t);
    }

    // add padding
    if ( padBytes != 0 )
    {
		vector<unsigned char> vPadding(padBytes);
		for ( int i = 0; i < pImage->liHeight; ++i )
		{
			auto idx = m_vData[liIdx].begin() + i*(pImage->liWidth+padBytes) + pImage->liWidth;
			m_vData[liIdx].insert(idx, vPadding.begin(), vPadding.end());
		}
    }
    m_vData[liIdx].insert(m_vData[liIdx].begin(), rgcBuff, rgcBuff+BMP_HEADER_SIZE+PALETTE_LEN);

    return 0;
}

// tail recursion, no stack allocated
void OpCaptureImage::swapY(vector<unsigned char>& vImage, int liImageWidth, int liImageHeight, int liTimes)
{
    if ( liTimes >= liImageHeight/2)    return;

    auto begin = vImage.begin() + liTimes * liImageWidth;
    auto end = begin + liImageWidth;
    auto beginLast = vImage.end() - liImageWidth*(liTimes+1);
    swap_ranges(begin, end, beginLast);

    return swapY(vImage, liImageWidth, liImageHeight, ++liTimes);
}

int OpCaptureImage::compileSendOutCmd()
{
    // set the status as SUCCESFULL so that the reply is composed with all the parameters
    OutCmdCaptureImage cmd;
    cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
    cmd.setUsage(getUsage());
    cmd.setID(getID());

    CaptureImagePayload* pCaptureImagePayload = new CaptureImagePayload();
    pCaptureImagePayload->setGetPictureAttr(1, m_CameraParams, m_vData);

    // Assign the payload to the outcommand to let the serializer will extract the info
    cmd.setPayload((Payload*)pCaptureImagePayload);

    /* ********************************************************************************************
     * Send the OutgoingCommand to the client
     * ********************************************************l***********************************
     */
    string strOutCommand;
    bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
    if ( ! bSerializeResult )
    {
		m_pLogger->log(LOG_ERR, "OutCmdCaptureImage::execute--> unable to serialize OutCmdCaptureImage");
    }
    else
    {
		sendCommandReply(strOutCommand);
    }
	return eOperationEnabledForDeath;
}

int OpCaptureImage::drawCropRectangle(vector<unsigned char>& vImage, const structInfoImage* pImage, cropRectangle rect, bool bRuotate)
{
	if ( pImage == nullptr )									return -1;
	if ( int(rect.xStart + rect.width) > pImage->liWidth )		return -1;
	if ( int(rect.yStart + rect.height) > pImage->liHeight )	return -1;

	// If the rectangle has to be ruotated, than modify cropRectangle
	if ( bRuotate )
	{
		rect.xStart = pImage->liWidth  - ( rect.xStart + rect.width  );
		rect.yStart = pImage->liHeight - ( rect.yStart + rect.height );
	}

	int def1 = rect.yStart * pImage->liWidth + rect.xStart;
	int def2 = (rect.yStart+rect.height)*pImage->liWidth + rect.xStart;
	size_t liSize = vImage.size();
	for ( uint i = 0; i <= rect.width; ++i )
	{
		if ( def2+i > liSize )	break;
		vImage[def1+i] = WHITE;
		vImage[def2+i] = WHITE;
	}

	for ( uint i = 0; i < rect.height; ++i )
	{
		size_t foo = (rect.yStart+i)*pImage->liWidth + rect.xStart + rect.width;
		if ( foo > liSize )	break;

		vImage[(rect.yStart+i)*pImage->liWidth + rect.xStart] = WHITE;
		vImage[foo] = WHITE;
	}

	// Draw target
	for ( int i = 0; i < pImage->liHeight; ++i )
	{
		if ( i > int(rect.yStart) && i < int(rect.yStart+rect.height) )	continue;
		vImage[i*pImage->liWidth+rect.xStart+rect.width/2] = WHITE;
	}

	for ( int i = 0; i < pImage->liWidth; ++i )
	{
		if ( i > int(rect.xStart) && i < int(rect.xStart+rect.width) )	continue;
		vImage[(rect.yStart+rect.height/2)*pImage->liWidth+i+1] = WHITE;
	}

	return 0;
}
