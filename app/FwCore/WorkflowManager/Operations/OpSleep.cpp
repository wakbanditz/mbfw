/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSleep.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSleep class.
 @details

 ****************************************************************************
*/

#include "OpSleep.h"
#include "OutCmdSleep.h"
#include "SleepPayload.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardManager.h"
#include "SectionBoardGeneral.h"
#include "CommonInclude.h"

OpSleep::OpSleep() : Operation("OpSleep")
{
	m_bSleep = false;
}

OpSleep::~OpSleep()
{
}

int OpSleep::perform(void)
{
	int res;

	res = eOperationWaitForDeath;

	if(m_bSleep)
	{
		// enter sleep mode -> send command to sections

		if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) != eStatusDisable)
		{
			if(sectionBoardGeneralSingleton(SCT_A_ID)->sleepOrDisable(SLEEP_DISABLE_ENABLE, SET_SLEEP) == -1)
				res = eOperationError;
		}
		if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) != eStatusDisable)
		{
			if(sectionBoardGeneralSingleton(SCT_B_ID)->sleepOrDisable(SLEEP_DISABLE_ENABLE, SET_SLEEP) == -1)
				res = eOperationError;
		}
	}
	else
	{
		// exit sleep mode -> send command to sections

		if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) != eStatusDisable)
		{
			if(sectionBoardGeneralSingleton(SCT_A_ID)->sleepOrDisable(SLEEP_DISABLE_DISABLE, SET_SLEEP) == -1)
				res = eOperationError;
		}
		if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) != eStatusDisable)
		{
			if(sectionBoardGeneralSingleton(SCT_B_ID)->sleepOrDisable(SLEEP_DISABLE_DISABLE, SET_SLEEP) == -1)
				res = eOperationError;
		}

		if(stateMachineSingleton()->getDeviceStatus(eIdNsh) != eStatusDisable)
		{
			//  !!!!! TODO
		}
	}

	restoreStateMachine();

	OutCmdSleep outCmdSleep;
	string strOutCommand;

	outCmdSleep.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdSleep.setUsage(getUsage());
	outCmdSleep.setID(getID());

	// compose the payload ...
	SleepPayload * pSleepPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	outCmdSleep.setPayload((Payload *)pSleepPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	bool bProtocolSerialize = protocolSerialize(&outCmdSleep, strOutCommand);

	if (bProtocolSerialize == false)
	{
		m_pLogger->log(LOG_ERR, "OpSleep:: unable to serialize SleepOut cmd");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	if(m_bSleep == false)
	{
		m_pLogger->log(LOG_INFO, "OpSleep:: going to reinit");
		// delay to let the sections reboot
		sleep(10);
		// start the reinit procedure
		MainExecutor::getInstance()->reinitInstrument();
	}

	return res;
}

void OpSleep::setSleepActive(bool status)
{
	m_bSleep = status;
}

SleepPayload* OpSleep::buildPayload()
{
	SleepPayload * pSleepPayload = new SleepPayload();

	pSleepPayload->setSleepActive(m_bSleep);

	return pSleepPayload;
}

