/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetParameter.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSetParameter class.
 @details

 ****************************************************************************
*/

#ifndef OPSETPARAMETER_H
#define OPSETPARAMETER_H


#include "Operation.h"
#include "Payload.h"

class SetParameterPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SETPARAMETER command
 * ********************************************************************************************************************
 */
class OpSetParameter : public Operation
{

	public:
		/*! ***********************************************************************************************************
		 * @brief OpSetParameter default constructor
		 * ***********************************************************************************************************
		 */
		OpSetParameter();

		/*! ***********************************************************************************************************
		 * @brief ~OpSetParameter default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpSetParameter();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

		/*! *************************************************************************************************
		 * @brief DATA SETTERS
		 * **************************************************************************************************
		 */
		void setComponent(std::string strValue);
		void setMotor(std::string strValue);
		void setMicrostep(std::string strValue);
		void setConversion(std::string strValue);
		void setHighCurrent(std::string strValue);
		void setLowCurrent(std::string strValue);
		void setAcceleration(std::string strValue);

	private:

		/*! ************************************************************************************************************
		 * @brief buildPayload	Fills the SetParameterPayload of the outgoing command with related values.
		 * @return  pointer to the SetParameterPayload object just allocated
		 * *************************************************************************************************************
		 */
		SetParameterPayload * buildPayload(void);

		/*! ************************************************************************************************************
		 * @brief executeSetMotorSection	set the current of the Section's motor
		 * @param ubNumSection number of the section of interest
		 * @param setting to identify the parameter to be set
		 * @param strSetting the value of the parameter as string
		 * @return  0 in case of success, -1 otherwise
		 * *************************************************************************************************************
		 */
		int executeSetMotorSection(uint8_t ubNumSection, uint8_t setting, std::string strSetting);

		/*! ************************************************************************************************************
		 * @brief executeSetMotorSection	set the current of the NSH's motor
		 * @param setting to identify the parameter to be set
		 * @param strSetting the value of the parameter as string
		 * @return  0 in case of success, -1 otherwise
		 * *************************************************************************************************************
		 */
		int executeSetMotorNSH(uint8_t setting, std::string strSetting);

    private:
        std::string m_strComponent, m_strMotor;
        std::string m_strMicrostep, m_strHighCurrent, m_strLowCurrent;
        std::string m_strAcceleration, m_strConversion;

};

#endif // OPSETPARAMETER_H
