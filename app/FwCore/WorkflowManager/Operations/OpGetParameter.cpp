/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetParameter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetParameter class.
 @details

 ****************************************************************************
*/

#include "OpGetParameter.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "OutCmdGetParameter.h"
#include "GetParameterPayload.h"
#include "SectionBoardManager.h"
#include "CommonInclude.h"


OpGetParameter::OpGetParameter(const string &strComponent, const string &strMotor)	:
	Operation("GetParameter")
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strMicrostepResolution.clear();
	m_strAccelerationFactor.clear();
	m_strOperatingCurrent.clear();
	m_strStandbyCurrent.clear();
	m_strStepConversionFactor.clear();

	m_counterAction = 0;
	m_bAskBoards = false;

	// build the vectors of actions according to the Component / Motor coming from the command
	std::vector<std::string> listMotorSection, listMotorNsh;

	if( (strMotor.compare(SECTION_PUMP_STRING) == 0) ||
		(strMotor.compare(SECTION_TOWER_STRING) == 0) ||
		(strMotor.compare(SECTION_TRAY_STRING) == 0) ||
		(strMotor.compare(SECTION_SPR_STRING) == 0)   )
	{
		// section devices
		listMotorSection.push_back(strMotor);
	}
	else if( strMotor.compare(DEVICE_NSH_STRING) == 0)
	{
		// nsh device
		listMotorNsh.push_back(strMotor);
	}
	else if( strMotor.empty())
	{
		// all devices
		listMotorSection.push_back(SECTION_PUMP_STRING);
		listMotorSection.push_back(SECTION_TOWER_STRING);
		listMotorSection.push_back(SECTION_TRAY_STRING);
		listMotorSection.push_back(SECTION_SPR_STRING);
		listMotorNsh.push_back(DEVICE_NSH_STRING);
	}

	if(strComponent.empty())
	{
		// in this case the request is on all components
		// if a device is disabled we exclude it from the action
		// build the vectors associating to the Component the corresponding motors
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
		{
			for(uint8_t i = 0; i < listMotorSection.size(); i++)
			{
				m_strComponent.push_back(XML_TAG_SECTIONA_NAME);
				m_strMotor.push_back(listMotorSection.at(i));
				m_strMicrostepResolution.push_back("");
				m_strAccelerationFactor.push_back("");
				m_strOperatingCurrent.push_back("");
				m_strStandbyCurrent.push_back("");
				m_strStepConversionFactor.push_back("");
			}
		}
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
		{
			for(uint8_t i = 0; i < listMotorSection.size(); i++)
			{
				m_strComponent.push_back(XML_TAG_SECTIONB_NAME);
				m_strMotor.push_back(listMotorSection.at(i));
				m_strMicrostepResolution.push_back("");
				m_strAccelerationFactor.push_back("");
				m_strOperatingCurrent.push_back("");
				m_strStandbyCurrent.push_back("");
				m_strStepConversionFactor.push_back("");
			}
		}
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true)
		{
			for(uint8_t i = 0; i < listMotorNsh.size(); i++)
			{
				m_strComponent.push_back(XML_TAG_NSH_NAME);
				m_strMotor.push_back(listMotorNsh.at(i));
				m_strMicrostepResolution.push_back("");
				m_strAccelerationFactor.push_back("");
				m_strOperatingCurrent.push_back("");
				m_strStandbyCurrent.push_back("");
				m_strStepConversionFactor.push_back("");
			}
		}
	}
	else if (strComponent.compare(XML_TAG_SECTIONA_NAME) == 0)
	{
		for(uint8_t i = 0; i < listMotorSection.size(); i++)
		{
			m_strComponent.push_back(XML_TAG_SECTIONA_NAME);
			m_strMotor.push_back(listMotorSection.at(i));
			m_strMicrostepResolution.push_back("");
			m_strAccelerationFactor.push_back("");
			m_strOperatingCurrent.push_back("");
			m_strStandbyCurrent.push_back("");
			m_strStepConversionFactor.push_back("");
		}
	}
	else if (strComponent.compare(XML_TAG_SECTIONB_NAME) == 0)
	{
		for(uint8_t i = 0; i < listMotorSection.size(); i++)
		{
			m_strComponent.push_back(XML_TAG_SECTIONB_NAME);
			m_strMotor.push_back(listMotorSection.at(i));
			m_strMicrostepResolution.push_back("");
			m_strAccelerationFactor.push_back("");
			m_strOperatingCurrent.push_back("");
			m_strStandbyCurrent.push_back("");
			m_strStepConversionFactor.push_back("");
		}
	}
	else if (strComponent.compare(XML_TAG_NSH_NAME) == 0)
	{
		for(uint8_t i = 0; i < listMotorNsh.size(); i++)
		{
			m_strComponent.push_back(XML_TAG_NSH_NAME);
			m_strMotor.push_back(listMotorNsh.at(i));
			m_strMicrostepResolution.push_back("");
			m_strAccelerationFactor.push_back("");
			m_strOperatingCurrent.push_back("");
			m_strStandbyCurrent.push_back("");
			m_strStepConversionFactor.push_back("");
		}
	}
}

OpGetParameter::~OpGetParameter()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strMicrostepResolution.clear();
	m_strAccelerationFactor.clear();
	m_strOperatingCurrent.clear();
	m_strStandbyCurrent.clear();
	m_strStepConversionFactor.clear();
}

int OpGetParameter::perform()
{
	int8_t res = -1;

	if(m_strComponent.size() == 0)
	{
		m_pLogger->log(LOG_WARNING, "OpGetParameter::perform-->Error %d", m_strComponent.size());
		// operation completed -> send reply and close it
		closeOperation();
		return eOperationRunning;
	}

	if(m_bAskBoards == false)
	{
		// read data from memory
		for(m_counterAction = 0; m_counterAction < m_strComponent.size(); m_counterAction++)
		{


#ifdef __DEBUG_PROTOCOL
			m_strMicrostepResolution.at(m_counterAction).assign("1");
			m_strAccelerationFactor.at(m_counterAction).assign("0");
			m_strStandbyCurrent.at(m_counterAction).assign("3");
			m_strOperatingCurrent.at(m_counterAction).assign("4");
			m_strStepConversionFactor.at(m_counterAction).assign("12345");
			continue;
#endif


			uint8_t section = 0xFF;

			if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONA_NAME) == 0)
			{
				section = SCT_A_ID;
			}
			else if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONB_NAME) == 0)
			{
				section = SCT_B_ID;
			}
			else if(m_strComponent.at(m_counterAction).compare(XML_TAG_NSH_NAME) == 0)
			{
				section = NSH_ID;
			}

			uint8_t  ubMicroRes = 0;
			uint16_t usStandbyCurr = 0;
			uint16_t usOperatingCurr = 0;
			uint8_t  ubStepConvFactor = 0;
			uint32_t ulMotorConvFactor = 0;

			if(section == SCT_A_ID || section == SCT_B_ID)
			{
				SectionCommonMessage*	pCommonMessage = NULL;

				//choice of the component to move
				if (m_strMotor.at(m_counterAction).compare(SECTION_TOWER_STRING) == 0)
				{
					pCommonMessage = &sectionBoardLinkSingleton(section)->m_pTower->m_CommonMessage;
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_PUMP_STRING) == 0)
				{
					pCommonMessage = &sectionBoardLinkSingleton(section)->m_pPump->m_CommonMessage;
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_TRAY_STRING) == 0)
				{
					pCommonMessage = &sectionBoardLinkSingleton(section)->m_pTray->m_CommonMessage;
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_SPR_STRING) == 0)
				{
					pCommonMessage = &sectionBoardLinkSingleton(section)->m_pSPR->m_CommonMessage;
				}

				if(pCommonMessage != NULL)
				{
                    // the section replies '1', '2', etc so the value is converted to number 1, 2
                    ubMicroRes = pCommonMessage->readMotorMicrosteps();
                    if(ubMicroRes >= ZERO_CHAR) ubMicroRes -= ZERO_CHAR;

                    usStandbyCurr = pCommonMessage->readMotorLowCurrent();
                    if(usStandbyCurr >= A_CHAR) usStandbyCurr = (usStandbyCurr - A_CHAR + 10);
                    else if(usStandbyCurr >= ZERO_CHAR) usStandbyCurr -= ZERO_CHAR;

                    usOperatingCurr = pCommonMessage->readMotorHighCurrent();
                    if(usOperatingCurr >= A_CHAR) usOperatingCurr = (usOperatingCurr - A_CHAR + 10);
                    else if(usOperatingCurr >= ZERO_CHAR) usOperatingCurr -= ZERO_CHAR;

                    ubStepConvFactor = pCommonMessage->readMotorRampFactor();
                    if(ubStepConvFactor >= ZERO_CHAR) ubStepConvFactor -= ZERO_CHAR;

                    ulMotorConvFactor = pCommonMessage->readMotorFactor();
                }
			}
			else if(section == NSH_ID)
			{
				if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true)
				{
					ubMicroRes = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->readMotorResolution();
					usStandbyCurr = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->readMotorLowCurrent();
					usOperatingCurr = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->readMotorHighCurrent();
					ubStepConvFactor = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->readConversionFactor();
					ulMotorConvFactor = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->readAccelerationFactor();
				}
			}
			m_strMicrostepResolution.at(m_counterAction).assign(to_string(ubMicroRes));
			m_strAccelerationFactor.at(m_counterAction).assign(to_string(ubStepConvFactor));
			m_strStandbyCurrent.at(m_counterAction).assign(to_string(usStandbyCurr));
			m_strOperatingCurrent.at(m_counterAction).assign(to_string(usOperatingCurr));
			m_strStepConversionFactor.at(m_counterAction).assign(to_string(ulMotorConvFactor));
		}
		// send back the final reply
		compileSendOutCmd();
		// end of story !
		return eOperationWaitForDeath;
	}


	// start the action on the current item of the vector

	if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONA_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONB_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_strComponent.at(m_counterAction).compare(XML_TAG_NSH_NAME) == 0)
	{
		res = nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
	}
	else if(m_strComponent.at(m_counterAction).compare(XML_TAG_CAMERA_NAME) == 0) // to be deleted
	{
		res = -2; // just to enter in closeOperation
	}

	if(res != eOperationRunning)
	{
		m_pLogger->log(LOG_WARNING, "OpGetParameter::perform-->Error %s", m_strComponent.at(m_counterAction).c_str());
		// operation completed -> send reply and close it
		closeOperation();
	}

//	return res; // to be checked
	return eOperationRunning;
}

GetParameterPayload * OpGetParameter::buildPayload(void)
{
	GetParameterPayload * pGetParameterPayload = new GetParameterPayload();

	pGetParameterPayload->setOutValues( m_strComponent, m_strMotor,
										m_strMicrostepResolution,
										m_strAccelerationFactor,
										m_strOperatingCurrent,
										m_strStandbyCurrent,
										m_strStepConversionFactor);

	return pGetParameterPayload;
}

int OpGetParameter::executeGetSectionParameter(uint8_t ubNumSection)
{
	SectionCommonMessage*	pCommonMessage = NULL;

	//choice of the component to move
	if (m_strMotor.at(m_counterAction).compare(SECTION_TOWER_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTower->m_CommonMessage;
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_PUMP_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pPump->m_CommonMessage;
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_TRAY_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTray->m_CommonMessage;
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_SPR_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pSPR->m_CommonMessage;
	}

	uint8_t  ubMicroRes = 0;
	uint16_t usStandbyCurr = 0;
	uint16_t usOperatingCurr = 0;
	uint8_t  ubStepConvFactor = 0;

	pCommonMessage->getMotorParameters(ubMicroRes,usStandbyCurr, usOperatingCurr, ubStepConvFactor);

    // the section replies '1', '2', etc so the value is converted to number 1, 2
    if(ubMicroRes >= ZERO_CHAR) ubMicroRes -= ZERO_CHAR;

    if(usStandbyCurr >= A_CHAR) usStandbyCurr = (usStandbyCurr - A_CHAR + 10);
    else if(usStandbyCurr >= ZERO_CHAR) usStandbyCurr -= ZERO_CHAR;

    if(usOperatingCurr >= A_CHAR) usOperatingCurr = (usOperatingCurr - A_CHAR + 10);
    else if(usOperatingCurr >= ZERO_CHAR) usOperatingCurr -= ZERO_CHAR;

    if(ubStepConvFactor >= ZERO_CHAR) ubStepConvFactor -= ZERO_CHAR;

    m_strMicrostepResolution.at(m_counterAction).assign(to_string(ubMicroRes));
	m_strAccelerationFactor.at(m_counterAction).assign(to_string(ubStepConvFactor));
	m_strStandbyCurrent.at(m_counterAction).assign(to_string(usStandbyCurr));
	m_strOperatingCurrent.at(m_counterAction).assign(to_string(usOperatingCurr));

	uint32_t ulMotorConvFactor = 0;
	pCommonMessage->getMotorConversionFactor(ulMotorConvFactor);
    m_strStepConversionFactor.at(m_counterAction).assign(to_string(ulMotorConvFactor));

	return 0;
}

int OpGetParameter::executeGetNSHParameter(void)
{
	uint8_t ucMicroRes = 0;
	uint8_t ucStandbyCurr = 0;
	uint8_t ucOperatingCurr = 0;
	uint8_t ucAccMode = 0;
	uint32_t usFactor = 0;

	int8_t cRes = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getStillMotorCurrent(ucStandbyCurr);
	if ( cRes )
	{
		m_pLogger->log(LOG_ERR, "OpGetParameter::executeGetNSHParameter: Unable to get hold motor current");
		return -1;
	}

	cRes = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getRunningMotorCurrent(ucOperatingCurr);
	if ( cRes )
	{
		m_pLogger->log(LOG_ERR, "OpGetParameter::executeGetNSHParameter: Unable to get running motor current");
		return -1;
	}

	cRes = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getStepResolution(ucMicroRes);
	if ( cRes )
	{
		m_pLogger->log(LOG_ERR, "OpGetParameter::executeGetNSHParameter: Unable to get step resolution");
		return -1;
	}

	ucAccMode = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->readAccelerationFactor();
	usFactor = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->readConversionFactor();

	m_strMicrostepResolution.at(m_counterAction).assign(to_string(ucMicroRes));
	m_strAccelerationFactor.at(m_counterAction).assign(to_string(ucAccMode));
	m_strStandbyCurrent.at(m_counterAction).assign(to_string(ucStandbyCurr));
	m_strOperatingCurrent.at(m_counterAction).assign(to_string(ucOperatingCurr));
	m_strStepConversionFactor.at(m_counterAction).assign(to_string(usFactor));

	return 0;
}

int OpGetParameter::executeCmd( int lNum )
{
	//action to perform
	int res = 0;

	if( lNum == NSH_ID )
	{
		res = executeGetNSHParameter();
	}
	else if ( m_strComponent.at(m_counterAction).compare(XML_TAG_CAMERA_NAME) )
	{

	}
	else
	{
		res = executeGetSectionParameter(lNum);
	}

	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "OpGetParameter::execute-->execute not performed");
		// operation completed -> send reply and close it
		closeOperation();
		return -1;
	}

	std::string previousComponent;
	previousComponent.assign(m_strComponent.at(m_counterAction));

	// increase the action counter
	m_counterAction++;

	// if we have performed the last action compose and send the reply, stop the loop
	if(m_counterAction >= m_strComponent.size() )
	{
		// operation completed -> send reply and close it
		closeOperation();
		res = -1;
	}
	else if(previousComponent.compare(m_strComponent.at(m_counterAction)) != 0)
	{
		// actions on the current device are completed:
		// --> go for next action
		perform();
		// -> unlock the actual
		res = -1;
	}

	m_pLogger->log(LOG_INFO, "OpGetParameter::execute-->completed");

	return res;
}

int OpGetParameter::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdGetParameter cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	GetParameterPayload * pGetParameterPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pGetParameterPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpGetParameter::execute--> unable to serialize OutCmdGetParameter");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}
