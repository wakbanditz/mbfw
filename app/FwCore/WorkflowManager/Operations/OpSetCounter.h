/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetCounter.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSetCounter class.
 @details

 ****************************************************************************
*/

#ifndef OPSETCOUNTER_H
#define OPSETCOUNTER_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SETCOUNTER command
 * ********************************************************************************************************************
 */
class OpSetCounter : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpSetCounter default constructor
		 * ***********************************************************************************************************
		 */
		OpSetCounter();

		/*! ***********************************************************************************************************
		 * @brief ~OpSetCounter default destructor
		 * ************************************************************************************************************
		 */
		~OpSetCounter();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! *************************************************************************************************
		 * @brief setCounter	Sets the name of the counter
		 * @param strCounter the label string
		 * *************************************************************************************************
		 */
		void setCounter(const string& strCounter);

		/*! *************************************************************************************************
		 * @brief setValue	Sets the value of the counter
		 * @param value the integer value
		 * *************************************************************************************************
		 */
		void setValue(uint16_t value);

	private :

		std::string m_strCounter;
		uint16_t m_intValue;
};


#endif // OPSETCOUNTER_H
