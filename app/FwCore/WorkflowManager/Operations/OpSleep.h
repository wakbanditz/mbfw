/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSleep.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSleep class.
 @details

 ****************************************************************************
*/

#ifndef OPSLEEP_H
#define OPSLEEP_H

#include "Operation.h"

class SleepPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SLEEP command
 * ********************************************************************************************************************
 */

class OpSleep : public Operation
{

	public:

		/*! ***********************************************************************************************************
		 * @brief OpSleep default constructor
		 * ***********************************************************************************************************
		 */
		OpSleep();

		/*! ***********************************************************************************************************
		 * @brief ~OpSleep default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpSleep();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief setSleepActive	set the action to perform
		 * @param status true -> goto sleep, false -> exit from sleep
		 * ************************************************************************************************************
		 */
		void setSleepActive(bool status);

	private:
		/*!
		 * @brief buildPayload	Fills the SleepPayload of the outgoing command with related values.
		 * @return  pointer to the SleepPayload object just allocated
		 */
		SleepPayload * buildPayload(void);

	private:

		bool m_bSleep;

};

#endif // OPSLEEP_H
