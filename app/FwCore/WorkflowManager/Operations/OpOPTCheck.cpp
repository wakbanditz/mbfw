/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpOPTCheck.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpOPTCheck class.
 @details

 ****************************************************************************
*/

#include "OpOPTCheck.h"
#include "MainExecutor.h"
#include "SectionBoardTray.h"
#include "WebServerAndProtocolInclude.h"
#include "OPTCheckPayload.h"
#include "OutCmdOPTCheck.h"

#define DEFAULT_AIR_RFU_CAL_VALUE		5
#define TRAY_READ_CODED_COMMAND			"Read"
#define TRAY_OUT_CODED_COMMAND			"Out"

OpOPTCheck::OpOPTCheck() : Operation("OpOPTCheck")
{
    m_eOptState = eState::eStart;
    m_intTarget = 0;
    m_intTolerance = 0;

    uint8_t i, j;

    for(i = SCT_A_ID; i < SCT_NUM_TOT_SECTIONS; i++)
    {
        m_Read[i] = 0;
        for(j = 0; j < SCT_NUM_TOT_SLOTS; j++)
        {
            // default value -> not to be read
            m_stsSectionSlotEnable[i][j] = -1;
        }
    }
}

OpOPTCheck::~OpOPTCheck()
{
    /* Nothing to do yet */
}

void OpOPTCheck::setParameters(int target, int tolerance)
{
    m_intTarget = target;
    m_intTolerance = tolerance;
}

void OpOPTCheck::setSlot(uint8_t section, uint8_t slot)
{
    if(section >= SCT_NUM_TOT_SECTIONS) return;
    if(slot >= SCT_NUM_TOT_SLOTS) return;

    // to be read
    m_stsSectionSlotEnable[section][slot] = 0;
}

bool OpOPTCheck::isSectionEnabled(uint8_t section)
{
    if(section >= SCT_NUM_TOT_SECTIONS) return false;

    for(uint8_t j = 0; j < SCT_NUM_TOT_SLOTS; j++)
    {
        // default value -> not to be read
        if(m_stsSectionSlotEnable[section][j] != -1)
        {
            return true;
        }
    }
    return false;
}

int OpOPTCheck::perform()
{
    int res = eOperationError;

    switch	( m_eOptState )
    {
        case eState::eStart:
        case eState::eExecute:
        case eState::eNshError:
        case eState::eClean:
            res = nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
        break;

        case eState::eMoveToTrayReadA:
        case eState::eMoveToTrayOutA:
        case eState::eSectErrorA:
            res = sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
        break;

        case eState::eMoveToTrayReadB:
        case eState::eMoveToTrayOutB:
        case eState::eSectErrorB:
            res = sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
        break;


        case eState::eStop:
        {
            restoreStateMachine();

            // before closing the operation (but after the restore !)
            // check if errors are still active for the device
            // (the OPT_CHECK is able to set or clear errors)
            if(spiBoardLinkSingleton()->m_pSPIErr->checkErrors() == false)
            {
                // no more errors on NSH: verify also the Sections (to reset the LED color if necessary)
                checkErrorsOnSection(SCT_A_ID);
                checkErrorsOnSection(SCT_B_ID);
            }

            compileSendOutCmd();

            operationSingleton()->removeOperation(this);
            res = eOperationWaitForDeath;
        }
        break;
    }

    return res;
}

int OpOPTCheck::executeCmd(int lNum)
{
    if ( lNum > SCT_NONE_ID )	return -1;

    eDeviceId eCurrDevice = eIdMax;
    eDeviceId eNextDevice = eIdMax;

    while ( eCurrDevice == eNextDevice && m_eOptState != eState::eStop )
    {
        switch ( m_eOptState )
        {
            case eState::eStart:
                eCurrDevice = eIdNsh;
                // select the section to move and so the corresponding state
                if(isSectionEnabled(SCT_A_ID))
                {
                    eNextDevice = eIdSectionA;
                    m_eOptState = eState::eMoveToTrayReadA;
                }
                else
                {
                    eNextDevice = eIdSectionB;
                    m_eOptState = eState::eMoveToTrayReadB;
                }

            break;

            case eState::eMoveToTrayReadA:
            case eState::eMoveToTrayReadB:
            {
                if(m_eOptState == eState::eMoveToTrayReadA)
                {
                    eCurrDevice = eIdSectionA;
                }
                else
                {
                    eCurrDevice = eIdSectionB;
                }
                eNextDevice = eIdNsh;

                string strCoded("");
                strCoded = sectionBoardLinkSingleton(lNum)->m_pTray->getCodedFromLabel(TRAY_READ_CODED_COMMAND);
                if ( strCoded.empty() )
                {
                    m_eOptState = eState::eNshError;
                    m_pLogger->log(LOG_ERR, "OpOPTCheck::executeCmd: unable to get coded position from tray read label [" TRAY_READ_CODED_COMMAND "]");
                    break;
                }

                char ubPosCode = *strCoded.c_str();
                m_pLogger->log(LOG_INFO, "OpOPTCheck::executeCmd: move tray to pos predefined: CMD= %X", ubPosCode);
                int liRes = sectionBoardLinkSingleton(lNum)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
                if ( liRes )
                {
                    m_eOptState = eState::eNshError;
                    m_pLogger->log(LOG_ERR, "OpOPTCheck::executeCmd: unable to move to coded position");
                    break;
                }

                if(m_eOptState == eState::eMoveToTrayReadB)
                {
                    m_eOptState = eState::eExecute;
                    eNextDevice = eIdNsh;
                }
                else if(isSectionEnabled(SCT_B_ID))
                {
                    m_eOptState = eState::eMoveToTrayReadB;
                    eNextDevice = eIdSectionB;
                }
                else
                {
                    m_eOptState = eState::eExecute;
                    eNextDevice = eIdNsh;
                }
            }
            break;

            case eState::eExecute:
            {
                eCurrDevice = eIdNsh;

                fillPositionsVector();

                int liRes = 0;
                for (int i = 0; i < (int)m_vPositions.size(); i++)
                {
                    liRes = nshBoardManagerSingleton()->moveNSHandRead(stoi(m_vPositions[i]), m_vRFUvalues[i]);
                    if ( liRes )
                    {
                        eNextDevice = eIdNsh;
                        m_eOptState = eState::eNshError;
                        m_pLogger->log(LOG_ERR, "OpOPTCheck::executeCmd: Impossible to move NSH and read the tray");
                        break;
                    }
                }

                if(isSectionEnabled(SCT_A_ID))
                {
                    m_eOptState = eState::eMoveToTrayOutA;
                    eNextDevice = eIdSectionA;
                }
                else
                {
                    m_eOptState = eState::eMoveToTrayOutB;
                    eNextDevice = eIdSectionB;
                }
            }
            break;

            case eState::eMoveToTrayOutA:
            case eState::eMoveToTrayOutB:
            {
                if(m_eOptState == eState::eMoveToTrayOutA)
                {
                    eCurrDevice = eIdSectionA;
                }
                else
                {
                    eCurrDevice = eIdSectionB;
                }
                eNextDevice = eIdNsh;

                string strCoded("");
                strCoded = sectionBoardLinkSingleton(lNum)->m_pTray->getCodedFromLabel(TRAY_OUT_CODED_COMMAND);
                if ( strCoded.empty() )
                {
                    m_eOptState = eState::eNshError;
                    m_pLogger->log(LOG_ERR, "OpOPTCheck::executeCmd: unable to get coded position from tray out label [" TRAY_OUT_CODED_COMMAND "]");
                    break;
                }

                char ubPosCode = *strCoded.c_str();
                m_pLogger->log(LOG_DEBUG, "OpOPTCheck::executeCmd: move tray to pos predefined: CMD= %X", ubPosCode);
                int liRes = sectionBoardLinkSingleton(lNum)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
                if ( liRes )
                {
                    m_eOptState = eState::eNshError;
                    m_pLogger->log(LOG_ERR, "OpOPTCheck::executeCmd: unable to perform tray read");
                    break;
                }

                if(m_eOptState == eState::eMoveToTrayOutB)
                {
                    m_eOptState = eState::eClean;
                    eNextDevice = eIdNsh;
                }
                else if(isSectionEnabled(SCT_B_ID))
                {
                    m_eOptState = eState::eMoveToTrayOutB;
                    eNextDevice = eIdSectionB;
                }
                else
                {
                    m_eOptState = eState::eClean;
                    eNextDevice = eIdNsh;
                }
            }
            break;

            case eState::eNshError:
            {
                eCurrDevice = eIdNsh;
                // Move machine to default position
                MainExecutor::getInstance()->m_SPIBoard.move(eHome);

                if(isSectionEnabled(SCT_A_ID))
                {
                    eNextDevice = eIdSectionA;
                    m_eOptState = eState::eSectErrorA;
                }
                else
                {
                    eNextDevice = eIdSectionB;
                    m_eOptState = eState::eSectErrorB;
                }
            }
            break;

            case eState::eSectErrorA:
            case eState::eSectErrorB:
            {
                m_eOptState = eState::eStop;
                string strCoded("");
                strCoded = sectionBoardLinkSingleton(lNum)->m_pTray->getCodedFromLabel(TRAY_OUT_CODED_COMMAND);
                if ( strCoded.empty() )	break;

                char ubPosCode = *strCoded.c_str();
                sectionBoardLinkSingleton(lNum)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);

                if(m_eOptState == eState::eSectErrorB)
                {
                    m_eOptState = eState::eStop;
                    eNextDevice = eIdNsh;
                }
                else if(isSectionEnabled(SCT_B_ID))
                {
                    m_eOptState = eState::eSectErrorB;
                    eNextDevice = eIdSectionB;
                }
                else
                {
                    m_eOptState = eState::eStop;
                    eNextDevice = eIdNsh;
                }
            }
            break;

            case eState::eClean:
            {
                m_eOptState = eState::eStop;

                string strPos("");
                string strCodedName("SS");
                nshBoardManagerSingleton()->m_pConfig->getItemValue(strCodedName, strPos);
                int liPos = stoi(strPos);
                int bRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
                if ( ! bRes )
                {
                    m_pLogger->log(LOG_ERR, "OpOPTCheck::executeCmd: unable to move NSH motor to desired position");
                    break;
                }

                // finally let's make some calculations .....
                uint8_t i, j, k, n;
                for(k = 0, i = SCT_A_ID; i < SCT_NUM_TOT_SECTIONS; i++)
                {
                    // sum all the readings on the slots of the section ....
                    m_Read[i] = 0;
                    for(j = 0, n = 0; j < SCT_NUM_TOT_SLOTS; j++)
                    {
                        // check if slot has been read
                        if(m_stsSectionSlotEnable[i][j] == 0)
                        {
                            m_Read[i] += m_vRFUvalues[k];
                            k++;
                            n++;
                        }
                    }
                    if(m_Read[i] > 0)
                    {
                        // .... and then make the average
                        m_Read[i] /= n;
                        // and compare it to the target (NOTE if tolerance is set to 0 -> no check and clear error)
                        int diffOPT = abs(m_intTarget - m_Read[i]);
                        if(m_intTolerance > 1 && diffOPT > m_intTolerance)
                        {
                            // difference out of tolerance -> set error
                            spiBoardLinkSingleton()->m_pSPIErr->decodeNotifyEvent(ERR_NSH_OPT_CHECK);
                            writeOPTCheckFile(true);
                        }
                        else
                        {
                            // difference is in tolerance -> clear error
                            if(spiBoardLinkSingleton()->m_pSPIErr->clearError(ERR_NSH_OPT_CHECK) == true)
                            {
                                // the error was present and it has been reset ->
                                writeOPTCheckFile(false);
                            }

                        }

                    }
                }

                m_pLogger->log(LOG_DEBUG, "OpOPTCheck::executeCmd: moved to SS position");
                m_pLogger->log(LOG_INFO, "OpOPTCheck::execute: OPT_CHECK executed");
            }
            break;

            default:	m_eOptState = eState::eStop; 	break;
        }
    }

    int liRes = perform();
    if(liRes == -1)
    {
        m_pLogger->log(LOG_ERR, "OpOPTCheck::executeCmd-->execute not performed");
    }
    else if ( liRes != eOperationRunning )
    {
        m_pLogger->log(LOG_DEBUG, "OpOPTCheck::executeCmd-->completed");
        return -1;
    }

    return eOperationRunning;
}

void OpOPTCheck::fillPositionsVector()
{
    uint8_t i, j;

    m_vRFUvalues.clear();
    m_vPositions.clear();

    for(i = SCT_A_ID; i < SCT_NUM_TOT_SECTIONS; i++)
    {
        for(j = 0; j < SCT_NUM_TOT_SLOTS; j++)
        {
            if(m_stsSectionSlotEnable[i][j] == 0)
            {
                // the slot is enabled -> read the position and insert into the array
                // NOTE: the slots are numbered from 1
                string strPosition, strName;

                strName.assign("Slot_" + to_string(i * SCT_NUM_TOT_SLOTS + j + 1) + "C");
                nshBoardManagerSingleton()->m_pConfig->getItemValue(strName, strPosition);

                m_vPositions.push_back(strPosition);
                m_vRFUvalues.push_back(0);

            }
        }
    }
}

int OpOPTCheck::compileSendOutCmd()
{
    OutCmdOPTCheck cmd;
    cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
    cmd.setUsage(getUsage());
    cmd.setID(getID());

    /* no Payload for the OPTCHECK reply
    // compose the payload ...
    OPTCheckPayload* pOPTCheckPayload = buildPayload();
    // ... and assign it to the outcommand so that the serialize will extract the info
    cmd.setPayload((Payload *)pOPTCheckPayload); */

    // Note: the payload will be deleted by the OutgoingCommand

    /* ********************************************************************************************
     * Send the OutgoingCommand to the client
     * ********************************************************l***********************************
     */
    string strOutCommand;
    bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
    if ( ! bSerializeResult )
    {
        m_pLogger->log(LOG_ERR, "OpOPTCheck::execute--> unable to serialize OutCmdOPTCheck");
    }
    else
    {
        sendCommandReply(strOutCommand);
    }

    return eOperationEnabledForDeath;
}

OPTCheckPayload* OpOPTCheck::buildPayload()
{
    OPTCheckPayload* pOPTCheckPayload = new OPTCheckPayload();

    pOPTCheckPayload->setAttributes(m_Read[SCT_A_ID], m_Read[SCT_B_ID]);

    return pOPTCheckPayload;
}

