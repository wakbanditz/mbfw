/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpInit.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpInit class.
 @details

 ****************************************************************************
*/

#include "OpInit.h"
#include "OutCmdInit.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "SectionBoardManager.h"
#include "InitPayload.h"
#include "CommonInclude.h"

OpInit::OpInit(): Operation("Init")
{
	m_strComponent.clear();
	m_deviceRunning.clear();
}

OpInit::~OpInit()
{
	/* Nothing to do yet */
}

void OpInit::setComponent(const string &strComponent)
{
	m_strComponent.assign(strComponent);
}

int OpInit::perform()
{
	// the REINIT procedure is started in parallel on all devices involved:
	// when a device ha completed the action a notify will be sent

	if (m_strComponent == XML_TAG_INSTRUMENT_NAME)
	{
		if(sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_REINIT, (Operation*)this) == eOperationRunning)
		{
			m_deviceRunning.push_back((uint8_t)eIdSectionA);
		}
		if(sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_REINIT, (Operation*)this) == eOperationRunning)
		{
			m_deviceRunning.push_back((uint8_t)eIdSectionB);
		}
		if(nshBoardManagerSingleton()->setNSHAction(eNSHActionReInit, (Operation*)this) == eOperationRunning)
		{
			m_deviceRunning.push_back((uint8_t)eIdNsh);
		}
		if(cameraBoardManagerSingleton()->setCameraAction(eCameraActionReInit, (Operation*)this) == eOperationRunning)
		{
			m_deviceRunning.push_back((uint8_t)eIdCamera);
		}
	}
	else if (m_strComponent == XML_TAG_SECTIONA_NAME)
	{
		if(sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_REINIT, (Operation*)this) == eOperationRunning)
		{
			m_deviceRunning.push_back((uint8_t)eIdSectionA);
		}
	}
	else if (m_strComponent == XML_TAG_SECTIONB_NAME)
	{
		if(sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_REINIT, (Operation*)this) == eOperationRunning)
		{
			m_deviceRunning.push_back((uint8_t)eIdSectionB);
		}
	}
	else if (m_strComponent == XML_TAG_NSH_NAME)
	{
		if(nshBoardManagerSingleton()->setNSHAction(eNSHActionReInit, (Operation*)this) == eOperationRunning)
		{
			m_deviceRunning.push_back((uint8_t)eIdNsh);
		}
	}
	else if (m_strComponent == XML_TAG_CAMERA_NAME)
	{
		if(cameraBoardManagerSingleton()->setCameraAction(eCameraActionReInit, (Operation*)this) == eOperationRunning)
		{
			m_deviceRunning.push_back((uint8_t)eIdCamera);
		}
	}

	if(m_deviceRunning.size() == 0)
	{
		// no devices started -> error
		return eOperationError;
	}

	return eOperationRunning;
}

InitPayload * OpInit::buildPayload(void)
{
	InitPayload * pPayload = new InitPayload();

	pPayload->setComponent(m_strComponent);

	return pPayload;

}

int OpInit::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdInit cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	InitPayload * pInitPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pInitPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpInit::compileSendOutCmd--> unable to serialize OutCmdSetMotorActivation");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

int OpInit::notifyCompleted(int lNum, bool bSuccess)
{
    (void)bSuccess;

    eDeviceId deviceId = eIdMax;

    // a device has notified the end of REINIT
    switch(lNum)
    {
        case SCT_A_ID:
                deviceId = eIdSectionA;
        break;
        case SCT_B_ID:
                deviceId = eIdSectionB;
        break;
        case NSH_ID:
                deviceId = eIdNsh;
        break;
        case CAMERA_ID:
                deviceId = eIdCamera;
        break;
    }

    // check in the Running list and remove the one completed
    std::vector<uint8_t>::const_iterator deviceIter;
    for(deviceIter = m_deviceRunning.begin(); deviceIter != m_deviceRunning.end(); deviceIter++)
    {
        if(*deviceIter == (uint8_t)deviceId)
        {
            m_deviceRunning.erase(deviceIter);
            break;
        }
    }

    if(m_deviceRunning.size() == 0)
    {
        // all devices have completed their action -> close operation
        closeOperation();
    }

    return m_deviceRunning.size();
}
