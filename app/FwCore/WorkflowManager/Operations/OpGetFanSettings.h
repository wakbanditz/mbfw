/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetFanSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetFanSettings class.
 @details

 ****************************************************************************
*/

#ifndef OPGETFANSETTINGS_H
#define OPGETFANSETTINGS_H

#include "Operation.h"


/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GET_FAN_SETTINGS command
 * ********************************************************************************************************************
 */
class OpGetFanSettings : public Operation
{

	public:
		/*! ***********************************************************************************************************
		 * @brief OpGetFanSettings default constructor
		 * ***********************************************************************************************************
		 */
		OpGetFanSettings();

		/*! ***********************************************************************************************************
		 * @brief ~OpGetFanSettings default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpGetFanSettings();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

};


#endif // OPGETFANSETTINGS_H
