/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetFanSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSetFanSettings class.
 @details

 ****************************************************************************
*/

#ifndef OPSETFANSETTINGS_H
#define OPSETFANSETTINGS_H

#include "Operation.h"


/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SET_FAN_SETTINGS command
 * ********************************************************************************************************************
 */
class OpSetFanSettings : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpSetFanSettings default constructor
		 * ***********************************************************************************************************
		 */
		OpSetFanSettings();

		/*! ***********************************************************************************************************
		 * @brief ~OpSetFanSettings default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpSetFanSettings();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

        /*! ***********************************************************************************************************
         * @brief compileSendOutCmd	method that builds the payload of the reply and sends it
         * @return 0 if success | -1 otherwise
         * ************************************************************************************************************
         */
        int compileSendOutCmd();

		/*! *************************************************************************************************
		 * @brief setFan set a couple component-power
		 * @param strComponent	the component string associated to the received command
		 * @param strPower the percentage power string
		 * **************************************************************************************************
		 */
		void setFan(std::string strComponent, std::string strPower);

		/*! *************************************************************************************************
		 * @brief setDuration set the time for settings
		 * @param strDuration	the duration time string associated to the received command
		 * **************************************************************************************************
		 */
		void setDuration(std::string strDuration);

    private :

        std::vector<std::pair<std::string, std::string>> m_FanSettings;
        std::string m_strDuration;
};


#endif // OPSETFANSETTINGS_H
