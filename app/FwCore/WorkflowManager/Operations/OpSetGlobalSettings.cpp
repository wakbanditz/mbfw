/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetGlobalSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSetGlobalSettings class.
 @details

 ****************************************************************************
*/

#include "OpSetGlobalSettings.h"
#include "OutCmdSetGlobalSettings.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardManager.h"
#include "SectionBoardSPR.h"
#include "SectionBoardTray.h"
#include "CommonInclude.h"

OpSetGlobalSettings::OpSetGlobalSettings() : Operation("OpGlobalSettings")
{
	for(uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		m_SprMin[section] = m_SprMax[section] = -1;
        m_SprTarget[section] = -1;
		m_TrayMin[section] = m_TrayMax[section] = -1;
        m_TrayTarget[section] = -1;
        m_SprTolerance[section] = m_TrayTolerance[section] = 0;
		m_bSection[section] = false;
	}
    m_InternalMin = m_InternalMax = -1;
	m_bInstrument = false;
	m_bForce = false;
}

OpSetGlobalSettings::~OpSetGlobalSettings()
{
}

int OpSetGlobalSettings::perform(void)
{
	int res = eOperationWaitForDeath;

	// if the Force has already been set the current settings have to be executed only if the
	// Force command is enabled

	if(infoSingleton()->getForceTemperature() == true)
	{
		if(m_bForce == false)
		{
			finalOperations();
			return res;
		}
	}

	for(uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		// only if section has to be set
		if(m_bSection[section] == false) continue;

		// and it's able to process commands
		if(section == SCT_A_ID && stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == false)
			continue;

		if(section == SCT_B_ID && stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == false)
			continue;


        // if the Section is in INIT do not send now the command but simply store info
        // at end of initialization the settings will be applied by SectionBoardManager
        if(section == SCT_A_ID && stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusInit)
        {
            if(m_SprTarget[section] != -1)
            {
                infoSingleton()->setSprMaxTemperature(section, m_SprMax[section]);
                infoSingleton()->setSprMinTemperature(section, m_SprMin[section]);
                infoSingleton()->setSprTargetTemperature(section, m_SprTarget[section]);
                infoSingleton()->setSprToleranceTemperature(section, m_SprTolerance[section]);
            }
            if(m_TrayTarget[section] != -1)
            {
                infoSingleton()->setTrayMaxTemperature(section, m_TrayMax[section]);
                infoSingleton()->setTrayMinTemperature(section, m_TrayMin[section]);
                infoSingleton()->setTrayTargetTemperature(section, m_TrayTarget[section]);
                infoSingleton()->setTrayToleranceTemperature(section, m_TrayTolerance[section]);
            }
            continue;
        }

        bool bStoreInfo = false;

        if(m_SprTarget[section] == 0)
		{
			// disable control
#ifndef __DEBUG_PROTOCOL
			if(sectionBoardManagerSingleton(section)->m_pSectionLink->m_pSPR->setTemperature(0, 0, 0) == -1)
				res = eOperationError;
#endif
			bStoreInfo = true;
		}
		else if(m_SprTarget[section] != -1)
		{
#ifndef __DEBUG_PROTOCOL
			if(sectionBoardManagerSingleton(section)->m_pSectionLink->m_pSPR->setTemperature(m_SprTarget[section],
																							 m_SprMin[section],
																							 m_SprMax[section]) == -1)
				res = eOperationError;
#endif
			bStoreInfo = true;
		}

		// store settings to InstrumentInfo container (may be it will be useful ...)
		if(bStoreInfo)
		{
			infoSingleton()->setSprMaxTemperature(section, m_SprMax[section]);
			infoSingleton()->setSprMinTemperature(section, m_SprMin[section]);
			infoSingleton()->setSprTargetTemperature(section, m_SprTarget[section]);
			infoSingleton()->setSprToleranceTemperature(section, m_SprTolerance[section]);
		}

		// same steps for Tray

		bStoreInfo = false;

		if(m_TrayTarget[section] == 0)
		{
			// disable control
#ifndef __DEBUG_PROTOCOL
			if(sectionBoardManagerSingleton(section)->m_pSectionLink->m_pTray->setTemperature(0, 0, 0) == -1)
				res = eOperationError;
#endif
			bStoreInfo = true;
		}
		else if(m_TrayTarget[section] != -1)
		{
#ifndef __DEBUG_PROTOCOL
			if(sectionBoardManagerSingleton(section)->m_pSectionLink->m_pTray->setTemperature(m_TrayTarget[section],
																							  m_TrayMin[section],
																							  m_TrayMax[section]) == -1)
				res = eOperationError;
#endif
			bStoreInfo = true;
		}

		// store settings to InstrumentInfo container (may be it will be useful ...)
		if(bStoreInfo)
		{
			infoSingleton()->setTrayMaxTemperature(section, m_TrayMax[section]);
			infoSingleton()->setTrayMinTemperature(section, m_TrayMin[section]);
			infoSingleton()->setTrayTargetTemperature(section, m_TrayTarget[section]);
			infoSingleton()->setTrayToleranceTemperature(section, m_TrayTolerance[section]);
		}

	}

	if(m_bInstrument == true)
	{
		masterBoardManagerSingleton()->setInternalMaxTemperature(m_InternalMax);
		masterBoardManagerSingleton()->setInternalMinTemperature(m_InternalMin);
		// store settings to InstrumentInfo container (may be it will be useful ...)
		infoSingleton()->setInternalMaxTemperature(m_InternalMax);
		infoSingleton()->setInternalMinTemperature(m_InternalMin);
	}

	// store also Force flag
	infoSingleton()->setForceTemperature(m_bForce);

	finalOperations();

	return res;
}

void OpSetGlobalSettings::finalOperations()
{
    // no need to restore stateMachine
    // restoreStateMachine();

	OutCmdSetGlobalSettings outCmdGlobalSettings;
	string strOutCommand;

	outCmdGlobalSettings.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdGlobalSettings.setUsage(getUsage());
	outCmdGlobalSettings.setID(getID());

	bool bProtocolSerialize = protocolSerialize(&outCmdGlobalSettings, strOutCommand);

	if (bProtocolSerialize == false)
	{
		m_pLogger->log(LOG_ERR, "OpGlobalSettings:: unable to serialize outCmdGlobalSettings");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}
}

void OpSetGlobalSettings::setSectionEnable(uint8_t section, bool value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_bSection[section] = value;
	}
}

void OpSetGlobalSettings::setSprMinTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprMin[section] = value;
	}
}

void OpSetGlobalSettings::setSprMaxTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprMax[section] = value;
	}
}

void OpSetGlobalSettings::setSprTargetTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprTarget[section] = value;
	}
}

void OpSetGlobalSettings::setSprToleranceTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprTolerance[section] = value;
	}
}

void OpSetGlobalSettings::setTrayMinTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayMin[section] = value;
	}
}

void OpSetGlobalSettings::setTrayMaxTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayMax[section] = value;
	}
}

void OpSetGlobalSettings::setTrayTargetTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayTarget[section] = value;
	}
}

void OpSetGlobalSettings::setTrayToleranceTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayTolerance[section] = value;
	}
}

void OpSetGlobalSettings::setInternalMinTemperature(int value)
{
	m_InternalMin = value;
}

void OpSetGlobalSettings::setInternalMaxTemperature(int value)
{
	m_InternalMax = value;
}

void OpSetGlobalSettings::setForceTemperature(bool status)
{
	m_bForce = status;
}

void OpSetGlobalSettings::setInstrumentEnable(bool value)
{
	m_bInstrument = value;
}

int OpSetGlobalSettings::getSprMinTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprMin[section];
}

int OpSetGlobalSettings::getSprMaxTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprMax[section];
}

int OpSetGlobalSettings::getSprTargetTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprTarget[section];
}

int OpSetGlobalSettings::getSprToleranceTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprTolerance[section];
}

int OpSetGlobalSettings::getTrayMinTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayMin[section];
}

int OpSetGlobalSettings::getTrayMaxTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayMax[section];
}

int OpSetGlobalSettings::getTrayTargetTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayTarget[section];
}

int OpSetGlobalSettings::getTrayToleranceTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayTolerance[section];
}

int OpSetGlobalSettings::getInternalMinTemperature()
{
	return m_InternalMin;
}

int OpSetGlobalSettings::getInternalMaxTemperature()
{
	return m_InternalMax;
}

bool OpSetGlobalSettings::getForceTemperature()
{
	return m_bForce;
}
