/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetMotorActivation.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSetMotorActivation class.
 @details

 ****************************************************************************
*/

#include "OpSetMotorActivation.h"
#include "OutCmdSetMotorActivation.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "SectionBoardManager.h"
#include "SetMotorActivationPayload.h"
#include "CommonInclude.h"

OpSetMotorActivation::OpSetMotorActivation() : Operation("SetMotorActivation")
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strActivation.clear();
}

OpSetMotorActivation::~OpSetMotorActivation()
{
	/* Nothing to do yet */
}

void OpSetMotorActivation::setParams(const string &strComponent, const string &strMotor, const string &strActivation)
{
	m_strComponent.assign(strComponent);
	m_strMotor.assign(strMotor);
	m_strActivation.assign(strActivation);
}

int OpSetMotorActivation::perform()
{
	if (m_strComponent.compare(XML_TAG_SECTIONA_NAME) == 0)
	{
		return sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_CMD, (Operation*)this);
	}
	else if (m_strComponent.compare(XML_TAG_SECTIONB_NAME) == 0)
	{
		return sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_CMD, (Operation*)this);
	}
	else if ( m_strComponent.compare(XML_TAG_NSH_NAME) == 0 )
	{
		return nshBoardManagerSingleton()->setNSHAction(eNSHActionCmd, (Operation*)this);
	}

	m_pLogger->log(LOG_WARNING, "OpSetMotorActivation::perform-->Component %s not in list", m_strComponent.c_str());
	return eOperationError;
}

int OpSetMotorActivation::executeCmd(int lNum)
{

#ifdef __DEBUG_PROTOCOL
	return 0;
#endif

	int liRes = -1;

	switch ( lNum )
	{
		case SCT_A_ID:
		case SCT_B_ID:	//action to perform
			liRes = executeSetMotorSection(lNum);
		break;

		case NSH_ID:
			liRes = executeSetMotorNSH();
		break;
	}

	if ( liRes != 0 )
	{
		m_pLogger->log(LOG_ERR, "OpSetMotorActivation::execute-->execute not performed");
		return -1;
	}

    m_pLogger->log(LOG_DEBUG, "OpSetMotorActivation::execute-->SETMOTORACTIVATION executed");
	return 0;
}

SetMotorActivationPayload * OpSetMotorActivation::buildPayload(void)
{
	SetMotorActivationPayload * pPayload = new SetMotorActivationPayload();

	pPayload->setOutParams(m_strComponent, m_strMotor, m_strActivation);

	return pPayload;

}

int OpSetMotorActivation::executeSetMotorSection(uint8_t ubNumSection)
{
	SectionCommonMessage*	pMotor = NULL;

	//choice of the component to move
	if ( m_strMotor.compare(SECTION_TOWER_STRING) == 0 )
	{
		pMotor = &sectionBoardLinkSingleton(ubNumSection)->m_pTower->m_CommonMessage;
	}
	else if ( m_strMotor.compare(SECTION_PUMP_STRING) == 0 )
	{
		pMotor = &sectionBoardLinkSingleton(ubNumSection)->m_pPump->m_CommonMessage;
	}
	else if ( m_strMotor.compare(SECTION_TRAY_STRING) == 0 )
	{
		pMotor = &sectionBoardLinkSingleton(ubNumSection)->m_pTray->m_CommonMessage;
	}
	else if ( m_strMotor.compare(SECTION_SPR_STRING) == 0 )
	{
		pMotor = &sectionBoardLinkSingleton(ubNumSection)->m_pSPR->m_CommonMessage;
	}
	else
	{
		m_pLogger->log(LOG_WARNING, "OpSetMotorActivation::executeCmd: Motor %s not in list",
					   m_strMotor.c_str());
		return -1;
	}

	int res = 0;
	if ( m_strActivation.compare(XML_ATTR_STATUS_ENABLED) == 0 )
	{
		// set default low current
		res = pMotor->setMotorCurrent(eCoil, 'T');
	}
	else
	{
		// disable current on motor
		res = pMotor->setMotorCurrent(eCoil, '0');
	}
	return res;
}

int OpSetMotorActivation::executeSetMotorNSH(void)
{
	int liRes = -1;
	if ( m_strActivation.compare(XML_ATTR_STATUS_ENABLED) == 0 )
	{
		liRes = spiBoardLinkSingleton()->m_pNSHMotor->setMotorCurrent(eSetTValHold, dSPIN_CONF_PARAM_TVAL_HOLD);
	}
	else
	{
		liRes = spiBoardLinkSingleton()->m_pNSHMotor->setMotorCurrent(eSetTValHold, 31.25f);
	}
	return liRes;
}

int OpSetMotorActivation::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdSetMotorActivation cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	SetMotorActivationPayload * pSetMotorActivationPayload = buildPayload();
	// ... and assign it to the outcommand so that the serializle will extract the info
	cmd.setPayload((Payload *)pSetMotorActivationPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSetMotorActivation::execute--> unable to serialize OutCmdSetMotorActivation");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

