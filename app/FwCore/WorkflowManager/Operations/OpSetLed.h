/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetLed.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSetLed class.
 @details

 ****************************************************************************
*/

#ifndef OPSETLED_H
#define OPSETLED_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SETLED command
 * ********************************************************************************************************************
 */
class OpSetLed : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpSetLed default constructor
		 * ***********************************************************************************************************
		 */
		OpSetLed(std::string strComponent, std::string strMode, std::string strLed);

		/*! ***********************************************************************************************************
		 * @brief ~OpSetLed default destructor
		 * ************************************************************************************************************
		 */
		~OpSetLed();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	function to be called in order to execute the operation the actions related to the command
		 * @return 0
		 * ************************************************************************************************************
		 */
		int executeCmd( int lNum );

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	method that builds the payload of the reply and sends it
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

	private:

		std::string m_strComponent, m_strMode, m_strLed;

};


#endif // OPSETLED_H
