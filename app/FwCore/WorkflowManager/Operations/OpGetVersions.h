/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetVersions.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetVersions class.
 @details

 ****************************************************************************
*/

#ifndef OPGETVERSIONS_H
#define OPGETVERSIONS_H


#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETVERSIONS command
 * ********************************************************************************************************************
 */
class OpGetVersions : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpGetVersions default constructor
		 * ***********************************************************************************************************
		 */
		OpGetVersions();

		/*! ***********************************************************************************************************
		 * @brief ~OpGetVersions default destructor
		 * ************************************************************************************************************
		 */
		~OpGetVersions();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);
};

#endif // OPGETVERSIONS_H
