/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpMove.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpMove class.
 @details

 ****************************************************************************
*/

#ifndef OPMOVE_H
#define OPMOVE_H

#include "Operation.h"
#include "SectionBoardManager.h"
#include "Loggable.h"

class MovePayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the MOVE command
 * ********************************************************************************************************************
 */
class OpMove : public Operation
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OpMove default constructor
		 * ***********************************************************************************************************
		 */
		OpMove(const string &strComponent, const string &strMotor, const string &strMove, const string& strMoveValue);

		/*! ***********************************************************************************************************
		 * @brief ~OpMove default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpMove();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*!
		 * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
		 * then it composes the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 */
		int compileSendOutCmd();

	private:

		/*!
		 * @brief executeMoveSection	Controls what component should be moved and the typology of the received
		 * move command. Then it calls the related function.
		 * @param ubNumSection
		 * @return 0 if success | -1 otherwise
		 */
		int executeMoveSection(uint8_t ubNumSection);

		/*!
		 * @brief executeMoveSectionPredefined	Controls the movement to the predefined position.
		 * @param ubNumSection
		 * @return 0 if success | -1 otherwise
		 */
		int executeMoveSectionPredefined(uint8_t ubNumSection);

		/*!
		 * @brief executeMoveSection	Controls what component should be moved and the typology of the received
		 * move command. Then it calls the related function.
		 * @param ubNumSection
		 * @return 0 if success | -1 otherwise
		 */
		int executeMoveNSH(void);

		/*!
		 * @brief getNSHCodedSteps	Controls the movement to the predefined position.
		 * @param ubNumSection
		 * @return 0 if success | -1 otherwise
		 */
		int getNSHCodedSteps(int& liSteps);

		/*!
		 * @brief buildPayload	Fills the MovePayload of the outgoing command with related values.
		 * @return  pointer to the MovePayload object just allocated
		 */
		MovePayload * buildPayload(void);

    private:
        string m_strComponent;
        string m_strMotor;
        string m_strMove;
        string m_strMoveValue;

        string m_strNumSection;
        string m_strPosition;

        uint8_t	m_ubNumSection;

};

#endif // OPMOVE_H
