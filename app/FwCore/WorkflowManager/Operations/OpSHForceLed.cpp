/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSHForceLed.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSHForceLed class.
 @details

 ****************************************************************************
*/

#include <chrono>

#include "OpSHForceLed.h"
#include "OutCmdSHForceLed.h"
#include "CommonInclude.h"
#include "SHForceLedPayload.h"
#include "WebServerAndProtocolInclude.h"

chrono::time_point<chrono::steady_clock> ChronoStart;

OpSHForceLed::OpSHForceLed() : Operation("OpSHForceLed")
{
	m_strDuration.clear();
	m_strStart.clear();
}

OpSHForceLed::~OpSHForceLed()
{

}

void OpSHForceLed::setParam(string strDuration)
{
	m_strDuration.assign(strDuration);
}

int OpSHForceLed::perform()
{
	return nshBoardManagerSingleton()->setNSHAction(eNSHActionCmd, (Operation*)this);
}

int OpSHForceLed::executeCmd(int lNum)
{
	int liRes = -lNum;

	liRes = nshReaderGeneralSingleton()->switchUVLed(eNSHLedEnabled);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "OpSHForceLed::executeCmd: unable to turn on NSH led");
		return -1;
	}

	m_strStart = getFormattedTime(true);
	ChronoStart = std::chrono::steady_clock::now();

	return 0;
}

int OpSHForceLed::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdSHForceLed cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	SHForceLedPayload* pSHForceLed = buildPayload();
	// ... and assign it to the outcommand so that the serializle will extract the info
	cmd.setPayload((Payload *)pSHForceLed);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSetMotorActivation::execute--> unable to serialize OutCmdSetMotorActivation");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	auto ChronoStop = std::chrono::steady_clock::now();
	std::chrono::duration<double> elapsedSeconds = ChronoStop - ChronoStart;
	int liDurationMsec = stoi(m_strDuration)*MSEC_PER_SEC;

	while ( elapsedSeconds.count()*MSEC_PER_SEC < liDurationMsec )
	{
		msleep(200);
		ChronoStop = std::chrono::steady_clock::now();
		elapsedSeconds = ChronoStop - ChronoStart;
	}

	nshReaderGeneralSingleton()->switchUVLed(eNSHLedDisabled);

	return eOperationEnabledForDeath;
}

SHForceLedPayload* OpSHForceLed::buildPayload()
{
	SHForceLedPayload* pPayload = new SHForceLedPayload();

	pPayload->setOutParams(m_strStart, m_strDuration);

	return pPayload;
}
