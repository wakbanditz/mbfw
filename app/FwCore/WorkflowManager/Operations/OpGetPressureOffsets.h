/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetPressureOffsets.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetPressureOffsets class.
 @details

 ****************************************************************************
*/

#ifndef OPGETPRESSUREOFFSETS_H
#define OPGETPRESSUREOFFSETS_H

#include "Operation.h"

class GetPressureOffsetsPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETPRESSUREOFFSETS command
 * ********************************************************************************************************************
 */
class OpGetPressureOffsets : public Operation
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OpGetPressureOffsets default constructor
		 * ***********************************************************************************************************
		 */
		OpGetPressureOffsets(std::vector<uint8_t> componentList);

		/*! ***********************************************************************************************************
		 * @brief ~OpGetPressureOffsets default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpGetPressureOffsets();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd( void );

		/*! ***********************************************************************************************************
		 * @brief setAskBoard	to set if the command has to perform a real request to Hardware
		 * @param status true -> action required, false -> no action (default) get data from memory
		 * ************************************************************************************************************
		 */
		void setAskBoard(bool status);

	private:
		/*!
		 * @brief executeGetPressureOffset	request to the related section component and label.
		 * @param section		number of the section to ask for value
		 * @return 0 if success | -1 otherwise
		 */
		int executeGetPressureOffset(int section);

		/*!
		 * @brief buildPayload	Fills the GetPressureOffsetsPayload of the outgoing command with related values.
		 * @return  pointer to the GetPressureOffsetsPayload object just allocated
		 */
		GetPressureOffsetsPayload * buildPayload(void);

    private:
        std::vector<std::string> m_strComponent;
        std::vector<std::string> m_strOffsets;

        uint8_t	m_counterAction;
        bool	m_bAskBoards;

};


#endif // OPGETPRESSUREOFFSETS_H
