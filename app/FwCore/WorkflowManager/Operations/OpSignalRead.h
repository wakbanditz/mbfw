/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSignalRead.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSignalRead class.
 @details

 ****************************************************************************
*/

#ifndef OPSIGNALREAD_H
#define OPSIGNALREAD_H

#include "Operation.h"
#include "Payload.h"
#include "MainExecutor.h"

class SignalReadPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SIGNALREAD command
 * ********************************************************************************************************************
 */

class OpSignalRead : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpSignalRead default constructor
		 * ***********************************************************************************************************
		 */
		OpSignalRead();

		/*! ***********************************************************************************************************
		 * @brief ~OpSignalRead default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpSignalRead();

		/*!
		 * @brief setParams	set the parameters for calibration
         * @param strRepetitions
         * @param strInterval
		 */
		void setParams(string strRepetitions, string strInterval);

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
         * @param lNum device index
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

	private:

		SignalReadPayload* buildPayload();

    private:

        string			m_strRepetitions;
        string			m_strInterval;

        vector<string>	m_vFluoRead;
        vector<string>	m_vTimeStamps;
        vector<string>	m_vFluoMv;
        vector<string>	m_vRefMv;

};

#endif // OPSIGNALREAD_H
