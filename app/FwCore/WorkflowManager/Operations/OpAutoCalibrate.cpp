/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpAutoCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpAutoCalibrate class.
 @details

 ****************************************************************************
*/

#include "OpAutoCalibrate.h"
#include "MainExecutor.h"
#include "WebServerAndProtocolInclude.h"
#include "OutCmdAutoCalibrate.h"
#include "AutoCalibratePayload.h"

OpAutoCalibrate::OpAutoCalibrate() : Operation("OpAutoCalibrate")
{
	m_strRFUVal.clear();
	m_strGain.clear();
}

OpAutoCalibrate::~OpAutoCalibrate()
{
	/* Nothing to do yet */
}

int OpAutoCalibrate::perform()
{
	return nshBoardManagerSingleton()->setNSHAction(eNSHActionCmd, (Operation*)this);
}

int OpAutoCalibrate::executeCmd(int lNum)
{
	int liRes = -lNum;
	int liRfuValSS;
	liRes = nshBoardManagerSingleton()->autoCalibrateSS(liRfuValSS);
	if ( liRes )
	{
		// bring nsh in home position since the value stored in ss position may be not correct
		if ( spiBoardLinkSingleton() != nullptr )
		{
			spiBoardLinkSingleton()->move(eHome);
		}
		m_pLogger->log(LOG_ERR, "OpAutoCalibrate::executeCmd: unable to perform auto calibration");
		return -1;
	}

	m_strRFUVal = to_string(liRfuValSS);

	strucCalibrParam sCalibr;
    liRes = nshReaderGeneralSingleton()->getCalibrParams(&sCalibr);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR,"OpGetSSReference::executeCmd: not able to get gain");
		return -1;
	}
    m_strGain = to_string(int(sCalibr.fSSCalibPar * 100));

	return 0;
}

int OpAutoCalibrate::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdAutoCalibrate cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	AutoCalibratePayload * pAutoCalibratePayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pAutoCalibratePayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpGetParameter::execute--> unable to serialize OutCmdGetParameter");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

AutoCalibratePayload* OpAutoCalibrate::buildPayload()
{
	AutoCalibratePayload * pAutoCalibratePayload = new AutoCalibratePayload();

	pAutoCalibratePayload->setParams(m_strRFUVal, m_strGain);

	return pAutoCalibratePayload;
}
