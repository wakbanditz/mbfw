/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetCalibration.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetCalibration class.
 @details

 ****************************************************************************
*/
#ifndef OPGETCALIBRATION_H

#define OPGETCALIBRATION_H

#include "Operation.h"

class SectionCommonMessage;
class GetCalibrationPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETCALIBRATION command
 * ********************************************************************************************************************
 */
class OpGetCalibration : public Operation
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OpGetCalibration default constructor
		 * ***********************************************************************************************************
		 */
		OpGetCalibration(const string &strComponent, const string &strMotor, const string &strLabel);

		/*! ***********************************************************************************************************
		 * @brief ~OpGetCalibration default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpGetCalibration();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd( void );

		/*! ***********************************************************************************************************
		 * @brief setAskBoard	to set if the command has to perform a real request to Hardware
		 * @param status true -> action required, false -> no action (default) get data from memory
		 * ************************************************************************************************************
		 */
		void setAskBoard(bool status);

	private:
		/*!
		 * @brief executeGetCalibration	request to the related section component and label.
		 * @param section		number of the section to ask for value
		 * @return 0 if success | -1 otherwise
		 */
		int executeGetCalibration(int section);

		/*!
		 * @brief executeGetCalibration	request to the NSH component and label.
		 * @param section		number of the section to ask for value
		 * @return 0 if success | -1 otherwise
		 */
		int executeGetNshCalibration();

		/*!
		 * @brief buildPayload	Fills the GetCalibrationPayload of the outgoing command with related values.
		 * @return  pointer to the GetCalibrationPayload object just allocated
		 */
		GetCalibrationPayload * buildPayload(void);

    private:
        std::vector<std::string> m_strComponent;
        std::vector<std::string> m_strMotor;
        std::vector<std::string> m_strLabel;
        std::vector<std::string> m_strCalibration;

        uint8_t	m_counterAction;
        bool	m_bAskBoards;

};

#endif // OPGETCALIBRATION_H
