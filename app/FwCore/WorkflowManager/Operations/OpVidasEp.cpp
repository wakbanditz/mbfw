/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpVidasEp.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpVidasEp class.
 @details

 ****************************************************************************
*/

#include "OpVidasEp.h"
#include "OutCmdVidasEp.h"
#include "GetVidasEpPayload.h"
#include "CommonInclude.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "THVidasEp.h"
#include "SectionBoardGeneral.h"
#include "StatusInclude.h"

#include <ctime>

OpVidasEp::OpVidasEp() : Operation("OpVidasEp")
{

}

OpVidasEp::~OpVidasEp()
{

}

int OpVidasEp::perform(void)
{
	THVidasEp::getInstance()->sendHttpData(getUsage(), getWebUrl() ,getID());

	return eOperationWaitForDeath;
}


int OpVidasEp::compileSendOutCmd()
{
	OutCmdVidasEp outCmdVidasEp;
	string strOutCommand;

	outCmdVidasEp.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdVidasEp.setUsage(getUsage());
	outCmdVidasEp.setID(getID());

	// build the payload of the asynchrounous reply
	GetVidasEpPayload * pGetVidasEpPayload = new GetVidasEpPayload();

	// status of the devices
	pGetVidasEpPayload->setCameraStatus(stateMachineSingleton()->getDeviceStatusAsString(eIdCamera));
	pGetVidasEpPayload->setNshStatus(stateMachineSingleton()->getDeviceStatusAsString(eIdNsh));
	pGetVidasEpPayload->setSectionAStatus(stateMachineSingleton()->getDeviceStatusAsString(eIdSectionA));
	pGetVidasEpPayload->setSectionBStatus(stateMachineSingleton()->getDeviceStatusAsString(eIdSectionB));

	if(infoSingleton()->getMaintenanceMode() == true)
	{
		// the Module status is overwritten in Maintenance Mode
		pGetVidasEpPayload->setInstrumentStatus(MAINTENANCE_STATUS_STRING);
	}
	else
	{
		pGetVidasEpPayload->setInstrumentStatus(stateMachineSingleton()->getDeviceStatusAsString(eIdModule));
	}

	// temperatures
	pGetVidasEpPayload->setSectionTemperature(SCT_A_ID,
											  sectionBoardGeneralSingleton(SCT_A_ID)->getIntTemperatureTray(),
											  sectionBoardGeneralSingleton(SCT_A_ID)->getIntTemperatureSPR());

	pGetVidasEpPayload->setSectionTemperature(SCT_B_ID,
											  sectionBoardGeneralSingleton(SCT_B_ID)->getIntTemperatureTray(),
											  sectionBoardGeneralSingleton(SCT_B_ID)->getIntTemperatureSPR());

	pGetVidasEpPayload->setInstrumentTemperature(masterBoardManagerSingleton()->getInternalTemperature());

	/* !!!!  TODO pulsanti Start -> spostare
	std::string strButton;
	if(sectionBoardGeneralSingleton(SCT_A_ID)->getStartButton() == true)
	{
		strButton.assign(XML_ATTR_STATUS_ENABLED);
	}
	else
	{
		strButton.assign(XML_ATTR_STATUS_DISABLED);
	}
	pGetVidasEpPayload->setSectionStartButton(SCT_A_ID, strButton);

	if(sectionBoardGeneralSingleton(SCT_B_ID)->getStartButton() == true)
	{
		strButton.assign(XML_ATTR_STATUS_ENABLED);
	}
	else
	{
		strButton.assign(XML_ATTR_STATUS_DISABLED);
	}
	pGetVidasEpPayload->setSectionStartButton(SCT_B_ID, strButton);
	// when the status is acquired -> reset it
	sectionBoardGeneralSingleton(SCT_A_ID)->clearStartButton();
	sectionBoardGeneralSingleton(SCT_B_ID)->clearStartButton();
--------------- */


	// !!!! TODO stato pulsante POWER

	// doors
	pGetVidasEpPayload->setSectionDoor(SCT_A_ID, sectionBoardGeneralSingleton(SCT_A_ID)->getDoorStatus());
	pGetVidasEpPayload->setSectionDoor(SCT_B_ID, sectionBoardGeneralSingleton(SCT_B_ID)->getDoorStatus());

	// se sezione in run RunEndTime
	std::string strTime;

	strTime.clear();
	if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusProtocol)
	{
		strTime = runwlInfoPointer(SCT_A_ID)->getLastTime();
	}
	pGetVidasEpPayload->setSectionEndTime(SCT_A_ID, strTime);

	strTime.clear();
	if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusProtocol)
	{
		strTime = runwlInfoPointer(SCT_B_ID)->getLastTime();
	}
	pGetVidasEpPayload->setSectionEndTime(SCT_B_ID, strTime);


	std::vector<std::string> vectorErrors;

    vectorErrors.clear();
    sectionBoardManagerSingleton(SCT_A_ID)->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ERROR);
    sectionBoardManagerSingleton(SCT_A_ID)->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ALARM);
    pGetVidasEpPayload->setErrorList(&vectorErrors, SCT_A_ID);

	vectorErrors.clear();
    sectionBoardManagerSingleton(SCT_B_ID)->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ERROR);
    sectionBoardManagerSingleton(SCT_B_ID)->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ALARM);
    pGetVidasEpPayload->setErrorList(&vectorErrors, SCT_B_ID);

	vectorErrors.clear();
    masterBoardManagerSingleton()->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ERROR);
    masterBoardManagerSingleton()->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ALARM);
    pGetVidasEpPayload->setErrorList(&vectorErrors, INSTRUMENT_ID);

	// NSH errors
	vectorErrors.clear();
    nshBoardManagerSingleton()->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ERROR);
    nshBoardManagerSingleton()->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ALARM);
    pGetVidasEpPayload->setErrorList(&vectorErrors, NSH_ID);

	// Camera errors
	vectorErrors.clear();
    cameraBoardManagerSingleton()->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ERROR);
    cameraBoardManagerSingleton()->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ALARM);
    pGetVidasEpPayload->setErrorList(&vectorErrors, CAMERA_ID);

	// set current date-time
    strTime = getVidasepTimestamp();
	pGetVidasEpPayload->setTimestamp(strTime);

    // OPT identifier
    pGetVidasEpPayload->setOPTid(infoSingleton()->getOPTid());

	// and assign it to the outcommand so that the serialize will extract the info
	outCmdVidasEp.setPayload((Payload *)pGetVidasEpPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	protocolSerialize(&outCmdVidasEp, strOutCommand);

	sendCommandReply(strOutCommand);

	return 0;
}
