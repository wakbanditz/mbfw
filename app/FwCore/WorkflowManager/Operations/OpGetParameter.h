/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetParameter.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetPArameter class.
 @details

 ****************************************************************************
*/

#ifndef OPGETPARAMETER_H
#define OPGETPARAMETER_H

#include "Operation.h"
#include "SectionBoardManager.h"

class GetParameterPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETPARAMETER command
 * ********************************************************************************************************************
 */
class OpGetParameter : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpGetParameter constructor with two parameter associated
		 * ***********************************************************************************************************
		 */
		OpGetParameter(const string &strComponent, const string &strMotor);

		/*! ***********************************************************************************************************
		 * @brief ~OpGetParameter default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpGetParameter();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	method that builds the payload of the reply and sends it
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

		/*! ***********************************************************************************************************
		 * @brief setAskBoard	to set if the command has to perform a real request to Hardware
		 * @param status true -> action required, false -> no action (default) get data from memory
		 * ************************************************************************************************************
		 */
		void setAskBoard(bool status);

	private:
		/*!
		 * @brief executeGetSectionParameter	Controls what component should be moved and the typology of the received
		 * move command. Then it calls the related function.
		 * @param ubNumSection
		 * @return 0 if success | -1 otherwise
		 */
		int executeGetSectionParameter(uint8_t ubNumSection);

		/*!
		 * @brief executeGetNSHParameter	Controls what component should be moved and the typology of the received
		 * move command. Then it calls the related function.
		 * @return 0 if success | -1 otherwise
		 */
		int executeGetNSHParameter(void);

		/*!
		 * @brief buildPayload	Fills the GetParameterPayload of the outgoing command with related values.
		 * @return  pointer to the GetParameterPayload object just allocated
		 */
		GetParameterPayload * buildPayload(void);

    private:
        std::vector<std::string> m_strComponent;
        std::vector<std::string> m_strMotor;
        std::vector<std::string> m_strMicrostepResolution;
        std::vector<std::string> m_strAccelerationFactor;
        std::vector<std::string> m_strOperatingCurrent;
        std::vector<std::string> m_strStandbyCurrent;
        std::vector<std::string> m_strStepConversionFactor;

        uint8_t	m_counterAction;
        bool	m_bAskBoards;

};

#endif // OPGETPARAMETER_H
