/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpReadSec.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpReadSec class.
 @details

 ****************************************************************************
*/

#ifndef OPREADSEC_H
#define OPREADSEC_H

#include "InCmdReadSec.h"
#include "Operation.h"
#include "CR8062Include.h"
#include "CameraSettings.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the READSEC command
 * ********************************************************************************************************************
 */

struct structRead
{
	bool bIsEnabled;
	string strCode;
	vector<unsigned char> vImage;
};

struct structSampleRead
{
	bool bIsEnabled;
	int  liWell;
	int  liScore;
	bool bSampleIsPresent;
	vector<unsigned char> vImage;
};

struct structSubstrate
{
	bool bIsEnabled;
	int liSubsRead;
};

struct structReadSlot
{
	bool bIsEnabled; // change with a struct of enabled elements all initialized to false.
	structRead Spr;
	structRead Strip;
	structSubstrate Substrate;
	structSampleRead W0;
	structSampleRead W3;
};

enum class eTarget { eNone, eSpr, eStrip, eSubstrate, eConeAbsence, eSample0, eSample3 };

class ReadSecPayload;

class OpReadSec : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpReadSec default constructor
		 * ***********************************************************************************************************
		 */
		OpReadSec();

		/*! ***********************************************************************************************************
		 * @brief ~OpReadSec default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpReadSec();

		/*! ***********************************************************************************************************
		 * @brief setParameters	called by the workflow to transfer the parameters for the ReadSection procedure
		 * @param section the section index
		 * @param stripReadSection the structure containing the info
         * @param vSettings camera settings list
		 * ************************************************************************************************************
		 */
		void setParameters(int section, const struct stripReadSection_tag * stripReadSection, CameraSettings& cSettings);

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
         * @param liNum the calling device identifier
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int liNum);

        /*! ***********************************************************************************************************
         * @brief executeStripRead	method that implements the Strip Barcode Reading action
         * @return 0 if success | -1 otherwise
         * ************************************************************************************************************
         */
        int executeStripRead(void);

        /*! ***********************************************************************************************************
		 * @brief executeSprRead	method that implements the Spr DataMatrix Reading action.
		 *
		 *
		 * NOTE: THIS IS ONLY USED IN SSW, THEREFORE (TESTS PURPOSE) IS ONLY POSSIBLE TO PERFORM DTMX READ OR SPR
		 * PRESENCE ONE AT A TIME
		 *
		 *
         * @return 0 if success | -1 otherwise
         * ************************************************************************************************************
         */
        int executeSprRead(void);

        /*! ***********************************************************************************************************
         * @brief executeSubstrateRead	method that implements the Strip Optic Reading action
         * @return 0 if success | -1 otherwise
         * ************************************************************************************************************
         */
        int executeSubstrateRead(void);

        /*! ***********************************************************************************************************
         * @brief executeSampleRead	method that implements the Strip Well Reading action
         * @param target the action target (well0 or well3)
         * @return 0 if success | -1 otherwise
         * ************************************************************************************************************
         */
        int executeSampleRead(eTarget target);

        /*! ***********************************************************************************************************
         * @brief executeRestore	method that implements the Strip reset action
         * @return 0 if success | -1 otherwise
         * ************************************************************************************************************
         */
        int executeRestore(void);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
		 * then it composes the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * *************************************************************************************************************/
		int compileSendOutCmd();

		/*! ***********************************************************************************************************
		 * @brief getSection get the section involved in the ReadSection procedure
		 * @return the index of the section if defined, -1 otherwise
		 * ************************************************************************************************************
		 */
		int getSection();

		/*! ***********************************************************************************************************
		 * @brief getDataStructure get the whole data structure involved in the ReadSection procedure
		 * @return the m_stripReadSection structure
		 * ************************************************************************************************************
		 */
		stripReadSection_tag* getDataStructure();

	private:

		void createActionList(void);

        void recoverCamera(void);

        ReadSecPayload* buildPayload(void);

		// add fake image in case of error
		void addFakeImage(vector<unsigned char>& vImage);

    private:

        enum class eState { eZero, eStart, eSendBatch, eMoveToTrayRead, eMoveToSprRead, eMoveToTraySample, eMoveNshToBC,
                            eMoveNshToDM, eMoveNshToSample, eReadBarcode, eReadDataMatrix, eReadSubstrate,
							eGetPicture, eCheckSprPresence, eSampleAlgorithm, eSectRestore, eNshRestore, eInstrRestore, eStop, eEnd };

        vector<structReadSlot> m_vReadSec;
        stripReadSection_tag m_stripReadSection[SCT_NUM_TOT_SLOTS];
        vector<eTarget> m_vTargets[SCT_NUM_TOT_SLOTS];
        eState m_eCurrState;
        eTarget m_eCurrTarget;

		CameraSettings m_cSettings;
        structInfoImage m_InfoImage;
        int m_sectionId;
        int m_nCurrSlotId;

};

#endif // OPREADSEC_H
