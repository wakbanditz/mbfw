/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpMonitorPressure.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpMonitorPressure class.
 @details

 ****************************************************************************
*/

#include "OpMonitorPressure.h"
#include "THMonitorPressure.h"

OpMonitorPressure::OpMonitorPressure() : Operation("OpMonitorPressure")
{
	m_section = SCT_NONE_ID;
	m_action = MONITOR_PRESSURE_NONE;
}

OpMonitorPressure::~OpMonitorPressure()
{

}

int OpMonitorPressure::perform(void)
{
	if(m_action == MONITOR_PRESSURE_STOP)
	{
		// the thread is working -> let's stop it
		return THMonitorPressure::getInstance()->stopRawMonitoring(this);
	}
	else if(m_action == MONITOR_PRESSURE_START)
	{
		return THMonitorPressure::getInstance()->startRawMonitoring(this);
	}
	else return eOperationError;
}

void OpMonitorPressure::setParameters(int section, int action)
{
	m_action = action;
	m_section = section;
}

int OpMonitorPressure::getSection()
{
	return m_section;
}

int OpMonitorPressure::getAction()
{
	return m_action;
}
