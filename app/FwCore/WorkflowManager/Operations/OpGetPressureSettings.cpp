/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetPressureSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetPressureSettings class.
 @details

 ****************************************************************************
*/

#include "OpGetPressureSettings.h"
#include "OutCmdGetPressureSettings.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardPIB.h"
#include "GetPressureSettingsPayload.h"
#include "CommonInclude.h"


/*! *******************************************************************************************************************
 * Protocol command name element values
 * ********************************************************************************************************************
 */

OpGetPressureSettings::OpGetPressureSettings(std::vector<uint8_t> componentList)
									: Operation("GetPressureOffsets")
{
	bool bA = false;
	bool bB = false;

	m_strComponent.clear();
	m_strConversions.clear();
	m_strPressures.clear();
	m_strConversionFactor.clear();
	m_strPressureFactor.clear();

	for(uint8_t i = 0; i < componentList.size(); i++)
	{
		if(componentList.at(i) == SCT_A_ID &&
				stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
		{
			bA = true;
		}
		else if(componentList.at(i) == SCT_B_ID	&&
				stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
		{
			bB = true;
		}
	}

	// to be sure that A is always before B ...
	if(bA)	m_strComponent.push_back(XML_VALUE_SECTION_A);
	if(bB)	m_strComponent.push_back(XML_VALUE_SECTION_B);

	m_bAskBoards = false;
}

OpGetPressureSettings::~OpGetPressureSettings()
{
	m_strComponent.clear();
	m_strConversions.clear();
	m_strPressures.clear();
}

int OpGetPressureSettings::perform()
{
	uint32_t storage;
	int32_t  intTmp1, intTmp2;
	uint8_t section;

	// no action to be performed: the parameters are automatically acquired by the Masterboard
	// during the Initialization procedure

	// default value is typically 100000 -> factor should be 5
	storage = sectionBoardLinkSingleton(SCT_A_ID)->m_pPIB->readStorageFactor();
	int factor = 0;
	while(storage > 1)
	{
		storage /= 10;
		factor++;
	}
	m_strConversionFactor.assign(std::to_string(factor));
	m_strPressureFactor.assign("0");

	for(uint8_t i = 0; i < m_strComponent.size(); i++)
	{
		if(m_strComponent.at(i).compare(XML_VALUE_SECTION_A) == 0)
		{
			section = SCT_A_ID;
		}
		else if(m_strComponent.at(i).compare(XML_VALUE_SECTION_B) == 0)
		{
			section = SCT_B_ID;
		}
		else
		{
			continue;	// should not happen ....
		}

		uint8_t j;

		for(j = 0; j < SCT_NUM_TOT_SLOTS; j++)
		{
#ifndef __DEBUG_PROTOCOL
			intTmp1 = sectionBoardLinkSingleton(section)->m_pPIB->readPressureCalibrationChannel(j);
			intTmp2 = sectionBoardLinkSingleton(section)->m_pPIB->readPressureConversionChannel(j);
#else
			intTmp1 = section * 2 + 100 + i * j;
			intTmp2 = section * 2 + 6000 + i * j;
#endif

			m_strConversions.push_back(std::to_string(intTmp2));
			m_strPressures.push_back(std::to_string(intTmp1));
		}
	}

	m_pLogger->log(LOG_WARNING, "OpGetPressureSettings::perform-->Completed");

	// operation completed -> simply send reply and close it

	compileSendOutCmd();

	return eOperationWaitForDeath;
}

GetPressureSettingsPayload * OpGetPressureSettings::buildPayload(void)
{
	GetPressureSettingsPayload * pGetPressureSettingsPayload = new GetPressureSettingsPayload();

	pGetPressureSettingsPayload->setOutValues(m_strComponent, m_strConversions, m_strPressures,
											  m_strConversionFactor, m_strPressureFactor);

	return pGetPressureSettingsPayload;
}


int OpGetPressureSettings::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdGetPressureSettings cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	GetPressureSettingsPayload * pGetPressureSettingsPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pGetPressureSettingsPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpGetPressureSettings::compileSendOutCmd--> unable to serialize");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}


