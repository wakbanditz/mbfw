/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetMotorActivation.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetMotorActivation class.
 @details

 ****************************************************************************
*/

#ifndef OPGETMOTORACTIVATION_H
#define OPGETMOTORACTIVATION_H

#include "Operation.h"
#include "SectionBoardManager.h"

class GetMotorActivationPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETMOTORACTIVATION command
 * ********************************************************************************************************************
 */
class OpGetMotorActivation : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpGetMotorActivation constructor with two parameter associated
		 * ***********************************************************************************************************
		 */
		OpGetMotorActivation(const string &strComponent, const string &strMotor);

		/*! ***********************************************************************************************************
		 * @brief ~OpGetMotorActivation default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpGetMotorActivation();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @param lNum the index of the sectino (or SCT_NONE in case of NSH)
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	method that builds the payload of the reply and sends it
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

		/*! ***********************************************************************************************************
		 * @brief setAskBoard	to set if the command has to perform a real request to Hardware
		 * @param status true -> action required, false -> no action (default) get data from memory
		 * ************************************************************************************************************
		 */
		void setAskBoard(bool status);

	private:
		/*!
		 * @brief executeGetMotorActivation	Controls what component should be moved and the typology of the received
		 * move command. Then it calls the related function.
		 * @param ubNumSection
		 * @return 0 if success | -1 otherwise
		 */
		int executeGetMotorActivation(uint8_t ubNumSection);

		/*!
		 * @brief executeGetNSHActivation	Controls what component should be moved and the typology of the received
		 * move command. Then it calls the related function.
		 * @return 0 if success | -1 otherwise
		 */
		int executeGetNSHActivation(void);

		/*!
		 * @brief buildPayload	Fills the GetMotorActivationPayload of the outgoing command with related values.
		 * @return  pointer to the GetMotorActivationPayload object just allocated
		 */
		GetMotorActivationPayload * buildPayload(void);

    private:
        std::vector<std::string> m_strComponent;
        std::vector<std::string> m_strMotor;
        std::vector<std::string> m_strActivation;

        uint8_t	m_counterAction;
        bool	m_bAskBoards;

};

#endif // OPGETMOTORACTIVATION_H
