/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpInit.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpInit class.
 @details

 ****************************************************************************
*/

#ifndef OPINIT_H
#define OPINIT_H

#include "Operation.h"
#include "Payload.h"

class InitPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the INIT command
 * ********************************************************************************************************************
 */

class OpInit : public Operation
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OpInit default constructor
		 * ***********************************************************************************************************
		 */
		OpInit();

		/*! ***********************************************************************************************************
		 * @brief ~OpInit default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpInit();

		/*!
		 * @brief setParams	set the parameters for the sending of the messages to the component and motor
		 * requested
		 * @param strComponent
		 */
		void setComponent( const string &strComponent );

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

		/*! ***********************************************************************************************************
		 * @brief notifyCompleted	function to be called in order to notify from a device that the operation
		 *  actions related to the command are completed
		 * @param lNum the device
         * @param bSuccess true if action completed correctly, false otherwise
		 * @return 0
		 * ************************************************************************************************************
		 */
		 int notifyCompleted(int lNum, bool bSuccess);


	private:
		/*! ************************************************************************************************************
		 * @brief buildPayload	Fills the InitPayload of the outgoing command with related values.
		 * @return  pointer to the InitPayload object just allocated
		 * *************************************************************************************************************
		 */
		InitPayload * buildPayload(void);

    private:
        string	m_strComponent;
        vector<uint8_t> m_deviceRunning;

};

#endif // OPINIT_H
