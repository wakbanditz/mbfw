/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpDisable.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpDisable class.
 @details

 ****************************************************************************
*/

#ifndef OPDISABLE_H
#define OPDISABLE_H

#include "Operation.h"

class DisablePayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the DISABLE command
 * ********************************************************************************************************************
 */

class OpDisable : public Operation
{

	public:

		/*! ***********************************************************************************************************
		 * @brief OpDisable default constructor
		 * ***********************************************************************************************************
		 */
		OpDisable();

		/*! ***********************************************************************************************************
		 * @brief ~OpDisable default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpDisable();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief setAction	set the action to perform
		 * @param status true -> disable, false ->enable
		 * ************************************************************************************************************
		 */
		void setAction(bool status);

	private:
		/*!
		 * @brief buildPayload	Fills the DisablePayload of the outgoing command with related values.
		 * @return  pointer to the DisablePayload object just allocated
		 */
		DisablePayload * buildPayload(void);


	private:

		bool m_bDisable;
};

#endif // OPDISABLE_H
