/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetTime.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSetTime class.
 @details

 ****************************************************************************
*/

#ifndef OPSETTIME_H
#define OPSETTIME_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SETTIME command
 * ********************************************************************************************************************
 */
class OpSetTime : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpSetTime default constructor
		 * ***********************************************************************************************************
		 */
		OpSetTime();

		/*! ***********************************************************************************************************
		 * @brief ~OpSetTime default destructor
		 * ************************************************************************************************************
		 */
		~OpSetTime();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief setTimeString sets the Time string
		 * @param strTime the Time string
		 * ************************************************************************************************************
		 */
		void setTimeString(const string& strTime);


	private :

		string m_strTime;

};

#endif // OPSETTIME_H
