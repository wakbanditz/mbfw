/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetMotorActivation.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetMotorActivation class.
 @details

 ****************************************************************************
*/

#include "OpGetMotorActivation.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "OutCmdGetMotorActivation.h"
#include "GetMotorActivationPayload.h"
#include "SectionBoardManager.h"
#include "CommonInclude.h"


OpGetMotorActivation::OpGetMotorActivation(const string &strComponent, const string &strMotor)
	: Operation("GetMotorActivation")
{
	m_strActivation.clear();
	m_strComponent.clear();
	m_strMotor.clear();

	m_counterAction = 0;
	m_bAskBoards = false;

	// build the vectors of actions according to the Component / Motor coming from the command
	std::vector<std::string> listMotorSection, listMotorNsh;

	if( (strMotor.compare(SECTION_PUMP_STRING) == 0) ||
		(strMotor.compare(SECTION_TOWER_STRING) == 0) ||
		(strMotor.compare(SECTION_TRAY_STRING) == 0) ||
		(strMotor.compare(SECTION_SPR_STRING) == 0)   )
	{
		// section devices
		listMotorSection.push_back(strMotor);
	}
	else if( strMotor.compare(DEVICE_NSH_STRING) == 0)
	{
		// nsh device
		listMotorNsh.push_back(strMotor);
	}
	else if( strMotor.empty())
	{
		// all devices
		listMotorSection.push_back(SECTION_PUMP_STRING);
		listMotorSection.push_back(SECTION_TOWER_STRING);
		listMotorSection.push_back(SECTION_TRAY_STRING);
		listMotorSection.push_back(SECTION_SPR_STRING);
		listMotorNsh.push_back(DEVICE_NSH_STRING);
	}

	if(strComponent.empty())
	{
		// in this case the request is on all components
		// if a device is disabled we exclude it from the action
		// build the vectors associating to the Component the corresponding motors
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
		{
			for(uint8_t i = 0; i < listMotorSection.size(); i++)
			{
				m_strComponent.push_back(XML_TAG_SECTIONA_NAME);
				m_strMotor.push_back(listMotorSection.at(i));
				m_strActivation.push_back("");
			}
		}
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
		{
			for(uint8_t i = 0; i < listMotorSection.size(); i++)
			{
				m_strComponent.push_back(XML_TAG_SECTIONB_NAME);
				m_strMotor.push_back(listMotorSection.at(i));
				m_strActivation.push_back("");
			}
		}
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true)
		{
			for(uint8_t i = 0; i < listMotorNsh.size(); i++)
			{
				m_strComponent.push_back(XML_TAG_NSH_NAME);
				m_strMotor.push_back(listMotorNsh.at(i));
				m_strActivation.push_back("");
			}
		}
	}
	else if (strComponent.compare(XML_TAG_SECTIONA_NAME) == 0)
	{
		for(uint8_t i = 0; i < listMotorSection.size(); i++)
		{
			m_strComponent.push_back(XML_TAG_SECTIONA_NAME);
			m_strMotor.push_back(listMotorSection.at(i));
			m_strActivation.push_back("");
		}
	}
	else if (strComponent.compare(XML_TAG_SECTIONB_NAME) == 0)
	{
		for(uint8_t i = 0; i < listMotorSection.size(); i++)
		{
			m_strComponent.push_back(XML_TAG_SECTIONB_NAME);
			m_strMotor.push_back(listMotorSection.at(i));
			m_strActivation.push_back("");
		}
	}
	else if (strComponent.compare(XML_TAG_NSH_NAME) == 0)
	{
		for(uint8_t i = 0; i < listMotorNsh.size(); i++)
		{
			m_strComponent.push_back(XML_TAG_NSH_NAME);
			m_strMotor.push_back(listMotorNsh.at(i));
			m_strActivation.push_back("");
		}
	}
}

OpGetMotorActivation::~OpGetMotorActivation()
{
	m_strActivation.clear();
	m_strComponent.clear();
	m_strMotor.clear();
}

int OpGetMotorActivation::perform()
{
	int8_t res = -1;

	if(m_bAskBoards == false)
	{
		// read data from memory
		for(m_counterAction = 0; m_counterAction < m_strComponent.size(); m_counterAction++)
		{
#ifdef __DEBUG_PROTOCOL
			if ( m_counterAction % 2 )
			{
				m_strActivation.at(m_counterAction).assign(STRING_CURRENT_ENABLED);
			}
			else
			{
				m_strActivation.at(m_counterAction).assign(STRING_CURRENT_DISABLED);
			}
			continue;
#endif

			uint8_t section = 0xFF;

			if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONA_NAME) == 0)
			{
				section = SCT_A_ID;
			}
			else if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONB_NAME) == 0)
			{
				section = SCT_B_ID;
			}
			else if(m_strComponent.at(m_counterAction).compare(XML_TAG_NSH_NAME) == 0)
			{
				section = NSH_ID;
			}


			if(section == SCT_A_ID || section == SCT_B_ID)
			{
				uint8_t disactivation;

				if (m_strMotor.at(m_counterAction).compare(SECTION_TOWER_STRING) == 0)
				{
					disactivation = sectionBoardLinkSingleton(section)->m_pTower->m_CommonMessage.readMotorDisactivation();
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_PUMP_STRING) == 0)
				{
					disactivation = sectionBoardLinkSingleton(section)->m_pPump->m_CommonMessage.readMotorDisactivation();
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_TRAY_STRING) == 0)
				{
					disactivation = sectionBoardLinkSingleton(section)->m_pTray->m_CommonMessage.readMotorDisactivation();
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_SPR_STRING) == 0)
				{
					disactivation = sectionBoardLinkSingleton(section)->m_pSPR->m_CommonMessage.readMotorDisactivation();
				}

				if(disactivation == 0)
				{
					m_strActivation.at(m_counterAction).assign(STRING_CURRENT_ENABLED);
				}
				else
				{
					m_strActivation.at(m_counterAction).assign(STRING_CURRENT_DISABLED);
				}
			}
			else if(section == NSH_ID)
			{
				bool bRes = false;

				if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true)
				{
					bRes = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->isMotorEnabled();
				}

				if ( bRes )
				{
					m_strActivation.at(m_counterAction).assign(STRING_CURRENT_ENABLED);
				}
				else
				{
					m_strActivation.at(m_counterAction).assign(STRING_CURRENT_DISABLED);
				}
			}
		}
		// send back the final reply
		compileSendOutCmd();
		// end of story !
		return eOperationWaitForDeath;
	}

	// start the action on the current item of the vector

	if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONA_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONB_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_strComponent.at(m_counterAction).compare(XML_TAG_NSH_NAME) == 0)
	{
		res = nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
	}

	if(res != eOperationRunning)
	{
		m_pLogger->log(LOG_WARNING, "OpGetMotorActivation::perform-->Error %s", m_strComponent.at(m_counterAction).c_str());
		// operation completed -> send reply and close it
		closeOperation();
	}

//	return res; // to be checked
	return eOperationRunning;
}

GetMotorActivationPayload * OpGetMotorActivation::buildPayload(void)
{
	GetMotorActivationPayload * pGetMotorActivationPayload = new GetMotorActivationPayload();

	pGetMotorActivationPayload->setOutValues(m_strComponent, m_strMotor, m_strActivation);

	return pGetMotorActivationPayload;
}

int OpGetMotorActivation::executeGetMotorActivation(uint8_t ubNumSection)
{
	SectionCommonMessage*	pCommonMessage = NULL;

	//choice of the component to move
	if (m_strMotor.at(m_counterAction).compare(SECTION_TOWER_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTower->m_CommonMessage;
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_PUMP_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pPump->m_CommonMessage;
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_TRAY_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTray->m_CommonMessage;
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_SPR_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pSPR->m_CommonMessage;
	}

	uint8_t  ubActivation = 0;

	if(pCommonMessage->getMotorActivation(ubActivation) == 0)
	{
		if(ubActivation == '0')
		{
			m_strActivation.at(m_counterAction).assign(STRING_CURRENT_ENABLED);
		}
		else
		{
			m_strActivation.at(m_counterAction).assign(STRING_CURRENT_DISABLED);
		}
		return 0;
	}
	return -1;
}

int OpGetMotorActivation::executeGetNSHActivation(void)
{
	bool bRes = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->isMotorEnabled();

	if ( bRes )
	{
		m_strActivation.at(m_counterAction).assign(STRING_CURRENT_ENABLED);
	}
	else
	{
		m_strActivation.at(m_counterAction).assign(STRING_CURRENT_DISABLED);
	}

	return 0;
}

int OpGetMotorActivation::executeCmd(int lNum)
{
	//action to perform
	int res = 0;
	if( lNum == NSH_ID )
	{
		res = executeGetNSHActivation();
	}
	else
	{
		res = executeGetMotorActivation(lNum);
	}

	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "OpGetMotorActivation::execute-->execute not performed");
		// operation completed -> send reply and close it
		closeOperation();
		return -1;
	}

	std::string previousComponent;
	previousComponent.assign(m_strComponent.at(m_counterAction));

	// increase the action counter
	m_counterAction++;

	// if we have performed the last action compose and send the reply, stop the loop
	if(m_counterAction >= m_strComponent.size() )
	{
		// operation completed -> send reply and close it
		closeOperation();
		res = -1;
	}
	else if(previousComponent.compare(m_strComponent.at(m_counterAction)) != 0)
	{
		// actions on the current device are completed:
		// --> go for next action
		perform();
		// -> unlock the actual
		res = -1;
	}

	m_pLogger->log(LOG_INFO, "OpGetMotorActivation::execute-->completed");

	return res;
}


int OpGetMotorActivation::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdGetMotorActivation cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	GetMotorActivationPayload * pGetMotorActivationPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pGetMotorActivationPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpGetMotorActivation::execute--> unable to serialize OutCmdGetMotorActivation");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

void OpGetMotorActivation::setAskBoard(bool status)
{
	m_bAskBoards = status;
}
