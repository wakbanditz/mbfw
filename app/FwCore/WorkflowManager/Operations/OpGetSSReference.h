/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetSSReference.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetSSReference class.
 @details

 ****************************************************************************
*/

#ifndef OPGETSSREFERENCE_H
#define OPGETSSREFERENCE_H

#include "Operation.h"
#include "Loggable.h"

class GetSSReferencePayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETSSREFERENCE command
 * ********************************************************************************************************************
 */

class OpGetSSReference : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpGetSSReference default constructor
		 * ***********************************************************************************************************
		 */
		OpGetSSReference();

		/*! ***********************************************************************************************************
		 * @brief ~OpGetSSReference default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpGetSSReference();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*!
		 * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
		 * then it composes the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 */
		int compileSendOutCmd();

	private:

		/*!
		 * @brief buildPayload	Fills the GetSSReferencePayload of the outgoing command with related values.
		 * @return  pointer to the GetSSReferencePayload object just allocated
		 */
		GetSSReferencePayload* buildPayload(void);

    private:

        string m_strRFUval;
        string m_strGain;

};

#endif // OPGETSSREFERENCE_H
