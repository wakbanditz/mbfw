/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpLogin.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpLogin class.
 @details

 ****************************************************************************
*/

#include "OpLogin.h"
#include "OutCmdLogin.h"
#include "THAutoLogout.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpLogin::OpLogin() : Operation("OpLogin")
{
	m_strLoginUsage.clear();
}

OpLogin::~OpLogin()
{
}

int OpLogin::perform(void)
{
	// no action is required -> read data from memory and send them back to SW

	// register the login linked to the ip address + port
	if(!m_strLoginUsage.empty() && !m_strWebResponseUrl.empty())
	{
		if(MainExecutor::getInstance()->m_loginManager.addLogin(m_strLoginUsage, m_strWebResponseUrl))
		{
			// login successfull -> start autoLogout timer
			THAutoLogout::getInstance()->startConnection(m_strLoginUsage);
			// and request the VidasEp sending
			requestVidasEp();
			// and also check if the RUN reply has to be resent
			protocolThreadPointer()->repeatRunwlReply(m_strWebResponseUrl);
		}
	}

	OutCmdLogin outCmdLogin;
	string strOutCommand;

	outCmdLogin.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdLogin.setUsage(getUsage());
	outCmdLogin.setID(getID());

	protocolSerialize(&outCmdLogin, strOutCommand);

	sendCommandReply(strOutCommand);

	return eOperationWaitForDeath;
}

void OpLogin::setLoginUsage(std::string strLoginUsage)
{
	m_strLoginUsage.assign(strLoginUsage);
}
