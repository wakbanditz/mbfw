/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpMove.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpMove class.
 @details

 ****************************************************************************
*/

#include "OpMove.h"
#include "SectionBoardManager.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "OutCmdMove.h"
#include "MovePayload.h"
#include "CommonInclude.h"

#define NSHR_DEF_MOVEMENT		100

/*! *******************************************************************************************************************
 * Class implementation
 * ********************************************************************************************************************
 */
OpMove::OpMove(const string &strComponent,
			   const string &strMotor,
			   const string &strMove,
			   const string &strMoveValue)
				: Operation("OpMove")
{
	m_strComponent.assign(strComponent);
	m_strMotor.assign(strMotor);
	m_strMove.assign(strMove);
	m_strMoveValue.assign(strMoveValue);

	m_strNumSection.clear();
	m_strPosition.clear();

}

OpMove::~OpMove()
{
	/* Nothing to do yet */
}

int OpMove::perform()
{	
	if ( m_strComponent.compare(XML_TAG_SECTIONA_NAME) == 0 )
	{
		m_ubNumSection = SCT_A_ID;
		m_strNumSection = m_strComponent;
		return sectionBoardManagerSingleton(m_ubNumSection)->setSectionAction(SECTION_ACTION_CMD, (Operation*)this);
	}
	else if ( m_strComponent.compare(XML_TAG_SECTIONB_NAME) == 0 )
	{
		m_ubNumSection = SCT_B_ID;
		m_strNumSection = m_strComponent;
		return sectionBoardManagerSingleton(m_ubNumSection)->setSectionAction(SECTION_ACTION_CMD, (Operation*)this);
	}
	else if ( m_strComponent.compare(XML_TAG_NSH_NAME) == 0 )
	{
		return MainExecutor::getInstance()->m_NSHBoardManager.setNSHAction(eNSHActionCmd, (Operation*)this);
	}
	else
	{
		m_pLogger->log(LOG_WARNING, "OpMove::perform-->Component %s not in Move list", m_strComponent.c_str());
		return eOperationWaitForDeath;
	}
}

int OpMove::executeMoveSectionPredefined(uint8_t ubNumSection)
{
	SectionCommonMessage*	pCommonMessage = NULL;
	std::string strCoded;

	strCoded.clear();

	if (m_strMotor.compare(SECTION_TOWER_STRING) == 0)
	{
		strCoded = sectionBoardLinkSingleton(ubNumSection)->m_pTower->getCodedFromLabel(m_strMoveValue);
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTower->m_CommonMessage;
	}
	else if (m_strMotor.compare(SECTION_PUMP_STRING) == 0)
	{
		strCoded = sectionBoardLinkSingleton(ubNumSection)->m_pPump->getCodedFromLabel(m_strMoveValue);
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pPump->m_CommonMessage;
	}
	else if (m_strMotor.compare(SECTION_TRAY_STRING) == 0)
	{
		strCoded = sectionBoardLinkSingleton(ubNumSection)->m_pTray->getCodedFromLabel(m_strMoveValue);
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTray->m_CommonMessage;
	}
	else if (m_strMotor.compare(SECTION_SPR_STRING) == 0)
	{
		strCoded = sectionBoardLinkSingleton(ubNumSection)->m_pSPR->getCodedFromLabel(m_strMoveValue);
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pSPR->m_CommonMessage;
	}
	else
	{
		m_pLogger->log(LOG_WARNING, "OpMove::executeMoveSectionPredefined: Motor %s not in List", m_strMotor.c_str());
		return -1;
	}

	if(strCoded.empty())
	{
		m_pLogger->log(LOG_WARNING, "OpMove::executeMoveSectionPredefined: Coded not found", m_strMoveValue.c_str());
		return -1;
	}

	char ubPosCode = *strCoded.c_str();
	m_pLogger->log(LOG_INFO, "OpMove::executeMoveSectionPredefined: CMD= %X", ubPosCode);
	return pCommonMessage->moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
}

int OpMove::executeMoveSection(uint8_t ubNumSection)
{
	SectionCommonMessage*	pCommonMessage = NULL;
	int slSteps = 0;
	sscanf(m_strMoveValue.c_str(),"%d",&slSteps);

	//choice of the component to move
	if (m_strMotor.compare(SECTION_TOWER_STRING) == 0)
	{
		pCommonMessage = &MainExecutor::getInstance()->m_sectionBoardLink[ubNumSection].m_pTower->m_CommonMessage;
	}
	else if (m_strMotor.compare(SECTION_PUMP_STRING) == 0)
	{
		pCommonMessage = &MainExecutor::getInstance()->m_sectionBoardLink[ubNumSection].m_pPump->m_CommonMessage;
	}
	else if (m_strMotor.compare(SECTION_TRAY_STRING) == 0)
	{
		pCommonMessage = &MainExecutor::getInstance()->m_sectionBoardLink[ubNumSection].m_pTray->m_CommonMessage;
	}
	else if (m_strMotor.compare(SECTION_SPR_STRING) == 0)
	{
		pCommonMessage = &MainExecutor::getInstance()->m_sectionBoardLink[ubNumSection].m_pSPR->m_CommonMessage;
	}
	else
	{
		m_pLogger->log(LOG_WARNING, "OpMove::executeMoveSection: Motor %s not in Move List", m_strMotor.c_str());
		return -1;
	}

	//choice of the movement to execute
	if (m_strMove.compare("Home") == 0)
	{
		// HOME
        pCommonMessage->searchHome();
	}
	else if (m_strMove.compare("Relative") == 0)
	{
		// RELATIVE
		enumSectionCommonMsgStepDir eDir;
		uint16_t	usSteps = 0;
		if (slSteps < 0)
		{
			usSteps = (uint16_t)-slSteps;
			eDir = eDecreasePosition;
		}
		else
		{
			usSteps = (uint16_t)slSteps;
			eDir = eIncreasePosition;
		}

		pCommonMessage->setNumberRelativeSteps(usSteps, TH_MOVE_CMD_TIMEOUT);
		pCommonMessage->startMotor(eDir, TH_MOVE_CMD_TIMEOUT);
	}
	else if (m_strMove.compare("Absolute") == 0)
	{
		// ABSOLUTE
		pCommonMessage->moveToAbsolutePosition(slSteps, TH_MOVE_CMD_TIMEOUT);
	}
	else if (m_strMove.compare("Predefined") == 0)
	{
		// PREDEFINED
		executeMoveSectionPredefined(ubNumSection);
	}
	else
	{
		m_pLogger->log(LOG_WARNING, "OpMove::executeMoveSection-->Move %s not in Move list", m_strMove.c_str());
		return -1;
	}

	int32_t slValue;
	pCommonMessage->getAbsolutePosition(slValue, TH_MOVE_CMD_TIMEOUT);
	m_strPosition = to_string(slValue);

	return 0;
}

int OpMove::executeMoveNSH(void)
{
	if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHMotorEnabled() )
	{
		m_pLogger->log(LOG_WARNING, "OpMove::executeMoveNSH: NSH motor not enabled");
		return -1;
	}

	int liSteps = 0;
	sscanf(m_strMoveValue.c_str(),"%d",&liSteps);

	// NSH is the only component admitted
	if ( m_strMotor.compare(XML_TAG_MOTOR_NAME_NSH) != 0 )
	{
		m_pLogger->log(LOG_WARNING, "OpMove::executeMoveNSH: Motor %s not in Move List", m_strMotor.c_str());
		return -1;
	}

	eNSHMotorPositions ePos = eNone;

	if ( m_strMove.compare("Home") == 0 )
	{
		ePos = eHome;
	}
	else if ( m_strMove.compare("Relative") == 0 )
	{
		ePos = eRelPos;
	}
	else if ( m_strMove.compare("Absolute") == 0 )
	{
		ePos = eAbsPos;
	}
	else if ( m_strMove.compare("Predefined") == 0 )
	{
		// Get predefined value from table and save it in liSteps
		getNSHCodedSteps(liSteps);
		ePos = eAbsPos;
	}
	else
	{
		ePos = eNone;
		m_pLogger->log(LOG_WARNING, "OpMove::executeMoveNSH-->Move %s not in Move list", m_strMove.c_str());
	}

	if ( ! MainExecutor::getInstance()->m_SPIBoard.move(ePos, liSteps) )	return -1;

	int32_t liValue;
	MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getAbsPosition(liValue);
	liValue /= MOT_HALF_TO_MICRO_STEPS;
	m_strPosition = to_string(-liValue);
	m_strNumSection = m_strComponent;

	return 0;
}

int OpMove::getNSHCodedSteps(int& liSteps)
{
	string strStepsVal("");
	bool bRes = nshBoardManagerSingleton()->m_pConfig->getItemValue(m_strMoveValue, strStepsVal);
	if ( ! bRes )
	{
		return -1;
	}
	liSteps = stoi(strStepsVal);

	return 0;
}

MovePayload* OpMove::buildPayload(void)
{
	structMoveCmdOutValues moveCmdValues;

	MovePayload * pMovePayload = new MovePayload();

	moveCmdValues.strComponent = m_strNumSection;
	moveCmdValues.strMotor	= m_strMotor;
	moveCmdValues.strPosition = m_strPosition;

    pMovePayload->setOutValuesStruct(moveCmdValues);

	return pMovePayload;
}

int OpMove::executeCmd(int lNum)
{

#ifdef __DEBUG_PROTOCOL
		m_strPosition = to_string((lNum + 1) * 100);
		return 0;
#endif


	int liRes = -1;

	switch ( lNum )
	{
		case SCT_A_ID:
		case SCT_B_ID:
			m_ubNumSection = lNum;

			//action to perform
			liRes = executeMoveSection(m_ubNumSection);
			if ( liRes != 0 )
			{
				m_pLogger->log(LOG_ERR, "OpMove::executeMoveSection-->execute not performed");
				return -1;
			}
		break;

		default: // NSH
			liRes = executeMoveNSH();
			if ( liRes != 0 )
			{
				m_pLogger->log(LOG_ERR, "OpMove::executeMoveNSH-->execute not performed");
				return -1;
			}
		break;
	}

	m_pLogger->log(LOG_INFO, "OpMove::execute-->MOVE executed");

	return 0;
}

int OpMove::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdMove cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	MovePayload * pMovePayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pMovePayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpMove::execute--> unable to serialize OutCmdMove");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}
	return eOperationEnabledForDeath;
}


