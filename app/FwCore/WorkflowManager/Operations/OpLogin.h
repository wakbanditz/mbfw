/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpLogin.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpLogin class.
 @details

 ****************************************************************************
*/

#ifndef OPLOGIN_H
#define OPLOGIN_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the LOGIN command
 * ********************************************************************************************************************
 */

class OpLogin : public Operation
{

	public:

		/*! ***********************************************************************************************************
		 * @brief OpLogin default constructor
		 * ***********************************************************************************************************
		 */
		OpLogin();

		/*! ***********************************************************************************************************
		 * @brief ~OpLogin default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpLogin();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief setLoginUsage	set the Login string as received in the incoming command
         * @param strLoginUsage the type of connection (COMMERCIAL vs SERVICE) as string
		 * ************************************************************************************************************
		 */
		void setLoginUsage(std::string strLoginUsage);

	private:
		std::string m_strLoginUsage;
};

#endif // OPLOGIN_H
