/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetSSReference.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetSSReference class.
 @details

 ****************************************************************************
*/

#include "OpGetSSReference.h"
#include "MainExecutor.h"
#include "OutCmdGetSSReference.h"
#include "WebServerAndProtocolInclude.h"
#include "GetSSReferencePayload.h"

OpGetSSReference::OpGetSSReference() : Operation("OpGetSSReference")
{
	m_strGain.clear();
	m_strRFUval.clear();
}

OpGetSSReference::~OpGetSSReference()
{
	/* Nothing to do yet */
}

int OpGetSSReference::perform()
{

	// Get the stored SS RFU value
    m_strRFUval = infoSingleton()->getSSvalue();
	// Get NSH gain
    m_strGain = infoSingleton()->getSSgain();

    return compileSendOutCmd();
}

int OpGetSSReference::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdGetSSReference cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	GetSSReferencePayload * pGetSSReferencePayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pGetSSReferencePayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSSCalibrate::execute--> unable to serialize OpGetSSReference");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}
    return eOperationWaitForDeath;
}

GetSSReferencePayload* OpGetSSReference::buildPayload()
{
	GetSSReferencePayload * pGetSSReferencePayload = new GetSSReferencePayload();

	pGetSSReferencePayload->setParameters(m_strRFUval, m_strGain);

	return pGetSSReferencePayload;
}
