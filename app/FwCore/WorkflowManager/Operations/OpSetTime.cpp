/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetTime.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSetTime class.
 @details

 ****************************************************************************
*/

#include "OpSetTime.h"
#include "OutCmdSetTime.h"

#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

#include <ctime>

OpSetTime::OpSetTime() : Operation("OpSetTime")
{
	m_strTime.clear();
}

OpSetTime::~OpSetTime()
{

}

void OpSetTime::setTimeString(const string& strTime)
{
	m_strTime.assign(strTime);
}

int OpSetTime::perform(void)
{
    if(m_strTime.empty() == false)
	{
		int year, month, day, hour, minute, seconds;

		sscanf(m_strTime.c_str(), "%04d-%02d-%02dT%02d:%02d:%02d",
				  &year, &month, &day, &hour, &minute, &seconds);

        struct tm *currentTime;
        time_t rawtime;

        // get current timeinfo and modify it to the user's choice
        time ( &rawtime );
        currentTime = localtime ( &rawtime );
        currentTime->tm_year = year - 1900;
        currentTime->tm_mon = month - 1;
        currentTime->tm_mday = day;
        currentTime->tm_hour = hour;
        currentTime->tm_min = minute;
        currentTime->tm_sec = seconds;

        time_t t = mktime(currentTime);

		if(t != (time_t)(-1))
		{
            // change system time
            char bufferTmp[32];
            sprintf(bufferTmp, "date -s '%02d/%02d/%02d %02d:%02d:%02d'",
                    month, day, year, hour, minute, seconds);
            system((const char *)bufferTmp);
            m_pLogger->log(LOG_INFO, "OpSetTime::perform-->%s", bufferTmp);

            // stime(&t); -> requires root provilege

			uint64_t currentMsec = getClockFromStart();
			// register the date is now valid
			infoSingleton()->setSetTime(true);
			// update timing of error list
			int counter = 0;

            counter += sectionBoardManagerSingleton(SCT_A_ID)->updateErrorsTime(currentMsec, currentTime);
            counter += sectionBoardManagerSingleton(SCT_B_ID)->updateErrorsTime(currentMsec, currentTime);
            counter += nshBoardManagerSingleton()->updateErrorsTime(currentMsec, currentTime);
            counter += cameraBoardManagerSingleton()->updateErrorsTime(currentMsec, currentTime);

			if(counter > 0)
			{
				// send the updated errors list
				requestVidasEp();
			}
		}
	}

	OutCmdSetTime cmd;
	string strOutCommand;

	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// no payload to build

	protocolSerialize(&cmd, strOutCommand);

	restoreStateMachine();

	sendCommandReply(strOutCommand);

	return eOperationWaitForDeath;
}


