/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpLogout.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpLogout class.
 @details

 ****************************************************************************
*/

#ifndef OPLOGOUT_H
#define OPLOGOUT_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the LOGOUT command
 * ********************************************************************************************************************
 */

class OpLogout : public Operation
{

	public:

		/*! ***********************************************************************************************************
		 * @brief OpLogout default constructor
		 * ***********************************************************************************************************
		 */
		OpLogout();

		/*! ***********************************************************************************************************
		 * @brief ~OpLogout default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpLogout();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);
};

#endif // OPLOGOUT_H
