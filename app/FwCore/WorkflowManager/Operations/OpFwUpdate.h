/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpFwUpdate.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpFwUpdate class.
 @details

 ****************************************************************************
*/

#ifndef OPFWUPDATE_H
#define OPFWUPDATE_H

#include "Operation.h"
#include "FWUpdateCommonInclude.h"
#include "FWUpdateEEprom.h"

class FwUpdatePayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the FWUODATE command
 * ********************************************************************************************************************
 */

class OpFwUpdate : public Operation
{

	public:

		/*! ***********************************************************************************************************
		 * @brief OpFwUpdate default constructor
		 * ***********************************************************************************************************
		 */
		OpFwUpdate();

		/*! ***********************************************************************************************************
		 * @brief ~OpFwUpdate default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpFwUpdate();

		/*! ***********************************************************************************************************
		 * @brief setParamsFromCmd set class parameters
		 * ************************************************************************************************************
		 */
		void setParamsFromCmd(FwUpdateContainer* pContainer);

		/*! ***********************************************************************************************************
		 * @brief setParamsFromManager needed to tell the various manager file length, crc32, fw type, source path etc.
		 * @param vFwInfo
		 * ************************************************************************************************************
		 */
		void setParamsFromManager(FwUpdateInfo& vFwInfo);

		void setEepromParamsFromManager(FwUpdateEEprom& fwEeprom);

		/*! ***********************************************************************************************************
		 * @brief getLastFwUpdateInfo
		 * @return
		 * ************************************************************************************************************
		 */
		FwUpdateInfo* getLastDeviceInfoForUpd(void);

		FwUpdateEEprom* getEepromInfoForUpd(void);

		int addItemToLog(eDeviceId eId, enumUpdType eType);

		int writeUpdOutcomeLog(eDeviceId eId, enumUpdType eType, bool bIsSuccess);

		int parseLog(void);

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*!
		 * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
		 *		  then it composes the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 */
		int compileSendOutCmd(void);

        /*!
         * @brief compileSendProgressCmd	set the status as ACCEPTED and includes in reply only the progress percentage
         * @param percentage the progress percentage
         * @return true if success
         */
        bool compileSendProgressCmd(uint8_t percentage);


	private:

        FwUpdatePayload* buildPayload(uint8_t percentage = 0);

		char* getFwTypeFromEnum(enumUpdType eType);
		char* getDeviceFromEnum(eDeviceId eId);

    private:

        FwUpdateContainer m_FwContainer;
        vector<FwUpdateInfo> m_vFwInfo;
        FwUpdateEEprom m_FwEeprom;

        vector<string> m_vDevUpd;

};

#endif // OPFWUPDATE_H
