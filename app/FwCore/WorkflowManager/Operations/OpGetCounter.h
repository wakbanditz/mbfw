/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetCounter.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetCounter class.
 @details

 ****************************************************************************
*/

#ifndef OPGETCOUNTER_H
#define OPGETCOUNTER_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETCOUNTER command
 * ********************************************************************************************************************
 */
class OpGetCounter : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpGetCounter default constructor
		 * ***********************************************************************************************************
		 */
		OpGetCounter();

		/*! ***********************************************************************************************************
		 * @brief ~OpGetCounter default destructor
		 * ************************************************************************************************************
		 */
		~OpGetCounter();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! *************************************************************************************************
		 * @brief setCounter	Sets the name of the counter
		 * @param strCounter the label string
		 * *************************************************************************************************
		 */
		void setCounter(const string& strCounter);

	private :

		std::string m_strCounter;
};

#endif // OPGETCOUNTER_H
