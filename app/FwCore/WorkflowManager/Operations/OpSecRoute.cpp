/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSecRoute.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSecRoute class.
 @details

 ****************************************************************************
*/

#include "OpSecRoute.h"
#include "MainExecutor.h"
#include "OutCmdSecRoute.h"
#include "SecRoutePayload.h"
#include "WebServerAndProtocolInclude.h"

int nDecodeTimeOutMsec = 0;

OpSecRoute::OpSecRoute() : Operation("OpSecRoute")
{
	m_strCommand.clear();
	m_strReply.clear();
	m_sectionRoute = -1;
}

OpSecRoute::~OpSecRoute()
{

}

int OpSecRoute::perform(void)
{
	if(m_sectionRoute == -1)
	{
		return eOperationError;
	}

	if(m_sectionRoute == SCT_A_ID || m_sectionRoute == SCT_B_ID)
	{
		return sectionBoardManagerSingleton(m_sectionRoute)->setSectionAction(SECTION_ACTION_CMD, (Operation*)this);
	}
	else if(m_sectionRoute == NSH_ID)
	{
		return nshBoardManagerSingleton()->setNSHAction(eNSHActionCmd, (Operation*)this);
	}
	else if(m_sectionRoute == CAMERA_ID)
	{
		return cameraBoardManagerSingleton()->setCameraAction(eCameraActionCmd, (Operation*)this);
	}
	else if (m_sectionRoute == INSTRUMENT_ID)
	{
		return masterBoardManagerSingleton()->setMasterAction(eMasterActionCmd, (Operation*)this);
	}
	return eOperationWaitForDeath;
}

void OpSecRoute::setParameters(int section, std::string strCommand)
{
	m_strCommand.assign(strCommand);
	m_sectionRoute = section;

    // Message comes out with double quotes
    if (m_strCommand.back() == '"' ) m_strCommand.pop_back();
    if (m_strCommand.front() == '"') m_strCommand.erase(0, 1);

}

int OpSecRoute::getSection()
{
	return m_sectionRoute;
}

std::string OpSecRoute::getCommand()
{
	return m_strCommand;
}

int OpSecRoute::executeCmd(int lNum)
{
	if ( lNum == NSH_ID )
	{
		executeNSHRoute();
	}
	else if ( lNum == CAMERA_ID )
	{
		executeCameraRoute();
	}
	else if ( lNum == INSTRUMENT_ID )
	{
		executeMasterRoute();
	}
	else
	{
		// send the command to the section and wait for the accepted / rejected (timeout = 30 secs)
		m_strReply = MainExecutor::getInstance()->m_sectionBoardLink[m_sectionRoute].sendSecRouteMsg(m_strCommand, 30000);
	}
	return 0;
}

int OpSecRoute::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdSecRoute cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	SecRoutePayload * pSecRoutePayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pSecRoutePayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************************************************
	 */
	std::string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "SECROUTE: unable to serialize OutCmdSecRoute");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}
	return eOperationEnabledForDeath;
}

SecRoutePayload * OpSecRoute::buildPayload()
{
	SecRoutePayload * pSecRoutePayload = new SecRoutePayload();

	pSecRoutePayload->setComponent(m_sectionRoute);
	pSecRoutePayload->setSectionReply(m_strReply);

	return pSecRoutePayload;
}

bool OpSecRoute::executeNSHRoute()
{
	if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled() )	return false;

	m_strReply.clear();
	string strAns("");
	int8_t cRes = MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.nshRouteCmd(m_strCommand, strAns);
	if ( cRes )
	{
		m_pLogger->log(LOG_INFO, "OpSecRoute::executeCameraRoute-->Error performing camRouteCmd");
		return false;
	}

	m_strReply.assign(strAns);
	return true;
}

bool OpSecRoute::executeCameraRoute()
{
	if ( ! stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdCamera) )	return false;

	uint32_t uliTimeOutMsec = 100;

	// this is needed to be able to store the decoding timeout
	string strDecodeTimeoutCmd("CDOP*AT"); // CDOP*AT* actually
	string subStrCmd(m_strCommand, 0, strDecodeTimeoutCmd.size());
	if ( compareWithWildCard(strDecodeTimeoutCmd, subStrCmd, '*') )
	{
		// To be sure is a set with a number and not a get
		if ( m_strCommand.size() > strDecodeTimeoutCmd.size() )
		{
			nDecodeTimeOutMsec = stoi(m_strCommand.substr(strDecodeTimeoutCmd.size()));
			usbReaderSingleton()->setDecodingTimeoutMs(nDecodeTimeOutMsec);
		}
	}
	else if ( ! m_strCommand.compare("RDCMXEV7") )
	{
		uliTimeOutMsec = nDecodeTimeOutMsec;
	}

	// this is needed because getting pictures is slower than the other simple commands
	else if ( ! m_strCommand.compare("CDTPXEV1") )
	{
		uliTimeOutMsec = 1200;
    }

    vector<unsigned char> vAns;
    int8_t cRes = MainExecutor::getInstance()->m_USBBoard.m_pCR8062->m_Camera.camRouteCmd(m_strCommand, vAns, uliTimeOutMsec);
	if ( cRes )
	{
		m_pLogger->log(LOG_INFO, "OpSecRoute::executeCameraRoute-->Error performing camRouteCmd");
		return false;
	}

	m_strReply.assign(vAns.begin(), vAns.end());
	return true;
}

bool OpSecRoute::executeMasterRoute()
{
	// Remove white spaces
	string strTmp;
	for ( auto c : m_strCommand )
	{
		if ( c != ' ')	strTmp.push_back(c);
	}
	m_strCommand.assign(strTmp);

	// Check msg correctness
	// m_strCommand can only be: shift=x; BCshiftA=x; BCshiftB=x; getpos(*element*).
	size_t pos = m_strCommand.find("=");
	if ( m_strCommand.find("=") != string::npos )
	{
		bool bRes = false;
		string strCmd = m_strCommand.substr(0, pos);
		int shift = stoi(m_strCommand.substr(pos+1));

		if ( ! strCmd.compare("shift") )
		{
			vector<pair<string, string>> vConfig;
			nshBoardManagerSingleton()->m_pConfig->getNSHConfiguration(vConfig);
			for ( auto it = vConfig.begin(); it != vConfig.end(); ++it )
			{
				if ( it->first.find("Slot") != string::npos )	continue;
				if ( it->first.find("SS") != string::npos )		continue;

				auto pos = it->first.find("_");
				if ( pos == string::npos )						continue;

				int liSlot = stoi(it->first.substr(pos+1));
				int realShift = ( liSlot > 6 ) ? shift : -shift;

				int liPosition = stoi(it->second) + realShift;
				if ( liPosition < 0 ||  liPosition > 5650 )
				{
					m_strReply = "FAILURE: " + it->first + " Position desired too high: " + to_string(liPosition);
					return true;
				}
				it->second = to_string(liPosition);
			}
			bRes = nshBoardManagerSingleton()->m_pConfig->updateNSHConfigurations(vConfig);
		}
		else if ( ! strCmd.compare("BCshift") && shift >= 0 )
		{
			vector<pair<string, string>> vConfig;
			nshBoardManagerSingleton()->m_pConfig->getNSHConfiguration(vConfig);
			for ( auto it = vConfig.begin(); it != vConfig.end(); ++it )
			{
				string target("CameraBC_");
				size_t start = it->first.find(target);
				if ( start != string::npos )
				{
					int liSlot = stoi(it->first.substr(start+target.size()));

					// Depending on the section I need to move in different directions
					int realShift = ( liSlot > 6 ) ? shift : -shift;
					it->second = to_string(stoi(it->second) + realShift);
// TODO L8R
//					// Move ROI
//					float fShiftROI = float(shift) * 135.0f / 360.0f;

//					structRect rect;
//					string strRoiName = "BC_" + to_string(liSlot);
//					cameraBoardManagerSingleton()->m_pConfig->getItemValue(strRoiName, rect);
//					rect.x += int(fShiftROI);
//					if ( rect.x < 10 )
//					{
//						m_strReply = "FAILURE: " + strRoiName + " ROI.x=" + to_string(rect.x);
//						return true;
//					}
//					cameraBoardManagerSingleton()->m_pConfig->setItemValue(strRoiName, rect);
				}
			}
			bRes = nshBoardManagerSingleton()->m_pConfig->updateNSHConfigurations(vConfig);
//			cameraBoardManagerSingleton()->m_pConfig->updateCameraConfigFile();
		}

		if ( ! bRes )	m_strReply = "Invalid command";
		else			m_strReply = "DONE";
		return true;
	}

	pos = m_strCommand.find("getpos");
	if ( pos != string::npos )
	{
		size_t start = m_strCommand.find("(");
		size_t end = m_strCommand.find(")");
		if ( start != string::npos && end != string::npos )
		{
			string strLabel = m_strCommand.substr(m_strCommand.find("(")+1, end-start-1);
			string strValue("");
			bool bRes = nshBoardManagerSingleton()->m_pConfig->getItemValue(strLabel, strValue);
			if ( ! bRes )
			{
				m_strReply = "Invalid predefined item";
				return true;
			}

			m_strReply = "Position is: " + strValue;
			return true;
		}
	}

	m_strReply = "Invalid command";
	return true;
}

bool OpSecRoute::compareWithWildCard(const string& str1, const string& str2, char cWildCardChar)
{
	if ( str1.size() != str2.size() )	return false;

	int liSize = str1.size();

	for ( int i = 0; i != liSize; i++ )
	{
		if ( str1[i] != cWildCardChar )
		{
			if ( str1[i] != str2 [i] )
			{
				return false;
			}
		}
	}
	return true;
}
