/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSaveCalibration.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSaveCalibration class.
 @details

 ****************************************************************************
*/

#include "OpSaveCalibration.h"
#include "OutCmdSaveCalibration.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "SectionBoardManager.h"
#include "SaveCalibrationPayload.h"
#include "CommonInclude.h"


OpSaveCalibration::OpSaveCalibration() : Operation("OpSaveCalibration")
{
	m_vecSaveCalibration.clear();
	m_counterAction = 0;
}

OpSaveCalibration::~OpSaveCalibration()
{
	m_vecSaveCalibration.clear();
}

int OpSaveCalibration::setValueCalibration(vector<structSaveCalibration> &stCal)
{
	m_vecSaveCalibration = stCal;
	return 0;
}

int OpSaveCalibration::perform()
{
	int8_t res = -1;

	// start the action on the current item of the vector

	if(m_vecSaveCalibration.at(m_counterAction).strComponent.compare(XML_TAG_SECTIONA_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_vecSaveCalibration.at(m_counterAction).strComponent.compare(XML_TAG_SECTIONB_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_vecSaveCalibration.at(m_counterAction).strComponent.compare(XML_TAG_NSH_NAME) == 0)
	{
		res = nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
	}

	if(res != eOperationRunning)
	{
		m_pLogger->log(LOG_WARNING, "OpSaveCalibration::perform-->Error %s",
					   m_vecSaveCalibration.at(m_counterAction).strComponent.c_str());
		// operation completed -> send reply and close it
		closeOperation();
	}

	// the return code has to be eOperationRunning to prevent PostMan to delete the Operation:
	// if necessary it's the closeOperation that has done it
	return eOperationRunning;
}

SaveCalibrationPayload * OpSaveCalibration::buildPayload(void)
{
	SaveCalibrationPayload * pSaveCalibrationPayload = new SaveCalibrationPayload();

	for (size_t i = 0; i < m_vecSaveCalibration.size(); i++)
	{
		//setOutValuePayload
		structSaveCalibration & stSaveCalib = m_vecSaveCalibration.at(i);
		pSaveCalibrationPayload->setOutElement(stSaveCalib);
	}
	return pSaveCalibrationPayload;
}

int OpSaveCalibration::executeCalibration(int ubNumSection)
{
	SectionCommonMessage*	pCommonMessage = NULL;
	std::string strCalibration;

	strCalibration.clear();

	//choice of the component to move
	if (m_vecSaveCalibration.at(m_counterAction).strMotor.compare(SECTION_TOWER_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTower->m_CommonMessage;
		strCalibration = sectionBoardLinkSingleton(ubNumSection)->m_pTower->getCalibrationFromLabel(m_vecSaveCalibration.at(m_counterAction).strLabel);
	}
	else if (m_vecSaveCalibration.at(m_counterAction).strMotor.compare(SECTION_PUMP_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pPump->m_CommonMessage;
		strCalibration = sectionBoardLinkSingleton(ubNumSection)->m_pPump->getCalibrationFromLabel(m_vecSaveCalibration.at(m_counterAction).strLabel);
	}
	else if (m_vecSaveCalibration.at(m_counterAction).strMotor.compare(SECTION_TRAY_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTray->m_CommonMessage;
		strCalibration = sectionBoardLinkSingleton(ubNumSection)->m_pTray->getCalibrationFromLabel(m_vecSaveCalibration.at(m_counterAction).strLabel);
	}
	else if (m_vecSaveCalibration.at(m_counterAction).strMotor.compare(SECTION_SPR_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pSPR->m_CommonMessage;
		strCalibration = sectionBoardLinkSingleton(ubNumSection)->m_pSPR->getCalibrationFromLabel(m_vecSaveCalibration.at(m_counterAction).strLabel);
	}

	if(strCalibration.empty())
	{
		m_pLogger->log(LOG_WARNING, "OpSaveCalibration: Coded not found", m_vecSaveCalibration.at(m_counterAction).strLabel.c_str());
		return -1;
	}

	int ret;
	int32_t slValue = 0;

	char ubPosCode = *strCalibration.c_str();


#ifdef __DEBUG_PROTOCOL
		m_vecSaveCalibration.at(m_counterAction).strValue.assign(to_string((ubNumSection + 1) * (m_counterAction + 1) * 10));
		return 0;
#endif


	// select the type of action
	if(m_vecSaveCalibration.at(m_counterAction).strType.compare(XML_TAG_CALIBRATION_NAME) == 0)
	{
		// run calibration movement
		m_pLogger->log(LOG_INFO, "OpSaveCalibration::offset CMD= %X", ubPosCode);
		ret = pCommonMessage->saveCalibration(ubPosCode, 0);
	}
	else
	{
		// set calibration value
		slValue = atoi(m_vecSaveCalibration.at(m_counterAction).strValue.c_str());
		m_pLogger->log(LOG_INFO, "OpSaveCalibration::setOffset CMD= %X -> %d", ubPosCode, slValue);
		ret = pCommonMessage->setDirectCalibration(ubPosCode, slValue, 0);
	}

	if(ret == -1)
	{
		// error during calibration
		m_vecSaveCalibration.at(m_counterAction).strValue.clear();
	}
	else
	{
		// retrieve stored calibration value
		ret = pCommonMessage->getCalibration(ubPosCode, slValue);
		m_vecSaveCalibration.at(m_counterAction).strValue.assign(to_string(slValue));
	}
	return ret;
}

int OpSaveCalibration::executeNSHCalibration()
{
	if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled() )
	{
		m_pLogger->log(LOG_WARNING, "OpSaveCalibration: NSH encoder not enabled");
		return -1;
	}

	// if different from NSH than error
	if ( m_vecSaveCalibration.at(m_counterAction).strMotor.compare(DEVICE_NSH_STRING) )
	{
		m_pLogger->log(LOG_WARNING, "OpSaveCalibration: wrong motor [%s], with NSH component",
					   m_vecSaveCalibration.at(m_counterAction).strMotor.c_str());
		return -1;
	}

	// Read motor or encoder (the one in half step) position
	int liPositionRead;
	int8_t cRes = spiBoardLinkSingleton()->m_pNSHEncoder->readCounter(liPositionRead);
	if ( ! cRes )
	{
		return -1;
	}

	liPositionRead /= ENC_COUNTS_TO_HALF_STEPS;
	m_vecSaveCalibration.at(m_counterAction).strValue.assign(to_string(liPositionRead));

	// Save the value in config file
	bool bRes = nshBoardManagerSingleton()->m_pConfig->updateNSHConfiguration(m_vecSaveCalibration.at(m_counterAction).strLabel, liPositionRead);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_WARNING, "OpSaveCalibration: label [%s] not found in nsh calibration file",
					   m_vecSaveCalibration.at(m_counterAction).strLabel.c_str());
		return -1;
	}

	return 0;
}

int OpSaveCalibration::executeCmd(int lNum)
{
	//action to perform
	int res = 0;

	switch (lNum)
	{
		case SCT_NONE_ID:
			res = executeNSHCalibration();
		break;

		default:
			res = executeCalibration(lNum);
		break;
	}

	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "OpSaveCalibration::execute-->execute not performed");
		// operation completed -> send reply and close it
		closeOperation();
		return -1;
	}

	std::string previousComponent;
	previousComponent.assign(m_vecSaveCalibration.at(m_counterAction).strComponent);

	// increase the action counter
	m_counterAction++;

	// if we have performed the last action compose and send the reply, stop the loop
	if(m_counterAction >= m_vecSaveCalibration.size() )
	{
		// operation completed -> send reply and close it
		closeOperation();
		res = -1;
	}
	else if(previousComponent.compare(m_vecSaveCalibration.at(m_counterAction).strComponent) != 0)
	{
		// actions on the current device are completed:
		// --> go for next action
		perform();
		// -> unlock the actual
		res = -1;
	}

	m_pLogger->log(LOG_INFO, "OpSaveCalibration::execute-->completed");

	return res;
}

int OpSaveCalibration::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdSaveCalibration cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	SaveCalibrationPayload * pSaveCalibrationPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pSaveCalibrationPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************************************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSaveCalibration::execute-->unable to serialize OutCmdSaveCalibration");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}
	return eOperationEnabledForDeath;
}
