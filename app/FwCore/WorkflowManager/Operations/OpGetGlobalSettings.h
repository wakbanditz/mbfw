/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetGlobalSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetGlobalSettings class.
 @details

 ****************************************************************************
*/

#ifndef OPGETGLOBALSETTINGS_H
#define OPGETGLOBALSETTINGS_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GET_GLOBAL_SETTINGS command
 * ********************************************************************************************************************
 */
class OpGetGlobalSettings : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpGetGlobalSettings default constructor
		 * ***********************************************************************************************************
		 */
		OpGetGlobalSettings();

		/*! ***********************************************************************************************************
		 * @brief ~OpGetGlobalSettings default destructor
		 * ************************************************************************************************************
		 */
		~OpGetGlobalSettings();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);
};

#endif // OPGETGLOBALSETTINGS_H
