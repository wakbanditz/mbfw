/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpOPTCheck.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpOPTCheck class.
 @details

 ****************************************************************************
*/

#ifndef OPOPTCHECK_H
#define OPOPTCHECK_H

#include "Operation.h"
#include "Payload.h"
#include "Loggable.h"
#include "CommonInclude.h"

class OPTCheckPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the OPT_CHECK command
 * ********************************************************************************************************************
 */

class OpOPTCheck : public Operation
{
    public:

        /*! ***********************************************************************************************************
         * @brief OpOPTCheck default constructor
         * ***********************************************************************************************************
         */
        OpOPTCheck();

        /*! ***********************************************************************************************************
         * @brief ~OpOPTCheck default destructor
         * ************************************************************************************************************
         */
        virtual ~OpOPTCheck();

        /*!
         * @brief setParameters set the parameters for OPT
         * @param target the OPT target
         * @param tolerance the OPT tolerance
         * @return
         */
        void setParameters(int target, int tolerance);

        /*! ***********************************************************************************************************
         * @brief perform	method that implements the action
         * ************************************************************************************************************
         */
        int perform(void);

        /*! ***********************************************************************************************************
         * @brief execute	method that implements the action
         * @return 0 if success | -1 otherwise
         * ************************************************************************************************************
         */
        int executeCmd(int lNum);

        /*!
         * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
         * then it composes the message to be sent to the sw
         * @return eOperationEnabledForDeath
         */
        int compileSendOutCmd();

        /*! *************************************************************************************************
         * @brief setSlot enable the section-slot
         * @param section the section index
         * @param slot the slot index
         * *************************************************************************************************
         */
        void setSlot(uint8_t section, uint8_t slot);


    private:
        /*!
         * @brief buildPayload	Fills the OpOPTCheckPayload of the outgoing command with related values.
         * @return  pointer to the OpOPTCheckPayload object just allocated
         */
        OPTCheckPayload * buildPayload(void);

        /*! *************************************************************************************************
         * @brief isSectionEnabled check if the section has at least one slot enabled
         * @param section the section index
         * @return true if section enabled
         * *************************************************************************************************
         */
        bool isSectionEnabled(uint8_t section);

        /*! *************************************************************************************************
         * @brief fillPositionsVector according to the section-slot enable builds a array of positions
         *          (only center slots) to read
         * *************************************************************************************************
         */
        void fillPositionsVector();

    private :

        enum class eState { eStart, eMoveToTrayReadA, eMoveToTrayReadB, eExecute, eMoveToTrayOutA, eMoveToTrayOutB,
                            eClean, eNshError, eSectErrorA, eSectErrorB, eStop };

        eState	m_eOptState;

        int m_intTarget, m_intTolerance;

        int m_Read[SCT_NUM_TOT_SECTIONS];
        int m_stsSectionSlotEnable[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];
        vector<string>	m_vPositions;
        vector<int>		m_vRFUvalues;
};


#endif // OPOPTCHECK_H
