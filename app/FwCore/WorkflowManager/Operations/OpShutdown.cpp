/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpShutdown.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpShutdown class.
 @details

 ****************************************************************************
*/

#include "OpShutdown.h"
#include "MainExecutor.h"

OpShutdown::OpShutdown() : Operation("OpShutdown")
{
	m_sectionRunning[SCT_A_ID] = false;
	m_sectionRunning[SCT_B_ID] = false;
}

OpShutdown::~OpShutdown()
{
}

int OpShutdown::perform(void)
{
	// masterboard and instrument shutdown
	if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
	{
		m_sectionRunning[SCT_A_ID] = true;
		sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_END, (Operation*)this);
	}
	if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
	{
		m_sectionRunning[SCT_B_ID] = true;
		sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_END, (Operation*)this);
	}

	if(m_sectionRunning[SCT_A_ID] == false && m_sectionRunning[SCT_B_ID] == false)
	{
		// both sections disabled -> switch off
		MainExecutor::getInstance()->shutdownInstrument();
		return eOperationWaitForDeath;
	}

	return eOperationRunning;
}

int OpShutdown::executeCmd(int lNum)
{
	m_sectionRunning[lNum] = false;

	if(m_sectionRunning[SCT_A_ID] == false && m_sectionRunning[SCT_B_ID] == false)
	{
		// both sections disabled -> switch off
		MainExecutor::getInstance()->shutdownInstrument();
		return 0;
	}
	return -1;
}


