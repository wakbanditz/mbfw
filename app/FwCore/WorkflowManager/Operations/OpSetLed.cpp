/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetLed.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSetLed class.
 @details

 ****************************************************************************
*/

#include "OpSetLed.h"
#include "OutCmdSetLed.h"

#include "SectionBoardGeneral.h"
#include "SectionBoardManager.h"
#include "CommonInclude.h"

#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpSetLed::OpSetLed(std::string strComponent, std::string strMode, std::string strLed) :
	Operation("OpSetLed")
{
	m_strComponent.assign(strComponent);
	m_strMode.assign(strMode);
	m_strLed.assign(strLed);
}

OpSetLed::~OpSetLed()
{

}

int OpSetLed::perform(void)
{
	int8_t res;

	if(m_strComponent.compare(XML_TAG_GLOBAL_LED_NAME) == 0)
	{
        res = masterBoardManagerSingleton()->setMasterAction(eMasterActionCmd, (Operation*)this);
	}
	else if(m_strComponent.compare(XML_TAG_SECTIONA_LED_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_CMD, (Operation*)this);
	}
	else if(m_strComponent.compare(XML_TAG_SECTIONB_LED_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_CMD, (Operation*)this);
	}

	if(res != eOperationRunning)
	{
		m_pLogger->log(LOG_WARNING, "OpSetLed::perform-->Error %s", m_strComponent.c_str());
	}

	return res;
}

int OpSetLed::executeCmd(int lNum)
{
    int res = -1;

    if(lNum == SCT_A_ID || lNum == SCT_B_ID)
    {
        enumSectionLedType ledType = eLedBlue;
        enumSectionLedFreq ledFreq = eLedOff;

        //action to perform
        if(m_strLed.compare(GREEN_COLOR_STRING) == 0)
        {
            ledType = eLedGreen;
        }
        else if(m_strLed.compare(RED_COLOR_STRING) == 0)
        {
            ledType = eLedRed;
        }
        else if(m_strLed.compare(BLUE_COLOR_STRING) == 0)
        {
            ledType = eLedBlue;
        }

        if(m_strMode.compare(ON_MODE_STRING) == 0)
        {
            ledFreq = eLedOn;
        }
        else if(m_strMode.compare(OFF_MODE_STRING) == 0)
        {
            ledFreq = eLedOff;
        }
        else if(m_strMode.compare(BLK1_MODE_STRING) == 0)
        {
            ledFreq = eLedBlinkSlow;
        }
        else if(m_strMode.compare(BLK2_MODE_STRING) == 0)
        {
            ledFreq = eLedBlinkHigh;
        }

        res = sectionBoardGeneralSingleton(lNum)->setLed(ledType, ledFreq);
    }
    else if(lNum == INSTRUMENT_ID)
    {
        eModuleLedId ledType = eModuleLedMax;
        eModuleLedMode ledFreq = eModuleLedOff;

        //action to perform
        if(m_strLed.compare(GREEN_COLOR_STRING) == 0)
        {
            ledType = eModuleLedGreen;
        }
        else if(m_strLed.compare(RED_COLOR_STRING) == 0)
        {
            ledType = eModuleLedRed;
        }
        else if(m_strLed.compare(ORANGE_COLOR_STRING) == 0)
        {
            ledType = eModuleLedOrange;
        }

        if(m_strMode.compare(ON_MODE_STRING) == 0)
        {
            ledFreq = eModuleLedSteady;
        }
        else if(m_strMode.compare(OFF_MODE_STRING) == 0)
        {
            ledFreq = eModuleLedOff;
        }
        else if(m_strMode.compare(BLK1_MODE_STRING) == 0)
        {
            ledFreq = eModuleLedBlink;
        }
        else if(m_strMode.compare(BLK2_MODE_STRING) == 0)
        {
            ledFreq = eModuleLedBlink;
        }

        if(masterBoardManagerSingleton()->setLed(ledType, ledFreq) == true)
        {
            res = 0;
        }
    }
    return res;
}

int OpSetLed::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdSetLed outCmdSetLed;
	string strOutCommand;

	outCmdSetLed.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdSetLed.setUsage(getUsage());
	outCmdSetLed.setID(getID());

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	bool bSerializeResult = protocolSerialize(&outCmdSetLed, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSetLed--> unable to serialize OutCmdSetLed");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}
