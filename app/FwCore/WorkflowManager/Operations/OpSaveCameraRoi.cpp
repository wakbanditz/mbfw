/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSaveCameraRoi.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSaveCameraRoi class.
 @details

 ****************************************************************************
*/

#include "OpSaveCameraRoi.h"
#include "OutCmdSaveCameraRoi.h"
#include "MainExecutor.h"
#include "SaveCameraRoiPayload.h"
#include "WebServerAndProtocolInclude.h"
#include "CR8062Include.h"


OpSaveCameraRoi::OpSaveCameraRoi() : Operation("OpSHCalibrate")
{
	m_mapSubSet.clear();
}

OpSaveCameraRoi::~OpSaveCameraRoi()
{

}

void OpSaveCameraRoi::setParams(vector<string>& vTarget, vector<string>& vTop, vector<string>& vBottom, vector<string>& vLeft, vector<string>& vRight)
{
	/** ****************************************************************************
	 * All the x0 and y0 are not absolute, but related to the image the SSW gets.
	 * If that image has been already cropped, than an additional offset has to be
	 * added to the values found.
	 * For this reason we need to know first the original crop.
	 * ****************************************************************************/

	int X0 = -1, Y0 = -1, siWidth = -1, siHeight = -1;
	bool bIsEnabled = false;
	int liRes = usbCameraSingleton()->getCropCoordinates(&X0, &Y0, &siWidth, &siHeight, &bIsEnabled);
	if ( liRes != ERR_NONE )	return;
	if ( ! bIsEnabled )
	{
		X0 = 0;
		Y0 = 0;
	}

	int liSize = vTarget.size();
	structRect rect;

	for ( int i = 0; i != liSize; i++ )
	{
		// find what is the target I am dealing with
		if		( vTarget[i].find(TARGET_NAME_SAMPLE0)	!= string::npos )	rect.eTarget = eBatchTarget::eSampleX0;
		else if ( vTarget[i].find(TARGET_NAME_SAMPLE3)	!= string::npos )	rect.eTarget = eBatchTarget::eSampleX3;
		else if ( vTarget[i].find(TARGET_NAME_BC)		!= string::npos )	rect.eTarget = eBatchTarget::eLinearBarcode;
		else if ( vTarget[i].find(TARGET_NAME_BC_ABS)	!= string::npos )	rect.eTarget = eBatchTarget::eConeAbsence;
		else if ( vTarget[i].find(TARGET_NAME_DATAMATRIX) != string::npos )	rect.eTarget = eBatchTarget::eDataMatrix;
		else continue;

		rect.x = stoi(vLeft[i]) + X0;
		rect.y = stoi(vTop[i]) + Y0;
		rect.width = stoi(vRight[i]) - stoi(vLeft[i]);
		rect.height = stoi(vBottom[i]) - stoi(vTop[i]);
		m_mapSubSet[vTarget[i]] = rect;
    }
}

int OpSaveCameraRoi::perform()
{
	return cameraBoardManagerSingleton()->setCameraAction(eCameraActionCmd, (Operation*)this);
}

int OpSaveCameraRoi::executeCmd(int lNum)
{
	bool bRes = !lNum;

	// In case we are dealing with slot 6, then linearize
	if ( m_mapSubSet.size() == 1 )
	{
		string strIdx("");
		size_t pos = m_mapSubSet.begin()->first.find("6");
		if ( pos != string::npos )			strIdx.assign("6");
		else
		{
			pos = m_mapSubSet.begin()->first.find("12");
			if ( pos != string::npos )		strIdx.assign("12");
		}

		if ( ! strIdx.empty() )
		{
			string strElement1 = m_mapSubSet.begin()->first;
			int start = ( ! strIdx.compare("6") ) ? 1 : 7;
			strElement1.replace(pos, strIdx.size(), to_string(start));
			structRect rect;
			cameraBoardManagerSingleton()->m_pConfig->getItemValue(strElement1, rect);

			// Linearize slots
			int liDiffX = m_mapSubSet.begin()->second.x - rect.x;
			int liDiffY = m_mapSubSet.begin()->second.y - rect.y;
            for ( int i = start + 1; i < start + 6; ++i )
			{
				structRect rectElem = rect;

				// linearize correctly with float
				float fFoo = float(liDiffX) * float(i-start) / 5.0f;
				int iFoo = liDiffX * (i-start) / 5;
				fFoo -= iFoo;
				if ( fFoo > 0.5f )		++iFoo;
				rectElem.x += iFoo;

				fFoo = float(liDiffY) * float(i-start) / 5.0f;
				iFoo = liDiffY * (i-start) / 5;
				fFoo -= iFoo;
				if ( fFoo > 0.5f )		++iFoo;
				rectElem.y += iFoo;

				string strElem = m_mapSubSet.begin()->first;
				strElem.replace(pos, strIdx.size(), to_string(i));
				m_pLogger->log(LOG_DEBUG, "OpSaveCameraRoi::executeCmd: new contrast calculated for %s is [%d %d %d %d]",
							   strElem.c_str(), rectElem.x, rectElem.y, rectElem.width, rectElem.height);
				m_mapSubSet[strElem] = rectElem;
			}
		}
	}
	// Update camera config file anyhow
	bRes = cameraBoardManagerSingleton()->m_pConfig->updateCameraConfig(m_mapSubSet);

	return ( bRes ) ? 0 : -1;
}

int OpSaveCameraRoi::compileSendOutCmd()
{
	OutCmdSaveCameraRoi cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	SaveCameraRoiPayload* pSaveCameraRoiPayload = buildPayload();
	// ... and assign it to the outcommand so that the serializle will extract the info
	cmd.setPayload((Payload *)pSaveCameraRoiPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSaveCameraRoi::execute--> unable to serialize OutCmdSaveCameraRoi");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

SaveCameraRoiPayload* OpSaveCameraRoi::buildPayload()
{
	SaveCameraRoiPayload* pSaveCameraRoiPayload = new SaveCameraRoiPayload();

	pSaveCameraRoiPayload->setAttributes(m_mapSubSet);

	return pSaveCameraRoiPayload;
}
