/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetCameraRoi.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetCameraRoi class.
 @details

 ****************************************************************************
*/

#include "OpGetCameraRoi.h"
#include "MainExecutor.h"
#include "WebServerAndProtocolInclude.h"
#include "OutCmdGetCameraRoi.h"
#include "GetCameraRoiPayload.h"

OpGetCameraRoi::OpGetCameraRoi() : Operation("OpGetCameraRoi")
{
	m_vTargets.clear();
	m_mapConfig.clear();
}

OpGetCameraRoi::~OpGetCameraRoi()
{
	/* Nothing to do yet */
}

void OpGetCameraRoi::setParams(vector<string>& vTargets)
{
	m_vTargets = vTargets;
}

int OpGetCameraRoi::perform()
{
	return cameraBoardManagerSingleton()->setCameraAction(eCameraActionCmd, (Operation*)this);
}

int OpGetCameraRoi::executeCmd(int lNum)
{
	bool bRes = ( ! lNum );

//	// This is only because we want the relative position and not the absolute one
//	int X0 = -1, Y0 = -1, siWidth = -1, siHeight = -1;
//	bool bIsEnabled = false;
//	int liRes = usbCameraSingleton()->getCropCoordinates(&X0, &Y0, &siWidth, &siHeight, &bIsEnabled);
//	if ( liRes != ERR_NONE )	return -1;
//	if ( ! bIsEnabled )
//	{
//		X0 = 0;
//		Y0 = 0;
//	}
//	// SSW starts from 1 while camera's first pixel is 0
//	X0++;

	if ( m_vTargets.empty() )
	{		
		bRes = cameraBoardManagerSingleton()->m_pConfig->getCameraConfig(m_mapConfig);
	}
	else
	{
		int liCycles = m_vTargets.size();
		for ( int i = 0; i < liCycles; i++ )
		{
			structRect rect;
			bRes = cameraBoardManagerSingleton()->m_pConfig->getItemValue(m_vTargets[i], rect);
			if ( bRes )
			{
//				rect.x -= X0;
//				rect.y -= Y0;
				m_mapConfig[m_vTargets[i]] = rect;
			}
		}
	}

	return ( ! bRes );
}


int OpGetCameraRoi::compileSendOutCmd()
{
	OutCmdGetCameraRoi cmd;
	string strOutCommand;

	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	GetCameraRoiPayload* pGetCameraRoiPayload = buildPayload();
	// ... and assign it to the outcommand so that the serializle will extract the info
	cmd.setPayload((Payload *)pGetCameraRoiPayload);
	// Note: the payload will be deleted by the OutgoingCommand


	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpGetCameraRoi::execute--> unable to serialize OutCmdGetCameraRoi");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

GetCameraRoiPayload* OpGetCameraRoi::buildPayload(void)
{
	GetCameraRoiPayload* pGetCameraRoiPayload = new GetCameraRoiPayload();

	pGetCameraRoiPayload->setAttributes(m_mapConfig);

	return pGetCameraRoiPayload;
}

