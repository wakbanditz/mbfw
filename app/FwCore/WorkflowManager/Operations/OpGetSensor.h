/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetSensor.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetSensor class.
 @details

 ****************************************************************************
*/

#ifndef OPGETSENSOR_H
#define OPGETSENSOR_H

#include "Operation.h"
#include "SectionBoardManager.h"
#include "CommonInclude.h"
#include "Mutex.h"

class GetSensorPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETSENSOR command
 * ********************************************************************************************************************
 */

class OpGetSensor : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpGetSensor default constructor
		 * ***********************************************************************************************************
		 */
		OpGetSensor(const string &strCategory,
					const string &strComponent,
					const string &strId);

		/*! ***********************************************************************************************************
		 * @brief ~OpGetSensor default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpGetSensor();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		void setCategory(const string& strCategory);
		void setComponent(const string& strComponent);
		void setId(const string& strId);

	private:
		/*!
		 * @brief buildPayload	Fills the GetSensorPayload of the outgoing command with related values.
		 * @return  pointer to the GetSensorPayload object just allocated
		 */
		GetSensorPayload * buildPayload(void);

    private:
        string m_strCategory;
        string m_strComponent;
        string m_strId;

};

#endif // OPGETSENSOR_H
