/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpFwUpdate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpFwUpdate class.
 @details

 ****************************************************************************
*/

#include <algorithm>

#include "HttpClient.h"

#include "OpFwUpdate.h"
#include "MainExecutor.h"
#include "WebServerAndProtocolInclude.h"
#include "OutCmdFwUpdate.h"
#include "FwUpdatePayload.h"
#include "CRCFile.h"

#define UPDATE_OUTCOME_SUCCESS		"SUCCESS"
#define UPDATE_OUTCOME_FAILURE		"FAILURE"

OpFwUpdate::OpFwUpdate() : Operation("OpFwUpdate")
{
	m_vFwInfo.clear();
	m_vDevUpd.clear();
}

OpFwUpdate::~OpFwUpdate()
{
	/* Nothing to do yet */
}

void OpFwUpdate::setParamsFromCmd(FwUpdateContainer* pContainer)
{
	m_FwContainer = *pContainer;
}

void OpFwUpdate::setParamsFromManager(FwUpdateInfo& vFwInfo)
{
	m_vFwInfo.push_back(vFwInfo);
}

void OpFwUpdate::setEepromParamsFromManager(FwUpdateEEprom& fwEeprom)
{
	m_FwEeprom = fwEeprom;
}

FwUpdateInfo* OpFwUpdate::getLastDeviceInfoForUpd()
{
	if ( m_vFwInfo.empty() )	return nullptr;

	return &m_vFwInfo.back();
}

FwUpdateEEprom* OpFwUpdate::getEepromInfoForUpd()
{
	return &m_FwEeprom;
}

int OpFwUpdate::addItemToLog(eDeviceId eId, enumUpdType eType)
{
	ofstream log(UPDATE_LOG_PATH, ios_base::app | ios_base::out);
	if ( ! log.good() )
	{
		m_pLogger->log(LOG_ERR, "OpFwUpdate::addItemToLog: unable to open %s", UPDATE_LOG_PATH);
		return -1;
	}

	char* pDeviceName = getDeviceFromEnum(eId);
	char* pFwType = getFwTypeFromEnum(eType);

	log << pDeviceName << " # " << pFwType << " # " << endl;
	m_pLogger->log(LOG_DEBUG, "OpFwUpdate::addItemToLog: added to log %s - %s", pDeviceName, pFwType);
	return 0;
}

int OpFwUpdate::writeUpdOutcomeLog(eDeviceId eId, enumUpdType eType, bool bIsSuccess)
{
	char* pDeviceName = getDeviceFromEnum(eId);
	char* pFwType = getFwTypeFromEnum(eType);

	ifstream iLogFile(UPDATE_LOG_PATH);
	if ( ! iLogFile.is_open() )
	{
		m_pLogger->log(LOG_ERR, "OpFwUpdate::writeUpdOutcomeInLog: unable to open %s", UPDATE_LOG_PATH);
		return -1;
	}

	typedef struct
	{
		string strDevice;
		string strFwType;
		string strResult;
	} structLogConfig;

	vector<structLogConfig> vLog;
	string strTmp("");
	while ( getline(iLogFile, strTmp) )
	{
		// Remove white spaces
		strTmp.erase(remove(strTmp.begin(), strTmp.end(), ' '), strTmp.end());

		// Do not consider if empty
		if ( strTmp.empty() )		continue;
		// Remove comments
		if ( strTmp[0] == '#' )		continue;

		int liIdx = 0;
		structLogConfig logStruct = {};
		for ( auto c : strTmp )
		{
			if ( c == '#' )
			{
				liIdx++;
				if ( liIdx > 2 )	break;
				else				continue;
			}

			switch (liIdx)
			{
				case 0:		logStruct.strDevice.push_back(c);	break;
				case 1:		logStruct.strFwType.push_back(c);	break;
				case 2:		logStruct.strResult.push_back(c);	break;
				default:	break;
			}
		}
		vLog.push_back(logStruct);
	}
	iLogFile.close();

	string strRes = bIsSuccess ? UPDATE_OUTCOME_SUCCESS : UPDATE_OUTCOME_FAILURE;
	size_t liSize = vLog.size();
	for ( size_t i = 0; i < liSize; i++ )
	{
		if ( ! vLog[i].strDevice.compare(pDeviceName) )
		{
			if ( ! vLog[i].strFwType.compare(pFwType) )
			{
				m_pLogger->log(LOG_DEBUG, "OpFwUpdate::writeUpdOutcomeLog: result of %s - %s is %s",
							   pDeviceName, pFwType, strRes.c_str());
				vLog[i].strResult.assign(strRes);
				break;
			}
		}
	}

	// Update file
	ofstream oLogFile;
	oLogFile.open(UPDATE_LOG_PATH, ofstream::out | ofstream::trunc); // trunc to delete the content of the file when opening
	if ( ! oLogFile.good() )
	{
		m_pLogger->log(LOG_ERR, "OpFwUpdate::writeUpdOutcomeInLog: unable to open %s", UPDATE_LOG_PATH);
		return -1;
	}

	for ( size_t i = 0; i < liSize; i++ )
	{
		oLogFile << vLog[i].strDevice << " # " << vLog[i].strFwType
				 << " # " << vLog[i].strResult << endl;
	}
	oLogFile.close();

	return 0;
}

int OpFwUpdate::parseLog()
{
	ifstream iLogFile(UPDATE_LOG_PATH);
	if ( ! iLogFile.is_open() )
	{
		m_pLogger->log(LOG_ERR, "OpFwUpdate::parseLog: unable to open %s", UPDATE_LOG_PATH);
		return -1;
	}

	typedef struct
	{
		string strDevice;
		string strFwType;
		string strResult;
	} structLogConfig;

	string strTmp("");
	vector<string> vFailed;
	while ( getline(iLogFile, strTmp) )
	{
		// Remove white spaces
		strTmp.erase(remove(strTmp.begin(), strTmp.end(), ' '), strTmp.end());

		// Do not consider if empty
		if ( strTmp.empty() )		continue;
		// Remove comments
		if ( strTmp[0] == '#' )		continue;

		int liIdx = 0;
		structLogConfig logStruct = {};
		for ( auto c : strTmp )
		{
			if ( c == '#' )
			{
				liIdx++;
				if ( liIdx > 2 )	break;
				else				continue;
			}

			switch (liIdx)
			{
				case 0:		logStruct.strDevice.push_back(c);	break;
				case 1:		logStruct.strFwType.push_back(c);	break;
				case 2:		logStruct.strResult.push_back(c);	break;
				default:	break;
			}
		}

		// Add device if not already present or remove if present, but an error occurred
		if ( ! logStruct.strResult.compare(UPDATE_OUTCOME_FAILURE) )
		{
			if ( find(vFailed.begin(), vFailed.end(), logStruct.strDevice) == vFailed.end() )
			{
				vFailed.push_back(logStruct.strDevice);
			}
		}
		else if ( find(m_vDevUpd.begin(), m_vDevUpd.end(), logStruct.strDevice) == m_vDevUpd.end() )
		{
			m_vDevUpd.push_back(logStruct.strDevice);
		}
	}

	iLogFile.close();

	// If a device have correctly updated a part (e.g. boot), but not all the
	// required parts (e.g. also applicative), it has complessively failed
	for ( size_t i = 0; i < vFailed.size(); ++i )
	{
		for ( size_t j = 0; j < m_vDevUpd.size(); ++j )
		{
			if ( ! vFailed[i].compare(m_vDevUpd[j]) )
			{
				m_vDevUpd.erase(m_vDevUpd.begin()+j);
				break;
			}
		}
	}

	// clear file
	ofstream ofs;
	ofs.open(UPDATE_LOG_PATH, std::ofstream::out | std::ofstream::trunc);
	ofs.close();

	return 0;
}

int OpFwUpdate::perform()
{
	string strTarPath(DEFAULT_INSTR_PACK_PATH);

    // get tar file from gateway at the link FwContainer.m_strUrl and save in root
    string strTarFileUrl;
    if(m_FwContainer.m_strUrl.empty() == false)
    {
        // isolate the ip address inside the string (http://192.168.0.1:8080/api/process or http://192.168.0.1/api/process)
        // skip the "http://" prefix
        strTarFileUrl.assign(getWebUrl());
        std::size_t foundDelimiter = strTarFileUrl.find("//");
        if (foundDelimiter != std::string::npos)
        {
            std::size_t foundFinalDelimiter = strTarFileUrl.find(":", foundDelimiter + 2);
            if (foundFinalDelimiter == std::string::npos)
            {
                foundFinalDelimiter = strTarFileUrl.find("/", foundDelimiter + 2);
            }
            if (foundFinalDelimiter != std::string::npos)
            {
                strTarFileUrl.resize(foundFinalDelimiter);
            }
        }
        strTarFileUrl.append(m_FwContainer.m_strUrl);

        m_pLogger->log(LOG_INFO, "OpFwUpdate::perform: download %s to %s", strTarFileUrl.c_str(), strTarPath.c_str());

        std::string strError;

        HttpClient * pHttpClient = new HttpClient();
        if(pHttpClient->transferFile(strTarFileUrl, strTarPath, strError) == false)
        {
            m_pLogger->log(LOG_ERR, "OpFwUpdate::downloading file error %s", strError.c_str());
        }
        else
        {
            m_pLogger->log(LOG_INFO, "OpFwUpdate::downloading file completed");
        }
    }


	// FwContainer.crc32 check over the full tar file
	CRCFile* pCalculator = new CRCFile(m_pLogger);
	unsigned long ulCRC32;
	pCalculator->calculateCRCFileMaxAlloc(strTarPath, ulCRC32);
	SAFE_DELETE(pCalculator);

//	unsigned long ulCRC32Passed = stoul(m_FwContainer.m_strCRC32, nullptr, 16);
//	if ( ulCRC32 != ulCRC32Passed )
//	{
//		m_pLogger->log(LOG_ERR, "OpFwUpdate::perform: crc calculated and crc indicated in command does "
//								"not correspond [%ul vs %ul]", ulCRC32, ulCRC32Passed);
//		closeOperation();
//		return eOperationError;
//	}

	FWUpdateManager* pFwManager = new FWUpdateManager();
	bool bForceUpd = ! m_FwContainer.m_strForce.compare("true");

	int liRes = pFwManager->init(m_pLogger, this, m_FwContainer.m_vComponents, bForceUpd);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "OpFwUpdate::perform: error in initialization of fw manager");
        restoreStateMachine();
        compileSendOutCmd();
        return eOperationError;
	}

	// Create backup
	string strTarBackupPath(DEFAULT_BACKUP_DIR);
	m_pLogger->log(LOG_INFO, "OpFwUpdate::perform: creating backup in %s", strTarBackupPath.c_str());
	liRes = pFwManager->createBackup(strTarPath, strTarBackupPath);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "OpFwUpdate::perform: unable to create fw pkt backup");
        restoreStateMachine();
        compileSendOutCmd();
        return eOperationError;
	}

	// Update all the devices requested
    m_pLogger->log(LOG_INFO, "OpFwUpdate::perform: ready to start the time evaluation");
    liRes = pFwManager->updateDevices(strTarPath, true);
    if ( liRes )
    {
        m_pLogger->log(LOG_ERR, "OpFwUpdate::perform: unable to try to update devices");
        restoreStateMachine();
        compileSendOutCmd();
        return eOperationError;
    }
    pFwManager->clearFwUpdateManager();

    m_pLogger->log(LOG_INFO, "OpFwUpdate::perform: ready to start the update procedure");
    liRes = pFwManager->updateDevices(strTarPath);
    if ( liRes )
    {
        m_pLogger->log(LOG_ERR, "OpFwUpdate::perform: unable to try to update devices");
        restoreStateMachine();
        compileSendOutCmd();
        return eOperationError;
    }

	m_pLogger->log(LOG_INFO, "OpFwUpdate::perform: cleaning rubbish");
    pFwManager->clearRubbish(strTarPath);
	SAFE_DELETE(pFwManager);

	// Get update results
    parseLog();
	// build payload, restore state machine and remove op
    restoreStateMachine();
    compileSendOutCmd();

    return eOperationWaitForDeath;
}

int OpFwUpdate::compileSendOutCmd()
{
	OutCmdFwUpdate cmd;
	string strOutCommand;

	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	FwUpdatePayload* pFwUpdatePayload = buildPayload();
	// ... and assign it to the outcommand so that the serializle will extract the info
	cmd.setPayload((Payload *)pFwUpdatePayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
        m_pLogger->log(LOG_ERR, "OpFwUpdate::compileSendOutCmd--> unable to serialize OutCmdFwUpdate");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

bool OpFwUpdate::compileSendProgressCmd(uint8_t percentage)
{
    OutCmdFwUpdate cmd;
    string strOutCommand;

    cmd.setStatus(XML_ATTR_STATUS_ACCEPTED);
    cmd.setUsage(getUsage());
    cmd.setID(getID());

    // compose the payload ...
    FwUpdatePayload* pFwUpdatePayload = buildPayload(percentage);
    // ... and assign it to the outcommand so that the serialize will extract the info
    cmd.setPayload((Payload *)pFwUpdatePayload);
    // Note: the payload will be deleted by the OutgoingCommand

    /* ********************************************************************************************
     * Send the OutgoingCommand to the client
     * ********************************************************l***********************************
     */
    bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
    if ( ! bSerializeResult )
    {
        m_pLogger->log(LOG_ERR, "OpFwUpdate::compileSendProgressCmd--> unable to serialize OutCmdFwUpdate");
    }
    else
    {
        sendCommandReply(strOutCommand);
    }

    return bSerializeResult;
}

FwUpdatePayload* OpFwUpdate::buildPayload(uint8_t percentage)
{
	FwUpdatePayload* pFwUpdatePayload = new FwUpdatePayload();

    // if the percentage is valid we are composing a partial reply (so only percentage has to be sent)
    // otherwise percentage is not sent while the list of components is included
    if(percentage > 0)
    {
        vector<string> vDevUpdNull;
        vDevUpdNull.clear();

        pFwUpdatePayload->setAttributes(vDevUpdNull, percentage);
    }
    else
    {
        pFwUpdatePayload->setAttributes(m_vDevUpd, 0);
    }

	return pFwUpdatePayload;
}

char* OpFwUpdate::getFwTypeFromEnum(enumUpdType eType)
{
	switch ( eType )
	{
		case eKernel:		return (char*)"Kernel";
		case eMiniKernel:	return (char*)"MiniKernel";
		case eBootLoader:	return (char*)"BootLoader";
		case eApplicative:	return (char*)"Applicative";
		case eFPGA:			return (char*)"FPGA";
		case eEeprom:		return (char*)"Eeprom";
		default:			return (char*)"";
	}
}

char* OpFwUpdate::getDeviceFromEnum(eDeviceId eId)
{
	switch ( eId )
	{
		case eIdModule:		return (char*)"Module";
		case eIdSectionA:	return (char*)"SectionA";
		case eIdSectionB:	return (char*)"SectionB";
		case eIdNsh:		return (char*)"NSH";
		case eIdCamera:		return (char*)"Camera";
		default:			return (char*)"";
	}
}

