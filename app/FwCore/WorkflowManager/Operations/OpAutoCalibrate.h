/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpAutoCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpAutoCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef OPAUTOCALIBRATE_H
#define OPAUTOCALIBRATE_H

#include "Operation.h"
#include "Payload.h"
#include "Loggable.h"

class AutoCalibratePayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the AUTOCALIBRATE command
 * ********************************************************************************************************************
 */

class OpAutoCalibrate : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpAirCalibrate default constructor
		 * ***********************************************************************************************************
		 */
		OpAutoCalibrate();

		/*! ***********************************************************************************************************
		 * @brief ~OpAirCalibrate default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpAutoCalibrate();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*!
		 * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
		 * then it composes the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 */
		int compileSendOutCmd();

	private:

		/*!
		 * @brief buildPayload	Fills the AutoCalibratePayload of the outgoing command with related values.
		 * @return  pointer to the AutoCalibratePayload object just allocated
		 */
		AutoCalibratePayload* buildPayload(void);

    private:

        string m_strRFUVal;
        string m_strGain;

};

#endif // OPAUTOCALIBRATE_H
