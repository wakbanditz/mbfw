/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetPressureOffsets.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetPressureOffsets class.
 @details

 ****************************************************************************
*/

#include "OpGetPressureOffsets.h"
#include "OutCmdGetPressureOffsets.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardPIB.h"
#include "SectionBoardManager.h"
#include "GetPressureOffsetsPayload.h"
#include "CommonInclude.h"


/*! *******************************************************************************************************************
 * Protocol command name element values
 * ********************************************************************************************************************
 */

OpGetPressureOffsets::OpGetPressureOffsets(std::vector<uint8_t> componentList)
									: Operation("GetPressureOffsets")
{
	bool bA = false;
	bool bB = false;

	for(uint8_t i = 0; i < componentList.size(); i++)
	{
		if(componentList.at(i) == SCT_A_ID &&
				stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
		{
			bA = true;
		}
		else if(componentList.at(i) == SCT_B_ID	&&
				stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
		{
			bB = true;
		}
	}

	// to be sure that A is always before B ...
	if(bA)	m_strComponent.push_back(XML_VALUE_SECTION_A);
	if(bB)	m_strComponent.push_back(XML_VALUE_SECTION_B);

	for(uint8_t i = 0; i < m_strComponent.size() * SCT_NUM_TOT_SLOTS; i++)
	{
		m_strOffsets.push_back("");
	}

	m_counterAction = 0;
	m_bAskBoards = false;
}

OpGetPressureOffsets::~OpGetPressureOffsets()
{
	m_strComponent.clear();
	m_strOffsets.clear();
}

int OpGetPressureOffsets::perform()
{
	int8_t res = -1;

	if(m_bAskBoards == false)
	{
		// read data from memory
		for(m_counterAction = 0; m_counterAction < m_strComponent.size(); m_counterAction++)
		{
			uint8_t section = 0xFF;

			if(m_strComponent.at(m_counterAction).compare(XML_VALUE_SECTION_A) == 0)
			{
				section = SCT_A_ID;
			}
			else if(m_strComponent.at(m_counterAction).compare(XML_VALUE_SECTION_B) == 0)
			{
				section = SCT_B_ID;
			}

			if(section != 0xFF)
			{
				int32_t value;
				for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
				{
	#ifdef __DEBUG_PROTOCOL
					value = (8000000 + m_counterAction * channel);
	#else
					value = sectionBoardLinkSingleton(section)->m_pPIB->getOffsetChannel(channel);
	#endif
					m_strOffsets.at(m_counterAction * SCT_NUM_TOT_SLOTS + channel).assign(to_string(value));
				}
			}
		}
		// send back the final reply
		compileSendOutCmd();
		// end of story !
		return eOperationWaitForDeath;
	}

	// start the action on the current item of the vector

	if(m_strComponent.at(m_counterAction).compare(XML_VALUE_SECTION_A) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_strComponent.at(m_counterAction).compare(XML_VALUE_SECTION_B) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}

	if(res != eOperationRunning)
	{
		m_pLogger->log(LOG_WARNING, "OpGetPressureOffsets::perform-->Error %s", m_strComponent.at(m_counterAction).c_str());
		// operation completed -> send reply and close it
		closeOperation();
	}

	// the return code has to be eOperationRunning to prevent PostMan to delete the Operation:
	// if necessary it's the closeOperation that has done it
	return eOperationRunning;
}

GetPressureOffsetsPayload * OpGetPressureOffsets::buildPayload(void)
{
	GetPressureOffsetsPayload * pGetPressureOffsetsPayload = new GetPressureOffsetsPayload();

	pGetPressureOffsetsPayload->setOutValues(m_strComponent, m_strOffsets);

	return pGetPressureOffsetsPayload;
}

int OpGetPressureOffsets::executeGetPressureOffset(int ubNumSection)
{
	int res = sectionBoardLinkSingleton(ubNumSection)->m_pPIB->getPressureConfiguration();

	if(res != 0)
	{
		m_pLogger->log(LOG_WARNING, "OpGetPressureOffsets: error");
		return -1;
	}

	int32_t value;
	for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
	{
		value = sectionBoardLinkSingleton(ubNumSection)->m_pPIB->getOffsetChannel(channel);
		m_strOffsets.at(m_counterAction * SCT_NUM_TOT_SLOTS + channel).assign(to_string(value));
	}

	return res;
}

int OpGetPressureOffsets::executeCmd( int lNum )
{
	//action to perform
	int res = executeGetPressureOffset(lNum);

	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "OpGetPressureOffsets::execute-->execute not performed");
		// operation completed -> send reply and close it
		closeOperation();
		return -1;
	}

	std::string previousComponent;
	previousComponent.assign(m_strComponent.at(m_counterAction));

	// increase the action counter
	m_counterAction++;

	// if we have performed the last action compose and send the reply, stop the loop
	if(m_counterAction >= m_strComponent.size() )
	{
		// operation completed -> send reply and close it
		closeOperation();
		res = -1;
	}
	else if(previousComponent.compare(m_strComponent.at(m_counterAction)) != 0)
	{
		// actions on the current device are completed:
		// --> go for next action
		perform();
		// -> unlock the actual
		res = -1;
	}

	m_pLogger->log(LOG_INFO, "OpGetPressureOffsets::execute-->completed");

	return res;
}

int OpGetPressureOffsets::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdGetPressureOffsets cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	GetPressureOffsetsPayload * pGetPressureOffsetsPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pGetPressureOffsetsPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpGetPressureOffsets::compileSendOutCmd--> unable to serialize OutCmdGetPresureOffsets");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

