/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpTrayRead.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpTrayRead class.
 @details

 ****************************************************************************
*/

#include "OpTrayRead.h"
#include "OutCmdTrayRead.h"
#include "MainExecutor.h"
#include "TrayReadPayload.h"
#include "SectionBoardTray.h"
#include "WebServerAndProtocolInclude.h"

#define NSHR_DEF_MOVEMENT			100
#define NSH_CALIBR_POSITIONS_NUM	3
#define TRAY_READ_CODED_COMMAND		"Read"
#define TRAY_OUT_CODED_COMMAND		"Out"


OpTrayRead::OpTrayRead() : Operation("OpTrayRead")
{
	m_eReadState = eState::eStart;
	m_cSectionNum = -1;
	m_vRFUvalues.clear();
	m_vPositions.clear();
	m_vRFUvalues.resize(READING_POSITION_NUM);
	m_vPositions.resize(READING_POSITION_NUM);
}

OpTrayRead::~OpTrayRead()
{

}

void OpTrayRead::setSection(int8_t cSecNum)
{
	m_cSectionNum = cSecNum;
}

int OpTrayRead::perform()
{
	if ( m_cSectionNum == SCT_NONE_ID )	return -1;

    int res = eOperationError;

	switch	( m_eReadState )
	{
		case eState::eStart:
		case eState::eExecute:
		case eState::eNshError:
		case eState::eClean:
            res = nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
		break;

		case eState::eMoveToTrayRead:
		case eState::eMoveToTrayOut:
		case eState::eSectError:
            res = sectionBoardManagerSingleton(m_cSectionNum)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
		break;

		case eState::eStop:
		{
			restoreStateMachine();
            compileSendOutCmd();

            operationSingleton()->removeOperation(this);
            res = eOperationWaitForDeath;
        }
		break;
	}

    return res;
}

int OpTrayRead::executeCmd(int lNum)
{
	if ( lNum > SCT_NONE_ID )	return -1;

	enum eDevice { eNsh, eSection, eUnInit };
	eDevice eCurrDevice = eUnInit;
	eDevice eNextDevice = eUnInit;

	while ( eCurrDevice == eNextDevice && m_eReadState != eState::eStop )
	{
		switch ( m_eReadState )
		{
			case eState::eStart:
				m_eReadState = eState::eMoveToTrayRead;
				eCurrDevice = eNsh;
				eNextDevice = eSection;
			break;

			case eState::eMoveToTrayRead:
			{
				eCurrDevice = eSection;
				eNextDevice = eNsh;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(lNum)->m_pTray->getCodedFromLabel(TRAY_READ_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eReadState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpTrayRead::executeCmd: unable to get coded position from tray read label [" TRAY_READ_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_INFO, "OpTrayRead::executeCmd: move tray to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(lNum)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eReadState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpTrayRead::executeCmd: unable to move to coded position");
					break;
				}
				m_eReadState = eState::eExecute;
			}
			break;

			case eState::eExecute:
			{
				eCurrDevice = eNsh;

                const int liNumPositionToBeRead = READING_POSITION_NUM;

				fillPositionsVector(m_cSectionNum);

				int liRes = 0;
				for (int i = 0; i != liNumPositionToBeRead; i++)
				{
					liRes = nshBoardManagerSingleton()->moveNSHandRead(stoi(m_vPositions[i]), m_vRFUvalues[i]);
					if ( liRes )
					{
						eNextDevice = eNsh;
						m_eReadState = eState::eNshError;
						m_pLogger->log(LOG_ERR, "OpTrayRead::executeCmd: Impossible to move NSH and read the tray");
						break;
					}
				}
				eNextDevice = eSection;
				m_eReadState = eState::eMoveToTrayOut;
			}
			break;

			case eState::eMoveToTrayOut:
			{
				eCurrDevice = eSection;
				eNextDevice = eNsh;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(lNum)->m_pTray->getCodedFromLabel(TRAY_OUT_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eReadState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpTrayRead::executeCmd: unable to get coded position from tray out label [" TRAY_OUT_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_DEBUG, "OpTrayRead::executeCmd: move tray to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(lNum)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eReadState = eState::eNshError;
                    m_pLogger->log(LOG_ERR, "OpTrayRead::executeCmd: unable to perform tray read");
					break;
				}
				m_eReadState = eState::eClean;
			}
			break;

			case eState::eNshError:
			{
				eCurrDevice = eNsh;
				eNextDevice = eSection;
				// Move machine to default position
				MainExecutor::getInstance()->m_SPIBoard.move(eHome);
				m_eReadState = eState::eSectError;
			}
			break;

			case eState::eSectError:
			{
				m_eReadState = eState::eStop;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(lNum)->m_pTray->getCodedFromLabel(TRAY_OUT_CODED_COMMAND);
				if ( strCoded.empty() )	break;

				char ubPosCode = *strCoded.c_str();
				int liRes = sectionBoardLinkSingleton(lNum)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )			break;
			}
			break;

			case eState::eClean:
			{
				m_eReadState = eState::eStop;
				string strPos("");
				string strCodedName("SS");
				nshBoardManagerSingleton()->m_pConfig->getItemValue(strCodedName, strPos);
				int liPos = stoi(strPos);
				int bRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
				if ( ! bRes )
				{
					m_pLogger->log(LOG_ERR, "OpTrayRead::executeCmd: unable to move NSH motor to desired position");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpTrayRead::executeCmd: moved to SS position");
				m_pLogger->log(LOG_INFO, "OpTrayRead::execute: TRAY_READ executed");
			}
			break;

			default:	m_eReadState = eState::eStop; 	break;
		}
	}

	int liRes = perform();
    if(liRes == -1)
    {
        m_pLogger->log(LOG_ERR, "OpTrayRead::executeCmd-->execute not performed");
    }
    else if ( liRes != eOperationRunning )
	{
        m_pLogger->log(LOG_DEBUG, "OpTrayRead::executeCmd-->completed");
		return -1;
	}

	return eOperationRunning;
}

TrayReadPayload* OpTrayRead::buildPayload(void)
{
	TrayReadPayload *pPayload = new TrayReadPayload();

	pPayload->setTrayReadAttributes(m_vRFUvalues, m_cSectionNum);

	return pPayload;
}

int OpTrayRead::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdTrayRead cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	TrayReadPayload * pTrayReadPayload = buildPayload();
	// ... and assign it to the outcommand so that the serializle will extract the info
	cmd.setPayload((Payload *)pTrayReadPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpTrayRead::execute--> unable to serialize OutCmdOpTrayRead");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

void OpTrayRead::fillPositionsVector(int liSectionNum)
{
	if ( ( liSectionNum != SCT_A_ID ) && ( liSectionNum != SCT_B_ID ) )		return;

    int liInitVal = liSectionNum * SCT_NUM_TOT_SLOTS;
	int liEndVal = liInitVal + SCT_NUM_TOT_SLOTS;

	for ( int i = liInitVal; i != liEndVal; i++)
	{
		int liPos0 = 3 * (i-liInitVal);
        string strName("Slot_" + to_string(i + 1) + "L");
		nshBoardManagerSingleton()->m_pConfig->getItemValue(strName, m_vPositions[liPos0]);
        strName = "Slot_" + to_string(i + 1) + "C";
		nshBoardManagerSingleton()->m_pConfig->getItemValue(strName, m_vPositions[liPos0+1]);
        strName = "Slot_" + to_string(i + 1) + "R";
		nshBoardManagerSingleton()->m_pConfig->getItemValue(strName, m_vPositions[liPos0+2]);
	}
}
