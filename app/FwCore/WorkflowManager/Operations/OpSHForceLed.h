/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSHForceLed.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSHForceLed class.
 @details

 ****************************************************************************
*/

#ifndef OPSHFORCELED_H
#define OPSHFORCELED_H

#include "Operation.h"
#include "Payload.h"
#include "MainExecutor.h"

class SHForceLedPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SHFORCELED command
 * ********************************************************************************************************************
 */

class OpSHForceLed : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpSHForceLed default constructor
		 * ***********************************************************************************************************
		 */
		OpSHForceLed();

		/*! ***********************************************************************************************************
		 * @brief ~OpSignalRead default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpSHForceLed();

		/*!
		 * @brief setParams	set the parameters for calibration
		 * @param strDuration
		 */
		void setParam(string strDuration);

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief  compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

	private:

		SHForceLedPayload* buildPayload();

    private:

        string	m_strDuration;
        string	m_strStart;

};

#endif // OPSHFORCELED_H
