/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetFanSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetFanSettings class.
 @details

 ****************************************************************************
*/

#include "OpGetFanSettings.h"
#include "OutCmdGetFanSettings.h"
#include "GetFanSettingsPayload.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

#include "CommonInclude.h"
#include "ModuleInclude.h"

OpGetFanSettings::OpGetFanSettings() : Operation("OpGetFanSettings")
{

}

OpGetFanSettings::~OpGetFanSettings()
{
	/* Nothing to do yet */
}

int OpGetFanSettings::perform()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdGetFanSettings cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// build the payload of the asynchrounous reply
	GetFanSettingsPayload * pGetFanSettingsPayload = new GetFanSettingsPayload();

	pGetFanSettingsPayload->setFanPower(eFanSectionA, masterBoardManagerSingleton()->getFanPower(eFanSectionA));
	pGetFanSettingsPayload->setFanPower(eFanSectionB, masterBoardManagerSingleton()->getFanPower(eFanSectionB));
	pGetFanSettingsPayload->setFanPower(eFanSupply, masterBoardManagerSingleton()->getFanPower(eFanSupply));

	// and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pGetFanSettingsPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSetFanSettings::execute--> unable to serialize");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationWaitForDeath;
}

