/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpReadSec.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpReadSec class.
 @details

 ****************************************************************************
*/

#include <iterator>

#include "OpReadSec.h"
#include "THReadSec.h"
#include "MainExecutor.h"
#include "SectionBoardSPR.h"
#include "SectionBoardTray.h"
#include "SectionBoardTower.h"
#include "OutCmdReadSec.h"
#include "ReadSecPayload.h"
#include "WebServerAndProtocolInclude.h"
#include "SprDetection/SprDetectionLink.h"

#define TOWER_UP_CODED_COMMAND			"Up"
#define TRAY_OUT_CODED_COMMAND			"Out"
#define TRAY_READ_CODED_COMMAND			"Read"
#define TRAY_BC_CODED_COMMAND			"Strip_BC"
#define SPR_READ_DM_CODED_COMMAND		"Data_matrix"
#define SPR_LOAD_DM_CODED_COMMAND		"Load"
#define CODE_NOT_DECODED_MSG			"______" // when dtmx not decoded, but we don't know if the spr is present or not
#define CODE_NOT_PRESENT_MSG			"!"		 // when the spr is not present
#define CODE_PRESENT_MSG				"?"		 // when the spr is present, but dtmx not decoded
#define READER_ERROR_MESSAGE			"ERROR!"
#define READER_REBOOTED					"REBOOT"
#define DEF_CROPPED_SPR_LENGTH			250
#define DEF_FAKE_IMG_SIZE				40000
#define DEF_FAKE_IMG_SIDE				200


OpReadSec::OpReadSec() : Operation("OpReadSec")
{
	m_vReadSec.resize(SCT_NUM_TOT_SLOTS);
	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		m_vTargets[i].clear();
	}
	m_eCurrState = eState::eZero;
	m_eCurrTarget = eTarget::eStrip;
	m_nCurrSlotId = SCT_SLOT_1_ID;
}

OpReadSec::~OpReadSec()
{

}

void OpReadSec::setParameters(int section, const stripReadSection_tag * stripReadSection, CameraSettings& cSettings)
{
	memcpy(m_stripReadSection, stripReadSection, sizeof(stripReadSection_tag) * SCT_NUM_TOT_SLOTS);

	/*** TODO: delete, only for testing with HMI sw ***/
	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( m_stripReadSection[i].m_SampleDetection.m_nSettingsX0 == 0 )
		{
			m_stripReadSection[i].m_SampleDetection.m_bCheckX0 = false;
			m_stripReadSection[i].m_SampleDetection.m_bImageX0 = false;
		}

		if ( m_stripReadSection[i].m_SampleDetection.m_nSettingsX3 == 0 )
		{
			m_stripReadSection[i].m_SampleDetection.m_bCheckX3 = false;
			m_stripReadSection[i].m_SampleDetection.m_bImageX3 = false;
		}
	}
	/************ TODO: Delete until here *************/

	m_sectionId = section;
	m_cSettings = cSettings;

	createActionList();

	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		m_vReadSec[i].bIsEnabled = m_stripReadSection[i].m_bEnabled;
	}
}

int OpReadSec::perform(void)
{
	if ( this->getUsage() == "SERVICE" && ! m_cSettings.isScheduledReadSec(1) )
	{
		switch ( m_eCurrState )
		{
			case eState::eMoveNshToBC:
			case eState::eMoveNshToDM:
			case eState::eMoveNshToSample:
			case eState::eReadSubstrate:
			case eState::eNshRestore:
				return nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
			break;

			case eState::eZero:
			case eState::eStart:
			case eState::eMoveToSprRead:
			case eState::eMoveToTrayRead:
			case eState::eMoveToTraySample:
			case eState::eSectRestore:
				return sectionBoardManagerSingleton(m_sectionId)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
			break;

			case eState::eSendBatch:
			case eState::eReadBarcode:
			case eState::eReadDataMatrix:
			case eState::eGetPicture:
			case eState::eCheckSprPresence:
			case eState::eSampleAlgorithm:
				return cameraBoardManagerSingleton()->setCameraAction(eCameraActionExecute, (Operation*)this);
			break;

			case eState::eInstrRestore:
			case eState::eEnd:
				return masterBoardManagerSingleton()->setMasterAction(eMasterActionExecute, (Operation*)this);
			break;

			case eState::eStop:
			{
				restoreStateMachine();
				int8_t cResult = compileSendOutCmd();
				if ( cResult == eOperationEnabledForDeath )
				{
					operationSingleton()->removeOperation(this);
					m_pLogger->log(LOG_INFO, "NSHBoardManager::workerThread-->operation removed");
				}
				else
				{
					m_pLogger->log(LOG_ERR, "NSHBoardManager::workerThread-->NO operation removal");
				}
			}
			break;
		}
		return 0;
	}
	else
	{
		return eOperationRunning;
	}
}

int OpReadSec::executeCmd(int liNum)
{
	// Handle rehoming of the instrument
	if ( m_eCurrState == eState::eInstrRestore || m_eCurrState == eState::eSectRestore ||
		 m_eCurrState == eState::eNshRestore )
	{
		executeRestore();
		goto doJob;
	}

	/** *******************************************************************************
	 * If the current slot has not to perform the operation on the current target
	 * or if it is empty, then go to the next slot
	 *********************************************************************************/
	while ( m_nCurrSlotId < SCT_NUM_TOT_SLOTS )
	{
		if ( m_vTargets[m_nCurrSlotId].empty() || m_vTargets[m_nCurrSlotId].front() != m_eCurrTarget )
		{
			m_nCurrSlotId++;
		}
		else break;
	}

	// Handle new target
	if ( m_nCurrSlotId >= SCT_NUM_TOT_SLOTS )
	{
		m_eCurrTarget = eTarget::eNone;
		for ( m_nCurrSlotId = SCT_SLOT_1_ID; m_nCurrSlotId != SCT_NUM_TOT_SLOTS; ++m_nCurrSlotId )
		{
			if ( m_vTargets[m_nCurrSlotId].empty() )	continue;
			m_eCurrTarget = m_vTargets[m_nCurrSlotId].front();
			break;
		}
		// Set instrument default position if we have ended the loop
		if ( m_eCurrTarget == eTarget::eNone )
		{
			m_eCurrState = eState::eInstrRestore;
			executeRestore();
			goto doJob;
		}
	}

	// execute actions
	if ( m_vTargets[m_nCurrSlotId].front() == eTarget::eStrip )
	{
		executeStripRead();
	}
	else if ( m_vTargets[m_nCurrSlotId].front() == eTarget::eSpr )
	{
		executeSprRead();
	}
	else if ( m_vTargets[m_nCurrSlotId].front() == eTarget::eSubstrate )
	{
		executeSubstrateRead();
	}
	else if ( m_vTargets[m_nCurrSlotId].front() == eTarget::eSample0 ||
			  m_vTargets[m_nCurrSlotId].front() == eTarget::eSample3 )
	{
		executeSampleRead(m_vTargets[m_nCurrSlotId].front());
	}
	else
	{
		executeRestore();
		goto doJob;
	}

	doJob:
	int liRes = liNum;
	liRes = perform();
	if ( liRes != eOperationRunning )
	{
		m_pLogger->log(LOG_ERR, "OpReadSec::executeCmd-->execute not performed");
		return -1;
	}
	return eOperationRunning;
}

int OpReadSec::executeStripRead()
{
	enum eDevice { eNsh, eSection, eCamera, eMaster, eUnInit };
	eDevice eCurrDevice = eUnInit;
	eDevice eNextDevice = eUnInit;

	bool bLoop = true;
	// Not to call the perform on the same device -> Op in progress otherwise
	while ( eCurrDevice == eNextDevice && bLoop )
	{
		switch ( m_eCurrState )
		{
			case eState::eZero:
            {
				eCurrDevice = eSection;
				eNextDevice = eMaster;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pTower->getCodedFromLabel(TOWER_UP_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to get coded position from tower up label [" TOWER_UP_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_INFO, "OpReadSec::executeStripRead: move tray to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(m_sectionId)->m_pTower->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to move to position [%d]", ubPosCode);
					break;
                }
                // home procedure without considering loss of steps (user may have moved the device)
                sectionBoardLinkSingleton(m_sectionId)->m_pTray->m_CommonMessage.searchHome(1);
				sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.moveToAbsolutePosition(10, TH_MOVE_CMD_TIMEOUT);
                sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.searchHome(1);
                eNextDevice = eSection;
				m_eCurrState = eState::eStart;
			}
			break;

			case eState::eStart:
				msleep(50);
				m_vReadSec[m_nCurrSlotId].Strip.bIsEnabled = true;
				eCurrDevice = eSection;
				eNextDevice = eCamera;
				m_eCurrState = eState::eSendBatch;
			break;

			case eState::eSendBatch:
			{
				eCurrDevice = eCamera;
				eNextDevice = eSection;
				uint8_t ucSlot = m_nCurrSlotId+1; // slot1 has to be 1 here, not 0
				ucSlot = ( m_sectionId == SCT_A_ID ) ? ucSlot : ucSlot + SCT_NUM_TOT_SLOTS;
				int liRes = usbBoardLinkSingleton()->m_pCR8062->uploadBatchFile(ucSlot, eBatchTarget::eLinearBarcode);
				if ( liRes )	recoverCamera();
				else			m_eCurrState = eState::eMoveToTrayRead;
			}
			break;

			case eState::eMoveToTrayRead:
			{
				eCurrDevice = eSection;
				eNextDevice = eMaster;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pTray->getCodedFromLabel(TRAY_BC_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to get coded position from tray read label [" TRAY_READ_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_INFO, "OpReadSec::executeStripRead: move tray to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(m_sectionId)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to move to position [%d]", ubPosCode);
					break;
				}
				eNextDevice = eSection;
				m_eCurrState = eState::eMoveToSprRead;
			}
			break;

			case eState::eMoveToSprRead:
			{
				eCurrDevice = eSection;
				eNextDevice = eMaster;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pSPR->getCodedFromLabel(SPR_READ_DM_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to get coded position from data matrix read label [" SPR_READ_DM_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_DEBUG, "OpReadSec::executeStripRead: move spr to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to move to position [%d]", ubPosCode);
					break;
				}
				eNextDevice = eNsh;
				m_eCurrState = eState::eMoveNshToBC;
			}
			break;

			case eState::eMoveNshToBC:
			{
				eCurrDevice = eNsh;
				string strPos("");
				int liSlotId = m_nCurrSlotId + 1; // slot1 has to be 1 here, not 0
				liSlotId = ( m_sectionId == SCT_A_ID ) ? liSlotId : liSlotId + SCT_NUM_TOT_SLOTS;
				string strCodedName = "CameraBC_" + to_string(liSlotId);
				nshBoardManagerSingleton()->m_pConfig->getItemValue(strCodedName, strPos);

				int liPos = stoi(strPos);
				int liRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
				if ( ! liRes )
				{
					eNextDevice = eMaster;
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to move NSH motor to desired position");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpReadSec::executeStripRead: moved NSH to slot position");
				eNextDevice = eCamera;
				m_eCurrState = eState::eReadBarcode;
			}
			break;

			case eState::eReadBarcode:
			{
				eCurrDevice = eCamera;
				eNextDevice = eCamera;
				string strBarcode("");
				int liRes = usbReaderSingleton()->getLinearBarcode(strBarcode);
				if ( liRes != ERR_NONE )
				{
					strBarcode = CODE_PRESENT_MSG;
					if ( liRes == -1 )
					{
						recoverCamera();
						break;
					}
				}
				m_vReadSec[m_nCurrSlotId].Strip.strCode = strBarcode;
				// Restart next loop from the beginning
				if ( m_stripReadSection[m_nCurrSlotId].m_bStripImage )
				{
					m_eCurrState = eState::eGetPicture;
				}
				else
				{
					m_eCurrState = eState::eEnd;
				}
				msleep(50);
			}
			break;

			case eState::eGetPicture:
			{
				eCurrDevice = eCamera;
				eNextDevice = eCamera;

				int liRes = ERR_NONE;
				// Go on only if RAW
				if ( usbCameraSingleton()->getImageFormat() != eRAW )
				{
					liRes = usbCameraSingleton()->setImageMode(eRAW);
					if ( liRes )
					{
						m_eCurrState = eState::eInstrRestore;
						break;
					}
				}

				liRes = usbCameraSingleton()->getPicture(m_vReadSec[m_nCurrSlotId].Strip.vImage, &m_InfoImage);
				if ( liRes != ERR_NONE )
				{
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to get picture");
					recoverCamera();
					// Adding this fake image the user understands something went wrong
					addFakeImage(m_vReadSec[m_nCurrSlotId].Strip.vImage);
					break;
				}

				usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&m_InfoImage, m_vReadSec[m_nCurrSlotId].Strip.vImage);
				m_eCurrState = eState::eEnd;
			}
			break;

			case eState::eEnd:
				m_vTargets[m_nCurrSlotId].erase(m_vTargets[m_nCurrSlotId].begin());
				++m_nCurrSlotId; // change slot
				m_eCurrState = eState::eStart;
				bLoop = false;
			break;

			default:
				bLoop = false;
				m_eCurrState = eState::eInstrRestore;
			break;
		}
	}
	return 0;
}

int OpReadSec::executeSprRead()
{
	enum eDevice { eNsh, eSection, eCamera, eMaster, eUnInit };
	eDevice eCurrDevice = eUnInit;
	eDevice eNextDevice = eUnInit;

	bool bLoop = true;
	// Not to call the perform on the same device -> Op in progress otherwise
	while ( eCurrDevice == eNextDevice && bLoop )
	{
		switch ( m_eCurrState )
		{
			case eState::eZero:
			{
				eCurrDevice = eSection;
				eNextDevice = eMaster;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pTower->getCodedFromLabel(TOWER_UP_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to get coded position from tower up label [" TOWER_UP_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_INFO, "OpReadSec::executeStripRead: move tray to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(m_sectionId)->m_pTower->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to move to position [%d]", ubPosCode);
					break;
				}
                // home procedure without considering loss of steps (user may have moved the device)
                sectionBoardLinkSingleton(m_sectionId)->m_pTray->m_CommonMessage.searchHome(1);
                sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.moveToAbsolutePosition(10, TH_MOVE_CMD_TIMEOUT);
                sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.searchHome(1);
				eNextDevice = eSection;
				m_eCurrState = eState::eStart;
			}
			break;

			case eState::eStart:
				msleep(50);
				m_vReadSec[m_nCurrSlotId].Spr.bIsEnabled = true;
				eCurrDevice = eSection;
				eNextDevice = eCamera;
				m_eCurrState = eState::eSendBatch;
			break;

			case eState::eSendBatch:
			{
				eCurrDevice = eCamera;
				eNextDevice = eSection;
				uint8_t ucSlot = m_nCurrSlotId+1; // slot1 has to be 1 here, not 0
				ucSlot = ( m_sectionId == SCT_A_ID ) ? ucSlot : ucSlot + SCT_NUM_TOT_SLOTS;

				eBatchTarget eTarget = m_cSettings.hasDtmxToBeDecoded(1) ? eBatchTarget::eDataMatrix : eBatchTarget::eConeAbsence;
				int liRes = usbBoardLinkSingleton()->m_pCR8062->uploadBatchFile(ucSlot, eTarget);
				if ( liRes )	recoverCamera();
				else			m_eCurrState = eState::eMoveToTrayRead;
			}
			break;

			case eState::eMoveToTrayRead:
			{
				eCurrDevice = eSection;
				eNextDevice = eMaster;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pTray->getCodedFromLabel(TRAY_BC_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to get coded position from tray read label [" TRAY_BC_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_INFO, "OpReadSec::executeStripRead: move tray to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(m_sectionId)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to move to position [%d]", ubPosCode);
					break;
				}
				eNextDevice = eSection;
				m_eCurrState = eState::eMoveToSprRead;
			}
			break;

			case eState::eMoveToSprRead:
			{
				eCurrDevice = eSection;
				eNextDevice = eMaster;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pSPR->getCodedFromLabel(SPR_READ_DM_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSprRead: unable to get coded position from data matrix read label [" SPR_READ_DM_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_DEBUG, "OpReadSec::executeSprRead: move spr to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSprRead: unable to move to position [%d]", ubPosCode);
					break;
				}
				eNextDevice = eNsh;
				m_eCurrState = eState::eMoveNshToDM;
			}
			break;

			case eState::eMoveNshToDM:
			{
				eCurrDevice = eNsh;
				string strPos("");
				int liSlotId = m_nCurrSlotId + 1; // slot1 has to be 1 here, not 0
				liSlotId = ( m_sectionId == SCT_A_ID ) ? liSlotId : liSlotId + SCT_NUM_TOT_SLOTS;
				string strCodedName = "CameraDataMatrix_" + to_string(liSlotId);
				nshBoardManagerSingleton()->m_pConfig->getItemValue(strCodedName, strPos);

				int liPos = stoi(strPos);
				int liRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
				if ( ! liRes )
				{
					eNextDevice = eMaster;
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSprRead: unable to move NSH motor to desired position");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpReadSec::executeSprRead: moved NSH to slot position");
				eNextDevice = eCamera;
				if ( m_cSettings.hasDtmxToBeDecoded(1) )
				{
					m_eCurrState = eState::eReadDataMatrix;
				}
				else
				{
					m_eCurrState = eState::eCheckSprPresence;
				}
			}
			break;

			case eState::eReadDataMatrix:
			{
				eCurrDevice = eCamera;
				eNextDevice = eCamera;
				string strDataMatrix("");
				// The work around can change the reading timeout. We want to restore it each time
				int liOldDecodingTimeOut = usbReaderSingleton()->getDecodingTimeoutMs();
				int liRes = usbReaderSingleton()->_debug_DM_read(strDataMatrix, m_sectionId);
//				int liRes = usbReaderSingleton()->getDataMatrix(strDataMatrix);
				if ( liRes == ERR_USB_PERIPH_COMMUNICATION )
				{
					recoverCamera();
					usbReaderSingleton()->setDecodingTimeMsec(liOldDecodingTimeOut);
					strDataMatrix = READER_REBOOTED;
				}
				if ( strDataMatrix.empty() )		strDataMatrix.assign(READER_ERROR_MESSAGE);

				int liNewDecodingTimeOut = usbReaderSingleton()->getDecodingTimeoutMs();
				if ( liNewDecodingTimeOut != liOldDecodingTimeOut )
				{
					usbReaderSingleton()->setDecodingTimeMsec(liOldDecodingTimeOut);
				}
				m_vReadSec[m_nCurrSlotId].Spr.strCode = strDataMatrix;
				// Restart next loop from the beginning
				if ( m_stripReadSection[m_nCurrSlotId].m_bSprImage )
				{
					m_eCurrState = eState::eGetPicture;
				}
				else
				{
					m_eCurrState = eState::eEnd;
				}
				msleep(50); // after decoding
			}
			break;

			case eState::eGetPicture:
			{
				eCurrDevice = eCamera;
				eNextDevice = eCamera;
				m_eCurrState = eState::eInstrRestore;
				int liRes = ERR_NONE;

				liRes = usbCameraSingleton()->getPicture(m_vReadSec[m_nCurrSlotId].Spr.vImage, &m_InfoImage);
				if ( liRes != ERR_NONE )
				{
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSprRead: unable to get picture");
					recoverCamera();
					// Adding this fake image the user understands something went wrong
					addFakeImage(m_vReadSec[m_nCurrSlotId].Spr.vImage);
					break;
				}

				// add only if image is raw
				usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&m_InfoImage, m_vReadSec[m_nCurrSlotId].Spr.vImage);
				m_eCurrState = eState::eEnd;
			}
			break;

			case eState::eCheckSprPresence:
			{
				eCurrDevice = eCamera;
				eNextDevice = eCamera;

				m_eCurrState = eState::eInstrRestore;
				int liRes = ERR_NONE;
				if ( usbCameraSingleton()->getImageFormat() != eRAW )
				{
					liRes = usbCameraSingleton()->setImageMode(eRAW);
					if ( liRes )				break;
				}

				liRes = usbCameraSingleton()->getPicture(m_vReadSec[m_nCurrSlotId].Spr.vImage, &m_InfoImage);
				if ( liRes != ERR_NONE )
				{
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSprRead: unable to get picture");
					recoverCamera();
					// Adding this fake image the user understands something went wrong
					addFakeImage(m_vReadSec[m_nCurrSlotId].Spr.vImage);
					break;
				}

				vector<unsigned char> vImage(m_vReadSec[m_nCurrSlotId].Spr.vImage);
				if ( int(vImage.size()) == (m_InfoImage.liWidth*m_InfoImage.liHeight) )
				{
					usbBoardLinkSingleton()->m_pCR8062->ruotate180degree<unsigned char>(vImage.data(), m_InfoImage.liWidth, m_InfoImage.liHeight);
					SprDetectionLink detector;
					detector.init(vImage.data(), m_pLogger, m_InfoImage.liWidth, m_InfoImage.liHeight);
					bool bIsPresent = false;

					if ( m_InfoImage.liWidth == DEF_CROPPED_SPR_LENGTH &&
						 m_InfoImage.liHeight == DEF_CROPPED_SPR_LENGTH )
					{
						detector.perform_cropped(bIsPresent);
					}
					else
					{
						detector.perform(bIsPresent);
					}
					if ( ! bIsPresent )		m_vReadSec[m_nCurrSlotId].Spr.strCode.assign(CODE_NOT_PRESENT_MSG);
					else					m_vReadSec[m_nCurrSlotId].Spr.strCode.assign(CODE_PRESENT_MSG);
				}
				else
				{
					m_vReadSec[m_nCurrSlotId].Spr.strCode.assign(CODE_NOT_DECODED_MSG);
				}

				// add only if image is raw
				usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&m_InfoImage, m_vReadSec[m_nCurrSlotId].Spr.vImage);

				// Don't send back the image if not requested
				if ( ! m_stripReadSection[m_nCurrSlotId].m_bSprImage )	m_vReadSec[m_nCurrSlotId].Spr.vImage.clear();

				m_eCurrState = eState::eEnd;
			}
			break;

			case eState::eEnd:
				m_vTargets[m_nCurrSlotId].erase(m_vTargets[m_nCurrSlotId].begin());
				++m_nCurrSlotId; // change slot
				m_eCurrState = eState::eStart;
				bLoop = false;
			break;

			default:
				bLoop = false;
				m_eCurrState = eState::eInstrRestore;
			break;
		}
	}

	return 0;
}

int OpReadSec::executeSubstrateRead()
{
	enum eDevice { eNsh, eSection, eMaster, eUnInit };
	eDevice eCurrDevice = eUnInit;
	eDevice eNextDevice = eUnInit;

	bool bLoop = true;
	// Not to call the perform on the same device -> Op in progress otherwise
	while ( eCurrDevice == eNextDevice && bLoop )
	{
		switch ( m_eCurrState )
		{
			case eState::eZero:
                // home procedure without considering loss of steps (user may have moved the device)
                sectionBoardLinkSingleton(m_sectionId)->m_pTray->m_CommonMessage.searchHome(1);
                sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.moveToAbsolutePosition(10, TH_MOVE_CMD_TIMEOUT);
                sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.searchHome(1);
                eCurrDevice = eSection;
				eNextDevice = eSection;
				m_eCurrState = eState::eStart;
			break;

			case eState::eStart:
				msleep(50);
				m_vReadSec[m_nCurrSlotId].Substrate.bIsEnabled = true;
				eCurrDevice = eSection;
				eNextDevice = eSection;
				m_eCurrState = eState::eMoveToTrayRead;
			break;

			case eState::eMoveToTrayRead:
			{
				eCurrDevice = eSection;
				eNextDevice = eMaster;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pTray->getCodedFromLabel(TRAY_READ_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSubstrateRead: unable to get coded position from tray read label [" TRAY_READ_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_INFO, "OpReadSec::executeSubstrateRead: move tray to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(m_sectionId)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSubstrateRead: unable to move to position [%d]", ubPosCode);
					break;
				}
				eNextDevice = eNsh;
				m_eCurrState = eState::eReadSubstrate;
			}
			break;

			case eState::eReadSubstrate:
			{
				eCurrDevice = eNsh;
				string strPos("");
				int liSlotId = m_nCurrSlotId + 1; // slot1 has to be 1 here, not 0
				liSlotId = ( m_sectionId == SCT_A_ID ) ? liSlotId : liSlotId + SCT_NUM_TOT_SLOTS;
				string strCodedName = "Slot_" + to_string(liSlotId) + "C";
				nshBoardManagerSingleton()->m_pConfig->getItemValue(strCodedName, strPos);

				int liPos = stoi(strPos);
				int liRes = nshBoardManagerSingleton()->moveNSHandRead(liPos, m_vReadSec[m_nCurrSlotId].Substrate.liSubsRead);
				if ( liRes )
				{
					eNextDevice = eMaster;
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSubstrateRead: unable to move or read RFU");
					break;
				}
				eNextDevice = eNsh;
				m_eCurrState = eState::eEnd;
			}
			break;

			case eState::eEnd:
				m_vTargets[m_nCurrSlotId].erase(m_vTargets[m_nCurrSlotId].begin());
				++m_nCurrSlotId; // change slot
				m_eCurrState = eState::eStart;
				bLoop = false;
			break;

			default:
				bLoop = false;
				m_eCurrState = eState::eInstrRestore;
			break;
		}
	}
	return 0;
}

int OpReadSec::executeSampleRead(eTarget target)
{
	if ( target != eTarget::eSample0 && target != eTarget::eSample3 ) return -1;

	enum eDevice { eNsh, eSection, eCamera, eMaster, eUnInit };
	eDevice eCurrDevice = eUnInit;
	eDevice eNextDevice = eUnInit;

	bool bLoop = true;
	// Not to call the perform on the same device -> Op in progress otherwise
	while ( eCurrDevice == eNextDevice && bLoop )
	{
		switch ( m_eCurrState )
		{
			case eState::eZero:
			{
				eCurrDevice = eSection;
				eNextDevice = eMaster;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pTower->getCodedFromLabel(TOWER_UP_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to get coded position from tower up label [" TOWER_UP_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_INFO, "OpReadSec::executeStripRead: move tray to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(m_sectionId)->m_pTower->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to move to position [%d]", ubPosCode);
					break;
				}
                // home procedure without considering loss of steps (user may have moved the device)
                sectionBoardLinkSingleton(m_sectionId)->m_pTray->m_CommonMessage.searchHome(1);
                sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.moveToAbsolutePosition(10, TH_MOVE_CMD_TIMEOUT);
                sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.searchHome(1);
                eNextDevice = eSection;
				m_eCurrState = eState::eStart;
			}
			break;

			case eState::eStart:
				msleep(50);
				if ( target == eTarget::eSample0 )
				{
					m_vReadSec[m_nCurrSlotId].W0.bIsEnabled = true;
					m_vReadSec[m_nCurrSlotId].W0.liWell = 0;
				}
				else
				{
					m_vReadSec[m_nCurrSlotId].W3.bIsEnabled = true;
					m_vReadSec[m_nCurrSlotId].W3.liWell = 3;
				}
				eCurrDevice = eSection;
				eNextDevice = eCamera;
				m_eCurrState = eState::eSendBatch;
			break;

			case eState::eSendBatch:
			{
				eCurrDevice = eCamera;
				eNextDevice = eSection;
				uint8_t ucSlot = m_nCurrSlotId+1; // slot1 has to be 1 here, not 0
				ucSlot = ( m_sectionId == SCT_A_ID ) ? ucSlot : ucSlot + SCT_NUM_TOT_SLOTS;
				eBatchTarget eBatch = ( target == eTarget::eSample0 ) ? eBatchTarget::eSampleX0 : eBatchTarget::eSampleX3;
				int liRes = usbBoardLinkSingleton()->m_pCR8062->uploadBatchFile(ucSlot, eBatch);
				if ( liRes )	recoverCamera();
				else			m_eCurrState = eState::eMoveToTraySample;
			}
			break;

			case eState::eMoveToTraySample:
			{
				eCurrDevice = eSection;
				eNextDevice = eMaster;
				string strCodedCmd = ( target == eTarget::eSample0 ) ? "W0" : "W3";
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pTray->getCodedFromLabel(strCodedCmd);
				if ( strCoded.empty() )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSampleRead: unable to get coded position from tray sample "
											"label [%s]", strCodedCmd.c_str());
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_INFO, "OpReadSec::executeSampleRead: move tray to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(m_sectionId)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSampleRead: unable to move to coded position [%d]", ubPosCode);
					break;
				}
				eNextDevice = eSection;
				m_eCurrState = eState::eMoveToSprRead;
			}
			break;

			case eState::eMoveToSprRead:
			{
				eCurrDevice = eSection;
				eNextDevice = eMaster;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pSPR->getCodedFromLabel(SPR_READ_DM_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSprRead: unable to get coded position from data matrix read label [" SPR_READ_DM_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_DEBUG, "OpReadSec::executeSprRead: move spr to pos predefined: CMD= %X", ubPosCode);
				int liRes = sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeSprRead: unable to move to position [%d]", ubPosCode);
					break;
				}
				eNextDevice = eNsh;
				m_eCurrState = eState::eMoveNshToSample;
			}
			break;

			case eState::eMoveNshToSample:
			{
				eCurrDevice = eNsh;
				string strCodedName = ( target == eTarget::eSample0 ) ? "CameraSample0_" : "CameraSample3_";
				string strPos("");
				int liSlotId = m_nCurrSlotId + 1; // slot1 has to be 1 here, not 0
				liSlotId = ( m_sectionId == SCT_A_ID ) ? liSlotId : liSlotId + SCT_NUM_TOT_SLOTS;
				strCodedName += to_string(liSlotId);
				nshBoardManagerSingleton()->m_pConfig->getItemValue(strCodedName, strPos);

				int liPos = stoi(strPos);
				int liRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
				if ( ! liRes )
				{
					eNextDevice = eMaster;
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to move NSH motor to desired position");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpReadSec::executeStripRead: moved NSH to sample position");
				eNextDevice = eCamera;
				m_eCurrState = eState::eGetPicture;
			}
			break;

			case eState::eGetPicture:
			{
				eCurrDevice = eCamera;
				eNextDevice = eCamera;
				if ( usbCameraSingleton()->getImageFormat() != eRAW )
				{
					int res = usbCameraSingleton()->setImageMode(eRAW);
					if ( res != ERR_NONE )
					{
						eNextDevice = eMaster;
						m_eCurrState = eState::eInstrRestore;
						break;
					}
				}

				vector<unsigned char>* pImage;
				pImage = ( target == eTarget::eSample0 ) ? &m_vReadSec[m_nCurrSlotId].W0.vImage : &m_vReadSec[m_nCurrSlotId].W3.vImage;
				int liRes = usbCameraSingleton()->getPicture(*pImage, &m_InfoImage);
				if ( liRes != ERR_NONE )
				{
					eNextDevice = eMaster;
					m_eCurrState = eState::eInstrRestore;
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to get picture");
					recoverCamera();
					// Adding this fake image the user understands something went wrong
					addFakeImage(*pImage);
					break;
				}
				m_eCurrState = eState::eSampleAlgorithm;
			}
			break;

			case eState::eSampleAlgorithm:
			{
				eCurrDevice = eCamera;				
				vector<unsigned char>* pImage;
				pImage = ( target == eTarget::eSample0 ) ? &m_vReadSec[m_nCurrSlotId].W0.vImage : &m_vReadSec[m_nCurrSlotId].W3.vImage;
				vector<uint32_t> vImage(pImage->begin(), pImage->end());
				usbBoardLinkSingleton()->m_pCR8062->ruotate180degree<uint32_t>(vImage.data(), m_InfoImage.liWidth, m_InfoImage.liHeight);
				for ( auto it = vImage.begin(); it != vImage.end(); ++it )
				{
					(*it) *= DECIMAL_DIGITS_FACTOR;
				}

				SampleDetectionLink detector;
				int liSlotId = m_nCurrSlotId + 1; // slot1 has to be 1 here, not 0
				liSlotId = ( m_sectionId == SCT_A_ID ) ? liSlotId : liSlotId + SCT_NUM_TOT_SLOTS;
				bool bRes = detector.initAlgo(m_pLogger, liSlotId, (target == eTarget::eSample0)?0:3);
				if ( ! bRes )
				{
					eNextDevice = eMaster;
					m_eCurrState = eState::eInstrRestore;
					usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&m_InfoImage, *pImage);
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to init sample detection algorithm");
					break;
				}

				float fResult;
				bRes = detector.sampleDetectionAlgorithm(vImage.data(), m_InfoImage.liWidth, fResult);
				if ( ! bRes )
				{
					eNextDevice = eMaster;
					m_eCurrState = eState::eInstrRestore;
					usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&m_InfoImage, *pImage);
					m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to apply sample detection algorithm");
					break;
				}
				if ( fResult < 0.0f )	fResult = 0.0f; // it has to be non negative
				float fThreshold;
				int liSettingsId = ( target == eTarget::eSample0 ) ? m_stripReadSection->m_SampleDetection.m_nSettingsX0 :
																	 m_stripReadSection->m_SampleDetection.m_nSettingsX3;
				fThreshold = m_cSettings.getDetectionThreshold(liSettingsId);
				// Check if the threshold has been specified, otherwise get the default
				if ( fThreshold < 0 )	fThreshold = 70.0f;

				if ( target == eTarget::eSample0 )
				{
					m_vReadSec[m_nCurrSlotId].W0.liScore = fResult;
					m_vReadSec[m_nCurrSlotId].W0.bSampleIsPresent = ( fResult < fThreshold );
				}
				else
				{
					m_vReadSec[m_nCurrSlotId].W3.liScore = fResult;
					m_vReadSec[m_nCurrSlotId].W3.bSampleIsPresent = ( fResult < fThreshold );
				}

				// add only now so that the algorithm is applied on a clean image
				usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&m_InfoImage, *pImage);
				m_eCurrState = eState::eEnd;
			}
			break;

			case eState::eEnd:
				// Clear image if not needed
				if ( target == eTarget::eSample0 && ! m_stripReadSection[m_nCurrSlotId].m_SampleDetection.m_bImageX0 )
				{
					m_vReadSec[m_nCurrSlotId].W0.vImage.clear();
				}
				else if ( target == eTarget::eSample3 && ! m_stripReadSection[m_nCurrSlotId].m_SampleDetection.m_bImageX3 )
				{
					m_vReadSec[m_nCurrSlotId].W3.vImage.clear();
				}

				m_vTargets[m_nCurrSlotId].erase(m_vTargets[m_nCurrSlotId].begin());
				++m_nCurrSlotId; // change slot
				bLoop = false;
				m_eCurrState = eState::eStart;
			break;

			default:
				bLoop = false;
				m_eCurrState = eState::eInstrRestore;
			break;
		}
	}
	return 0;
}

int OpReadSec::executeRestore()
{
	switch ( m_eCurrState )
	{
		case eState::eInstrRestore:
			msleep(50);
			m_eCurrState = eState::eNshRestore;
		break;

		case eState::eNshRestore:
		{
			string strPos("");
			string strCodedName("SS");
			nshBoardManagerSingleton()->m_pConfig->getItemValue(strCodedName, strPos);
			int liPos = stoi(strPos);
			bool bRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
			if ( ! bRes )
			{
				m_pLogger->log(LOG_ERR, "OpSHCalibrate::executeCmd: unable to move NSH motor to desired position");
				break;
			}
			m_eCurrState = eState::eSectRestore;
		}
		break;

		case eState::eSectRestore:
		{
			m_eCurrState = eState::eStop;
			string strCoded("");
			strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pTray->getCodedFromLabel(TRAY_OUT_CODED_COMMAND);
			if ( strCoded.empty() )	break;

			char ubPosCode = *strCoded.c_str();
			sectionBoardLinkSingleton(m_sectionId)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);

			strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pSPR->getCodedFromLabel(SPR_LOAD_DM_CODED_COMMAND);
			if ( strCoded.empty() )	break;

			ubPosCode = *strCoded.c_str();
			sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
		}
		break;

		default:
			m_eCurrState = eState::eStop;
		break;
	}
	return 0;
}

int OpReadSec::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdReadSec cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	ReadSecPayload * pReadSecPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pReadSecPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSSCalibrate::execute--> unable to serialize OutSSCalibrate");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}
	return eOperationEnabledForDeath;
}

int OpReadSec::getSection()
{
	return m_sectionId;
}

stripReadSection_tag * OpReadSec::getDataStructure()
{
	return &m_stripReadSection[0];
}

void OpReadSec::createActionList()
{
	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( ! m_stripReadSection[i].m_bEnabled )					continue;

		if (  m_stripReadSection[i].m_bStripCheck )					m_vTargets[i].push_back(eTarget::eStrip);
		if (  m_stripReadSection[i].m_bSprCheck )					m_vTargets[i].push_back(eTarget::eSpr);
		if (  m_stripReadSection[i].m_bSubstrateCheck )				m_vTargets[i].push_back(eTarget::eSubstrate);
		if (  m_stripReadSection[i].m_SampleDetection.m_bCheckX0 )	m_vTargets[i].push_back(eTarget::eSample0);
		if (  m_stripReadSection[i].m_SampleDetection.m_bCheckX3 )	m_vTargets[i].push_back(eTarget::eSample3);

		// init m_eCurrTarget only the first time
		if ( m_eCurrTarget == eTarget::eNone && ! m_vTargets[i].empty() )
		{
			m_eCurrTarget = m_vTargets[i].front();
		}
	}
	return;
}

void OpReadSec::recoverCamera()
{
	msleep(4000);
	bool bRes = usbBoardLinkSingleton()->m_pCR8062->bringToLife();
	if ( bRes )
	{
		m_eCurrState = eState::eEnd;
		m_pLogger->log(LOG_WARNING, "OpReadSec::recoverCamera: recover action performed");
	}
	else
	{
		m_eCurrState = eState::eInstrRestore;
		m_pLogger->log(LOG_WARNING, "OpReadSec::recoverCamera: unable to recover camera");
	}
	return;
}

void OpReadSec::addFakeImage(vector<unsigned char>& vImage)
{
	vImage.clear();
	vImage.resize(DEF_FAKE_IMG_SIZE);

	for ( int i = 0; i != DEF_FAKE_IMG_SIZE; i+=2 )
	{
		vImage.at(i) = 0;
		vImage.at(i+1) = 255;
	}

	structInfoImage info;
	info.liSize = DEF_FAKE_IMG_SIZE;
	info.eFormat = eRAW;
	info.liWidth = DEF_FAKE_IMG_SIDE;
	info.liHeight = DEF_FAKE_IMG_SIDE;
	usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&info, vImage);

	return;
}

ReadSecPayload* OpReadSec::buildPayload()
{
	ReadSecPayload * pReadSecPayload = new ReadSecPayload();

	string cSection = ( m_sectionId == SCT_A_ID ) ? "A" : "B";
	pReadSecPayload->setParams(cSection, &m_vReadSec);

	return pReadSecPayload;
}
