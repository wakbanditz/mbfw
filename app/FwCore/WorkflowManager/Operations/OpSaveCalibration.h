/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSaveCalibration.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSaveCalibration class.
 @details

 ****************************************************************************
*/

#ifndef OPSAVECALIBRATION_H
#define OPSAVECALIBRATION_H

#include "Mutex.h"

#include "SectionInclude.h"

#include "Operation.h"

class SaveCalibrationPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SAVECALIBRATION command
 * ********************************************************************************************************************
 */
class OpSaveCalibration : public Operation
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OpGetCalibration default constructor
		 * ***********************************************************************************************************
		 */
		OpSaveCalibration();

		/*! ***********************************************************************************************************
		 * @brief ~OpGetCalibration default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpSaveCalibration();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ************************************************************************************************************
		 * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
		 * then it composes the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 */
		int compileSendOutCmd( void );

		/*! ************************************************************************************************************
		 * @brief setValueCalibration sets the vector of structures for saving of the calibration arrived from the sw
		 * @param stCal
		 * @return 0
		 * *************************************************************************************************************
		 */
		int setValueCalibration(vector<structSaveCalibration> &stCal);

	private:
		/*! ************************************************************************************************************
         * @brief buildPayload	Fills the SaveCalibrationPayload of the outgoing command with related values.
         * @return  pointer to the SaveCalibrationPayload object just allocated
		 * *************************************************************************************************************
		 */
		SaveCalibrationPayload * buildPayload(void);

		/*! ************************************************************************************************************
		 * @brief executeCalibration
		 * @param	lNumSection
		 * @param	strMotor
		 * @param	strLabel
		 * @param	strRetVal
		 * @return  0 if success | -1 otherwise
		 * *************************************************************************************************************
		 */
		int	executeCalibration(int lNumSection);

		/*! ************************************************************************************************************
		 * @brief  executeNSHCalibration
		 * @return 0 if success | -1 otherwise
		 * *************************************************************************************************************
		 */
		int executeNSHCalibration(void);

    private:

        vector<structSaveCalibration>	m_vecSaveCalibration;
        uint8_t	m_counterAction;

};

#endif // OPSAVECALIBRATION_H
