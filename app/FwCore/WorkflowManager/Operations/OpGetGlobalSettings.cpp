/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetGlobalSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetGlobalSettings class.
 @details

 ****************************************************************************
*/

#include "OpGetGlobalSettings.h"
#include "OutCmdGetGlobalSettings.h"
#include "GetGlobalSettingsPayload.h"

#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpGetGlobalSettings::OpGetGlobalSettings() : Operation("OpGetGlobalSettings")
{

}

OpGetGlobalSettings::~OpGetGlobalSettings()
{

}

int OpGetGlobalSettings::perform(void)
{
	// no action is required -> read data from memory and send them back to SW

	OutCmdGetGlobalSettings outCmdGetSettings;
	string strOutCommand;

	outCmdGetSettings.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdGetSettings.setUsage(getUsage());
	outCmdGetSettings.setID(getID());

	// build the payload of the asynchrounous reply
	GetGlobalSettingsPayload * pGetSettingsPayload = new GetGlobalSettingsPayload();

    pGetSettingsPayload->setForce(infoSingleton()->getForceTemperature());

    pGetSettingsPayload->setInternalMaxTemperature(infoSingleton()->getInternalMaxTemperature());
	pGetSettingsPayload->setInternalMinTemperature(infoSingleton()->getInternalMinTemperature());

	for(uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		pGetSettingsPayload->setSprMaxTemperature(section, infoSingleton()->getSprMaxTemperature(section));
		pGetSettingsPayload->setSprMinTemperature(section, infoSingleton()->getSprMinTemperature(section));
		pGetSettingsPayload->setSprTargetTemperature(section, infoSingleton()->getSprTargetTemperature(section));
		pGetSettingsPayload->setSprToleranceTemperature(section, infoSingleton()->getSprToleranceTemperature(section));

		pGetSettingsPayload->setTrayMaxTemperature(section, infoSingleton()->getTrayMaxTemperature(section));
		pGetSettingsPayload->setTrayMinTemperature(section, infoSingleton()->getTrayMinTemperature(section));
		pGetSettingsPayload->setTrayTargetTemperature(section, infoSingleton()->getTrayTargetTemperature(section));
		pGetSettingsPayload->setTrayToleranceTemperature(section, infoSingleton()->getTrayToleranceTemperature(section));
	}

	// and assign it to the outcommand so that the serialize will extract the info
	outCmdGetSettings.setPayload((Payload *)pGetSettingsPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	protocolSerialize(&outCmdGetSettings, strOutCommand);

	sendCommandReply(strOutCommand);

	return eOperationWaitForDeath;
}


