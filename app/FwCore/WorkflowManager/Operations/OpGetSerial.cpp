/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetSerial.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetSerial class.
 @details

 ****************************************************************************
*/

#include "OpGetSerial.h"
#include "OutCmdGetSerial.h"
#include "GetSerialPayload.h"

#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpGetSerial::OpGetSerial() : Operation("OpGetSerial")
{

}

OpGetSerial::~OpGetSerial()
{

}

int OpGetSerial::perform(void)
{
	// no action is required -> read data from memory and send them back to SW

	OutCmdGetSerial outCmdGetSerial;
	string strOutCommand;

	outCmdGetSerial.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdGetSerial.setUsage(getUsage());
	outCmdGetSerial.setID(getID());

	// build the payload of the asynchrounous reply
	GetSerialPayload * pGetSerialPayload = new GetSerialPayload();
	pGetSerialPayload->setSerialInstrument(infoSingleton()->getInstrumentSerialNumber());
	pGetSerialPayload->setSerialMasterBoard(infoSingleton()->getMasterSerialNumber());
	pGetSerialPayload->setSerialNSH(infoSingleton()->getNshSerialNumber());
	pGetSerialPayload->setSerialSectionA(infoSingleton()->getSectionASerialNumber());
	pGetSerialPayload->setSerialSectionB(infoSingleton()->getSectionBSerialNumber());
	pGetSerialPayload->setSerialCamera(infoSingleton()->getCameraSerialNumber());

	// and assign it to the outcommand so that the serialize will extract the info
	outCmdGetSerial.setPayload((Payload *)pGetSerialPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	protocolSerialize(&outCmdGetSerial, strOutCommand);

	sendCommandReply(strOutCommand);

	return eOperationWaitForDeath;
}

