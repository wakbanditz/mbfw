/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetFanSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSetFanSettings class.
 @details

 ****************************************************************************
*/

#include "OpSetFanSettings.h"
#include "OutCmdSetFanSettings.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

#include "CommonInclude.h"
#include "ModuleInclude.h"


OpSetFanSettings::OpSetFanSettings() : Operation("OpSetFanSettings")
{
	m_FanSettings.clear();
	m_strDuration.clear();
}

OpSetFanSettings::~OpSetFanSettings()
{
	/* Nothing to do yet */
	m_FanSettings.clear();
}

int OpSetFanSettings::perform()
{
	uint16_t duration = (uint16_t)atoi(m_strDuration.c_str());

	if(duration > 0)
	{
		// set also the fan power
		eModuleFanId fanId;

		for(uint8_t index = 0; index < m_FanSettings.size() ;index++)
		{
			if(m_FanSettings.at(index).first.empty() == true)
				continue;

			if(m_FanSettings.at(index).first.compare(XML_TAG_SECTIONA_NAME) == 0)
			{
				fanId = eFanSectionA;
			}
			else if(m_FanSettings.at(index).first.compare(XML_TAG_SECTIONB_NAME) == 0)
			{
				fanId = eFanSectionB;
			}
			else if(m_FanSettings.at(index).first.compare(XML_TAG_INSTRUMENT_NAME) == 0)
			{
				fanId = eFanSupply;
			}
			else
			{
				continue;
			}

			uint8_t power = (uint8_t)atoi(m_FanSettings.at(index).second.c_str());
			masterBoardManagerSingleton()->setFanSettings(fanId, power);
		}
	}

	masterBoardManagerSingleton()->setFanDuration(duration);

    closeOperation();

    return eOperationRunning;
}

int OpSetFanSettings::compileSendOutCmd()
{
    // set the status as SUCCESFULL so that the reply is composed with all the parameters
    OutCmdSetFanSettings cmd;
    cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
    cmd.setUsage(getUsage());
    cmd.setID(getID());

    /* ********************************************************************************************
     * Send the OutgoingCommand to the client
     * ********************************************************l***********************************
     */
    string strOutCommand;
    bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
    if ( ! bSerializeResult )
    {
        m_pLogger->log(LOG_ERR, "OpSetFanSettings::execute--> unable to serialize");
    }
    else
    {
        sendCommandReply(strOutCommand);
    }

    return eOperationEnabledForDeath;
}

void OpSetFanSettings::setFan(string strComponent, string strPower)
{
	m_FanSettings.emplace_back(strComponent, strPower);
}

void OpSetFanSettings::setDuration(string strDuration)
{
	m_strDuration.assign(strDuration);
}

