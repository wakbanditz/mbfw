/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpRunwl.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpRunwl class.
 @details

 ****************************************************************************
*/

#ifndef OPRUNWL_H
#define OPRUNWL_H


#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the RUN command
 * ********************************************************************************************************************
 */

class OpRunwl : public Operation
{

	public:

		/*! ***********************************************************************************************************
		 * @brief OpRunwl default constructor
		 * ***********************************************************************************************************
		 */
		OpRunwl();

		/*! ***********************************************************************************************************
		 * @brief ~OpRunwl default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpRunwl();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

	private:

};

#endif // OPRUNWL_H
