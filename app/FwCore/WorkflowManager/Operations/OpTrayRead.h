/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpTrayRead.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpTrayRead class.
 @details

 ****************************************************************************
*/

#ifndef OPTRAYREAD_H
#define OPTRAYREAD_H

#include "Operation.h"
#include "InCmdTrayRead.h"
#include "Payload.h"

#define READING_PER_POSITION	3 // left, center and right
#define READING_POSITION_NUM	(SCT_NUM_TOT_SLOTS * READING_PER_POSITION)

class TrayReadPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the TRAYREAD command
 * ********************************************************************************************************************
 */

class OpTrayRead : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpTrayRead default constructor
		 * ***********************************************************************************************************
		 */
		OpTrayRead();

		/*! ***********************************************************************************************************
		 * @brief ~OpTrayRead default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpTrayRead();

		/*! ***********************************************************************************************************
		 * @brief setSection	called by the workflow to transfer the parameters needed by the Camera to perform any
		 *		  actions
         * @param cSecNum section index
		 * ************************************************************************************************************
		 */
		void setSection(int8_t cSecNum);

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
         * @param lNum the device index
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

	private:

		/*! ************************************************************************************************************
		 * @brief buildPayload	Fills the TrayReadPayload of the outgoing command with related values.
		 * @return  pointer to the TrayReadPayload object just allocated
		 * *************************************************************************************************************
		 */
		TrayReadPayload* buildPayload(void);

		void fillPositionsVector(int liSectionNum);

    private:

        enum class eState { eStart, eMoveToTrayRead, eExecute, eMoveToTrayOut, eClean, eNshError, eSectError, eStop };

        volatile eState	m_eReadState;
        int8_t			m_cSectionNum;
        vector<string>	m_vPositions;
        vector<int>		m_vRFUvalues;

};

#endif // OPTRAYREAD_H
