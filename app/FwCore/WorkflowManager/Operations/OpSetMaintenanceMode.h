/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetMaintenanceMode.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSetMaintenanceMode class.
 @details

 ****************************************************************************
*/

#ifndef OPSETMAINTENANCEMODE_H
#define OPSETMAINTENANCEMODE_H


#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SETMAINTENANCEMODE command
 * ********************************************************************************************************************
 */
class OpSetMaintenanceMode : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpSetMaintenanceMode default constructor
		 * ***********************************************************************************************************
		 */
		OpSetMaintenanceMode();

		/*! ***********************************************************************************************************
		 * @brief ~OpSetMaintenanceMode default destructor
		 * ************************************************************************************************************
		 */
		~OpSetMaintenanceMode();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief setActivation sets the Activation string
		 * @param strActivation the Activation string
		 * ************************************************************************************************************
		 */
		void setActivation(const string& strActivation);

	private :

		std::string m_strActivation;
};



#endif // OPSETMAINTENANCEMODE_H
