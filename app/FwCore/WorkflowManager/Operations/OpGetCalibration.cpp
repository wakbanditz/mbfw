/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetCalibration.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetCalibration class.
 @details

 ****************************************************************************
*/

#include "OpGetCalibration.h"
#include "OutCmdGetCalibration.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "SectionBoardManager.h"
#include "GetCalibrationPayload.h"
#include "CommonInclude.h"


/*! *******************************************************************************************************************
 * Protocol command name element values
 * ********************************************************************************************************************
 */

OpGetCalibration::OpGetCalibration(const string &strComponent,
								   const string &strMotor,
								   const string &strLabel)
									: Operation("GetCalibration")
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strLabel.clear();
	m_strCalibration.clear();

	m_counterAction = 0;
	m_bAskBoards = false;

	// build the vectors of actions according to the Component / Motor coming from the command
	std::vector<std::string> listComponent, listMotorSection, listMotorNsh, listMotorLabel, listMotorCalibration;

	listMotorCalibration.clear();
	listMotorSection.clear();
	listMotorNsh.clear();
	listComponent.clear();

	if(strComponent.empty())
	{
		// in this case the request is complete on all calibrations of all components
		// if a device is disabled we exclude it from the action
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
		{
			listComponent.push_back(XML_TAG_SECTIONA_NAME);
		}
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
		{
			listComponent.push_back(XML_TAG_SECTIONB_NAME);
		}
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true)
		{
			listComponent.push_back(XML_TAG_NSH_NAME);
		}
	}
	else
	{
		listComponent.push_back(strComponent);
	}

	if( strMotor.empty() || strComponent.empty())
	{
		// all devices (if the Motor string OR the Component string are empty)
		listMotorSection.push_back(SECTION_PUMP_STRING);
		listMotorSection.push_back(SECTION_TOWER_STRING);
		listMotorSection.push_back(SECTION_TRAY_STRING);
		listMotorSection.push_back(SECTION_SPR_STRING);
		listMotorNsh.push_back(DEVICE_NSH_STRING);
	}
	else if( (strMotor.compare(SECTION_PUMP_STRING) == 0) ||
		(strMotor.compare(SECTION_TOWER_STRING) == 0) ||
		(strMotor.compare(SECTION_TRAY_STRING) == 0) ||
		(strMotor.compare(SECTION_SPR_STRING) == 0)   )
	{
		// section devices
		listMotorSection.push_back(strMotor);
	}
	else if( strMotor.compare(DEVICE_NSH_STRING) == 0)
	{
		// nsh device
		listMotorNsh.push_back(strMotor);
	}


	for(uint8_t counter = 0; counter < listComponent.size(); counter++)
	{
		if(listComponent.at(counter).compare(XML_TAG_SECTIONA_NAME) == 0 ||
				listComponent.at(counter).compare(XML_TAG_SECTIONB_NAME) == 0)
		{
			for(uint8_t i = 0; i < listMotorSection.size(); i++)
			{
				listMotorLabel.clear();
				if(listMotorSection.at(i).compare(SECTION_PUMP_STRING) == 0)
				{
					// the request is per all calibrations or per a single one ?
					if(strLabel.empty())
					{
						// note: the section is not important in this case as the calibration are the same for both
						sectionBoardLinkSingleton(SCT_A_ID)->m_pPump->getCalibrationLabelList(&listMotorLabel);
					}
					else
					{
						if(sectionBoardLinkSingleton(SCT_A_ID)->m_pPump->isCalibrationLabelValid(strLabel))
						{
							listMotorLabel.push_back(strLabel);
						}
					}
				}
				else if(listMotorSection.at(i).compare(SECTION_TOWER_STRING) == 0)
				{
					// the request is per all calibrations or per a single one ?
					if(strLabel.empty())
					{
						// note: the section is not important in this case as the calibration are the same for both
						sectionBoardLinkSingleton(SCT_A_ID)->m_pTower->getCalibrationLabelList(&listMotorLabel);
					}
					else
					{
						if(sectionBoardLinkSingleton(SCT_A_ID)->m_pTower->isCalibrationLabelValid(strLabel))
						{
							listMotorLabel.push_back(strLabel);
						}
					}
				}
				else if(listMotorSection.at(i).compare(SECTION_TRAY_STRING) == 0)
				{
					// the request is per all calibrations or per a single one ?
					if(strLabel.empty())
					{
						// note: the section is not important in this case as the calibration are the same for both
						sectionBoardLinkSingleton(SCT_A_ID)->m_pTray->getCalibrationLabelList(&listMotorLabel);
					}
					else
					{
						if(sectionBoardLinkSingleton(SCT_A_ID)->m_pTray->isCalibrationLabelValid(strLabel))
						{
							listMotorLabel.push_back(strLabel);
						}
					}
				}
				else if(listMotorSection.at(i).compare(SECTION_SPR_STRING) == 0)
				{
					// the request is per all calibrations or per a single one ?
					if(strLabel.empty())
					{
						// note: the section is not important in this case as the calibration are the same for both
						sectionBoardLinkSingleton(SCT_A_ID)->m_pSPR->getCalibrationLabelList(&listMotorLabel);
					}
					else
					{
						if(sectionBoardLinkSingleton(SCT_A_ID)->m_pSPR->isCalibrationLabelValid(strLabel))
						{
							listMotorLabel.push_back(strLabel);
						}
					}
				}
				for(uint8_t k = 0; k < listMotorLabel.size(); k++)
				{
					m_strComponent.push_back(listComponent.at(counter));
					m_strMotor.push_back(listMotorSection.at(i));
					m_strLabel.push_back(listMotorLabel.at(k));
					m_strCalibration.push_back("");
				}
			}
		}
		else if(listComponent.at(counter).compare(XML_TAG_NSH_NAME) == 0)
		{
			// Only motor admitted is NSH

			for ( uint8_t i = 0; i < listMotorNsh.size(); i++ )
			{
				listMotorLabel.clear();
				// the request is per all calibrations or per a single one ?
				if ( strLabel.empty() )
				{
					// note: the section is not important in this case as the calibration are the same for both
					MainExecutor::getInstance()->m_NSHBoardManager.m_pConfig->getLabelList(listMotorLabel);
				}
				else
				{
					if ( MainExecutor::getInstance()->m_NSHBoardManager.m_pConfig->isLabelValid(strLabel) )
					{
						listMotorLabel.push_back(strLabel);
					}
				}
				for ( uint8_t k = 0; k < listMotorLabel.size(); k++ )
				{
					m_strComponent.push_back(listComponent.at(counter));
					m_strMotor.push_back(listMotorNsh.at(i));
					m_strLabel.push_back(listMotorLabel.at(k));
					m_strCalibration.push_back("");
				}
			}
		}
	}
}

OpGetCalibration::~OpGetCalibration()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strLabel.clear();
	m_strCalibration.clear();
}

int OpGetCalibration::perform()
{
	int8_t res = -1;


	if(m_bAskBoards == false)
	{
		// read data from memory
		for(m_counterAction = 0; m_counterAction < m_strComponent.size(); m_counterAction++)
		{

#ifdef __DEBUG_PROTOCOL
			m_strCalibration.at(m_counterAction).assign(to_string(m_counterAction));
			continue;
#endif

			uint8_t section = 0xFF;

			if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONA_NAME) == 0)
			{
				section = SCT_A_ID;
			}
			else if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONB_NAME) == 0)
			{
				section = SCT_B_ID;
			}
			else if(m_strComponent.at(m_counterAction).compare(XML_TAG_NSH_NAME) == 0)
			{
				section = NSH_ID;
			}

			std::string strCalibration;
			strCalibration.clear();

			if(section == SCT_A_ID || section == SCT_B_ID)
			{
				SectionCommonMessage*	pCommonMessage = NULL;

				//choice of the component to move
				if (m_strMotor.at(m_counterAction).compare(SECTION_TOWER_STRING) == 0)
				{
					pCommonMessage = &sectionBoardLinkSingleton(section)->m_pTower->m_CommonMessage;
					strCalibration = sectionBoardLinkSingleton(section)->m_pTower->getCalibrationFromLabel(m_strLabel.at(m_counterAction));
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_PUMP_STRING) == 0)
				{
					pCommonMessage = &sectionBoardLinkSingleton(section)->m_pPump->m_CommonMessage;
					strCalibration = sectionBoardLinkSingleton(section)->m_pPump->getCalibrationFromLabel(m_strLabel.at(m_counterAction));
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_TRAY_STRING) == 0)
				{
					pCommonMessage = &sectionBoardLinkSingleton(section)->m_pTray->m_CommonMessage;
					strCalibration = sectionBoardLinkSingleton(section)->m_pTray->getCalibrationFromLabel(m_strLabel.at(m_counterAction));
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_SPR_STRING) == 0)
				{
					pCommonMessage = &sectionBoardLinkSingleton(section)->m_pSPR->m_CommonMessage;
					strCalibration = sectionBoardLinkSingleton(section)->m_pSPR->getCalibrationFromLabel(m_strLabel.at(m_counterAction));
				}

				if(strCalibration.empty() == false)
				{
					// convert the strCalibration to the integer value
					uint8_t index = (uint8_t)atoi(strCalibration.c_str());

					int32_t calibrationPos = pCommonMessage->readCalibrationPosition(index);

					strCalibration.assign(std::to_string(calibrationPos));
				}
			}
			else if(section == NSH_ID)
			{
				bool bRes;
				bRes = MainExecutor::getInstance()->m_NSHBoardManager.m_pConfig->getItemValue(m_strLabel.at(m_counterAction), strCalibration);

				if (bRes == false)
				{
					strCalibration.clear();
				}
			}
			m_strCalibration.at(m_counterAction).assign(strCalibration);
		}
		// send back the final reply
		compileSendOutCmd();
		// end of story !
		return eOperationWaitForDeath;
	}


	// start the action on the current item of the vector

	if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONA_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONB_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_strComponent.at(m_counterAction).compare(XML_TAG_NSH_NAME) == 0)
	{
		res = nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
	}

	if(res != eOperationRunning)
	{
		m_pLogger->log(LOG_WARNING, "OpGetCalibration::perform-->Error %s", m_strComponent.at(m_counterAction).c_str());
		// operation completed -> send reply and close it
		closeOperation();
	}

	return eOperationRunning;
}

GetCalibrationPayload * OpGetCalibration::buildPayload(void)
{
	GetCalibrationPayload * pGetCalibrationPayload = new GetCalibrationPayload();

	pGetCalibrationPayload->setOutValues(m_strComponent, m_strMotor, m_strLabel, m_strCalibration);

	return pGetCalibrationPayload;
}

int OpGetCalibration::executeGetCalibration(int ubNumSection)
{
	SectionCommonMessage*	pCommonMessage = NULL;
	std::string strCalibration;

	strCalibration.clear();

	//choice of the component to move
	if (m_strMotor.at(m_counterAction).compare(SECTION_TOWER_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTower->m_CommonMessage;
		strCalibration = sectionBoardLinkSingleton(ubNumSection)->m_pTower->getCalibrationFromLabel(m_strLabel.at(m_counterAction));
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_PUMP_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pPump->m_CommonMessage;
		strCalibration = sectionBoardLinkSingleton(ubNumSection)->m_pPump->getCalibrationFromLabel(m_strLabel.at(m_counterAction));
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_TRAY_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTray->m_CommonMessage;
		strCalibration = sectionBoardLinkSingleton(ubNumSection)->m_pTray->getCalibrationFromLabel(m_strLabel.at(m_counterAction));
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_SPR_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pSPR->m_CommonMessage;
		strCalibration = sectionBoardLinkSingleton(ubNumSection)->m_pSPR->getCalibrationFromLabel(m_strLabel.at(m_counterAction));
	}

	if(strCalibration.empty())
	{
		m_pLogger->log(LOG_WARNING, "OpGetCalibration: Coded not found", m_strLabel.at(m_counterAction).c_str());
		return -1;
	}

	char ubPosCode = *strCalibration.c_str();
	m_pLogger->log(LOG_INFO, "OpGetCalibration::execute CMD= %X", ubPosCode);

	int32_t slValue = 0;

	int ret = pCommonMessage->getCalibration(ubPosCode, slValue);

	m_strCalibration.at(m_counterAction).assign(to_string(slValue));
	return ret;
}

int OpGetCalibration::executeGetNshCalibration()
{
	string strCalibration;

	strCalibration.clear();

	//choice of the component to move
	if ( m_strMotor.at(m_counterAction).compare(DEVICE_NSH_STRING) == 0 )
	{
		if(MainExecutor::getInstance()->m_NSHBoardManager.m_pConfig->getItemValue(m_strLabel.at(m_counterAction), strCalibration) == false)
		{
			strCalibration.clear();
		}
	}

	if ( strCalibration.empty() )
	{
		m_pLogger->log(LOG_WARNING, "OpGetCalibration: Coded not found", m_strLabel.at(m_counterAction).c_str());
		return -1;
	}

	m_strCalibration.at(m_counterAction).assign(strCalibration);

	return 0;
}

int OpGetCalibration::executeCmd( int lNum )
{
	//action to perform
	int res = 0;
	if( lNum == NSH_ID )
	{
		res = executeGetNshCalibration();
	}
	else
	{
		res = executeGetCalibration(lNum);
	}

	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "OpGetCalibration::execute-->execute not performed");
		// operation completed -> send reply and close it
		closeOperation();
		return -1;
	}

	std::string previousComponent;
	previousComponent.assign(m_strComponent.at(m_counterAction));

	// increase the action counter
	m_counterAction++;

	// if we have performed the last action compose and send the reply, stop the loop
	if(m_counterAction >= m_strComponent.size() )
	{
		// operation completed -> send reply and close it
		closeOperation();
		res = -1;
	}
	else if(previousComponent.compare(m_strComponent.at(m_counterAction)) != 0)
	{
		// actions on the current device are completed:
		// --> go for next action
		perform();
		// -> unlock the actual
		res = -1;
	}

	m_pLogger->log(LOG_INFO, "OpGetMotorCalibration::execute-->completed");

	return res;
}

int OpGetCalibration::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdGetCalibration cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	GetCalibrationPayload * pGetCalibrationPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pGetCalibrationPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpGetCalibration::compileSendOutCmd--> unable to serialize OutCmdGetCalibration");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

