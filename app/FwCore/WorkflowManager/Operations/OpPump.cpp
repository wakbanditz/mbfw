/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpPump.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpPump class.
 @details

 ****************************************************************************
*/

#include "OpPump.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardManager.h"
#include "SectionBoardPump.h"
#include "OutCmdPump.h"
#include "CommonInclude.h"

/*! *******************************************************************************************************************
 * Class implementation
 * ********************************************************************************************************************
 */
OpPump::OpPump(const string &strComponent,
               const string &strAction,
               const string &strCommand)
                : Operation("OpPump")
{
    m_strComponent.assign(strComponent);
    m_strAction.assign(strAction);
    m_strCommand.assign(strCommand);
}

OpPump::~OpPump()
{
    /* Nothing to do yet */
}

int OpPump::perform()
{
    if ( m_strComponent.compare(XML_VALUE_SECTION_A) == 0 )
    {
        return sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_CMD, (Operation*)this);
    }
    else if ( m_strComponent.compare(XML_VALUE_SECTION_B) == 0 )
    {
        return sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_CMD, (Operation*)this);
    }

    m_pLogger->log(LOG_ERR, "OpPump::perform-->Component %s not in Pump list", m_strComponent.c_str());
    return eOperationWaitForDeath;
}

int OpPump::executeCmd(int lNum)
{

#ifdef __DEBUG_PROTOCOL
        return 0;
#endif

    int liRes = -1;

    switch ( lNum )
    {
        case SCT_A_ID:
        case SCT_B_ID:
        {
            char buffer[12];
            sprintf(buffer, "%s", m_strCommand.c_str());

            if ( m_strAction.compare(XML_TAG_PUMP_ACTION_ASPIRATE) == 0 )
            {
                liRes = sectionBoardLinkSingleton(lNum)->m_pPump->aspirateVolume(buffer[0], 'Q', 5000);
            }
            else if ( m_strAction.compare(XML_TAG_PUMP_ACTION_DISPENSE) == 0 )
            {
                liRes = sectionBoardLinkSingleton(lNum)->m_pPump->dispenseVolume(buffer[0], 'Q', 5000);
            }
        }
        break;
    }

    if ( liRes != 0 )
    {
        m_pLogger->log(LOG_ERR, "OpPump::execute-->execute not performed");
        return -1;
    }

    m_pLogger->log(LOG_INFO, "OpPump::execute-->PUMP executed");
    return 0;
}

int OpPump::compileSendOutCmd()
{
    // set the status as SUCCESFULL so that the reply is composed with all the parameters
    OutCmdPump cmd;
    cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
    cmd.setUsage(getUsage());
    cmd.setID(getID());

    // no payload for the reply

    /* ********************************************************************************************
     * Send the OutgoingCommand to the client
     * ********************************************************l***********************************
     */
    string strOutCommand;
    bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
    if ( ! bSerializeResult )
    {
        m_pLogger->log(LOG_ERR, "OpPump::execute--> unable to serialize OutCmdPump");
    }
    else
    {
        sendCommandReply(strOutCommand);
    }
    return eOperationEnabledForDeath;
}


