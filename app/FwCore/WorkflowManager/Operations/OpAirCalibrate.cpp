/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpAirCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpAirCalibrate class.
 @details

 ****************************************************************************
*/

#include "OpAirCalibrate.h"
#include "MainExecutor.h"
#include "WebServerAndProtocolInclude.h"
#include "OutCmdAirCalibrate.h"

OpAirCalibrate::OpAirCalibrate() : Operation("OpAirCalibrate")
{
	m_strTarget.clear();
}

OpAirCalibrate::~OpAirCalibrate()
{
	/* Nothing to do yet */
}

void OpAirCalibrate::setParameter(string strTarget)
{
	m_strTarget.assign(strTarget);
}

int OpAirCalibrate::perform()
{
	return nshBoardManagerSingleton()->setNSHAction(eNSHActionCmd, (Operation*)this);
}

int OpAirCalibrate::executeCmd(int lNum)
{
	int liRes = -lNum;

	uint8_t ucRFUval = stoi(m_strTarget);

	m_pLogger->log(LOG_DEBUG_PARANOIC, "OpAirCalibrate::executeCmd: rfu got is %d", ucRFUval);

	liRes = MainExecutor::getInstance()->m_SPIBoard.m_pNSHReader->m_NSH.calibrateAirFact(ucRFUval);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "OpAirCalibrate::executeCmd: unable to perform air calibration");
		return -1;
	}

	return liRes;
}


int OpAirCalibrate::compileSendOutCmd()
{
	OutCmdAirCalibrate cmd;
	string strOutCommand;

	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpAirCalibrate::execute--> unable to serialize OutCmdSetMotorActivation");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}
