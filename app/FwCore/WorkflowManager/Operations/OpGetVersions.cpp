/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetVersions.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetVersions class.
 @details

 ****************************************************************************
*/

#include "OpGetVersions.h"
#include "OutCmdGetVersions.h"
#include "GetVersionsPayload.h"

#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpGetVersions::OpGetVersions() : Operation("OpGetVersions")
{

}

OpGetVersions::~OpGetVersions()
{

}

int OpGetVersions::perform(void)
{
	// no action is required -> read data from memory and send them back to SW

	OutCmdGetVersions outCmdGetVersions;
	string strOutCommand;

	outCmdGetVersions.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdGetVersions.setUsage(getUsage());
	outCmdGetVersions.setID(getID());

	// build the payload of the asynchrounous reply
	GetVersionsPayload * pGetVersionsPayload = new GetVersionsPayload();

	pGetVersionsPayload->setVersionMasterApplication(infoSingleton()->getMasterApplicationRelease());
	pGetVersionsPayload->setVersionMasterBoot(infoSingleton()->getMasterBootRelease());
	pGetVersionsPayload->setVersionMasterKernel(infoSingleton()->getMasterKernelRelease());
	pGetVersionsPayload->setVersionMasterMiniKernel(infoSingleton()->getMasterMiniKernelRelease());

#ifdef __DEBUG_PROTOCOL
	pGetVersionsPayload->setVersionNshApplication("NSH 1.0.1");
	pGetVersionsPayload->setVersionNshBoot("NSHboot 1.0.1");
	pGetVersionsPayload->setVersionSectionAApplication("SA app 1.0.0");
	pGetVersionsPayload->setVersionSectionABoot("SA boot 1.0.0");
	pGetVersionsPayload->setVersionSectionAFpga("SA fpga 1.0.0");
	pGetVersionsPayload->setVersionSectionBApplication("SB app 1.0.0");
	pGetVersionsPayload->setVersionSectionBBoot("SB boot 1.0.0");
	pGetVersionsPayload->setVersionSectionBFpga("SB fpga 1.0.0");
	pGetVersionsPayload->setVersionCameraApplication("Camera 1.3.4");

#else
	pGetVersionsPayload->setVersionNshApplication(infoSingleton()->getNshApplicationRelease());
	pGetVersionsPayload->setVersionNshBoot(infoSingleton()->getNshBootRelease());
	pGetVersionsPayload->setVersionSectionAApplication(infoSingleton()->getSectionAApplicationRelease());
	pGetVersionsPayload->setVersionSectionABoot(infoSingleton()->getSectionABootRelease());
	pGetVersionsPayload->setVersionSectionAFpga(infoSingleton()->getSectionAFpgaRelease());
	pGetVersionsPayload->setVersionSectionBApplication(infoSingleton()->getSectionBApplicationRelease());
	pGetVersionsPayload->setVersionSectionBBoot(infoSingleton()->getSectionBBootRelease());
	pGetVersionsPayload->setVersionSectionBFpga(infoSingleton()->getSectionBFpgaRelease());
	pGetVersionsPayload->setVersionCameraApplication(infoSingleton()->getCameraApplicationRelease());
#endif

	// and assign it to the outcommand so that the serialize will extract the info
	outCmdGetVersions.setPayload((Payload *)pGetVersionsPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	protocolSerialize(&outCmdGetVersions, strOutCommand);

	sendCommandReply(strOutCommand);

	return eOperationWaitForDeath;
}

