/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpCancelSection.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpCancelSection class.
 @details

 ****************************************************************************
*/

#ifndef OPCANCELSECTION_H
#define OPCANCELSECTION_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the CANCELSECTION command
 * ********************************************************************************************************************
 */

class OpCancelSection : public Operation
{

	public:

		/*! ***********************************************************************************************************
		 * @brief OpCancelSection default constructor
		 * ***********************************************************************************************************
		 */
		OpCancelSection();

		/*! ***********************************************************************************************************
		 * @brief ~OpCancelSection default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpCancelSection();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

};

#endif // OPCANCELSECTION_H
