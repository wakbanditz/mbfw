/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpVidasEp.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpVidasEp class.
 @details

 ****************************************************************************
*/

#ifndef OPVIDASEP_H
#define OPVIDASEP_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the VIDASEP command
 * ********************************************************************************************************************
 */
class OpVidasEp : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpVidasEp default constructor
		 * ***********************************************************************************************************
		 */
		OpVidasEp();

		/*! ***********************************************************************************************************
		 * @brief ~OpVidasEp default destructor
		 * ************************************************************************************************************
		 */
		~OpVidasEp();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	build and send the message to gateway
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

};

#endif // OPVIDASEP_H
