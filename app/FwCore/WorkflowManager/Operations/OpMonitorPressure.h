/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpMonitorPressure.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpMonitorPressure class.
 @details

 ****************************************************************************
*/

#ifndef OPMONITORPRESSURE_H
#define OPMONITORPRESSURE_H

#include "InCmdMonitorPressure.h"
#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the MONITOR_PRESSURE command
 * ********************************************************************************************************************
 */

class OpMonitorPressure : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpMonitorPressure default constructor
		 * ***********************************************************************************************************
		 */
		OpMonitorPressure();

		/*! ***********************************************************************************************************
		 * @brief ~OpMonitorPressure default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpMonitorPressure();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief setParameters	called by the workflow to transfer the parameters for the MonitorePressure procedure
		 * @param section the section involved
		 * @param action the action to be performed
		 * ************************************************************************************************************
		 */
		void setParameters(int section, int action);

		/*! ***********************************************************************************************************
		 * @brief getSection get the section involved in the ReadSection procedure
		 * @return the index of the section if defined, -1 otherwise
		 * ************************************************************************************************************
		 */
		int getSection();

		/*! ***********************************************************************************************************
		 * @brief getAction get the action to be performed
		 * @return the action START /STOP
		 * ************************************************************************************************************
		 */
		int getAction();

	private:

		int m_section, m_action;

};

#endif // OPMONITORPRESSURE_H
