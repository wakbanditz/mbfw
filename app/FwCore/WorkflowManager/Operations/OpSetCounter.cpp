/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetCounter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSetCounter class.
 @details

 ****************************************************************************
*/

#include "OpSetCounter.h"
#include "OutCmdSetCounter.h"

#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpSetCounter::OpSetCounter() : Operation("OpSetCounter")
{
	m_strCounter.clear();
	m_intValue = 0;
}

OpSetCounter::~OpSetCounter()
{

}

int OpSetCounter::perform(void)
{
	// no action is required -> set data to memory and reply to SW
	uint8_t indexCounter = infoSingleton()->getInstrumentCounterIndexFromString(m_strCounter);
	infoSingleton()->setInstrumentCounter((eCounterId)indexCounter, m_intValue);

	OutCmdSetCounter outCmdSetCounter;
	string strOutCommand;

	outCmdSetCounter.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdSetCounter.setUsage(getUsage());
	outCmdSetCounter.setID(getID());

	// no payload of the asynchrounous reply

	protocolSerialize(&outCmdSetCounter, strOutCommand);

	sendCommandReply(strOutCommand);

	return eOperationWaitForDeath;
}

void OpSetCounter::setCounter(const string& strCounter)
{
	m_strCounter.assign(strCounter);
}

void OpSetCounter::setValue(uint16_t value)
{
	m_intValue = value;
}


