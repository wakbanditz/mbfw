/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetPosition.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetPosition class.
 @details

 ****************************************************************************
*/

#include "OpGetPosition.h"
#include "OutCmdGetPosition.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "SectionBoardManager.h"
#include "GetPositionPayload.h"
#include "CommonInclude.h"

OpGetPosition::OpGetPosition(const string &strComponent, const string &strMotor): Operation("GetPosition")
{
	m_strPosition.clear();
	m_strComponent.clear();
	m_strMotor.clear();

	m_counterAction = 0;
	m_bAskBoards = false;

	// build the vectors of actions according to the Component / Motor coming from the command
	std::vector<std::string> listMotorSection, listMotorNsh;

	if( (strMotor.compare(SECTION_PUMP_STRING) == 0) ||
		(strMotor.compare(SECTION_TOWER_STRING) == 0) ||
		(strMotor.compare(SECTION_TRAY_STRING) == 0) ||
		(strMotor.compare(SECTION_SPR_STRING) == 0)   )
	{
		// section devices
		listMotorSection.push_back(strMotor);
	}
	else if( strMotor.compare(DEVICE_NSH_STRING) == 0)
	{
		// nsh device
		listMotorNsh.push_back(strMotor);
	}
	else if( strMotor.empty())
	{
		// all devices
		listMotorSection.push_back(SECTION_PUMP_STRING);
		listMotorSection.push_back(SECTION_TOWER_STRING);
		listMotorSection.push_back(SECTION_TRAY_STRING);
		listMotorSection.push_back(SECTION_SPR_STRING);
		listMotorNsh.push_back(DEVICE_NSH_STRING);
	}

	if(strComponent.empty())
	{
		// in this case the request is on all components
		// if a device is disabled we exclude it from the action
		// build the vectors associating to the Component the corresponding motors
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
		{
			for(uint8_t i = 0; i < listMotorSection.size(); i++)
			{
				m_strComponent.push_back(XML_TAG_SECTIONA_NAME);
				m_strMotor.push_back(listMotorSection.at(i));
				m_strPosition.push_back("");
			}
		}
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
		{
			for(uint8_t i = 0; i < listMotorSection.size(); i++)
			{
				m_strComponent.push_back(XML_TAG_SECTIONB_NAME);
				m_strMotor.push_back(listMotorSection.at(i));
				m_strPosition.push_back("");
			}
		}
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true)
		{
			for(uint8_t i = 0; i < listMotorNsh.size(); i++)
			{
				m_strComponent.push_back(XML_TAG_NSH_NAME);
				m_strMotor.push_back(listMotorNsh.at(i));
				m_strPosition.push_back("");
			}
		}
	}
	else if (strComponent.compare(XML_TAG_SECTIONA_NAME) == 0)
	{
		for(uint8_t i = 0; i < listMotorSection.size(); i++)
		{
			m_strComponent.push_back(XML_TAG_SECTIONA_NAME);
			m_strMotor.push_back(listMotorSection.at(i));
			m_strPosition.push_back("");
		}
	}
	else if (strComponent.compare(XML_TAG_SECTIONB_NAME) == 0)
	{
		for(uint8_t i = 0; i < listMotorSection.size(); i++)
		{
			m_strComponent.push_back(XML_TAG_SECTIONB_NAME);
			m_strMotor.push_back(listMotorSection.at(i));
			m_strPosition.push_back("");
		}
	}
	else if (strComponent.compare(XML_TAG_NSH_NAME) == 0)
	{
		for(uint8_t i = 0; i < listMotorNsh.size(); i++)
		{
			m_strComponent.push_back(XML_TAG_NSH_NAME);
			m_strMotor.push_back(listMotorNsh.at(i));
			m_strPosition.push_back("");
		}
	}
	// TODO delete
	else if (strComponent.compare(XML_TAG_CAMERA_NAME) == 0)
	{
		m_strComponent.push_back(XML_TAG_CAMERA_NAME);
		m_strMotor.push_back(strMotor);
		m_strPosition.push_back("");
	}
}

OpGetPosition::~OpGetPosition()
{
	m_strPosition.clear();
	m_strComponent.clear();
	m_strMotor.clear();
}

int OpGetPosition::perform()
{
	int8_t res = -1;

	if(m_bAskBoards == false)
	{
		// read data from memory
		for(m_counterAction = 0; m_counterAction < m_strComponent.size(); m_counterAction++)
		{
#ifdef __DEBUG_PROTOCOL
			m_strPosition.at(m_counterAction).assign(to_string((m_counterAction + 1) * 2));
			continue;
#endif

			uint8_t section = 0xFF;

			if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONA_NAME) == 0)
			{
				section = SCT_A_ID;
			}
			else if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONB_NAME) == 0)
			{
				section = SCT_B_ID;
			}
			else if(m_strComponent.at(m_counterAction).compare(XML_TAG_NSH_NAME) == 0)
			{
				section = NSH_ID;
			}

			int32_t	position = 0;

			if(section == SCT_A_ID || section == SCT_B_ID)
			{
				if (m_strMotor.at(m_counterAction).compare(SECTION_TOWER_STRING) == 0)
				{
					position = sectionBoardLinkSingleton(section)->m_pTower->m_CommonMessage.readMotorPosition();
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_PUMP_STRING) == 0)
				{
					position = sectionBoardLinkSingleton(section)->m_pPump->m_CommonMessage.readMotorPosition();
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_TRAY_STRING) == 0)
				{
					position = sectionBoardLinkSingleton(section)->m_pTray->m_CommonMessage.readMotorPosition();
				}
				else if (m_strMotor.at(m_counterAction).compare(SECTION_SPR_STRING) == 0)
				{
					position = sectionBoardLinkSingleton(section)->m_pSPR->m_CommonMessage.readMotorPosition();
				}
			}
			else if(section == NSH_ID)
			{
				if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true)
				{
					position = MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->readMotorPosition();
					// motor resolution is 1/16 step. We need to convert it in half steps
					position /= (-MOT_HALF_TO_MICRO_STEPS);
				}
			}
			// TODO delete ssw bug camera, motor tower
			else if(section == CAMERA_ID)
			{
				msleep(100);
				position = 111;
			}
			m_strPosition.at(m_counterAction).assign(to_string(position));
		}
		// send back the final reply
		compileSendOutCmd();
		// end of story !
		return eOperationWaitForDeath;
	}


	// start the action on the current item of the vector

	if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONA_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_strComponent.at(m_counterAction).compare(XML_TAG_SECTIONB_NAME) == 0)
	{
		res = sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if(m_strComponent.at(m_counterAction).compare(XML_TAG_NSH_NAME) == 0)
	{
		res = nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
	}

	if(res != eOperationRunning)
	{
		m_pLogger->log(LOG_WARNING, "OpGetPosition::perform-->Error %s", m_strComponent.at(m_counterAction).c_str());
		// operation completed -> send reply and close it
		closeOperation();
	}

//	return res; // to be checked
	return eOperationRunning;
}

int OpGetPosition::executeSectionGetPosition(uint8_t ubNumSection)
{
	SectionCommonMessage*	pCommonMessage = NULL;

	//choice of the component to move
	if (m_strMotor.at(m_counterAction).compare(SECTION_TOWER_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTower->m_CommonMessage;
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_PUMP_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pPump->m_CommonMessage;
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_TRAY_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pTray->m_CommonMessage;
	}
	else if (m_strMotor.at(m_counterAction).compare(SECTION_SPR_STRING) == 0)
	{
		pCommonMessage = &sectionBoardLinkSingleton(ubNumSection)->m_pSPR->m_CommonMessage;
	}

	int32_t slValue = 0;
	pCommonMessage->getAbsolutePosition(slValue);
	m_strPosition.at(m_counterAction).assign(to_string(slValue));

	m_pLogger->log(LOG_DEBUG, "OpGetPosition::executeGetPosition: POSi:%d POSs:%s ",
				   slValue, m_strPosition.at(m_counterAction).c_str());
	return 0;
}

int OpGetPosition::executeNSHGetPosition(void)
{
	int liValue = 0;
	MainExecutor::getInstance()->m_SPIBoard.m_pNSHMotor->getAbsPosition(liValue);
	liValue /= MOT_HALF_TO_MICRO_STEPS;
	m_strPosition.at(m_counterAction).assign(to_string(-liValue));

	m_pLogger->log(LOG_DEBUG, "OpGetPosition::executeGetPosition: POSi:%d POSs:%s ",
				   liValue, m_strPosition.at(m_counterAction).c_str());
	return 0;
}

int OpGetPosition::executeCmd(int lNum)
{
	//action to perform
	int res = 0;

    if( lNum == NSH_ID )
	{
		res = executeNSHGetPosition();
	}
	else
	{
		res = executeSectionGetPosition(lNum);
	}

	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "OpGetPosition::execute-->execute not performed");
		// operation completed -> send reply and close it
		closeOperation();
		return -1;
	}

	std::string previousComponent;
	previousComponent.assign(m_strComponent.at(m_counterAction));

	// increase the action counter
	m_counterAction++;

	// if we have performed the last action compose and send the reply, stop the loop
	if(m_counterAction >= m_strComponent.size() )
	{
		// operation completed -> send reply and close it
		closeOperation();
		res = -1;
	}
	else if(previousComponent.compare(m_strComponent.at(m_counterAction)) != 0)
	{
		// actions on the current device are completed:
		// --> go for next action
		perform();
		// -> unlock the actual
		res = -1;
	}

	m_pLogger->log(LOG_INFO, "OpGetPosition::execute-->completed");

	return res;
}

GetPositionPayload * OpGetPosition::buildPayload(void)
{
    GetPositionPayload * pGetPositionPayload = new GetPositionPayload();

    pGetPositionPayload->setOutValues(m_strComponent, m_strMotor, m_strPosition);

    return pGetPositionPayload;
}


int OpGetPosition::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdGetPosition cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
    GetPositionPayload * pGetPositionPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
    cmd.setPayload((Payload *)pGetPositionPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpGetPosition::execute--> unable to serialize OutCmdMove");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}
