/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetPosition.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetPosition class.
 @details

 ****************************************************************************
*/

#ifndef OPGETPOSITION_H
#define OPGETPOSITION_H

#include "Operation.h"
#include "Payload.h"

class GetPositionPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETPOSITION command
 * ********************************************************************************************************************
 */
class OpGetPosition : public Operation
{
    public:

        /*! ***********************************************************************************************************
         * @brief OpGetPosition default constructor
         * ***********************************************************************************************************
         */
        OpGetPosition(const string &strComponent, const string &strMotor);

        /*! ***********************************************************************************************************
         * @brief ~OpGetPosition default destructor
         * ************************************************************************************************************
         */
        virtual ~OpGetPosition();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	method that builds the payload of the reply and sends it
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

		/*! ***********************************************************************************************************
		 * @brief setAskBoard	to set if the command has to perform a real request to Hardware
		 * @param status true -> action required, false -> no action (default) get data from memory
		 * ************************************************************************************************************
		 */
		void setAskBoard(bool status);

	private:

		/*!
		 * @brief executeGetPosition	calls the GetAbsolutePosition method of the related section component.
		 * @param ubNumSection		number of the section to ask for value
		 * @return 0 if success | -1 otherwise
		 */
		int executeSectionGetPosition(uint8_t ubNumSection);

		/*!
         * @brief buildPayload	Fills the GetPositioPayload of the outgoing command with related values.
		 * @return  pointer to the MovePayload object just allocated
		 */
        GetPositionPayload * buildPayload(void);

		/*!
		 * @brief executeNSHGetPosition	calls the GetAbsolutePosition method of the related NSH component.
		 * @return 0 if success | -1 otherwise
		 */
		int executeNSHGetPosition(void);

    private:
        std::vector<std::string> m_strComponent;
        std::vector<std::string> m_strMotor;
        std::vector<std::string> m_strPosition;

        uint8_t	m_counterAction;
        bool	m_bAskBoards;

};

#endif // OPGETPOSITION_H
