/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetSerial.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSetSerial class.
 @details

 ****************************************************************************
*/

#ifndef OPSETSERIAL_H
#define OPSETSERIAL_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SETSERIAL command
 * ********************************************************************************************************************
 */
class OpSetSerial : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpSetSerial default constructor
		 * ***********************************************************************************************************
		 */
		OpSetSerial();

		/*! ***********************************************************************************************************
		 * @brief ~OpSetMaintenanceMode default destructor
		 * ************************************************************************************************************
		 */
		~OpSetSerial();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief setSerialInstrument sets the Instrument Serial string
		 * @param strSerial the Instrument Serial string
		 * ************************************************************************************************************
		 */
		void setSerialInstrument(const string& strSerial);

		/*! ***********************************************************************************************************
		 * @brief setSerialMasterboard sets the Masterboard Serial string
		 * @param strSerial the Masterboard Serial string
		 * ************************************************************************************************************
		 */
		void setSerialMasterboard(const string& strSerial);

		/*! ***********************************************************************************************************
		 * @brief setSerialNSH sets the NSH Serial string
		 * @param strSerial the NSH Serial string
		 * ************************************************************************************************************
		 */
		void setSerialNSH(const string& strSerial);

		/*! ***********************************************************************************************************
		 * @brief setSerialSectionA sets the SectionA Serial string
		 * @param strSerial the SectionA Serial string
		 * ************************************************************************************************************
		 */
		void setSerialSectionA(const string& strSerial);

		/*! ***********************************************************************************************************
		 * @brief setSerialSectionB sets the SectionA Serial string
		 * @param strSerial the SectionB Serial string
		 * ************************************************************************************************************
		 */
		void setSerialSectionB(const string& strSerial);


	private :

		string m_strSerialInstrument;
		string m_strSerialMasterboard;
		string m_strSerialNSH;
		string m_strSerialSectionA;
		string m_strSerialSectionB;

};

#endif // OPSETSERIAL_H
