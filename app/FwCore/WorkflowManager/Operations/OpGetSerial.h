/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetSerial.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetSerial class.
 @details

 ****************************************************************************
*/

#ifndef OPGETSERIAL_H
#define OPGETSERIAL_H

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETSERIAL command
 * ********************************************************************************************************************
 */
class OpGetSerial : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpGetSerial default constructor
		 * ***********************************************************************************************************
		 */
		OpGetSerial();

		/*! ***********************************************************************************************************
		 * @brief ~OpGetSerial default destructor
		 * ************************************************************************************************************
		 */
		~OpGetSerial();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);
};

#endif // OPGETSERIAL_H
