/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSHCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSHCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef OPSHCALIBRATE_H
#define OPSHCALIBRATE_H

#include "Operation.h"
#include "Payload.h"

class SHCalibratePayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SHCALIBRATE command
 * ********************************************************************************************************************
 */

class OpSHCalibrate : public Operation
{
	private:

		enum class eState { eStart, eMoveToTrayRead, eCalibrateSH, eMoveToTrayOut, eClean, eNshError, eSectError, eStop };

		string m_strSection;
		string m_strSlot;
		volatile eState m_eShState;

		vector<pair<string, string>> m_vsTheoricItem;
		vector<pair<string, string>> m_vsRealItem;

	public:

		/*! ***********************************************************************************************************
		 * @brief OpSHCalibrate default constructor
		 * ***********************************************************************************************************
		 */
		OpSHCalibrate();

		/*! ***********************************************************************************************************
		 * @brief ~v default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpSHCalibrate();

		/*!
		 * @brief setParams	set the parameters for calibration
		 * @param strSection
		 * @param strSlot
		 */
		void setParams(string strSection, string strSlot);

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

	private:

		/*! ***********************************************************************************************************
		 * @brief createItemVector create a vector<pair<string, string>> with the data related to slot_num (1 to 12)
		 * @param vpItem vector to be populated
		 * @param liSlotNum slot num (1 to 12)
		 * ************************************************************************************************************
		 */
		void createItemVector(vector<pair<string, string> >& vpItem, int liSlotNum);

		/*! ************************************************************************************************************
		 * @brief buildPayload	Fills the SHCalibratePayload of the outgoing command with related values.
		 * @return  pointer to the SHCalibratePayload object just allocated
		 * *************************************************************************************************************
		 */
		SHCalibratePayload* buildPayload();
};

#endif // OPSHCALIBRATE_H
