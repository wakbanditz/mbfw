/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpOPTCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpOPTCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef OPOPTCALIBRATE_H
#define OPOPTCALIBRATE_H

#include "Operation.h"
#include "Payload.h"
#include "Loggable.h"

class OPTCalibratePayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the OPTCALIBRATE command
 * ********************************************************************************************************************
 */

class OpOPTCalibrate : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpOPTCalibrate default constructor
		 * ***********************************************************************************************************
		 */
		OpOPTCalibrate();

		/*! ***********************************************************************************************************
		 * @brief ~OpOPTCalibrate default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpOPTCalibrate();

		/*!
		 * @brief setParameters set the parameters for calibration
		 * @param strSection
		 * @param strSlot
		 * @param strTarget
		 * @return
		 */
		void setParameters(string strSection, string strSlot, string strTarget);

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*!
		 * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
		 * then it composes the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 */
		int compileSendOutCmd();

	private:

		/*!
		 * @brief buildPayload	Fills the OPTCalibratePayload of the outgoing command with related values.
		 * @return  pointer to the OPTCalibratePayload object just allocated
		 */
		OPTCalibratePayload * buildPayload(void);

        /*!
         * @brief updateOPT	if procedure is OK the new OPT identifier (based on current date - time)
         *  is generated and stored
         * @return the OPT string
         */
        std::string updateOPT();

    private:

        enum class eState { eStart, eMoveToTrayOut, eMoveNshToSlot, eAirCalibrate, eMoveToTrayRead, eCalibrateOpt,
                  eMoveNshToSS, eReadSS, eStoreSSVal, eGetNshGain, eDefault, eClean, eNshError, eSectError, eStop };

        eState	m_eOptState;
        string	m_strSection;
        string	m_strSlot;
        string	m_strTarget;
        string	m_strSSFluoVal;
        string	m_strGain;
        string  m_strID;

};

#endif // OPOPTCALIBRATE_H
