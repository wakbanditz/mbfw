/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSecRoute.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSecRoute class.
 @details

 ****************************************************************************
*/

#ifndef OPSECROUTE_H
#define OPSECROUTE_H

#include "InCmdSecRoute.h"
#include "Operation.h"

class SecRoutePayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the ROUTE command
 * ********************************************************************************************************************
 */

class OpSecRoute : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpSecRoute default constructor
		 * ***********************************************************************************************************
		 */
		OpSecRoute();

		/*! ***********************************************************************************************************
		 * @brief ~OpSecRoute default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpSecRoute();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief setParameters	called by the workflow to transfer the parameters for the ReadSection procedure
		 * @param section the index of the section involved
		 * @param strCommand the string command to be sent
		 * ************************************************************************************************************
		 */
		void setParameters(int section, std::string strCommand);

		/*! ***********************************************************************************************************
		 * @brief getSection get the section involved in the RouteSection procedure
		 * @return the index of the section if defined, -1 otherwise
		 * ************************************************************************************************************
		 */
		int getSection();

		/*! ***********************************************************************************************************
		 * @brief getCommand get the command string
		 * @return the m_strCommand string
		 * ************************************************************************************************************
		 */
		std::string getCommand();

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*!
		 * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
		 * then it composes the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 */
		int compileSendOutCmd();

		/*! ***********************************************************************************************************
		 * @brief buildPayload compose the xml payload to be sent
		 * @return pointer to the payload object
		 * ***********************************************************************************************************
		 */
		SecRoutePayload * buildPayload();


	private:

		std::string m_strCommand;
		std::string m_strReply;
		int m_sectionRoute;

		bool executeNSHRoute(void);
		bool executeCameraRoute(void);
		bool executeMasterRoute(void);
		bool compareWithWildCard(const string& str1, const string& str2, char cWildCardChar);
};

#endif // OPSECROUTE_H
