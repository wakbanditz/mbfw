/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpOPTCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpOPTCalibrate class.
 @details

 ****************************************************************************
*/

#include "OpOPTCalibrate.h"
#include "MainExecutor.h"
#include "SectionBoardTray.h"
#include "WebServerAndProtocolInclude.h"
#include "OPTCalibratePayload.h"
#include "OutCmdOPTCalibrate.h"

#define DEFAULT_AIR_RFU_CAL_VALUE		5
#define TRAY_READ_CODED_COMMAND			"Read"
#define TRAY_OUT_CODED_COMMAND			"Out"

OpOPTCalibrate::OpOPTCalibrate() : Operation("OpOPTCalibrate")
{
	m_eOptState = eState::eStart;
	m_strSection.clear();
	m_strSlot.clear();
	m_strTarget.clear();
	m_strSSFluoVal.clear();
	m_strGain.clear();
    m_strID.clear();
}

OpOPTCalibrate::~OpOPTCalibrate()
{
	/* Nothing to do yet */
}

void OpOPTCalibrate::setParameters(string strSection, string strSlot, string strTarget)
{
	m_strSection.assign(strSection);
	m_strSlot.assign(strSlot);
	m_strTarget.assign(strTarget);
}

int OpOPTCalibrate::perform()
{
	int liNumSection = ( ! m_strSection.compare(XML_VALUE_SECTION_A) ) ? SCT_A_ID : SCT_B_ID;

	switch ( m_eOptState )
	{
		case eState::eStart:
		case eState::eMoveNshToSlot:
		case eState::eAirCalibrate:
		case eState::eCalibrateOpt:
		case eState::eMoveNshToSS:
		case eState::eReadSS:
		case eState::eStoreSSVal:
		case eState::eGetNshGain:
		case eState::eDefault:
		case eState::eNshError:
			return nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
		break;

		case eState::eMoveToTrayRead:
		case eState::eMoveToTrayOut:
		case eState::eSectError:
		case eState::eClean:
			return sectionBoardManagerSingleton(liNumSection)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
		break;

		case eState::eStop:
		{
            // register the event
            registerOPTCalibration(m_strSection, m_strSlot, m_strTarget, m_strGain, m_strSSFluoVal, m_strID);

            restoreStateMachine();
			int8_t cResult = compileSendOutCmd();
			if ( cResult == eOperationEnabledForDeath )
			{
				operationSingleton()->removeOperation(this);
				m_pLogger->log(LOG_INFO, "NSHBoardManager::workerThread-->operation removed");
			}
			else
			{
				m_pLogger->log(LOG_ERR, "NSHBoardManager::workerThread-->NO operation removal");
			}
		}
		break;
	}

	return 0;
}

int OpOPTCalibrate::executeCmd(int lNum)
{
	int liRes = -lNum;
	int liNumSection = ( ! m_strSection.compare(XML_VALUE_SECTION_A) ) ? SCT_A_ID : SCT_B_ID;
	int liFluoVal = 0;

	enum eDevice { eNsh, eSection, eUnInit };
	eDevice eCurrDevice = eUnInit;
	eDevice eNextDevice = eUnInit;

	// Not to call the perform on the same device -> Op in progress otherwise
	while ( eCurrDevice == eNextDevice && m_eOptState != eState::eStop )
	{
		switch ( m_eOptState )
		{
			case eState::eStart:
				m_eOptState = eState::eMoveToTrayOut;
				eCurrDevice = eNsh;
				eNextDevice = eSection;
			break;

			case eState::eMoveToTrayOut:
			{
				eCurrDevice = eSection;
				eNextDevice = eNsh;

				string strCoded("");
				strCoded = sectionBoardLinkSingleton(liNumSection)->m_pTray->getCodedFromLabel(TRAY_OUT_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to get coded position from tray out label [" TRAY_OUT_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_DEBUG, "OpOPTCalibrate::executeCmd: move tray to pos predefined: CMD= %X", ubPosCode);
				liRes = sectionBoardLinkSingleton(liNumSection)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to perform air calibration");
					break;
				}
				m_eOptState = eState::eMoveNshToSlot;
			}
			break;

			case eState::eMoveNshToSlot:
			{
				eCurrDevice = eNextDevice = eNsh;
				string strPos("");
				int liSlotId = stoi(m_strSlot);
				liSlotId = ( liNumSection == SCT_A_ID ) ? liSlotId : liSlotId + SCT_NUM_TOT_SLOTS;
				string strCodedName = "Slot_" + to_string(liSlotId) + "C";
				nshBoardManagerSingleton()->m_pConfig->getItemValue(strCodedName, strPos);

				int liPos = stoi(strPos);
				liRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
				if ( ! liRes )
				{
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to move NSH motor to desired position");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpOPTCalibrate::executeCmd: moved NSH to slot position");
				m_eOptState = eState::eAirCalibrate;
			}
			break;

			case eState::eAirCalibrate:
				eCurrDevice = eNsh;
				liRes = nshReaderGeneralSingleton()->calibrateAirFact(DEFAULT_AIR_RFU_CAL_VALUE);
				if ( liRes )
				{
					eNextDevice = eNsh;
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to perform air calibration");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpOPTCalibrate::executeCmd: air calibrated");
				eNextDevice = eSection;
				m_eOptState = eState::eMoveToTrayRead;
			break;

			case eState::eMoveToTrayRead:
			{
				eCurrDevice = eSection;
				eNextDevice = eNsh;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(liNumSection)->m_pTray->getCodedFromLabel(TRAY_READ_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to get coded position from tray read label [" TRAY_READ_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_INFO, "OpOPTCalibrate::executeCmd: move tray to pos predefined: CMD= %X", ubPosCode);
				liRes = sectionBoardLinkSingleton(liNumSection)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to perform air calibration");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpOPTCalibrate::executeCmd: air calibrated");
				m_eOptState = eState::eCalibrateOpt;
			}
			break;

			case eState::eCalibrateOpt:
			{
				eCurrDevice = eNextDevice = eNsh;
				liFluoVal = stoi(m_strTarget);
				liRes = nshReaderGeneralSingleton()->optCalibrateRFU(liFluoVal);
				if ( liRes )
				{
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to calibrate opt");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpOPTCalibrate::executeCmd: opt calibrated");
				m_eOptState = eState::eMoveNshToSS;
			}
			break;

			case eState::eMoveNshToSS:
			{
				eCurrDevice = eNextDevice = eNsh;
				string strPos("");
				string strCodedName("SS");
				nshBoardManagerSingleton()->m_pConfig->getItemValue(strCodedName, strPos);

				int liPos = stoi(strPos);
				liRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
				if ( ! liRes )
				{
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to move NSH motor to desired position");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpOPTCalibrate::executeCmd: moved to SS position");
				m_eOptState = eState::eReadSS;
			}
			break;

			case eState::eReadSS:
			{
				eCurrDevice = eNextDevice = eNsh;
				liRes = nshReaderGeneralSingleton()->readSolidStandardRFU(liFluoVal);
				if ( liRes )
				{
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to read Solid Standard RFU");
					break;
				}
				m_strSSFluoVal = to_string(liFluoVal);
				m_pLogger->log(LOG_DEBUG, "OpOPTCalibrate::executeCmd: read SS");
				m_eOptState = eState::eStoreSSVal;
			}
			break;

			case eState::eStoreSSVal:
			{
				eCurrDevice = eNextDevice = eNsh;
				liRes = nshReaderGeneralSingleton()->storeSolidStdRFU(liFluoVal);
				if ( liRes )
				{
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to store Solid Standard RFU");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpOPTCalibrate::executeCmd: stored SS val");
				m_eOptState = eState::eGetNshGain;
			}
			break;

			case eState::eGetNshGain:
			{
				eCurrDevice = eNextDevice = eNsh;
				strucCalibrParam sCalibr;
				liRes = nshReaderGeneralSingleton()->getCalibrParams(&sCalibr);
				if ( liRes )
				{
					m_eOptState = eState::eNshError;
					m_pLogger->log(LOG_ERR,"OpOPTCalibrate::executeCmd: not able to get gain");
					break;
				}
                m_strGain = to_string(int(sCalibr.fSSCalibPar * 100));
				m_eOptState = eState::eClean;
			}
			break;

			case eState::eNshError:
			{
				eCurrDevice = eNsh;
				eNextDevice = eSection;
				MainExecutor::getInstance()->m_SPIBoard.move(eHome);
				m_eOptState = eState::eSectError;
			}
			break;

			case eState::eSectError:
			{
				m_eOptState = eState::eStop;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(liNumSection)->m_pTray->getCodedFromLabel(TRAY_OUT_CODED_COMMAND);
				if ( strCoded.empty() )	break;

				char ubPosCode = *strCoded.c_str();
				int liRes = sectionBoardLinkSingleton(liNumSection)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )			break;
			}
			break;

			case eState::eClean:
			{
				m_eOptState = eState::eStop;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(liNumSection)->m_pTray->getCodedFromLabel(TRAY_OUT_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to get coded position from tray out label [" TRAY_OUT_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				liRes = sectionBoardLinkSingleton(liNumSection)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
                    m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd: unable to perform tray out");
					break;
				}

                // procedure is completed -> update the OPT identifier
                m_strID = updateOPT();

                // store value to local variables so that are available without asking the NSH
                infoSingleton()->setSSvalue(m_strSSFluoVal);
                infoSingleton()->setSSgain(m_strGain);

                m_pLogger->log(LOG_DEBUG, "OpOPTCalibrate::executeCmd: tray out");
			}
			break;

			default:	m_eOptState = eState::eStop;		break;
		}
	}

	liRes = perform();
	if ( liRes != eOperationRunning )
	{
		m_pLogger->log(LOG_ERR, "OpOPTCalibrate::executeCmd-->execute not performed");
		return -1;
	}

	return eOperationRunning;
}

int OpOPTCalibrate::compileSendOutCmd()
{
	OutCmdOPTCalibrate cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	OPTCalibratePayload* pOPTCalibratePayload = buildPayload();
    // ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pOPTCalibratePayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
        m_pLogger->log(LOG_ERR, "OpOPTCalibrate::execute--> unable to serialize OutCmdOPTCalibrate");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

OPTCalibratePayload* OpOPTCalibrate::buildPayload()
{
	OPTCalibratePayload* pOPTCalibratePayload = new OPTCalibratePayload();

    pOPTCalibratePayload->setAttributes(m_strSection, m_strSlot, m_strGain, m_strSSFluoVal, m_strID);

	return pOPTCalibratePayload;
}

std::string OpOPTCalibrate::updateOPT()
{
    // compose the OPT string from current date time
    std::string strOPT;

    strOPT.assign("OPT");
    strOPT.append(getFormattedTime(false));

    infoSingleton()->setOPTid(strOPT);

    return strOPT;
}
