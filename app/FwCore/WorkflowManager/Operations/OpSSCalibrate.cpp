/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSSCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSSCalibrate class.
 @details

 ****************************************************************************
*/

#include "OpSSCalibrate.h"
#include "MainExecutor.h"
#include "OutCmdSSCalibrate.h"
#include "WebServerAndProtocolInclude.h"
#include "SSCalibratePayload.h"


OpSSCalibrate::OpSSCalibrate() : Operation("OpSSCalibrate")
{
	m_strTheorPos.clear();
	m_strRealPos.clear();
}

OpSSCalibrate::~OpSSCalibrate()
{
	/* Nothing to do yet */
}

int OpSSCalibrate::perform()
{
	// get the value of the theoretical position of the solid standard from config file
	string strName(NSH_SOLID_STANDARD_CONFIG_NAME);
	nshBoardManagerSingleton()->m_pConfig->getItemValue(strName, m_strTheorPos);

	return nshBoardManagerSingleton()->setNSHAction(eNSHActionCmd, (Operation*)this);
}

int OpSSCalibrate::executeCmd(int lNum)
{
	// Find real solid standard position
	int liRes = nshBoardManagerSingleton()->findXMaxFluoVal(NSH_SOLID_STANDARD_ID, lNum);
	if ( liRes )	return liRes;

	m_strRealPos = to_string(lNum);	

	// Replace the older one in the config file
	string strName(NSH_SOLID_STANDARD_CONFIG_NAME);
	nshBoardManagerSingleton()->m_pConfig->updateNSHConfiguration(strName, lNum);

	return 0;
}

int OpSSCalibrate::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdSSCalibrate cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	SSCalibratePayload * pSSCalibratePayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pSSCalibratePayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSSCalibrate::execute--> unable to serialize OutSSCalibrate");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}
	return eOperationEnabledForDeath;
}

SSCalibratePayload* OpSSCalibrate::buildPayload()
{
	SSCalibratePayload * pSSCalibratePayload = new SSCalibratePayload();

	string strName(NSH_SOLID_STANDARD_CONFIG_NAME);
	pSSCalibratePayload->setPositions(strName, m_strTheorPos, m_strRealPos);

	return pSSCalibratePayload;
}
