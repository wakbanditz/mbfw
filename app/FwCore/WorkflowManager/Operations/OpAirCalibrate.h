/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpAirCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpAirCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef OPAIRCALIBRATE_H
#define OPAIRCALIBRATE_H

#include "Operation.h"
#include "Loggable.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the AIRCALINRATE command
 * ********************************************************************************************************************
 */

class OpAirCalibrate : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpAirCalibrate default constructor
		 * ***********************************************************************************************************
		 */
		OpAirCalibrate();

		/*! ***********************************************************************************************************
		 * @brief ~OpAirCalibrate default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpAirCalibrate();

		/*!
		 * @brief setParameter set the parameters for calibration
		 * @param strTarget
		 * @return
		 */
		void setParameter(string strTarget);

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*!
		 * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
		 * then it composes the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 */
		int compileSendOutCmd();

    private:

        string	m_strTarget;

};

#endif // OPAIRCALIBRATE_H
