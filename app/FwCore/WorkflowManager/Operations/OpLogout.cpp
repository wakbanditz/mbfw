/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpLogout.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpLogout class.
 @details

 ****************************************************************************
*/

#include "OpLogout.h"
#include "OutCmdLogout.h"
#include "THAutoLogout.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpLogout::OpLogout() : Operation("OpLogout")
{

}

OpLogout::~OpLogout()
{

}

int OpLogout::perform(void)
{
	// no action is required -> read data from memory and send them back to SW

	// remove from the active login the one linked to the ip address + port
	if(MainExecutor::getInstance()->m_loginManager.removeLogin(m_strUsage, m_strWebResponseUrl))
	{
		// login removed from list: disable autoLogout
		THAutoLogout::getInstance()->stopConnection(m_strUsage);
	}

	OutCmdLogout outCmdLogout;
	string strOutCommand;

	outCmdLogout.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdLogout.setUsage(getUsage());
	outCmdLogout.setID(getID());

	protocolSerialize(&outCmdLogout, strOutCommand);

	sendCommandReply(strOutCommand);

	return eOperationWaitForDeath;
}
