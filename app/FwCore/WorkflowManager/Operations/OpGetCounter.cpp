/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetCounter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetCounter class.
 @details

 ****************************************************************************
*/

#include "OpGetCounter.h"
#include "OutCmdGetCounter.h"
#include "GetCounterPayload.h"

#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpGetCounter::OpGetCounter() : Operation("OpGetCounter")
{
	m_strCounter.clear();
}

OpGetCounter::~OpGetCounter()
{

}

int OpGetCounter::perform(void)
{
	// no action is required -> read data from memory and send them back to SW
	uint8_t indexCounter = infoSingleton()->getInstrumentCounterIndexFromString(m_strCounter);

	OutCmdGetCounter outCmdGetCounter;
	string strOutCommand;

	outCmdGetCounter.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdGetCounter.setUsage(getUsage());
	outCmdGetCounter.setID(getID());

	// build the payload of the asynchrounous reply
	GetCounterPayload * pGetCounterPayload = new GetCounterPayload();
	pGetCounterPayload->setCounter(m_strCounter);
	pGetCounterPayload->setValue(infoSingleton()->getInstrumentCounter((eCounterId)indexCounter));
	// and assign it to the outcommand so that the serialize will extract the info
	outCmdGetCounter.setPayload((Payload *)pGetCounterPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	protocolSerialize(&outCmdGetCounter, strOutCommand);

	sendCommandReply(strOutCommand);

	return eOperationWaitForDeath;
}

void OpGetCounter::setCounter(const string& strCounter)
{
	m_strCounter.assign(strCounter);
}

