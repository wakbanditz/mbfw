/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetGlobalSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSetGlobalSettings class.
 @details

 ****************************************************************************
*/

#ifndef OPGLOBALSETTINGS_H
#define OPGLOBALSETTINGS_H

#include "CommonInclude.h"

#include "Operation.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GLOBAL_SETTINGS command
 * ********************************************************************************************************************
 */

class OpSetGlobalSettings : public Operation
{

	public:

		/*! ***********************************************************************************************************
		 * @brief OpGlobalSettings default constructor
		 * ***********************************************************************************************************
		 */
		OpSetGlobalSettings();

		/*! ***********************************************************************************************************
		 * @brief ~OpGlobalSettings default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpSetGlobalSettings();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);


		/*! *************************************************************************************************
		 * @brief DATA SETTERS
		 * **************************************************************************************************
		 */
		void setSectionEnable(uint8_t section, bool value);
		void setSprMinTemperature(uint8_t section, int value);
		void setSprMaxTemperature(uint8_t section, int value);
		void setSprTargetTemperature(uint8_t section, int value);
		void setSprToleranceTemperature(uint8_t section, int value);

		void setTrayMinTemperature(uint8_t section, int value);
		void setTrayMaxTemperature(uint8_t section, int value);
		void setTrayTargetTemperature(uint8_t section, int value);
		void setTrayToleranceTemperature(uint8_t section, int value);

		void setInstrumentEnable(bool value);
		void setInternalMinTemperature(int value);
		void setInternalMaxTemperature(int value);

		void setForceTemperature(bool status);

		/*! *************************************************************************************************
		 * @brief DATA GETTERS
		 * **************************************************************************************************
		 */
		int getSprMinTemperature(uint8_t section);
		int getSprMaxTemperature(uint8_t section);
		int getSprTargetTemperature(uint8_t section);
		int getSprToleranceTemperature(uint8_t section);

		int getTrayMinTemperature(uint8_t section);
		int getTrayMaxTemperature(uint8_t section);
		int getTrayTargetTemperature(uint8_t section);
		int getTrayToleranceTemperature(uint8_t section);

		int getInternalMinTemperature();
		int getInternalMaxTemperature();

		bool getForceTemperature();

	private :

		/*! ***********************************************************************************************************
		 * @brief finalOperations	to close the operation and send back reply
		 * ************************************************************************************************************
		 */
		void finalOperations();

	private:

		int m_SprMin[SCT_NUM_TOT_SECTIONS], m_SprMax[SCT_NUM_TOT_SECTIONS];
		int m_SprTarget[SCT_NUM_TOT_SECTIONS], m_SprTolerance[SCT_NUM_TOT_SECTIONS];
		int m_TrayMin[SCT_NUM_TOT_SECTIONS], m_TrayMax[SCT_NUM_TOT_SECTIONS];
		int m_TrayTarget[SCT_NUM_TOT_SECTIONS], m_TrayTolerance[SCT_NUM_TOT_SECTIONS];
		bool m_bForce;
		int m_InternalMin, m_InternalMax;

		bool m_bSection[SCT_NUM_TOT_SECTIONS];
		bool m_bInstrument;
};

#endif // OPGLOBALSETTINGS_H
