/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSHCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSHCalibrate class.
 @details

 ****************************************************************************
*/

#include "OpSHCalibrate.h"
#include "OutCmdSHCalibrate.h"
#include "CommonInclude.h"
#include "MainExecutor.h"
#include "SectionBoardTray.h"
#include "SHCalibratePayload.h"
#include "WebServerAndProtocolInclude.h"

#define NSH_CALIBR_POSITIONS_NUM		3
#define TRAY_READ_CODED_COMMAND			"Read"
#define TRAY_OUT_CODED_COMMAND			"Out"

OpSHCalibrate::OpSHCalibrate() : Operation("OpSHCalibrate")
{
	m_eShState = eState::eStart;
	m_strSection.clear();
	m_strSlot.clear();
	m_vsTheoricItem.clear();
	m_vsRealItem.clear();
}

OpSHCalibrate::~OpSHCalibrate()
{

}

void OpSHCalibrate::setParams(string strSection, string strSlot)
{
	m_strSection.assign(strSection);
	m_strSlot.assign(strSlot);
}

int OpSHCalibrate::perform()
{
	// Get all the theoric values
	int liSlotNum = stoi(m_strSlot);
    int liStart = 1;
    int liNumSection = SCT_A_ID;

    // If we are in section B slots start from 7
    if(m_strSection.compare(XML_VALUE_SECTION_B) == 0)
    {
        liSlotNum += SCT_NUM_TOT_SLOTS;
        liNumSection = SCT_B_ID;
        liStart = SCT_NUM_TOT_SLOTS + 1;
    }

//	int liStart = ( stoi(m_strSlot) <= SCT_NUM_TOT_SLOTS ) ? 1 : SCT_NUM_TOT_SLOTS+1;

	switch	( m_eShState )
	{
		case eState::eStart:
		{
			for ( int i = liStart; i < liStart+SCT_NUM_TOT_SLOTS; i++ )
			{
				createItemVector(m_vsTheoricItem, i);
			}
			return nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
		}
		break;

		case eState::eCalibrateSH:
		case eState::eNshError:
		case eState::eClean:
			return nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
		break;

		case eState::eMoveToTrayRead:
		case eState::eMoveToTrayOut:
		case eState::eSectError:
			return sectionBoardManagerSingleton(liNumSection)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
		break;

		case eState::eStop:
		{
			for ( int i = liStart; i < liStart+SCT_NUM_TOT_SLOTS; i++ )
			{
				createItemVector(m_vsRealItem, i);
			}

			restoreStateMachine();
			int8_t cResult = compileSendOutCmd();
			if ( cResult == eOperationEnabledForDeath )
			{
				operationSingleton()->removeOperation(this);
				m_pLogger->log(LOG_INFO, "NSHBoardManager::workerThread-->operation removed");
			}
			else
			{
				m_pLogger->log(LOG_ERR, "NSHBoardManager::workerThread-->NO operation removal");
			}
		}
		break;
	}

	return 0;
}

int OpSHCalibrate::executeCmd(int lNum)
{
	int liRes = -lNum; // just to use lNum
	int liNumSection = ( ! m_strSection.compare(XML_VALUE_SECTION_A) ) ? SCT_A_ID : SCT_B_ID;
	bool bRes = false;
	int liFluoVal,  liSlot;
	liSlot = stoi(m_strSlot);
	liSlot = liSlot + ( SCT_NUM_TOT_SLOTS * liNumSection );

	enum eDevice { eNsh, eSection, eUnInit };
	eDevice eCurrDevice = eUnInit;
	eDevice eNextDevice = eUnInit;

	// Not to call the perform on the same device -> Op in progress otherwise
	while ( eCurrDevice == eNextDevice && m_eShState != eState::eStop )
	{
		switch ( m_eShState )
		{
			case eState::eStart:
				eCurrDevice = eNsh;
				eNextDevice = eSection;
				m_eShState = eState::eMoveToTrayRead;
			break;

			case eState::eMoveToTrayRead:
			{
				eCurrDevice = eSection;
				eNextDevice = eNsh;

				string strCoded("");
				strCoded = sectionBoardLinkSingleton(liNumSection)->m_pTray->getCodedFromLabel(TRAY_READ_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eShState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpSHCalibrate::executeCmd: unable to get coded position from tray read label [" TRAY_READ_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_INFO, "OpSHCalibrate::executeCmd: move tray to pos predefined: CMD= %X", ubPosCode);
				liRes = sectionBoardLinkSingleton(liNumSection)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eShState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpSHCalibrate::executeCmd: unable to move to coded position");
					break;
				}
				m_eShState = eState::eCalibrateSH;
			}
			break;

			case eState::eCalibrateSH:
				eCurrDevice = eNsh;
				liRes = nshBoardManagerSingleton()->findXMaxFluoVal(liSlot, liFluoVal);
				if ( liRes != 0 )
				{
					eNextDevice = eNsh;
					m_eShState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpSHCalibrate::execute: execute not performed");
				}
				eNextDevice = eSection;
				m_eShState = eState::eMoveToTrayOut;
			break;

			case eState::eMoveToTrayOut:
			{
				eCurrDevice = eSection;
				eNextDevice = eNsh;

				string strCoded("");
				strCoded = sectionBoardLinkSingleton(liNumSection)->m_pTray->getCodedFromLabel(TRAY_OUT_CODED_COMMAND);
				if ( strCoded.empty() )
				{
					m_eShState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpSHCalibrate::executeCmd: unable to get coded position from tray out label [" TRAY_OUT_CODED_COMMAND "]");
					break;
				}

				char ubPosCode = *strCoded.c_str();
				m_pLogger->log(LOG_DEBUG, "OpSHCalibrate::executeCmd: move tray to pos predefined: CMD= %X", ubPosCode);
				liRes = sectionBoardLinkSingleton(liNumSection)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )
				{
					m_eShState = eState::eNshError;
					m_pLogger->log(LOG_ERR, "OpSHCalibrate::executeCmd: unable to perform air calibration");
					break;
				}
				m_eShState = eState::eClean;
			}
			break;

			case eState::eNshError:
			{
				eCurrDevice = eNsh;
				eNextDevice = eSection;
				// Move machine to default position
				MainExecutor::getInstance()->m_SPIBoard.move(eHome);
				m_eShState = eState::eSectError;
			}
			break;

			case eState::eSectError:
			{
				// Move machine to default position
				m_eShState = eState::eStop;
				string strCoded("");
				strCoded = sectionBoardLinkSingleton(liNumSection)->m_pTray->getCodedFromLabel(TRAY_OUT_CODED_COMMAND);
				if ( strCoded.empty() )	break;

				char ubPosCode = *strCoded.c_str();
				liRes = sectionBoardLinkSingleton(liNumSection)->m_pTray->m_CommonMessage.moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
				if ( liRes )			break;
			}
			break;

			case eState::eClean:
			{
				m_eShState = eState::eStop;
				string strPos("");
				string strCodedName("SS");
				nshBoardManagerSingleton()->m_pConfig->getItemValue(strCodedName, strPos);
				int liPos = stoi(strPos);
				bRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
				if ( ! bRes )
				{
					m_pLogger->log(LOG_ERR, "OpSHCalibrate::executeCmd: unable to move NSH motor to desired position");
					break;
				}
				m_pLogger->log(LOG_DEBUG, "OpSHCalibrate::executeCmd: moved to SS position");
				m_pLogger->log(LOG_INFO, "OpSHCalibrate::execute: SH_CALIBRATE executed");
			}
			break;

			default:	m_eShState = eState::eStop;		break;
		}
	}

	liRes = perform();
	if ( liRes != eOperationRunning )
	{
		m_pLogger->log(LOG_ERR, "OpTrayRead::executeCmd-->execute not performed");
		return -1;
	}

	return eOperationRunning;

}

int OpSHCalibrate::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdSHCalibrate cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	SHCalibratePayload* pSHCalibratePayload = buildPayload();
	// ... and assign it to the outcommand so that the serializle will extract the info
	cmd.setPayload((Payload *)pSHCalibratePayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSetMotorActivation::execute--> unable to serialize OutCmdSetMotorActivation");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

void OpSHCalibrate::createItemVector(vector<pair<string, string>>& vpItem, int liSlotNum)
{
	char rgcPositions[NSH_CALIBR_POSITIONS_NUM] = {'C', 'L', 'R'};

	for ( int i = 0; i != NSH_CALIBR_POSITIONS_NUM; i++ )
	{
		pair<string, string> psItem;
		psItem.first = "Slot_" + to_string(liSlotNum) + rgcPositions[i];
		nshBoardManagerSingleton()->m_pConfig->getItemValue(psItem.first, psItem.second);
		vpItem.push_back(psItem);
	}
}

SHCalibratePayload* OpSHCalibrate::buildPayload(void)
{
	SHCalibratePayload* pPayload = new SHCalibratePayload();

	pPayload->setOutParams(m_strSection, m_vsTheoricItem, m_vsRealItem);

	return pPayload;

}
