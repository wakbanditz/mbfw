/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetParameter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSetParameter class.
 @details

 ****************************************************************************
*/

#include "OpSetParameter.h"
#include "OutCmdSetParameter.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "SectionBoardManager.h"
#include "SetParameterPayload.h"
#include "CommonInclude.h"

#define SET_MICROSTEP		1
#define SET_HIGH_CURRENT	2
#define SET_LOW_CURRENT		3
#define SET_CONVERSION		4

OpSetParameter::OpSetParameter() : Operation("OpSetParameter")
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strMicrostep.clear();
	m_strHighCurrent.clear();
	m_strLowCurrent.clear();
	m_strAcceleration.clear();
	m_strConversion.clear();
}

OpSetParameter::~OpSetParameter()
{
	/* Nothing to do yet */
}

int OpSetParameter::perform()
{
	if (m_strComponent.compare(XML_TAG_SECTIONA_NAME) == 0)
	{
		return sectionBoardManagerSingleton(SCT_A_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if (m_strComponent.compare(XML_TAG_SECTIONB_NAME) == 0)
	{
		return sectionBoardManagerSingleton(SCT_B_ID)->setSectionAction(SECTION_ACTION_EXECUTE, (Operation*)this);
	}
	else if ( m_strComponent.compare(XML_TAG_NSH_NAME) == 0 )
	{
		return nshBoardManagerSingleton()->setNSHAction(eNSHActionExecute, (Operation*)this);
	}

	m_pLogger->log(LOG_WARNING, "OpSetMotorActivation::perform-->Component %s not in list", m_strComponent.c_str());
	return eOperationError;
}

int OpSetParameter::executeCmd(int lNum)
{

#ifdef __DEBUG_PROTOCOL
	return 0;
#endif

	uint8_t setting = 0;
	std::string strSetting;

	// settings are performed in sequence and every time the corresponding string
	// is cleared to detect when the procedure is over
	// NOTE: acceleration is ignored !
	if(m_strMicrostep.empty() == false)
	{
		setting = SET_MICROSTEP;
		strSetting.assign(m_strMicrostep);
		m_strMicrostep.clear();
	}
	else if(m_strHighCurrent.empty() == false)
	{
		setting = SET_HIGH_CURRENT;
        strSetting.assign(m_strHighCurrent);
		m_strHighCurrent.clear();
	}
	else if(m_strLowCurrent.empty() == false)
	{
		setting = SET_LOW_CURRENT;
		strSetting.assign(m_strLowCurrent);
		m_strLowCurrent.clear();
	}
	else if(m_strConversion.empty() == false)
	{
		setting = SET_CONVERSION;
		strSetting.assign(m_strConversion);
		m_strConversion.clear();
	}
	else
	{
		// all settings completed -> end
		closeOperation();
		return -1;
	}

	int liRes = -1;

	switch ( lNum )
	{
		case SCT_A_ID:
		case SCT_B_ID:	//action to perform
			liRes = executeSetMotorSection(lNum, setting, strSetting);
		break;

		case NSH_ID:
			liRes = executeSetMotorNSH(setting, strSetting);
		break;
	}

	if ( liRes != 0 )
	{
		closeOperation();
		m_pLogger->log(LOG_ERR, "OpSetParameter::execute-->execute not performed");
		return -1;
	}

	m_pLogger->log(LOG_INFO, "OpSetParameter::execute--> executed");
	return 0;
}

SetParameterPayload * OpSetParameter::buildPayload(void)
{
	SetParameterPayload * pPayload = new SetParameterPayload();

	pPayload->setOutValues(m_strComponent, m_strMotor);

	return pPayload;

}

int OpSetParameter::executeSetMotorSection(uint8_t ubNumSection, uint8_t setting, std::string strSetting)
{
	SectionCommonMessage*	pMotor = NULL;

	//choice of the component to move
	if ( m_strMotor.compare(SECTION_TOWER_STRING) == 0 )
	{
		pMotor = &sectionBoardLinkSingleton(ubNumSection)->m_pTower->m_CommonMessage;
	}
	else if ( m_strMotor.compare(SECTION_PUMP_STRING) == 0 )
	{
		pMotor = &sectionBoardLinkSingleton(ubNumSection)->m_pPump->m_CommonMessage;
	}
	else if ( m_strMotor.compare(SECTION_TRAY_STRING) == 0 )
	{
		pMotor = &sectionBoardLinkSingleton(ubNumSection)->m_pTray->m_CommonMessage;
	}
	else if ( m_strMotor.compare(SECTION_SPR_STRING) == 0 )
	{
		pMotor = &sectionBoardLinkSingleton(ubNumSection)->m_pSPR->m_CommonMessage;
	}
	else
	{
		m_pLogger->log(LOG_WARNING, "OpSetMotorActivation::executeCmd: Motor %s not in list",
					   m_strMotor.c_str());
		return -1;
	}

	int res = 0;

	char buffer[12];
	sprintf(buffer, "%s", strSetting.c_str());

	switch(setting)
	{
		case SET_MICROSTEP:
			res = pMotor->setMicrostepResolution((enumSectionCommonMsgStepMode)buffer[0]);
		break;
		case SET_HIGH_CURRENT:
            sprintf(buffer, "%X", atoi(strSetting.c_str()));
			res = pMotor->setMotorCurrent(eRun, (uint8_t)buffer[0]);
		break;
		case SET_LOW_CURRENT:
        sprintf(buffer, "%X", atoi(strSetting.c_str()));
            res = pMotor->setMotorCurrent(eStill, (uint8_t)buffer[0]);
		break;
		case SET_CONVERSION:
			res = pMotor->setMotorConversionFactor((uint32_t)atoi(buffer));
		break;
	}

	return res;
}

int OpSetParameter::executeSetMotorNSH(uint8_t setting, string strSetting)
{
	int liRes = 0;
	float liCurrVal = stof(strSetting);

	switch (setting)
	{
		case SET_HIGH_CURRENT:
			liRes = spiBoardLinkSingleton()->m_pNSHMotor->setMotorCurrent(eSetTValRun, liCurrVal);
		break;
		case SET_LOW_CURRENT:
			liRes = spiBoardLinkSingleton()->m_pNSHMotor->setMotorCurrent(eSetTValHold, liCurrVal);
		break;

		default:
		break;
	}

	return liRes;
}

int OpSetParameter::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdSetParameter cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	SetParameterPayload * pSetParameterPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pSetParameterPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSetParameter::execute--> unable to serialize");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

void OpSetParameter::setComponent(string strValue)
{
	m_strComponent.assign(strValue);
}

void OpSetParameter::setMotor(string strValue)
{
	m_strMotor.assign(strValue);
}

void OpSetParameter::setMicrostep(string strValue)
{
	m_strMicrostep.assign(strValue);
}

void OpSetParameter::setConversion(string strValue)
{
	m_strConversion.assign(strValue);
}

void OpSetParameter::setHighCurrent(string strValue)
{
	m_strHighCurrent.assign(strValue);
}

void OpSetParameter::setLowCurrent(string strValue)
{
	m_strLowCurrent.assign(strValue);
}

void OpSetParameter::setAcceleration(string strValue)
{
	m_strAcceleration.assign(strValue);
}


