/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetSerial.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSetSerial class.
 @details

 ****************************************************************************
*/

#include "OpSetSerial.h"
#include "OutCmdSetSerial.h"

#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpSetSerial::OpSetSerial() : Operation("OpSetSerial")
{
	m_strSerialInstrument.clear();
	m_strSerialMasterboard.clear();
	m_strSerialNSH.clear();
	m_strSerialSectionA.clear();
	m_strSerialSectionB.clear();
}

OpSetSerial::~OpSetSerial()
{

}

void OpSetSerial::setSerialInstrument(const string& strSerial)
{
	m_strSerialInstrument.assign(strSerial);
}

void OpSetSerial::setSerialMasterboard(const string& strSerial)
{
	m_strSerialMasterboard.assign(strSerial);
}

void OpSetSerial::setSerialNSH(const string& strSerial)
{
	m_strSerialNSH.assign(strSerial);
}

void OpSetSerial::setSerialSectionA(const string& strSerial)
{
	m_strSerialSectionA.assign(strSerial);
}

void OpSetSerial::setSerialSectionB(const string& strSerial)
{
	m_strSerialSectionB.assign(strSerial);
}

int OpSetSerial::perform(void)
{
	if(!m_strSerialInstrument.empty())
	{
		infoSingleton()->setInstrumentSerialNumber(m_strSerialInstrument);
	}
	if(!m_strSerialMasterboard.empty())
	{
		infoSingleton()->setMasterSerialNumber(m_strSerialMasterboard);
	}
	if(!m_strSerialNSH.empty())
	{
		infoSingleton()->setNshSerialNumber(m_strSerialNSH);
	}
	if(!m_strSerialSectionA.empty())
	{
		infoSingleton()->setSectionASerialNumber(m_strSerialSectionA);
	}
	if(!m_strSerialSectionB.empty())
	{
		infoSingleton()->setSectionASerialNumber(m_strSerialSectionB);
	}


	OutCmdSetSerial cmd;
	string strOutCommand;

	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// no payload to build

	protocolSerialize(&cmd, strOutCommand);

	restoreStateMachine();

	sendCommandReply(strOutCommand);

	return eOperationWaitForDeath;
}

