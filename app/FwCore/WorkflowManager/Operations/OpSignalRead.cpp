/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSignalRead.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSignalRead class.
 @details

 ****************************************************************************
*/

#include "OpSignalRead.h"
#include "OutCmdSignalRead.h"
#include "CommonInclude.h"
#include "SignalReadPayload.h"
#include "WebServerAndProtocolInclude.h"

OpSignalRead::OpSignalRead() : Operation("OpSignalRead")
{
	m_strRepetitions.clear();
	m_strInterval.clear();
	m_vFluoRead.clear();
	m_vTimeStamps.clear();
	m_vFluoMv.clear();
	m_vRefMv.clear();
}

OpSignalRead::~OpSignalRead()
{

}

void OpSignalRead::setParams(string strRepetitions, string strInterval)
{
	m_strRepetitions.assign(strRepetitions);
	m_strInterval.assign(strInterval);
}

int OpSignalRead::perform()
{
	return nshBoardManagerSingleton()->setNSHAction(eNSHActionCmd, (Operation*)this);
}

int OpSignalRead::executeCmd(int lNum)
{
	int liCycles = -lNum; // just to use lNum
	liCycles = stoi(m_strRepetitions);
	m_vFluoRead.resize(liCycles);
	m_vTimeStamps.resize(liCycles);
	m_vFluoMv.resize(liCycles);
	m_vRefMv.resize(liCycles);

	for ( int i = 0; i < liCycles; i++ )
	{
		m_vTimeStamps[i] = getFormattedTime(true);

		structNSHFluoRead structFluoRead;
		int8_t cRes = nshReaderGeneralSingleton()->getNSHFluoRead(NSH_ENABLE_CONVERSION_FACTOR, &structFluoRead);
		if ( cRes )
		{
			m_pLogger->log(LOG_ERR, "OpSignalRead::executeCmd: unable to read RFU value");
			return -1;
		}
		m_vFluoRead[i] = to_string(structFluoRead.liRFUValue);

		structNSHLastRFV structLastFluoVal;
		structLastFluoVal.liReading = 11;
		cRes = nshReaderGeneralSingleton()->getLastFluoRawValuemV(&structLastFluoVal, 255);
		if ( cRes )
		{
			m_pLogger->log(LOG_ERR, "OpSignalRead::executeCmd: unable to get last RFU value mv");
			return -1;
		}
		m_vFluoMv[i] = to_string(structLastFluoVal.liReading);

		structNSHLastReference structReference;
		structReference.liReading = 11;
		cRes = nshReaderGeneralSingleton()->getLastRefRawValuemV(&structReference, 255);;
		if ( cRes )
		{
			m_pLogger->log(LOG_ERR, "OpSignalRead::executeCmd: unable to get last RFU value mv");
			return -1;
		}
		m_vRefMv[i] = to_string(structReference.liReading);

		msleep(stoi(m_strInterval));
	}

	return 0;
}

int OpSignalRead::compileSendOutCmd()
{
	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdSignalRead cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	SignalReadPayload* pSignalReadPayload = buildPayload();
	// ... and assign it to the outcommand so that the serializle will extract the info
	cmd.setPayload((Payload *)pSignalReadPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************l***********************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpSetMotorActivation::execute--> unable to serialize OutCmdSetMotorActivation");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationEnabledForDeath;
}

SignalReadPayload* OpSignalRead::buildPayload(void)
{
	SignalReadPayload* pPayload = new SignalReadPayload();

	pPayload->setOutParams(m_vFluoRead, m_vTimeStamps, m_vFluoMv, m_vRefMv);

	return pPayload;
}
