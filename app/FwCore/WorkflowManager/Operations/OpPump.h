/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpPump.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpPump class.
 @details

 ****************************************************************************
*/

#ifndef OPPUMP_H
#define OPPUMP_H

#include "Operation.h"
#include "SectionBoardManager.h"
#include "Loggable.h"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the PUMP command
 * ********************************************************************************************************************
 */
class OpPump : public Operation
{
    public:
        /*! ***********************************************************************************************************
         * @brief OpPump default constructor
         * ***********************************************************************************************************
         */
        OpPump(const string &strComponent, const string &strAction, const string &strCommand);

        /*! ***********************************************************************************************************
         * @brief ~OpPump default destructor
         * ************************************************************************************************************
         */
        virtual ~OpPump();

        /*! ***********************************************************************************************************
         * @brief perform	method that implements the action
         * ************************************************************************************************************
         */
        int perform(void);

        /*! ***********************************************************************************************************
         * @brief execute	method that implements the action
         * @return 0 if success | -1 otherwise
         * ************************************************************************************************************
         */
        int executeCmd(int lNum);

        /*!
         * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
         * then it composes the message to be sent to the sw
         * @return eOperationEnabledForDeath
         */
        int compileSendOutCmd();

    private:


    private:
        string m_strComponent;
        string m_strAction;
        string m_strCommand;
};

#endif // OPPUMP_H
