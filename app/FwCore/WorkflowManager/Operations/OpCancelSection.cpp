/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpCancelSection.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpCancelSection class.
 @details

 ****************************************************************************
*/

#include "OpCancelSection.h"
#include "OutCmdCancelSection.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpCancelSection::OpCancelSection() : Operation("OpCancelSection")
{
}

OpCancelSection::~OpCancelSection()
{
}

int OpCancelSection::perform(void)
{

	// ............

	OutCmdCancelSection outCmdCancelSection;
	string strOutCommand;

	outCmdCancelSection.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdCancelSection.setUsage(getUsage());
	outCmdCancelSection.setID(getID());

	protocolSerialize(&outCmdCancelSection, strOutCommand);

	restoreStateMachine();

	sendCommandReply(strOutCommand);
	return eOperationWaitForDeath;
}

