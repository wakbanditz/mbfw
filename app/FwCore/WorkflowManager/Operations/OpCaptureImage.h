/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpCaptureImage.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpCaptureImage class.
 @details

 ****************************************************************************
*/

#ifndef OPCAPTUREIMAGE_H
#define OPCAPTUREIMAGE_H

#include "Operation.h"
#include "WFCaptureImage.h"

#define ONLY_ONE_IMAGE			1
#define STOP_TAKING_PICTURES	"STOP"

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the CAPTUREIMAGE command
 * ********************************************************************************************************************
 */

class OpCaptureImage : public Operation
{
	public:

		/*! ***********************************************************************************************************
         * @brief OpCaptureImage default constructor
		 * ***********************************************************************************************************
		 */
		OpCaptureImage();

		/*! ***********************************************************************************************************
         * @brief ~OpCaptureImage default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpCaptureImage();

		/*! ***********************************************************************************************************
		 * @brief setParameters	called by the workflow to transfer the parameters needed by the Camera to perform any
		 *		  actions
         * @param strAction the action string
         * @param tMotor the motor parameters
         * @param pStruct the camera structure
		 * ************************************************************************************************************
		 */
		void setParameters(string strAction, vector<vector<tuple<string, string, string>>>& tMotor, structCameraParams& pStruct);

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/** ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/** ***********************************************************************************************************
		 * @brief  getParams get all the params needed to build the payload correctly
		 * @return pointer to the struct with the params desired
		 * ************************************************************************************************************
		 */
		structCameraParams* getParams(void);

		/** ***********************************************************************************************************
		 * @brief  getAction to be performed, can be "ONESHOT", "RUN" or "STOP".
		 * @return the action
		 * ************************************************************************************************************
		 */
		string getAction(void);

private:

		/** ************************************************************************************************************
		 * @brief executeMoveSection	move Section's motor
		 * @return  0 in case of success, -1 otherwise
		 * *************************************************************************************************************
		 */
		int executeMoveSection(int liSecNum);

		/** ************************************************************************************************************
		 * @brief executeMoveNSH	move NSH's motor
		 * @return  0 in case of success, -1 otherwise
		 * *************************************************************************************************************
		 */
		int executeMoveNSH(void);

		/** ************************************************************************************************************
		 * @brief executeCamera set camera settings and get picture
		 * @return  0 in case of success, -1 otherwise
		 * *************************************************************************************************************
		 */
		int executeCamera(void);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	set the status as SUCCESFULL so that the reply is composed with all the parameters
		 * then it composes the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * *************************************************************************************************************/
		int compileSendOutCmd();

		int drawCropRectangle(vector<unsigned char>& vImage, const structInfoImage* pImage, cropRectangle rect, bool bRuotate);
		bool customization(int*liIll);
		int addBMPheader(const structInfoImage* pImage, int liIdx);
		void swapY(vector<unsigned char>&vImage, int liImageWidth, int liImageHeight, int liTimes = 0);

    private:

		int					m_nSectionId;
		int					m_nCurrImageId;
		bool				m_bEnableRoi[2];
		bool				m_isTimeToHomeMotors;
        string				m_strAction;
        structCameraParams	m_CameraParams;
		vector<vector<unsigned char>>				  m_vData;
		vector<vector<tuple<string, string, string>>> m_vMotorTuple;

};

#endif // OPCAPTUREIMAGE_H
