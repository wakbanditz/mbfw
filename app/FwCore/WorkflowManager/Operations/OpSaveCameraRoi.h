/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSaveCameraRoi.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSaveCameraRoi class.
 @details

 ****************************************************************************
*/

#ifndef OPSAVECAMERAROI_H
#define OPSAVECAMERAROI_H

#include "Operation.h"
#include "Payload.h"
#include "CameraConfiguration.h"

class SaveCameraRoiPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SAVECAMERAROI command
 * ********************************************************************************************************************
 */

class OpSaveCameraRoi : public Operation
{
public:

        /*! ***********************************************************************************************************
         * @brief OpSaveCameraRoi default constructor
         * ***********************************************************************************************************
         */
        OpSaveCameraRoi();

        /*! ***********************************************************************************************************
         * @brief ~OpSaveCameraRoi default destructor
         * ************************************************************************************************************
         */
        virtual ~OpSaveCameraRoi();

        /*!
         * @brief setParams	set the parameters for calibration
         * @param vTarget
         * @param vTop
         * @param vBottom
         * @param vLeft
         * @param vRight
         */
        void setParams(vector<string>& vTarget, vector<string>& vTop, vector<string>& vBottom,
                                   vector<string>& vLeft, vector<string>& vRight);

        /*! ***********************************************************************************************************
         * @brief perform	method that implements the action
         * ************************************************************************************************************
         */
        int perform(void);

        /*! ***********************************************************************************************************
         * @brief execute	method that implements the action
         * @param lNum the device index
         * @return 0 if success | -1 otherwise
         * ************************************************************************************************************
         */
        int executeCmd(int lNum);

        /*! ***********************************************************************************************************
         * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
         * @return eOperationEnabledForDeath
         * ************************************************************************************************************
         */
        int compileSendOutCmd();

    private:

        /*! ************************************************************************************************************
         * @brief buildPayload	Fills the SaveCameraRoiPayload of the outgoing command with related values.
         * @return  pointer to the SaveCameraRoiPayload object just allocated
         * *************************************************************************************************************
         */
        SaveCameraRoiPayload* buildPayload();

    private:

        unordered_map<string, structRect> m_mapSubSet;
		unordered_map<string, structRect> m_mapSubSetExpected;

};

#endif // OPSAVECAMERAROI_H
