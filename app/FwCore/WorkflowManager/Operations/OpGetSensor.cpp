/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetSensor.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpGetSensor class.
 @details

 ****************************************************************************
*/

#include "OpGetSensor.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "OutCmdGetSensor.h"
#include "GetSensorPayload.h"
#include "SectionBoardManager.h"
#include "SectionBoardGeneral.h"

#include "CommonInclude.h"
#include "ModuleInclude.h"

OpGetSensor::OpGetSensor( const string &strCategory,
						  const string &strComponent,
						  const string &strId)
							: Operation("OpGetSensor")
{
	m_strCategory.assign(strCategory);
	m_strComponent.assign(strComponent);
	m_strId.assign(strId);
}

OpGetSensor::~OpGetSensor()
{
	/* Nothing to do yet */
}

void OpGetSensor::setCategory(const string& strCategory)
{
	m_strCategory.assign(strCategory);
}

void OpGetSensor::setComponent(const string& strComponent)
{
	m_strComponent.assign(strComponent);
}

void OpGetSensor::setId(const string& strId)
{
	m_strId.assign(strId);
}

int OpGetSensor::perform(void)
{
	// no action is required -> read data from memory and send them back to SW

	// set the status as SUCCESFULL so that the reply is composed with all the parameters
	OutCmdGetSensor cmd;
	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// compose the payload ...
	GetSensorPayload * pGetSensorPayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pGetSensorPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************************************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&cmd, strOutCommand);
	if ( ! bSerializeResult )
	{
		m_pLogger->log(LOG_ERR, "OpGetSensor::compileSendOutCmd: unable to serialize GetSensor Out cmd");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	return eOperationWaitForDeath;
}


GetSensorPayload * OpGetSensor::buildPayload()
{
    GetSensorPayload * pGetSensorPayload = new GetSensorPayload();

    pGetSensorPayload->setOutCmdByParameterRequested(m_strCategory, m_strComponent, m_strId);

    structPayloadSectionSensorValues getSensorValues;
    structPayloadInstrumentSensorValues sensorInstrument;

    // NOTE: Pay Attention to the Decimals

    // !!! Add PowerManager Voltages

#ifdef __DEBUG_PROTOCOL

    getSensorValues.strPumpHome = to_string(0);
    getSensorValues.strSprHome = to_string(1);
    getSensorValues.strTrayHome = to_string(0);
    getSensorValues.strTowerHome = to_string(1);
    getSensorValues.strDoor = to_string(0);
    getSensorValues.strSprNtc = to_string(3690);
    getSensorValues.strTrayNtc = to_string(3710);

    pGetSensorPayload->setSensorValuesSection(SCT_A_ID, getSensorValues);

    getSensorValues.strPumpHome = to_string(1);
    getSensorValues.strSprHome = to_string(0);
    getSensorValues.strTrayHome = to_string(1);
    getSensorValues.strTowerHome = to_string(0);
    getSensorValues.strDoor = to_string(1);
    getSensorValues.strSprNtc = to_string(3730);
    getSensorValues.strTrayNtc = to_string(3750);

    pGetSensorPayload->setSensorValuesSection(SCT_B_ID, getSensorValues);

    structPayloadSectionVoltageValues getVoltageValues;

    getVoltageValues.str24V = to_string(24100);
    getVoltageValues.str5V = to_string(5200);
    getVoltageValues.str3V3 = to_string(3280);
    getVoltageValues.str1V2 = to_string(1190);

    pGetSensorPayload->setVoltageValuesSection(SCT_A_ID, getVoltageValues);

    getVoltageValues.str24V = to_string(23900);
    getVoltageValues.str5V = to_string(4900);
    getVoltageValues.str3V3 = to_string(3310);
    getVoltageValues.str1V2 = to_string(1230);

    pGetSensorPayload->setVoltageValuesSection(SCT_B_ID, getVoltageValues);

    sensorInstrument.strSAFan = to_string(75);
    sensorInstrument.strSBFan = to_string(85);
    sensorInstrument.strSANtc = to_string(3290);
    sensorInstrument.strSBNtc = to_string(3560);
    sensorInstrument.strInstrFan = to_string(55);
	sensorInstrument.strInstrNtc = to_string(3333);

    structPayloadNshSensorValues nshSensor;
	nshSensor.strNSHHome = to_string(1);

    pGetSensorPayload->setSensorValuesInstrument(sensorInstrument);

#else

    getSensorValues.strPumpHome = to_string(sectionBoardGeneralSingleton(SCT_A_ID)->getSensorPump());
    getSensorValues.strSprHome = to_string(sectionBoardGeneralSingleton(SCT_A_ID)->getSensorSpr());
    getSensorValues.strTrayHome = to_string(sectionBoardGeneralSingleton(SCT_A_ID)->getSensorTray());
    getSensorValues.strTowerHome = to_string(sectionBoardGeneralSingleton(SCT_A_ID)->getSensorTower());
    getSensorValues.strDoor = to_string(sectionBoardGeneralSingleton(SCT_A_ID)->getDoorStatus());
    getSensorValues.strSprNtc = to_string(sectionBoardGeneralSingleton(SCT_A_ID)->getIntTemperatureSPR());
    getSensorValues.strTrayNtc = to_string(sectionBoardGeneralSingleton(SCT_A_ID)->getIntTemperatureTray());

    pGetSensorPayload->setSensorValuesSection(SCT_A_ID, getSensorValues);

    getSensorValues.strPumpHome = to_string(sectionBoardGeneralSingleton(SCT_B_ID)->getSensorPump());
    getSensorValues.strSprHome = to_string(sectionBoardGeneralSingleton(SCT_B_ID)->getSensorSpr());
    getSensorValues.strTrayHome = to_string(sectionBoardGeneralSingleton(SCT_B_ID)->getSensorTray());
    getSensorValues.strTowerHome = to_string(sectionBoardGeneralSingleton(SCT_B_ID)->getSensorTower());
    getSensorValues.strDoor = to_string(sectionBoardGeneralSingleton(SCT_B_ID)->getDoorStatus());
    getSensorValues.strSprNtc = to_string(sectionBoardGeneralSingleton(SCT_B_ID)->getIntTemperatureSPR());
    getSensorValues.strTrayNtc = to_string(sectionBoardGeneralSingleton(SCT_B_ID)->getIntTemperatureTray());

    pGetSensorPayload->setSensorValuesSection(SCT_B_ID, getSensorValues);

    structPayloadSectionVoltageValues getVoltageValues;

    getVoltageValues.str24V = to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_A_ID)->getVoltage24V() * 1000. / 100.));
    getVoltageValues.str5V = to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_A_ID)->getVoltage5V() * 1000. / 100.));
    getVoltageValues.str3V3 = to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_A_ID)->getVoltage3V3() * 1000. / 100.));
    getVoltageValues.str1V2 = to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_A_ID)->getVoltage1V2() * 1000. / 100.));

    pGetSensorPayload->setVoltageValuesSection(SCT_A_ID, getVoltageValues);

    getVoltageValues.str24V = to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_B_ID)->getVoltage24V() * 1000. / 100.));
    getVoltageValues.str5V = to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_B_ID)->getVoltage5V() * 1000. / 100.));
    getVoltageValues.str3V3 = to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_B_ID)->getVoltage3V3() * 1000. / 100.));
    getVoltageValues.str1V2 = to_string((uint16_t)(sectionBoardGeneralSingleton(SCT_B_ID)->getVoltage1V2() * 1000. / 100.));

    pGetSensorPayload->setVoltageValuesSection(SCT_B_ID, getVoltageValues);

	structPayloadNshSensorValues nshSensor;
	if ( stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) )
	{
		nshSensor.strNSHHome = to_string(spiBoardLinkSingleton()->m_pNSHMotor->isSensorAtHome());
	}
	else
	{
		nshSensor.strNSHHome = to_string(0);
	}
	pGetSensorPayload->setSensorValuesNsh(nshSensor);


    sensorInstrument.strSAFan = to_string(masterBoardManagerSingleton()->getFanRpm(eFanSectionA));
    sensorInstrument.strSBFan = to_string(masterBoardManagerSingleton()->getFanRpm(eFanSectionB));
    sensorInstrument.strSANtc = to_string(masterBoardManagerSingleton()->getNtcValue(eNtcInstrument));
    sensorInstrument.strSBNtc = to_string(masterBoardManagerSingleton()->getNtcValue(eNtcInstrument));
    sensorInstrument.strInstrFan = to_string(masterBoardManagerSingleton()->getFanRpm(eFanSupply));
    sensorInstrument.strInstrNtc = to_string(masterBoardManagerSingleton()->getNtcValue(eNtcSupply));

    pGetSensorPayload->setSensorValuesInstrument(sensorInstrument);

#endif

    return pGetSensorPayload;
}
