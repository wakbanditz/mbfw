/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpRunwl.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpRunwl class.
 @details

 ****************************************************************************
*/

#include "OpRunwl.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpRunwl::OpRunwl() : Operation("OpRunwl")
{
}

OpRunwl::~OpRunwl()
{
}

int OpRunwl::perform(void)
{
	// the operation is kept alive: it will be closed at the end of protocol

	// ............
	return eOperationRunning;
}
