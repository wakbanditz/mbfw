/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpDisable.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpDisable class.
 @details

 ****************************************************************************
*/

#include "OpDisable.h"
#include "OutCmdDisable.h"
#include "DisablePayload.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "SectionBoardManager.h"
#include "SectionBoardGeneral.h"
#include "CommonInclude.h"

OpDisable::OpDisable() : Operation("OpDisable")
{
}

OpDisable::~OpDisable()
{
}

int OpDisable::perform(void)
{
	int res;
	uint8_t sectionAction;

	res = eOperationWaitForDeath;


	if(m_bDisable)
	{
		// enter disable mode -> send command to sections
		sectionAction = SLEEP_DISABLE_ENABLE;
	}
	else
	{
		// exit disable mode -> send command to sections
		sectionAction = SLEEP_DISABLE_DISABLE;
	}

	for(uint8_t i = 0; i < m_deviceList.size(); i++)
	{
		switch(m_deviceList.at(i))
		{
			case eIdSectionA :
				if(sectionBoardGeneralSingleton(SCT_A_ID)->sleepOrDisable(sectionAction, SET_DISABLE) == -1)
					res = eOperationError;
			break;

			case eIdSectionB :
				if(sectionBoardGeneralSingleton(SCT_B_ID)->sleepOrDisable(sectionAction, SET_DISABLE) == -1)
					res = eOperationError;
			break;

			case eIdNsh :
			case eIdCamera :
				// !!!! TODO something to do ?
			break;

			case eIdModule :
			default:
			break;
		}
	}

	restoreStateMachine();

	OutCmdDisable outCmdDisable;
	string strOutCommand;

	outCmdDisable.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	outCmdDisable.setUsage(getUsage());
	outCmdDisable.setID(getID());

	// compose the payload ...
	DisablePayload * pDisablePayload = buildPayload();
	// ... and assign it to the outcommand so that the serialize will extract the info
	outCmdDisable.setPayload((Payload *)pDisablePayload);
	// Note: the payload will be deleted by the OutgoingCommand

	bool bProtocolSerialize = protocolSerialize(&outCmdDisable, strOutCommand);

	if (bProtocolSerialize == false)
	{
		m_pLogger->log(LOG_ERR, "OpDisable:: unable to serialize DisableOut cmd");
	}
	else
	{
		sendCommandReply(strOutCommand);
	}

	MainExecutor::getInstance()->saveDisableState();

	if(m_bDisable == false)
	{
		m_pLogger->log(LOG_INFO, "OpDisable:: going to reinit");
		// delay to let the sections reboot
		sleep(10);
		// start the reinit procedure
		MainExecutor::getInstance()->reinitInstrument();
	}

	return res;
}

void OpDisable::setAction(bool status)
{
	m_bDisable = status;
}

DisablePayload* OpDisable::buildPayload()
{
	DisablePayload * pDisablePayload = new DisablePayload();

	if(stateMachineSingleton()->getDeviceStatus(eIdModule) == eStatusDisable)
	{
		pDisablePayload->setInstrumentDisabled(true);
	}
	else
	{
		pDisablePayload->setInstrumentDisabled(false);
	}

	if(stateMachineSingleton()->getDeviceStatus(eIdCamera) == eStatusDisable)
	{
		pDisablePayload->setCameraDisabled(true);
	}
	else
	{
		pDisablePayload->setCameraDisabled(false);
	}

	if(stateMachineSingleton()->getDeviceStatus(eIdNsh) == eStatusDisable)
	{
		pDisablePayload->setNshDisabled(true);
	}
	else
	{
		pDisablePayload->setNshDisabled(false);
	}

	if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusDisable)
	{
		pDisablePayload->setSectionADisabled(true);
	}
	else
	{
		pDisablePayload->setSectionADisabled(false);
	}

	if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusDisable)
	{
		pDisablePayload->setSectionBDisabled(true);
	}
	else
	{
		pDisablePayload->setSectionBDisabled(false);
	}

	return pDisablePayload;
}
