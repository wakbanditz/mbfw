/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetMaintenanceMode.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the OpSetMaintenanceMode class.
 @details

 ****************************************************************************
*/

#include "OpSetMaintenanceMode.h"
#include "OutCmdSetMaintenanceMode.h"
#include "SetMaintenanceModePayload.h"

#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"

OpSetMaintenanceMode::OpSetMaintenanceMode() : Operation("OpSetMaintenanceMode")
{
	m_strActivation.clear();
}

OpSetMaintenanceMode::~OpSetMaintenanceMode()
{

}

void OpSetMaintenanceMode::setActivation(const string& strActivation)
{
	m_strActivation.assign(strActivation);
}

int OpSetMaintenanceMode::perform(void)
{
	// set the new status if Maintenance mode according to the command string
	if(m_strActivation.compare(XML_ATTR_STATUS_ENABLED) == 0)
	{
		infoSingleton()->setMaintenanceMode(true);
	}
	else
	{
		infoSingleton()->setMaintenanceMode(false);
	}

	OutCmdSetMaintenanceMode cmd;
	string strOutCommand;

	cmd.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	cmd.setUsage(getUsage());
	cmd.setID(getID());

	// build the payload of the asynchrounous reply

	SetMaintenanceModePayload * pSetMaintenanceModePayload = new SetMaintenanceModePayload();
	pSetMaintenanceModePayload->setActivation(m_strActivation);
	// and assign it to the outcommand so that the serialize will extract the info
	cmd.setPayload((Payload *)pSetMaintenanceModePayload);
	// Note: the payload will be deleted by the OutgoingCommand

	protocolSerialize(&cmd, strOutCommand);

	restoreStateMachine();

	sendCommandReply(strOutCommand);

	return eOperationWaitForDeath;
}

