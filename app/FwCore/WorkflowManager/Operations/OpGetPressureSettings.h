/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpGetPressureSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpGetPressureSettings class.
 @details

 ****************************************************************************
*/

#ifndef OPGETPRESSURESETTINGS_H
#define OPGETPRESSURESETTINGS_H

#include "Operation.h"

class GetPressureSettingsPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the GETPRESSURESETTINGS command
 * ********************************************************************************************************************
 */
class OpGetPressureSettings : public Operation
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OpGetPressureSettings default constructor
		 * ***********************************************************************************************************
		 */
		OpGetPressureSettings(std::vector<uint8_t> componentList);

		/*! ***********************************************************************************************************
		 * @brief ~OpGetPressureSettings default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpGetPressureSettings();

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd( void );

		/*! ***********************************************************************************************************
		 * @brief setAskBoard	to set if the command has to perform a real request to Hardware
		 * @param status true -> action required, false -> no action (default) get data from memory
		 * ************************************************************************************************************
		 */
		void setAskBoard(bool status);

	private:

		/*!
		 * @brief buildPayload	Fills the GetPressureOffsetsPayload of the outgoing command with related values.
		 * @return  pointer to the GetPressureOffsetsPayload object just allocated
		 */
		GetPressureSettingsPayload * buildPayload(void);

    private:
        std::vector<std::string> m_strComponent;
        std::vector<std::string> m_strConversions;
        std::vector<std::string> m_strPressures;
        std::string m_strConversionFactor;
        std::string m_strPressureFactor;

        bool	m_bAskBoards;

};


#endif // OPGETPRESSURESETTINGS_H
