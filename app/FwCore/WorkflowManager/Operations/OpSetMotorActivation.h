/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OpSetMotorActivation.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the OpSetMotorActivation class.
 @details

 ****************************************************************************
*/

#ifndef OPSETMOTORACTIVATION_H
#define OPSETMOTORACTIVATION_H

#include "Operation.h"
#include "Payload.h"

class SetMotorActivationPayload;

/*! *******************************************************************************************************************
 * @brief class	derived from Operation to perform the actions of the SETMOTORACTIVATION command
 * ********************************************************************************************************************
 */
class OpSetMotorActivation : public Operation
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OpSetMotorActivation default constructor
		 * ***********************************************************************************************************
		 */
		OpSetMotorActivation();

		/*! ***********************************************************************************************************
		 * @brief ~OpSetMotorActivation default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OpSetMotorActivation();

		/*!
		 * @brief setParams	set the parameters for the sending of the messages to the component and motor
		 * requested
		 * @param strComponent
		 * @param strMotor
		 * @param strActivation
		 */
		void setParams( const string &strComponent, const string &strMotor, const string &strActivation );

		/*! ***********************************************************************************************************
		 * @brief perform	method that implements the action
		 * ************************************************************************************************************
		 */
		int perform(void);

		/*! ***********************************************************************************************************
		 * @brief execute	method that implements the action
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int executeCmd(int lNum);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return eOperationEnabledForDeath
		 * ************************************************************************************************************
		 */
		int compileSendOutCmd();

	private:

		/*! ************************************************************************************************************
		 * @brief buildPayload	Fills the SetMotorActivationPayload of the outgoing command with related values.
		 * @return  pointer to the SetMotorActivationPayload object just allocated
		 * *************************************************************************************************************
		 */
		SetMotorActivationPayload * buildPayload(void);

		/*! ************************************************************************************************************
		 * @brief executeSetMotorSection	set the current of the Section's motor
		 * @param ubNumSection number of the section of interest
		 * @return  0 in case of success, -1 otherwise
		 * *************************************************************************************************************
		 */
		int executeSetMotorSection(uint8_t ubNumSection);

		/*! ************************************************************************************************************
		 * @brief executeSetMotorSection	set the current of the NSH's motor
		 * @return  0 in case of success, -1 otherwise
		 * *************************************************************************************************************
		 */
		int executeSetMotorNSH(void);

    private:
        string	m_strComponent;
        string	m_strMotor;
        string	m_strActivation;

};

#endif // OPSETMOTORACTIVATION_H
