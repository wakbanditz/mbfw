/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFCancelSection.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFCancelSection class.
 @details

 ****************************************************************************
*/

#include "WFCancelSection.h"
#include "OutCmdCancelSection.h"
#include "InCmdCancelSection.h"
#include "OpCancelSection.h"
#include "THRunProtocol.h"
#include "MainExecutor.h"

WFCancelSection::WFCancelSection()
{
}

WFCancelSection::~WFCancelSection()
{

}

bool WFCancelSection::checkAcceptance(int& liErrorCode)
{
	InCmdCancelSection* p = (InCmdCancelSection*)m_pIncomingCommand;

	eDeviceStatus sectionStatus;
	uint8_t section;

	section = p->getSection();

	if(section == SCT_A_ID)
		sectionStatus = stateMachineSingleton()->getDeviceStatus(eIdSectionA);
	else if(section == SCT_B_ID)
		sectionStatus = stateMachineSingleton()->getDeviceStatus(eIdSectionB);

	if(sectionStatus != eStatusProtocol)
	{
		log(LOG_ERR, "CANCELRUN: section %c status :0x%x", SECTION_CHAR_A + section, sectionStatus);
		liErrorCode = ERR_STATUS_NOT_CONGRUENT;
		return false;
	}

	// request protocol abort
	if(protocolThreadPointer()->abortProtocolFromCommand(section) == false)
	{
		log(LOG_ERR, "CANCELRUN: section %c abort failed", SECTION_CHAR_A + section);
		liErrorCode = ERR_STATUS_NOT_CONGRUENT;
		return false;
	}

	return true;
}

OutgoingCommand* WFCancelSection::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdCancelSection;
	return pCmd;
}

Operation* WFCancelSection::getOperation(void)
{
	OpCancelSection* pOp = new OpCancelSection;
	return (Operation *)pOp;
}
