/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSetMotorActivation.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSetMotorActivation class.
 @details

 ****************************************************************************
*/

#include "WFSetMotorActivation.h"
#include "InCmdSetMotorActivation.h"
#include "OutCmdSetMotorActivation.h"
#include "OpSetMotorActivation.h"
#include "WebServerAndProtocolInclude.h"

WFSetMotorActivation::WFSetMotorActivation()
{
}

WFSetMotorActivation::~WFSetMotorActivation()
{
	/* Nothing to do yet */
}

bool WFSetMotorActivation::checkAcceptance(int &liErrorCode)
{
	InCmdSetMotorActivation* p = (InCmdSetMotorActivation*)m_pIncomingCommand;

	string strComponent = p->getComponent();
	string strMotor = p->getMotor();

	if((strComponent != XML_TAG_SECTIONA_NAME) &&
		 (strComponent != XML_TAG_SECTIONB_NAME) &&
		 (strComponent != XML_TAG_NSH_NAME))
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	if((strMotor != XML_TAG_MOTOR_NAME_TOWER) && (strMotor != XML_TAG_MOTOR_NAME_PUMP) &&
		(strMotor != XML_TAG_MOTOR_NAME_NSH) && (strMotor != XML_TAG_MOTOR_NAME_TRAY) &&
		(strMotor != XML_TAG_MOTOR_NAME_SPR))
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	if ( strComponent == XML_TAG_SECTIONA_NAME )
	{
		m_deviceList.push_back(eIdSectionA);
	}
	if ( strComponent == XML_TAG_SECTIONB_NAME )
	{
		m_deviceList.push_back(eIdSectionB);
	}
	if ( strComponent == XML_TAG_NSH_NAME )
	{
		m_deviceList.push_back(eIdNsh);
	}

#ifdef __DEBUG_PROTOCOL
	return (liErrorCode == 0);
#endif

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSetMotorActivation::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSetMotorActivation;
	return pCmd;
}

Operation* WFSetMotorActivation::getOperation(void)
{
	InCmdSetMotorActivation* pInCmd = (InCmdSetMotorActivation*)m_pIncomingCommand;

	OpSetMotorActivation* pOp = new OpSetMotorActivation();

	pOp->setParams(pInCmd->getComponent(), pInCmd->getMotor(), pInCmd->getActivation());
	return pOp;
}
