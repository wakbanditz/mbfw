/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSHCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSHCalibrate class.
 @details

 ****************************************************************************
*/

#include "WFSHCalibrate.h"
#include "InCmdSHCalibrate.h"
#include "OutCmdSHCalibrate.h"
#include "OpSHCalibrate.h"

#include "SectionBoardGeneral.h"

#include "WebServerAndProtocolInclude.h"

WFSHCalibrate::WFSHCalibrate()
{

}

WFSHCalibrate::~WFSHCalibrate()
{

}

bool WFSHCalibrate::checkAcceptance(int& liErrorCode)
{
	InCmdSHCalibrate* p = (InCmdSHCalibrate*)m_pIncomingCommand;

	string strSection = p->getSection();
	string strSlot = p->getSlot();


	if ( strSection.compare(XML_VALUE_SECTION_A) && strSection.compare(XML_VALUE_SECTION_B) )
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	if ( strSlot.compare(XML_VALUE_STRIP_1) && strSlot.compare(XML_VALUE_STRIP_2) &&
		 strSlot.compare(XML_VALUE_STRIP_3) && strSlot.compare(XML_VALUE_STRIP_4) &&
		 strSlot.compare(XML_VALUE_STRIP_5) && strSlot.compare(XML_VALUE_STRIP_6) )
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

    if ( strSection.compare(XML_VALUE_SECTION_A) == 0 )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_A_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFShCalibrate: section %c door open", A_CHAR + SCT_A_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }
    else if ( strSection.compare(XML_VALUE_SECTION_B) == 0 )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_B_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFShCalibrate: section %c door open", A_CHAR + SCT_B_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }

	m_deviceList.push_back(eIdNsh);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSHCalibrate::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdSHCalibrate();
	return pCmd;
}

Operation* WFSHCalibrate::getOperation()
{
	InCmdSHCalibrate* pInCmd = (InCmdSHCalibrate*)m_pIncomingCommand;

	OpSHCalibrate* pOp = new OpSHCalibrate();
	pOp->setParams(pInCmd->getSection(), pInCmd->getSlot());

	return (Operation *)pOp;
}
