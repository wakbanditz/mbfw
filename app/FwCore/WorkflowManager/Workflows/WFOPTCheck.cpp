/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFOPTCheck.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFOPTCheck class.
 @details

 ****************************************************************************
*/

#include "WFOPTCheck.h"
#include "InCmdOPTCheck.h"
#include "OutCmdOPTCheck.h"
#include "OpOPTCheck.h"

#include "SectionBoardGeneral.h"

#include "WebServerAndProtocolInclude.h"

WFOPTCheck::WFOPTCheck()
{

}

WFOPTCheck::~WFOPTCheck()
{

}

bool WFOPTCheck::checkAcceptance(int& liErrorCode)
{
    if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled() )
    {
        liErrorCode = ERR_STATUS_NOT_CONGRUENT;
        return false;
    }

    InCmdOPTCheck* p = (InCmdOPTCheck*)m_pIncomingCommand;

    if(p->getTarget() == 0)
    {
        liErrorCode = ERR_COMMAND_PARAMETERS;
        return false;
    }

    bool bValid[SCT_NUM_TOT_SECTIONS];

    for (uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
    {
        bValid[section] = false;
        for (uint8_t slot = 0; slot < SCT_NUM_TOT_SLOTS; slot++)
        {
            if(p->isSlotEnabled(section, slot))
            {
                // the section has at least one slot enabled -> let's check the door status
                if(sectionBoardGeneralSingleton(section)->isDoorClosed() == false)
                {
                    log(LOG_ERR, "WFOptCheck: section %c door open", A_CHAR + section);
                    liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
                    return false;
                }
                bValid[section] = true;
                break;
            }
        }
    }

    if(bValid[SCT_A_ID] == false && bValid[SCT_B_ID] == false)
    {
        // no section-slot enabled
        liErrorCode = ERR_COMMAND_PARAMETERS;
        return false;
    }

    m_deviceList.push_back(eIdNsh);

    // check and interact with the StateMachine
    liErrorCode = setStateMachine(m_deviceList);

    return (liErrorCode == 0);
}

OutgoingCommand* WFOPTCheck::getOutgoingCommand()
{
    OutgoingCommand* pCmd = new OutCmdOPTCheck();
    return pCmd;
}

Operation* WFOPTCheck::getOperation()
{
    InCmdOPTCheck* p = (InCmdOPTCheck *)m_pIncomingCommand;

    OpOPTCheck* pOp = new OpOPTCheck();

    pOp->setParameters(p->getTarget(), p->getTolerance());

    uint8_t i, j;

    for(i = SCT_A_ID; i < SCT_NUM_TOT_SECTIONS; i++)
    {
        for(j = 0; j < SCT_NUM_TOT_SLOTS; j++)
        {
            if(p->isSlotEnabled(i, j))
            {
                pOp->setSlot(i, j);
            }
        }
    }

    return (Operation*)pOp;
}

