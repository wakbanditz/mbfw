/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetCounter.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the WFGetCounter class.
 @details

 ****************************************************************************
*/

#ifndef WFGETCOUNTER_H
#define WFGETCOUNTER_H

#include "Workflow.h"

class OutgoingCommand;
class Operation;

/*! ********************************************************************************************************************
 * @brief WFGetCounter	class derived from Workflow : refer to Workflow for redefined functions this class manages the
 *						workflow for the GETCOUNTER command
 * *********************************************************************************************************************
 */
class WFGetCounter : public Workflow
{
	public:
		/*! ***********************************************************************************************************
		 * @brief WFGetCounter	constructor
		 * ************************************************************************************************************
		 */
		WFGetCounter();

		/*! ***********************************************************************************************************
		 * @brief ~WFGetCounter	destructor
		 * ************************************************************************************************************
		 */
		virtual ~WFGetCounter();

		/*! ***********************************************************************************************************
		 * @brief checkAcceptance		routine to access the state machine and provide permits of execution of the
		 *								Workflow
		 * ************************************************************************************************************
		 */
		bool checkAcceptance(int& liErrorCode);

		/*! ***********************************************************************************************************
		 * @brief getOutgoingCommand	build the OutgoingCommand associated to the Workflow
		 * ************************************************************************************************************
		 */
		OutgoingCommand* getOutgoingCommand(void);

		/*! ***********************************************************************************************************
		 * @brief getOperation			build the Operationt associated to the Workflow
		 * ************************************************************************************************************
		 */
		Operation* getOperation(void);
};


#endif // WFGETCOUNTER_H
