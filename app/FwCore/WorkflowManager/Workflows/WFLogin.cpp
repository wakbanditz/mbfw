/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFLogin.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFLogin class.
 @details

 ****************************************************************************
*/

#include "WFLogin.h"
#include "OutCmdLogin.h"
#include "InCmdLogin.h"
#include "Operations/OpLogin.h"

WFLogin::WFLogin()
{
	m_strId.clear();
}

WFLogin::~WFLogin()
{

}

bool WFLogin::checkAcceptance(int& liErrorCode)
{
	std::string strPwd;
	InCmdLogin* p = (InCmdLogin*)m_pIncomingCommand;

	liErrorCode = 0;

	m_strId.assign(p->getId());
	strPwd.assign(p->getPwd());

	// Check Id and Password validity
	if(!MainExecutor::getInstance()->m_loginManager.checkLoginValid(m_strId, strPwd))
	{
		liErrorCode = ERR_LOGIN_NOT_VALID;
		return false;
	}

	// if there already is a connection of the same type  -> refuse it
	if(!MainExecutor::getInstance()->m_loginManager.isLoginAcceptable(m_pIncomingCommand->getUsage(),
															 m_pIncomingCommand->getCallbackUrl()))
	{
		liErrorCode = ERR_LOGIN_NOT_VALID;
		return false;
	}

	return (liErrorCode == 0);
}

OutgoingCommand* WFLogin::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdLogin;
	return pCmd;
}

Operation* WFLogin::getOperation(void)
{
	OpLogin* pOp = new OpLogin;
	pOp->setLoginUsage(m_pIncomingCommand->getUsage());

	return (Operation*)pOp;
}
