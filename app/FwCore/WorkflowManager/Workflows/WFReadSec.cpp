/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFReadSec.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFReadSec class.
 @details

 ****************************************************************************
*/

#include "WFReadSec.h"
#include "OutCmdReadSec.h"
#include "InCmdReadSec.h"
#include "OpReadSec.h"
#include "SectionBoardGeneral.h"
#include "SectionTimeCalc.h"
#include "CameraSettings.h"

WFReadSec::WFReadSec()
{
	m_pCameraSettings = nullptr;
}

WFReadSec::~WFReadSec()
{
	if ( m_pCameraSettings != nullptr )
	{
		delete(m_pCameraSettings);
		m_pCameraSettings = nullptr;
	}
}

bool WFReadSec::checkAcceptance(int& liErrorCode)
{
	InCmdReadSec* p = (InCmdReadSec*)m_pIncomingCommand;
	if ( p->getSection() == -1 )
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}
	else if ( ! p->isAtLeastOneStripEnabled() )
	{
		log(LOG_ERR, "WFReadSec::checkAcceptance: no strips enabled");
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	// Check for settings first
	m_pCameraSettings = new CameraSettings;
	if ( p->getSettings().size() )
	{
		vector<pair<string, string>> vSettings;
		vSettings = p->getSettings();

		MainExecutor::getInstance()->m_protocolValidator.parseXMLtoCameraSettings(vSettings, m_pCameraSettings);
	}

	if ( p->getUsage() == "SERVICE" && ! m_pCameraSettings->isScheduledReadSec(1) )
	{
		if ( p->getSection() == 0 )
		{
			m_deviceList.push_back(eIdSectionA);
		}
		else
		{
			m_deviceList.push_back(eIdSectionB);
		}

		m_deviceList.push_back(eIdCamera);
		m_deviceList.push_back(eIdNsh);

		// check and interact with the StateMachine
		liErrorCode = setStateMachine(m_deviceList);

		return (liErrorCode == 0);
	}
	else
	{
	#ifndef __DEBUG_PROTOCOL
		// Check the status of all the devices
		eDeviceId eDevice = eIdMax;
		eDevice = ( p->getSection() == SCT_A_ID ) ? eIdSectionA : eIdSectionB;
		eDeviceStatus eStatus = stateMachineSingleton()->getDeviceStatus(eDevice);
		if ( eStatus != eStatusIdle )
		{
			log(LOG_ERR, "WFReadSec::checkAcceptance: section %s status :%s", (eDevice==eIdSectionA)?"A":"B",
				stateMachineSingleton()->getDeviceStatusAsString(eDevice).c_str());
			liErrorCode = ERR_STATUS_NOT_CONGRUENT;
			return false;
		}

		// check also NSH status (that can be in Idle or in Protocol on the other section)
		eStatus = stateMachineSingleton()->getDeviceStatus(eIdNsh);
		if ( eStatus != eStatusIdle && eStatus != eStatusProtocol )
		{
			log(LOG_ERR, "WFReadSec::checkAcceptance: NSH status :%d", eStatus);
			liErrorCode = ERR_STATUS_NOT_CONGRUENT;
			return false;
		}

		// check also Camera status (that can be in Idle or in Protocol/ReadSec on the other section)
		eStatus = stateMachineSingleton()->getDeviceStatus(eIdCamera);
		if ( eStatus != eStatusIdle && eStatus != eStatusProtocol && eStatus != eStatusReadsec )
		{
			log(LOG_ERR, "WFReadSec::checkAcceptance: Camera status :%d", eStatus);
			liErrorCode = ERR_STATUS_NOT_CONGRUENT;
			return false;
		}

		// check the door section status
		if ( ! sectionBoardGeneralSingleton( p->getSection())->isDoorClosed() )
		{
			log(LOG_ERR, "WFReadSec::checkAcceptance: section %d door open", p->getSection());
			liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
			return false;
		}
	#endif

		// pass to infopointer all the parameters needed and copy them in the class
		readSecInfoPointer(p->getSection())->setCameraSettings(m_pCameraSettings);
		readSecInfoPointer(p->getSection())->setParameters(p->getDataStructure(), p->getSettings());

		// scheduling
		if ( ! scheduleInit() )
		{
			log(LOG_ERR, "WFReadSec::checkAcceptance: section %c error init scheduling", SECTION_CHAR_A + p->getSection());
			liErrorCode = ERR_PROTOCOL_SCHEDULER;
			return false;
		}

		int liDMNum = 0, liBCNum = 0, liSubstrateNum = 0, liSample0Num = 0, liSample3Num = 0;
		createTargetsList(p->getDataStructure(), &liDMNum, &liBCNum, &liSubstrateNum, &liSample0Num, &liSample3Num);
		if ( ! scheduleReadSec(p->getSection(), liDMNum, liBCNum, liSubstrateNum, liSample0Num, liSample3Num) )
		{
			log(LOG_ERR, "WFReadSec::checkAcceptance: section %c error scheduling", SECTION_CHAR_A + p->getSection());
			liErrorCode = ERR_PROTOCOL_SCHEDULER;
			return false;
		}

		if ( ! scheduleExecute() )
		{
			log(LOG_ERR, "WFReadSec::checkAcceptance: schedule execute failed");
			liErrorCode = ERR_PROTOCOL_SCHEDULER;
			return false;
		}

		uint8_t readSecId = 1;
		protocolThreadPointer()->perform(p->getSection(), readSecId);

		return true;
	}
}

bool WFReadSec::scheduleInit()
{
	t_ErrorData ErrorData;
	log(LOG_DEBUG, "WFReadSec::scheduleInit: init scheduling");
	ErrorData.iErrorCode = SCHED_SUCCESS;

	schedulerThreadPointer()->problemInit(ErrorData);
	if ( ErrorData.iErrorCode != SCHED_SUCCESS )
	{
		schedulerThreadPointer()->closeAndAbort();
		log(LOG_ERR, "WFReadSec::scheduleInit: Scheduler initialization error :%d", ErrorData.iErrorCode);
		return false;
	}
	log(LOG_DEBUG, "WFReadSec::scheduleInit: init scheduling -> Done");

	return true;
}

bool WFReadSec::scheduleReadSec(uint8_t section, int liDMNum, int liBCNum, int liSubstrateNum, int liSample0Num, int liSample3Num, bool isConstraint)
{
	// analyze the parameters and call accordingly the scheduler

	t_ErrorData ErrorData;
	struct precedence SLastPrec;
	int iFailureCode = SCHED_ERROR;

	log(LOG_INFO, "WFReadSec: start scheduling section %c", SECTION_CHAR_A + section);

	ErrorData.iErrorCode = SCHED_SUCCESS;
	SLastPrec = schedulerThreadPointer()->getLastPrec();

	// schedule the protocol Reading phases
	iFailureCode = schedulerThreadPointer()->addReadSecEvent(section, SLastPrec.section_id, SLastPrec.slot_id, ErrorData,
															 true, true, liDMNum, liBCNum, liSubstrateNum, liSample0Num, liSample3Num, 0);

	// consider the constraint
	if ( iFailureCode == SCHED_SUCCESS && isConstraint == true )
	{
		iFailureCode = schedulerThreadPointer()->addPrecedenceHead(section, (int64_t)D_MIN_ZERO, (int64_t)D_MAX_ZERO, ErrorData);
	}

	if ( iFailureCode == SCHED_SUCCESS )
	{
		log(LOG_INFO, "WFReadSec::scheduleReadSec: scheduling section %c completed", SECTION_CHAR_A + section);
	}
	else
	{
		log(LOG_ERR, "WFReadSec::scheduleReadSec: scheduling process error: %d", ErrorData.iErrorCode);
	}

	return ( iFailureCode == SCHED_SUCCESS );
}

void WFReadSec::createTargetsList(stripReadSection_tag* data, int* liDMNum, int* liBCNum, int* liSubstrateNum, int* liSample0Num, int* liSample3Num)
{
	if ( data == nullptr )			return;

	if ( liDMNum == nullptr )		return;
	else *liDMNum = 0;

	if ( liBCNum == nullptr )		return;
	else *liBCNum = 0;

	if ( liSample0Num == nullptr )	return;
	else *liSample0Num = 0;

	if ( liSample3Num == nullptr )	return;
	else *liSample3Num = 0;

	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( ! data[i].m_bEnabled )	continue;

		if ( data[i].m_bSprCheck )						(*liDMNum)++;
		if ( data[i].m_bStripCheck )					(*liBCNum)++;
		if ( data[i].m_bSubstrateCheck )				(*liSubstrateNum)++;
		if ( data[i].m_SampleDetection.m_bCheckX0 )		(*liSample0Num)++;
		if ( data[i].m_SampleDetection.m_bCheckX3 )		(*liSample3Num)++;
	}

	return;
}

bool WFReadSec::scheduleExecute()
{
	t_ErrorData ErrorData;

	log(LOG_INFO, "WFReadSec::scheduleExecute: closeAndRun");

	if ( schedulerThreadPointer()->closeAndRun(ErrorData) == -1 )
	{
		return false;
	}

	// save the EndTime of the section involved
	InCmdReadSec* p = (InCmdReadSec*)m_pIncomingCommand;
	uint64_t lEndTime = schedulerThreadPointer()->getSectionEndTime(p->getSection());
	readSecInfoPointer(p->getSection())->setLastTime(lEndTime);

	log(LOG_INFO, "WFReadSec::scheduleExecute: end section %c  = %s (%ld)", SECTION_CHAR_A + p->getSection(),
				runwlInfoPointer(p->getSection())->getLastTime().c_str(), lEndTime);

	return true;
}

OutgoingCommand* WFReadSec::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdReadSec;
	return pCmd;
}

Operation* WFReadSec::getOperation(void)
{
	OpReadSec* pOp = new OpReadSec;

	// transfer the info on procedure execution to the Operation
	InCmdReadSec* p = (InCmdReadSec *)m_pIncomingCommand;
	pOp->setParameters(p->getSection(), p->getDataStructure(), *m_pCameraSettings);
	protocolThreadPointer()->setOperation(p->getSection(), pOp);

	return (Operation *)pOp;
}
