/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetParameter.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the WFGetParameter class.
 @details

 ****************************************************************************
*/

#ifndef WFGETPARAMETER_H
#define WFGETPARAMETER_H

#include "Workflow.h"

class OutgoingCommand;
class Operation;

/*! ********************************************************************************************************************
 * @brief WFGetParameter WFMove	class derived from Workflow : refer to Workflow for redefined functions this class manages the
 *						workflow for the GETPARAMETER command
 * *********************************************************************************************************************
 */
class WFGetParameter : public Workflow
{
	public:

		/*! ***********************************************************************************************************
		 * @brief WFGetParameter	constructor
		 * ************************************************************************************************************
		 */
		WFGetParameter();

		/*! ***********************************************************************************************************
		 * @brief ~WFGetParameter	destructor
		 * ************************************************************************************************************
		 */
		virtual ~WFGetParameter();

		/*! ***********************************************************************************************************
		 * @brief checkAcceptance		routine to access the state machine and provide permits of execution of the
		 *								Workflow
		 * ************************************************************************************************************
		 */
		bool checkAcceptance(int& liErrorCode);

		/*! ***********************************************************************************************************
		 * @brief getOutgoingCommand	build the OutgoingCommand associated to the Workflow
		 * ************************************************************************************************************
		 */
		OutgoingCommand* getOutgoingCommand(void);

		/*! ***********************************************************************************************************
		 * @brief getOperation			build the Operation associated to the Workflow
		 * ************************************************************************************************************
		 */
		Operation* getOperation(void);
};

#endif // WFGETPARAMETER_H
