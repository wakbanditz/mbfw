/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSignalRead.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSignalRead class.
 @details

 ****************************************************************************
*/

#include "WFSignalRead.h"
#include "InCmdSignalRead.h"
#include "OutCmdSignalRead.h"
#include "OpSignalRead.h"
#include "WebServerAndProtocolInclude.h"

WFSignalRead::WFSignalRead()
{

}

WFSignalRead::~WFSignalRead()
{

}

bool WFSignalRead::checkAcceptance(int& liErrorCode)
{
	if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled() )	return false;

	m_deviceList.push_back(eIdNsh);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSignalRead::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdSignalRead();
	return pCmd;
}

Operation* WFSignalRead::getOperation()
{
	InCmdSignalRead* pInCmd = (InCmdSignalRead*)m_pIncomingCommand;

	OpSignalRead* pOp = new OpSignalRead();
	pOp->setParams(pInCmd->getRepetitions(), pInCmd->getInterval());

	return (Operation *)pOp;
}
