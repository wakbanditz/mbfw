/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFMove.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFMove class.
 @details

 ****************************************************************************
*/

#include "WFMove.h"
#include "InCmdMove.h"
#include "OutCmdMove.h"
#include "OpMove.h"

#include "SectionBoardGeneral.h"

#include "WebServerAndProtocolInclude.h"

WFMove::WFMove()
{
}

WFMove::~WFMove()
{
	/* Nothing to do yet */
}

bool WFMove::checkAcceptance(int &liErrorCode)
{
	InCmdMove* p = (InCmdMove*)m_pIncomingCommand;

	string strComponent = p->getComponent();
	string strMotor = p->getMotor();
	string strMove = p->getMove();

	if((strComponent != XML_TAG_SECTIONA_NAME)    && (strComponent != XML_TAG_SECTIONB_NAME)	&& (strComponent != XML_TAG_NSH_NAME))
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	if( (strMotor != XML_TAG_MOTOR_NAME_TOWER) && (strMotor != XML_TAG_MOTOR_NAME_PUMP)	&&
		(strMotor != XML_TAG_MOTOR_NAME_NSH) && (strMotor != XML_TAG_MOTOR_NAME_TRAY) &&
		(strMotor != XML_TAG_MOTOR_NAME_SPR))
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	if (((strMove != XML_TAG_MOVE_VALUE_HOME)       && (strMove != XML_TAG_MOVE_VALUE_ABSOLUTE) &&
		 (strMove != XML_TAG_MOVE_VALUE_PREDEFINED)	&& (strMove != XML_TAG_MOVE_VALUE_RELATIVE)))
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	if ( strComponent == XML_TAG_SECTIONA_NAME )
	{
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_A_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFMove: section %c door open", A_CHAR + SCT_A_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }
    if ( strComponent == XML_TAG_SECTIONB_NAME )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_B_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFMove: section %c door open", A_CHAR + SCT_B_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }

    if ( strComponent == XML_TAG_SECTIONA_NAME )
    {
        m_deviceList.push_back(eIdSectionA);
    }
	if ( strComponent == XML_TAG_SECTIONB_NAME )
	{
        m_deviceList.push_back(eIdSectionB);
    }
	if ( strComponent == XML_TAG_NSH_NAME )
	{
		m_deviceList.push_back(eIdNsh);
	}

#ifdef __DEBUG_PROTOCOL
	return (liErrorCode == 0);
#endif

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFMove::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdMove();
	return pCmd;
}

Operation* WFMove::getOperation(void)
{
	InCmdMove* p = (InCmdMove*)m_pIncomingCommand;


	Operation* pOp = new OpMove(p->getComponent(), p->getMotor(), p->getMove(), p->getMoveValue());
	return pOp;
}
