/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSetGlobalSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSetGlobalSettings class.
 @details

 ****************************************************************************
*/

#include "WFSetGlobalSettings.h"
#include "InCmdSetGlobalSettings.h"
#include "OutCmdSetGlobalSettings.h"
#include "OpSetGlobalSettings.h"
#include "WebServerAndProtocolInclude.h"

WFSetGlobalSettings::WFSetGlobalSettings()
{
	m_bSection[SCT_A_ID] = false;
	m_bSection[SCT_B_ID] = false;
	m_bInstrument = false;
}

WFSetGlobalSettings::~WFSetGlobalSettings()
{
	/* Nothing to do yet */
}

bool WFSetGlobalSettings::checkAcceptance(int &liErrorCode)
{
	uint8_t section;

	InCmdSetGlobalSettings* p = (InCmdSetGlobalSettings*)m_pIncomingCommand;

	liErrorCode = 0;

    // if the module is in PROTOCOL (it means at least one section is running a test) ->
    // reject command
    if(stateMachineSingleton()->getDeviceStatus(eIdModule) == eStatusProtocol)
    {
        liErrorCode = ERR_STATUS_NOT_CONGRUENT;
        return false;
    }

	for(section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		if(p->getSprTargetTemperature(section) != -1)
		{
			if(p->getSprTargetTemperature(section) == 0)
			{
				// disable control
			}
			else if((p->getSprMinTemperature(section) == -1) ||
				(p->getSprMaxTemperature(section) == -1))
			{
				liErrorCode = ERR_COMMAND_PARAMETERS;
				return false;
			}
			else if((p->getSprMinTemperature(section) >= p->getSprTargetTemperature(section)) ||
				(p->getSprMaxTemperature(section) <= p->getSprTargetTemperature(section)) ||
				(p->getSprMinTemperature(section) >= p->getSprMaxTemperature(section))	)
			{
				liErrorCode = ERR_COMMAND_PARAMETERS;
				return false;
			}
			m_bSection[section] = true;
		}

		if(p->getTrayTargetTemperature(section) != -1)
		{
			if(p->getTrayTargetTemperature(section) == 0)
			{
				// disable control
			}
			else if((p->getTrayMinTemperature(section) == -1) ||
				(p->getTrayMaxTemperature(section) == -1))
			{
				liErrorCode = ERR_COMMAND_PARAMETERS;
				return false;
			}
			else if((p->getTrayMinTemperature(section) >= p->getTrayTargetTemperature(section)) ||
				(p->getTrayMaxTemperature(section) <= p->getTrayTargetTemperature(section)) ||
				(p->getTrayMinTemperature(section) >= p->getTrayMaxTemperature(section))	)
			{
				liErrorCode = ERR_COMMAND_PARAMETERS;
				return false;
			}
			m_bSection[section] = true;
		}
	}

	if(p->getInternalMinTemperature() == -1 && p->getInternalMaxTemperature() == -1)
	{
		m_bInstrument = false;
	}
	else if((p->getInternalMaxTemperature() == -1) || (p->getInternalMinTemperature() == -1))
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}
	else if(p->getInternalMinTemperature() > p->getInternalMaxTemperature())
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}
	else
	{
		m_bInstrument = true;
	}

    // for this command no need to lock the state machine: the Operation will check the device status
    // and will apply the settings immediately if the devices are available or later (if they are in INIT)
    // .. so no need to restore stateMachine in the Operation
/*
	if(m_bInstrument)
	{
		m_deviceList.push_back(eIdModule);
	}
	if(m_bSection[SCT_A_ID])
	{
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
        {
			m_deviceList.push_back(eIdSectionA);
        }
	}
	if(m_bSection[SCT_B_ID])
	{
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
        {
			m_deviceList.push_back(eIdSectionB);
        }
	}

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);
*/
	return (liErrorCode == 0);
}

OutgoingCommand* WFSetGlobalSettings::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSetGlobalSettings;
	return pCmd;
}

Operation* WFSetGlobalSettings::getOperation(void)
{
	InCmdSetGlobalSettings* p = (InCmdSetGlobalSettings*)m_pIncomingCommand;

	OpSetGlobalSettings* pOp = new OpSetGlobalSettings();

	pOp->setInstrumentEnable(m_bInstrument);
	pOp->setSectionEnable(SCT_A_ID, m_bSection[SCT_A_ID]);
	pOp->setSectionEnable(SCT_B_ID, m_bSection[SCT_B_ID]);

	if(m_bInstrument)
	{
		pOp->setInternalMaxTemperature(p->getInternalMaxTemperature());
		pOp->setInternalMinTemperature(p->getInternalMinTemperature());
	}

	if(m_bSection[SCT_A_ID])
	{
		pOp->setSprMaxTemperature(SCT_A_ID, p->getSprMaxTemperature(SCT_A_ID));
		pOp->setSprMinTemperature(SCT_A_ID, p->getSprMinTemperature(SCT_A_ID));
		pOp->setSprTargetTemperature(SCT_A_ID, p->getSprTargetTemperature(SCT_A_ID));
		pOp->setSprToleranceTemperature(SCT_A_ID, p->getSprToleranceTemperature(SCT_A_ID));

		pOp->setTrayMaxTemperature(SCT_A_ID, p->getTrayMaxTemperature(SCT_A_ID));
		pOp->setTrayMinTemperature(SCT_A_ID, p->getTrayMinTemperature(SCT_A_ID));
		pOp->setTrayTargetTemperature(SCT_A_ID, p->getTrayTargetTemperature(SCT_A_ID));
		pOp->setTrayToleranceTemperature(SCT_A_ID, p->getTrayToleranceTemperature(SCT_A_ID));
	}

	if(m_bSection[SCT_B_ID])
	{
		pOp->setSprMaxTemperature(SCT_B_ID, p->getSprMaxTemperature(SCT_B_ID));
		pOp->setSprMinTemperature(SCT_B_ID, p->getSprMinTemperature(SCT_B_ID));
		pOp->setSprTargetTemperature(SCT_B_ID, p->getSprTargetTemperature(SCT_B_ID));
		pOp->setSprToleranceTemperature(SCT_B_ID, p->getSprToleranceTemperature(SCT_B_ID));

		pOp->setTrayMaxTemperature(SCT_B_ID, p->getTrayMaxTemperature(SCT_B_ID));
		pOp->setTrayMinTemperature(SCT_B_ID, p->getTrayMinTemperature(SCT_B_ID));
		pOp->setTrayTargetTemperature(SCT_B_ID, p->getTrayTargetTemperature(SCT_B_ID));
		pOp->setTrayToleranceTemperature(SCT_B_ID, p->getTrayToleranceTemperature(SCT_B_ID));
	}

	pOp->setForceTemperature(p->getForceTemperature());

	return (Operation *)pOp;
}

