/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetParameter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFGetPArameter class.
 @details

 ****************************************************************************
*/

#include "WFGetParameter.h"
#include "InCmdGetParameter.h"
#include "OutCmdGetParameter.h"
#include "OpGetParameter.h"
#include "WebServerAndProtocolInclude.h"

WFGetParameter::WFGetParameter()
{
}

WFGetParameter::~WFGetParameter()
{
	/* Nothing to do yet */
}

bool WFGetParameter::checkAcceptance(int &liErrorCode)
{
	InCmdGetParameter* p = (InCmdGetParameter*)m_pIncomingCommand;

	string strComponent = p->getComponent();
	string strMotor = p->getMotor();

	liErrorCode = 0;

	// check the devices involved
	if(strComponent.empty())
	{
		// in this case the request is on all components
		// if a device is disabled we exclude it from the action
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
			m_deviceList.push_back(eIdSectionA);
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
			m_deviceList.push_back(eIdSectionB);
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true)
			m_deviceList.push_back(eIdNsh);
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdCamera) == true)
			m_deviceList.push_back(eIdCamera);
	}
	else
	{
		if ( strComponent == XML_TAG_SECTIONA_NAME )
		{
			m_deviceList.push_back(eIdSectionA);
		}
		else if ( strComponent == XML_TAG_SECTIONB_NAME )
		{
			m_deviceList.push_back(eIdSectionB);
		}
		else if ( strComponent == XML_TAG_NSH_NAME )
		{
			m_deviceList.push_back(eIdNsh);
		}
		else if ( strComponent == XML_TAG_CAMERA_NAME )
		{
			m_deviceList.push_back(eIdCamera);
		}
	}

	if(m_deviceList.size() == 0)
	{
		// no valid device
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	// check the single motor
	if(!strMotor.empty())
	{
		if((strMotor != XML_TAG_MOTOR_NAME_TOWER) &&
			(strMotor != XML_TAG_MOTOR_NAME_PUMP) &&
			(strMotor != XML_TAG_MOTOR_NAME_NSH)  &&
			(strMotor != XML_TAG_MOTOR_NAME_TRAY) &&
			(strMotor != XML_TAG_MOTOR_NAME_SPR) )
		{
			liErrorCode = ERR_COMMAND_PARAMETERS;
			return false;
		}
	}

	/* this command does not require actions -->
	NO check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);
	*/

	return (liErrorCode == 0);
}

OutgoingCommand* WFGetParameter::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdGetParameter();
	return pCmd;
}

Operation* WFGetParameter::getOperation(void)
{
	InCmdGetParameter* p = (InCmdGetParameter*)m_pIncomingCommand;

	Operation* pOp = new OpGetParameter(p->getComponent(), p->getMotor());
	return pOp;
}
