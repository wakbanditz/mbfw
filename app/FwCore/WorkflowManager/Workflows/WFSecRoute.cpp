/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSecRoute.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSecRoute class.
 @details

 ****************************************************************************
*/

#include "WFSecRoute.h"
#include "OutCmdSecRoute.h"
#include "InCmdSecRoute.h"
#include "OpSecRoute.h"

WFSecRoute::WFSecRoute()
{
}

WFSecRoute::~WFSecRoute()
{

}

bool WFSecRoute::checkAcceptance(int& liErrorCode)
{
	InCmdSecRoute* p = (InCmdSecRoute*)m_pIncomingCommand;

	if(p->getSection() == -1)
	{
		// no section enabled for the ReadSec -> stop workflow
		return ERR_COMMAND_PARAMETERS;
	}

	if(p->getSection() == SCT_A_ID)
	{
		m_deviceList.push_back(eIdSectionA);
	}
	else if(p->getSection() == SCT_B_ID)
	{
		m_deviceList.push_back(eIdSectionB);
	}
	else if(p->getSection() == NSH_ID)
	{
		m_deviceList.push_back(eIdNsh);
	}
	else if(p->getSection() == CAMERA_ID)
	{
		m_deviceList.push_back(eIdCamera);
	}
	else if (p->getSection() == INSTRUMENT_ID)
	{
		m_deviceList.push_back(eIdModule);
	}

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSecRoute::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSecRoute;
	return pCmd;
}

Operation* WFSecRoute::getOperation(void)
{
	OpSecRoute* pOp = new OpSecRoute;

	// transfer the info on procedure execution to the Operation
	InCmdSecRoute* p = (InCmdSecRoute *)m_pIncomingCommand;
	pOp->setParameters(p->getSection(), p->getCommand() );

	return (Operation *)pOp;
}

