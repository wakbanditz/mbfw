/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFDisable.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFDisable class.
 @details

 ****************************************************************************
*/

#include "WFDisable.h"
#include "InCmdDisable.h"
#include "OpDisable.h"
#include "OutCmdDisable.h"

WFDisable::WFDisable()
{
	m_bDisable = false;
}

WFDisable::~WFDisable()
{

}

bool WFDisable::checkAcceptance(int& liErrorCode)
{
	InCmdDisable* p = (InCmdDisable*)m_pIncomingCommand;

	liErrorCode = 0;

	bool bDisable = false;
	bool bEnable = false;

	if(p->getDisableInstrument().compare(XML_VALUE_DISABLE) == 0)
	{
		// the whole instrument is disabled
		bDisable = true;
	}
	else if(p->getDisableInstrument().compare(XML_VALUE_ENABLE) == 0)
	{
		// the whole instrument is enabled
		bEnable = true;
	}
	else
	{
		// check the single devices
		if(p->getDisableNSH().compare(XML_VALUE_DISABLE) == 0)
		{
			bDisable = true;
		}
		else if(p->getDisableNSH().compare(XML_VALUE_ENABLE) == 0)
		{
			bEnable = true;
		}

		if(p->getDisableCamera().compare(XML_VALUE_DISABLE) == 0)
		{
			bDisable = true;
		}
		else if(p->getDisableCamera().compare(XML_VALUE_ENABLE) == 0)
		{
			bEnable = true;
		}

		if(p->getDisableSection(SCT_A_ID).compare(XML_VALUE_DISABLE) == 0)
		{
			bDisable = true;
		}
		else if(p->getDisableSection(SCT_A_ID).compare(XML_VALUE_ENABLE) == 0)
		{
			bEnable = true;
		}

		if(p->getDisableSection(SCT_B_ID).compare(XML_VALUE_DISABLE) == 0)
		{
			bDisable = true;
		}
		else if(p->getDisableSection(SCT_B_ID).compare(XML_VALUE_ENABLE) == 0)
		{
			bEnable = true;
		}
	}

	if(bEnable == bDisable)
	{
		// both settings at the same time ? not allowed
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	if(bDisable)
	{
		// try to disable
		if(p->getDisableInstrument().compare(XML_VALUE_DISABLE) == 0)
		{
			// whole instrument
			if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) != eStatusDisable)
				m_deviceList.push_back(eIdSectionA);
			if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) != eStatusDisable)
				m_deviceList.push_back(eIdSectionB);
			if(stateMachineSingleton()->getDeviceStatus(eIdNsh) != eStatusDisable)
				m_deviceList.push_back(eIdNsh);
			if(stateMachineSingleton()->getDeviceStatus(eIdCamera) != eStatusDisable)
				m_deviceList.push_back(eIdCamera);
		}
		else
		{
			// single devices
			if(p->getDisableNSH().compare(XML_VALUE_DISABLE) == 0)
			{
				if(stateMachineSingleton()->getDeviceStatus(eIdNsh) != eStatusDisable)
					m_deviceList.push_back(eIdNsh);
			}

			if(p->getDisableCamera().compare(XML_VALUE_DISABLE) == 0)
			{
				if(stateMachineSingleton()->getDeviceStatus(eIdCamera) != eStatusDisable)
					m_deviceList.push_back(eIdCamera);
			}

			if(p->getDisableSection(SCT_A_ID).compare(XML_VALUE_DISABLE) == 0)
			{
				if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) != eStatusDisable)
					m_deviceList.push_back(eIdSectionA);
			}

			if(p->getDisableSection(SCT_B_ID).compare(XML_VALUE_DISABLE) == 0)
			{
				if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) != eStatusDisable)
					m_deviceList.push_back(eIdSectionB);
			}
		}

	}
	else
	{
		// try to enable
		if(p->getDisableInstrument().compare(XML_VALUE_ENABLE) == 0)
		{
			// whole instrument
			if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusDisable)
				m_deviceList.push_back(eIdSectionA);
			if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusDisable)
				m_deviceList.push_back(eIdSectionB);
			if(stateMachineSingleton()->getDeviceStatus(eIdNsh) == eStatusDisable)
				m_deviceList.push_back(eIdNsh);
			if(stateMachineSingleton()->getDeviceStatus(eIdCamera) == eStatusDisable)
				m_deviceList.push_back(eIdCamera);
		}
		else
		{
			// single devices
			if(p->getDisableNSH().compare(XML_VALUE_ENABLE) == 0)
			{
				if(stateMachineSingleton()->getDeviceStatus(eIdNsh) == eStatusDisable)
					m_deviceList.push_back(eIdNsh);
			}

			if(p->getDisableCamera().compare(XML_VALUE_ENABLE) == 0)
			{
				if(stateMachineSingleton()->getDeviceStatus(eIdCamera) == eStatusDisable)
					m_deviceList.push_back(eIdCamera);
			}

			if(p->getDisableSection(SCT_A_ID).compare(XML_VALUE_ENABLE) == 0)
			{
				if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusDisable)
					m_deviceList.push_back(eIdSectionA);
			}

			if(p->getDisableSection(SCT_B_ID).compare(XML_VALUE_ENABLE) == 0)
			{
				if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusDisable)
					m_deviceList.push_back(eIdSectionB);
			}
		}
	}


	if(m_deviceList.size() == 0)
	{
		// no valid device identified
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	m_bDisable = bDisable;

	// the final state depends on the action
	std::string strState;
	if(m_bDisable)
	{
		strState = stateMachineSingleton()->getStatusAsString(eStatusDisable);
	}
	else
	{
		strState = stateMachineSingleton()->getStatusAsString(eStatusInit);
	}
	p->setTargetState(strState);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFDisable::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdDisable;
	return pCmd;
}

Operation* WFDisable::getOperation(void)
{
	OpDisable* pOp = new OpDisable;
	pOp->setAction(m_bDisable);
	return (Operation *)pOp;
}

