/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSaveCameraRoi.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSaveCameraRoi class.
 @details

 ****************************************************************************
*/

#include "WFSaveCameraRoi.h"
#include "InCmdSaveCameraRoi.h"
#include "OutCmdSaveCameraRoi.h"
#include "OpSaveCameraRoi.h"
#include "WebServerAndProtocolInclude.h"

WFSaveCameraRoi::WFSaveCameraRoi()
{

}

WFSaveCameraRoi::~WFSaveCameraRoi()
{

}

bool WFSaveCameraRoi::checkAcceptance(int& liErrorCode)
{
	if ( ! stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdCamera) )	return false;

	InCmdSaveCameraRoi* p = (InCmdSaveCameraRoi*)m_pIncomingCommand;

	vector<string> vTarget = p->getTarget();
	size_t liTopSize = p->getTop().size();
	size_t liBottomSize = p->getBottom().size();
	size_t liLeftSize = p->getLeft().size();
	size_t liRightSize = p->getRight().size();

	if ( (vTarget.size() != liTopSize) || (liTopSize != liBottomSize) ||
		 (liBottomSize != liLeftSize) || (liLeftSize != liRightSize) )
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	m_deviceList.push_back(eIdCamera);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSaveCameraRoi::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdSaveCameraRoi();
	return pCmd;
}

Operation* WFSaveCameraRoi::getOperation()
{
	InCmdSaveCameraRoi* pInCmd = (InCmdSaveCameraRoi*)m_pIncomingCommand;

	OpSaveCameraRoi* pOp = new OpSaveCameraRoi();
	pOp->setParams(pInCmd->getTarget(), pInCmd->getTop(), pInCmd->getBottom(),
				   pInCmd->getLeft(), pInCmd->getRight());

	return (Operation *)pOp;
}
