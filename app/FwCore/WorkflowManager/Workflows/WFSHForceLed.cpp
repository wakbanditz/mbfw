/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSHForceLed.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSHForceLed class.
 @details

 ****************************************************************************
*/

#include "WFSHForceLed.h"
#include "InCmdSHForceLed.h"
#include "OutCmdSHForceLed.h"
#include "OpSHForceLed.h"
#include "WebServerAndProtocolInclude.h"

WFSHForceLed::WFSHForceLed()
{

}

WFSHForceLed::~WFSHForceLed()
{

}

bool WFSHForceLed::checkAcceptance(int& liErrorCode)
{
	if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled() )	return false;

	InCmdSHForceLed* p = (InCmdSHForceLed*)m_pIncomingCommand;
	int liDuration = stoi(p->getDuration());
	if ( liDuration < 0 )	return false;

	m_deviceList.push_back(eIdNsh);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSHForceLed::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdSHForceLed();
	return pCmd;
}

Operation* WFSHForceLed::getOperation()
{
	InCmdSHForceLed* pInCmd = (InCmdSHForceLed*)m_pIncomingCommand;

	OpSHForceLed* pOp = new OpSHForceLed();
	pOp->setParam(pInCmd->getDuration());

	return (Operation *)pOp;
}
