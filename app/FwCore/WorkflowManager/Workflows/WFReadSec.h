/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFReadSec.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the WFReadSEc class.
 @details

 ****************************************************************************
*/

#ifndef WFREADSEC_H
#define WFREADSEC_H

#include "Workflow.h"

class OutgoingCommand;
class Operation;

/*! ***********************************************************************************************************
 * @brief WFReadSec	class derived from Workflow : refer to Workflow for redefined functions
 *					this class manages the workflow for the READSEC command
 * ************************************************************************************************************
 */

class WFReadSec : public Workflow
{
	public:

		/*! ***********************************************************************************************************
		 * @brief WFReadSec	constructor
		 * ************************************************************************************************************
		 */
		WFReadSec();

		/*! ***********************************************************************************************************
		 * @brief ~WFReadSec	destructor
		 * ************************************************************************************************************
		 */
		virtual ~WFReadSec();

		/*! ***********************************************************************************************************
		 * @brief checkAcceptance		routine to access the state machine and provide permits of execution of the
		 *								Workflow
		 * ************************************************************************************************************
		 */
		bool checkAcceptance(int& liErrorCode);

		/*! ***********************************************************************************************************
		 * @brief getOutgoingCommand	build the OutgoingCommand associated to the Workflow
		 * ************************************************************************************************************
		 */
		OutgoingCommand* getOutgoingCommand(void);

		/*! ***********************************************************************************************************
		 * @brief getOperation			build the Operationt associated to the Workflow
		 * ************************************************************************************************************
		 */
		Operation* getOperation(void);

	private:

		bool scheduleInit();
		bool scheduleExecute();
		bool scheduleReadSec(uint8_t section, int liDMNum, int liBCNum, int liSubstrateNum, int liSample0Num, int liSample3Num, bool isConstraint = false);
		bool calculateReadSec(uint8_t protoIndex, unsigned int*JgapsRel);
		void createTargetsList(stripReadSection_tag* data, int* liDMNum, int* liBCNum, int*liSubstrateNum, int* liSample0Num, int* liSample3Num);

	private:

		CameraSettings* m_pCameraSettings;

};

#endif // WFREADSEC_H
