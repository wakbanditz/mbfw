/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSetCounter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSetCounter class.
 @details

 ****************************************************************************
*/

#include "WFSetCounter.h"
#include "OutCmdSetCounter.h"
#include "InCmdSetCounter.h"
#include "OpSetCounter.h"
#include "MainExecutor.h"

WFSetCounter::WFSetCounter()
{
}

WFSetCounter::~WFSetCounter()
{

}

bool WFSetCounter::checkAcceptance(int& liErrorCode)
{
	liErrorCode = 0;

	InCmdSetCounter* p = (InCmdSetCounter*)m_pIncomingCommand;

	string strCounter = p->getCounter();
	string strValue = p->getValue();

	if((infoSingleton()->getInstrumentCounterIndexFromString(strCounter) == -1) ||
			(atoi(strValue.c_str()) < 0) )
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	// if a run is in progress the command cannot be executed
	if( (stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusProtocol) ||
		(stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusProtocol) )
	{
		liErrorCode = ERR_STATUS_NOT_CONGRUENT;
		return false;
	}

	/* this command does not require actions -->
	NO check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);
	*/

	return (liErrorCode == 0);
}

OutgoingCommand* WFSetCounter::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSetCounter;
	return pCmd;
}

Operation* WFSetCounter::getOperation(void)
{
	OpSetCounter* pOp = new OpSetCounter;

	InCmdSetCounter* p = (InCmdSetCounter*)m_pIncomingCommand;

	string strCounter = p->getCounter();
	string strValue = p->getValue();

	pOp->setCounter(strCounter);
	pOp->setValue(atoi(strValue.c_str()));

	return (Operation*)pOp;
}

