/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSleep.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSleep class.
 @details

 ****************************************************************************
*/

#include "WFSleep.h"
#include "InCmdSleep.h"
#include "OpSleep.h"
#include "OutCmdSleep.h"

WFSleep::WFSleep()
{

}

WFSleep::~WFSleep()
{

}

bool WFSleep::checkAcceptance(int& liErrorCode)
{
	liErrorCode = 0;

	InCmdSleep* p = (InCmdSleep*)m_pIncomingCommand;

	bool bSleep = p->getSleepInstrument();

	if(bSleep == false &&
			stateMachineSingleton()->getDeviceStatus(eIdModule) != eStatusSleep)
	{
		// exit from sleep but instrument is not sleeping
		liErrorCode = ERR_STATUS_NOT_CONGRUENT;
	}
	else if(bSleep == true &&
			stateMachineSingleton()->getDeviceStatus(eIdModule) == eStatusSleep)
	{
		// enter sleep but instrument is already sleeping
		liErrorCode = ERR_STATUS_NOT_CONGRUENT;
	}
	else
	{
		// the sleep is set for all devices	(unless they are disabled)
		if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) != eStatusDisable)
			m_deviceList.push_back(eIdSectionA);
		if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) != eStatusDisable)
			m_deviceList.push_back(eIdSectionB);
		if(stateMachineSingleton()->getDeviceStatus(eIdNsh) != eStatusDisable)
			m_deviceList.push_back(eIdNsh);
		if(stateMachineSingleton()->getDeviceStatus(eIdCamera) != eStatusDisable)
			m_deviceList.push_back(eIdCamera);
		m_deviceList.push_back(eIdModule);

		// the final state depends on the action
		std::string strState;

		if(bSleep)
		{
			strState = stateMachineSingleton()->getStatusAsString(eStatusSleep);
			p->setTargetState(strState);
		}
		else
		{
			strState = stateMachineSingleton()->getStatusAsString(eStatusInit);
			p->setTargetState(strState);
		}

		// check and interact with the StateMachine
		liErrorCode = setStateMachine(m_deviceList);
	}
	return (liErrorCode == 0);
}

OutgoingCommand* WFSleep::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSleep;
	return pCmd;
}

Operation* WFSleep::getOperation(void)
{
	InCmdSleep* p = (InCmdSleep*)m_pIncomingCommand;
	bool bSleep = p->getSleepInstrument();

	OpSleep* pOp = new OpSleep;
	pOp->setSleepActive(bSleep);

	return (Operation *)pOp;
}

