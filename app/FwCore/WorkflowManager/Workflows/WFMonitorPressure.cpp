/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFMonitorPressure.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFMonitorPressure class.
 @details

 ****************************************************************************
*/

#include "WFMonitorPressure.h"
#include "OutCmdMonitorPressure.h"
#include "InCmdMonitorPressure.h"
#include "OpMonitorPressure.h"
#include "THMonitorPressure.h"

WFMonitorPressure::WFMonitorPressure()
{
}

WFMonitorPressure::~WFMonitorPressure()
{

}

bool WFMonitorPressure::checkAcceptance(int& liErrorCode)
{
	InCmdMonitorPressure* p = (InCmdMonitorPressure*)m_pIncomingCommand;

	if(p->getSection() == SCT_NONE_ID || p->getAction() == MONITOR_PRESSURE_NONE)
	{
		// no section or action enabled for the Pressure -> stop workflow
		liErrorCode = ERR_MONITOR_NOT_ALLOWED;
		return false;
	}

	liErrorCode = 0;
	// to accept the comand let's check if the command is congruent
	int action = THMonitorPressure::getInstance()->getSectionAction(p->getSection());
	switch(action)
	{
		case MONITOR_PRESSURE_PROTOCOL_START :
			// the section is collecting data during a protocol : the action will have effect only on data sending
			liErrorCode = 0;
		break;

		case MONITOR_PRESSURE_START :
			if(p->getAction() != MONITOR_PRESSURE_STOP)
				liErrorCode = ERR_MONITOR_NOT_ALLOWED;
		break;

		case MONITOR_PRESSURE_STOP :
			if(p->getAction() != MONITOR_PRESSURE_START)
				liErrorCode = ERR_MONITOR_NOT_ALLOWED;
		break;

		default :
			liErrorCode = ERR_MONITOR_NOT_ALLOWED;
		break;
	}

/* as the command can be sent even during a protocol execution no interact with state machine
	if(liErrorCode != 0)
	{
		return false;
	}

	if(p->getSection() == SCT_A_ID)
	{
		m_deviceList.push_back(eIdSectionA);
	}
	else
	{
		m_deviceList.push_back(eIdSectionB);
	}

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);
*/

	return (liErrorCode == 0);
}

OutgoingCommand* WFMonitorPressure::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdMonitorPressure;
	return pCmd;
}

Operation* WFMonitorPressure::getOperation(void)
{
	OpMonitorPressure* pOp = new OpMonitorPressure;

	// transfer the info on procedure execution to the Operation
	InCmdMonitorPressure* p = (InCmdMonitorPressure *)m_pIncomingCommand;
	pOp->setParameters(p->getSection(), p->getAction());

	return (Operation *)pOp;
}

