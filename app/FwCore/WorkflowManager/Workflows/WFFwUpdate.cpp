/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFFwUpdate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFFwUpdate class.
 @details

 ****************************************************************************
*/

#include "WFFwUpdate.h"
#include "OutCmdFwUpdate.h"
#include "InCmdFwUpdate.h"
#include "OpFwUpdate.h"

WFFwUpdate::WFFwUpdate()
{

}

WFFwUpdate::~WFFwUpdate()
{

}

bool WFFwUpdate::checkAcceptance(int& liErrorCode)
{
	InCmdFwUpdate* p = (InCmdFwUpdate*)m_pIncomingCommand;

	vector<string> vComponents = p->getComponents();

	// check the devices involved
	if ( vComponents.empty() )
	{
		// in this case the request is on all components
		// if a device is disabled we exclude it from the action
		if ( stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true )
			m_deviceList.push_back(eIdSectionA);
		if ( stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true )
			m_deviceList.push_back(eIdSectionB);
		if ( stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true )
			m_deviceList.push_back(eIdNsh);
		if ( stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdCamera) == true )
			m_deviceList.push_back(eIdCamera);
	}
	else
	{
		for ( auto s : vComponents )
		{
			if ( ! s.compare(XML_TAG_SECTIONA_NAME) )
			{
                if ( stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true )
                    m_deviceList.push_back(eIdSectionA);
			}
			else if ( ! s.compare(XML_TAG_SECTIONB_NAME) )
			{
                if ( stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true )
                    m_deviceList.push_back(eIdSectionB);
			}
			else if ( ! s.compare(XML_TAG_NSH_NAME) )
			{
                if ( stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true )
                    m_deviceList.push_back(eIdNsh);
			}
			else if ( ! s.compare(XML_TAG_CAMERA_NAME) )
			{
                if ( stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdCamera) == true )
                    m_deviceList.push_back(eIdCamera);
			}
		}
	}

	if ( m_deviceList.size() == 0 )
	{
		// no valid device
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	createDataContainer();

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFFwUpdate::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdFwUpdate;
	return pCmd;
}

Operation* WFFwUpdate::getOperation()
{
	OpFwUpdate* pOp = new OpFwUpdate();
	pOp->setParamsFromCmd(&FwContainer);
	return (Operation*)pOp;
}

void WFFwUpdate::createDataContainer()
{
	InCmdFwUpdate* p = (InCmdFwUpdate*)m_pIncomingCommand;

	FwContainer.m_vComponents = p->getComponents();
	FwContainer.m_strCRC32.assign(p->getCRC());
	FwContainer.m_strUrl.assign(p->getUrl());
	FwContainer.m_strForce.assign(p->getVersioningCheck());
}
