/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSetFanSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSetFanSettings class.
 @details

 ****************************************************************************
*/

#include "WFSetFanSettings.h"
#include "InCmdSetFanSettings.h"
#include "OutCmdSetFanSettings.h"
#include "OpSetFanSettings.h"
#include "WebServerAndProtocolInclude.h"

WFSetFanSettings::WFSetFanSettings()
{
}

WFSetFanSettings::~WFSetFanSettings()
{
	/* Nothing to do yet */
}

bool WFSetFanSettings::checkAcceptance(int &liErrorCode)
{
	InCmdSetFanSettings* p = (InCmdSetFanSettings*)m_pIncomingCommand;

	liErrorCode = 0;

	std::pair<std::string, std::string> pairFan;

	for(uint8_t index = 0; ;index++)
	{
		pairFan = p->getFan(index);
		if(pairFan.first.empty() == true)
			break;

		if((pairFan.first != XML_TAG_SECTIONA_NAME) &&
			 (pairFan.first != XML_TAG_SECTIONB_NAME) &&
			 (pairFan.first != XML_TAG_INSTRUMENT_NAME))
		{
			liErrorCode = ERR_COMMAND_PARAMETERS;
			return false;
		}
	}

#ifdef __DEBUG_PROTOCOL
	return (liErrorCode == 0);
#endif

	// fan are managed directly by MasterBoard
	m_deviceList.push_back(eIdModule);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSetFanSettings::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSetFanSettings;
	return pCmd;
}

Operation* WFSetFanSettings::getOperation(void)
{
	InCmdSetFanSettings* p = (InCmdSetFanSettings*)m_pIncomingCommand;

	OpSetFanSettings* pOp = new OpSetFanSettings();

	std::pair<std::string, std::string> pairFan;

	for(uint8_t index = 0; ;index++)
	{
		pairFan = p->getFan(index);
		if(pairFan.first.empty() == true)
			break;

		pOp->setFan(pairFan.first, pairFan.second);
	}

	pOp->setDuration(p->getDuration());

	return (Operation *)pOp;
}


