/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFCaptureImage.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFCaptureImage class.
 @details

 ****************************************************************************
*/

#include "WFCaptureImage.h"
#include "OpCaptureImage.h"
#include "OutCmdCaptureImage.h"

#define MAX_IMAGE_NUM	2

WFCaptureImage::WFCaptureImage()
{
	m_vMotorTuple.resize(MAX_IMAGE_NUM);
	m_CameraParams.liPicturesNum = -1;
	m_CameraParams.vFormat.clear();
	m_CameraParams.vFocus.clear();
	m_CameraParams.vLight.clear();
	m_CameraParams.vQuality.clear();
	m_CameraParams.vrgCoordinates.clear();
    m_CameraParams.vPredefinedRoi.clear();
}

WFCaptureImage::~WFCaptureImage()
{

}

bool WFCaptureImage::checkAcceptance(int& liErrorCode)
{
	if ( ! stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdCamera) )	return false;

	InCmdCaptureImage* p = (InCmdCaptureImage*)m_pIncomingCommand;

	if ( p->getAction().empty() )
	{
		return false;
	}
	else if ( ! p->getAction().front().compare("STOP") )
	{
		// if it's just a stop command, no pictures have to be taken and no state machine has to be set
		return true;
	}
	else if ( p->getPicturesNum() > 2 )
	{
		return false;
	}

	for ( int i = 0; i < p->getPicturesNum(); i++ )
	{
		if ( p->getCoordinates()[i].liRight > MAX_WIDTH || p->getCoordinates()[i].liBottom > MAX_HEIGHT )
		{
			return false;
		}

		if ( p->getImageLight()[i] > 100 )
		{
			return false;
		}
		else if ( p->getImageQuality()[i] > 100 )
		{
			return false;
		}

		int liAttrSize = p->getAttribute()[i].size();
		int liMotorTypeSize = p->getMotor()[i].size();
		int liComponentSize = p->getComponent()[i].size();
		if ( ( liAttrSize != liMotorTypeSize ) || ( liMotorTypeSize != liComponentSize ) )
		{
			return false;
		}

		if ( p->getPicturesNum() > 1 )
		{
			if ( ! p->getAction()[i].compare("START") || ! p->getAction()[i].compare("STOP") )
			{
				return false;
			}
		}
	}

	// Create struct to organize all the data
	createTupleFromMember();
	createDataStruct();
	m_deviceList.push_back(eIdCamera);

	// If the predefinedRoi field is not empty, than also movement of the section and NSH are expected
	if ( ! p->getPredefinedRoi().front().empty() )
	{
		m_deviceList.push_back(eIdNsh);
		string strSection("");
		size_t pos = p->getPredefinedRoi().front().rfind("_");
		if ( pos != string::npos )
		{
			eDeviceId id = ( stoi(p->getPredefinedRoi().front().substr(pos+1)) > 6 ) ? eIdSectionA : eIdSectionB;
			m_deviceList.push_back(id);
		}
	}

	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFCaptureImage::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdCaptureImage();
	return pCmd;
}

Operation* WFCaptureImage::getOperation()
{
	OpCaptureImage* pOp = new OpCaptureImage();

	InCmdCaptureImage* p = (InCmdCaptureImage *)m_pIncomingCommand;
	pOp->setParameters(p->getAction()[0], m_vMotorTuple, m_CameraParams);

	return (Operation*)pOp;
}

void WFCaptureImage::createTupleFromMember(void)
{
	InCmdCaptureImage* p = (InCmdCaptureImage*)m_pIncomingCommand;

	for ( int i = 0; i < p->getPicturesNum(); ++i )
	{
		for ( size_t j = 0; j != p->getMotor()[i].size(); j++ )
		{
			auto newTuple = make_tuple(p->getMotor()[i][j], p->getComponent()[i][j], p->getAttribute()[i][j]);
			m_vMotorTuple[i].push_back(newTuple);
		}

		// The motors have to be moved in case of predefined ROI
		if ( ! p->getPredefinedRoi()[i].empty() && m_vMotorTuple[i].empty() )
		{
			char cCase = -1;
			if ( p->getPredefinedRoi()[i].find("Sample0") != string::npos )				cCase = 0;
			else if ( p->getPredefinedRoi()[i].find("Sample3") != string::npos )		cCase = 1;
			else if ( p->getPredefinedRoi()[i].find("BC") != string::npos )				cCase = 2;
			else if ( p->getPredefinedRoi()[i].find("DataMatrix") != string::npos )		cCase = 3;
			else if ( p->getPredefinedRoi()[i].find("ConeAbsence") != string::npos )	cCase = 4;

			string strTrayPos("");
			string strSprPos("Data_matrix");
			string strNshPos("Camera");
			if ( p->getPredefinedRoi()[i].find("CW") != string::npos )
				strNshPos += p->getPredefinedRoi()[i].substr(3); // remove CW_
			else
				strNshPos += p->getPredefinedRoi()[i];

			switch ( cCase )
			{
				case 0:
					strTrayPos.assign("W0");
				break;

				case 1:
					strTrayPos.assign("W3");
				break;

				case 2:
					strTrayPos.assign("Strip_BC");
				break;

				case 3:
					strTrayPos.assign("Strip_BC");
				break;

				case 4:
				{
					strTrayPos.assign("Strip_BC");
					strSprPos.assign("Cone_absence");
					string strSlotId("");
					for ( size_t j = 0; j < p->getPredefinedRoi()[i].size(); ++j )
					{
						if ( isdigit(p->getPredefinedRoi()[i].at(j)) )	strSlotId.push_back(p->getPredefinedRoi()[i].at(j));
					}
					strNshPos.assign("CameraDataMatrix_"+strSlotId);
				}
				break;

				default:	break;
			}

			string strSection("");
			size_t pos = p->getPredefinedRoi()[i].rfind("_");
			if ( pos != string::npos )
			{
				strSection = ( stoi(p->getPredefinedRoi()[i].substr(pos+1)) > 6 ) ? "SectionB" : "SectionA";
			}

			m_vMotorTuple[i].push_back(make_tuple(SECTION_TRAY_STRING, strSection, strTrayPos));
			m_vMotorTuple[i].push_back(make_tuple(SECTION_SPR_STRING, strSection, strSprPos));
			m_vMotorTuple[i].push_back(make_tuple(XML_TAG_NSH_NAME, XML_TAG_NSH_NAME, strNshPos));
		}
	}

	return;
}

void WFCaptureImage::createDataStruct(void)
{
	InCmdCaptureImage* p = (InCmdCaptureImage*)m_pIncomingCommand;

	m_CameraParams.liPicturesNum = p->getPicturesNum();
	m_CameraParams.vFormat = p->getImageFormat(); // they should be equal
	m_CameraParams.vFocus = p->getImageFocus();
	m_CameraParams.vLight = p->getImageLight();
	m_CameraParams.vQuality = p->getImageQuality();
	m_CameraParams.vrgCoordinates = p->getCoordinates();
    m_CameraParams.vPredefinedRoi = p->getPredefinedRoi();
	m_CameraParams.vCropEnabled = p->getCropEnabled();
}
