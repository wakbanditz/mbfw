/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetFanSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFGetFanSettings class.
 @details

 ****************************************************************************
*/

#include "WFGetFanSettings.h"
#include "InCmdGetFanSettings.h"
#include "OutCmdGetFanSettings.h"
#include "OpGetFanSettings.h"
#include "WebServerAndProtocolInclude.h"

WFGetFanSettings::WFGetFanSettings()
{
}

WFGetFanSettings::~WFGetFanSettings()
{
	/* Nothing to do yet */
}

bool WFGetFanSettings::checkAcceptance(int &liErrorCode)
{
	// this command can always be executed
	liErrorCode = 0;

	return (liErrorCode == 0);
}

OutgoingCommand* WFGetFanSettings::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdGetFanSettings;
	return pCmd;
}

Operation* WFGetFanSettings::getOperation(void)
{
	OpGetFanSettings* pOp = new OpGetFanSettings();

	return (Operation *)pOp;
}


