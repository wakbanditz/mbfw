/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFLogout.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFLogout class.
 @details

 ****************************************************************************
*/

#include "WFLogout.h"
#include "OutCmdLogout.h"
#include "InCmdLogout.h"
#include "Operations/OpLogout.h"

WFLogout::WFLogout()
{
}

WFLogout::~WFLogout()
{

}

bool WFLogout::checkAcceptance(int& liErrorCode)
{
	// this command can always be executed
	liErrorCode = 0;
	return (liErrorCode == 0);
}

OutgoingCommand* WFLogout::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdLogout;
	return pCmd;
}

Operation* WFLogout::getOperation(void)
{
	Operation* pOp = new OpLogout;
	return pOp;
}
