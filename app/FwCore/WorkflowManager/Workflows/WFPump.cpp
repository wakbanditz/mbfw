/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFPump.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFPump class.
 @details

 ****************************************************************************
*/

#include "WFPump.h"
#include "InCmdPump.h"
#include "OutCmdPump.h"
#include "OpPump.h"

#include "SectionBoardGeneral.h"

#include "WebServerAndProtocolInclude.h"

static std::string convertVolumeToCommand(uint16_t volume);

WFPump::WFPump()
{
}

WFPump::~WFPump()
{
    /* Nothing to do yet */
}

bool WFPump::checkAcceptance(int &liErrorCode)
{
    InCmdPump* p = (InCmdPump*)m_pIncomingCommand;

    string strComponent = p->getComponent();
    string strAction = p->getAction();
    uint16_t volume = p->getVolume();

    if((strComponent != XML_VALUE_SECTION_A) && (strComponent != XML_VALUE_SECTION_B))
    {
        liErrorCode = ERR_COMMAND_PARAMETERS;
        return false;
    }

    if( (strAction != XML_TAG_PUMP_ACTION_ASPIRATE) && (strAction != XML_TAG_PUMP_ACTION_DISPENSE))
    {
        liErrorCode = ERR_COMMAND_PARAMETERS;
        return false;
    }

    // check the volume corresponds to a valid one
    if(convertVolumeToCommand(volume).empty() == true)
    {
        log(LOG_ERR, "WFPump: volume %d not corresponding", volume);
        liErrorCode = ERR_COMMAND_PARAMETERS;
        return false;
    }

    if ( strComponent == XML_VALUE_SECTION_A )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_A_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFPump: section %c door open", A_CHAR + SCT_A_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }
    else if ( strComponent == XML_VALUE_SECTION_B )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_B_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFPump: section %c door open", A_CHAR + SCT_B_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }

    if ( strComponent == XML_VALUE_SECTION_A )
    {
        m_deviceList.push_back(eIdSectionA);
    }
    else if ( strComponent == XML_VALUE_SECTION_B )
    {
        m_deviceList.push_back(eIdSectionB);
    }

#ifdef __DEBUG_PROTOCOL
    return (liErrorCode == 0);
#endif

    // check and interact with the StateMachine
    liErrorCode = setStateMachine(m_deviceList);

    return (liErrorCode == 0);
}

OutgoingCommand* WFPump::getOutgoingCommand(void)
{
    OutgoingCommand* pCmd = new OutCmdPump();
    return pCmd;
}

Operation* WFPump::getOperation(void)
{
    InCmdPump* p = (InCmdPump*)m_pIncomingCommand;


    Operation* pOp = new OpPump(p->getComponent(), p->getAction(), convertVolumeToCommand(p->getVolume()));
    return pOp;
}

std::string convertVolumeToCommand(uint16_t volume)
{
    std::string strVolume;

    std::string fileName, strRead;
    std::ifstream inFile;

    strVolume.clear();

    fileName.assign(CONF_PUMP_VOLUMES);

    inFile.open(fileName);
    if(inFile.fail())
    {
        return strVolume;
    }

    while(!inFile.eof())
    {
        getline(inFile, strRead);
        if(!strRead.empty())
        {
            // lines beginning with # are comments
            if(strRead.front() != '#')
            {
                // split the line separating the fields
                std::vector<std::string> listStr;

                listStr = splitString(strRead, HASHTAG_SEPARATOR);
                if(listStr.size() == 2)
                {
                    // split is valid: compare values
                    if( (uint16_t)atoi(listStr.at(0).c_str()) == volume)
                    {
                        strVolume.assign(trimString(listStr.at(1)));
                        break;
                    }
                }
            }
        }
    }
    inFile.close();
    return strVolume;
}
