/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFShutdown.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFShutdown class.
 @details

 ****************************************************************************
*/

#include "WFShutdown.h"
#include "OutCmdShutdown.h"
#include "InCmdShutdown.h"
#include "OpShutdown.h"

WFShutdown::WFShutdown()
{
}

WFShutdown::~WFShutdown()
{

}

bool WFShutdown::checkAcceptance(int& liErrorCode)
{
	liErrorCode = 0;

	if(stateMachineSingleton()->getDeviceStatus(eIdModule) == eStatusProtocol)
	{
		liErrorCode = ERR_STATUS_NOT_CONGRUENT;
		return false;
	}

	// it's the shutdown -> no need to interact with state machine

	return (liErrorCode == 0);
}

OutgoingCommand* WFShutdown::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdShutdown;
	return pCmd;
}

Operation* WFShutdown::getOperation(void)
{
	OpShutdown* pOp = new OpShutdown;
	return (Operation*)pOp;
}

