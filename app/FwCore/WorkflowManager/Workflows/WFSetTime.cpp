/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSetTime.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSetTime class.
 @details

 ****************************************************************************
*/

#include "WFSetTime.h"
#include "OutCmdSetTime.h"
#include "InCmdSetTime.h"
#include "OpSetTime.h"

WFSetTime::WFSetTime()
{
}

WFSetTime::~WFSetTime()
{

}

bool WFSetTime::checkAcceptance(int& liErrorCode)
{
	InCmdSetTime* p = (InCmdSetTime*)m_pIncomingCommand;

	if(p->getTimeString().empty())
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	int year, month, day, hour, minute, seconds;

	if(sscanf(p->getTimeString().c_str(), "%04d-%02d-%02dT%02d:%02d:%02d",
			  &year, &month, &day, &hour, &minute, &seconds) != 6)
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	m_deviceList.push_back(eIdModule);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSetTime::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSetTime;
	return pCmd;
}

Operation* WFSetTime::getOperation(void)
{
	InCmdSetTime* p = (InCmdSetTime*)m_pIncomingCommand;

	OpSetTime* pOp = new OpSetTime;
	pOp->setTimeString(p->getTimeString());

	return (Operation *)pOp;
}

