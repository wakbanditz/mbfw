/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSSCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSSCalibrate class.
 @details

 ****************************************************************************
*/

#include "WFSSCalibrate.h"
#include "OutCmdSSCalibrate.h"
#include "OpSSCalibrate.h"
#include "WebServerAndProtocolInclude.h"

WFSSCalibrate::WFSSCalibrate()
{

}

WFSSCalibrate::~WFSSCalibrate()
{

}

bool WFSSCalibrate::checkAcceptance(int& liErrorCode)
{
	m_deviceList.push_back(eIdNsh);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSSCalibrate::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdSSCalibrate();
	return pCmd;
}

Operation* WFSSCalibrate::getOperation()
{
	Operation* pOp = new OpSSCalibrate();
	return pOp;
}
