/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFRunwl.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFRunwl class.
 @details

 ****************************************************************************
*/

#include "WFRunwl.h"
#include "OutCmdRunwl.h"
#include "InCmdRunwl.h"
#include "OpRunwl.h"
#include "MainExecutor.h"
#include "SectionBoardManager.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardPIB.h"
#include "ProtocolInfo.h"
#include "PressureSettings.h"
#include "SectionTimeCalc.h"

WFRunwl::WFRunwl()
{
}

WFRunwl::~WFRunwl()
{
	for(uint8_t i = 0; i < m_pPressureSettings.size(); i++)
	{
		SAFE_DELETE(m_pPressureSettings.at(i));
	}
	m_pPressureSettings.clear();
}

bool WFRunwl::checkAcceptance(int& liErrorCode)
{
	uint8_t section, counter;
	uint8_t protocol[SCT_NUM_TOT_SECTIONS] = {SCT_NONE_ID, SCT_NONE_ID};

#ifndef __DEBUG_PROTOCOL
	eDeviceStatus sectionStatus = eStatusNull;
#endif

	InCmdRunwl* p = (InCmdRunwl*)m_pIncomingCommand;

	liErrorCode = 0;


	// check if the section included in command is available
	for(section = SCT_A_ID, counter = 0; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		if(p->isSectionEnabled(section) == false) continue;
		++counter;

		// check at least one strip of the section is set to run
		if(p->isAtLeastOneStripEnabled(section) == false)
		{
			log(LOG_ERR, "RUNWL: no strips enabled");
			liErrorCode = ERR_COMMAND_PARAMETERS;
			return false;
		}

#ifndef __DEBUG_PROTOCOL
		if(section == SCT_A_ID)
			sectionStatus = stateMachineSingleton()->getDeviceStatus(eIdSectionA);
		else if(section == SCT_B_ID)
			sectionStatus = stateMachineSingleton()->getDeviceStatus(eIdSectionB);
		if(sectionStatus != eStatusIdle)
		{
            if(section == SCT_A_ID)
                log(LOG_ERR, "RUNWL: section A status :%s", stateMachineSingleton()->getDeviceStatusAsString(eIdSectionA).c_str());
            else if(section == SCT_B_ID)
                log(LOG_ERR, "RUNWL: section B status :%s", stateMachineSingleton()->getDeviceStatusAsString(eIdSectionB).c_str());
            liErrorCode = ERR_STATUS_NOT_CONGRUENT;
			return false;
		}
#endif

#ifndef __DEBUG_PROTOCOL
		// check the door section status
		if(sectionBoardGeneralSingleton(section)->isDoorClosed() == false)
		{
			log(LOG_ERR, "RUNWL: section %d door open", section);
			liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
			return false;
		}
#endif
	}

	if(counter == 0)
	{
		// no section to start
		log(LOG_ERR, "RUNWL: no sections enabled");
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

#ifndef __DEBUG_PROTOCOL
	// check also NSH status (that can be in Idle or in Protocol/ReadSec on the other section)
	sectionStatus = stateMachineSingleton()->getDeviceStatus(eIdNsh);
	if(sectionStatus != eStatusIdle && sectionStatus != eStatusProtocol &&
	   sectionStatus != eStatusReadsec)
	{
		log(LOG_ERR, "RUNWL: NSH status :%d", sectionStatus);
		liErrorCode = ERR_STATUS_NOT_CONGRUENT;
		return false;
	}
#endif

	// check if the protocol linked to the section is correct
	for(section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		if(p->isSectionEnabled(section) == false) continue;

		std::string strID, strProtoID;

		strID = p->getSectionProtoId(section);

		if(strID.empty())
		{
			// section protocol not defined
			liErrorCode = ERR_COMMAND_PARAMETERS;
			return false;
		}

		for(uint8_t j = 0; j < SCT_NUM_TOT_SECTIONS; j++)
		{
			strProtoID = p->getProtocolInfoId(j);

			if(!strProtoID.empty())
			{
				if(strID.compare(strProtoID.c_str()) == 0)
				{
					protocol[section] = j;
					break;
				}
			}
		}

		if(protocol[section] == SCT_NONE_ID)
		{
			// section protocol not defined
			log(LOG_ERR, "RUNWL: protocol not defined");
			liErrorCode = ERR_COMMAND_PARAMETERS;
			return false;
		}

/*		// check that the Proto parameters are correct -> no more necessary
        uint8_t jCnt;
        for(jCnt = 0; jCnt < J_MAXMIN_VECTOR_SIZE; jCnt++)
        {
            if(p->getProtoMinMaxJ(protocol[section], jCnt, 0) != 0 &&
                p->getProtoMinMaxJ(protocol[section], jCnt, 1) != 0)
            {
                // at least one Reading is defined
                break;
            }
        }

        if(jCnt == J_MAXMIN_VECTOR_SIZE)
        {
            log(LOG_ERR, "RUNWL: proto readings not correct ");
            liErrorCode = ERR_COMMAND_PARAMETERS;
            return false;
        } */

        char strParams1[MAX_PROTOCOL_GLOBALS_LEN + 1];
		char strParams2[MAX_PROTOCOL_LOCALS_LEN + 1];

		if(p->getProtoGlobals(protocol[section], strParams1) == false ||
				p->getProtoLocals(protocol[section], strParams2) == false)
		{
			log(LOG_ERR, "RUNWL: locals/globals not correct ");
			liErrorCode = ERR_COMMAND_PARAMETERS;
			return false;
		}

		// decode and check if the constraint string is correct
		if(p->decodeConstraint(section) == false)
		{
			log(LOG_ERR, "RUNWL: constraint not correct ");
			liErrorCode = ERR_COMMAND_PARAMETERS;
			return false;
		}
	}

	// check if there are PressureSettings to decode from XML
	for(uint8_t i = 0; i < SCT_NUM_TOT_SLOTS; i++)
	{
		std::string strID;
		std::string strSettings;

		if(p->getPressureSettings(i, &strID, &strSettings) == false)
			break;	// no more elements

		PressureSettings * pPressureSettings = 0;
		// let's'create a new PressureSetting element to be filled
		if(MainExecutor::getInstance()->m_protocolValidator.parseXMLtoPressureSettings(strSettings, pPressureSettings) == true)
		{
			if(pPressureSettings)
			{
				pPressureSettings->setID(strID);
				m_pPressureSettings.push_back(pPressureSettings);
			}
		}
	}


	// associate PressureSettings to strip
	for(section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		if(p->isSectionEnabled(section) == false) continue;

		for(uint8_t strip = 0; strip < SCT_NUM_TOT_SLOTS; strip ++)
		{
			if(p->getSectionStripSettings(section, strip).empty() == true)
				continue;

			std::string strSettings;
			strSettings.assign(p->getSectionStripSettings(section, strip));

			PressureSettings * pPressureSettings;

			// the strip needs a specific setting -> let's check if it exist in the list just built
			uint8_t cnt;
			for(cnt = 0; cnt < m_pPressureSettings.size(); cnt++)
			{
				std::string strID;

				pPressureSettings = m_pPressureSettings.at(cnt);

				strID.assign(pPressureSettings->getID());
				if(strID.compare(strSettings) == 0)
				{
					break;
				}
			}

			if(cnt == m_pPressureSettings.size())
			{
				log(LOG_ERR, "RUNWL: pressure settings not associated");
				liErrorCode = ERR_COMMAND_PARAMETERS;
				return false;
			}

			// copy the PressureSettings to the permanent object
			pressureSettingsPointer(section, strip)->copyPressureSettings(pPressureSettings);
		}
	}


	// syntax checks are passed
    uint32_t JgapsRel[SCT_NUM_TOT_SECTIONS][J_MAXMIN_VECTOR_SIZE];

	for(section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		if(p->isSectionEnabled(section) == false) continue;

		// copy the protocol info to the permanent object

		// protocol linked to section A -> position 0, section B -> position 1
		protocolInfoPointer(section)->copyProtocolInfo(p->getProtocolInfoPointer(protocol[section]));

		// copy the runwl data from the temporary object to the working one
		runwlInfoPointer(section)->clearAllData();
		runwlInfoPointer(section)->copyRunwlCommandInfo(p->getRunwlPointer(section));

		// calculate reading time from the protocol string
        memset(JgapsRel[section], 0, sizeof(JgapsRel[section]));

        if(calculateRunwl(section, JgapsRel[section]) == false)
		{
			log(LOG_ERR, "RUNWL: section %d timing", section);
			liErrorCode = ERR_PROTOCOL_TIMING;
			return false;
		}
        runwlInfoPointer(section)->buildJtoPerform(JgapsRel[section]);

#ifndef __DEBUG_PROTOCOL
		// send protocol to the section
		if(sendRunwl(section, section) == false)
		{
			log(LOG_ERR, "RUNWL: section %d transfer ", section);
			liErrorCode = ERR_PROTOCOL_TRANSFER;
			return false;
		}
#endif
	}


	// and now .... Scheduling
	if(scheduleInit() == false)
	{
		log(LOG_ERR, "RUNWL: section %c scheduling", SECTION_CHAR_A + section);
		liErrorCode = ERR_PROTOCOL_SCHEDULER;
		return false;
	}

	for(section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		if(p->isSectionEnabled(section) == false) continue;

        if(scheduleRunwl(section, JgapsRel[section],
						 runwlInfoPointer(section)->getMinimumConstraint(),
						 runwlInfoPointer(section)->getMaximumConstraint(),
						 runwlInfoPointer(section)->getValidConstraint()) == false)
		{
			log(LOG_ERR, "RUNWL: section %d scheduling", section);
			liErrorCode = ERR_PROTOCOL_SCHEDULER;
			return false;
		}
	}

	if(scheduleExecute() == false)
	{
        log(LOG_ERR, "RUNWL: schedule execute failed");
		liErrorCode = ERR_PROTOCOL_SCHEDULER;
		return false;
	}

	// scheduler starts -> start the protocol thread
	for(section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		if(p->isSectionEnabled(section) == false) continue;

		uint8_t protoId = 0;
		protocolThreadPointer()->perform(section, protoId);
	}

	return true;
}

OutgoingCommand* WFRunwl::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdRunwl;
	return pCmd;
}

Operation* WFRunwl::getOperation(void)
{
	InCmdRunwl* p = (InCmdRunwl*)m_pIncomingCommand;

	OpRunwl* pOp = new OpRunwl;

    // the RUN reply has to be "persistent" so set the flag in the Operation
    pOp->setPersistent(true);

	for(uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		if(p->isSectionEnabled(section) == false) continue;

		protocolThreadPointer()->setOperation(section, pOp);
	}

	return (Operation *)pOp;
}

bool WFRunwl::scheduleInit()
{
	t_ErrorData ErrorData;

    log(LOG_DEBUG, "RUNWL: init scheduling");

	ErrorData.iErrorCode = SCHED_SUCCESS;

	schedulerThreadPointer()->problemInit(ErrorData);

    if ( ErrorData.iErrorCode != SCHED_SUCCESS )
	{
		schedulerThreadPointer()->closeAndAbort();
		log(LOG_ERR, "RUNWL: Scheduler initialization error :%d", ErrorData.iErrorCode);
		return false;
	}
    log(LOG_DEBUG, "RUNWL: init scheduling -> Done");
    return true;
}

bool WFRunwl::scheduleRunwl(uint8_t section, uint32_t * JgapsRel, uint32_t minDelay, uint32_t maxDelay, bool isConstraint)
{
	// analyze the parameters and call accordingly the scheduler
	// -------------------------------------------------------------------------------------
	t_ErrorData ErrorData;
	struct precedence SLastPrec;

	int iFailureCode = SCHED_ERROR;

	log(LOG_INFO, "RUNWL: start scheduling section %c", SECTION_CHAR_A + section);

	ErrorData.iErrorCode = SCHED_SUCCESS;

	SLastPrec = schedulerThreadPointer()->getLastPrec();

	// schedule the protocol Reading phases
	iFailureCode = schedulerThreadPointer()->addSectionReadEvent(section,
																	   SLastPrec.section_id,
																	   SLastPrec.slot_id,
																	   ErrorData,
																	   JgapsRel,
																	   true,
																	   minDelay,
																	   maxDelay,
																	   0);

	// consider the constraint
	if (iFailureCode == SCHED_SUCCESS && isConstraint == true)
	{
		iFailureCode = schedulerThreadPointer()->addPrecedenceHead(section, minDelay, maxDelay, ErrorData);
	}

	if ( iFailureCode == SCHED_SUCCESS )
	{
		log(LOG_INFO, "RUNWL: scheduling section %c completed", SECTION_CHAR_A + section);
	}
	else
	{
		log(LOG_ERR, "RUNWL: scheduling process error :%d", ErrorData.iErrorCode);
	}


	return (iFailureCode == SCHED_SUCCESS);
}

bool WFRunwl::scheduleExecute()
{
	t_ErrorData ErrorData;

    log(LOG_INFO, "RUNWL: closeAndRun");

    if(schedulerThreadPointer()->closeAndRun(ErrorData) == -1)
    {
        return false;
    }

    // save the EndTime of each section involved
    InCmdRunwl* p = (InCmdRunwl*)m_pIncomingCommand;

    for(uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
    {
        if(p->isSectionEnabled(section) == false) continue;

        uint64_t lEndTime = schedulerThreadPointer()->getSectionEndTime(section);
        runwlInfoPointer(section)->setLastTime(lEndTime);

        log(LOG_INFO, "RUNWL: end section %c  = %s (%ld)", SECTION_CHAR_A + section,
                    runwlInfoPointer(section)->getLastTime().c_str(), lEndTime);
    }
    return true;
}

bool WFRunwl::calculateRunwl(uint8_t protoIndex, unsigned int * JgapsRel)
{
	char	strGlobals[MAX_PROTOCOL_GLOBALS_LEN + 1];
	char	strLocals[MAX_PROTOCOL_LOCALS_LEN + 1];

	// get the Locals and Globals string of the selected protocol
	protocolInfoPointer(protoIndex)->getProtoLocals(strLocals);
	protocolInfoPointer(protoIndex)->getProtoGlobals(strGlobals);

	SectionTimeCalc * pSectionTimeCalc = new SectionTimeCalc();
	bool bRet = pSectionTimeCalc->processProtocol(strLocals, strGlobals, JgapsRel);
	SAFE_DELETE(pSectionTimeCalc);

	return bRet;
}

bool WFRunwl::sendRunwl(uint8_t section, uint8_t protoIndex)
{
	char	strGlobals[MAX_PROTOCOL_GLOBALS_LEN + 1];
	char	strLocals[MAX_PROTOCOL_LOCALS_LEN + 1];

	// get the Locals and Globals string of the selected protocol
	protocolInfoPointer(protoIndex)->getProtoLocals(strLocals);
	protocolInfoPointer(protoIndex)->getProtoGlobals(strGlobals);

	// compose the protocol string : globals@locals
	std::string strProtocol;
	strProtocol.assign(strGlobals);
	strProtocol.append("@");
	strProtocol.append(strLocals);

	if(sectionBoardGeneralSingleton(section)->sendProtocolString(strProtocol))
	{
        // error sending protocol to section -> generates correponding error on section
        sectionBoardGeneralSingleton(section)->setEventToDecode('M', '0');
		return false;
	}

    int errSection = 0;

    RunwlInfo * pRunwl = runwlInfoPointer(section);

    if(pRunwl)
    {
        // send the strip configuration to enable/disable the pressure check per each well
        errSection += sectionBoardGeneralSingleton(section)->sendStripCheck(
                                                                pRunwl->getStripCheck(0), pRunwl->getStripCheck(1),
                                                                pRunwl->getStripCheck(2), pRunwl->getStripCheck(3),
                                                                pRunwl->getStripCheck(4), pRunwl->getStripCheck(5) );

        if(errSection == 0)
        {
            // send the strip layout to set the fill/empty status per each well
            errSection += sectionBoardGeneralSingleton(section)->sendStripLayout(
                                                              pRunwl->getStripProfile(0), pRunwl->getStripProfile(1),
                                                              pRunwl->getStripProfile(2), pRunwl->getStripProfile(3),
                                                              pRunwl->getStripProfile(4), pRunwl->getStripProfile(5) );
        }

        if(errSection == 0)
        {
            // send the pressure raw data acquisition flag per each strip
            errSection += sectionBoardManagerSingleton(section)->m_pSectionLink->m_pPIB->enableProtocolPressure(
                    pRunwl->getProtoTreatedPressure(0), pRunwl->getProtoTreatedPressure(1),
                    pRunwl->getProtoTreatedPressure(2), pRunwl->getProtoTreatedPressure(3),
                    pRunwl->getProtoTreatedPressure(4), pRunwl->getProtoTreatedPressure(5));
        }

        if(errSection == 0)
        {
            // send the pressure raw data sending rate per each strip
            errSection += sectionBoardGeneralSingleton(section)->setPressureSendingRate(
                    pRunwl->getProtoTreatedPressure(0), pRunwl->getProtoTreatedPressure(1),
                    pRunwl->getProtoTreatedPressure(2), pRunwl->getProtoTreatedPressure(3),
                    pRunwl->getProtoTreatedPressure(4), pRunwl->getProtoTreatedPressure(5));
        }

        // send to each strip the Pressure Settings
        for(uint8_t strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
        {
            if(pRunwl->getStripId(strip).empty() == false)
            {
                // the strip is enabled
                if(errSection == 0)
                {
                    errSection += sendPressureSettings(section, strip);
                }
            }
        }
    }

    return (errSection == 0);
}

int WFRunwl::sendPressureSettings(uint8_t section, uint8_t strip)
{
    PressureSettings * pPressure;
    pPressure = pressureSettingsPointer(section, strip);

    if(pPressure == 0) return 1;

    int error = 0;


    std::string strAlgo;
    int16_t intAlgo;

    strAlgo.assign(pPressure->getAlgorithm());
    if(strAlgo.compare(CLINIC_ALGO_STRING) == 0)
    {
        intAlgo = CLINIC_ALGO_INT;
    }
    else if(strAlgo.compare(INDUSTRY_ALGO_STRING) == 0)
    {
        intAlgo = INDUSTRY_ALGO_INT;
    }
    else if(strAlgo.compare(VETERINARY_ALGO_STRING) == 0)
    {
        intAlgo = VETERINARY_ALGO_INT;
    }
    else
    {
        return 1;
    }


    // send the values for all parameters ...
    error += sectionBoardGeneralSingleton(section)->sendStripPressureSettings(strip,
                                        ALGORITHM_SELECTION_PARAMETER, intAlgo);
    msleep(10);
    error += sectionBoardGeneralSingleton(section)->sendStripPressureSettings(strip,
                                        AREA_RATIO_CLINIC_PARAMETER, pPressure->getAreaRatio());
    msleep(10);
    error += sectionBoardGeneralSingleton(section)->sendStripPressureSettings(strip,
                                        MIN_SAMPLE_POS_CLINIC_PARAMETER, pPressure->getMinimumPositionPercentage());
    msleep(10);
    error += sectionBoardGeneralSingleton(section)->sendStripPressureSettings(strip,
                                        SLOPE_PERCENTAGE_CLINIC_PARAMETER, pPressure->getFinalSlopePercentage());
    msleep(10);
    error += sectionBoardGeneralSingleton(section)->sendStripPressureSettings(strip,
                                        SLOPE_THRESHOLD_CLINIC_PARAMETER, pPressure->getFinalSlopeThreshold());
    msleep(10);
    error += sectionBoardGeneralSingleton(section)->sendStripPressureSettings(strip,
                                        LOW_THRESHOLD_INDUSTRY_PARAMETER, pPressure->getIndustryThresholdLow());
    msleep(10);
    error += sectionBoardGeneralSingleton(section)->sendStripPressureSettings(strip,
                                        HIGH_THRESHOLD_INDUSTRY_PARAMETER, pPressure->getIndustryThresholdHigh());
    msleep(10);
    error += sectionBoardGeneralSingleton(section)->sendStripPressureSettings(strip,
                                        CYCLES_PERCENTAGE_INDUSTRY_PARAMETER, pPressure->getIndustryCyclesPercentage());
    msleep(10);
    error += sectionBoardGeneralSingleton(section)->sendStripPressureSettings(strip,
                                        CYCLES_MINIMUM_INDUSTRY_PARAMETER, pPressure->getIndustryCyclesMinimum());
    msleep(10);
    error += sectionBoardGeneralSingleton(section)->sendStripPressureSettings(strip,
                                        CYCLES_THRESHOLD_INDUSTRY_PARAMETER, pPressure->getIndustryCyclesThreshold());

    // .. and then the thresholds defined
    uint8_t index = 0;
    char speed, volume;
    uint64_t low, high;

    while(true)
    {
        if(pPressure->getSpeedVolumeThresholds(index, &speed, &volume, &low, &high) == false)
            break;

        msleep(10);
        error += sectionBoardGeneralSingleton(section)->sendStripPressureThresholds(strip, volume, speed, low, high);
        index++;
    }

    return error;
}
