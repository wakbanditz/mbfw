/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFOPTCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFOPTCalibrate class.
 @details

 ****************************************************************************
*/

#include "WFOPTCalibrate.h"
#include "InCmdOPTCalibrate.h"
#include "OutCmdOPTCalibrate.h"
#include "OpOPTCalibrate.h"

#include "SectionBoardGeneral.h"

#include "WebServerAndProtocolInclude.h"

WFOPTCalibrate::WFOPTCalibrate()
{

}

WFOPTCalibrate::~WFOPTCalibrate()
{

}

bool WFOPTCalibrate::checkAcceptance(int& liErrorCode)
{
    if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled() )
    {
        liErrorCode = ERR_STATUS_NOT_CONGRUENT;
        return false;
    }

	InCmdOPTCalibrate* p = (InCmdOPTCalibrate*)m_pIncomingCommand;

	string strSection = p->getSection();
	string strSlot = p->getSlot();
	// No control on target string for the moment

	if ( strSection.compare(XML_VALUE_SECTION_A) && strSection.compare(XML_VALUE_SECTION_B) )
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

    if ( strSection.compare(XML_VALUE_SECTION_A) == 0 )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_A_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFOptCalibrate: section %c door open", A_CHAR + SCT_A_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }
    else if ( strSection.compare(XML_VALUE_SECTION_B) == 0 )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_B_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFOptCalibrate: section %c door open", A_CHAR + SCT_B_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }


	if ( strSlot.compare(XML_VALUE_STRIP_1) && strSlot.compare(XML_VALUE_STRIP_2) &&
		 strSlot.compare(XML_VALUE_STRIP_3) && strSlot.compare(XML_VALUE_STRIP_4) &&
		 strSlot.compare(XML_VALUE_STRIP_5) && strSlot.compare(XML_VALUE_STRIP_6) )
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	m_deviceList.push_back(eIdNsh);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFOPTCalibrate::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdOPTCalibrate();
	return pCmd;
}

Operation* WFOPTCalibrate::getOperation()
{
	OpOPTCalibrate* pOp = new OpOPTCalibrate();
	InCmdOPTCalibrate* p = (InCmdOPTCalibrate *)m_pIncomingCommand;
	pOp->setParameters(p->getSection(), p->getSlot(), p->getTarget());

	return (Operation*)pOp;
}
