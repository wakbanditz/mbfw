/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFInit.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFInit class.
 @details

 ****************************************************************************
*/

#include "WFInit.h"
#include "InCmdInit.h"
#include "OutCmdInit.h"
#include "OpInit.h"
#include "WebServerAndProtocolInclude.h"

WFInit::WFInit()
{
}

WFInit::~WFInit()
{
	/* Nothing to do yet */
}

bool WFInit::checkAcceptance(int &liErrorCode)
{
	InCmdInit* p = (InCmdInit*)m_pIncomingCommand;

	string strComponent = p->getComponent();


	if ( (strComponent != XML_TAG_INSTRUMENT_NAME ) && (strComponent != XML_TAG_NSH_NAME ) &&
		 (strComponent != XML_TAG_SECTIONA_NAME) && (strComponent != XML_TAG_SECTIONB_NAME) &&
		 (strComponent != XML_TAG_CAMERA_NAME ))
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	if (strComponent == XML_TAG_INSTRUMENT_NAME)
	{
		m_deviceList.push_back(eIdSectionA);
		m_deviceList.push_back(eIdSectionB);
		m_deviceList.push_back(eIdNsh);
		m_deviceList.push_back(eIdCamera);
	}
	else if (strComponent == XML_TAG_SECTIONA_NAME)
	{
		m_deviceList.push_back(eIdSectionA);
	}
	else if (strComponent == XML_TAG_SECTIONB_NAME)
	{
		m_deviceList.push_back(eIdSectionB);
	}
	else if (strComponent == XML_TAG_NSH_NAME)
	{
		m_deviceList.push_back(eIdNsh);
	}
	else if (strComponent == XML_TAG_CAMERA_NAME)
	{
		m_deviceList.push_back(eIdCamera);
	}

	// check and interact with the StateMachine
    // NOTE for the INIT command is allowed to override the device lock
    // this means that the device must be in the allowed state but even
    // if the device is already locked the action is allowed
    // (the INIT shall be able to recover the jam situation)
    liErrorCode = setStateMachine(m_deviceList, true);

	return (liErrorCode == 0);
}

OutgoingCommand* WFInit::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdInit();
	return pCmd;
}

Operation* WFInit::getOperation(void)
{
	InCmdInit* pInCmd = (InCmdInit*)m_pIncomingCommand;

	OpInit* pOp = new OpInit();

	pOp->setComponent(pInCmd->getComponent());
	return pOp;
}
