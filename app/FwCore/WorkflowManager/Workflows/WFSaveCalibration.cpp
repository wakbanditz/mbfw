/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSaveCalibration.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSaveCalibration class.
 @details

 ****************************************************************************
*/

#include "WFSaveCalibration.h"
#include "InCmdSaveCalibration.h"
#include "OutCmdSaveCalibration.h"
#include "OpSaveCalibration.h"

#include "SectionBoardGeneral.h"

#include "WebServerAndProtocolInclude.h"
#include "CommonInclude.h"

WFSaveCalibration::WFSaveCalibration()
{
}

WFSaveCalibration::~WFSaveCalibration()
{
	/* Nothing to do yet */
}

bool WFSaveCalibration::checkAcceptance(int &liErrorCode)
{
	uint16_t usCntSectionA = 0;
	uint16_t usCntSectionB = 0;
	uint16_t usCntNSH = 0;

	InCmdSaveCalibration* p = (InCmdSaveCalibration*)m_pIncomingCommand;
	vector<structSaveCalibration> vecSaveCal;

	p->getCalibrationToSave(vecSaveCal);

	for (size_t i = 0; i < vecSaveCal.size(); i++)
	{
		structSaveCalibration & vecSaveTmp = vecSaveCal.at(i);

		if (vecSaveTmp.strComponent == XML_TAG_SECTIONA_NAME)
		{
			usCntSectionA++;
		}
		else if (vecSaveTmp.strComponent == XML_TAG_SECTIONB_NAME)
		{
			usCntSectionB++;
		}
		else if (vecSaveTmp.strComponent == XML_TAG_NSH_NAME)
		{
			usCntNSH++;
		}
		else
		{
			liErrorCode = ERR_COMMAND_PARAMETERS;
			return false;
		}

		// check the single motor
		if ((vecSaveTmp.strMotor != XML_TAG_MOTOR_NAME_TOWER) &&
			(vecSaveTmp.strMotor != XML_TAG_MOTOR_NAME_PUMP) &&
			(vecSaveTmp.strMotor != XML_TAG_MOTOR_NAME_TRAY) &&
			(vecSaveTmp.strMotor != XML_TAG_MOTOR_NAME_SPR) &&
			(vecSaveTmp.strMotor != XML_TAG_MOTOR_NAME_NSH) )
		{
			liErrorCode = ERR_COMMAND_PARAMETERS;
			return false;
		}
	}

    if ( usCntSectionA > 0 )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_A_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFSaveCalibration: section %c door open", A_CHAR + SCT_A_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }
    if ( usCntSectionB > 0 )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_B_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFSaveCalibration: section %c door open", A_CHAR + SCT_B_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }

	//select section to lock
	if (usCntSectionA > 0 && stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
	{
		m_deviceList.push_back(eIdSectionA);
	}

	if (usCntSectionB > 0 && stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
	{
		m_deviceList.push_back(eIdSectionB);
	}

	if (usCntNSH > 0 && stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdNsh) == true)
	{
		m_deviceList.push_back(eIdNsh);
	}

#ifdef __DEBUG_PROTOCOL
	return (liErrorCode == 0);
#endif

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSaveCalibration::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSaveCalibration();
	return pCmd;
}

Operation* WFSaveCalibration::getOperation(void)
{
	vector<structSaveCalibration> vecSaveCal;

	InCmdSaveCalibration* pInCmd = (InCmdSaveCalibration*)m_pIncomingCommand;
	pInCmd->getCalibrationToSave(vecSaveCal);

	OpSaveCalibration* pOp = new OpSaveCalibration();
	pOp->setValueCalibration(vecSaveCal);

	return pOp;
}
