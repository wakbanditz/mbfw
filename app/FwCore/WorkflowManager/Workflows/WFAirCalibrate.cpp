/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFAirCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFAirCalibrate class.
 @details

 ****************************************************************************
*/

#include "WFAirCalibrate.h"
#include "InCmdAirCalibrate.h"
#include "OutCmdAirCalibrate.h"
#include "OpAirCalibrate.h"
#include "WebServerAndProtocolInclude.h"

#define MIN_AIR_VAL_ACCEPTABLE	1
#define MAX_AIR_VAL_ACCEPTABLE	9

WFAirCalibrate::WFAirCalibrate()
{

}

WFAirCalibrate::~WFAirCalibrate()
{

}

bool WFAirCalibrate::checkAcceptance(int& liErrorCode)
{
	if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled() )	return false;

	InCmdAirCalibrate* p = (InCmdAirCalibrate*)m_pIncomingCommand;

	string strTarget = p->getTarget();
	int liTargetVal = stoi(strTarget);
	if ( ( liTargetVal < MIN_AIR_VAL_ACCEPTABLE ) || ( liTargetVal > MAX_AIR_VAL_ACCEPTABLE ) )
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	m_deviceList.push_back(eIdNsh);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFAirCalibrate::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdAirCalibrate();
	return pCmd;
}

Operation* WFAirCalibrate::getOperation()
{
	OpAirCalibrate* pOp = new OpAirCalibrate();
	InCmdAirCalibrate* p = (InCmdAirCalibrate *)m_pIncomingCommand;
	pOp->setParameter(p->getTarget());

	return (Operation*)pOp;
}
