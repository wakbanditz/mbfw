/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFVidasEp.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFVidasEp class.
 @details

 ****************************************************************************
*/

#include "WFVidasEp.h"
#include "OutCmdVidasEp.h"
#include "InCmdVidasEp.h"
#include "Operations/OpVidasEp.h"

WFVidasEp::WFVidasEp()
{
}

WFVidasEp::~WFVidasEp()
{

}

bool WFVidasEp::checkAcceptance(int& liErrorCode)
{
	liErrorCode = 0;

	// the GetSensor does not change the StateMachine so that it can be processed together with other commands
	// m_deviceList.push_back(eIdModule);
	// liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFVidasEp::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdVidasEp;
	return pCmd;
}

Operation* WFVidasEp::getOperation(void)
{
	Operation* pOp = new OpVidasEp;
	return pOp;
}
