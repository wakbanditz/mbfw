/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetCameraRoi.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFGetCameraRoi class.
 @details

 ****************************************************************************
*/

#include "WFGetCameraRoi.h"
#include "InCmdGetCameraRoi.h"
#include "OutCmdGetCameraRoi.h"
#include "OpGetCameraRoi.h"
#include "WebServerAndProtocolInclude.h"

WFGetCameraRoi::WFGetCameraRoi()
{
}

WFGetCameraRoi::~WFGetCameraRoi()
{

}

bool WFGetCameraRoi::checkAcceptance(int& liErrorCode)
{
	liErrorCode = 0;

	if ( ! stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdCamera) )	return false;

	m_deviceList.push_back(eIdCamera);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFGetCameraRoi::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdGetCameraRoi();
	return pCmd;
}

Operation* WFGetCameraRoi::getOperation()
{
	InCmdGetCameraRoi* pInCmd = (InCmdGetCameraRoi*)m_pIncomingCommand;

	OpGetCameraRoi* pOp = new OpGetCameraRoi();
	pOp->setParams(pInCmd->getTargets());

	return (Operation*)pOp;
}

