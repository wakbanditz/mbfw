/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFAutoCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFAutoCalibrate class.
 @details

 ****************************************************************************
*/

#include "WFAutoCalibrate.h"
#include "OutCmdAutoCalibrate.h"
#include "OpAutoCalibrate.h"
#include "WebServerAndProtocolInclude.h"

WFAutoCalibrate::WFAutoCalibrate()
{

}

WFAutoCalibrate::~WFAutoCalibrate()
{

}

bool WFAutoCalibrate::checkAcceptance(int& liErrorCode)
{
	if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled() )	return false;

	m_deviceList.push_back(eIdNsh);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFAutoCalibrate::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdAutoCalibrate();
	return pCmd;
}

Operation* WFAutoCalibrate::getOperation()
{
	OpAutoCalibrate* pOp = new OpAutoCalibrate();
	return (Operation*)pOp;
}
