/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFTrayRead.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFTrayRead class.
 @details

 ****************************************************************************
*/

#include "WFTrayRead.h"
#include "InCmdTrayRead.h"
#include "OpTrayRead.h"
#include "OutCmdTrayRead.h"

#include "SectionBoardGeneral.h"

WFTrayRead::WFTrayRead()
{

}

WFTrayRead::~WFTrayRead()
{

}

bool WFTrayRead::checkAcceptance(int& liErrorCode)
{
	InCmdTrayRead* p = (InCmdTrayRead*)m_pIncomingCommand;

	uint8_t section = p->getSection();

	if ( ( section != SCT_A_ID ) && ( section != SCT_B_ID) )
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

    if ( section == SCT_A_ID )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_A_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFTrayRead: section %c door open", A_CHAR + SCT_A_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }
    else if ( section == SCT_B_ID )
    {
        // check the door section status
        if(sectionBoardGeneralSingleton(SCT_B_ID)->isDoorClosed() == false)
        {
            log(LOG_ERR, "WFTrayRead: section %c door open", A_CHAR + SCT_B_ID);
            liErrorCode = ERR_PROTOCOL_DOOR_OPEN;
            return false;
        }
    }

    if( section == SCT_A_ID )
    {
        m_deviceList.push_back(eIdSectionA);
    }
    else
    {
        m_deviceList.push_back(eIdSectionB);
    }

	return true;
}

OutgoingCommand* WFTrayRead::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdTrayRead;
	return pCmd;
}

Operation* WFTrayRead::getOperation(void)
{
	InCmdTrayRead* p = (InCmdTrayRead*)m_pIncomingCommand;

	OpTrayRead* pOp = new OpTrayRead;
	pOp->setSection(p->getSection());
	return (Operation *)pOp;
}
