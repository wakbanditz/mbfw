/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFRunwl.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the WFRunwl class.
 @details

 ****************************************************************************
*/

#ifndef WFRUNWL_H
#define WFRUNWL_H

#include <string>
#include <vector>

#include "Workflow.h"

class OutgoingCommand;
class Operation;
class PressureSettings;

/*! ***********************************************************************************************************
 * @brief WFRunwl	class derived from Workflow : refer to Workflow for redefined functions
 *					this class manages the workflow for the RUNWL command
 * ************************************************************************************************************
 */

class WFRunwl : public Workflow
{
	public:

		/*! ***********************************************************************************************************
		 * @brief WFRunwl	constructor
		 * ************************************************************************************************************
		 */
		WFRunwl();

		/*! ***********************************************************************************************************
		 * @brief ~WFRunwl	destructor
		 * ************************************************************************************************************
		 */
		virtual ~WFRunwl();

		/*! ***********************************************************************************************************
		 * @brief checkAcceptance		routine to access the state machine and provide permits of execution of the
		 *								Workflow
		 * ************************************************************************************************************
		 */
		bool checkAcceptance(int& liErrorCode);

		/*! ***********************************************************************************************************
		 * @brief getOutgoingCommand	build the OutgoingCommand associated to the Workflow
		 * ************************************************************************************************************
		 */
		OutgoingCommand* getOutgoingCommand(void);

		/*! ***********************************************************************************************************
		 * @brief getOperation			build the Operation associated to the Workflow
		 * ************************************************************************************************************
		 */
		Operation* getOperation(void);

	private:


		/*! ***********************************************************************************************************
		 * @brief scheduleInit		initialize the schedule
		 * @return true if succesfull, false otherwise
		 * ************************************************************************************************************
		 */
		bool scheduleInit();

		/*! ***********************************************************************************************************
		 * @brief scheduleExecute	run the schedule process
		 * @return true if succesfull, false otherwise
		 * ************************************************************************************************************
		 */
		bool scheduleExecute();

		/*! ***********************************************************************************************************
		 * @brief scheduleRunwl		schedule the protocol for the section
		 * @param section the section index
		 * @param JgapsRel pointer to the Reading timings filled by TimeCalc
		 * @param minDelay the minimum delay set by Constraint
		 * @param maxDelay the maximum delay set by Constraint
		 * @param isConstraint true in case a timing constraint is defined
		 * @return true if succesfull, false otherwise
		 * ************************************************************************************************************
		 */
		bool scheduleRunwl(uint8_t section, uint32_t * JgapsRel, uint32_t minDelay, uint32_t maxDelay,
						   bool isConstraint);

		/*! ***********************************************************************************************************
		 * @brief calculateRunwl		calculate reading timings from the protocol string
		 * @param protoIndex the index of protocol
		 * @param JgapsRel pointer to the Reading timings filled by TimeCalc
		 * @return true if succesfull, false otherwise
		 * ************************************************************************************************************
		 */
		bool calculateRunwl(uint8_t protoIndex, unsigned int * JgapsRel);

		/*! ***********************************************************************************************************
		 * @brief sendRunwl		send the protocol to the section
		 * @param section the section involved
		 * @param protoIndex the index of protocol
		 * @return true if succesfull, false otherwise
		 * ************************************************************************************************************
		 */
		bool sendRunwl(uint8_t section, uint8_t protoIndex);

        /*! ***********************************************************************************************************
         * @brief sendPressureSettings send to the strip all the corresponding pressure settings
         * @param section the section involved
         * @param strip the strip involved
         * @return 0 if successful, 1 otherwise
         * ************************************************************************************************************
         */
        int sendPressureSettings(uint8_t section, uint8_t strip);

	private:

		std::vector<PressureSettings *> m_pPressureSettings;
};

#endif // WFRUNWL_H
