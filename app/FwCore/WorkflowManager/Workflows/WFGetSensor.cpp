/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetSensor.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFGetSensor class.
 @details

 ****************************************************************************
*/

#include "WFGetSensor.h"
#include "OutCmdGetSensor.h"
#include "InCmdGetSensor.h"
#include "OpGetSensor.h"

WFGetSensor::WFGetSensor()
{
}

WFGetSensor::~WFGetSensor()
{

}

bool WFGetSensor::checkAcceptance(int& liErrorCode)
{
	// this command can always be executed
	liErrorCode = 0;
	return (liErrorCode == 0);
}

OutgoingCommand* WFGetSensor::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdGetSensor();
	return pCmd;
}

Operation* WFGetSensor::getOperation(void)
{
	InCmdGetSensor* p = (InCmdGetSensor*)m_pIncomingCommand;

	Operation* pOp = new OpGetSensor(p->getCategory(), p->getComponent(), p->getId());

	return pOp;
}


