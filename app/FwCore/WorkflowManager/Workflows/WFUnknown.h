/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFUnknown.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the WFUnknown class.
 @details

 ****************************************************************************
*/

#ifndef WFUNKNOWN_H
#define WFUNKNOWN_H


#include <string>

#include "Workflow.h"

class OutgoingCommand;

/*! ***********************************************************************************************************
 * @brief WFUnknown	class derived from Workflow : refer to Workflow for redefined functions
 *					this class manages the workflow for the unknown command
 * ************************************************************************************************************
 */

class WFUnknown : public Workflow
{
	public:

		/*! ***********************************************************************************************************
		 * @brief WFUnknown	constructor
		 * ************************************************************************************************************
		 */
		WFUnknown();

		/*! ***********************************************************************************************************
		 * @brief ~WFUnknown	destructor
		 * ************************************************************************************************************
		 */
		virtual ~WFUnknown();

		/*! ***********************************************************************************************************
		 * @brief checkAcceptance		routine to access the state machine and provide permits of execution of the
		 *								Workflow
		 * ************************************************************************************************************
		 */
		bool checkAcceptance(int& liErrorCode);

		/*! ***********************************************************************************************************
		 * @brief getOutgoingCommand	build the OutgoingCommand associated to the Workflow
		 * ************************************************************************************************************
		 */
		OutgoingCommand* getOutgoingCommand(void);

};

#endif // WFUNKNOWN_H
