/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetSSReference.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFGetSSReference class.
 @details

 ****************************************************************************
*/

#include "WFGetSSReference.h"

#include "OutCmdGetSSReference.h"
#include "OpGetSSReference.h"

WFGetSSReference::WFGetSSReference()
{

}

WFGetSSReference::~WFGetSSReference()
{

}

bool WFGetSSReference::checkAcceptance(int& liErrorCode)
{
    liErrorCode = 0;

    /* this command does not require actions -->
    NO check and interact with the StateMachine

    if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled() )	return false;

	m_deviceList.push_back(eIdNsh);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);
    */

	return (liErrorCode == 0);
}

OutgoingCommand* WFGetSSReference::getOutgoingCommand()
{
	OutgoingCommand* pCmd = new OutCmdGetSSReference();
	return pCmd;
}

Operation*WFGetSSReference::getOperation()
{
	Operation* pOp = new OpGetSSReference();
	return pOp;
}
