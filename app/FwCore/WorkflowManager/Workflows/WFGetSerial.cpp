/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetSerial.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFGetSerial class.
 @details

 ****************************************************************************
*/

#include "WFGetSerial.h"
#include "OutCmdGetSerial.h"
#include "InCmdGetSerial.h"
#include "Operations/OpGetSerial.h"

WFGetSerial::WFGetSerial()
{
}

WFGetSerial::~WFGetSerial()
{

}

bool WFGetSerial::checkAcceptance(int& liErrorCode)
{
	// this command can always be executed
	liErrorCode = 0;

	return (liErrorCode == 0);
}

OutgoingCommand* WFGetSerial::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdGetSerial;
	return pCmd;
}

Operation* WFGetSerial::getOperation(void)
{
	Operation* pOp = new OpGetSerial;
	return pOp;
}
