/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSetSerial.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSetSerial class.
 @details

 ****************************************************************************
*/

#include "WFSetSerial.h"
#include "OutCmdSetSerial.h"
#include "InCmdSetSerial.h"
#include "OpSetSerial.h"

WFSetSerial::WFSetSerial()
{
}

WFSetSerial::~WFSetSerial()
{

}

bool WFSetSerial::checkAcceptance(int& liErrorCode)
{
	liErrorCode = 0;

	InCmdSetSerial* p = (InCmdSetSerial*)m_pIncomingCommand;

	if(p->isCommandValid() == false)
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

#ifdef __DEBUG_PROTOCOL
	return (liErrorCode == 0);
#endif


	m_deviceList.push_back(eIdModule);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSetSerial::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSetSerial;
	return pCmd;
}

Operation* WFSetSerial::getOperation(void)
{
	InCmdSetSerial* p = (InCmdSetSerial*)m_pIncomingCommand;

	OpSetSerial* pOp = new OpSetSerial;

	pOp->setSerialNSH(p->getSerialNSH());
	pOp->setSerialInstrument(p->getSerialInstrument());
	pOp->setSerialMasterboard(p->getSerialMasterboard());
	pOp->setSerialSectionA(p->getSerialSectionA());
	pOp->setSerialSectionB(p->getSerialSectionB());

	return (Operation *)pOp;
}
