/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetVersions.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFGetVersions class.
 @details

 ****************************************************************************
*/

#include "WFGetVersions.h"
#include "OutCmdGetVersions.h"
#include "InCmdGetVersions.h"
#include "Operations/OpGetVersions.h"

WFGetVersions::WFGetVersions()
{
}

WFGetVersions::~WFGetVersions()
{

}

bool WFGetVersions::checkAcceptance(int& liErrorCode)
{
	// this command can always be executed
	liErrorCode = 0;
	return (liErrorCode == 0);
}

OutgoingCommand* WFGetVersions::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdGetVersions;
	return pCmd;
}

Operation* WFGetVersions::getOperation(void)
{
	Operation* pOp = new OpGetVersions;
	return pOp;
}
