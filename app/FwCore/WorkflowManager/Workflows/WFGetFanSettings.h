/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetFanSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the WFGetFanSettings class.
 @details

 ****************************************************************************
*/

#ifndef WFGETFANSETTINGS_H
#define WFGETFANSETTINGS_H

#include "Workflow.h"

class OutgoingCommand;
class Operation;

/*! ********************************************************************************************************************
 * @brief The WFGetFanSettings	class derived from Workflow : refer to Workflow for redefined functions this class manages
 * the workflow for the GET_FAN_SETTINGS command
 * *********************************************************************************************************************
 */
class WFGetFanSettings : public Workflow
{
	public:

		/*! ***********************************************************************************************************
		 * @brief WFGetFanSettings	constructor
		 * ************************************************************************************************************
		 */
		WFGetFanSettings();

		/*! ***********************************************************************************************************
		 * @brief ~WFGetFanSettings	destructor
		 * ************************************************************************************************************
		 */
		virtual ~WFGetFanSettings();

		/*! ***********************************************************************************************************
		 * @brief checkAcceptance		routine to access the state machine and provide permits of execution of the
		 *								Workflow
		 * ************************************************************************************************************
		 */
		bool checkAcceptance(int& liErrorCode);

		/*! ***********************************************************************************************************
		 * @brief getOutgoingCommand	build the OutgoingCommand associated to the Workflow
		 * ************************************************************************************************************
		 */
		OutgoingCommand* getOutgoingCommand(void);

		/*! ***********************************************************************************************************
		 * @brief getOperation			build the Operation associated to the Workflow
		 * ************************************************************************************************************
		 */
		Operation* getOperation(void);

};

#endif // WFGETFANSETTINGS_H
