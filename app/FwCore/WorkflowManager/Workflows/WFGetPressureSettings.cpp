/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetPressureSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFGetPressureSettings class.
 @details

 ****************************************************************************
*/

#include "WFGetPressureSettings.h"
#include "OutCmdGetPressureSettings.h"
#include "InCmdGetPressureSettings.h"
#include "OpGetPressureSettings.h"
#include "MainExecutor.h"

WFGetPressureSettings::WFGetPressureSettings()
{
}

WFGetPressureSettings::~WFGetPressureSettings()
{

}

bool WFGetPressureSettings::checkAcceptance(int& liErrorCode)
{
	std::vector<uint8_t> sectionList;
	InCmdGetPressureSettings* p = (InCmdGetPressureSettings *)m_pIncomingCommand;

	liErrorCode = 0;

	if(p->getSection(&sectionList) == 0)
	{
		// it means all sections: exclude if disabled
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
			m_deviceList.push_back(eIdSectionA);
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
			m_deviceList.push_back(eIdSectionB);
	}
	else
	{
		for(uint8_t i = 0; i < sectionList.size(); i++)
		{
			if(sectionList.at(i) == SCT_A_ID && stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == true)
				m_deviceList.push_back(eIdSectionA);
			else if(sectionList.at(i) == SCT_B_ID && stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == true)
				m_deviceList.push_back(eIdSectionB);
		}
	}

	if(m_deviceList.size() == 0)
	{
		// no valid device
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	/* this command does not require actions -->
	NO check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);
	*/

	return (liErrorCode == 0);
}

OutgoingCommand* WFGetPressureSettings::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdGetPressureSettings;
	return pCmd;
}

Operation* WFGetPressureSettings::getOperation(void)
{
	std::vector<uint8_t> sectionList;

	InCmdGetPressureSettings* p = (InCmdGetPressureSettings *)m_pIncomingCommand;
	p->getSection(&sectionList);
	if(p->getSection(&sectionList) == 0)
	{
		// it means all sections
		sectionList.push_back(SCT_A_ID);
		sectionList.push_back(SCT_B_ID);
	}

	OpGetPressureSettings* pOp = new OpGetPressureSettings(sectionList);
	return (Operation *)pOp;
}

