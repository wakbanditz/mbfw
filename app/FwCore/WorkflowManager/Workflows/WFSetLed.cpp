/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSetLed.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSetLed class.
 @details

 ****************************************************************************
*/

#include "WFSetLed.h"
#include "OutCmdSetLed.h"
#include "InCmdSetLed.h"
#include "OpSetLed.h"

WFSetLed::WFSetLed()
{

}

WFSetLed::~WFSetLed()
{

}

bool WFSetLed::checkAcceptance(int& liErrorCode)
{
	liErrorCode = 0;

	InCmdSetLed* p = (InCmdSetLed*)m_pIncomingCommand;

	std::string strComponent = p->getComponent();

	if(strComponent.compare(XML_TAG_GLOBAL_LED_NAME) == 0)
	{
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdModule) == false)
		{
			liErrorCode = ERR_STATUS_NOT_CONGRUENT;
		}
	}
	else if(strComponent.compare(XML_TAG_SECTIONA_LED_NAME) == 0)
	{
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA) == false)
		{
			liErrorCode = ERR_STATUS_NOT_CONGRUENT;
		}
	}
	else if(strComponent.compare(XML_TAG_SECTIONB_LED_NAME) == 0)
	{
		if(stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB) == false)
		{
			liErrorCode = ERR_STATUS_NOT_CONGRUENT;
		}
	}
    else
    {
        liErrorCode = ERR_COMMAND_PARAMETERS;
        return false;
    }

	// the LED command can always be executed so it does not change the StateMachine
	m_deviceList.clear();
	// liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSetLed::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSetLed;
	return pCmd;
}

Operation* WFSetLed::getOperation(void)
{
	InCmdSetLed* p = (InCmdSetLed*)m_pIncomingCommand;


	Operation* pOp = new OpSetLed(p->getComponent(),
								  p->getMode(),
								  p->getLed());
	return pOp;
}
