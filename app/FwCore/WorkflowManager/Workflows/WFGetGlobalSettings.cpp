/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetGlobalSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFGetGlobalSettings class.
 @details

 ****************************************************************************
*/

#include "WFGetGlobalSettings.h"
#include "OutCmdGetGlobalSettings.h"
#include "InCmdGetGlobalSettings.h"
#include "OpGetGlobalSettings.h"

WFGetGlobalSettings::WFGetGlobalSettings()
{
}

WFGetGlobalSettings::~WFGetGlobalSettings()
{

}

bool WFGetGlobalSettings::checkAcceptance(int& liErrorCode)
{
	// this command can always be executed
	liErrorCode = 0;

	return (liErrorCode == 0);
}

OutgoingCommand* WFGetGlobalSettings::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdGetGlobalSettings;
	return pCmd;
}

Operation* WFGetGlobalSettings::getOperation(void)
{
	Operation* pOp = new OpGetGlobalSettings;
	return pOp;
}

