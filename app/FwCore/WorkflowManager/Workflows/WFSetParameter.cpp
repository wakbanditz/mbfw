/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSetParameter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSetPArameter class.
 @details

 ****************************************************************************
*/

#include "WFSetParameter.h"
#include "InCmdSetParameter.h"
#include "OutCmdSetParameter.h"
#include "OpSetParameter.h"
#include "WebServerAndProtocolInclude.h"

WFSetParameter::WFSetParameter()
{
}

WFSetParameter::~WFSetParameter()
{
	/* Nothing to do yet */
}

bool WFSetParameter::checkAcceptance(int &liErrorCode)
{
	InCmdSetParameter* p = (InCmdSetParameter*)m_pIncomingCommand;

	liErrorCode = 0;

	string strComponent = p->getComponent();
	string strMotor = p->getMotor();

	if((strComponent != XML_TAG_SECTIONA_NAME) &&
		 (strComponent != XML_TAG_SECTIONB_NAME) &&
		 (strComponent != XML_TAG_NSH_NAME))
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	if((strMotor != XML_TAG_MOTOR_NAME_TOWER) && (strMotor != XML_TAG_MOTOR_NAME_PUMP) &&
		(strMotor != XML_TAG_MOTOR_NAME_NSH) && (strMotor != XML_TAG_MOTOR_NAME_TRAY) &&
		(strMotor != XML_TAG_MOTOR_NAME_SPR))
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	if ( strComponent == XML_TAG_SECTIONA_NAME )
	{
		m_deviceList.push_back(eIdSectionA);
	}
	else if ( strComponent == XML_TAG_SECTIONB_NAME )
	{
		m_deviceList.push_back(eIdSectionB);
	}
	else if ( strComponent == XML_TAG_NSH_NAME )
	{
		m_deviceList.push_back(eIdNsh);
	}

#ifdef __DEBUG_PROTOCOL
	return (liErrorCode == 0);
#endif

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSetParameter::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSetParameter;
	return pCmd;
}

Operation* WFSetParameter::getOperation(void)
{
	InCmdSetParameter* p = (InCmdSetParameter*)m_pIncomingCommand;

	OpSetParameter* pOp = new OpSetParameter();

	/*! *************************************************************************************************
	 * @brief DATA SETTERS
	 * **************************************************************************************************
	 */
	pOp->setComponent(p->getComponent());
	pOp->setMotor(p->getMotor());
	pOp->setMicrostep(p->getMicrostep());
	pOp->setConversion(p->getConversion());
	pOp->setHighCurrent(p->getHighCurrent());
	pOp->setLowCurrent(p->getLowCurrent());
	pOp->setAcceleration(p->getAcceleration());

	return (Operation *)pOp;
}

