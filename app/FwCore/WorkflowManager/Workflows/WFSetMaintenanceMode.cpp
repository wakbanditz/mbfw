/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFSetMaintenanceMode.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFSetMaintenanceMode class.
 @details

 ****************************************************************************
*/

#include "WFSetMaintenanceMode.h"
#include "OutCmdSetMaintenanceMode.h"
#include "InCmdSetMaintenanceMode.h"
#include "OpSetMaintenanceMode.h"

WFSetMaintenanceMode::WFSetMaintenanceMode()
{
}

WFSetMaintenanceMode::~WFSetMaintenanceMode()
{

}

bool WFSetMaintenanceMode::checkAcceptance(int& liErrorCode)
{
	InCmdSetMaintenanceMode* p = (InCmdSetMaintenanceMode*)m_pIncomingCommand;

	std::string strActivation;
	strActivation.assign(p->getActivation());

	if(strActivation.compare(XML_ATTR_STATUS_ENABLED) != 0 &&
		strActivation.compare(XML_ATTR_STATUS_DISABLED) != 0 )
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
		return false;
	}

	m_deviceList.push_back(eIdModule);

	// check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);

	return (liErrorCode == 0);
}

OutgoingCommand* WFSetMaintenanceMode::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdSetMaintenanceMode;
	return pCmd;
}

Operation* WFSetMaintenanceMode::getOperation(void)
{
	InCmdSetMaintenanceMode* p = (InCmdSetMaintenanceMode*)m_pIncomingCommand;

	OpSetMaintenanceMode* pOp = new OpSetMaintenanceMode;
	pOp->setActivation(p->getActivation());

	return (Operation *)pOp;
}
