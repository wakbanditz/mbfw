/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFHello.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFHello class.
 @details

 ****************************************************************************
*/

#include "WFHello.h"
#include "InCmdHello.h"
#include "OutCmdHello.h"

WFHello::WFHello()
{
	m_bHasOperation = false;
}

WFHello::~WFHello()
{

}

bool WFHello::checkAcceptance(int& liErrorCode)
{
	liErrorCode = 0;

	return true;
}

OutgoingCommand* WFHello::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdHello;
	return pCmd;
}
