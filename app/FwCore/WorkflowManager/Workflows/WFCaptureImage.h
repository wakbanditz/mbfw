/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFCaptureImage.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the WFCaptureImage class.
 @details

 ****************************************************************************
*/


#ifndef WFCAPTUREIMAGE_H
#define WFCAPTUREIMAGE_H

#include "Workflow.h"
#include "InCmdCaptureImage.h"

class OutgoingCommand;
class Operation;

typedef struct
{
	int liPicturesNum;
	vector<string> vFormat;
	vector<string> vFocus;
	vector<int> vQuality;
	vector<int> vLight;
    vector<string> vPredefinedRoi;
	vector<bool> vCropEnabled;
	vector<structCoordinates> vrgCoordinates;
} structCameraParams;

/*! ***********************************************************************************************************
 * @brief WFCaptureImage	class derived from Workflow : refer to Workflow for redefined functions
 *					this class manages the workflow for the CAPTUREIMAGE command
 * ************************************************************************************************************
 */

class WFCaptureImage : public Workflow
{
	public:

		/*! ***********************************************************************************************************
         * @brief WFCaptureImage	constructor
		 * ************************************************************************************************************
		 */
		WFCaptureImage();

		/*! ***********************************************************************************************************
         * @brief ~WFCaptureImage	destructor
		 * ************************************************************************************************************
		 */
		virtual ~WFCaptureImage();

		/*! ***********************************************************************************************************
		 * @brief checkAcceptance		routine to access the state machine and provide permits of execution of the
		 *								Workflow
		 * ************************************************************************************************************
		 */
		bool checkAcceptance(int& liErrorCode);

		/*! ***********************************************************************************************************
		 * @brief getOutgoingCommand	build the OutgoingCommand associated to the Workflow
		 * ************************************************************************************************************
		 */
		OutgoingCommand* getOutgoingCommand(void);

		/*! ***********************************************************************************************************
		 * @brief getOperation			build the Operationt associated to the Workflow
		 * ************************************************************************************************************
		 */
		Operation* getOperation(void);

	private:

		void createTupleFromMember(void);
		void createDataStruct(void);

    private:

        structCameraParams m_CameraParams;
		vector<vector<tuple<string, string, string>>> m_vMotorTuple;

};

#endif // WFCAPTUREIMAGE_H
