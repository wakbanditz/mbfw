/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFUnknown.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFUnknown class.
 @details

 ****************************************************************************
*/

#include "WFUnknown.h"
#include "OutCmdUnknown.h"
#include "InCmdUnknown.h"
#include "MainExecutor.h"

WFUnknown::WFUnknown()
{
	// no operation linked
	m_bHasOperation = false;
}

WFUnknown::~WFUnknown()
{

}

bool WFUnknown::checkAcceptance(int& liErrorCode)
{
	// command unknown -> always refused
	liErrorCode = ERR_UNKNOWN_COMMAND;
	return false;
}

OutgoingCommand* WFUnknown::getOutgoingCommand(void)
{
	InCmdUnknown* p = (InCmdUnknown*)m_pIncomingCommand;

	OutCmdUnknown* pCmd = new OutCmdUnknown;
	pCmd->setCommandString(p->getCommandString());

	return (OutgoingCommand *)pCmd;
}
