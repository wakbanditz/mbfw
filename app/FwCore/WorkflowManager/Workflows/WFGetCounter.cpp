/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WFGetCounter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the WFGetCounter class.
 @details

 ****************************************************************************
*/

#include "WFGetCounter.h"
#include "OutCmdGetCounter.h"
#include "InCmdGetCounter.h"
#include "OpGetCounter.h"
#include "MainExecutor.h"

WFGetCounter::WFGetCounter()
{
}

WFGetCounter::~WFGetCounter()
{

}

bool WFGetCounter::checkAcceptance(int& liErrorCode)
{
	liErrorCode = 0;

	InCmdGetCounter* p = (InCmdGetCounter*)m_pIncomingCommand;

	string strCounter = p->getCounter();

	if(infoSingleton()->getInstrumentCounterIndexFromString(strCounter) == -1)
	{
		liErrorCode = ERR_COMMAND_PARAMETERS;
	}

	// if a run is in progress the command cannot be executed
	if( (stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusProtocol) ||
		(stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusProtocol) )
	{
		liErrorCode = ERR_STATUS_NOT_CONGRUENT;
	}

	/* this command does not require actions -->
	NO check and interact with the StateMachine
	liErrorCode = setStateMachine(m_deviceList);
	*/

	return (liErrorCode == 0);
}

OutgoingCommand* WFGetCounter::getOutgoingCommand(void)
{
	OutgoingCommand* pCmd = new OutCmdGetCounter;
	return pCmd;
}

Operation* WFGetCounter::getOperation(void)
{
	InCmdGetCounter* p = (InCmdGetCounter*)m_pIncomingCommand;
	string strCounter = p->getCounter();

	OpGetCounter* pOp = new OpGetCounter;
	pOp->setCounter(strCounter);
	return (Operation* )pOp;
}
