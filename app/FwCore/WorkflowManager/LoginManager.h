/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    LoginManager.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the LoginManager class.
 @details

 ****************************************************************************
*/

#ifndef LOGINMANAGER_H
#define LOGINMANAGER_H

#include <list>
#include <string.h>
#include <iostream>
#include <vector>
#include <utility>

#include "Loggable.h"
#include "StaticSingleton.h"

using namespace std;

/*! *******************************************************************************************************************
 * @brief singleton class instance, managed by MainExecutor, to manage the login access	
 * ********************************************************************************************************************
 */
class LoginManager :		public StaticSingleton<LoginManager>,
							public Loggable
{
	public:
		
		/*! ***********************************************************************************************************
		 * @brief LoginManager default constructor
		 * ***********************************************************************************************************
		 */
		LoginManager();

		/*! ***********************************************************************************************************
		 * @brief ~LoginManager default destructor
		 * ************************************************************************************************************
		 */
		~LoginManager();
		
		/*! ***********************************************************************************************************
		 * @brief init reads the LOGINSETUP.cfg file and build the list login - password
		 *					(if not present only default values)
		 * @return	true if succesfull, false otherwise
		 * ***********************************************************************************************************
		 */
		bool init();

		/*! ***********************************************************************************************************
		 * @brief addLogin function used to add a connection string when the login command is executed
         * @param strUsage the connection string ("SERVICE" or "COMMERCIAL")
		 * @param strWebUrl the connection address string in the format "http://10.105.192.109:29091"
		 * @return true if new login added to list, false if already present
		 * ***********************************************************************************************************
		 */
		bool addLogin(std::string strUsage, std::string strWebUrl);
		
		/*! ***********************************************************************************************************
		 * @brief removeLogin function used to remove a connection string when the logout command is executed
		 * @param strWebUrl the connection address string in the format "http://10.105.192.109:29091"
		 * @param strUsage the type of connection (COMMERCIAL vs SERVICE) as string
		 * @return true if login removed from list, false if not found in list
		 * ***********************************************************************************************************
		 */
		bool removeLogin(std::string strUsage, std::string strWebUrl);
		
		/*! ***********************************************************************************************************
		 * @brief removeLogin function used to remove a connection string when the hello timeout expires
		 * @param strUsage the type of connection (COMMERCIAL vs SERVICE) as string: only one per type can be active !
		 * @return true if login removed from list, false if not found in list
		 * ***********************************************************************************************************
		 */
		bool removeLoginByUsage(std::string strUsage);

		/*! ***********************************************************************************************************
		 * @brief checkLoginValid function used to check if a login/password is present in setup list
		 * @param strLogin the login string
		 * @param strPassword the password string
		 * @return true if login present in list, false if not found in list
		 * ***********************************************************************************************************
		 */
		bool checkLoginValid(std::string strLogin, std::string strPassword);

		/*! ***********************************************************************************************************
		 * @brief isLoginUrlActive function used to check if the port is associated to a login in list (active)
		 *			with the corresponding USAGE (COMMERCIAL vs SERVICE)
		 *			used when a command is received to check if a login on the connection has been done
         * @param strUsage the type of connection (COMMERCIAL vs SERVICE) as string: only one per type can be active !
         * @param strWebUrl the connection address string in the format "http://10.105.192.109:29091"
		 * @return the index in vector if the port is present in list, -1 otherwise
		 * ***********************************************************************************************************
		 */
		int isLoginUrlActive(std::string strUsage, std::string strWebUrl);

		/*! ***********************************************************************************************************
		 * @brief isLoginAcceptable function to be called after the id-password check; it is used to check if the
		 *				login can be accepted: if it is a login already active OR if no login of the same Usage is
		 *				active
		 * @param strUsage the connection type string COMMERCIAL or SERVICE
		 * @param strWebUrl the connection address string in the format "http://10.105.192.109:29091"
		 * @return true if the login can be accepted, false otherwise
		 * ***********************************************************************************************************
		 */
		bool isLoginAcceptable(std::string strUsage, std::string strWebUrl);

		/*! ***********************************************************************************************************
		 * @brief getUrlActiveByUsage function used to get the weburl linked to the usage required (COMMERCIAL vs SERVICE)
		 * @param strUsage the connection type string COMMERCIAL or SERVICE
		 * @return the connection address string in the format "http://10.105.192.109:29091"
		 * ***********************************************************************************************************
		 */
		std::string getUrlActiveByUsage(std::string strUsage);

		/*!
		 * @brief getLoginActiveList	Retrives the list of active logins previosuly connected to master board
		 * @param loginList	pair of values of the active logins with the corresponding USAGE (COMMERCIAL vs SERVICE)
		 *			and the connection address string in the format "http://10.105.192.109:29091"
		 * @return the number of the active logins
		 */
		int getLoginActiveList(vector<pair<string, string>> & loginList);

	private :
		
		std::vector<std::pair<std::string, std::string>> m_loginActiveList;
		std::vector<std::pair<std::string, std::string>> m_loginSetupList;
};

#endif // LOGINMANAGER_H
