/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    Operation.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the Operation class.
 @details

 ****************************************************************************
*/

#ifndef OPERATION_H
#define OPERATION_H

#include <string>
#include <vector>

using namespace std;
#define DEFAULT_NUM_RESPAWN_TRIALS	0

#include "HttpClient.h"
#include "StatusInclude.h"

typedef enum
{
	eOperationError 		= 0,
	eOperationRunning		= 1,
	eOperationWaitForBirth	= 2,
	eOperationWaitForDeath	= 3,
	eOperationEnabledForDeath= 4
} eOperationStatus;

typedef struct
{
        string	strNameCmd;
        uint8_t	ubCmd;

}structOpCmdStruct;


class Log;

/*! *************************)******************************************************************************************
 * @brief The Operation class	represents an operation to perform. It is the fundamental type handled
 *								by the OperationManager and the PorterThread
 * ********************************************************************************************************************
 */
class Operation
{

	public:

		/*! ***********************************************************************************************************
		 * @brief Operation constructor
		 * @param strName	the Operation name
		 * ***********************************************************************************************************
		 */
		Operation(string strName);

		/*! ***********************************************************************************************************
		 * @brief ~Operation default destructor
		 * ************************************************************************************************************
		 */
		virtual ~Operation();

		/*! ***********************************************************************************************************
		 * @brief getName retrieves the Operation name
		 * @return a string containing the Operation name
		 * ************************************************************************************************************
		 */
		string getName(void) const;

		/*! ***********************************************************************************************************
		 * @brief getStatus		retrieves the Operation status, it is used to take decision on the operation itself
		 *						i.e. destruction, respawning, birth...
		 * @return the operation current status
		 * ************************************************************************************************************
		 */
		eOperationStatus getStatus(void) const;

		/*! ***********************************************************************************************************
		 * @brief setStatus		method used to set the Operation status, depending on its lifetime
		 * @param liStatus		status to set
		 * ************************************************************************************************************
		 */
		void setStatus(eOperationStatus liStatus);

		/*! ***********************************************************************************************************
		 * @brief perform					method that executes the operationBody(), respawns it, upon failure,
         *                  				the specified number of times, and finally removes the Operation from
		 *									the OperationManager queue
		 * @return							the result reported by the last execution of the operationBody()
		 * ************************************************************************************************************
		 */
		virtual int perform(void);

		/*! ***********************************************************************************************************
		 * @brief setWebResponseBaseUrl		set the web base url to be used by the http client to send the asynchronous
		 *									response at the end of the operation execution
         * @param strUrl				an url of type "http:/some_address:some_port/"
		 * ************************************************************************************************************
		 */
		void setWebResponseUrl(string& strUrl);

        /*! ***********************************************************************************************************
         * @brief setPersistent		set the persistent flag associated to the operation: if true the reply url is
         *                              completed with the persist=true suffix
         * @param status		the persistent flag
         * ************************************************************************************************************
         */
        void setPersistent(bool status);

        /*! ***********************************************************************************************************
         * @brief getPersistent		retrieve persistent flag associated to the operation: if true the reply url is
         *                              completed with the persist=true suffix
         * @return		the persistent flag
         * ************************************************************************************************************
         */
        bool getPersistent();

		/*! ***********************************************************************************************************
		 * @brief setUsage		set the Usage connection associated to the received command (COMMERCIAL vs SERVICE)
		 * @param strUsage		the Usage string
		 * ************************************************************************************************************
		 */
		void setUsage(string strUsage);

		/*! ***********************************************************************************************************
		 * @brief getUsage		get the Usage connection associated to the received command (COMMERCIAL vs SERVICE)
		 * @return m_strUsage		the Usage string
		 * ************************************************************************************************************
		 */
		std::string getUsage();

		/*! ***********************************************************************************************************
		 * @brief getWebUrl		get the WebUrl associated to the received command (COMMERCIAL vs SERVICE)
		 * @return m_strWebUrl		the WebUrl string
		 * ************************************************************************************************************
		 */
		std::string getWebUrl();

		/*! ***********************************************************************************************************
		 * @brief setID			set the CommandId
		 * @param strId			std::string that represents the CommandId
		 * ***********************************************************************************************************
		 */
		void setID(const std::string& strId);

		/*! ***********************************************************************************************************
		 * @brief getID			retrieves the CommandId
		 * @return				the CommandId as a std::string
		 * ***********************************************************************************************************
		 */
		std::string getID() const;

		/*! ***********************************************************************************************************
		 * @brief setLogger					initialize the m_pLogger member to print out log messages wherever
		 * @param pLogger					pointer to a valid Log object
		 * ************************************************************************************************************
		 */
		void setLogger(Log* pLogger);

		/*! ***********************************************************************************************************
		 * @brief sendCommandReply	called by the command specific perform function to send via HTTP the asynchronous
		 *						reply and log it using the logCommandReply function
		 * @param strCommandReply		the complete reply (in xml syntax) as string
		 * @return true if communication succedeed (return code from Host = 200)
		 * ************************************************************************************************************
		 */
		bool sendCommandReply(string& strCommandReply);

		/*! ***********************************************************************************************************
		 * @brief logCommandReply			to log using the m_pLogger object the command reply
		 * @param r							pointer to a valid HttpResponse
		 * @param strCommandReply		the complete reply (in xml syntax) as string
		 * ************************************************************************************************************
		 */
		void logCommandReply(HttpResponse r, string& strCommandReply);

		/*! ***********************************************************************************************************
		 * @brief setDevicesList	to store in Operation object the list of devices involved in the command execution
		 *								to be restored at the end of operation
		 * @param deviceList		the list of devices to be managed
		 * ************************************************************************************************************
		 */
		void setDevicesList(vector<eDeviceId> deviceList);

		/*! ***********************************************************************************************************
		 * @brief restoreStateMachine	function to be called at the end of the perform to restore the State Machine
		 *									in the correct status and lock (using the devicelist set by workflow)
		 * ************************************************************************************************************
		 */
		void restoreStateMachine();

		/*! ***********************************************************************************************************
		 * @brief setErrorCode	set the Error code
		 * @param code		the error code
		 * ***********************************************************************************************************
		 */
		void setErrorCode(uint16_t code);

		/*! ***********************************************************************************************************
		 * @brief getErrorCode			retrieves the Error code
		 * @return				the error code
		 * ***********************************************************************************************************
		 */
		uint16_t getErrorCode();

		/*! ***********************************************************************************************************
		 * @brief execute	function to be called in order to execute the operation the actions related to the command
         * @param lNum the device index
		 * @return 0
		 * ************************************************************************************************************
		 */
		virtual int executeCmd( int lNum = 0);

		/*! ***********************************************************************************************************
		 * @brief compileSendOutCmd	calls the function to compose Payload and serialize the message to be sent to the sw
		 * @return 0
		 * ************************************************************************************************************
		 */
		virtual int compileSendOutCmd(void);

		/*! ***********************************************************************************************************
		 * @brief notifyCompleted	function to be called in order to notify from a device that the operation
		 *  actions related to the command are completed
		 * @param lNum the device
         * @param bSuccess true if action completed correctly, false otherwise
		 * @return 0
		 * ************************************************************************************************************
		 */
		virtual int notifyCompleted(int lNum = 0, bool bSuccess = false);


	protected:

		/*! ***********************************************************************************************************
		 * @brief addFieldCmd	Fills the structThMoveCmdStruct whic is helpfull for the association between the xml
		 * element and the value of the related protocol name.
		 * @param cmd			struct containing the XML value and the hexadecimal value to send to section
		 * @param strName		string of the XML value
		 * @param ubVal			value to send to the section
		 * ************************************************************************************************************
		 */
		void addFieldCmd(vector<structOpCmdStruct>& cmd, const string& strName, uint8_t ubVal);

		/*! ***********************************************************************************************************
		 * @brief setCodedPositions	Research the value of the coded position in the input structThMoveCmdStruct
		 * according to input param strPos.
		 * @param cmd			source struct where to look for the related coded position
		 * @param strPos		string name to reserch
		 * @param ubPosCoded	return value of the coded position
		 * @return	0 if there is a coded position | -1 otherwise
		 * ************************************************************************************************************
		 */
		int setCodedPositions(vector<structOpCmdStruct>& cmd, const string strPos, uint8_t &ubPosCoded);

		/*! ***********************************************************************************************************
		 * @brief closeOperation	method that sends the reply (call to compileSendOutCmd) and close the operation object
		 * @return 0 if success | -1 otherwise
		 * ************************************************************************************************************
		 */
		int closeOperation();

    protected:

        string m_strWebResponseUrl;
        string m_strUsage;
        string m_strID;
        vector<eDeviceId> m_deviceList;
        Log* m_pLogger;
        uint16_t m_errorCode;
        bool m_bPersistent;

    private:

        string m_strName;
        eOperationStatus m_liStatus;

};

#endif // OPERATION_H
