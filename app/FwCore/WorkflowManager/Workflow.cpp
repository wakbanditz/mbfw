/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    Workflow.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the Workflow class.
 @details

 ****************************************************************************
*/

#include "Workflow.h"

#include "MainExecutor.h"
#include "IncomingCommand.h"
#include "OutgoingCommand.h"
#include "FailureCause.h"
#include "ErrorCodes.h"
#include "WebServerAndProtocolInclude.h"

bool Workflow::checkAcceptance(int& liErrorCode)
{
	liErrorCode = 0;
	return true;
}

OutgoingCommand* Workflow::getOutgoingCommand(void)
{
	return 0;
}

Operation* Workflow::getOperation(void)
{
	return 0;
}

Workflow::Workflow()
{
	// by default all worrkflows have an associated operation
	// (at least just to send the COMPLETED asynchronous reply)
	// the only exceptio is the HELO command
	m_bHasOperation = true;
	m_pIncomingCommand = 0;
	m_deviceList.clear();


	setLogger(MainExecutor::getInstance()->getLogger());
}

Workflow::~Workflow()
{
	m_deviceList.clear();
}

void Workflow::setIncomingCommand(IncomingCommand* pCmd)
{
	m_pIncomingCommand = pCmd;
}

void Workflow::setup(OutgoingCommand* &pCmd, Operation* &pOp)
{

	/* ****************************************************************************************************************
	 * Build the synchronous OutgoingCommand
	 * ****************************************************************************************************************
	 */
	pCmd = getOutgoingCommand();
	pOp = 0;

	/* ****************************************************************************************************************
	 * Copy the Usage and Id string from the Incoming command
	 * ****************************************************************************************************************
	 */
	pCmd->setUsage(m_pIncomingCommand->getUsage());
	pCmd->setID(m_pIncomingCommand->getID());

	/* ****************************************************************************************************************
	 * If the Maintenance Mode is set and the connection is COMMERCIAL -> reject command
	 * ****************************************************************************************************************
	 */
	if(infoSingleton()->getMaintenanceMode() == true &&
		m_pIncomingCommand->getUsage().compare(WEB_COMMERCIAL_CONNECTION) == 0)
	{
		pCmd->setStatus(XML_ATTR_STATUS_REJECTED);
		FailureCause* pFailureCause = new FailureCause(ERR_MAINTENANCE_MODE);
		pCmd->setFailureCause(pFailureCause);

		m_pLogger->log(LOG_INFO, "Workflow::setup-->setFailureCause 3 XML_ATTR_STATUS_REJECTED");
		return;
	}

	/* ****************************************************************************************************************
	 * If this Workflow is provided with an Operation, before accessing the StateMachine with the checkAcceptance()
	 * (in order to understand if it can be queued or not), it is necessary to check if the maximum number of parallel
	 * Operations being actually managed has been reached, and, if it is the case, a rejection gets generated a-priori
	 * ****************************************************************************************************************
	 */
	if ( ( m_bHasOperation == true ) && ( ! operationSingleton()->isAvailable() ) )
	{
		pCmd->setStatus(XML_ATTR_STATUS_REJECTED);
		FailureCause* pFailureCause = new FailureCause(ERR_OP_MANAGER_TOO_MANY_OPERATIONS);
		pCmd->setFailureCause(pFailureCause);

		m_pLogger->log(LOG_INFO, "Workflow::setup-->setFailureCause 1 XML_ATTR_STATUS_REJECTED");
		return;
	}

	/* ****************************************************************************************************************
	 * Check if the command requires a login active on the connection received
	 * ****************************************************************************************************************
	 */
	if(m_pIncomingCommand->getLoginRequested())
	{
		// is the connection received compatible with the connections of the command ?
		if(!m_pIncomingCommand->isConnectionAllowed())
		{
			pCmd->setStatus(XML_ATTR_STATUS_REJECTED);
			FailureCause* pFailureCause = new FailureCause(ERR_CONNECTION_NOT_ALLOWED);
			pCmd->setFailureCause(pFailureCause);
			return;
		}

		std::string strUsage = m_pIncomingCommand->getUsage();
		std::string strWebUrl = m_pIncomingCommand->getCallbackUrl();
		if(MainExecutor::getInstance()->m_loginManager.isLoginUrlActive(strUsage, strWebUrl) == -1)
		{
			pCmd->setStatus(XML_ATTR_STATUS_REJECTED);
			FailureCause* pFailureCause = new FailureCause(ERR_LOGIN_NOT_EXECUTED);
			pCmd->setFailureCause(pFailureCause);
			return;
		}
	}

	/* ****************************************************************************************************************
	 * The checkAcceptance() method is called in order to verify whether the IncomingCommand can be performed or not
	 * ****************************************************************************************************************
	 */
	int liError = ERR_NONE;
	bool bCommandCanBePerformed = checkAcceptance(liError);
	if ( bCommandCanBePerformed == true )
	{
		/* ************************************************************************************************************
		 * The state machine declared that the command can be performed so generate the acceptance
		 * ************************************************************************************************************
		 */
		pCmd->setStatus(XML_ATTR_STATUS_ACCEPTED);
		if ( m_bHasOperation == true )
		{
			pOp = getOperation();
			pOp->setDevicesList(m_deviceList);
			pOp->setUsage(m_pIncomingCommand->getUsage());
			pOp->setID(m_pIncomingCommand->getID());
		}
	}
	else
	{
		/* ************************************************************************************************************
		 * The state machine declared that the command cannot be performed so generate the rejection
		 * ************************************************************************************************************
		 */
		pCmd->setStatus(XML_ATTR_STATUS_REJECTED);
		FailureCause* pFailureCause = new FailureCause(liError);
		pCmd->setFailureCause(pFailureCause);

		m_pLogger->log(LOG_INFO, "Workflow::setup-->setFailureCause 2 XML_ATTR_STATUS_REJECTED");
	}

}

eDeviceStatus Workflow::decodeTargetStatus(const std::string strRead)
{
	eDeviceStatus status = eStatusNull;

	if(strRead.empty()) return status;

	// search the corresponding status and set the bitmask
	vector<string>::const_iterator strIter;
	int i;
	for(i = 0, strIter = global_strDeviceStatus.begin(); strIter != global_strDeviceStatus.end(); i++, strIter++)
	{
		if(strcmp(strIter->c_str(), strRead.c_str()) == 0)
		{
            status = (eDeviceStatus)(0x0001 << i);
			break;
		}
	}
	if(status == eStatusNull)
	{
		// no corrispondence to a single state -> try with BACK
		if(strcmp(BACK_STATUS_DEVICE, strRead.c_str()) == 0)
		{
			status = eStatusUnknown;
		}
		// ... or NONE (no action)
		else if(strcmp(NONE_STATUS_DEVICE, strRead.c_str()) == 0)
		{
			status = eStatusNull;
		}
	}
	return status;
}

int Workflow::setStateMachine(std::vector<eDeviceId> deviceList, bool bForceLock)
{

	// check congruence between the current module status and the command status mask
	if(!stateMachineSingleton()->checkDeviceListStatusCongruent(deviceList, m_pIncomingCommand->getMaskStates()))
	{
		m_pLogger->log(LOG_ERR, "Workflow::setStateMachine--> error status ERR_STATUS_NOT_CONGRUENT");
		return ERR_STATUS_NOT_CONGRUENT;
	}

	// try to lock the devices (if the device is available)
    if(!stateMachineSingleton()->setDeviceListLockOn(deviceList, bForceLock))
	{
		m_pLogger->log(LOG_ERR, "Workflow::setStateMachine--> error status ERR_DEVICE_LOCKED");
		return ERR_DEVICE_LOCKED;
	}

	// determine the state of the device after the command execution
	eDeviceStatus targetStatus = decodeTargetStatus(m_pIncomingCommand->getTargetState());

	for(int i = 0; i < (int)deviceList.size(); i++)
	{
		eDeviceStatus tmpStatus;

		tmpStatus = targetStatus;
		if(targetStatus == eStatusUnknown)
		{
			// if the target is unknown it means the device has to be restored in the current status
			// when the command execution is completed so get the device current status
			tmpStatus = stateMachineSingleton()->getDeviceStatus(deviceList.at(i));
		}

		// store the target status to be set at the end of the operation
		stateMachineSingleton()->setDeviceTargetStatus(deviceList.at(i), tmpStatus);

		// command the device state transition
		// if the target is null means no action on the device state (e.g. login command)
		if(tmpStatus != eStatusNull)
		{
			stateMachineSingleton()->executeCommand(deviceList.at(i));
		}
	}

	return 0;
}
