/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PressureSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the PressureSettings class.
 @details

 ****************************************************************************
*/

#ifndef PRESSURESETTINGS_H
#define PRESSURESETTINGS_H

#include <string>
#include <tuple>
#include <vector>

class PressureSettings
{
	public:

		/*! ***********************************************************************************************************
		 * @brief PressureSettings default constructor
		 * ***********************************************************************************************************
		 */
		PressureSettings();

		/*! ***********************************************************************************************************
		 * SETTERS FUNCTIONS
		 * ***********************************************************************************************************
		 */
		void setID(std::string strID);

		void setVersion(std::string strVersion);

		void setAlgorithm(std::string strAlgorithm);

		void setAreaRatio(uint16_t value);

		void setMinimumPositionPercentage(uint16_t value);

		void setFinalSlopePercentage(uint16_t value);

		void setFinalSlopeThreshold(int16_t value);

		void setIndustryThresholdLow(uint64_t value);

		void setIndustryThresholdHigh(uint64_t value);

		void setIndustryCyclesPercentage(uint16_t value);

		void setIndustryCyclesMinimum(uint16_t value);

		void setIndustryCyclesThreshold(uint16_t value);

		void setSpeedVolumeThresholds(char speed, char volume, uint64_t low, uint64_t high);

		/*! *************************************************************************************************
		 * @brief copyPressureSettings copy all data of the input pressureSettings object to the current one
		 * @param pPressureSettings the pointer to the source objet
		 * @return true if successful, false otherwise
		 * **************************************************************************************************
		 */
		bool copyPressureSettings(PressureSettings* pPressureSettings);


		/*! ***********************************************************************************************************
		 * GETTERS FUNCTIONS
		 * ***********************************************************************************************************
		 */
		std::string getID();

		std::string getVersion();

		std::string getAlgorithm();

		uint16_t getAreaRatio();

		uint16_t getMinimumPositionPercentage();

		uint16_t getFinalSlopePercentage();

		int16_t getFinalSlopeThreshold();

		uint64_t getIndustryThresholdLow();

		uint64_t getIndustryThresholdHigh();

		uint16_t getIndustryCyclesPercentage();

		uint16_t getIndustryCyclesMinimum();

		uint16_t getIndustryCyclesThreshold();

		bool getSpeedVolumeThresholds(uint8_t index, char* speed, char* volume, uint64_t* low, uint64_t* high);

	private :


		std::string m_strID;
		std::string m_strVersion;
		std::string m_strAlgorithm;

		uint16_t m_intAreaRatio, m_intMinimumPositionPercentage, m_intFinalSlopePercentage;
		int16_t m_intFinalSlopeThreshold;

		uint64_t m_intIndustryThresholdLow, m_intIndustryThresholdHigh;
		uint16_t m_intIndustryCyclesPercentage,	m_intIndustryCyclesMinimum, m_intIndustryCyclesThreshold;

		std::vector<std::tuple<char, char, uint64_t, uint64_t>> m_vecThresholds;
};

#endif // PRESSURESETTINGS_H
