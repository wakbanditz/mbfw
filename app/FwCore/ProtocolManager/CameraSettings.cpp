#include <iostream>
#include <stdexcept>

#include "CameraSettings.h"

#define KEY_SCHEDULED		"ScheduledVersion"
#define KEY_THRESHOLD		"DetectionThreshold"
#define KEY_DTMX_READ		"DecodeDataMatrix"
#define KEY_SPR_CHECK		"CheckSprPresence"

CameraSettings::CameraSettings()
{
	vConfig.clear();
	vVersion.clear();
	vAlgorithm.clear();
}

CameraSettings::~CameraSettings()
{

}

void CameraSettings::setVersion(string strVersion, uint liSettingId)
{
	// only one version for setting id
	if ( liSettingId != vVersion.size()+1 )		return;

	vVersion.push_back(strVersion);
	return;
}

void CameraSettings::setAlgorithm(string strAlgo, uint liSettingId)
{
	// only one version for setting id
	if ( liSettingId != vAlgorithm.size()+1 )		return;

	vAlgorithm.push_back(strAlgo);
	return;
}

void CameraSettings::addSettingPair(string strKey, string strVal, uint liSettingId)
{
	// only one id added at a time
	if ( liSettingId <= 0 )					return;
	if ( liSettingId > vConfig.size()+1 )	return;

	// Increase size if is a new setting
	if ( liSettingId == vConfig.size()+1 )	vConfig.resize(vConfig.size()+1);

	int liVal = 0;
	try
	{
		liVal = stoi(strVal);
	}
	catch ( const invalid_argument& ia )
	{
		std::cerr << "CameraSettings::setSettingPair: Invalid argument: " << ia.what() << std::endl;
		return;
	}

	vConfig[liSettingId-1].push_back(make_pair(strKey, liVal));
	return;
}

bool CameraSettings::isScheduledReadSec(uint liSettingId)
{
	if ( liSettingId > vConfig.size() )	return false;

	for ( auto a : vConfig[liSettingId-1] )
	{
		if ( ! a.first.compare(KEY_SCHEDULED) )		return ( a.second == 1 );
	}

	return false;
}

int CameraSettings::getDetectionThreshold(uint liSettingId)
{
	if ( liSettingId == 0 || liSettingId > vConfig.size() )	return -1;

	for ( auto a : vConfig[liSettingId-1] )
	{
		if ( ! a.first.compare(KEY_THRESHOLD) )			return a.second;
	}

	return -1;
}

bool CameraSettings::hasSprToBeChecked(uint liSettingId)
{
	if ( liSettingId > vConfig.size() )	return true;

	for ( auto a : vConfig[liSettingId-1] )
	{
		if ( ! a.first.compare(KEY_SPR_CHECK) )	return (a.second == 1);
	}

	// if default then check
	return true;
}

bool CameraSettings::hasDtmxToBeDecoded(uint liSettingId)
{
	if ( liSettingId > vConfig.size() )	return true;

	for ( auto a : vConfig[liSettingId-1] )
	{
		if ( ! a.first.compare(KEY_DTMX_READ) )	return (a.second == 1);
	}

	// if default then check
	return true;
}
