#ifndef READSECINFO_H
#define READSECINFO_H

#include "OpReadSec.h"

//struct structRead
//{
//	bool bIsEnabled;
//	string strCode;
//	vector<unsigned char> vImage;
//};

//struct structSampleRead
//{
//	bool bIsEnabled;
//	int  liWell;
//	int  liScore;
//	bool bSampleIsPresent;
//	vector<unsigned char> vImage;
//};

//struct structSubstrate
//{
//	bool bIsEnabled;
//	int liSubsRead;
//};

//struct structReadSlot
//{
//	bool bIsEnabled; // change with a struct of enabled elements all initialized to false.
//	structRead Spr;
//	structRead Strip;
//	structSubstrate Substrate;
//	structSampleRead W0;
//	structSampleRead W3;
//};


class ReadSecInfo
{
	public:

		/*! ***********************************************************************************************************
		 * @brief ReadSecInfo	constructor
		 * ************************************************************************************************************
		 */
		ReadSecInfo();

		/*! ***********************************************************************************************************
		 * @brief ~ReadSecInfo	destructor
		 * ************************************************************************************************************
		 */
		virtual ~ReadSecInfo();

		void clearAll(void);

		void setParameters(/*const*/ stripReadSection_tag * stripReadSection, vector<pair<string, string>>& vSettings);

		bool setLastTime(uint64_t value);

		void setStartTime(void);

		void getSlotsEnabled(vector<bool>&vSlotEnabled, eTarget target);

		void setStripValuesRead(const vector<int>& vRFVvalues);

		void getStripValuesRead(vector<int>& vRFVvalues);

		void setCurrentSlot(int liSlotId);

		int getCurrentSlot(void);

		void setCameraSettings(CameraSettings* pSettings);

		CameraSettings* getCameraSettings(void);

		int setBarcodeRead(const string& strCode, int liSlotId);

		int setBarcodeImage(const vector<unsigned char>& vImage, int liSlotId);

		int setDataMatrixRead(const string& strCode, int liSlotId);

		int setDataMatrixImage(const vector<unsigned char>& vImage, int liSlotId);

		int setSampleImage(const vector<unsigned char>& vImage, int liWell, int liSlotId);

		int setSampleResult(const float& fResult, int liWell, int liSlotId, uint liSettingId = 1);

		int getSampleDetectionSettingId(int liSlotId, int liWell);

		vector<structReadSlot>& getReadSecPayload(void);

		string getDataMatrixRead(uint liSlotId);

		void getTrueEventNumAndUpdRes(int* pBC, int* pDM, int* pWell0, int* pWell3);

		// get only dtmx not decoded where the cause is not known (if for cone absence or real miss)
		void getDtmxFailedForUnknownCause(vector<unsigned int>&vSlotId);
		void setDtmxFailedToUndecoded(void);

		bool hasBcToBeDecoded(void);
		bool hasPictureToBeTakenBC(int liSlotId);
		bool hasDmToBeDecoded(void);
		bool hasPictureToBeTakenDM(int liSlotId);
		bool hasSampleW0ToBeDetected(void);
		bool hasSampleW3ToBeDetected(void);
		bool hasPictureToBeTakenW0(int liSlotId);
		bool hasPictureToBeTakenW3(int liSlotId);

private:

		CameraSettings* m_pCameraSettings;
		vector<structReadSlot> m_vReadSec;
		stripReadSection_tag m_stripReadSection[SCT_NUM_TOT_SLOTS];
		vector<pair<string, string>> m_vSettings; // TODO not handled for the moment
		int m_nCurrSlotId;
		uint64_t m_startTime;
		string m_strLastTime;
};

#endif // READSECINFO_H
