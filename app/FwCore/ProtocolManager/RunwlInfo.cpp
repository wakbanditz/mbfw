/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    RunwlInfo.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the RunwlInfo class.
 @details

 ****************************************************************************
*/

#include <string.h>
#include <ctime>

#include "RunwlInfo.h"
#include "MainExecutor.h"

#define D_MIN_READ 0
#define D_MAX_READ 60000

RunwlInfo::RunwlInfo()
{
	clearAllData();
}

RunwlInfo::~RunwlInfo()
{

}

bool RunwlInfo::setSection(std::string strSection)
{
	if(strcmp(strSection.c_str(), "A") == 0)
	{
		m_section = SCT_A_ID;
	}
	else if(strcmp(strSection.c_str(), "B") == 0)
	{
		m_section = SCT_B_ID;
	}
	else
	{
		return false;
	}
	return true;
}

bool RunwlInfo::setProtoId(std::__cxx11::string strId)
{
	m_strProtoId.assign(strId);
	return true;
}

bool RunwlInfo::setUnitConstraint(std::__cxx11::string unitConstraint)
{
	m_unitConstraint.assign(unitConstraint);
	return true;
}

bool RunwlInfo::setMinConstraint(uint32_t minConstraint)
{
	m_minDelayConstraint = minConstraint;
	return true;
}

bool RunwlInfo::setMaxConstraint(uint32_t maxConstraint)
{
	m_maxDelayConstraint = maxConstraint;
	return true;
}

bool RunwlInfo::setMonitoringTemperature(uint8_t value)
{
	m_bMonitoringTemperature = (bool)value;
	return true;
}

bool RunwlInfo::setMonitoringAutocheck(uint8_t value)
{
	m_bMonitoringAutocheck = (bool)value;
	return true;
}

bool RunwlInfo::setLastTime(uint64_t value)
{
	time_t secsValue = value / 1000;
	struct tm * setTime = gmtime(&secsValue);

	char bufferTmp[64];
	sprintf(bufferTmp, "%04d-%02d-%02dT%02d:%02d:%02d",
		setTime->tm_year + 1900, setTime->tm_mon + 1, setTime->tm_mday,
		setTime->tm_hour, setTime->tm_min, setTime->tm_sec);

	m_strSectionLastTime.assign(bufferTmp);
	return true;
}

bool RunwlInfo::setStripId(int strip, std::__cxx11::string strId)
{
	if(strip > SCT_NUM_TOT_SLOTS) return false;

	m_stripRunwl[strip].strId.assign(strId);
	return true;
}

bool RunwlInfo::setStripRfuconv(int strip, uint8_t rfuconv)
{
	if(strip > SCT_NUM_TOT_SLOTS) return false;

    m_stripRunwl[strip].rfuConv = rfuconv;
	return true;
}

bool RunwlInfo::setStripProfile(int strip, std::__cxx11::string strProfile)
{
	if(strip > SCT_NUM_TOT_SLOTS) return false;
	if(strProfile.length() != SCT_SLOT_NUM_WELLS) return false;

	char charProfile[SCT_SLOT_NUM_WELLS + 1];
	strcpy(charProfile, strProfile.c_str());

	for(int i = 0; i < SCT_SLOT_NUM_WELLS; i++)
	{
		m_stripRunwl[strip].liquidProfile[i] = charProfile[i];
	}
	return true;
}

bool RunwlInfo::setStripCheck(int strip, std::__cxx11::string strCheck)
{
	if(strip > SCT_NUM_TOT_SLOTS) return false;
	if(strCheck.length() != SCT_SLOT_NUM_WELLS) return false;

	char charCheck[SCT_SLOT_NUM_WELLS + 1];
	strcpy(charCheck, strCheck.c_str());

	for(int i = 0; i < SCT_SLOT_NUM_WELLS; i++)
	{
		m_stripRunwl[strip].pressureProfile[i] = charCheck[i];
	}
	return true;
}

bool RunwlInfo::setProtoSettings(int strip, std::__cxx11::string strSettings)
{
	if(strip > SCT_NUM_TOT_SLOTS) return false;

	m_stripRunwl[strip].strSettings.assign(strSettings);
	return true;
}

bool RunwlInfo::setProtoMeasureAir(int strip, uint8_t value)
{
	if(strip > SCT_NUM_TOT_SLOTS) return false;

	m_stripRunwl[strip].measureAir = value;
	return true;
}

bool RunwlInfo::setProtoTreatedPressure(int strip, uint8_t value)
{
	if(strip > SCT_NUM_TOT_SLOTS) return false;

	m_stripRunwl[strip].treatedPressure = value;
	return true;
}

bool RunwlInfo::setProtoElaboratedPressure(int strip, uint8_t value)
{
	if(strip > SCT_NUM_TOT_SLOTS) return false;

	m_stripRunwl[strip].elaboratedPressure = value;
	return true;
}

uint8_t RunwlInfo::getSection()
{
	return m_section;
}

std::__cxx11::string RunwlInfo::getProtoId()
{
	return m_strProtoId;
}

std::__cxx11::string RunwlInfo::getUnitConstraint()
{
	return m_unitConstraint;
}

bool RunwlInfo::getValidConstraint()
{
	return m_validConstraint;
}

uint32_t RunwlInfo::getMinimumConstraint()
{
	return m_minDelayConstraint;
}

uint32_t RunwlInfo::getMaximumConstraint()
{
	return m_maxDelayConstraint;
}

std::__cxx11::string RunwlInfo::getLastTime()
{
	return m_strSectionLastTime;
}

std::__cxx11::string RunwlInfo::getStripId(int strip)
{
	if(strip > SCT_NUM_TOT_SLOTS) return "";

	return m_stripRunwl[strip].strId;
}

uint8_t RunwlInfo::getStripRfuconv(int strip)
{
	if(strip > SCT_NUM_TOT_SLOTS) return 0;

	return m_stripRunwl[strip].rfuConv;
}

std::__cxx11::string RunwlInfo::getProtoSettings(int strip)
{
	if(strip > SCT_NUM_TOT_SLOTS) return 0;

	return m_stripRunwl[strip].strSettings;
}

uint8_t RunwlInfo::getProtoMeasureAir(int strip)
{
	if(strip > SCT_NUM_TOT_SLOTS) return 0;

	return m_stripRunwl[strip].measureAir;
}

uint8_t RunwlInfo::getProtoTreatedPressure(int strip)
{
	if(strip > SCT_NUM_TOT_SLOTS) return 0;

	return m_stripRunwl[strip].treatedPressure;
}

uint8_t RunwlInfo::getProtoElaboratedPressure(int strip)
{
	if(strip > SCT_NUM_TOT_SLOTS) return 0;

	return m_stripRunwl[strip].elaboratedPressure;
}

uint8_t RunwlInfo::getStripWellProfile(int strip, int well)
{
    if(strip >= SCT_NUM_TOT_SLOTS) return ZERO_CHAR;
    if(well >= SCT_SLOT_NUM_WELLS) return ZERO_CHAR;

	return m_stripRunwl[strip].liquidProfile[well];
}

uint8_t RunwlInfo::getStripWellCheck(int strip, int well)
{
    if(strip >= SCT_NUM_TOT_SLOTS) return ZERO_CHAR;
    if(well >= SCT_SLOT_NUM_WELLS) return ZERO_CHAR;

	return m_stripRunwl[strip].pressureProfile[well];
}

uint8_t * RunwlInfo::getStripProfile(int strip)
{
	if(strip > SCT_NUM_TOT_SLOTS) return 0;

	return m_stripRunwl[strip].liquidProfile;
}

uint8_t * RunwlInfo::getStripCheck(int strip)
{
	if(strip > SCT_NUM_TOT_SLOTS) return 0;

	return m_stripRunwl[strip].pressureProfile;
}

bool RunwlInfo::getMonitoringTemperature()
{
	return m_bMonitoringTemperature;
}

bool RunwlInfo::getMonitoringAutocheck()
{
	return m_bMonitoringAutocheck;
}

bool RunwlInfo::isAtLeastOneStripEnabled()
{
	for(int i = 0; i < SCT_NUM_TOT_SLOTS; i++)
	{
		if(!m_stripRunwl[i].strId.empty())
		{
			// this one is a strip with the RunId defined
			return true;
		}
	}
	return false;
}

bool RunwlInfo::copyRunwlCommandInfo(RunwlInfo* pRunwlInfo)
{
	if(pRunwlInfo == 0) return false;

	m_strProtoId.assign(pRunwlInfo->getProtoId());
	m_section = pRunwlInfo->getSection();

	m_validConstraint = pRunwlInfo->getValidConstraint();
	m_unitConstraint.assign(pRunwlInfo->getUnitConstraint());
	m_minDelayConstraint = pRunwlInfo->getMinimumConstraint();
	m_maxDelayConstraint = pRunwlInfo->getMaximumConstraint();

	m_bMonitoringTemperature = pRunwlInfo->getMonitoringTemperature();
	m_bMonitoringAutocheck = pRunwlInfo->getMonitoringAutocheck();

	for(int i = 0; i < SCT_NUM_TOT_SLOTS; i++)
	{
		m_stripRunwl[i].rfuConv = pRunwlInfo->getStripRfuconv(i);
		m_stripRunwl[i].strId.assign(pRunwlInfo->getStripId(i));
		m_stripRunwl[i].strSettings.assign(pRunwlInfo->getProtoSettings(i));
		m_stripRunwl[i].elaboratedPressure = pRunwlInfo->getProtoElaboratedPressure(i);
		m_stripRunwl[i].treatedPressure = pRunwlInfo->getProtoTreatedPressure(i);
		m_stripRunwl[i].measureAir = pRunwlInfo->getProtoMeasureAir(i);

		for(int j = 0; j < SCT_SLOT_NUM_WELLS; j++)
		{
			m_stripRunwl[i].liquidProfile[j] = pRunwlInfo->getStripWellProfile(i, j);
			m_stripRunwl[i].pressureProfile[j] = pRunwlInfo->getStripWellCheck(i, j);
		}
	}
	return true;
}

void RunwlInfo::buildJtoPerform(uint32_t * JgapsRel)
{
	uint32_t Jabs;
	uint8_t i;

	// preliminary cleanup
	for(i = 0; i < J_MAXMIN_VECTOR_SIZE; i++)
	{
		m_JtoPerform[i] = 0;
	}

	// reading time setting
	for(Jabs = 0, i = 0; i < J_MAXMIN_VECTOR_SIZE; i++, JgapsRel++)
	{
		if(*JgapsRel > 0)
		{
			Jabs += *JgapsRel;
			m_JtoPerform[i] = Jabs;

            printf("Section %d reading %d time %d\n", m_section, i, m_JtoPerform[i]);

		}
	}
}

uint32_t RunwlInfo::getReadingValueJ(uint8_t index)
{
	if(index >= J_MAXMIN_VECTOR_SIZE) return 0;

	return m_JtoPerform[index];
}

bool RunwlInfo::decodeConstraint()
{
	m_validConstraint = false;

	if(m_unitConstraint.empty())
	{
		// no constraint defined
		return true;
	}

	// the constraint can be only of type M or T that have a standard length

	if(strcmp(m_unitConstraint.c_str(), CONSTRAINT_TIME_MINUTES) == 0 ||
		strcmp(m_unitConstraint.c_str(), CONSTRAINT_TIME_SECONDS) == 0 )
	{
		if(m_minDelayConstraint == 9999) m_minDelayConstraint = 0;

		if(strcmp(m_unitConstraint.c_str(), CONSTRAINT_TIME_MINUTES) == 0)
		{
			// translate the values from minutes to seconds
			m_minDelayConstraint *= 60;
			m_maxDelayConstraint *= 60;
		}

		// translate the values from seconds to mseconds and assign them
		m_minDelayConstraint *= 1000;
		m_maxDelayConstraint *= 1000;
		m_validConstraint = true;
	}

	return m_validConstraint;
}

uint8_t RunwlInfo::findReadingToSet()
{
	uint8_t j, strip;

	for(j = 0; j < J_MAXMIN_VECTOR_SIZE; j++)
	{
		if(m_JtoPerform[j] != 0)
		{
			// this reading index is valid
			for(strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
			{
				if(m_stripRunwl[strip].strId.empty() == false)
				{
					// this is a valid strip
					if(m_stripRunwl[strip].readingData[j].JtimeRead == 0)
					{
						// if the time value has not been set yet -> bingo !
						return j;
					}
				}
			}
		}
	}
	return J_MAXMIN_VECTOR_SIZE;
}

bool RunwlInfo::setStripReadingIndexTime(uint8_t strip, uint8_t index, uint64_t value)
{
	if(strip >= SCT_NUM_TOT_SLOTS || index >= J_MAXMIN_VECTOR_SIZE)
	{
		return false;
	}

	if(m_stripRunwl[strip].strId.empty() || m_stripRunwl[strip].readingData[index].JtimeRead != 0)
	{
		return false;
	}

	m_stripRunwl[strip].readingData[index].JtimeRead = value;
	return true;
}

uint64_t RunwlInfo::getStripReadingIndexTime(uint8_t strip, uint8_t index)
{
	if(strip >= SCT_NUM_TOT_SLOTS || index >= J_MAXMIN_VECTOR_SIZE)
	{
		return 0;
	}

	if(m_stripRunwl[strip].strId.empty())
	{
		return 0;
	}

	if(m_startTime == 0)
	{
		return 0;
	}

	if(m_stripRunwl[strip].readingData[index].JtimeRead <= m_startTime)
	{
		return 0;
	}

	return (m_stripRunwl[strip].readingData[index].JtimeRead - m_startTime);
}

bool RunwlInfo::setStripReadingIndexValues(uint8_t strip, uint8_t index, int32_t minReading,
										   int32_t maxReading, int32_t meanReading)
{
	if(strip >= SCT_NUM_TOT_SLOTS || index >= J_MAXMIN_VECTOR_SIZE)
	{
		return false;
	}

	if(m_stripRunwl[strip].strId.empty())
	{
		return false;
	}

	m_stripRunwl[strip].readingData[index].JfluoMax = maxReading;
	m_stripRunwl[strip].readingData[index].JfluoMin = minReading;
	m_stripRunwl[strip].readingData[index].JfluoRead = meanReading;
	return true;
}

bool RunwlInfo::getStripReadingIndexValues(uint8_t strip, uint8_t index,
										   int32_t * minReading, int32_t * maxReading, int32_t * meanReading)
{
	if(strip >= SCT_NUM_TOT_SLOTS || index >= J_MAXMIN_VECTOR_SIZE)
	{
		return false;
	}

	if(m_stripRunwl[strip].strId.empty())
	{
		return false;
	}

	if(maxReading) *maxReading = m_stripRunwl[strip].readingData[index].JfluoMax;
	if(minReading) *minReading = m_stripRunwl[strip].readingData[index].JfluoMin;
	if(meanReading) *meanReading = m_stripRunwl[strip].readingData[index].JfluoRead;
	return true;
}

bool RunwlInfo::setStripReadingAir(uint8_t strip, uint32_t value)
{
	if(strip >= SCT_NUM_TOT_SLOTS)
	{
		return false;
	}

	if(m_stripRunwl[strip].strId.empty())
	{
		return false;
	}

	m_stripRunwl[strip].readingAir = value;
	return true;
}

uint32_t RunwlInfo::getStripReadingAir(uint8_t strip)
{
	if(strip >= SCT_NUM_TOT_SLOTS)
	{
		return 0;
	}

	if(m_stripRunwl[strip].strId.empty())
	{
		return 0;
	}

	return m_stripRunwl[strip].readingAir;
}

bool RunwlInfo::setStripReadingAirTime(uint8_t strip, uint64_t value)
{
	if(strip >= SCT_NUM_TOT_SLOTS)
	{
		return false;
	}

	if(m_stripRunwl[strip].strId.empty())
	{
		return false;
	}

	m_stripRunwl[strip].readingAirTime = value;
	return true;
}

uint32_t RunwlInfo::getStripReadingAirTime(uint8_t strip)
{
	if(strip >= SCT_NUM_TOT_SLOTS)
	{
		return 0;
	}

	if(m_stripRunwl[strip].strId.empty())
	{
		return 0;
	}

	if(m_stripRunwl[strip].readingAirTime <= m_startTime)
	{
		return 0;
	}

	return (m_stripRunwl[strip].readingAirTime - m_startTime);
}

void RunwlInfo::setStartTime()
{
	m_startTime = (uint64_t)getSecondsTime();
}

uint64_t RunwlInfo::getStartTime()
{
	return m_startTime;
}

void RunwlInfo::clearAllData()
{
	int i, j;

	m_strProtoId.clear();
	m_section = SCT_NONE_ID;
	m_startTime = 0;

	m_validConstraint = false;
	m_unitConstraint.clear();
	m_minDelayConstraint = D_MIN_READ;
	m_maxDelayConstraint = D_MAX_READ;

	m_bMonitoringTemperature = false;
	m_bMonitoringAutocheck = false;

	m_strSectionLastTime.clear();

	for(i = 0; i < SCT_NUM_TOT_SLOTS; i++)
	{
		m_stripRunwl[i].rfuConv = 0;
		m_stripRunwl[i].strId.clear();
		m_stripRunwl[i].strSettings.clear();
		m_stripRunwl[i].measureAir = 0;
		m_stripRunwl[i].treatedPressure = 0;
		m_stripRunwl[i].elaboratedPressure = 0;

		for(j = 0; j < SCT_SLOT_NUM_WELLS; j++)
		{
			m_stripRunwl[i].liquidProfile[j] = '0';
			m_stripRunwl[i].pressureProfile[j] = '0';
		}
		for(j = 0; j < J_MAXMIN_VECTOR_SIZE; j++)
		{
			m_stripRunwl[i].readingData[j].JtimeRead = 0;
			m_stripRunwl[i].readingData[j].JfluoRead = 0;
			m_stripRunwl[i].readingData[j].JfluoMin = 0;
			m_stripRunwl[i].readingData[j].JfluoMax = 0;
			m_stripRunwl[i].readingData[j].JfluoCv = 0;
			m_stripRunwl[i].readingData[j].JerrorCode = 0;
		}

		m_stripRunwl[i].readingAir = 0;
		m_stripRunwl[i].readingAirTime = 0;
	}

	for(j = 0; j < J_MAXMIN_VECTOR_SIZE; j++)
	{
		m_JtoPerform[j] = 0;
	}
}
