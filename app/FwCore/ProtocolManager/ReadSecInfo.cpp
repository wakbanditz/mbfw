#include "ReadSecInfo.h"
#include "MainExecutor.h"

#define CODE_NOT_DECODED_MSG			"______"	// when dtmx decoded, but we don't know if the spr is present or not
#define CODE_PRESENT_MSG				"?"			// when the spr is present, but dtmx not decoded
#define CODE_ABSENT_MSG					"!"				// when the spr is absent
#define SAMPLE_DETECTION_THRESHOLD		70

ReadSecInfo::ReadSecInfo()
{
	clearAll();
}

ReadSecInfo::~ReadSecInfo()
{
	SAFE_DELETE(m_pCameraSettings);
}

void ReadSecInfo::clearAll()
{
	m_nCurrSlotId = SCT_SLOT_1_ID;
	m_startTime = 0;
	m_vSettings.clear();
	m_strLastTime.clear();

	for ( int i = 0; i < SCT_NUM_TOT_SLOTS; i++ )
	{
		m_stripReadSection[i].m_bEnabled = false;
		m_stripReadSection[i].m_bStripCheck = false;
		m_stripReadSection[i].m_bSprCheck = false;
		m_stripReadSection[i].m_bSubstrateCheck = false;
		m_stripReadSection[i].m_bSprImage = false;
		m_stripReadSection[i].m_bStripImage = false;
		m_stripReadSection[i].m_SampleDetection.m_bCheckX0 = false;
		m_stripReadSection[i].m_SampleDetection.m_bCheckX3 = false;
		m_stripReadSection[i].m_SampleDetection.m_nSettingsX0 = -1;
		m_stripReadSection[i].m_SampleDetection.m_nSettingsX3 = -1;
		m_stripReadSection[i].m_SampleDetection.m_bImageX0 = false;
		m_stripReadSection[i].m_SampleDetection.m_bImageX3 = false;
	}

	m_vReadSec.clear();
	m_vReadSec.resize(SCT_NUM_TOT_SLOTS);
	SAFE_DELETE(m_pCameraSettings);
	m_pCameraSettings = new CameraSettings;

	return;
}

void ReadSecInfo::setParameters(/*const*/ stripReadSection_tag* stripReadSection, vector<pair<string, string> >&vSettings)
{
	if ( stripReadSection == nullptr )	return;

	memcpy(m_stripReadSection, stripReadSection, sizeof(stripReadSection_tag) * SCT_NUM_TOT_SLOTS);

	/*** TODO: delete, only for testing with HMI sw ***/
	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( m_stripReadSection[i].m_SampleDetection.m_nSettingsX0 == 0 )
		{
			m_stripReadSection[i].m_SampleDetection.m_bCheckX0 = false;
			m_stripReadSection[i].m_SampleDetection.m_bImageX0 = false;
		}

		if ( m_stripReadSection[i].m_SampleDetection.m_nSettingsX3 == 0 )
		{
			stripReadSection[i].m_SampleDetection.m_bCheckX3 = false;
			stripReadSection[i].m_SampleDetection.m_bImageX3 = false;

			m_stripReadSection[i].m_SampleDetection.m_bCheckX3 = false;
			m_stripReadSection[i].m_SampleDetection.m_bImageX3 = false;
		}
	}
	/************ TODO: Delete until here *************/

	m_vSettings = vSettings;
	m_vReadSec.clear();
	m_vReadSec.resize(SCT_NUM_TOT_SLOTS);

	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		m_vReadSec[i].bIsEnabled = m_stripReadSection[i].m_bEnabled;

		m_vReadSec[i].Strip.bIsEnabled = m_stripReadSection[i].m_bStripCheck;
		m_vReadSec[i].Substrate.bIsEnabled = m_stripReadSection[i].m_bSubstrateCheck;
		m_vReadSec[i].Spr.bIsEnabled = m_stripReadSection[i].m_bSprCheck;
		m_vReadSec[i].W0.bIsEnabled = m_stripReadSection[i].m_SampleDetection.m_bCheckX0;
		m_vReadSec[i].W3.bIsEnabled = m_stripReadSection[i].m_SampleDetection.m_bCheckX3;
	}
	return;
}

bool ReadSecInfo::setLastTime(uint64_t value)
{
	time_t secsValue = value / 1000;
	struct tm * setTime = gmtime(&secsValue);

	char bufferTmp[64];
	sprintf(bufferTmp, "%04d-%02d-%02dT%02d:%02d:%02d",
		setTime->tm_year + 1900, setTime->tm_mon + 1, setTime->tm_mday,
		setTime->tm_hour, setTime->tm_min, setTime->tm_sec);

	m_strLastTime.assign(bufferTmp);
	return true;
}

void ReadSecInfo::setStartTime()
{
	m_startTime = (uint64_t)getSecondsTime();
}

void ReadSecInfo::getSlotsEnabled(vector<bool>& vSlotEnabled, eTarget target)
{
	vSlotEnabled.clear();
	vSlotEnabled.resize(SCT_NUM_TOT_SLOTS);

	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		switch ( target )
		{
			case eTarget::eStrip:
				vSlotEnabled[i] = m_vReadSec[i].Strip.bIsEnabled;
			break;

			case eTarget::eSubstrate:
				vSlotEnabled[i] = m_vReadSec[i].Substrate.bIsEnabled;
			break;

			case eTarget::eConeAbsence:
			case eTarget::eSpr:
				vSlotEnabled[i] = m_vReadSec[i].Spr.bIsEnabled;
			break;

			case eTarget::eSample0:
				vSlotEnabled[i] = m_vReadSec[i].W0.bIsEnabled;
			break;

			case eTarget::eSample3:
				vSlotEnabled[i] = m_vReadSec[i].W3.bIsEnabled;
			break;

			default:
				vSlotEnabled[i] = false;
			break;
		}
	}

	return;
}

void ReadSecInfo::setStripValuesRead(const vector<int>& vRFVvalues)
{
	if ( vRFVvalues.size() < SCT_NUM_TOT_SLOTS )		return;

	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( ! m_vReadSec[i].Substrate.bIsEnabled )		continue;
		m_vReadSec[i].Substrate.liSubsRead = vRFVvalues[i];
	}

	return;
}

void ReadSecInfo::getStripValuesRead(vector<int>& vRFVvalues)
{
	vRFVvalues.clear();
	vRFVvalues.resize(SCT_NUM_TOT_SLOTS);

	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		vRFVvalues[i] = m_vReadSec[i].Substrate.liSubsRead;
	}

	return;
}

void ReadSecInfo::setCurrentSlot(int liSlotId)
{
	m_nCurrSlotId = liSlotId;
}

int ReadSecInfo::getCurrentSlot()
{
	return m_nCurrSlotId;
}

void ReadSecInfo::setCameraSettings(CameraSettings* pSettings)
{
	if ( pSettings == nullptr )		return;

	SAFE_DELETE(m_pCameraSettings);
	m_pCameraSettings = new CameraSettings;
	*m_pCameraSettings = *pSettings;
}

CameraSettings* ReadSecInfo::getCameraSettings()
{
	return m_pCameraSettings;
}

int ReadSecInfo::setBarcodeRead(const string& strCode, int liSlotId)
{
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )			return -1;
	if ( ! m_vReadSec[liSlotId].Strip.bIsEnabled )	return -1;

	m_vReadSec[liSlotId].Strip.strCode.assign(strCode);
	return 0;
}

int ReadSecInfo::setBarcodeImage(const vector<unsigned char>& vImage, int liSlotId)
{
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )	return -1;

	m_vReadSec[liSlotId].Strip.vImage = vImage;
	return 0;
}

int ReadSecInfo::setDataMatrixRead(const string& strCode, int liSlotId)
{
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )			return -1;
	if ( ! m_vReadSec[liSlotId].Spr.bIsEnabled )	return -1;

	m_vReadSec[liSlotId].Spr.strCode.assign(strCode);
	return 0;
}

int ReadSecInfo::setDataMatrixImage(const vector<unsigned char>& vImage, int liSlotId)
{
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )	return -1;

	m_vReadSec[liSlotId].Spr.vImage = vImage;
	return 0;
}

int ReadSecInfo::setSampleImage(const vector<unsigned char>& vImage, int liWell, int liSlotId)
{
	if ( liWell != 0 && liWell != 3 )		return -1;
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )	return -1;

	if ( ! liWell )
	{
		if ( ! m_stripReadSection[liSlotId].m_SampleDetection.m_bImageX0 )		return -1;
		m_vReadSec[liSlotId].W0.vImage = vImage;
	}
	else
	{
		if ( ! m_stripReadSection[liSlotId].m_SampleDetection.m_bImageX3 )		return -1;
		m_vReadSec[liSlotId].W3.vImage = vImage;
	}

	return 0;
}

int ReadSecInfo::setSampleResult(const float& fResult, int liWell, int liSlotId, uint liSettingId)
{
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )	return -1;
	if ( liWell != 0 && liWell != 3 )		return -1;

	int liThreshold = m_pCameraSettings->getDetectionThreshold(liSettingId);
	if ( liThreshold <= 0 )
	{
		liThreshold = SAMPLE_DETECTION_THRESHOLD;
	}

	if ( ! liWell )
	{
		if ( ! m_vReadSec[liSlotId].W0.bIsEnabled )		return -1;
		m_vReadSec[liSlotId].W0.liWell = WELL_ZERO;
		m_vReadSec[liSlotId].W0.liScore = int(fResult);
		m_vReadSec[liSlotId].W0.bSampleIsPresent = ( fResult < float(liThreshold) );
	}
	else
	{
		if ( ! m_vReadSec[liSlotId].W3.bIsEnabled )		return -1;
		m_vReadSec[liSlotId].W3.liWell = WELL_THREE;
		m_vReadSec[liSlotId].W3.liScore = int(fResult);
		m_vReadSec[liSlotId].W3.bSampleIsPresent = ( fResult < float(liThreshold) );
	}

	return 0;
}

int ReadSecInfo::getSampleDetectionSettingId(int liSlotId, int liWell)
{
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )	return -1;

	if ( liWell == WELL_ZERO )				return m_stripReadSection[liSlotId].m_SampleDetection.m_nSettingsX0;
	else if ( liWell == WELL_THREE )		return m_stripReadSection[liSlotId].m_SampleDetection.m_nSettingsX3;
	else									return -1;
}

vector<structReadSlot>& ReadSecInfo::getReadSecPayload()
{
	return m_vReadSec;
}

string ReadSecInfo::getDataMatrixRead(uint liSlotId)
{
	if ( liSlotId >= m_vReadSec.size() )	return "";

	return m_vReadSec[liSlotId].Spr.strCode;
}

void ReadSecInfo::getTrueEventNumAndUpdRes(int* pBC, int* pDM, int* pWell0, int* pWell3)
{
	if ( pBC == nullptr || pDM == nullptr ||
		 pWell0 == nullptr || pWell3 == nullptr )
	{
		return;
	}

	*pBC = 0;
	*pDM = 0;
	*pWell0 = 0;
	*pWell3 = 0;

	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( ! m_vReadSec[i].bIsEnabled )	continue;

		if ( m_vReadSec[i].Spr.bIsEnabled )		(*pDM)++;

		// If the strip is not present, there is no need to decode barcode or check Sample presence.
		// Skip slot and update results
		if ( m_vReadSec[i].Substrate.bIsEnabled && m_vReadSec[i].Substrate.liSubsRead < RFU_THRESHOLD_STRIP )
		{
			if ( m_vReadSec[i].Strip.bIsEnabled )		setBarcodeRead(CODE_ABSENT_MSG, i);
			if ( m_vReadSec[i].W0.bIsEnabled )			setSampleResult(100.0f, WELL_ZERO, i, 0);
			if ( m_vReadSec[i].W3.bIsEnabled )			setSampleResult(100.0f, WELL_THREE, i, 0);
			continue;
		}

		if ( m_vReadSec[i].Strip.bIsEnabled )	(*pBC)++;
		if ( m_vReadSec[i].W0.bIsEnabled )		(*pWell0)++;
		if ( m_vReadSec[i].W3.bIsEnabled )		(*pWell3)++;
	}

	return;
}

void ReadSecInfo::getDtmxFailedForUnknownCause(vector<unsigned int>& vSlotId)
{
	vSlotId.clear();

	for ( size_t i = 0; i != m_vReadSec.size(); ++i )
	{
		if ( ! m_vReadSec[i].bIsEnabled )		continue;
		if ( ! m_vReadSec[i].Spr.bIsEnabled )	continue;

		// If the dtm hasn't been decoded, then add in the list
		if ( ! m_vReadSec[i].Spr.strCode.compare(CODE_NOT_DECODED_MSG) )
		{
			vSlotId.push_back(i);
		}
	}

	return;
}

void ReadSecInfo::setDtmxFailedToUndecoded()
{
	for ( size_t i = 0; i != m_vReadSec.size(); ++i )
	{
		if ( ! m_vReadSec[i].bIsEnabled )		continue;
		if ( ! m_vReadSec[i].Spr.bIsEnabled )	continue;

		// If the dtm hasn't been decoded, set  string to "?"
		if ( ! m_vReadSec[i].Spr.strCode.compare(CODE_NOT_DECODED_MSG) )
		{
			m_vReadSec[i].Spr.strCode.assign(CODE_PRESENT_MSG);
		}
	}
}

bool ReadSecInfo::hasBcToBeDecoded()
{
	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( ! m_vReadSec[i].bIsEnabled )		continue;
		if ( m_vReadSec[i].Strip.bIsEnabled )	return true;
	}
	return false;
}

bool ReadSecInfo::hasPictureToBeTakenBC(int liSlotId)
{
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )			return -1;

	return m_stripReadSection[liSlotId].m_bStripImage;
}

bool ReadSecInfo::hasDmToBeDecoded()
{
	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( ! m_vReadSec[i].bIsEnabled )		continue;
		if ( m_vReadSec[i].Spr.bIsEnabled )		return true;
	}
	return false;
}

bool ReadSecInfo::hasPictureToBeTakenDM(int liSlotId)
{
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )			return -1;

	return m_stripReadSection[liSlotId].m_bSprImage;
}

bool ReadSecInfo::hasSampleW0ToBeDetected()
{
	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( ! m_vReadSec[i].bIsEnabled )		continue;
		if ( m_vReadSec[i].W0.bIsEnabled )		return true;
	}
	return false;
}

bool ReadSecInfo::hasSampleW3ToBeDetected()
{
	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( ! m_vReadSec[i].bIsEnabled )		continue;
		if ( m_vReadSec[i].W3.bIsEnabled )		return true;
	}
	return false;
}

bool ReadSecInfo::hasPictureToBeTakenW0(int liSlotId)
{
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )			return -1;

	return m_stripReadSection[liSlotId].m_SampleDetection.m_bImageX0;
}

bool ReadSecInfo::hasPictureToBeTakenW3(int liSlotId)
{
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )			return -1;

	return m_stripReadSection[liSlotId].m_SampleDetection.m_bImageX3;
}
