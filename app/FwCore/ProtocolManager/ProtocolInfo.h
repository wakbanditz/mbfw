/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ProtocolInfo.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the ProtocolInfo class.
 @details

 ****************************************************************************
*/

#ifndef PROTOCOLINFO_H
#define PROTOCOLINFO_H

#include <stdint.h>
#include <string>
#include <list>
#include <vector>

#include "CommonInclude.h"

class ProtocolInfo
{
	public:

		/*! ***********************************************************************************************************
		 * @brief ProtocolInfo default constructor
		 * ***********************************************************************************************************
		 */
		ProtocolInfo();

		/*! *************************************************************************************************
		 * @brief ProtocolInfo destructor
		 * **************************************************************************************************
		 */
		virtual ~ProtocolInfo();


		/*! *************************************************************************************************
		 * @brief setProtoId set the Protocol Id read from the incoming command
		 * @param strId the protocol id
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoId(std::string strId);

		/*! *************************************************************************************************
		 * @brief setProtoName set the Protocol Name read from the incoming command
		 * @param strName the protocol name
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoName(std::string strName);

		/*! *************************************************************************************************
		 * @brief setProtoGlobals set the Protocol Globals read from the incoming command
		 * @param strGlobals the protocol Globals string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoGlobals(const char * strGlobals);

		/*! *************************************************************************************************
		 * @brief setProtoLocals set the Protocol Locals read from the incoming command
		 * @param strLocals the protocol Locals string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoLocals(const char * strLocals);

		/*! *************************************************************************************************
		 * @brief setProtoMinMaxJ set the Protocol J min / max read from the incoming command
		 * @param value the J time value
		 * @param index the J index
		 * @param isMax if = 1 -> Max, if = 0 -> Min
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoMinMaxJ(uint32_t value, uint8_t index, uint8_t isMax);

		/*! *************************************************************************************************
		 * @brief getProtoId get the Protocol Id read from the incoming command
		 * @return the protocol id
		 * **************************************************************************************************
		 */
		std::string getProtoId();

		/*! *************************************************************************************************
		 * @brief getProtoName set the Protocol Name read from the incoming command
		 * @return the protocol name
		 * **************************************************************************************************
		 */
		std::string getProtoName();

		/*! *************************************************************************************************
		 * @brief getProtoGlobals get the Protocol Globals read from the incoming command
		 * @return the protocol Globals string
		 * **************************************************************************************************
		 */
		bool getProtoGlobals(char * strGlobals);

		/*! *************************************************************************************************
		 * @brief getProtoLocals get the Protocol Locals read from the incoming command
		 * @return the protocol Locals string
		 * **************************************************************************************************
		 */
		bool getProtoLocals(char * strLocals);

		/*! *************************************************************************************************
		 * @brief getProtoMinMaxJ set the Protocol J min / max read from the incoming command
		 * @param index the J index
		 * @param isMax if = 1 -> Max, if = 0 -> Min
		 * @return the J value
		 * **************************************************************************************************
		 */
		uint32_t getProtoMinMaxJ(uint8_t index, uint8_t isMax);

		/*! *************************************************************************************************
		 * @brief clearAllData reset all values
		 * **************************************************************************************************
		 */
		void clearAllData();

		/*! *************************************************************************************************
		 * @brief copyProtocolInfo copy all data of the input protocolinfo object to the current one
		 * @param pProtocolInfo the pointer to the source objet
		 * @return true if successful, false otherwise
		 * **************************************************************************************************
		 */
		bool copyProtocolInfo(ProtocolInfo* pProtocolInfo);

	private :

		std::string	m_protoId;
		std::string	m_strName;

		char		m_strGlobals[MAX_PROTOCOL_GLOBALS_LEN + 1];
		char		m_strLocals[MAX_PROTOCOL_LOCALS_LEN + 1];

		std::vector<std::pair<uint32_t, uint32_t>> m_timeListJminJmax;
};

#endif // PROTOCOLINFO_H
