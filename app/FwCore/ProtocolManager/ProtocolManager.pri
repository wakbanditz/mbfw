INCLUDEPATH += $$PWD/../ProtocolManager
INCLUDEPATH += $$PWD/../WorkflowManager

HEADERS += \
    $$PWD/CameraSettings.h \
	$$PWD/ProtocolInfo.h \
    $$PWD/ReadSecInfo.h \
    $$PWD/RunwlInfo.h \
    $$PWD/THRunProtocol.h \
    $$PWD/PressureSettings.h

SOURCES += \
    $$PWD/CameraSettings.cpp \
	$$PWD/ProtocolInfo.cpp \
    $$PWD/ReadSecInfo.cpp \
    $$PWD/RunwlInfo.cpp \
    $$PWD/THRunProtocol.cpp \
    $$PWD/PressureSettings.cpp

