/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THRunProtocol.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the THRunProtocol class.
 @details

 ****************************************************************************
*/
#ifndef THRUNPROTOCOL_H

#define THRUNPROTOCOL_H

#include <fstream>

#include "Thread.h"
#include "StaticSingleton.h"
#include "Loggable.h"
#include "Semaphore.h"

#include "CommonInclude.h"
#include "StatusInclude.h"
#include "SectionInclude.h"
#include "OpReadSec.h"

class OpRunwl;
class OpReadSec;

class THRunProtocol :	public StaticSingleton<THRunProtocol>,
						public Thread,
						public Loggable
{
	public:

		/*! ***********************************************************************************************************
		 * @brief THRunProtocol default constructor
		 * ***********************************************************************************************************
		 */
		THRunProtocol();

		/*! ***********************************************************************************************************
		 * @brief ~THRunProtocol default destructor
		 * ***********************************************************************************************************
		 */
		virtual ~THRunProtocol();

		/*! ***********************************************************************************************************
		 * @brief setOperation associates the Runwl Op to the section protocol so that it can be used for the reply
         * @param pOp the pointer to the Runwl/ReadSec operation object
		 * @param section the section involved in the scheduler event
		 * @return true if success, false otherwise
		 * ***********************************************************************************************************
		 */
		bool setOperation(uint8_t section, OpRunwl * pOp);
		bool setOperation(uint8_t section, OpReadSec * pOp);

		/*! ***********************************************************************************************************
		 * @brief perform checks the context of the procedure is correct and release the semaphore to unlock the thread
		 * @param section the section involved in the scheduler event
		 * @param actionId can be 0 if protocol or 1 if readsec
		 * @return eRunning in case of success, eError otherwise (see eOperationStatus enum)
		 * ***********************************************************************************************************
		 */
		int perform(int section, uint8_t actionId);

		/*! ***********************************************************************************************************
		 * @brief requestAction called by the scheduler to request an action on the protocol
		 * @param section the section involved in the scheduler event
		 * @param action the requested action
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int requestAction(uint8_t section, uint8_t action);

		/*! ***********************************************************************************************************
		 * @brief startProtocol called by the scheduler to start the protocol on the section
		 * @param section the section involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int startProtocol(int section);

		/*! ***********************************************************************************************************
		 * @brief endProtocol called by the scheduler to end the protocol on the section
		 * @param section the section involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int endProtocol(int section);

		/*! ***********************************************************************************************************
		 * @brief continueProtocol called by the scheduler to continue the protocol on the section
		 * @param section the section involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int continueProtocol(int section);

		/*! ***********************************************************************************************************
		 * @brief startReadSec called by the scheduler to start the READSEC on the section
		 * @param section the section involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int startReadSec(int section);

		/*! ***********************************************************************************************************
		 * @brief  moveMotors allowes to move all the motors in the desired position
		 * @param  vMotorsSection vect of pair where the first argument is the motor to be moved, the second one is the
		 *		   coded position
		 * @param  pMotorNsh motor pair where the first argument is the target, while the second one is the slot id
		 *		   (1 - 6). Are also allowed 0, that is the SS, and "home".
		 * @param  section section id (0 - 1)
		 * @param  liNextRequest request to be expected after the movement
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int moveMotors(vector<pair<eSectionMotor, string> > vMotorsSection, pair<eTarget, string> pMotorNsh,
					   int section, int liNextRequest);

		/*! ***********************************************************************************************************
		 * @brief checkStripReadSec called by the scheduler to perform a reading on the section
		 * @param section the section involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int checkStripReadSec(int section);

		/*! ***********************************************************************************************************
		 * @brief moveBeforeDecodingReadSec called by the scheduler to perform a movement to go to the reading position
		 * @param section the section involved in the scheduler event
         * @param target operation identifier (eTarget enum)
         * @param liNextRequest successive request action
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int moveBeforeDecodingReadSec(int section, int target, int liNextRequest);

		/*! ***********************************************************************************************************
		 * @brief decodeReadSec called by the scheduler to perform a reading on the barcode of a section
		 * @param section the section involved in the scheduler event
         * @param target the action identifier (eTarget enum)
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int decodeReadSec(int section, int target);

		/*! ***********************************************************************************************************
		 * @brief updateDecodedReadSec called by the scheduler to store the last barcode/data matrix read
		 * @param section the section involved in the scheduler event
		 * @param target can be barcode or data matrix
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int updateResultReadSec(int section, int target);

		/*! ***********************************************************************************************************
		 * @brief checkSampleReadSec called by the scheduler to check the presence of the sample in W0 or W3
		 * @param section the section involved in the scheduler event
         * @param target the action identifier (eTarget enum)
         * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int checkSampleReadSec(int section, int target);

		/*! ***********************************************************************************************************
		 * @brief checkSprReadSec called by the scheduler to check the presence of the spr
		 * @param section the section involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int checkSprReadSec(int section);

		/*! ***********************************************************************************************************
		 * @brief readProtocol called by the scheduler to perform a reading on the section
		 * @param section the section involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int readProtocol(int section);

		/*! ***********************************************************************************************************
		 * @brief airProtocol called by the scheduler to perform a final Air reading on the section
		 * @param section the section involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int airProtocol(int section);

		/*! ***********************************************************************************************************
		 * @brief abortProtocol called by the scheduler to abort the protocol on the section
		 * @param section the section involved in the scheduler event
         * @param bSendCommand true if the Abort command has to be sent to section (abort from gateway)
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
        int abortProtocol(int section, bool bSendCommand);

		/*! ***********************************************************************************************************
		 * @brief stoppedProtocol called when a protocol is aborted on a section
		 * @param section the section involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int stoppedProtocol(int section);

		/*! ***********************************************************************************************************
		 * @brief getSectionStartSeconds to retrieve the absolute start time of the section
		 * @param section the section involved
		 * @return the time in seconds in case of success, 0 otherwise
		 * ***********************************************************************************************************
		 */
		size_t getSectionStartSeconds(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief getSectionFromStartSeconds to retrieve the relative time respect to the start of the section
		 * @param section the section involved
		 * @return the time in seconds in case of success, 0 otherwise
		 * ***********************************************************************************************************
		 */
		size_t getSectionFromStartMseconds(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief repeatRunwlReply to send again (reading from file) the last RUN reply of the sections (if necessary)
		 * @param strWebUrl the WebUrl to which send the message
		 * @return true if all correct (nothing to do or messages sent ok), false otherwise
		 * ***********************************************************************************************************
		 */
		bool repeatRunwlReply(std::string strWebUrl);

		/*! ***********************************************************************************************************
		 * @brief abortProtocolFromCommand called by the CancelSection command
		 * @param section the section involved
		 * @return true if ok, false otherwise
		 * ***********************************************************************************************************
		 */
		bool abortProtocolFromCommand(uint8_t section);

        /*! ***********************************************************************************************************
         * @brief setSectionReply set the value of the sectionReply variable: true if the reply has been
         *               correctly sent to gateway (called by THReplyProtocol)
         * @param section the section involved
         * @param status the flag status
         * ***********************************************************************************************************
         */
        void setSectionReply(uint8_t section, bool status);

        /*! ***********************************************************************************************************
         * @brief deleteOperation to remove the operation(s) linked to section(s) after the reply has been
         *               correctly sent to gateway (called by THReplyProtocol)
         * @param section the section involved
         * ***********************************************************************************************************
         */
        void deleteOperation(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief deleteOperationReadSec to remove the operation(s) linked to section(s) after the reply has been
		 *               correctly sent to gateway (called by THReplyProtocol)
		 * @param section the section involved
		 * ***********************************************************************************************************
		 */
		void deleteOperationReadSec(uint8_t section);

        /*! ***********************************************************************************************************
         * @brief sendReplyReadSec to compose and send the ReadSec final reply
         * @param section the section involved
         * @return 0 in case of success
         * ***********************************************************************************************************
         */
        int sendReplyReadSec(int section);

		/*! ***********************************************************************************************************
		 * @brief abortReadSec called by the scheduler to abort the readSec on the section
		 * @param section the section involved in the scheduler event
		 * @return 0 in case of success, error code otherwise
		 * ***********************************************************************************************************
		 */
		int abortReadSec(int section);

        /*! ***********************************************************************************************************
         * @brief to check if the section is in UNLOAD correctly or with error
         * @param section the section involved
         * @return true if the section is in UNLOAD but with error
         * ***********************************************************************************************************
         */
        bool isSectionUnloadInError(uint8_t section);


protected:


		/*! ***********************************************************************************************************
		 * @brief workerThread neverending thread normally waiting on the semaphore : when the semaphore it's released
		 *		the thread executes the ReadSection procedure and, finally, compose the asynchronous reply, restore
		 *		the state machine and destroys the operation
		 * @return 0 when thread ends
		 * ***********************************************************************************************************
		 */
		int workerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions before the start of the thread
		 * ***********************************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! ***********************************************************************************************************
         * @brief afterWorkerThread to perform actions after the end of the thread
		 * ***********************************************************************************************************
		 */
		void afterWorkerThread(void);

	private:


        /*! ***********************************************************************************************************
		 * @brief to save on the protocol temperature file of the section the SPR, Tray, Room temperatures
		 * @param section the section involved
		 * ************************************************************************************************************
		 */
		void saveTemperatures(uint8_t section);

        /*! ***********************************************************************************************************
		 * @brief readRunReply to read from file the last Runwl final reply status
		 * @param section the section involved
		 * @return the whole reply string already xml-formatted
		 * ***********************************************************************************************************
		 */
		std::string readRunReply(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief readRunReplyOk to read from file the corrector uncorrect Runwl final reply status
		 * @return true if the connection was correct, false otherwise
		 * @param section the section involved
		 * ***********************************************************************************************************
		 */
		bool readRunReplyOk(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief composeDefaultRunwlReply build and store the default reply of "not completed protocol"
		 * @return true if the connection was correct, false otherwise
		 * @param section the section involved
		 * ***********************************************************************************************************
		 */
		bool composeDefaultRunwlReply(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief to set the status of the section (and so also of the module)
		 * @param section the section involved
		 * @param status the status (as defined in StatusInclude.h)
		 * ************************************************************************************************************
		 */
		void updateStatus(uint8_t section, eDeviceStatus status);

        /*! ***********************************************************************************************************
         * @brief to set the status of the NSH when the protocol is over on a section
         * @param section the section involved
         * ************************************************************************************************************
         */
        void endProtocolNsh(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief to move the nsh on the first strip of the section befoire the reading phase
		 * @param section the section involved
		 * @param nextRequest following request
		 * @return 0 if ok, -1 in case of error
		 * ************************************************************************************************************
		 */
		int moveNshToSection(int section);

		/*! ***********************************************************************************************************
		 * @brief checkPendingAction in case a pending action is present -> activate it
		 * @param section the section involved
		 * @return 0 if ok, -1 in case of error
		 * ************************************************************************************************************
		 */
		int checkPendingAction(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief getReadProtocol to acquire from the NSH the read values for the whole section
		 * @param section the section involved
		 * @return 0 if ok, -1 in case of error
		 * ************************************************************************************************************
		 */
		int getReadProtocol(int section);

		/*! ***********************************************************************************************************
		 * @brief getReadAir to acquire from the NSH the AIR read values for the whole section
		 * @param section the section involved
		 * @return 0 if ok, -1 in case of error
		 * ************************************************************************************************************
		 */
		int getReadAir(int section);

		/*! ***********************************************************************************************************
		 * @brief getStripPresence to acquire from the NSH the rfu values read from the desired slots
		 * @param section the section involved
		 * @return 0 if ok, -1 in case of error
		 * ************************************************************************************************************
		 */
		int getSubstrateRead(int section);

		/*! ***********************************************************************************************************
		 * @brief  getTrueEventNumber function to be called after reading substrate. Skip BC, W0 or W3 if the strip is
		 *		   not present
		 * @param  pBC counter of BC events
		 * @param  pDM counter of DM events
		 * @param  pWell0 counter of Well0 events
		 * @param  pWell3 counter of Well3 events
		 * @param  liSection the section involved
		 * @return 0 if ok, -1 in case of error
		 * ************************************************************************************************************
		 */
		int getTrueEventNumber(int* pBC, int* pDM, int* pWell0, int* pWell3, int liSection);

		/*! ***********************************************************************************************************
		 * @brief stopDataAcquisition stop the Pressure and Temperature collecting
		 * @param section the section involved
		 * ************************************************************************************************************
		 */
		void stopDataAcquisition(uint8_t section);

        /*! ***********************************************************************************************************
         * @brief moveTrayOut the section tray is moved out to unlock the cover
         * @param section the section involved
         * ************************************************************************************************************
         */
        int moveTrayOut(uint8_t section);

        /*! ***********************************************************************************************************
		 * @brief initializeTemperatureData initialize the file for the Temperature Data
		 * @param section the section involved
		 * ************************************************************************************************************
		 */
		void initializeTemperatureData(uint8_t section);

        /*! ***********************************************************************************************************
         * @brief generateSectionSynchroFailure creates an error of synchronization on the section
         * @param section the section involved
         * ************************************************************************************************************
         */
        void generateSectionSynchroFailure(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief buildDebugFile to create fake data file to be usd for debug
		 * @param section the section involved
		 * ************************************************************************************************************
		 */
		void buildDebugFile(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief rescheduleReadSec
		 * @param liDMNum
		 * @param liBCNum
		 * @param liSubstrateNum
		 * @param liSample0Num
		 * @param liSample3Num
		 * @param liConeAbsenceNum
		 * @param section section involved in the rescheduling
		 * @return
		 * ************************************************************************************************************
		 */
		bool rescheduleReadSec(int liBCNum, int liDMNum, int liSample0Num, int liSample3Num, int liConeAbsenceNum, uint8_t section);

    private:

        Semaphore m_synchSem;

        bool 	m_protocolActive;
        bool	m_protocolAbortedCommand[SCT_NUM_TOT_SECTIONS];
        bool	m_sectionReplyOk[SCT_NUM_TOT_SECTIONS];
        uint8_t m_sectionStatus[SCT_NUM_TOT_SECTIONS];
        uint8_t m_sectionRequest[SCT_NUM_TOT_SECTIONS];
        uint8_t m_sectionPending[SCT_NUM_TOT_SECTIONS];
        time_t	m_sectionSeconds[SCT_NUM_TOT_SECTIONS];
        time_t	m_sectionTimeout[SCT_NUM_TOT_SECTIONS];
        ofstream m_sectionTemperatureFile[SCT_NUM_TOT_SECTIONS];
        OpRunwl * m_sectionOperation[SCT_NUM_TOT_SECTIONS];
		OpReadSec * m_readSecOperation[SCT_NUM_TOT_SECTIONS];
};

class THReplyProtocol :	public StaticSingleton<THReplyProtocol>,
                        public Thread,
                        public Loggable
{
    public:

        /*! ***********************************************************************************************************
         * @brief THReplyProtocol default constructor
         * ***********************************************************************************************************
         */
        THReplyProtocol();

        /*! ***********************************************************************************************************
         * @brief ~THReplyProtocol default destructor
         * ***********************************************************************************************************
         */
        virtual ~THReplyProtocol();


        /*! ***********************************************************************************************************
         * @brief requestReplySection request to compose and send the protocol reply for a section
         * @param section the section index
         * @param pOpRunwl pointer to the operation linked to section
         * @param bSuccess true -> protocol OK, false -> protocol KO
         * @param bProtocolAborted true if protocol aborted by command, false otherwise
         *              (meaningful only if bSuccess = false)
         * @return true if request accepted, false otherwise
         * ***********************************************************************************************************
         */
        bool requestReplySection(uint8_t section, OpRunwl * pOpRunwl, bool bSuccess,
                                 bool bProtocolAborted);

		/*! ***********************************************************************************************************
		 * @brief  requestReplySection request to compose and send the protocol reply for a section
		 * @param  section the section index
		 * @param  pOpReadSec pointer to the operation linked to section
		 * @param  bSuccess true -> protocol OK, false -> protocol KO
		 * @return true if request accepted, false otherwise
		 * ***********************************************************************************************************
		 */
		bool requestReplyReadSec(uint8_t section, OpReadSec* pOpReadSec, bool bSuccess);

    protected:

        /*! ***********************************************************************************************************
         * @brief workerThread neverending thread normally waiting on the semaphore : when the semaphore it's released
         *		the thread executes the ReadSection procedure and, finally, compose the asynchronous reply, restore
         *		the state machine and destroys the operation
         * @return 0 when thread ends
         * ***********************************************************************************************************
         */
        int workerThread(void);

        /*! ***********************************************************************************************************
         * @brief beforeWorkerThread to perform actions before the start of the thread
         * ***********************************************************************************************************
         */
        void beforeWorkerThread(void);

        /*! ***********************************************************************************************************
         * @brief afterWorkerThread to perform actions after the end of the thread
         * ***********************************************************************************************************
         */
        void afterWorkerThread(void);

    private:


        /*! ***********************************************************************************************************
         * @brief sendRunwlReply to compose and send the Runwl final reply
         * @param section the section involved
         * @return true in case of success, false otherwise
         * ***********************************************************************************************************
         */
        bool sendRunwlReply(uint8_t section);

		/*! ***********************************************************************************************************
		 * @brief sendReadSecReply to compose and send the ReadSec final reply
		 * @param section the section involved
		 * @return true in case of success, false otherwise
		 * ***********************************************************************************************************
		 */
		bool sendReadSecReply(uint8_t section);

    private:

        Semaphore m_synchSem;

        bool m_replyActive;
        bool m_requestSection[SCT_NUM_TOT_SECTIONS];
		bool m_requestReadSec[SCT_NUM_TOT_SECTIONS];
        OpRunwl * m_sectionOperation[SCT_NUM_TOT_SECTIONS];
		OpReadSec * m_readSecOperation[SCT_NUM_TOT_SECTIONS];
        bool m_bSuccess[SCT_NUM_TOT_SECTIONS];
        bool m_protocolAbortedCommand[SCT_NUM_TOT_SECTIONS];
};

/*! ***********************************************************************************************************
 * @brief THRunProtocol_startProtocol called by the scheduler to start protocol on the section: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_startProtocol(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_endProtocol called by the scheduler to end protocol on the section: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_endProtocol(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_continueProtocol called by the scheduler to continue protocol on the section: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_continueProtocol(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_readProtocol called by the scheduler to perform a reading on the section: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_readProtocol(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_readAir called by the scheduler to perform a Air reading on the section: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_readAir(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_abortProtocol called by the scheduler to abort protocol on the section: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_abortProtocol(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_moveNshProtocol called by the scheduler to move the reader during protocol on the section:
 *			wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_moveNshProtocol(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_startProtocol called by the scheduler to start readsec on the section: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_startReadSec(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_getReadyReadSec called by the scheduler to set correct position of resources for
 *		  readsecn: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_getReadyReadSec(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_checkStripPresenceReadSec called by the scheduler to check strip presence for readsec:
 *		  wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_checkStripPresenceReadSec(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_decodeBCReadSec called by the scheduler to read barcode for readsec: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_decodeBCReadSec(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_decodeBCReadSec called by the scheduler to read DM for readsec: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_decodeDMReadSec(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_checkSample0ReadSec called by the scheduler to check presence of the sample in X0
 *		  for readsec: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_checkSample0ReadSec(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_checkSample3ReadSec called by the scheduler to check presence of the sample in X3
 *		  for readsec: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_checkSample3ReadSec(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief ThRunProtocol_checkSprPresenceReadSec called by the scheduler to check presence of the SPR
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int ThRunProtocol_checkSprPresenceReadSec(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_rescheduleEvent called by the scheduler to reschedule if needed
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_rescheduleEvent(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_setDefaultReadSec called by the scheduler to move the motors back to default
 *		  for readsec: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_setDefaultReadSec(int section, int slot, int event);

/*! ***********************************************************************************************************
 * @brief THRunProtocol_replyReadSec called by the scheduler to send reply readsec: wrapper function
 * @param section the section involved in the scheduler event
 * @param slot the involved slot
 * @param event spare argument
 * @return 0 in case of success, error code otherwise
 * ***********************************************************************************************************
 */
int THRunProtocol_replyReadSec(int section, int slot, int event);

#endif // THRUNPROTOCOL_H
