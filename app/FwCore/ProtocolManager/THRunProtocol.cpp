/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    THRunProtocol.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the THRunProtocol class.
 @details

 ****************************************************************************
*/

#include <time.h>
#include <chrono>
#include <cstdint>

#include "THRunProtocol.h"
#include "MainExecutor.h"
#include "OutCmdRunwl.h"
#include "RunwlPayload.h"
#include "OpRunwl.h"
#include "OutCmdReadSec.h"
#include "ReadSecPayload.h"

#include "SectionBoardGeneral.h"
#include "SectionBoardPIB.h"
#include "SectionBoardTray.h"
#include "SectionBoardSPR.h"
#include "NSHBoardManager.h"
#include "FailureCause.h"

#include "THMonitorPressure.h"

#include "WebServerAndProtocolInclude.h"


#define SEM_PROTOCOL_SYNCH_NAME			"/sem-protocol-synch"
#define SEM_PROTOCOL_SYNCH_INIT_VALUE		0

#define SEM_REPLY_SYNCH_NAME			"/sem-reply-synch"
#define SEM_REPLY_SYNCH_INIT_VALUE		0


enum
{
    PROTO_NOT_RUNNING =	0,
    PROTO_RUNNING,
    PROTO_STARTED,
    PROTO_IN_ABORT,
    PROTO_UNLOAD
};

enum
{
	REQUEST_NONE = 0,
	REQUEST_START_PROTOCOL,
	REQUEST_CONTINUE_PROTOCOL,
	REQUEST_READ_PROTOCOL,
	REQUEST_READ_AIR,
	REQUEST_END_PROTOCOL,
	REQUEST_ABORT_PROTOCOL,
	REQUEST_MOVE_NSH_PROTOCOL,
	REQUEST_WAIT_NSH_MOVEMENT,
	REQUEST_WAIT_NSH_READING,
	REQUEST_WAIT_AIR_READING,
    REQUEST_ERROR_UNLOAD,
    REQUEST_UNLOAD,
	REQUEST_WAIT_END_PROTOCOL,

	REQUEST_START_READSEC,
	REQUEST_GETREADY_READSEC,
	REQUEST_CHECK_STRIP_PRESENCE,
	REQUEST_WAIT_NSH_READ_PRESENCE,
	REQUEST_WAIT_NSH_MOVE_AND_DECODE_BC,
	REQUEST_MOVE_BEFORE_DECODING_BC,
	REQUEST_WAIT_WHILE_DECODING_BC,
	REQUEST_MOVE_BEFORE_DECODING_DM,
	REQUEST_WAIT_NSH_MOVE_AND_DECODE_DM,
	REQUEST_WAIT_WHILE_DECODING_DM,
	REQUEST_MOVE_BEFORE_CHECKING_W0,
	REQUEST_WAIT_NSH_MOVE_AND_CHECK_W0,
	REQUEST_WAIT_WHILE_CHECKING_W0,
	REQUEST_MOVE_BEFORE_CHECKING_W3,
	REQUEST_WAIT_NSH_MOVE_AND_CHECK_W3,
	REQUEST_WAIT_WHILE_CHECKING_W3,
    REQUEST_MOVE_BEFORE_CHECKING_SPR,
	REQUEST_RESCHEDULE_EVENT,
    REQUEST_WAIT_NSH_MOVE_AND_CHECK_SPR,
    REQUEST_WAIT_WHILE_CHECKING_SPR,
	REQUEST_SET_DEFAULT_READSEC,
	REQUEST_SEND_REPLY_READSEC,
	REQUEST_ABORT_READSEC,
	REQUEST_END_READSEC
};

#define TEMPERATURE_SAMPLING_INTERVAL	10

#define RUN_REPLY_NAME			"RUN_REPLY_"
#define RUN_REPLY_STATUS		"RUN_STATUS_"
#define RUN_REPLY_EXTENSION		".reply"

#define PROTOCOL_SYNCHRO_TIMEOUT	2	// 2 secs
#define PROTOCOL_ACTION_TIMEOUT     15	// 15 secs
#define READSEC_ACTION_TIMEOUT		20	// 30 secs


/*! ***********************************************************************************************************
 * @brief convertNumberToChars convert a decimal value to the corresponding ascii string:
 *              1045 -> "1045"
 * @param value the integer value
 * @param buffer [out] the destination buffer
 * @param numberSize the number of char to be used (i.e. the dimension of the value)
 * ***********************************************************************************************************
 */
static void convertNumberToChars(int64_t value, char * buffer, uint8_t numberSize);

/*! ***********************************************************************************************************
 * @brief saveRunReply to save on file the Runwl final reply
 * @param strOutReply the reply string
 * @param section the section involved
 * ***********************************************************************************************************
 */
static void saveRunReply(uint8_t section, std::string& strOutReply);

/*! ***********************************************************************************************************
 * @brief saveRunReplyOk to save on file the corrector uncorrect Runwl final reply status
 * @param status true if the connection was correct, false otherwise
 * @param section the section involved
 * ***********************************************************************************************************
 */
static void saveRunReplyOk(uint8_t section, bool status);



static Mutex m_mutexProtocol;


// ------------------------------------------------------------------------------------------- //
THRunProtocol::THRunProtocol()
{
	m_synchSem.create(SEM_PROTOCOL_SYNCH_NAME, SEM_PROTOCOL_SYNCH_INIT_VALUE);
	m_synchSem.enableErrorPrint(true);

	m_protocolActive = false;

	for(uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		m_sectionStatus[section] = PROTO_NOT_RUNNING;
		m_sectionRequest[section] = REQUEST_NONE;
		m_sectionPending[section] = REQUEST_NONE;
		m_sectionSeconds[section] = 0;
		m_sectionOperation[section] = 0;
		m_sectionTimeout[section] = 0;
		m_sectionReplyOk[section] = true;
		m_readSecOperation[section] = 0;
	}
}

THRunProtocol::~THRunProtocol()
{

}

size_t THRunProtocol::getSectionStartSeconds(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	RunwlInfo * pRunwl = runwlInfoPointer(section);
	if(pRunwl == 0) return 0;

	return pRunwl->getStartTime();
}

size_t THRunProtocol::getSectionFromStartMseconds(uint8_t section)
{
    using namespace std::chrono;

    if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	RunwlInfo * pRunwl = runwlInfoPointer(section);
	if(pRunwl == 0) return 0;

	size_t tmpSeconds = getSecondsTime();

	if(tmpSeconds <= pRunwl->getStartTime()) return 0;


    uint64_t currentMsecs = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    currentMsecs -= (uint64_t)(pRunwl->getStartTime() * 1000);

    return (size_t)(currentMsecs);
//	return (tmpSeconds - pRunwl->getStartTime());
}

bool THRunProtocol::setOperation(uint8_t section, OpRunwl * pOp)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;
	if(pOp == 0) return false;

	m_sectionOperation[section] = pOp;
	return true;
}

bool THRunProtocol::setOperation(uint8_t section, OpReadSec * pOp)
{
	if ( pOp == nullptr)					return false;
	if ( section >= SCT_NUM_TOT_SECTIONS )	return false;

	m_readSecOperation[section] = pOp;
	return true;
}


int THRunProtocol::requestAction(uint8_t section, uint8_t action)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return -1;

	m_mutexProtocol.lock();

	int ret = 0;

	if(m_sectionRequest[section] == REQUEST_NONE)
	{
		m_sectionRequest[section] = action;
		log(LOG_INFO, "THRunProtocol::requestAction %c %d (%d)", SECTION_CHAR_A + section, action, m_sectionStatus[section]);

		// register the start time of the request
		m_sectionTimeout[section] = getSecondsTime();

	}
	else if(m_sectionPending[section] == REQUEST_NONE)
	{
		m_sectionPending[section] = action;
		log(LOG_INFO, "THRunProtocol::pendingAction %c %d (%d)", SECTION_CHAR_A + section, action, m_sectionStatus[section]);
	}
	else
	{
		log(LOG_ERR, "THRunProtocol::requestAction FAILED %c %d (%d)", SECTION_CHAR_A + section, action, m_sectionStatus[section]);

        generateSectionSynchroFailure(section);
        // timeout on synchronization expired -> force protocol abort
        m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		ret = -1;
	}

	m_mutexProtocol.unlock();

	return ret;
}

int THRunProtocol::checkPendingAction(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return -1;

	m_mutexProtocol.lock();

	if(m_sectionPending[section] != REQUEST_NONE)
	{
		m_sectionRequest[section] = m_sectionPending[section];
		m_sectionPending[section] = REQUEST_NONE;
		log(LOG_INFO, "THRunProtocol::checkPendingAction %c %d (%d)", SECTION_CHAR_A + section,
										m_sectionRequest[section], m_sectionStatus[section]);

		// register the start time of the request
		m_sectionTimeout[section] = getSecondsTime();
	}
	m_mutexProtocol.unlock();
	return 0;
}

int THRunProtocol::perform(int section, uint8_t actionId)
{
	if ( actionId > 1 )		return -1;

	// first set the new status (if not the thread may stop immediately)
	m_mutexProtocol.lock();
	m_sectionStatus[section] = PROTO_STARTED;
	m_sectionPending[section] = REQUEST_NONE;
	m_sectionRequest[section] = REQUEST_NONE;
    m_sectionPending[section] = REQUEST_NONE;
	m_mutexProtocol.unlock();

	// change the internal status of the section. Protocol if 0, else ReadSec
	updateStatus(section, actionId ? eStatusReadsec : eStatusProtocol);

    // set Blu Led steady
    sectionBoardGeneralSingleton(section)->setLed(eLedBlue, eLedOn);

	// if not already running unlock the semaphore
	if(m_protocolActive == false)
	{
		int liRetVal =  m_synchSem.release();
		if ( liRetVal == SEM_FAILURE )
		{
			return eOperationError;
		}
	}

    log(LOG_INFO, "THRunProtocol::perform: starting on section %c", section + SECTION_CHAR_A);
	return eOperationRunning;
}


int THRunProtocol::startProtocol(int section)
{
	int errSection = 0;

	m_mutexProtocol.lock();
	// first action of a protocol: clear also pending action
	m_sectionRequest[section] = REQUEST_NONE;
	m_sectionPending[section] = REQUEST_NONE;
	m_protocolAbortedCommand[section] = false;
	m_sectionReplyOk[section] = true;	// no pending replies
	m_mutexProtocol.unlock();

	log(LOG_INFO, "THRunProtocol::startProtocol section %c", (SECTION_CHAR_A + section));

    // clean all pressure errors active
	sectionBoardManagerSingleton(section)->clearSectionPressureErrors();

	// reset protocol flags from section
	sectionBoardGeneralSingleton(section)->clearProtocolFlags();

	RunwlInfo * pRunwl = runwlInfoPointer(section);
	if(pRunwl == 0) return -1;

	// register protocol start time
	pRunwl->setStartTime();

#ifndef __DEBUG_PROTOCOL
	if(errSection == 0)
	{
		errSection = sectionBoardGeneralSingleton(section)->executeProtocol();

		if(errSection == 0)
		{
			// enable/disable pressure data collection
			uint8_t treatedPressure = 0;
			for(uint8_t i = 0; i < SCT_NUM_TOT_SLOTS; i++)
			{
                treatedPressure += pRunwl->getProtoTreatedPressure(i);
                treatedPressure += pRunwl->getProtoElaboratedPressure(i);
			}
			if(treatedPressure > 0)
			{
				// at least for one strip it's necessary to acquire pressure data
				THMonitorPressure::getInstance()->startProtocolMonitoring(section);
			}

			initializeTemperatureData(section);
		}
	}
#endif

	if(errSection == 0)
	{
		// register a new protocol has started on the section
		if(section == SCT_A_ID)
			infoSingleton()->addInstrumentCounter(eCounterProtocolsA, 1);
		else
			infoSingleton()->addInstrumentCounter(eCounterProtocolsB, 1);

		m_sectionStatus[section] = PROTO_RUNNING;

		// by default save the reply status as not executed
		composeDefaultRunwlReply(section);

		// register the event to log
		std::string tmpStr;
		char buffer[32];
		sprintf(buffer, "Protocol Started on Section %c",  section + SECTION_CHAR_A);
		tmpStr.assign(buffer);
		registerGenericEvent(tmpStr);

        // change the internal status of the NSH:
        // to be done here so that the status is set only if the protocol is really starting
        updateStatus(NSH_ID, eStatusProtocol);

	}
	else
	{
		m_sectionRequest[section] = REQUEST_NONE;
		// reset the status of the section
		m_sectionStatus[section] = PROTO_NOT_RUNNING;

        // reset the status of the section as not started
        updateStatus(section, eStatusIdle);
        // remove the section from the scheduler
        schedulerThreadPointer()->removeSection(section);
    }

#ifdef __DEBUG_PROTOCOL
	// build pressure and temperature debug file
//	buildDebugFile(section);
    m_sectionRequest[section] = REQUEST_NONE;
    m_sectionStatus[section] = PROTO_NOT_RUNNING;
//    sendRunwlReply(section, true);
    protocolReplyThreadPointer()->requestReplySection(section, m_sectionOperation[section],
                                                      true, m_protocolAbortedCommand[section]);
#endif

	return errSection;
}

int THRunProtocol::endProtocol(int section)
{
	// stop data collection ...
	stopDataAcquisition(section);

    // .. and position tray out (unlock cover)
    moveTrayOut(section);

	log(LOG_INFO, "THRunProtocol::endProtocol section %c", (SECTION_CHAR_A + section));

	m_sectionRequest[section] = REQUEST_WAIT_END_PROTOCOL;

	return 0;
}


int THRunProtocol::continueProtocol(int section)
{
	int errSection = 0;

	m_sectionRequest[section] = REQUEST_NONE;

	log(LOG_INFO, "THRunProtocol::continueProtocol section %c", (SECTION_CHAR_A + section));

	// send strip configuration to section and then start protocol execution
	errSection += sectionBoardGeneralSingleton(section)->continueProtocol();

	if(errSection == 0)
	{
		// continue protocol succesfull
		sectionBoardGeneralSingleton(section)->clearProtocolWindowFlag();
		return errSection;
	}

	log(LOG_ERR, "THRunProtocol::continueProtocol FAILED section %c", (SECTION_CHAR_A + section));

	// stop data collection ...
	stopDataAcquisition(section);

	// the continue is not OK -> clean scheduler, set the section in error and send reply to PC
	updateStatus(section, eStatusError);
	schedulerThreadPointer()->removeSection(section);
    protocolReplyThreadPointer()->requestReplySection(section, m_sectionOperation[section],
                                                      false, m_protocolAbortedCommand[section]);

//	sendRunwlReply(section, false);

	// reset the status of the section
	m_sectionStatus[section] = PROTO_NOT_RUNNING;
	return errSection;
}

int THRunProtocol::startReadSec(int section)
{
	m_mutexProtocol.lock();
	// first action of a protocol: clear also pending action
	m_sectionRequest[section] = REQUEST_NONE;
	m_sectionPending[section] = REQUEST_NONE;
	m_sectionReplyOk[section] = true;	// no pending replies
	m_mutexProtocol.unlock();

	log(LOG_INFO, "THRunProtocol::startReadSec section %c", (SECTION_CHAR_A + section));

	ReadSecInfo* pReadSec = readSecInfoPointer(section);
	if( pReadSec == nullptr )	return -1;

	// register protocol start time
	pReadSec->setStartTime();

	// change the internal status of the devices involved
	updateStatus(section, eStatusReadsec);
	updateStatus(NSH_ID, eStatusProtocol);
	updateStatus(CAMERA_ID, eStatusReadsec);

	// register a new protocol has started on the section
	if ( section == SCT_A_ID )	infoSingleton()->addInstrumentCounter(eCounterReadSecA, 1);
	else						infoSingleton()->addInstrumentCounter(eCounterReadSecB, 1);

	m_sectionStatus[section] = PROTO_RUNNING;

	// register the event to log
	string tmpStr = "ReadSec Started on Section ";
	tmpStr += (section + SECTION_CHAR_A);
	registerGenericEvent(tmpStr);

	return 0;
}

int THRunProtocol::moveMotors(vector<pair<eSectionMotor, string>> vMotorsSection, pair<eTarget, string> pMotorNsh, int section, int liNextRequest)
{
	m_mutexProtocol.lock();
	m_sectionRequest[section] = REQUEST_NONE;

	bool bRes = true;
	int liDevicesMoved = 0;

	if ( vMotorsSection.size() )
	{
		liDevicesMoved |= 1;
		bRes = sectionBoardManagerSingleton(section)->requestMoveSectionMotors(vMotorsSection);
	}

	if ( bRes && pMotorNsh.second.size() )
	{
		liDevicesMoved |= 2;
		bRes = nshBoardManagerSingleton()->requestMoveNshMotor(section, pMotorNsh.second, pMotorNsh.first);
	}

	m_mutexProtocol.unlock();

	// retrieve the steps to position the reader on the first strip (left side) of the section
	if ( bRes )
	{
		m_sectionRequest[section] = liNextRequest;
	}
	else
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		return -1;
	}

	// wait until the actions end or timeout elapses
	while (1)
	{
		switch ( liDevicesMoved )
		{
			case 1:
				if ( ! sectionBoardManagerSingleton(section)->isActionInProgress() )
				{
					m_pLogger->log(LOG_INFO, "THRunProtocol::moveMotors: section motion completed");
					return 0;
				}
			break;

			case 2:
				if ( ! nshBoardManagerSingleton()->isActionInProgress() )
				{
					m_pLogger->log(LOG_INFO, "THRunProtocol::moveMotors: nsh motion completed");
					return 0;
				}
			break;

			case 3:
				if ( ! nshBoardManagerSingleton()->isActionInProgress() && ! sectionBoardManagerSingleton(section)->isActionInProgress() )
				{
					m_pLogger->log(LOG_INFO, "THRunProtocol::moveMotors: nsh and section motion completed");
					return 0;
				}
			break;

			default:
				return 0;
			break;
		}

		if ( (getSecondsTime() - m_sectionTimeout[section]) > READSEC_ACTION_TIMEOUT )
		{
			m_pLogger->log(LOG_ERR, "THRunProtocol::moveMotors: timeout expired");
			// timeout on action expired -> force protocol abort
			m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
			// set the error to be sent to SW
			masterBoardManagerSingleton()->setErrorFromCode(ERR_PROTOCOL_TIMEOUT, true);
			return -1;
		}
	}
}

int THRunProtocol::checkStripReadSec(int section)
{
	m_mutexProtocol.lock();

	m_sectionRequest[section] = REQUEST_NONE;

	ReadSecInfo* pReadSec = readSecInfoPointer(section);
	if ( pReadSec == nullptr )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	vector<bool> vSlotsEnabled;
	pReadSec->getSlotsEnabled(vSlotsEnabled, eTarget::eSubstrate);

	// execute the reading only on the requested strips
	vector<int> vSlots;
	for ( size_t i = 0; i != vSlotsEnabled.size(); ++i )
	{
		if ( vSlotsEnabled[i] )	vSlots.push_back(i);
	}

	if ( ! nshBoardManagerSingleton()->requestCheckStripPresence(section, vSlots) )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	m_sectionRequest[section] = REQUEST_WAIT_NSH_READ_PRESENCE;
	m_mutexProtocol.unlock();
	return 0;
}

int THRunProtocol::getSubstrateRead(int section)
{
	m_mutexProtocol.lock();
	m_sectionRequest[section] = REQUEST_NONE;

	ReadSecInfo* pReadSec = readSecInfoPointer(section);
	if ( pReadSec == nullptr )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	// vector is of size SCT_NUM_TOT_SLOTS*3 because "getReadSection" expects so,
	// but I am actually using only SCT_NUM_TOT_SLOTS values in this case
	vector<int> vReadingValues(SCT_NUM_TOT_SLOTS * 3);
	nshBoardManagerSingleton()->getReadSection(section, vReadingValues.data());    
	pReadSec->setStripValuesRead(vReadingValues);
	m_mutexProtocol.unlock();

	return 0;
}

int THRunProtocol::getTrueEventNumber(int* pBC, int* pDM, int* pWell0, int* pWell3, int liSection)
{
	m_mutexProtocol.lock();
	m_sectionRequest[liSection] = REQUEST_NONE;


	ReadSecInfo* pReadSec = readSecInfoPointer(liSection);
	if ( pReadSec == nullptr || pBC == nullptr || pDM == nullptr ||
		 pWell0 == nullptr || pWell3 == nullptr  )
	{
		m_sectionRequest[liSection] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	pReadSec->getTrueEventNumAndUpdRes(pBC, pDM, pWell0, pWell3);
	m_mutexProtocol.unlock();

	return 0;
}

int THRunProtocol::moveBeforeDecodingReadSec(int section, int target, int liNextRequest)
{
	if ( section >= SCT_NUM_TOT_SECTIONS )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		return -1;
	}

	m_sectionRequest[section] = REQUEST_NONE;
	ReadSecInfo* pReadSec = readSecInfoPointer(section);
	if ( pReadSec == nullptr )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		return -1;
	}

	// If is the first slot, then the tray has to be moved in the correct position
	int liLastSlotId = pReadSec->getCurrentSlot();

	if ( liLastSlotId == SCT_SLOT_1_ID )
	{
		vector<pair<eSectionMotor, string>> vMotorsSection;
		switch ( eTarget(target) )
		{
            case eTarget::eConeAbsence:
			case eTarget::eSpr:
			case eTarget::eStrip:
				vMotorsSection.push_back({eSectionMotor::eTray, "Strip_BC"});
			break;

			case eTarget::eSubstrate:
				vMotorsSection.push_back({eSectionMotor::eTray, "Read"});
			break;

			case eTarget::eSample0:
				vMotorsSection.push_back({eSectionMotor::eTray, "W0"});
			break;

			case eTarget::eSample3:
				vMotorsSection.push_back({eSectionMotor::eTray, "W3"});
			break;

			case eTarget::eNone:
			break;
		}

		moveMotors(vMotorsSection, {}, section, REQUEST_NONE);
	}

	m_mutexProtocol.lock();

	// Find slot to be moved
	vector<bool> vSlotsEnabled;
	pReadSec->getSlotsEnabled(vSlotsEnabled, eTarget(target));
	vector<int> vRFUValues;
	pReadSec->getStripValuesRead(vRFUValues);

	int liSlotId = -1;
	for ( int i = liLastSlotId; i < SCT_NUM_TOT_SLOTS; ++i )
	{
		if ( vSlotsEnabled[i] )
		{
            if ( eTarget(target) == eTarget::eConeAbsence )
            {
                if ( ! pReadSec->getDataMatrixRead(i).compare("______") )
                {
                    liSlotId = i;
                    break;
                }
                else
                {
                    continue;
                }
			}

			// Spr reading is the only one to be performed even though the strip is not present
			if ( eTarget(target) == eTarget::eSpr )
			{
				liSlotId = i;
				break;
			}

			/** ******************************************************************************
			 * We dont' want to perform strip reading or sample detection if the strip is not
			 * present
			 ****************************************************************************** **/
			vector<bool> vSubstrateEnabled;
			pReadSec->getSlotsEnabled(vSubstrateEnabled, eTarget::eSubstrate);
			if ( vSubstrateEnabled[i] && vRFUValues[i] < RFU_THRESHOLD_STRIP )
			{
				// check next slot
				pReadSec->setCurrentSlot(pReadSec->getCurrentSlot()+1);
				continue;
			}

			liSlotId = i;
			break;
		}
	}

	// No decoding action has to be performed
	if ( liSlotId == -1 )
	{
		// Send reply only if is the last action. The order is: BC, DM, W0, W3.
		switch ( eTarget(target) )
		{
			case eTarget::eStrip:
				if ( ! pReadSec->hasDmToBeDecoded() && ! pReadSec->hasSampleW0ToBeDetected() &&
					 ! pReadSec->hasSampleW3ToBeDetected() )
				{
					m_sectionRequest[section] = REQUEST_END_READSEC;
				}
			break;

			case eTarget::eSpr:
				if ( ! pReadSec->hasSampleW0ToBeDetected() && ! pReadSec->hasSampleW3ToBeDetected() )
				{
					m_sectionRequest[section] = REQUEST_END_READSEC;
				}
			break;


			case eTarget::eSample0:
				if ( ! pReadSec->hasSampleW3ToBeDetected() )
				{
					m_sectionRequest[section] = REQUEST_END_READSEC;
				}
			break;

			default:
				m_sectionRequest[section] = REQUEST_END_READSEC;
			break;
		}

		pReadSec->setCurrentSlot(SCT_SLOT_1_ID); // Default
		m_mutexProtocol.unlock();
		return 0;
	}

	// Update with the correct slot
	pReadSec->setCurrentSlot(liSlotId);

	// execute the movement only on the requested strips
	if ( ! nshBoardManagerSingleton()->requestMoveNshMotor(section, to_string(liSlotId+1), eTarget(target)) )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	m_sectionRequest[section] = liNextRequest;
	m_mutexProtocol.unlock();
	return 0;
}

int THRunProtocol::decodeReadSec(int section, int target)
{
	if ( eTarget(target) != eTarget::eStrip && eTarget(target) != eTarget::eSpr )	return -1;

	m_mutexProtocol.lock();

	m_sectionRequest[section] = REQUEST_NONE;

	ReadSecInfo* pReadSec = readSecInfoPointer(section);
	if ( pReadSec == nullptr )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	int liRes = 0;
	int liCurrSlot = pReadSec->getCurrentSlot();
	if ( eTarget(target) == eTarget::eStrip )
		liRes = cameraBoardManagerSingleton()->requestDecodeBarcode(section, liCurrSlot, pReadSec->hasPictureToBeTakenBC(liCurrSlot));
	else
		liRes = cameraBoardManagerSingleton()->requestDecodeDataMatrix(section, liCurrSlot, pReadSec->hasPictureToBeTakenDM(liCurrSlot));

	if ( liRes )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}
	if ( eTarget(target) == eTarget::eStrip)
	{
		m_sectionRequest[section] = REQUEST_WAIT_WHILE_DECODING_BC;
	}
	else
	{
		m_sectionRequest[section] = REQUEST_WAIT_WHILE_DECODING_DM;
	}
	m_mutexProtocol.unlock();
	return 0;
}

int THRunProtocol::checkSampleReadSec(int section, int target)
{
	if ( eTarget(target) != eTarget::eSample0 && eTarget(target) != eTarget::eSample3 )	return -1;

	m_mutexProtocol.lock();

	m_sectionRequest[section] = REQUEST_NONE;

	ReadSecInfo* pReadSec = readSecInfoPointer(section);
	if ( pReadSec == nullptr )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	int liRes = 0;
	int liCurrSlot = pReadSec->getCurrentSlot();
	int liWell = ( eTarget(target) == eTarget::eSample0 ) ? 0 : 3;
	bool bTakePicture = ( ! liWell ) ? pReadSec->hasPictureToBeTakenW0(liCurrSlot) : pReadSec->hasPictureToBeTakenW3(liCurrSlot);
	liRes = cameraBoardManagerSingleton()->requestCheckSamplePresence(liWell, section, liCurrSlot, bTakePicture);
	if ( liRes )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	if ( eTarget(target) == eTarget::eSample0)
	{
		m_sectionRequest[section] = REQUEST_WAIT_WHILE_CHECKING_W0;
	}
	else
	{
		m_sectionRequest[section] = REQUEST_WAIT_WHILE_CHECKING_W3;
	}
	m_mutexProtocol.unlock();
	return 0;
}

int THRunProtocol::checkSprReadSec(int section)
{
    m_mutexProtocol.lock();

    m_sectionRequest[section] = REQUEST_NONE;

    ReadSecInfo* pReadSec = readSecInfoPointer(section);
    if ( pReadSec == nullptr )
    {
        m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
        m_mutexProtocol.unlock();
        return -1;
    }

	int liCurrSlot = pReadSec->getCurrentSlot();
	int liRes = cameraBoardManagerSingleton()->requestCheckSprPresence(section, liCurrSlot, pReadSec->hasPictureToBeTakenDM(liCurrSlot));
    if ( liRes )
    {
        m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
        m_mutexProtocol.unlock();
        return -1;
    }

    m_sectionRequest[section] = REQUEST_WAIT_WHILE_CHECKING_SPR;
    m_mutexProtocol.unlock();
    return 0;
}

int THRunProtocol::updateResultReadSec(int section, int target)
{
	m_mutexProtocol.lock();
	m_sectionRequest[section] = REQUEST_NONE;

	ReadSecInfo* pReadSec = readSecInfoPointer(section);
	if ( pReadSec == nullptr )
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	string strCode("");
	vector<unsigned char> vImage;
	switch ( eTarget(target) )
	{
		case eTarget::eStrip:
		{
			cameraBoardManagerSingleton()->getDecodedCode(strCode, vImage, section, pReadSec->getCurrentSlot());
			pReadSec->setBarcodeRead(strCode, pReadSec->getCurrentSlot());
			pReadSec->setBarcodeImage(vImage, pReadSec->getCurrentSlot());
			m_sectionRequest[section] = REQUEST_MOVE_BEFORE_DECODING_BC;
		}
		break;

		case eTarget::eSpr:
		case eTarget::eConeAbsence:
		{
			cameraBoardManagerSingleton()->getDecodedCode(strCode, vImage, section, pReadSec->getCurrentSlot());
			pReadSec->setDataMatrixRead(strCode, pReadSec->getCurrentSlot());
			pReadSec->setDataMatrixImage(vImage, pReadSec->getCurrentSlot());
			if ( eTarget(target) == eTarget::eSpr)  m_sectionRequest[section] = REQUEST_MOVE_BEFORE_DECODING_DM;
			else                                    m_sectionRequest[section] = REQUEST_MOVE_BEFORE_CHECKING_SPR;
		}
		break;

		case eTarget::eSample0:
		{
			float fResult;
			cameraBoardManagerSingleton()->getSamplePresenceResult(fResult, vImage, section, pReadSec->getCurrentSlot());
			pReadSec->setSampleImage(vImage, 0, pReadSec->getCurrentSlot());
			int liCurrSlot = pReadSec->getCurrentSlot();
			pReadSec->setSampleResult(fResult, 0, liCurrSlot, pReadSec->getSampleDetectionSettingId(liCurrSlot, 0));
			m_sectionRequest[section] = REQUEST_MOVE_BEFORE_CHECKING_W0;
		}
		break;

		case eTarget::eSample3:
		{
			float fResult;
			vector<unsigned char> vImage;
			cameraBoardManagerSingleton()->getSamplePresenceResult(fResult, vImage, section, pReadSec->getCurrentSlot());
			pReadSec->setSampleImage(vImage, 3, pReadSec->getCurrentSlot());
			int liCurrSlot = pReadSec->getCurrentSlot();
			pReadSec->setSampleResult(fResult, 3, liCurrSlot, pReadSec->getSampleDetectionSettingId(liCurrSlot, 3));
			m_sectionRequest[section] = REQUEST_MOVE_BEFORE_CHECKING_W3;
		}
		break;

		default:
		break;
	}

	// Update current slot with the next one
	pReadSec->setCurrentSlot(pReadSec->getCurrentSlot()+1);
	m_sectionTimeout[section] = getSecondsTime();
	m_mutexProtocol.unlock();

	return 0;
}

int THRunProtocol::moveNshToSection(int section)
{
	int ret = 0;

	m_mutexProtocol.lock();
	m_sectionRequest[section] = REQUEST_NONE;

	// retrieve the steps to position the reader on the first strip (left side) of the section
	if(nshBoardManagerSingleton()->requestMoveSection(section) == false)
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		ret = -1;
	}
	else
	{
		m_sectionRequest[section] = REQUEST_WAIT_NSH_MOVEMENT;
	}
	m_mutexProtocol.unlock();
	return ret;
}

int THRunProtocol::readProtocol(int section)
{
	m_mutexProtocol.lock();

	m_sectionRequest[section] = REQUEST_NONE;

	RunwlInfo * pRunwl = runwlInfoPointer(section);
	if(pRunwl == 0)
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	uint8_t rfuConv[SCT_NUM_TOT_SLOTS];

	// read the RFU conversion flag
	for(uint8_t i = 0; i < SCT_NUM_TOT_SLOTS; i++)
	{
		rfuConv[i] = pRunwl->getStripRfuconv(i);
	}

	// execute the reading on all strips
	if(nshBoardManagerSingleton()->readSectionProtocol(section, rfuConv) == false)
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}
	m_sectionRequest[section] = REQUEST_WAIT_NSH_READING;
	m_mutexProtocol.unlock();
	return 0;
}

int THRunProtocol::getReadProtocol(int section)
{
	m_mutexProtocol.lock();
	m_sectionRequest[section] = REQUEST_NONE;

	RunwlInfo * pRunwl = runwlInfoPointer(section);
	if(pRunwl == 0)
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	// identify, for the enabled strips, the reading index
	uint8_t readingIndex = pRunwl->findReadingToSet();

	if(readingIndex == J_MAXMIN_VECTOR_SIZE)
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	int32_t readingValues[SCT_NUM_TOT_SLOTS * 3];

	// get the read values of the whole section
	nshBoardManagerSingleton()->getReadSection(section, readingValues);

	time_t readingTime = getSecondsTime();

	// loop on all active strips to set the values
	for(uint8_t strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
	{
		if(pRunwl->getStripId(strip).empty() == false)
		{
			// valid strip -> set time
			pRunwl->setStripReadingIndexTime(strip, readingIndex, (uint64_t)readingTime);
			// calculate min, max, mean on the readings

			int32_t minReading = readingValues[strip * 3];
			int32_t maxReading = readingValues[strip * 3];
			int32_t meanReading = readingValues[strip * 3];
			for(uint8_t cnt = 1; cnt < 3; cnt++)
			{
				if(readingValues[strip * 3 + cnt] < minReading)
				{
					minReading = readingValues[strip * 3 + cnt];
				}
				else if(readingValues[strip * 3 + cnt] > maxReading)
				{
					maxReading = readingValues[strip * 3 + cnt];
				}
				meanReading += readingValues[strip * 3 + cnt];
			}

			// You always have to discard minimum value
			meanReading -= minReading;
			meanReading /= 2;
			pRunwl->setStripReadingIndexValues(strip, readingIndex, minReading, maxReading, meanReading);
			log(LOG_INFO, "THRunProtocol::readProtocol values section %c index %d = %d",
				(SECTION_CHAR_A + section), readingIndex, meanReading);
		}
	}
	m_mutexProtocol.unlock();

	return 0;
}

int THRunProtocol::airProtocol(int section)
{
	m_mutexProtocol.lock();

	m_sectionRequest[section] = REQUEST_NONE;

	// execute the reading on all strips
	if(nshBoardManagerSingleton()->readSectionAir(section) == false)
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}
	m_sectionRequest[section] = REQUEST_WAIT_AIR_READING;
	m_mutexProtocol.unlock();
	return 0;
}

int THRunProtocol::getReadAir(int section)
{
	m_mutexProtocol.lock();
	m_sectionRequest[section] = REQUEST_NONE;

	RunwlInfo * pRunwl = runwlInfoPointer(section);
	if(pRunwl == 0)
	{
		m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
		m_mutexProtocol.unlock();
		return -1;
	}

	time_t readingTime = getSecondsTime();

	int32_t readingValues[SCT_NUM_TOT_SLOTS * 3];

	// get the Air read values of the whole section: only one read value per strip
	nshBoardManagerSingleton()->getReadSection(section, readingValues);
	// and store them
	for(uint8_t strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
	{
		pRunwl->setStripReadingAirTime(strip, readingTime);
		pRunwl->setStripReadingAir(strip, readingValues[strip]);
	}

	m_mutexProtocol.unlock();

	return 0;
}

bool THRunProtocol::abortProtocolFromCommand(uint8_t section)
{
	if(m_protocolAbortedCommand[section] == true ||
		m_sectionRequest[section] == REQUEST_ABORT_PROTOCOL ||
		m_sectionStatus[section] == PROTO_IN_ABORT )
	{
		// command already received : abort already ongoing
		return false;
	}

	m_protocolAbortedCommand[section] = true;
	m_sectionRequest[section] = REQUEST_ABORT_PROTOCOL;
    return true;
}

int THRunProtocol::abortProtocol(int section, bool bSendCommand)
{
	int errSection = 0;

	m_sectionStatus[section] = PROTO_IN_ABORT;
    // clear any pending action to avoid other actions
    m_sectionPending[section] = REQUEST_NONE;

	log(LOG_INFO, "THRunProtocol::abortProtocol section %c", (SECTION_CHAR_A + section));

	// stop data collection ...
	stopDataAcquisition(section);

	// send abort to section
    if(bSendCommand)
    {
        // if the abort is generated from the section itself no need to send the Abort command
        errSection += sectionBoardGeneralSingleton(section)->abortProtocol();
    }

	m_sectionRequest[section] = REQUEST_WAIT_END_PROTOCOL;

	return errSection;
}

int THRunProtocol::stoppedProtocol(int section)
{
	char buffer[32];

    // protocol is over, wait for the user to open the door
    updateStatus(section, eStatusUnload);

    if(m_sectionStatus[section] == PROTO_IN_ABORT)
	{
        // set the Section Led Red Blink
        sectionBoardGeneralSingleton(section)->setLed(eLedRed, eLedBlinkSlow);

        // the protocol is aborted -> clean scheduler, set the section in error and send reply to PC
		schedulerThreadPointer()->removeSection(section);
        protocolReplyThreadPointer()->requestReplySection(section, m_sectionOperation[section],
                                                          false, m_protocolAbortedCommand[section]);

		log(LOG_INFO, "THRunProtocol::abortedProtocol section %c", (SECTION_CHAR_A + section));

		// register the event to log
		sprintf(buffer, "Protocol Aborted on Section %c",  section + SECTION_CHAR_A);

        m_sectionRequest[section] = REQUEST_ERROR_UNLOAD;
    }
	else
	{
        // set the Section Led Blue blink
        sectionBoardGeneralSingleton(section)->setLed(eLedBlue, eLedBlinkSlow);

        protocolReplyThreadPointer()->requestReplySection(section, m_sectionOperation[section],
                                                          true, m_protocolAbortedCommand[section]);

		log(LOG_INFO, "THRunProtocol::stoppedProtocol section %c", (SECTION_CHAR_A + section));

		// register the event to log
		sprintf(buffer, "Protocol Completed on Section %c",  section + SECTION_CHAR_A);

        m_sectionRequest[section] = REQUEST_UNLOAD;
    }

	// register the event to log
	std::string tmpStr;
	tmpStr.assign(buffer);
	registerGenericEvent(tmpStr);

    // set the local status of the section
    m_sectionStatus[section] = PROTO_UNLOAD;

    // check if the other section is running before setting NSH in idle
    endProtocolNsh(section);

    // update the status of the Module led
    requestModuleLed();

	return 0;
}

int THRunProtocol::sendReplyReadSec(int section)
{
	char buffer[32];

	if ( m_sectionStatus[section] == PROTO_IN_ABORT )
	{
		// set the Section Led Red Blink
		sectionBoardGeneralSingleton(section)->setLed(eLedRed, eLedBlinkSlow);

		// the protocol is aborted -> clean scheduler, set the section in error and send reply to PC
		schedulerThreadPointer()->removeSection(section);
		protocolReplyThreadPointer()->requestReplyReadSec(section, m_readSecOperation[section], false);
		log(LOG_INFO, "THRunProtocol::endReadSec: error section %c", (SECTION_CHAR_A + section));

		// register the event to log
		sprintf(buffer, "ReadSec error on Section %c",  section + SECTION_CHAR_A);

		updateStatus(section, eStatusError);
	}
	else
	{
		// set the Section Led Green On
		sectionBoardGeneralSingleton(section)->setLed(eLedGreen, eLedOn);

		protocolReplyThreadPointer()->requestReplyReadSec(section, m_readSecOperation[section], true);
		log(LOG_INFO, "THRunProtocol::endReadSec: ok section %c", (SECTION_CHAR_A + section));
		// register the event to log
		sprintf(buffer, "ReadSec completed on section %c",  section + SECTION_CHAR_A);

		updateStatus(section, eStatusIdle);
	}

	// register the event to log
	std::string tmpStr;
	tmpStr.assign(buffer);
	registerGenericEvent(tmpStr);

	// set the local status of the section
	m_sectionStatus[section] = PROTO_NOT_RUNNING;
	m_sectionRequest[section] = REQUEST_NONE;

	// Update status NSH and Camera
	eDeviceId otherSection = eIdMax;
	if ( section == SCT_A_ID )		otherSection = eIdSectionB;
	else							otherSection = eIdSectionA;

	// nsh status has to be updated to idle only if the other section does not require it
	if ( stateMachineSingleton()->getDeviceStatus(eIdNsh) == eStatusProtocol &&
		 stateMachineSingleton()->getDeviceStatus(otherSection) != eStatusProtocol &&
		 stateMachineSingleton()->getDeviceStatus(otherSection) != eStatusReadsec )
	{
		updateStatus(NSH_ID, eStatusIdle);
	}

	// camera status has to be updated to idle only if the other section does not require it
	if ( stateMachineSingleton()->getDeviceStatus(eIdCamera) == eStatusReadsec &&
		 stateMachineSingleton()->getDeviceStatus(otherSection) != eStatusReadsec )
	{
		updateStatus(CAMERA_ID, eStatusIdle);
	}

	// TODO: to be decommented??
//	  // update the status of the Module led
//    requestModuleLed();

	return 0;
}

int THRunProtocol::abortReadSec(int section)
{
	int errSection = 0;

	m_sectionStatus[section] = PROTO_IN_ABORT;
	log(LOG_INFO, "THRunProtocol::abortProtocol section %c", (SECTION_CHAR_A + section));
	m_sectionRequest[section] = REQUEST_END_READSEC;

	return errSection;
}

bool THRunProtocol::isSectionUnloadInError(uint8_t section)
{
    if(m_sectionStatus[section] != PROTO_UNLOAD)
    {
        return false;
    }

    if(m_sectionRequest[section] == REQUEST_ERROR_UNLOAD)
    {
        // the section is in Unload but with error
        return true;
    }
    return false;
}

int THRunProtocol::moveTrayOut(uint8_t section)
{
    std::string strCoded = sectionBoardLinkSingleton(section)->m_pTray->getCodedFromLabel("Out");
    SectionCommonMessage* pCommonMessage = &sectionBoardLinkSingleton(section)->m_pTray->m_CommonMessage;

    char ubPosCode = *strCoded.c_str();
    m_pLogger->log(LOG_INFO, "OpMove::executeMoveSectionPredefined: CMD= %X", ubPosCode);

    return pCommonMessage->moveToCodedPosition(ubPosCode, TH_MOVE_CMD_TIMEOUT);
}

void THRunProtocol::beforeWorkerThread(void)
{
    log(LOG_DEBUG, "THRunProtocol::started");
}

int THRunProtocol::workerThread(void)
{
	// register in the scheduler thread the callback functions
	schedulerThreadPointer()->registerFunction(E_START_PROTO, THRunProtocol_startProtocol);
	schedulerThreadPointer()->registerFunction(E_END_PROTO, THRunProtocol_endProtocol);
	schedulerThreadPointer()->registerFunction(E_RESTART_PROTO, THRunProtocol_continueProtocol);
	schedulerThreadPointer()->registerFunction(E_FLUO_READ, THRunProtocol_readProtocol);
	schedulerThreadPointer()->registerFunction(E_SH_MOVE_FOR_READING, THRunProtocol_moveNshProtocol);
	schedulerThreadPointer()->registerFunction(E_MEASURE_ON_AIR, THRunProtocol_readAir);

	schedulerThreadPointer()->registerFunction(E_START_READ_SEC, THRunProtocol_startReadSec);
	schedulerThreadPointer()->registerFunction(E_GET_READY_READ_SEC, THRunProtocol_getReadyReadSec);
	schedulerThreadPointer()->registerFunction(E_CHECK_STRIP_PRESENCE, THRunProtocol_checkStripPresenceReadSec);	
	schedulerThreadPointer()->registerFunction(E_DECODE_BC, THRunProtocol_decodeBCReadSec);
	schedulerThreadPointer()->registerFunction(E_DECODE_DM, THRunProtocol_decodeDMReadSec);
	schedulerThreadPointer()->registerFunction(E_CHECK_SAMPLE0_PRESENCE, THRunProtocol_checkSample0ReadSec);
	schedulerThreadPointer()->registerFunction(E_CHECK_SAMPLE3_PRESENCE, THRunProtocol_checkSample3ReadSec);
    schedulerThreadPointer()->registerFunction(E_CHECK_CONE_PRESENCE, ThRunProtocol_checkSprPresenceReadSec);
	schedulerThreadPointer()->registerFunction(E_RESCHEDULE_EVENT, THRunProtocol_rescheduleEvent);
	schedulerThreadPointer()->registerFunction(E_SET_DEFAULT_READ_SEC, THRunProtocol_setDefaultReadSec);
	schedulerThreadPointer()->registerFunction(E_SEND_REPLY_READSEC, THRunProtocol_replyReadSec);

	// read from file (if present) if the last replies have been processed correctly
	m_sectionReplyOk[SCT_A_ID] = readRunReplyOk(SCT_A_ID);
	m_sectionReplyOk[SCT_B_ID] = readRunReplyOk(SCT_B_ID);

	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "THRunProtocol::th: unlocked but wait returned %d", liSynchRet);
		}

		m_protocolActive = true;
		bool bGetTrueEventNumber = false;
		log(LOG_INFO, "THRunProtocol::th: unlocked %d", m_protocolActive);

		// thread enabled as long as at least one section is in protocol
		while(m_sectionStatus[SCT_A_ID] != PROTO_NOT_RUNNING ||
			  m_sectionStatus[SCT_B_ID] != PROTO_NOT_RUNNING )
		{

			// process the request received from SchedulerThread
			for(uint8_t i = 0; i < SCT_NUM_TOT_SECTIONS; i++)
			{
				if(m_sectionStatus[i] == PROTO_NOT_RUNNING) continue;

				switch(m_sectionRequest[i])
				{
					case REQUEST_START_PROTOCOL :
						// start the section execution
						startProtocol(i);
					break;

					case REQUEST_CONTINUE_PROTOCOL :
						// restart the section execution after the reading
						continueProtocol(i);
					break;

					case REQUEST_END_PROTOCOL :
						// if the section has notified the ProtocolEnd -> proceed
                        uint8_t ubStatus;
                        if(sectionBoardGeneralSingleton(i)->getInternalStatus(ubStatus) == -1)
                        {
                            // error in communication -> abort
                            m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
                        }
                        // if the section has sent the message or if the status is the expected one
                        // (message lost ?)
                        else if((sectionBoardGeneralSingleton(i)->getProtocolEndFlag() == true) ||
                                (ubStatus == eSectionIdle) )
                        {
                            sectionBoardGeneralSingleton(i)->clearProtocolEndFlag();
                            endProtocol(i);
                        }
                        else if((getSecondsTime() - m_sectionTimeout[i]) > PROTOCOL_SYNCHRO_TIMEOUT)
                        {
                            generateSectionSynchroFailure(i);
                            // timeout on synchronization expired -> force protocol abort
                            m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
                        }
                    break;

					case REQUEST_MOVE_NSH_PROTOCOL :
						// move nsh before starting the reading phase
						moveNshToSection(i);
					break;

					case REQUEST_READ_PROTOCOL :
						// if the section has notified the Jwindow -> proceed
						if(m_sectionStatus[i] != PROTO_IN_ABORT)
						{
                            uint8_t ubStatus;
                            if(sectionBoardGeneralSingleton(i)->getInternalStatus(ubStatus) == -1)
                            {
                                // error in communication -> abort
                                m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
                            }
                            // if the section has sent the message or if the status is the expected one
                            // (message lost ?)
                            else if((sectionBoardGeneralSingleton(i)->getProtocolWindowFlag() == true) ||
                                (ubStatus == eSectionPause) )
                            {
                                sectionBoardGeneralSingleton(i)->clearProtocolWindowFlag();
                                readProtocol(i);
                            }
                            else if((getSecondsTime() - m_sectionTimeout[i]) > PROTOCOL_SYNCHRO_TIMEOUT)
                            {
                                generateSectionSynchroFailure(i);
                                // timeout on synchronization expired -> force protocol abort
                                m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
                            }
                        }
					break;

					case REQUEST_READ_AIR :
						// if the section has notified the Protocol End -> proceed
						if(m_sectionStatus[i] != PROTO_IN_ABORT)
						{
                            uint8_t ubStatus;
                            if(sectionBoardGeneralSingleton(i)->getInternalStatus(ubStatus) == -1)
                            {
                                // error in communication -> abort
                                m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
                            }
                            // if the section has sent the message or if the status is the expected one
                            // (message lost ?)
                            else if((sectionBoardGeneralSingleton(i)->getProtocolEndFlag() == true) ||
                                    (ubStatus == eSectionIdle) )
                            {
                                // the endProtocol flag must not be cleared as it is used also for the ProtocolEnd check
                                airProtocol(i);
                            }
                            else if((getSecondsTime() - m_sectionTimeout[i]) > PROTOCOL_SYNCHRO_TIMEOUT)
                            {
                                generateSectionSynchroFailure(i);
                                // timeout on synchronization expired -> force protocol abort
                                m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
                                // set the error to be sent to SW
                                masterBoardManagerSingleton()->setErrorFromCode(ERR_PROTOCOL_SYNCHRONIZATION, true);
                            }

						}
					break;

					case REQUEST_ABORT_PROTOCOL :
						// protocol has to be aborted from external command (only once !)
						if(m_sectionStatus[i] != PROTO_IN_ABORT)
						{
                            abortProtocol(i, true);
						}
					break;

					case REQUEST_WAIT_NSH_MOVEMENT :
						if(nshBoardManagerSingleton()->isActionInProgress() == false)
						{
							m_sectionRequest[i] = REQUEST_NONE;
						}
						else if((getSecondsTime() - m_sectionTimeout[i]) > PROTOCOL_ACTION_TIMEOUT)
						{
							// timeout on action expired -> force protocol abort
							m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
							// set the error to be sent to SW
							masterBoardManagerSingleton()->setErrorFromCode(ERR_PROTOCOL_TIMEOUT, true);
						}
					break;

					case REQUEST_WAIT_NSH_READING :
						if(nshBoardManagerSingleton()->isActionInProgress() == false)
						{
							getReadProtocol(i);
						}
						else if((getSecondsTime() - m_sectionTimeout[i]) > PROTOCOL_ACTION_TIMEOUT)
						{
							// timeout on action expired -> force protocol abort
							m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
							// set the error to be sent to SW
							masterBoardManagerSingleton()->setErrorFromCode(ERR_PROTOCOL_TIMEOUT, true);
						}
					break;

					case REQUEST_WAIT_AIR_READING :
						if(nshBoardManagerSingleton()->isActionInProgress() == false)
						{
							getReadAir(i);
						}
						else if((getSecondsTime() - m_sectionTimeout[i]) > PROTOCOL_ACTION_TIMEOUT)
						{
							// timeout on action expired -> force protocol abort
							m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
							// set the error to be sent to SW
							masterBoardManagerSingleton()->setErrorFromCode(ERR_PROTOCOL_TIMEOUT, true);
						}
					break;

					case REQUEST_WAIT_END_PROTOCOL:
						// wait for the pressure thread to stop
						if(THMonitorPressure::getInstance()->isActionPending(i) == false)
						{
							stoppedProtocol(i);
						}
					break;

					case REQUEST_START_READSEC:
						m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_START_READSEC");
						// start the section execution
						startReadSec(i);
					break;

					case REQUEST_GETREADY_READSEC:
					{
						m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_GETREADY_READSEC");
						// Note the order the vector is built, LIFO method
						vector<pair<eSectionMotor, string>> vMotorsSection;
						vMotorsSection.push_back({eSectionMotor::eSpr, "Data_matrix"});
						vMotorsSection.push_back({eSectionMotor::eSpr, "Home"});
						vMotorsSection.push_back({eSectionMotor::eTray, "Home"});
						vMotorsSection.push_back({eSectionMotor::eTower, "Up"});

						pair<eTarget, string> pMotorNsh({eTarget::eNone, to_string(SCT_SLOT_1_ID+1)});
						moveMotors(vMotorsSection, pMotorNsh, i, REQUEST_NONE);
					}
					break;

					case REQUEST_CHECK_STRIP_PRESENCE:
						if ( m_sectionStatus[i] != PROTO_IN_ABORT )
						{
							// Move tray to correct position
							vector<pair<eSectionMotor, string>> vMotorsSection;
							vMotorsSection.push_back({eSectionMotor::eTray, "Read"});
							moveMotors(vMotorsSection, {}, i, REQUEST_NONE);

							m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_CHECK_STRIP_PRESENCE");
							uint8_t ucStatus = eSectionInit;
							int liRes = sectionBoardGeneralSingleton(i)->getInternalStatus(ucStatus);
							if ( liRes == -1)
                            {
                                // error in communication -> abort
                                m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
                            }
							// continue only if the section status is the expected one
                            else if ( ucStatus == eSectionPause || ucStatus == eSectionIdle )
							{
								checkStripReadSec(i);
							}
							else if ( getSecondsTime()-m_sectionTimeout[i] > READSEC_ACTION_TIMEOUT )
							{
								generateSectionSynchroFailure(i);
								m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
							}
						}
					break;

					case REQUEST_WAIT_NSH_READ_PRESENCE:
						m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_WAIT_NSH_READ_PRESENCE");
						if ( ! nshBoardManagerSingleton()->isActionInProgress() )
						{
							getSubstrateRead(i);
							// get true event number after substrate read and reschedule readsec with the correct number of events
							bGetTrueEventNumber = true;
						}
						else if ( getSecondsTime()-m_sectionTimeout[i] > READSEC_ACTION_TIMEOUT )
						{
							// timeout on action expired -> force protocol abort
							m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
							masterBoardManagerSingleton()->setErrorFromCode(ERR_PROTOCOL_TIMEOUT, true);
						}
					break;

					case REQUEST_MOVE_BEFORE_DECODING_BC:
						m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_MOVE_BEFORE_DECODING_BC");
						moveBeforeDecodingReadSec(i, int(eTarget::eStrip), REQUEST_WAIT_NSH_MOVE_AND_DECODE_BC);
					break;

					case REQUEST_MOVE_BEFORE_DECODING_DM:
						m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_MOVE_BEFORE_DECODING_DM");
						moveBeforeDecodingReadSec(i, int(eTarget::eSpr), REQUEST_WAIT_NSH_MOVE_AND_DECODE_DM);
					break;

					case REQUEST_MOVE_BEFORE_CHECKING_W0:
						m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_MOVE_BEFORE_CHECKING_W0");
						moveBeforeDecodingReadSec(i, int(eTarget::eSample0), REQUEST_WAIT_NSH_MOVE_AND_CHECK_W0);
					break;

					case REQUEST_MOVE_BEFORE_CHECKING_W3:
						m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_MOVE_BEFORE_CHECKING_W3");
						moveBeforeDecodingReadSec(i, int(eTarget::eSample3), REQUEST_WAIT_NSH_MOVE_AND_CHECK_W3);
					break;

                    case REQUEST_MOVE_BEFORE_CHECKING_SPR:
                        m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_MOVE_BEFORE_CHECKING_SPR");
                        moveBeforeDecodingReadSec(i, int(eTarget::eConeAbsence), REQUEST_WAIT_NSH_MOVE_AND_CHECK_SPR);
                    break;

					case REQUEST_WAIT_NSH_MOVE_AND_DECODE_BC:
					case REQUEST_WAIT_NSH_MOVE_AND_DECODE_DM:
					case REQUEST_WAIT_NSH_MOVE_AND_CHECK_W0:
					case REQUEST_WAIT_NSH_MOVE_AND_CHECK_W3:
					case REQUEST_WAIT_NSH_MOVE_AND_CHECK_SPR:
						if ( ! nshBoardManagerSingleton()->isActionInProgress() )
						{
							m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_WAIT_NSH_MOVE_AND_PERFORM_ACTION");

							switch ( m_sectionRequest[i] )
							{
								case REQUEST_WAIT_NSH_MOVE_AND_DECODE_BC:	decodeReadSec(i, int(eTarget::eStrip));			break;
								case REQUEST_WAIT_NSH_MOVE_AND_DECODE_DM:	decodeReadSec(i, int(eTarget::eSpr));			break;
								case REQUEST_WAIT_NSH_MOVE_AND_CHECK_W0:	checkSampleReadSec(i, int(eTarget::eSample0));	break;
								case REQUEST_WAIT_NSH_MOVE_AND_CHECK_W3:	checkSampleReadSec(i, int(eTarget::eSample3));	break;
								case REQUEST_WAIT_NSH_MOVE_AND_CHECK_SPR:   checkSprReadSec(i);								break;
							}
						}
						else if ( getSecondsTime()-m_sectionTimeout[i] > READSEC_ACTION_TIMEOUT )
						{
							// timeout on action expired -> force protocol abort
							m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
							masterBoardManagerSingleton()->setErrorFromCode(ERR_PROTOCOL_TIMEOUT, true);
						}
					break;

					case REQUEST_WAIT_WHILE_DECODING_BC:
					case REQUEST_WAIT_WHILE_DECODING_DM:
					case REQUEST_WAIT_WHILE_CHECKING_W0:
					case REQUEST_WAIT_WHILE_CHECKING_W3:
                    case REQUEST_WAIT_WHILE_CHECKING_SPR:
						if ( ! cameraBoardManagerSingleton()->isActionInProgress() )
						{
							m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_WAIT_WHILE_ACTION");

							switch(m_sectionRequest[i])
							{
								case REQUEST_WAIT_WHILE_DECODING_BC:	updateResultReadSec(i, int(eTarget::eStrip));		break;
								case REQUEST_WAIT_WHILE_DECODING_DM:	updateResultReadSec(i, int(eTarget::eSpr));			break;
								case REQUEST_WAIT_WHILE_CHECKING_W0:	updateResultReadSec(i, int(eTarget::eSample0));		break;
								case REQUEST_WAIT_WHILE_CHECKING_W3:	updateResultReadSec(i, int(eTarget::eSample3));		break;
                                case REQUEST_WAIT_WHILE_CHECKING_SPR:   updateResultReadSec(i, int(eTarget::eConeAbsence));	break;
							}
						}
						else if ( getSecondsTime()-m_sectionTimeout[i] > READSEC_ACTION_TIMEOUT )
						{
							m_pLogger->log(LOG_ERR, "THRunProtocol::workerThread: REQUEST_WAIT_WHILE_ACTION [%d] > REQUEST_ACTION_TIMEOUT",
										   getSecondsTime()-m_sectionTimeout[i]);
							// timeout on action expired -> force protocol abort
							m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
							masterBoardManagerSingleton()->setErrorFromCode(ERR_PROTOCOL_TIMEOUT, true);
						}
					break;

					case REQUEST_END_READSEC:
						if ( ! nshBoardManagerSingleton()->isActionInProgress() )
						{
							m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_END_READSEC");
							m_sectionRequest[i] = REQUEST_NONE;
						}
						else if ( getSecondsTime()-m_sectionTimeout[i] > READSEC_ACTION_TIMEOUT )
						{
							// timeout on action expired -> force protocol abort
							m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
							masterBoardManagerSingleton()->setErrorFromCode(ERR_PROTOCOL_TIMEOUT, true);
						}
					break;

					case REQUEST_RESCHEDULE_EVENT:
					{
						m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_RESCHEDULE_EVENT!");

						/*! **************************************************************************************************
						 * The reschedule action is needed in the following circumstances:
						 * - after the read of the substrate, to schedule only the slots where the strip is actually present
						 * - after the dtmx read, in case of failure, to perform the cone absence check
						 * - at the end, to schedule an answer
						 *****************************************************************************************************/
						bool bScheduled = false;

						// get true event number after substrate read and reschedule readsec with the correct number of events
						if ( bGetTrueEventNumber )
						{
							int liDMNum = 0, liBCNum = 0, liSample0Num = 0, liSample3Num = 0;
							getTrueEventNumber(&liBCNum, &liDMNum, &liSample0Num, &liSample3Num, i);
							bScheduled = rescheduleReadSec(liBCNum, liDMNum, liSample0Num, liSample3Num, 0, i);
							bGetTrueEventNumber = false;
						}
						// get dtmx failed and reschedule to perform cone absence check.
						else
						{
							vector<unsigned int> vFailuresForUnknownCause;
							readSecInfoPointer(i)->getDtmxFailedForUnknownCause(vFailuresForUnknownCause);
							bScheduled = rescheduleReadSec(0, 0, 0, 0, vFailuresForUnknownCause.size(), i);
						}

						if ( ! bScheduled )		m_sectionRequest[i] = REQUEST_ABORT_PROTOCOL;
						else					m_sectionRequest[i] = REQUEST_NONE;
					}
					break;

					case REQUEST_SET_DEFAULT_READSEC:
					{
						m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_SET_DEFAULT_READSEC");

						string strSolidStandard("0");
						pair<eTarget, string> pMotorNsh({eTarget::eNone, strSolidStandard});
						vector<pair<eSectionMotor, string>> vMotorsSection;
						vMotorsSection.push_back({eSectionMotor::eSpr, "Load"});
						vMotorsSection.push_back({eSectionMotor::eTray, "Out"});
						moveMotors(vMotorsSection, pMotorNsh, i, REQUEST_NONE);
					}
					break;

					case REQUEST_SEND_REPLY_READSEC:
					{
						m_pLogger->log(LOG_INFO, "THRunProtocol::workerThread: REQUEST_SEND_REPLY_READSEC");
						sendReplyReadSec(i);
					}
					break;

                    case REQUEST_ERROR_UNLOAD :
                    case REQUEST_UNLOAD:
                        // protocol is over, wait for the user to open the door
                        if(sectionBoardGeneralSingleton(i)->isDoorClosed())
                        {
                            break;
                        }

                        m_sectionStatus[i] = PROTO_NOT_RUNNING;
                        if(m_sectionRequest[i] == REQUEST_ERROR_UNLOAD)
                        {
                            updateStatus(i, eStatusError);
                            // set the Section Led Red Blink
                            sectionBoardGeneralSingleton(i)->setLed(eLedRed, eLedBlinkSlow);
                        }
                        else
                        {
                            updateStatus(i, eStatusIdle);
                            // set the Section Led Green Steady
                            sectionBoardGeneralSingleton(i)->setLed(eLedGreen, eLedOn);
                        }
                    break;

					case REQUEST_NONE :
						// check pending actions
						checkPendingAction(i);
					break;

					default :
					break;
				}

				// check if the temperature has to be stored
				saveTemperatures(i);

				// the section has notified an abort
				if(sectionBoardGeneralSingleton(i)->getProtocolAbortedFlag())
				{
					sectionBoardGeneralSingleton(i)->clearProtocolAbortedFlag();
					// force the request to ABORT
                    if(m_sectionStatus[i] == PROTO_RUNNING)
					{
                        abortProtocol(i, false);
                    }
				}

                // if the NSH is in error -> abort both sections !
                if(stateMachineSingleton()->getDeviceStatus(eIdNsh) == eStatusError)
                {
                    if(m_sectionStatus[i] == PROTO_RUNNING)
                    {
                        abortProtocol(i, true);
                    }
                }

                usleep(10 * 1000);
			}
		}

		m_protocolActive = false;
		log(LOG_INFO, "THRunProtocol::th: done %d", m_protocolActive);
	}
	return 0;
}

void THRunProtocol::afterWorkerThread(void)
{
	m_synchSem.closeAndUnlink();
	log(LOG_INFO, "THRunProtocol::stopped");
}

void THRunProtocol::generateSectionSynchroFailure(uint8_t section)
{
    // error in synchro protocol with section -> generates correponding error on section
    sectionBoardGeneralSingleton(section)->setEventToDecode('M', '1');
}

bool THRunProtocol::repeatRunwlReply(std::string strWebUrl)
{
	// checks for both sections if the last RUN reply was correctly sent to sw
	// and, if necessary, resends it

	uint8_t section;
	std::string strOutReply;

	for(section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		if(m_sectionReplyOk[section] == false)
		{
			// read the reply from file
			strOutReply = readRunReply(section);
			if(strOutReply.empty() == false)
			{
				// create a "fake" operation on the fly ...
				OpRunwl* pOp = new OpRunwl;
				pOp->setWebResponseUrl(strWebUrl);
				pOp->setLogger(MainExecutor::getInstance()->getLogger());
				// .. simply to call this:
				m_sectionReplyOk[section] = pOp->sendCommandReply(strOutReply);
				// and update the reply status locally and on file
				saveRunReplyOk(section, m_sectionReplyOk[section]);
				SAFE_DELETE(pOp);
			}
		}
	}

	return (m_sectionReplyOk[SCT_A_ID] && m_sectionReplyOk[SCT_B_ID]);
}

bool THRunProtocol::composeDefaultRunwlReply(uint8_t section)
{
	OutCmdRunwl outCmdRunwl;

	outCmdRunwl.setUsage(m_sectionOperation[section]->getUsage());
	outCmdRunwl.setID(m_sectionOperation[section]->getID());

	// build the payload of the asynchrounous reply
	RunwlPayload * pRunwlPayload = new RunwlPayload();
	// pass to the payload the pointer to the runwl corresponding to section
	pRunwlPayload->setSectionRunwl(section, runwlInfoPointer(section));

	outCmdRunwl.setStatus(XML_ATTR_STATUS_FAILED);

	std::string strStatus;
	strStatus.assign(PROTOCOL_STATUS_PARTIALLY_DONE);
	pRunwlPayload->setProtocolStatus(strStatus);

	// and assign it to the outcommand so that the serialize will extract the info
	outCmdRunwl.setPayload((Payload *)pRunwlPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	// the protocol has aborted internally -> setFailureCause
	FailureCause* pFailureCause = new FailureCause(ERR_NONE);

	// set a "fake" failure cause for protocol not completed !
	strStatus.assign(masterBoardManagerSingleton()->setErrorFromCode(ERR_PROTOCOL_SHUTDOWN, false));
	pFailureCause->setCodeString(strStatus);

	outCmdRunwl.setFailureCause(pFailureCause);

	std::string strOutCommand;
	bool bSerializeResult = protocolSerialize(&outCmdRunwl, strOutCommand);
	if ( bSerializeResult == false )
	{
		log(LOG_ERR, "THRunProtocol: unable to serialize outCmdRunwl");
	}
	else
	{
		// save the reply on file
		saveRunReply(section, strOutCommand);
		// and store it as not sent
		saveRunReplyOk(section, false);
	}

	return bSerializeResult;
}

void THRunProtocol::setSectionReply(uint8_t section, bool status)
{
    if(section < SCT_NUM_TOT_SECTIONS)
    {
        m_sectionReplyOk[section] = status;
    }
}

void THRunProtocol::deleteOperation(uint8_t section)
{
    /* ********************************************************************************************
     * Remove the operation from the OperationManager list
     * (if the other section has a different one or it's completed)
     * ********************************************************************************************
     */
    uint8_t secondSection;
    if(section == SCT_A_ID)
    {
        secondSection = SCT_B_ID;
    }
    else
    {
        secondSection = SCT_A_ID;
    }

    bool bRemoveOp = false;
    if(m_sectionOperation[section] != m_sectionOperation[secondSection])
    {
        bRemoveOp = true;
    }
    else
    {
        if(m_sectionStatus[secondSection] == PROTO_NOT_RUNNING)
        {
            bRemoveOp = true;
        }
    }

    if(bRemoveOp == true)
    {
        operationSingleton()->removeOperation(m_sectionOperation[section]);
        m_sectionOperation[section] = 0;
    }

}

void THRunProtocol::deleteOperationReadSec(uint8_t section)
{
	/* ********************************************************************************************
	 * Remove the operation from the OperationManager list
	 * (if the other section has a different one or it's completed)
	 * ********************************************************************************************
	 */
	uint8_t secondSection;
	if(section == SCT_A_ID)
	{
		secondSection = SCT_B_ID;
	}
	else
	{
		secondSection = SCT_A_ID;
	}

	bool bRemoveOp = false;
	if ( m_readSecOperation[section] != m_readSecOperation[secondSection] )
	{
		bRemoveOp = true;
	}
	else
	{
		if ( m_sectionStatus[secondSection] == PROTO_NOT_RUNNING )
		{
			bRemoveOp = true;
		}
	}

	if ( bRemoveOp )
	{
		operationSingleton()->removeOperation(m_readSecOperation[section]);
		m_readSecOperation[section] = 0;
	}

}

void THRunProtocol::updateStatus(uint8_t section, eDeviceStatus status)
{
	if(section == SCT_A_ID)
		changeDeviceStatus(eIdSectionA, status);
    else if(section == SCT_B_ID)
		changeDeviceStatus(eIdSectionB, status);
    else if(section == NSH_ID)
        changeDeviceStatus(eIdNsh, status);
	else if(section == CAMERA_ID)
		changeDeviceStatus(eIdCamera, status);
}

void THRunProtocol::endProtocolNsh(uint8_t section)
{
    if(stateMachineSingleton()->getDeviceStatus(eIdNsh) == eStatusProtocol)
    {
        eDeviceId otherSection;

        if(section == SCT_A_ID)
        {
            otherSection = eIdSectionB;
        }
        else
        {
            otherSection = eIdSectionA;
        }
		if(stateMachineSingleton()->getDeviceStatus(otherSection) != eStatusProtocol ||
		   stateMachineSingleton()->getDeviceStatus(otherSection) != eStatusReadsec)
        {
            updateStatus(NSH_ID, eStatusIdle);
        }
    }
}

void THRunProtocol::saveTemperatures(uint8_t section)
{
	if ( m_sectionTemperatureFile[section].is_open() )
	{
		time_t tmpSeconds = getSecondsTime();

		if((tmpSeconds - m_sectionSeconds[section]) > TEMPERATURE_SAMPLING_INTERVAL)
		{
            uint16_t valueRoom = masterBoardManagerSingleton()->getInternalTemperature();
            uint16_t value1 = sectionBoardGeneralSingleton(section)->getIntTemperatureSPR();
            uint16_t value2 = sectionBoardGeneralSingleton(section)->getIntTemperatureTray();

            m_sectionTemperatureFile[section].write((char*)&valueRoom, sizeof(uint16_t));
            m_sectionTemperatureFile[section].write((char*)&value1, sizeof(uint16_t));
            m_sectionTemperatureFile[section].write((char*)&value2, sizeof(uint16_t));

            log(LOG_INFO, "THRunProtocol::saveTemperatures %c (%d %d %d)", (SECTION_CHAR_A + section), valueRoom, value1, value2);

			m_sectionSeconds[section] = tmpSeconds;
		}
	}
}

void THRunProtocol::initializeTemperatureData(uint8_t section)
{
	// open file for temperature data collection
	if ( m_sectionTemperatureFile[section].is_open() )
	{
		m_sectionTemperatureFile[section].close();
	}

	char nameFile[64];
	sprintf(nameFile, "%s%s%c%s", RUN_FOLDER, PROTO_TEMPERATURE_PREFIX, SECTION_CHAR_A + section, PROTO_TEMPERATURE_SUFFIX);

	m_sectionTemperatureFile[section].open(nameFile, ios_base::out | ios_base::binary);

	// time for registration
	m_sectionSeconds[section] = 0;
}

void THRunProtocol::stopDataAcquisition(uint8_t section)
{
	// stop pressure data collection ...
	THMonitorPressure::getInstance()->stopProtocolMonitoring(section);
	// ... and temperature data
	if ( m_sectionTemperatureFile[section].is_open() )
	{
		m_sectionTemperatureFile[section].close();
	}
}

std::string THRunProtocol::readRunReply(uint8_t section)
{
	std::string strOutReply;

	// reads from file the String reply to be used in case of resending
	char nameFile[64];

	sprintf(nameFile, "%s%s%c%s", RUN_FOLDER, RUN_REPLY_NAME, SECTION_CHAR_A + section, RUN_REPLY_EXTENSION);

	std::ifstream inFile(nameFile, ios_base::in);

	std::stringstream strStream;
	strStream << inFile.rdbuf(); //read the file
	strOutReply = strStream.str(); //str holds the content of the file

	inFile.close();

	return strOutReply;
}

bool THRunProtocol::readRunReplyOk(uint8_t section)
{
	// reads the last String reply status stored on file
	char nameFile[64];
	std::string strOutReply;

	sprintf(nameFile, "%s%s%c%s", RUN_FOLDER, RUN_REPLY_STATUS, SECTION_CHAR_A + section, RUN_REPLY_EXTENSION);
	std::ifstream inFile(nameFile);

	if(inFile.fail())
	{
		// file not exist -> ok
		return true;
	}

	strOutReply.clear();
	while(!inFile.eof())
	{
		getline(inFile, strOutReply);
		if(strOutReply.empty() == false)
			break;
	}
	inFile.close();

	if(strOutReply.empty() == true) return true;

	return (strOutReply.compare("OK") == 0);
}

void THRunProtocol::buildDebugFile(uint8_t section)
{
	// Temperature file
	initializeTemperatureData(section);

	if ( m_sectionTemperatureFile[section].is_open() )
	{
		for(uint8_t counter = 0; counter < 100; counter++)
		{
			uint32_t valueRoom = (3700 + counter);
			uint32_t value1 = (3600 + counter);
			uint32_t value2 = (3800 - counter);

			m_sectionTemperatureFile[section].write((char*)&valueRoom, sizeof(uint32_t));
			m_sectionTemperatureFile[section].write((char*)&value1, sizeof(uint32_t));
			m_sectionTemperatureFile[section].write((char*)&value2, sizeof(uint32_t));
		}
	}


	for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
	{
		char nameFile[64];
		sprintf(nameFile, "%s%s%c%c%s", RUN_FOLDER, PROTO_TREATED_PREFIX, SECTION_CHAR_A + section,
				ZERO_CHAR + channel + 1, PROTO_TREATED_SUFFIX);
		ofstream pressureFile;
		pressureFile.open(nameFile, ios_base::out | ios_base::binary);

		for(uint8_t counter = 0; counter < 10; counter++)
		{
			uint32_t value = RAW_DATA_HEADER_ADC_VALUE;
			value += uint32_t('A' + counter);
			pressureFile.write((char*)&value, sizeof(uint32_t));

			value = 0;
			value += uint32_t((10 + counter) << 24) + uint32_t(counter << 16) +
						uint32_t(('a' + counter) << 8) + uint32_t('1' + counter);
			pressureFile.write((char*)&value, sizeof(uint32_t));

			value = 123 + (10 * counter);
			pressureFile.write((char*)&value, sizeof(uint32_t));

			for(uint8_t k = 0; k < 100; k++)
			{
				value = 8000000 + (1000 * channel) + (k * 2);
				pressureFile.write((char*)&value, sizeof(uint32_t));
			}

			value = 0;
			pressureFile.write((char*)&value, sizeof(uint32_t));

		}
		pressureFile.close();
	}

	well_data_struct well_data;
	well_sampling_struct well_sampling;

	for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
	{
		char nameFile[64];
		sprintf(nameFile, "%s%s%c%c%s", RUN_FOLDER, PROTO_ELABORATED_PREFIX, SECTION_CHAR_A + section,
				ZERO_CHAR + channel + 1, PROTO_ELABORATED_SUFFIX);
		ofstream pressureFile;
		pressureFile.open(nameFile, ios_base::out | ios_base::binary);

		char bufferTmp[12];

		for(uint8_t counter = 0; counter < 10; counter++)
		{
			// header
			pressureFile.write((char*)&elab_header_chars, 8);

			well_data.motion_num = counter;
			convertNumberToChars((int64_t)well_data.motion_num, bufferTmp, sizeof(uint8_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint8_t));

			well_data.max_cycle = counter * 10;
			convertNumberToChars((int64_t)well_data.max_cycle, bufferTmp, sizeof(uint8_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint8_t));

			well_data.pos = counter + 1;
			convertNumberToChars((int64_t)well_data.pos, bufferTmp, sizeof(uint8_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint8_t));

			well_data.motion_type = 'A' + counter;
			convertNumberToChars((int64_t)well_data.motion_type, bufferTmp, sizeof(uint8_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint8_t));

			well_data.speed_char = 'a' + counter;
			convertNumberToChars((int64_t)well_data.speed_char, bufferTmp, sizeof(uint8_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint8_t));

			well_data.volume_char = '1' + counter;
			convertNumberToChars((int64_t)well_data.volume_char, bufferTmp, sizeof(uint8_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint8_t));

//			pressureFile.write((char*)&well_data, sizeof(well_data_struct));

			well_sampling.area = (channel * channel) * counter;
			convertNumberToChars((int64_t)well_sampling.area, bufferTmp, sizeof(int64_t));
			pressureFile.write((char*)&bufferTmp, sizeof(int64_t));

			well_sampling.integral = (channel * channel) * (counter * counter);
			convertNumberToChars((int64_t)well_sampling.integral, bufferTmp, sizeof(int64_t));
			pressureFile.write((char*)&bufferTmp, sizeof(int64_t));

//			well_sampling.area_dx = 256070;
//			convertNumberToChars((int64_t)well_sampling.area_dx, bufferTmp, sizeof(int64_t));
//			pressureFile.write((char*)&bufferTmp, sizeof(int64_t));

			well_sampling.area_sx = 23578;
			convertNumberToChars((int64_t)well_sampling.area_sx, bufferTmp, sizeof(int64_t));
			pressureFile.write((char*)&bufferTmp, sizeof(int64_t));

			well_sampling.start_time = 1000 + (channel * counter);
			convertNumberToChars((int64_t)well_sampling.start_time, bufferTmp, sizeof(uint32_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint32_t));

			well_sampling.first_sample_val = 8000000;
			convertNumberToChars((int64_t)well_sampling.first_sample_val, bufferTmp, sizeof(uint32_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint32_t));

			well_sampling.last_sample_val = 8500000;
			convertNumberToChars((int64_t)well_sampling.last_sample_val, bufferTmp, sizeof(uint32_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint32_t));

			well_sampling.max_sample_val = 8800000;
			convertNumberToChars((int64_t)well_sampling.max_sample_val, bufferTmp, sizeof(uint32_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint32_t));

			well_sampling.min_sample_val = 7000000;
			convertNumberToChars((int64_t)well_sampling.min_sample_val, bufferTmp, sizeof(uint32_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint32_t));

			well_sampling.max_min_difference = 250000;
			convertNumberToChars((int64_t)well_sampling.max_min_difference, bufferTmp, sizeof(uint32_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint32_t));

//			well_sampling.max_cycles_val = 8900000;
//			convertNumberToChars((int64_t)well_sampling.max_cycles_val, bufferTmp, sizeof(uint32_t));
//			pressureFile.write((char*)&bufferTmp, sizeof(uint32_t));

			well_sampling.min_cycles_val = 7000000;
			convertNumberToChars((int64_t)well_sampling.min_cycles_val, bufferTmp, sizeof(uint32_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint32_t));

			well_sampling.low_threshold = 123456 - channel;
			convertNumberToChars((int64_t)well_sampling.low_threshold, bufferTmp, sizeof(uint32_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint32_t));

			well_sampling.high_threshold = 123456 + channel;
			convertNumberToChars((int64_t)well_sampling.high_threshold, bufferTmp, sizeof(uint32_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint32_t));

            well_sampling.max_sample_pos = 100 + counter;
            convertNumberToChars((int64_t)well_sampling.max_sample_pos, bufferTmp, sizeof(uint16_t));
            pressureFile.write((char*)&bufferTmp, sizeof(uint16_t));

            well_sampling.min_sample_pos = 1000 - counter;
            convertNumberToChars((int64_t)well_sampling.min_sample_pos, bufferTmp, sizeof(uint16_t));
            pressureFile.write((char*)&bufferTmp, sizeof(uint16_t));

            well_sampling.tot_sample = counter * counter;
			convertNumberToChars((int64_t)well_sampling.tot_sample, bufferTmp, sizeof(uint16_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint16_t));

            well_sampling.air_area_counter = section * 10;
            convertNumberToChars((int64_t)well_sampling.air_area_counter, bufferTmp, sizeof(uint8_t));
            pressureFile.write((char*)&bufferTmp, sizeof(uint8_t));

			well_sampling.curve_slope = -100;
            convertNumberToChars((int64_t)well_sampling.curve_slope, bufferTmp, sizeof(int8_t));
            pressureFile.write((char*)&bufferTmp, sizeof(int8_t));

			well_sampling.result_flag = counter;
			convertNumberToChars((int64_t)well_sampling.result_flag, bufferTmp, sizeof(uint8_t));
			pressureFile.write((char*)&bufferTmp, sizeof(uint8_t));

            well_sampling.algorithm = ZERO_CHAR;
            convertNumberToChars((int64_t)well_sampling.algorithm, bufferTmp, sizeof(uint8_t));
            pressureFile.write((char*)&bufferTmp, sizeof(uint8_t));

//			pressureFile.write((char*)&well_sampling, sizeof(well_sampling_struct));

			uint32_t numOfChars = sizeof(well_data_struct) + sizeof(well_sampling_struct);

			// the Section sends data padded at 7 bytes so ....
			if((numOfChars % 7) > 0)
			{
				uint8_t zeroChar = 0;
				uint8_t count = (7 - (numOfChars % 7));
				for(uint8_t k = 0; k < count; k++)
					pressureFile.write((char*)&zeroChar, sizeof(char));
			}

			// footer
			pressureFile.write((char*)&elab_footer_chars, 8);
		}

		pressureFile.close();
	}
}

bool THRunProtocol::rescheduleReadSec(int liBCNum,int liDMNum, int liSample0Num,
									  int liSample3Num, int liConeAbsenceNum, uint8_t section)
{
	if ( section >= SCT_NUM_TOT_SECTIONS )	return false;

	// Init scheduler
	t_ErrorData ErrorData;
	m_pLogger->log(LOG_DEBUG, "THRunProtocol::rescheduleReadSec: init rescheduling");
	ErrorData.iErrorCode = SCHED_SUCCESS;

	schedulerThreadPointer()->problemInit(ErrorData);
	if ( ErrorData.iErrorCode != SCHED_SUCCESS )
	{
		schedulerThreadPointer()->closeAndAbort();
		m_pLogger->log(LOG_ERR, "THRunProtocol::rescheduleReadSec: Scheduler initialization error :%d", ErrorData.iErrorCode);
		return false;
	}
	m_pLogger->log(LOG_DEBUG, "THRunProtocol::rescheduleReadSec: init scheduling -> Done");

	// Schedule events
	struct precedence SLastPrec;
	int iFailureCode = SCHED_ERROR;
	m_pLogger->log(LOG_INFO, "THRunProtocol::rescheduleReadSec: start scheduling section %c", SECTION_CHAR_A + section);
	ErrorData.iErrorCode = SCHED_SUCCESS;
	SLastPrec = schedulerThreadPointer()->getLastPrec();

	iFailureCode = schedulerThreadPointer()->addReadSecEvent(section, SLastPrec.section_id, SLastPrec.slot_id, ErrorData,
															 true, false, liDMNum, liBCNum, 0, liSample0Num, liSample3Num, liConeAbsenceNum);

	if ( iFailureCode == SCHED_SUCCESS )
	{
		m_pLogger->log(LOG_INFO, "THRunProtocol::rescheduleReadSec: scheduling section %c completed", SECTION_CHAR_A + section);
	}
	else
	{
		m_pLogger->log(LOG_ERR, "THRunProtocol::rescheduleReadSec: scheduling process error: %d", ErrorData.iErrorCode);
	}

	// Execute Read Sec Scheduling
	m_pLogger->log(LOG_INFO, "THRunProtocol::rescheduleReadSec: closeAndRun");
	if ( schedulerThreadPointer()->closeAndRun(ErrorData) == -1 )
	{
		m_pLogger->log(LOG_ERR, "THRunProtocol::rescheduleReadSec: schedule execute failed");
		return false;
	}

	m_pLogger->log(LOG_INFO, "WFReadSec::scheduleExecute: end section %c", SECTION_CHAR_A + section);
	m_sectionRequest[section] = REQUEST_NONE;

    return ( iFailureCode == SCHED_SUCCESS );
}

void convertNumberToChars(int64_t value, char * buffer, uint8_t numberSize)
{
	uint8_t counter;

	for(counter = numberSize; counter > 0; counter--, buffer++)
	{
		uint8_t addendum = (uint8_t)(value >> (8 * (counter - 1)));

		*buffer = addendum;
	}
}

// ---------------- Scheduler Callback functions -----------------------

int THRunProtocol_startProtocol(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_START_PROTOCOL);
}

int THRunProtocol_endProtocol(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_END_PROTOCOL);
}

int THRunProtocol_continueProtocol(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_CONTINUE_PROTOCOL);
}

int THRunProtocol_readProtocol(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_READ_PROTOCOL);
}

int THRunProtocol_readAir(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_READ_AIR);
}

int THRunProtocol_abortProtocol(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_ABORT_PROTOCOL);
}

int THRunProtocol_moveNshProtocol(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_MOVE_NSH_PROTOCOL);
}

int THRunProtocol_startReadSec(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_START_READSEC);
}

int THRunProtocol_getReadyReadSec(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_GETREADY_READSEC);
}

int THRunProtocol_checkStripPresenceReadSec(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_CHECK_STRIP_PRESENCE);
}

int THRunProtocol_decodeBCReadSec(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_MOVE_BEFORE_DECODING_BC);
}

int THRunProtocol_decodeDMReadSec(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_MOVE_BEFORE_DECODING_DM);
}

int THRunProtocol_checkSample0ReadSec(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_MOVE_BEFORE_CHECKING_W0);
}

int THRunProtocol_checkSample3ReadSec(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_MOVE_BEFORE_CHECKING_W3);
}

int ThRunProtocol_checkSprPresenceReadSec(int section, int slot, int event)
{
    // just to avoid warnings ....
    (void)slot;
    (void)event;

    return protocolThreadPointer()->requestAction(section, REQUEST_MOVE_BEFORE_CHECKING_SPR);
}

int THRunProtocol_rescheduleEvent(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_RESCHEDULE_EVENT);
}

int THRunProtocol_setDefaultReadSec(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_SET_DEFAULT_READSEC);
}

int THRunProtocol_replyReadSec(int section, int slot, int event)
{
	// just to avoid warnings ....
	(void)slot;
	(void)event;

	return protocolThreadPointer()->requestAction(section, REQUEST_SEND_REPLY_READSEC);
}

// ------ Thread to send the Protocol reply

THReplyProtocol::THReplyProtocol()
{
    m_synchSem.create(SEM_REPLY_SYNCH_NAME, SEM_REPLY_SYNCH_INIT_VALUE);
    m_synchSem.enableErrorPrint(true);

    m_replyActive = false;

    for(uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
    {
		m_requestReadSec[section] = false;
        m_requestSection[section] = false;
        m_sectionOperation[section] = 0;
        m_bSuccess[section] = false;
        m_protocolAbortedCommand[section] = false;
    }

}

THReplyProtocol::~THReplyProtocol()
{

}

bool THReplyProtocol::requestReplySection(uint8_t section, OpRunwl * pOpRunwl,
                                          bool bSuccess, bool bProtocolAborted)
{
    if(section >= SCT_NUM_TOT_SECTIONS || pOpRunwl == 0)
    {
        return false;
    }

    // if the reply is already ongoing -> reject the request
    if(m_requestSection[section] == true)
    {
        return false;
    }

    m_requestSection[section] = true;
    m_sectionOperation[section] = pOpRunwl;
    m_bSuccess[section] = bSuccess;
    m_protocolAbortedCommand[section] = bProtocolAborted;

    // if necessary unlock the thread loop
    if(m_replyActive == false)
    {
        int liRetVal =  m_synchSem.release();
        if ( liRetVal == SEM_FAILURE )
        {
            return false;
        }
    }

	return true;
}

bool THReplyProtocol::requestReplyReadSec(uint8_t section, OpReadSec* pOpReadSec, bool bSuccess)
{
	if ( section >= SCT_NUM_TOT_SECTIONS || pOpReadSec == nullptr )
	{
		return false;
	}

	// if the reply is already ongoing -> reject the request
	if ( m_requestReadSec[section] )
	{
		return false;
	}

	m_requestReadSec[section] = true;
	m_readSecOperation[section] = pOpReadSec;
	m_bSuccess[section] = bSuccess;

	// if necessary unlock the thread loop
	// if not already running unlock the semaphore
	if ( m_replyActive == false )
	{
		int liRetVal =  m_synchSem.release();
		if ( liRetVal == SEM_FAILURE )
		{
			return false;
		}
	}

	return true;
}

int THReplyProtocol::workerThread()
{
    while ( isRunning() )
    {
        int liSynchRet = m_synchSem.wait();
        if( liSynchRet != SEM_SUCCESS )
        {
            log(LOG_WARNING, "THReplyProtocol::th: unlocked but wait returned %d", liSynchRet);
        }

        m_replyActive = true;
        log(LOG_INFO, "THReplyProtocol::th: unlocked %d", m_replyActive);

        while(true)
        {
            if(m_requestSection[SCT_A_ID] == true)
            {
                sendRunwlReply(SCT_A_ID);
                m_requestSection[SCT_A_ID] = false;
            }

            if(m_requestSection[SCT_B_ID] == true)
            {
                sendRunwlReply(SCT_B_ID);
                m_requestSection[SCT_B_ID] = false;
			}

			if ( m_requestReadSec[SCT_A_ID] == true )
			{
				sendReadSecReply(SCT_A_ID);
				m_requestReadSec[SCT_A_ID] = false;
			}

			if ( m_requestReadSec[SCT_B_ID] == true )
			{
				sendReadSecReply(SCT_B_ID);
				m_requestReadSec[SCT_B_ID] = false;
			}

			if ( m_requestSection[SCT_A_ID] == false && m_requestSection[SCT_B_ID] == false &&
				 m_requestReadSec[SCT_A_ID] == false && m_requestReadSec[SCT_B_ID] == false )
			{
				// no more request pending
				break;
			}
        }

        m_replyActive = false;
        log(LOG_INFO, "THReplyProtocol::th: done %d", m_replyActive);
    }
    return 0;
}

void THReplyProtocol::beforeWorkerThread()
{
    log(LOG_DEBUG, "THReplyProtocol::started");
}

void THReplyProtocol::afterWorkerThread()
{
    m_synchSem.closeAndUnlink();
    log(LOG_INFO, "THReplyProtocol::stopped");
}

bool THReplyProtocol::sendRunwlReply(uint8_t section)
{
    log(LOG_INFO, "THReplyProtocol::sendReply");

    if(m_sectionOperation[section] == 0) return false;

    m_sectionOperation[section]->restoreStateMachine();

    OutCmdRunwl outCmdRunwl;

    outCmdRunwl.setUsage(m_sectionOperation[section]->getUsage());
    outCmdRunwl.setID(m_sectionOperation[section]->getID());

    // build the payload of the asynchrounous reply
    RunwlPayload * pRunwlPayload = new RunwlPayload();
    // pass to the payload the pointer to the runwl corresponding to section
    pRunwlPayload->setSectionRunwl(section, runwlInfoPointer(section));

    if(m_bSuccess[section])
    {
        std::string strStatus;
        strStatus.assign(PROTOCOL_STATUS_COMPLETED);

        outCmdRunwl.setStatus(XML_ATTR_STATUS_SUCCESSFULL);

        pRunwlPayload->setProtocolStatus(strStatus);

        if(runwlInfoPointer(section)->getMonitoringTemperature() == true)
        {
            std::string strTemperature;
            strTemperature.clear();

            // prepare the Temperature data to be sent back
            protocolDataSerialize(section, 0, WEB_CMD_DATA_TEMPERATURE, strTemperature);
            // pass also the encoded data to be added as strings
            pRunwlPayload->setTemperature(strTemperature);
        }

        if(runwlInfoPointer(section)->getMonitoringAutocheck() == true)
        {
            std::string strOptics;
            strOptics.clear();

            // prepare the Optics data data to be sent back
            protocolDataSerialize(section, 0, WEB_CMD_DATA_OPTICS, strOptics);
            // pass also the encoded data to be added as strings
            pRunwlPayload->setOptics(strOptics);
        }


        // set Treated Pressure data per each strip
        uint8_t strip;
        std::string strTreatedPressure, strElaboratedPressure;

        for(strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
        {
            std::string strAlgorithm;
            strAlgorithm.assign(pressureSettingsPointer(section, strip)->getAlgorithm());
            pRunwlPayload->setPressureAlgorithm(strAlgorithm);

            int32_t offset;
        #ifdef __DEBUG_PROTOCOL
            offset = (8000000 + section * 1000 + strip * 100);
        #else
            offset = sectionBoardLinkSingleton(section)->m_pPIB->getOffsetChannel(strip);
        #endif
            pRunwlPayload->setPressureOffset(offset);

            int32_t factor;
        #ifdef __DEBUG_PROTOCOL
            factor = (65000 + section * 1000 + strip * 100);
        #else
            factor = sectionBoardLinkSingleton(section)->m_pPIB->readPressureConversionChannel(strip);
        #endif
            pRunwlPayload->setPressureFactor(factor);

        #ifdef __DEBUG_PROTOCOL
            factor = 1;
        #else
            factor = runwlInfoPointer(section)->getProtoTreatedPressure(strip);
        #endif
            pRunwlPayload->setPressureSendingRate(factor);

            strTreatedPressure.clear();
            if(runwlInfoPointer(section)->getProtoTreatedPressure(strip) > 0)
            {
                if(protocolDataSerialize(section, strip, WEB_CMD_DATA_TREATED_PRESSURE, strTreatedPressure) == true)
                {
                    // pass also the encoded data to be added as strings
                    pRunwlPayload->setTreatedPressure(strip, strTreatedPressure);
                }
            }

            strElaboratedPressure.clear();
            if(runwlInfoPointer(section)->getProtoElaboratedPressure(strip) == 1)
            {
                if(protocolDataSerialize(section, strip, WEB_CMD_DATA_ELABORATED_PRESSURE, strElaboratedPressure) == true)
                {
                    // pass also the encoded data to be added as strings
                    pRunwlPayload->setElaboratedPressure(strip, strElaboratedPressure);
                    log(LOG_DEBUG, "THReplyProtocol: strip %d Elab %d", strip, strElaboratedPressure.size());
                }
            }
        }
    }
    else
    {
        outCmdRunwl.setStatus(XML_ATTR_STATUS_FAILED);

        std::string strStatus;
        strStatus.assign(PROTOCOL_STATUS_PARTIALLY_DONE);

        pRunwlPayload->setProtocolStatus(strStatus);

        if(m_protocolAbortedCommand[section] == false)
        {
            // the protocol has aborted internally -> setFailureCause
            FailureCause* pFailureCause = new FailureCause(ERR_NONE);
            // get the error string from the devices:
            std::vector<std::string> vectorErrors;
            std::string strErrorComponent;

            vectorErrors.clear();

            // the error Context is the Section or the Slot if the error comes from Section
            // while for all others (NSH, CAMERA, INSTRUMENT) is Module

            // first the NSH (common device)
            nshBoardManagerSingleton()->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ERROR);
            if(vectorErrors.size() > 0)
            {
                strErrorComponent.assign(XML_TAG_NSH_NAME);
            }

            if(vectorErrors.size() == 0)
            {
                // then the involved section (only errors that stop protocol)
                sectionBoardManagerSingleton(section)->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ERROR);
                if(vectorErrors.size() > 0)
                {
                    if(section == SCT_A_ID)
                    {
                        strErrorComponent.assign(XML_TAG_SECTIONA_NAME);
                    }
                    else if(section == SCT_B_ID)
                    {
                        strErrorComponent.assign(XML_TAG_SECTIONB_NAME);
                    }
                }

            }

            // finally the masterboard
            if(vectorErrors.size() == 0)
            {
                masterBoardManagerSingleton()->getErrorList(&vectorErrors, GENERAL_ERROR_LEVEL_ERROR);
                if(vectorErrors.size() > 0)
                {
                    strErrorComponent.assign(XML_TAG_INSTRUMENT_NAME);
                }
            }

            if(vectorErrors.size() > 0)
            {
                // from the list extract the first one: it's enough !
                // and set the Component at the beginning
                strErrorComponent.append(HASHTAG_SEPARATOR);
                strErrorComponent.append(vectorErrors.at(0));
                pFailureCause->setCodeString(strErrorComponent);
                log(LOG_INFO, "THReplyProtocol Section %c failed -> %s",
                    section + A_CHAR, strErrorComponent.c_str());

                outCmdRunwl.setFailureCause(pFailureCause);
            }
        }
    }

    // and assign it to the outcommand so that the serialize will extract the info
    outCmdRunwl.setPayload((Payload *)pRunwlPayload);
    // Note: the payload will be deleted by the OutgoingCommand

    /* ********************************************************************************************
     * Send the OutgoingCommand to the client
     * ********************************************************************************************
     */
    std::string strOutCommand;
    bool bSerializeResult = protocolSerialize(&outCmdRunwl, strOutCommand);
    if ( bSerializeResult == false )
    {
        log(LOG_ERR, "THRunProtocol: unable to serialize outCmdRunwl");
    }
    else
    {
        // save the reply on file
        saveRunReply(section, strOutCommand);

        log(LOG_DEBUG_PARANOIC, "THReplyProtocol: %s", strOutCommand.c_str());
        bool bStatus = m_sectionOperation[section]->sendCommandReply(strOutCommand);

        // save the reply status locally and on file
        saveRunReplyOk(section, bStatus);
        // and update the corresponding flag in THRunProtocol
        protocolThreadPointer()->setSectionReply(section, bStatus);
    }

    // ask ProtocolThread to remove the Operation linked to section
    protocolThreadPointer()->deleteOperation(section);

	return bSerializeResult;
}

bool THReplyProtocol::sendReadSecReply(uint8_t section)
{
	log(LOG_INFO, "THReplyProtocol::sendReadSecReply");

	if ( m_readSecOperation[section] == nullptr )	return false;

	m_readSecOperation[section]->restoreStateMachine();

	OutCmdReadSec outCmdReadSec;
	outCmdReadSec.setUsage(m_readSecOperation[section]->getUsage());
	outCmdReadSec.setID(m_readSecOperation[section]->getID());

	// build the payload of the asynchrounous reply
	ReadSecPayload* pReadSecPayload = new ReadSecPayload();
	// pass to the payload the pointer to the runwl corresponding to section
	string sec = ( section == SCT_A_ID ) ? "A" : "B";
	pReadSecPayload->setParams(sec, &readSecInfoPointer(section)->getReadSecPayload());

	if ( m_bSuccess[section] )
	{
		outCmdReadSec.setStatus(XML_ATTR_STATUS_SUCCESSFULL);
	}
	else
	{
		outCmdReadSec.setStatus(XML_ATTR_STATUS_FAILED);
	}

	// and assign it to the outcommand so that the serialize will extract the info
	outCmdReadSec.setPayload((Payload*)pReadSecPayload);
	// Note: the payload will be deleted by the OutgoingCommand

	/* ********************************************************************************************
	 * Send the OutgoingCommand to the client
	 * ********************************************************************************************
	 */
	string strOutCommand;
	bool bSerializeResult = protocolSerialize(&outCmdReadSec, strOutCommand);
	if ( ! bSerializeResult )
	{
		log(LOG_ERR, "THReplyProtocol::sendReadSecReply: unable to serialize outCmdReadSec");
	}
	else
	{
		m_readSecOperation[section]->sendCommandReply(strOutCommand);
        // and update the corresponding flag in THRunProtocol
		protocolThreadPointer()->setSectionReply(section, m_bSuccess[section]);
	}

	// ask ProtocolThread to remove the Operation linked to section
	protocolThreadPointer()->deleteOperationReadSec(section);

	// clear all for next readsec command
	readSecInfoPointer(section)->clearAll();

	return bSerializeResult;
}

void saveRunReply(uint8_t section, std::string& strOutReply)
{
    // store the String reply to be used in case of resending
    char nameFile[64];

    sprintf(nameFile, "%s%s%c%s", RUN_FOLDER, RUN_REPLY_NAME, SECTION_CHAR_A + section, RUN_REPLY_EXTENSION);
    std::ofstream outFile(nameFile);
    outFile << strOutReply;
    outFile.close();
}

void saveRunReplyOk(uint8_t section, bool status)
{
    // store the String reply status to be read at startup
    char nameFile[64];
    std::string strOutReply;

    sprintf(nameFile, "%s%s%c%s", RUN_FOLDER, RUN_REPLY_STATUS, SECTION_CHAR_A + section, RUN_REPLY_EXTENSION);
    std::ofstream outFile(nameFile, ios_base::out);
    if(status == true)
    {
        strOutReply.assign("OK");
    }
    else
    {
        strOutReply.assign("KO");
    }
    outFile << strOutReply << std::endl;
    outFile.close();
}

