/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    RunwlInfo.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the RunwlInfo class.
 @details

 ****************************************************************************
*/

#ifndef RUNWLINFO_H
#define RUNWLINFO_H

#include <stdint.h>
#include <string>
#include <list>
#include <vector>

#include "CommonInclude.h"

#define CONSTRAINT_TIME_MINUTES	"min"
#define CONSTRAINT_TIME_SECONDS	"s"

#define PROTO_TEMPERATURE_PREFIX	"./Temperature"
#define PROTO_TEMPERATURE_SUFFIX	".bin"

#define PROTO_TREATED_PREFIX		"./TreatedPressure"
#define PROTO_TREATED_SUFFIX		".bin"

#define PROTO_ELABORATED_PREFIX		"./ElaboratedPressure"
#define PROTO_ELABORATED_SUFFIX		".bin"

typedef struct
{
	uint64_t	JtimeRead;		// system time of the reading
	uint32_t	JfluoRead;
	uint32_t	JfluoMin;
	uint32_t	JfluoMax;
	uint32_t	JfluoCv;
	uint32_t	JerrorCode;
}structStripReading;

typedef struct
{
	// info read from the incoming command
	std::string	strId;

	std::string strSettings;

	uint8_t		rfuConv;
	uint8_t		measureAir;
	uint8_t		treatedPressure;
	uint8_t		elaboratedPressure;

	uint8_t		liquidProfile[SCT_SLOT_NUM_WELLS];
	uint8_t		pressureProfile[SCT_SLOT_NUM_WELLS];

	// data read from the NSH during execution
	structStripReading readingData[J_MAXMIN_VECTOR_SIZE];
	// Air read from the NSH at the end of execution
	uint32_t	readingAir;
	uint64_t	readingAirTime;

} structStripRunwl;

class RunwlInfo
{
	public:

		/*! ***********************************************************************************************************
		 * @brief RunwlInfo default constructor
		 * ***********************************************************************************************************
		 */
		RunwlInfo();

		/*! *************************************************************************************************
		 * @brief RunwlInfo destructor
		 * **************************************************************************************************
		 */
		virtual ~RunwlInfo();


		/*! *************************************************************************************************
		 * @brief setSection set the Protocol Section read from the incoming command
		 * @param strSection the protocol section (A or B) to be translated into int value
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSection(std::string strSection);

		/*! *************************************************************************************************
		 * @brief setProtoId set the Protocol ID read from the incoming command
		 * @param strId the protocol id
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoId(std::string strId);

		/*! *************************************************************************************************
		 * @brief setUnitConstraint set the Protocol Constraint Unit read from the incoming command
		 * @param unitConstraint the Constraint Unit string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setUnitConstraint(std::string unitConstraint);

		/*! *************************************************************************************************
		 * @brief setMinConstraint set the Protocol Constraint Minimum read from the incoming command
		 * @param minConstraint the Constraint Minimum value
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setMinConstraint(uint32_t minConstraint);

		/*! *************************************************************************************************
		 * @brief setMaxConstraint set the Protocol Constraint Maximum read from the incoming command
		 * @param maxConstraint the Constraint Maximum value
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setMaxConstraint(uint32_t maxConstraint);

		/*! *************************************************************************************************
		 * @brief setMonitoringTemperature set the Temperature Monitoring flag read from the incoming command
		 * @param value the flag (1 or 0)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setMonitoringTemperature(uint8_t value);

		/*! *************************************************************************************************
		 * @brief setMonitoringAutocheck set the Autocheck Monitoring flag read from the incoming command
		 * @param value the flag (1 or 0)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setMonitoringAutocheck(uint8_t value);

		/*! *************************************************************************************************
		 * @brief setLastTime set the section protocol end time as int
		 * @param value the end time value
		 * **************************************************************************************************
		 */
		bool setLastTime(uint64_t value);

		/*! *************************************************************************************************
		 * @brief setStripId set the Strip ID read from the incoming command
		 * @param strip the strip index
		 * @param strId the ID string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
        bool setStripId(int strip, std::string strId);

		/*! *************************************************************************************************
		 * @brief setStripRfuconv set the Strip Rfuconv read from the incoming command
		 * @param strip the strip index
         * @param rfuconv the Rfuconv flag (0 or 1)
         * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
        bool setStripRfuconv(int strip, uint8_t rfuconv);

		/*! *************************************************************************************************
		 * @brief setStripProfile set the Strip Profile read from the incoming command
		 * @param strip the strip index
		 * @param strProfile the Profile string to be splitted per each well
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setStripProfile(int strip, std::string strProfile);

		/*! *************************************************************************************************
		 * @brief setStripCheck set the Strip Check read from the incoming command
		 * @param strip the strip index
		 * @param strCheck the Check string to be splitted per each well
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setStripCheck(int strip, std::string strCheck);

		/*! *************************************************************************************************
		 * @brief setProtoSettings set the Protocol Settings reference read from the incoming command
		 * @param strip the strip index
		 * @param strSettings the protocol settings string to be linked to AcquisitionSettings
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoSettings(int strip, std::string strSettings);

		/*! *************************************************************************************************
		 * @brief setProtoMeasureAir set the Protocol Measure Air flag read from the incoming command
		 * @param strip the strip index
		 * @param value the flag value
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoMeasureAir(int strip, uint8_t value);

		/*! *************************************************************************************************
		 * @brief setProtoTreatedPressure set the Protocol Treated Pressure flag read from the incoming command
		 * @param strip the strip index
		 * @param value the flag value
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoTreatedPressure(int strip, uint8_t value);

		/*! *************************************************************************************************
		 * @brief setProtoElaboratedPressure set the Protocol Elaborated Pressure flag read from the incoming command
		 * @param strip the strip index
		 * @param value the flag value
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoElaboratedPressure(int strip, uint8_t value);

		/*! *************************************************************************************************
		 * @brief getSection get the Protocol Section read from the incoming command
		 * @return the protocol section
		 * **************************************************************************************************
		 */
		uint8_t getSection();

		/*! *************************************************************************************************
		 * @brief getProtoId get the Protocol ID read from the incoming command
		 * @return the protocol id
		 * **************************************************************************************************
		 */
		std::string getProtoId();

		/*! *************************************************************************************************
		 * @brief getUnitConstraint get the Protocol Constraint Unit read from the incoming command
		 * @return the Constraint Unit string
		 * **************************************************************************************************
		 */
		std::string getUnitConstraint();

		/*! *************************************************************************************************
		 * @brief getValidConstraint return the status of the Constraint string (set by decodeConstraint)
		 * @return the Constraint valid flag
		 * **************************************************************************************************
		 */
		bool getValidConstraint();

		/*! *************************************************************************************************
		 * @brief getMinimumConstraint return the Constrint minimum time (0 if not set)
		 * @return the minimum time constraint
		 * **************************************************************************************************
		 */
		uint32_t getMinimumConstraint();

		/*! *************************************************************************************************
		 * @brief getMaximumConstraint return the Constraint maximum time (0 if not set)
		 * @return the maximum time constraint
		 * **************************************************************************************************
		 */
		uint32_t getMaximumConstraint();

		/*! *************************************************************************************************
		 * @brief getLastTime return the section protocol end time as int
		 * @return the end time string
		 * **************************************************************************************************
		 */
		std::string getLastTime();

		/*! *************************************************************************************************
		 * @brief getStripId get the Strip ID read from the incoming command
		 * @param strip the strip index
		 * @return the ID string
		 * **************************************************************************************************
		 */
		std::string getStripId(int strip);

		/*! *************************************************************************************************
		 * @brief getStripRfuconv get the Strip Rfuconv read from the incoming command
		 * @param strip the strip index
		 * @return the Rfuconv flag
		 * **************************************************************************************************
		 */
		uint8_t getStripRfuconv(int strip);

		/*! *************************************************************************************************
		 * @brief getProtoSettings get the Protocol Settings reference read from the incoming command
		 * @param strip the strip index
		 * @return the protocol settings string to be linked to AcquisitionSettings
		 * **************************************************************************************************
		 */
		std::string getProtoSettings(int strip);

		/*! *************************************************************************************************
		 * @brief getProtoMeasureAir get the Protocol Measure Air flag read from the incoming command
		 * @param strip the strip index
		 * @return the flag value
		 * **************************************************************************************************
		 */
		uint8_t getProtoMeasureAir(int strip);

		/*! *************************************************************************************************
		 * @brief getProtoTreatedPressure get the Protocol Treated Pressure flag read from the incoming command
		 * @param strip the strip index
		 * @return the flag value
		 * **************************************************************************************************
		 */
		uint8_t getProtoTreatedPressure(int strip);

		/*! *************************************************************************************************
		 * @brief getProtoElaboratedPressure get the Protocol Elaborated Pressure flag read from the incoming command
		 * @param strip the strip index
		 * @return the flag value
		 * **************************************************************************************************
		 */
		uint8_t getProtoElaboratedPressure(int strip);

		/*! *************************************************************************************************
		 * @brief getStripWellProfile get the Strip Profile read from the incoming command
		 * @param strip the strip index
         * @param well the well index
         * @return the Profile string splitted per each well
		 * **************************************************************************************************
		 */
		uint8_t getStripWellProfile(int strip, int well);

		/*! *************************************************************************************************
		 * @brief getStripWellCheck get the Strip Check read from the incoming command
		 * @param strip the strip index
         * @param well the well index
		 * @return the Check string splitted per each well
		 * **************************************************************************************************
		 */
		uint8_t getStripWellCheck(int strip, int well);

		/*! *************************************************************************************************
		 * @brief getStripProfile get the pointer to the Strip Profile read from the incoming command
		 * @param strip the strip index
		 * @return pointer
		 * **************************************************************************************************
		 */
		uint8_t * getStripProfile(int strip);

		/*! *************************************************************************************************
		 * @brief getStripCheck get the pointer to the Strip Check read from the incoming command
		 * @param strip the strip index
		 * @return pointer
		 * **************************************************************************************************
		 */
		uint8_t * getStripCheck(int strip);

		/*! *************************************************************************************************
		 * @brief getMonitoringTemperature get the Temperature Monitoring flag read from the incoming command
		 * @return the falg value
		 * **************************************************************************************************
		 */
		bool getMonitoringTemperature();

		/*! *************************************************************************************************
		 * @brief getMonitoringAutocheck get the Autocheck Monitoring flag read from the incoming command
		 * @return the flag value
		 * **************************************************************************************************
		 */
		bool getMonitoringAutocheck();

		/*! *************************************************************************************************
		 * @brief isAtLeastOneStripEnabled to check if at least one strip of the section is enable
		 *					(RunId not empty)
		 * @return false if no strip is enable, true otherwise
		 * **************************************************************************************************
		 */
		bool isAtLeastOneStripEnabled();

		/*! *************************************************************************************************
		 * @brief copyRunwlCommandInfo to copy to the internal structure the data of the runwlinfo parameter
		 *				read from the RUNWL command
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool copyRunwlCommandInfo(RunwlInfo * pRunwlInfo);

		/*! *************************************************************************************************
		 * @brief clearAllData to clear all the data of the object
		 * **************************************************************************************************
		 */
		void clearAllData();

		/*! *************************************************************************************************
		 * @brief buildJtoPerform from the array of relative reading time (calculated by NVTimeCalc)
		 *			build the absolute reading time array
		 * @param JgapsRel poiter to the array of Relative readings time
		 * **************************************************************************************************
		 */
		void buildJtoPerform(uint32_t * JgapsRel);

		/*! *************************************************************************************************
		 * @brief decodeConstraint decode the Constraint string received from command: only T or M are allowed
		 * @param true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool decodeConstraint();

		/*! *************************************************************************************************
		 * @brief findReadingToSet function called during the protocol: when the read values are available
		 *	the JToPerform array is scanned: the first value not zero (index valid) for which the strips have no
		 *  associated time -> this is the reading index to set
		 * @return the index if found, maximum value otherwise
		 * **************************************************************************************************
		 */
		uint8_t findReadingToSet();

		/*! *************************************************************************************************
		 * @brief setStripReadingIndexTime to set the reading time value for a specific strip-index
		 * @param strip the strip index
		 * @param index the reading index
		 * @param value the time value
		 * @return true if ok, false otherwise
		 * **************************************************************************************************
		 */
		bool setStripReadingIndexTime(uint8_t strip, uint8_t index, uint64_t value);

		/*! *************************************************************************************************
		 * @brief setStripReadingIndexValues to set the reading values for a specific strip-index
		 * @param strip the strip index
		 * @param index the reading index
		 * @param minReading the minimum value
		 * @param maxReading the maximum value
		 * @param meanReading the mean value
		 * @return true if ok, false otherwise
		 * **************************************************************************************************
		 */
		bool setStripReadingIndexValues(uint8_t strip, uint8_t index, int32_t minReading,
										int32_t maxReading, int32_t meanReading);

		/*! *************************************************************************************************
		 * @brief getReadingValueJ to get the value of a specific reading index
		 * @param index the reading index
		 * @return the value (0 if reading not set)
		 * **************************************************************************************************
		 */
		uint32_t getReadingValueJ(uint8_t index);

		/*! *************************************************************************************************
		 * @brief getStripReadingIndexTime to get the reading time value for a specific strip-index
		 * @param strip the strip index
		 * @param index the reading index
		 * @return the time value
		 * **************************************************************************************************
		 */
		uint64_t getStripReadingIndexTime(uint8_t strip, uint8_t index);

		/*! *************************************************************************************************
		 * @brief getStripReadingIndexValues to get the reading values for a specific strip-index
		 * @param strip the strip index
		 * @param index the reading index
		 * @param minReading (output) the minimum value
		 * @param maxReading (output) the maximum value
		 * @param meanReading (output) the mean value
		 * @return true if ok, false otherwise
		 * **************************************************************************************************
		 */
		bool getStripReadingIndexValues(uint8_t strip, uint8_t index,
												   int32_t * minReading, int32_t * maxReading, int32_t * meanReading);

		/*! *************************************************************************************************
		 * @brief setStripReadingAir to set the Air reading values for a specific strip
		 * @param strip the strip index
		 * @param value the Air value
		 * @return true if ok, false otherwise
		 * **************************************************************************************************
		 */
		bool setStripReadingAir(uint8_t strip, uint32_t value);

		/*! *************************************************************************************************
		 * @brief getStripReadingAir to get the Air reading values for a specific strip
		 * @param strip the strip index
		 * @return the Air value (0 in case of error)
		 * **************************************************************************************************
		 */
		uint32_t getStripReadingAir(uint8_t strip);

		/*! *************************************************************************************************
		 * @brief setStripReadingAirTime to set the Air reading Time values for a specific strip
		 * @param strip the strip index
		 * @param value the AirTime value
		 * @return true if ok, false otherwise
		 * **************************************************************************************************
		 */
		bool setStripReadingAirTime(uint8_t strip, uint64_t value);

		/*! *************************************************************************************************
		 * @brief getStripReadingAirTime to get the Air Time reading values for a specific strip
		 * @param strip the strip index
		 * @return the AirTime value (0 in case of error)
		 * **************************************************************************************************
		 */
		uint32_t getStripReadingAirTime(uint8_t strip);

		/*! *************************************************************************************************
		 * @brief setStartTime to set the protocol start time
		 * **************************************************************************************************
		 */
		void setStartTime();

		/*! *************************************************************************************************
		 * @brief getStartTime to set the protocol start time
		 * @return the protocol start time
		 * **************************************************************************************************
		 */
		uint64_t getStartTime();

	private :

		uint8_t		m_section;
		std::string	m_strProtoId;

		std::string	m_unitConstraint;
		bool		m_validConstraint;
		uint32_t	m_minDelayConstraint, m_maxDelayConstraint;
		uint64_t	m_startTime;

		bool		m_bMonitoringTemperature, m_bMonitoringAutocheck;

		// data calculated by NVTimeCalc
		uint32_t	m_JtoPerform[J_MAXMIN_VECTOR_SIZE];

		structStripRunwl	m_stripRunwl[SCT_NUM_TOT_SLOTS];

		uint32_t	m_minimumTime, m_maximumTime;
		std::string	m_strSectionLastTime;
};

#endif // RUNWLINFO_H
