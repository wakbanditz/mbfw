/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ProtocolInfo.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the ProtocolInfo class.
 @details

 ****************************************************************************
*/

#include <string.h>
#include <stdio.h>

#include "ProtocolInfo.h"

ProtocolInfo::ProtocolInfo()
{
	clearAllData();
}

ProtocolInfo::~ProtocolInfo()
{
	m_timeListJminJmax.clear();
}

bool ProtocolInfo::setProtoId(std::string strId)
{
	m_protoId.assign(strId);
	return true;
}

bool ProtocolInfo::setProtoName(std::__cxx11::string strName)
{
	m_strName.assign(strName);
	return true;
}

bool ProtocolInfo::setProtoGlobals(const char* strGlobals)
{
	strcpy(m_strGlobals, strGlobals);
	return true;
}

bool ProtocolInfo::setProtoLocals(const char* strLocals)
{
	strcpy(m_strLocals, strLocals);
	return true;
}

bool ProtocolInfo::setProtoMinMaxJ(uint32_t value, uint8_t index, uint8_t isMax)
{
	if(index >= J_MAXMIN_VECTOR_SIZE)	return false;

	if(isMax == 1)
	{
		m_timeListJminJmax.at(index).second = value;
	}
	else
	{
		m_timeListJminJmax.at(index).first = value;
	}
	return true;
}


std::string ProtocolInfo::getProtoId()
{
	return m_protoId;
}

std::string ProtocolInfo::getProtoName()
{
	return m_strName;
}

bool ProtocolInfo::getProtoGlobals(char* strGlobals)
{
	if(strGlobals)
	{
		strcpy(strGlobals, m_strGlobals);
		return true;
	}
	return false;
}

bool ProtocolInfo::getProtoLocals(char* strLocals)
{
	if(strLocals)
	{
		strcpy(strLocals, m_strLocals);
		return true;
	}
	return false;
}

uint32_t ProtocolInfo::getProtoMinMaxJ(uint8_t index, uint8_t isMax)
{
	uint32_t value = 0;

	if(index >= J_MAXMIN_VECTOR_SIZE)	return value;

	std::pair<uint32_t, uint32_t> timePair;

	timePair = m_timeListJminJmax.at(index);
	if(isMax == 1)
	{
		value = timePair.second;
	}
	else
	{
		value = timePair.first;
	}
	return value;
}

void ProtocolInfo::clearAllData()
{
	m_protoId.clear();
	m_strGlobals[0] = 0;
	m_strLocals[0] = 0;
	m_strName.clear();

	for(uint8_t i = 0; i < J_MAXMIN_VECTOR_SIZE; i++)
		m_timeListJminJmax.emplace_back(0, 0);
}

bool ProtocolInfo::copyProtocolInfo(ProtocolInfo* pProtocolInfo)
{
	if(pProtocolInfo == 0) return false;

	m_protoId = pProtocolInfo->getProtoId();
	pProtocolInfo->getProtoGlobals(m_strGlobals);
	pProtocolInfo->getProtoLocals(m_strLocals);
	m_strName = pProtocolInfo->getProtoName();

	for(uint8_t i = 0; i < J_MAXMIN_VECTOR_SIZE; i++)
	{
		setProtoMinMaxJ(pProtocolInfo->getProtoMinMaxJ(i, 0), i, 0);
		setProtoMinMaxJ(pProtocolInfo->getProtoMinMaxJ(i, 1), i, 1);
	}
	return true;
}

