#ifndef CAMERASETTINGS_H
#define CAMERASETTINGS_H

#include <string>
#include <vector>

using namespace std;

typedef vector<pair<string, int>> settings;

class CameraSettings
{
	public:

		CameraSettings();

		virtual ~CameraSettings();

		void setVersion(string strVersion, uint liSettingId);

		void setAlgorithm(string strAlgo, uint liSettingId);

		void addSettingPair(string strKey, string strVal, uint liSettingId);

		bool isScheduledReadSec(uint liSettingId);

		int getDetectionThreshold(uint liSettingId);

		bool hasSprToBeChecked(uint liSettingId);

		bool hasDtmxToBeDecoded(uint liSettingId);

	private:

		// settings = vector<Key - value>
		// key can be "DetectionThreshold" or "SkipSprCheck" or "SkipDtmxRead"
		vector<settings> vConfig;
		vector<string> vVersion;
		vector<string> vAlgorithm;
};

#endif // CAMERASETTINGS_H
