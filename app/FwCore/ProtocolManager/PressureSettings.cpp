/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PressureSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the PressureSettings class.
 @details

 ****************************************************************************
*/

#include "PressureSettings.h"

#define RATIO_VIDAS3        (65536.0 / 19927.0)

PressureSettings::PressureSettings()
{
	m_strID.clear();
	// set default values as defined in Section Application
	m_intAreaRatio = 50;
	m_intMinimumPositionPercentage = 75;
	m_intFinalSlopePercentage = 80;
	m_intFinalSlopeThreshold = -10;

	m_intIndustryThresholdLow = 0;
	m_intIndustryThresholdHigh = 0;
	m_intIndustryCyclesPercentage = 25;
	m_intIndustryCyclesMinimum = 10;
	m_intIndustryCyclesThreshold = 70;
}

void PressureSettings::setID(std::__cxx11::string strID)
{
	m_strID.assign(strID);
}

void PressureSettings::setVersion(std::__cxx11::string strVersion)
{
	m_strVersion.assign(strVersion);
}

void PressureSettings::setAlgorithm(std::__cxx11::string strAlgorithm)
{
	m_strAlgorithm.assign(strAlgorithm);
}

void PressureSettings::setAreaRatio(uint16_t value)
{
	m_intAreaRatio = value;
}

void PressureSettings::setMinimumPositionPercentage(uint16_t value)
{
	m_intMinimumPositionPercentage = value;
}

void PressureSettings::setFinalSlopePercentage(uint16_t value)
{
	m_intFinalSlopePercentage = value;
}

void PressureSettings::setFinalSlopeThreshold(int16_t value)
{
	m_intFinalSlopeThreshold = value;
}

void PressureSettings::setIndustryThresholdLow(uint64_t value)
{
	m_intIndustryThresholdLow = value;
}

void PressureSettings::setIndustryThresholdHigh(uint64_t value)
{
	m_intIndustryThresholdHigh = value;
}

void PressureSettings::setIndustryCyclesPercentage(uint16_t value)
{
	m_intIndustryCyclesPercentage = value;
}

void PressureSettings::setIndustryCyclesMinimum(uint16_t value)
{
	m_intIndustryCyclesMinimum = value;
}

void PressureSettings::setIndustryCyclesThreshold(uint16_t value)
{
	m_intIndustryCyclesThreshold = value;
}

void PressureSettings::setSpeedVolumeThresholds(char speed, char volume, uint64_t low, uint64_t high)
{
	m_vecThresholds.push_back(std::make_tuple(speed, volume, low, high));
}

bool PressureSettings::copyPressureSettings(PressureSettings* pPressureSettings)
{
	if(pPressureSettings == 0) return false;

	m_strID.assign(pPressureSettings->getID());
	m_strVersion.assign(pPressureSettings->getVersion());
	m_strAlgorithm.assign(pPressureSettings->getAlgorithm());

	m_intAreaRatio = pPressureSettings->getAreaRatio();
	m_intFinalSlopePercentage = pPressureSettings->getFinalSlopePercentage();
	m_intFinalSlopeThreshold = pPressureSettings->getFinalSlopeThreshold();
	m_intMinimumPositionPercentage = pPressureSettings->getMinimumPositionPercentage();

	m_intIndustryCyclesMinimum = pPressureSettings->getIndustryCyclesMinimum();
	m_intIndustryCyclesPercentage = pPressureSettings->getIndustryCyclesPercentage();
	m_intIndustryCyclesThreshold = pPressureSettings->getIndustryCyclesThreshold();
	m_intIndustryThresholdHigh = pPressureSettings->getIndustryThresholdHigh();
	m_intIndustryThresholdLow = pPressureSettings->getIndustryThresholdLow();

	m_vecThresholds.clear();

	uint8_t index = 0;
	char speed, volume;
	uint64_t low, high;

	while(true)
	{
		if(pPressureSettings->getSpeedVolumeThresholds(index, &speed, &volume, &low, &high) == false)
			break;

		setSpeedVolumeThresholds(speed, volume, low, high);
		index++;
	}

	return true;
}

std::__cxx11::string PressureSettings::getID()
{
	return m_strID;
}

std::__cxx11::string PressureSettings::getVersion()
{
	return m_strVersion;
}

std::__cxx11::string PressureSettings::getAlgorithm()
{
	return m_strAlgorithm;
}

uint16_t PressureSettings::getAreaRatio()
{
	return m_intAreaRatio;
}

uint16_t PressureSettings::getMinimumPositionPercentage()
{
	return m_intMinimumPositionPercentage;
}

uint16_t PressureSettings::getFinalSlopePercentage()
{
	return m_intFinalSlopePercentage;
}

int16_t PressureSettings::getFinalSlopeThreshold()
{
	return m_intFinalSlopeThreshold;
}

uint64_t PressureSettings::getIndustryThresholdLow()
{
	return m_intIndustryThresholdLow;
}

uint64_t PressureSettings::getIndustryThresholdHigh()
{
	return m_intIndustryThresholdHigh;
}

uint16_t PressureSettings::getIndustryCyclesPercentage()
{
	return m_intIndustryCyclesPercentage;
}

uint16_t PressureSettings::getIndustryCyclesMinimum()
{
	return m_intIndustryCyclesMinimum;
}

uint16_t PressureSettings::getIndustryCyclesThreshold()
{
	return m_intIndustryCyclesThreshold;
}

bool PressureSettings::getSpeedVolumeThresholds(uint8_t index, char* speed, char* volume, uint64_t* low, uint64_t* high)
{
	if(index >= m_vecThresholds.size()) return false;

	std::tuple<char, char, uint64_t, uint64_t> tmpTuple;
	tmpTuple = m_vecThresholds.at(index);

	if(speed)	*speed = std::get<0>(tmpTuple);
	if(volume)	*volume = std::get<1>(tmpTuple);
	if(low)		*low = std::get<2>(tmpTuple);
	if(high)	*high = std::get<3>(tmpTuple);

	return true;
}
