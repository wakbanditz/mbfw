/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    HttpPageBase.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the HttpPageBase class.
 @details

 ****************************************************************************
*/

#ifndef HTTPBASE_H
#define HTTPBASE_H

#include <string>
#include "Loggable.h"
//#include <xercesc/parsers/XercesDOMParser.hpp>

//using namespace xercesc;

class WebServerConnection;

class HttpPageBase	:	public Loggable
{

	public:

		HttpPageBase(std::string strName);
		virtual ~HttpPageBase();
		std::string getName(void);
		virtual bool handler(WebServerConnection& rConnection) = 0;

    protected:

        std::string m_strName;

};

#endif // HTTPBASE_H
