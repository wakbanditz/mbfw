/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    HttpPageProcessCommand.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the HttpPageProcessCommand class.
 @details

 ****************************************************************************
*/

#ifndef HTTPCOMMAND_H
#define HTTPCOMMAND_H

//#include <xercesc/parsers/XercesDOMParser.hpp>

#include "HttpPageBase.h"

class HttpPageProcessCommand : public HttpPageBase
{

	public:

        HttpPageProcessCommand(std::string sName, std::string sSerial);

		virtual ~HttpPageProcessCommand();

        bool handler(WebServerConnection &rConnection);

    private :

        std::string m_strSerial;
};

#endif // HTTPCOMMAND_H
