/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    HttpPageContainer.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the HttpPageContainer class.
 @details

 ****************************************************************************
*/

#include "HttpPageContainer.h"
#include "HttpPageBase.h"
#include "CommonInclude.h"
#include "WebServerConnection.h"


HttpPageContainer::HttpPageContainer()
{
//	m_pXercesDOMParser = 0;
}

HttpPageContainer::~HttpPageContainer()
{
//	SAFE_DELETE(m_pXercesDOMParser);
//	XMLPlatformUtils::Terminate();
//	log(LOG_INFO, "HttpPageContainer::~HttpPageContainer: XMLPlatformUtils terminated");
}

//bool HttpPageContainer::initXercesXmlParser(void)
//{
//	/*! ***************************************************************************************************************
//	 * Initializes the xerces infrastructure
//	 * ****************************************************************************************************************
//	 */
//	try
//	{
//		XMLPlatformUtils::Initialize();
//	}
//	catch( XMLException& e )
//	{
//		char* sMsg = XMLString::transcode(e.getMessage());
//		log(LOG_ERR, "HttpPageContainer::initXercesXmlParser: XMLPlatformUtils error: \"%s\"", sMsg);
//		XMLString::release(&sMsg);
//		return false;
//	}
//	log(LOG_INFO, "HttpPageContainer::initXercesXmlParser: XMLPlatformUtils initialization done");

//	/*! ***************************************************************************************************************
//	 * Create the xerces xml DOM parser, then attach an error handler to it
//	 * The parser will call back to methods of the ErrorHandler if it discovers errors while parsing the XML buffer
//	 * ****************************************************************************************************************
//	 */
//	SAFE_DELETE(m_pXercesDOMParser);
//	m_pXercesDOMParser = new XercesDOMParser;
//	if ( m_pXercesDOMParser == 0 )
//	{
//		log(LOG_ERR, "HttpPageContainer::initXercesXmlParser: memory error allocating xerces DOMParser");
//		return false;
//	}

//	// 	Do not report validation errors
//	m_pXercesDOMParser->setValidationScheme(xercesc::XercesDOMParser::Val_Never);

//	// If the validation scheme is set to Val_Always or Val_Auto
//	// then the document must contain a grammar that supports the use of namespaces
//	// In our case we do not perform Namespace processing
//	m_pXercesDOMParser->setDoNamespaces(false);

//	// Disable the parser's schema support
//	m_pXercesDOMParser->setDoSchema(false);

//	// Don't import multiple schemas with the same namespace
//	m_pXercesDOMParser->setHandleMultipleImports(false);

//	// Disable full schema constraint checking
//	m_pXercesDOMParser->setValidationSchemaFullChecking(false);

//	// Do not create EntityReference nodes in the DOM tree.
//	// No EntityReference nodes will be created,
//	// only the nodes corresponding to their fully expanded substitution text will be created.
//	m_pXercesDOMParser->setCreateEntityReferenceNodes(false);

//	m_pXercesDOMParser->setLoadExternalDTD(false);
//	m_pXercesDOMParser->setDoXInclude(false);

//	// Add the error handler, now is just a logger
//	m_pDomErrorReporter = new DOMTreeErrorReporter();
//	m_pDomErrorReporter->setLogger(getLogger());
//	m_pXercesDOMParser->setErrorHandler(m_pDomErrorReporter);

//	log(LOG_INFO, "HttpPageContainer::initXercesXmlParser: xerces DOMParser initialization done");

//	return true;
//}

bool HttpPageContainer::addPage(HttpPageBase& rHttpPage)
{
	std::string strNameToInsert(rHttpPage.getName());

	// If the page is already in the container it is ignored
	std::list<HttpPageBase*>::iterator it;
	for ( it = m_pageList.begin(); it != m_pageList.end(); ++it )
	{
		std::string strName((*it)->getName());
		if ( strName.compare(strNameToInsert) ==  0 )
		{
			log(LOG_INFO, "HttpPageContainer::addPage: \"%s\" already present", strName.c_str() );
			return false;
		}
	}
	// Insert the page in the container
	m_pageList.push_back(&rHttpPage);

	log(LOG_INFO, "HttpPageContainer::addPage: \"%s\" added", strNameToInsert.c_str() );
	return true;
}

bool HttpPageContainer::removePage(std::string strNameToRemove)
{
	std::list<HttpPageBase*>::iterator it;
	for ( it = m_pageList.begin(); it != m_pageList.end(); ++it )
	{
		HttpPageBase* pPageToRemove = (*it);
		std::string strName(pPageToRemove->getName());
		if ( strName.compare(strNameToRemove) ==  0 )
		{
			SAFE_DELETE(pPageToRemove);
			m_pageList.erase(it);
			log(LOG_INFO, "HttpPageContainer::removePage: \"%s\" removed", strNameToRemove.c_str());
			return true;
		}
	}
	log(LOG_INFO, "HttpPageContainer::removePage: \"%s\" not found", strNameToRemove.c_str());
	return false;
}

void HttpPageContainer::removeAll(void)
{
	std::list<HttpPageBase*>::iterator it;
	for ( it = m_pageList.begin(); it != m_pageList.end(); ++it )
	{
		HttpPageBase* pPageToRemove = (*it);
		std::string strName(pPageToRemove->getName());
		SAFE_DELETE(pPageToRemove);
		log(LOG_INFO, "HttpPageContainer::removeAll: page \"%s\" removed", strName.c_str());
	}
	m_pageList.clear();
	log(LOG_INFO, "HttpPageContainer::removeAll: container is empty");
}

void HttpPageContainer::printPageList(void)
{
	log(LOG_INFO, "HttpPageContainer::printPageList: the following pages have been registered");
	std::list<HttpPageBase*>::iterator it;
	int i = 1;
	for ( it = m_pageList.begin(); it != m_pageList.end(); ++it )
	{
		log(LOG_INFO, "HttpPageContainer::printPageList: %d) \"%s\"", i, (*it)->getName().c_str());
		i++;
	}
}

//XercesDOMParser* HttpPageContainer::getParser(void)
//{
//	return m_pXercesDOMParser;
//}

HttpPageBase* HttpPageContainer::getPageByName(std::string strPageName)
{
	HttpPageBase* pRetValue = 0;
	std::list<HttpPageBase*>::iterator it;
	for ( it = m_pageList.begin(); it != m_pageList.end(); ++it )
	{
		HttpPageBase* pTemp = (*it);
		std::string strName(pTemp->getName());
		if ( strName.compare(strPageName) ==  0 )
		{
			pRetValue = pTemp;
			//log(LOG_DEBUG, "HttpPageContainer::getPageByName: page \"%s\" found", strName.c_str());
			break;
		}
	}
	return pRetValue;
}

bool HttpPageContainer::processConnection(WebServerConnection* pConnection, void* pHttpPageContainer)
{
	if ( ( pConnection == 0 ) || ( pHttpPageContainer == 0 ) )
		return false;

	HttpPageContainer* pContainer = (HttpPageContainer*)pHttpPageContainer;
	Log* pLog = pContainer->getLogger();
	//XercesDOMParser* pParser = 0;
	//pParser = pContainer->getParser();

	//if ( pParser == 0 )
	//{
	//	pLog->log(LOG_ERR, "HttpPageContainer::processConnection: xml parser not initialized");
	//	pConnection->SendInternalError();
	//	return false;
	//}
	std::string strPageName(pConnection->GetPage());
	HttpPageBase* pPage = pContainer->getPageByName(strPageName);

	bool bRetValue = false;
	if ( pPage == 0 )
	{
		pLog->log(LOG_WARNING, "HttpPageContainer::processConnection: page \"%s\" not found", strPageName.c_str());
		pConnection->SendNotFound();
	}
	else
	{
		pLog->log(LOG_DEBUG_PARANOIC, "HttpPageContainer::processConnection: serving page \"%s\"", strPageName.c_str());
		//bRetValue = pPage->handler(*pConnection, *pParser);
		bRetValue = pPage->handler(*pConnection);
	}
	return bRetValue;
}
