/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    HttpPageContainer.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the HttpPageContainer class.
 @details

 ****************************************************************************
*/

#ifndef HTTPPAGECONTAINER_H
#define HTTPPAGECONTAINER_H

#include <list>
#include <string>

//#include <xercesc/util/PlatformUtils.hpp>
//#include <xercesc/dom/DOM.hpp>
//#include <xercesc/parsers/XercesDOMParser.hpp>
//#include "DOMTreeErrorReporter.h"
#include "Loggable.h"

//using namespace xercesc;

class HttpPageBase;
class WebServerConnection;

class HttpPageContainer	:	public Loggable
{

	public:

		/*! ***********************************************************************************************************
		 * @brief HttpPageContainer		void constructor
		 * ************************************************************************************************************
		 */
		HttpPageContainer();

		/*! ***********************************************************************************************************
		 * @brief ~HttpPageContainer	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~HttpPageContainer();

		/*! ***********************************************************************************************************
		 * @brief addPage				add a HttpPage to the container if not present.
		 *								Page must be already defined elsewhere.
		 * @param rHttpPage				reference to the page to add
		 * @return						true upon succesfull insertion, false otherwise
		 * ************************************************************************************************************
		 */
		bool addPage(HttpPageBase& rHttpPage);

		/*! ***********************************************************************************************************
		 * @brief removePage			remove the page with the specified name from the container, if present,
		 *								deleting the corresponding pointed object
		 * @param strNameToRemove		name of the page to remove
		 * @return						true if the page was present and correctly removed, false otherwise
		 * ************************************************************************************************************
		 */
		bool removePage(std::string strNameToRemove);

		/*! ***********************************************************************************************************
		 * @brief removeAll				remove all the pages in the container deleting all the corresponding objects
		 * ************************************************************************************************************
		 */
		void removeAll(void);

		/*! ***********************************************************************************************************
		 * @brief printPageList			log out all the pages names in the container
		 * ************************************************************************************************************
		 */
		void printPageList(void);

		/*! ***********************************************************************************************************
		 * @brief getParser				retrieves a pointer to the internal xml parser
		 * @return						the xml parser
		 * ************************************************************************************************************
		 */
		//XercesDOMParser* getParser(void);

		/*! ***********************************************************************************************************
		 * @brief getPageByName			retrieve the page with the specified name
		 * @param strPageName			name of the page to retrieve
		 * @return						a pointer to the requested page if present, null if the specified page is not
		 *								present in the container
		 * ************************************************************************************************************
		 */
		HttpPageBase* getPageByName(std::string strPageName);

		/*! ***********************************************************************************************************
		 * @brief processConnection		function process request contained in a WebServerConnection object
		 * @param pConnection			pointer to a WebServerConnection object filled with data received by the client
		 * @param pHttpPageContainer	container of pages, shall implement a method to search for the page contained
		 *								in the pConnection and provide a call to its handler()
		 * @return						true if the response has been correctly sent back to the client
		 * ************************************************************************************************************
		 */
		static bool processConnection(WebServerConnection* pConnection, void* pHttpPageContainer);

    private:

        std::list<HttpPageBase*> m_pageList;

};

#endif // HTTPPAGECONTAINER_H
