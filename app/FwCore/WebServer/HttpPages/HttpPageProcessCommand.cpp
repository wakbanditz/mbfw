/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    HttpPageProcessCommand.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the HttpPageProcessCommand class.
 @details

 ****************************************************************************
*/

#include "HttpPageProcessCommand.h"
#include "WebServerInclude.h"
#include "WebServerAndProtocolInclude.h"
#include "WebServerConnection.h"
#include "CommonInclude.h"
#include "IncomingCommand.h"
//#include "ProtocolValidator.h"
#include "MainExecutor.h"


HttpPageProcessCommand::HttpPageProcessCommand(std::string sName, std::string sSerial) : HttpPageBase(sName)
{
    m_strSerial.assign(sSerial);
}

HttpPageProcessCommand::~HttpPageProcessCommand()
{

}

bool HttpPageProcessCommand::handler(WebServerConnection &rConnection)
{
	/* ****************************************************************************************************************
	 * Build the response headers
	 * ****************************************************************************************************************
	 */
	rConnection.SetMimeType(WEB_HEADER_APPLICATION_XML_TYPE);
    rConnection.AddHeader(WEB_HEADER_SERIAL_NUMBER_NAME, m_strSerial /*WEB_HEADER_SERIAL_NUMBER_VALUE*/);

	/* ****************************************************************************************************************
	 * Retrieve the buffer of the HTTP/POST request
	 * ****************************************************************************************************************
	 */
	std::string sBody = rConnection.GetRequestBody();

	if ( sBody.length() < 1 )
	{
		log(LOG_WARNING, "HttpPageProcessCommand::handler: error empty xml buffer");
		rConnection.SendString("Empty body", HTTP_RET_CODE_ACCEPTED);
		return false;
	}

	/* ****************************************************************************************************************
	 * Print headers and body for debug
	 * ****************************************************************************************************************
	 */
	std::vector< std::pair<std::string, std::string> > pRequestHeaders = rConnection.GetRequestHeaders();
	std::vector<std::pair<std::string, std::string> >::iterator it;
	int cnt = 1;
	for ( it = pRequestHeaders.begin(); it != pRequestHeaders.end(); it++ )
	{
		string sName((*it).first);
		string sValue((*it).second);
		log(LOG_DEBUG_PARANOIC, "HttpPageProcessCommand::handler: header[%d]=[\"%s\"=\"%s\"",
			cnt, sName.c_str(), sValue.c_str());
		cnt++;
	}
	pRequestHeaders.clear();
	log(LOG_DEBUG, "HttpPageProcessCommand::handler: body=\"%s\"", sBody.c_str());

	/* ****************************************************************************************************************
	 * Parse message and try to build a valid IncomingCommand
	 * ****************************************************************************************************************
	 */

	IncomingCommand* pCmd = 0;
	bool bParserResult = MainExecutor::getInstance()->m_protocolValidator.parse(sBody, pCmd);
	if ( !bParserResult )
	{
		// Command malformed, report synchronously the error
		char sCommandNotRecognized[32];
		sprintf(sCommandNotRecognized, "ERROR Command not recognized\r\n");
		rConnection.SendString(sCommandNotRecognized, HTTP_RET_CODE_OK);
		return false;
	}

	/* ****************************************************************************************************************
	 * Valid message! Start management calling the execute() routine
	 * ****************************************************************************************************************
	 */

	bool bRetValue = false;
	if ( pCmd != NULL )
	{
		pCmd->setWebServerConnection(rConnection);
		if ( pCmd->execute() != true )
		{
			rConnection.SendInternalError();
		}
        SAFE_DELETE(pCmd);
		bRetValue = true;
	}
	else
	{
		rConnection.SendInternalError();
		log(LOG_ERR, "Critical memory error");
		bRetValue = false;
	}
	return bRetValue;
}
