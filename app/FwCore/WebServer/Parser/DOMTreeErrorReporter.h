/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    DOMTreeErrorReporter.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the DOMTreeErrorReporter class.
 @details

 ****************************************************************************
*/

#ifndef DOMTREEERRORREPORTER_H
#define DOMTREEERRORREPORTER_H

#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/sax/ErrorHandler.hpp>
#include "Loggable.h"

#if defined(XERCES_NEW_IOSTREAMS)
#include <iostream>
#else
#include <iostream.h>
#endif


XERCES_CPP_NAMESPACE_USE


class DOMTreeErrorReporter	:	public ErrorHandler,
								public Loggable
{

	public:

		/*! ***********************************************************************************************************
		 * @brief DOMTreeErrorReporter	void constructor, reset internal members
		 * ************************************************************************************************************
		 */
		DOMTreeErrorReporter();

		/*! ***********************************************************************************************************
		 * @brief ~DOMTreeErrorReporter	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~DOMTreeErrorReporter();

		/*! ***********************************************************************************************************
		 * @brief warning		displays a warning message making explicit the content of the specified exception
		 * @param toCatch		the exception descriptor
		 * ************************************************************************************************************
		 */
		void warning(const SAXParseException& toCatch);

		/*! ***********************************************************************************************************
		 * @brief error			displays an error message making explicit the content of the specified exception
		 * @param toCatch		the exception descriptor
		 * ************************************************************************************************************
		 */
		void error(const SAXParseException& toCatch);

		/*! ***********************************************************************************************************
		 * @brief fatalError	displays a fatal error message making explicit the content of the specified exception
		 * @param toCatch		the exception descriptor
		 * ************************************************************************************************************
		 */
		void fatalError(const SAXParseException& toCatch);

		/*! ***********************************************************************************************************
		 * @brief resetErrors	reset the internal error marker
		 * ************************************************************************************************************
		 */
		void resetErrors(void);

		/*! ***********************************************************************************************************
         * @brief getErrorsDetected	method used to check if an error has occurred
		 * @return				true if an error has occurred, i.e. one among warning(), error() or fatalError()
		 *						methods have been called at least once, false otherwise
		 * ************************************************************************************************************
		 */
		bool getErrorsDetected() const;

    private:

        bool m_bErrorsDetected;	// This is set if we get any errors, and is queryable via a getter method.
                            // Its used by the main code to suppress output if there are errors.

};

#endif
