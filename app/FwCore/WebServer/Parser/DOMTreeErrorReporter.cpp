/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    DOMTreeErrorReporter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the DOMTreeErrorReporter class.
 @details

 ****************************************************************************
*/

#include <xercesc/sax/SAXParseException.hpp>

#include "DOMTreeErrorReporter.h"

#if defined(XERCES_NEW_IOSTREAMS)
#include <iostream>
#else
#include <iostream.h>
#endif
#include <stdlib.h>
#include <string.h>

#include "StrX.h"

DOMTreeErrorReporter::DOMTreeErrorReporter() :
    m_bErrorsDetected(false)
{

}


DOMTreeErrorReporter::~DOMTreeErrorReporter()
{

}

void DOMTreeErrorReporter::warning(const SAXParseException& toCatch)
{
    m_bErrorsDetected = true;
	StrX sFile(toCatch.getSystemId());
	StrX sMsg(toCatch.getMessage());
	log(LOG_WARNING,
		"DOMError in file \"%s\", line %lu, column %lu, msg=<%s>",
		sFile.getString(),
		toCatch.getLineNumber(),
		toCatch.getColumnNumber(),
		sMsg.getString());
}

void DOMTreeErrorReporter::error(const SAXParseException& toCatch)
{
    m_bErrorsDetected = true;
	StrX sFile(toCatch.getSystemId());
	StrX sMsg(toCatch.getMessage());
	std::string strMsg(sMsg.getString());
	size_t len = strMsg.length();
	char s[1024] = "";
	snprintf(s, len-1, strMsg.c_str());
	log(LOG_ERR,
		"DOMError in file \"%s\", line %llu, column %llu, msg=\"%s\"",
		sFile.getString(),
		toCatch.getLineNumber(),
		toCatch.getColumnNumber(),
		s);
}

void DOMTreeErrorReporter::fatalError(const SAXParseException& toCatch)
{
	error(toCatch);
}

void DOMTreeErrorReporter::resetErrors(void)
{
    m_bErrorsDetected = false;
}

bool DOMTreeErrorReporter::getErrorsDetected() const
{
    return m_bErrorsDetected;
}
