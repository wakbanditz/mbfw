/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ProtocolValidator.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the ProtocolValidator class.
 @details

 ****************************************************************************
*/

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>

#include "ProtocolValidator.h"
#include "DOMTreeErrorReporter.h"

#include "CommonInclude.h"
#include "WebServerInclude.h"
#include "WebServerAndProtocolInclude.h"
#include "StrX.h"
#include "XStr.h"
#include "ParsingUtilities.h"
#include "SerializationUtilities.h"

using namespace xercesc_3_2;

/* ********************************************************************************************************************
 * ProtocolValidator class implementation
 * ********************************************************************************************************************
 */
ProtocolValidator::ProtocolValidator()
{
	m_pXercesDOMParser = 0;
}

ProtocolValidator::~ProtocolValidator()
{
	XMLPlatformUtils::Terminate();
	log(LOG_INFO, "ProtocolValidator::~ProtocolValidator: XMLPlatformUtils terminated");

	SAFE_DELETE(m_pDomErrorReporter);
	SAFE_DELETE(m_pXercesDOMParser);
}

bool ProtocolValidator::init(void)
{
	/*! ***************************************************************************************************************
	 * Initializes the xerces infrastructure
	 * ****************************************************************************************************************
	 */
	try
	{
        XMLPlatformUtils::Initialize();
	}
	catch( XMLException& e )
	{
		char* sMsg = XMLString::transcode(e.getMessage());
		log(LOG_ERR, "ProtocolValidator::init: XMLPlatformUtils error: \"%s\"", sMsg);
		XMLString::release(&sMsg);
		return false;
	}
	log(LOG_INFO, "ProtocolValidator::init: XMLPlatformUtils initialization done");

	/*! ***************************************************************************************************************
	 * Create the xerces xml DOM parser, then attach an error handler to it
	 * The parser will call back to methods of the ErrorHandler if it discovers errors while parsing the XML buffer
	 * ****************************************************************************************************************
	 */
	SAFE_DELETE(m_pXercesDOMParser);
	m_pXercesDOMParser = new XercesDOMParser();
	if ( m_pXercesDOMParser == 0 )
	{
		log(LOG_ERR, "ProtocolValidator::init: memory error allocating xerces DOMParser");
		return false;
	}

	// 	Do not report validation errors
	m_pXercesDOMParser->setValidationScheme(xercesc::XercesDOMParser::Val_Never);

	// If the validation scheme is set to Val_Always or Val_Auto
	// then the document must contain a grammar that supports the use of namespaces
	// In our case we do not perform Namespace processing
	m_pXercesDOMParser->setDoNamespaces(false);

	// Disable the parser's schema support
	m_pXercesDOMParser->setDoSchema(false);

	// Don't import multiple schemas with the same namespace
	m_pXercesDOMParser->setHandleMultipleImports(false);

	// Disable full schema constraint checking
	m_pXercesDOMParser->setValidationSchemaFullChecking(false);

	// Do not create EntityReference nodes in the DOM tree.
	// No EntityReference nodes will be created,
	// only the nodes corresponding to their fully expanded substitution text will be created.
	m_pXercesDOMParser->setCreateEntityReferenceNodes(false);

	m_pXercesDOMParser->setLoadExternalDTD(false);
	m_pXercesDOMParser->setDoXInclude(false);

	// Add the error handler, now is just a logger
	SAFE_DELETE(m_pDomErrorReporter);
	m_pDomErrorReporter = new DOMTreeErrorReporter();
	m_pDomErrorReporter->setLogger(getLogger());
	m_pXercesDOMParser->setErrorHandler(m_pDomErrorReporter);

	log(LOG_INFO, "ProtocolValidator::init: xerces DOMParser initialization done");

	return true;
}

bool ProtocolValidator::parse(const std::string& strXmlBuffer, IncomingCommand* &pCmd)
{
	pCmd = 0;
	if ( !m_pXercesDOMParser )
	{
		log(LOG_ERR, "ProtocolValidator::parse: DOMParser not initialized");
		return false;
	}

	bool bRetValue = false;
	try
	{
        m_pDomErrorReporter->resetErrors();

        /* ************************************************************************************************************
		 * Parse the xml buffer
		 * Every command contains a name and the "vminst" and "xsi" attributes, example:
		 *	<vminst:HELLO xmlns:vminst="http://biomerieux.com/vidas/instrument/protocol/v1/vminst"
		 *		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 *		Usage="SERVICE"/>
		 * ************************************************************************************************************
		 */

		// First of all a local copy of the buffer is built using a MemBufInputSource object
		// It must be specified not to adopt the buffer (4th param) because it is local -> therefore automatic
		MemBufInputSource xmlBuffer((const XMLByte*)strXmlBuffer.c_str(), strXmlBuffer.length(), "sCmdBody", false);

		// Reset the parser
		m_pXercesDOMParser->resetDocumentPool();
		m_pXercesDOMParser->resetCachedGrammarPool();
		m_pXercesDOMParser->reset();

		// Parse the buffer
		m_pXercesDOMParser->parse(xmlBuffer);

		// Get the entire DOM representation of the buffer
		// There is no need to free the following  pointer as it is owned by the parent parser object
		DOMDocument* xmlDoc = m_pXercesDOMParser->getDocument();
		if ( !xmlDoc )
		{
			throw ( std::runtime_error("corrupted XML buffer") );
		}
		// Get the top-level element: name is "vminst:COMMAND_NAME""
		DOMElement* pElementRoot = xmlDoc->getDocumentElement();
		if ( !pElementRoot )
		{
			throw ( std::runtime_error("XML root tag not found") );
		}

        // check if the xml is correctly formatted by reading the errors reported
        if(m_pDomErrorReporter->getErrorsDetected())
        {
            bRetValue = false;
        }
        else
        {
            /* ************************************************************************************************************
             *	Retrieve the command name and build the associated object
             * ************************************************************************************************************
             */
            StrX strTagCommandName(pElementRoot->getTagName());
            StrX strAttrProtocolUrlValue(pElementRoot->getAttribute(X(WEB_CMD_ATTR_PROTOCOL_URL_NAME)));
            StrX strAttrSchemaUrlValue(pElementRoot->getAttribute(X(WEB_CMD_ATTR_SCHEMA_URL_NAME)));
            StrX strAttrUsageValue(pElementRoot->getAttribute(X(WEB_CMD_ATTR_USAGE)));
            log(LOG_DEBUG,
                "ProtocolValidator::parse: elemRoot[cmd=\"%s\";protUrl=\"%s\";schemaUrl=\"%s\";usage=\"%s\"]",
                strTagCommandName.getString(),
                strAttrProtocolUrlValue.getString(),
                strAttrSchemaUrlValue.getString(),
                strAttrUsageValue.getString());
                bRetValue = buildIncomingCommandFromRootNode(pElementRoot, pCmd);
        }
	}
	catch ( xercesc::XMLException& e )
	{
		StrX sMessage(e.getMessage());
		log(LOG_WARNING, "ProtocolValidator::parse: error parsing command \"%s\"", sMessage.getString());
		bRetValue = false;
	}
	return bRetValue;
}

bool ProtocolValidator::parseXMLtoPressureSettings(const std::string& strXmlBuffer, PressureSettings* &pPressureSettings)
{
	bool bRetValue = false;
	pPressureSettings = 0;

	if ( !m_pXercesDOMParser )
	{
		log(LOG_ERR, "ProtocolValidator::parse: DOMParser not initialized");
		return bRetValue;
	}

	/* ************************************************************************************************************
	 * Parse the xml buffer
	 * Every command contains a name and the "vminst" and "xsi" attributes, example:
	 *	<vminst:HELLO xmlns:vminst="http://biomerieux.com/vidas/instrument/protocol/v1/vminst"
	 *		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	 *		Usage="SERVICE"/>
	 * ************************************************************************************************************
	 */

	// First of all a local copy of the buffer is built using a MemBufInputSource object
	// It must be specified not to adopt the buffer (4th param) because it is local -> therefore automatic
	MemBufInputSource xmlBuffer((const XMLByte*)strXmlBuffer.c_str(), strXmlBuffer.length(), "sCmdBody", false);

	// Reset the parser
	m_pXercesDOMParser->resetDocumentPool();
	m_pXercesDOMParser->resetCachedGrammarPool();
	m_pXercesDOMParser->reset();

	// Parse the buffer
	m_pXercesDOMParser->parse(xmlBuffer);

	// Get the entire DOM representation of the buffer
	// There is no need to free the following  pointer as it is owned by the parent parser object
	DOMDocument* xmlDoc = m_pXercesDOMParser->getDocument();
	if ( !xmlDoc )
	{
		log(LOG_ERR, "ProtocolValidator: corrupted XML buffer");
		return bRetValue;
	}
	// Get the top-level element: name is "vminst:COMMAND_NAME""
	DOMElement * pElementRoot = xmlDoc->getDocumentElement();
	if ( !pElementRoot )
	{
		log(LOG_ERR, "ProtocolValidator: XML root tag not found");
		return bRetValue;
	}

	/* ************************************************************************************************************
	 *	Retrieve the command name and build the associated object
	 * ************************************************************************************************************
	 */
	StrX strTagCommandName(pElementRoot->getTagName());
	log(LOG_DEBUG, "ProtocolValidator::parse: elemRoot[cmd=\"%s\"]", strTagCommandName.getString());

	if(strstr(strTagCommandName.getString(), XML_TAG_PROTO_PRESSURE_SETTINGS) != 0)
	{
		bRetValue = buildPressureSettingsFromRootNode(pElementRoot, pPressureSettings);
	}

	return bRetValue;
}

bool ProtocolValidator::parseXMLtoCameraSettings(const vector<pair<string, string>>& vXmlBuffer, CameraSettings* pCameraSettings)
{
	if ( vXmlBuffer.empty() )			return false;
	if ( pCameraSettings == nullptr )	return false;

	bool bRetValue = false;
	if ( ! m_pXercesDOMParser )
	{
		log(LOG_ERR, "ProtocolValidator::parse: DOMParser not initialized");
		return bRetValue;
	}

	// parse all the settings
	for ( auto a : vXmlBuffer )
	{
		int liId = stoi(a.first);
		string strXmlBuffer = a.second;
		// First of all a local copy of the buffer is built using a MemBufInputSource object
		// It must be specified not to adopt the buffer (4th param) because it is local -> therefore automatic
		MemBufInputSource xmlBuffer((const XMLByte*)strXmlBuffer.c_str(), strXmlBuffer.length(), "sCmdBody", false);

		// Reset the parser
		m_pXercesDOMParser->resetDocumentPool();
		m_pXercesDOMParser->resetCachedGrammarPool();
		m_pXercesDOMParser->reset();

		// Parse the buffer
		m_pXercesDOMParser->parse(xmlBuffer);

		// Get the entire DOM representation of the buffer
		// There is no need to free the following  pointer as it is owned by the parent parser object
		DOMDocument* xmlDoc = m_pXercesDOMParser->getDocument();
		if ( ! xmlDoc )
		{
			log(LOG_ERR, "ProtocolValidator: corrupted XML buffer");
			return bRetValue;
		}
		// Get the top-level element: name is "vminst:COMMAND_NAME""
		DOMElement * pElementRoot = xmlDoc->getDocumentElement();
		if ( ! pElementRoot )
		{
			log(LOG_ERR, "ProtocolValidator: XML root tag not found");
			return bRetValue;
		}

		/* ************************************************************************************************************
		 *	Retrieve the command name and build the associated object
		 * ************************************************************************************************************
		 */
		StrX strTagCommandName(pElementRoot->getTagName());
		log(LOG_DEBUG, "ProtocolValidator::parse: elemRoot[cmd=\"%s\"]", strTagCommandName.getString());

		if ( strstr(strTagCommandName.getString(), XML_TAG_PROTO_CAMERA_SETTINGS) != 0 )
		{
			bRetValue = buildCameraSettingsFromRootNode(pElementRoot, liId,  pCameraSettings);
		}
	}

	return bRetValue;
}

bool ProtocolValidator::serialize(OutgoingCommand* pCmd, std::string& strOutString)
{
	bool bRetValue = true;
	char sCmdName[256];

	snprintf(sCmdName, 255, "%s%s", WEB_CMD_VIDAS_MODULAR_NS, pCmd->getName().c_str());

    log(LOG_DEBUG_IPER_PARANOIC, "ProtocolValidator::serialize: start serializing cmd=\"%s\"", sCmdName);

	strOutString.clear();
	int liError = 0;

	DOMImplementation* pDOMImpl = DOMImplementationRegistry::getDOMImplementation(X(WEB_XML_IMPLEMENTATION_VERSION));
	if ( pDOMImpl != NULL )
	{
		try
		{
			DOMDocument* pOutDoc = pDOMImpl->createDocument(
									   X(WEB_CMD_ATTR_PROTOCOL_URL_VAL),	// root element namespace URI.
									   X(sCmdName),							// root element name
									   0);									// document type object (DTD).
//			DOMElement* pRootElem = pOutDoc->getDocumentElement();
//			//<vminst:hello
//			//	xmlns:vminst="http://biomerieux.com/vidas/instrument/protocol/v1/vminst"
//			//	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />
//			pRootElem->setAttribute(X(WEB_CMD_ATTR_SCHEMA_URL_NAME), X(WEB_CMD_ATTR_SCHEMA_URL_VAL));
			bool bSerializeSuccess = buildDOMfromOutgoingCommand(pCmd, pOutDoc);
			if ( bSerializeSuccess )
			{
				liError = XML_SERIALIZE_SUCCESS;
				pOutDoc->setXmlStandalone(true);
				strOutString.assign(dom2String(pOutDoc));
				log(LOG_DEBUG_PARANOIC, "OutgoingCommand::serialize: SUCCESS body=\"%s\"", strOutString.c_str());
			}
			else
			{
				log(LOG_ERR, "OutgoingCommand::serialize: error");
			}
			pOutDoc->release();

		}
		catch (const OutOfMemoryException&)
		{
			log(LOG_ERR, "OutgoingCommand::serialize: OutOfMemoryException");
			liError = XML_SERIALIZE_OUT_OF_MEMORY;
		}
		catch (const DOMException& e)
		{
			log(LOG_ERR, "OutgoingCommand::serialize: DOMException code %h", e.code);
			liError = XML_SERIALIZE_DOM_EXCEPTION;
		}
		catch (...)
		{
			log(LOG_ERR, "OutgoingCommand::serialize: unhandled exception");
			liError = XML_SERIALIZE_UNHANDLED_EXCEPTION;
		}
	}  // (pDOMImpl != NULL)
	else
	{
		log(LOG_ERR, "OutgoingCommand::serialize: implementation not supported");
		liError = XML_SERIALIZE_IMPL_NOT_SUPPORTED;
	}

	/* ************************************************************************************************************
	 * Build a HttpConnection and post the answer to the server
	 * ************************************************************************************************************
	 */
	if ( liError == XML_SERIALIZE_SUCCESS )
	{
		bRetValue = true;
	}
	else
	{
		strOutString.assign("Internal Error");
		bRetValue = false;
	}

	return bRetValue;
}

bool ProtocolValidator::serializeDataToXML(uint8_t section, uint8_t strip, std::string strDataType, std::string& strOutString)
{
	strOutString.clear();

	if(strDataType.empty()) return false;

	bool bRetValue = true;
	char sCmdName[256], sCmdUrl[256];
	int liError = 0;

	if(strDataType.compare(WEB_CMD_DATA_TEMPERATURE) == 0)
	{
		snprintf(sCmdName, 255, "%s:%s", WEB_CMD_MODULAR_TEMPERATURE, WEB_CMD_DATA_TEMPERATURE);
		snprintf(sCmdUrl, 255, "%s", WEB_CMD_ATTR_TEMPERATURE_URL_VAL);
	}
	else if(strDataType.compare(WEB_CMD_DATA_TREATED_PRESSURE) == 0)
	{
		snprintf(sCmdName, 255, "%s:%s", WEB_CMD_MODULAR_PRESSURE, WEB_CMD_DATA_PRESSURE);
		snprintf(sCmdUrl, 255, "%s", WEB_CMD_ATTR_PRESSURE_URL_VAL);
	}
	else if(strDataType.compare(WEB_CMD_DATA_ELABORATED_PRESSURE) == 0)
	{
		snprintf(sCmdName, 255, "%s:%s", WEB_CMD_MODULAR_PRESSURE, WEB_CMD_DATA_PRESSURE);
		snprintf(sCmdUrl, 255, "%s", WEB_CMD_ATTR_PRESSURE_URL_VAL);
	}
	else if(strDataType.compare(WEB_CMD_DATA_OPTICS) == 0)
	{
		snprintf(sCmdName, 255, "%s:%s", WEB_CMD_MODULAR_OPTICS, WEB_CMD_DATA_OPTICS);
		snprintf(sCmdUrl, 255, "%s", WEB_CMD_ATTR_OPTICS_URL_VAL);
	}

    log(LOG_DEBUG_IPER_PARANOIC, "ProtocolValidator::serialize: start serializing cmd=\"%s\"", sCmdName);

	DOMImplementation* pDOMImpl = DOMImplementationRegistry::getDOMImplementation(X(WEB_XML_IMPLEMENTATION_VERSION));
	if ( pDOMImpl != NULL )
	{
		try
		{
			DOMDocument* pOutDoc = pDOMImpl->createDocument(
									   X(sCmdUrl),							// root element namespace URI.
									   X(sCmdName),							// root element name
									   0);									// document type object (DTD).
//			DOMElement* pRootElem = pOutDoc->getDocumentElement();
//			//<vminst:hello
//			//	xmlns:vminst="http://biomerieux.com/vidas/instrument/protocol/v1/vminst"
//			//	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />
//			pRootElem->setAttribute(X(WEB_CMD_ATTR_SCHEMA_URL_NAME), X(WEB_CMD_ATTR_SCHEMA_URL_VAL));
			bool bSerializeSuccess = buildDOMfromData(section, strip, strDataType, pOutDoc);
			if ( bSerializeSuccess )
			{
				liError = XML_SERIALIZE_SUCCESS;
				pOutDoc->setXmlStandalone(true);
				strOutString.assign(dom2String(pOutDoc));
				log(LOG_DEBUG_PARANOIC, "OutgoingCommand::serialize: SUCCESS body=\"%s\"", strOutString.c_str());
			}
			else
			{
                log(LOG_ERR, "OutgoingCommand::serialize %s -> error", strDataType.c_str());
                liError = XML_SERIALIZE_BAD_FORMAT;
			}
			pOutDoc->release();

		}
		catch (const OutOfMemoryException&)
		{
			log(LOG_ERR, "OutgoingCommand::serialize: OutOfMemoryException");
			liError = XML_SERIALIZE_OUT_OF_MEMORY;
		}
		catch (const DOMException& e)
		{
			log(LOG_ERR, "OutgoingCommand::serialize: DOMException code %h", e.code);
			liError = XML_SERIALIZE_DOM_EXCEPTION;
		}
		catch (...)
		{
			log(LOG_ERR, "OutgoingCommand::serialize: unhandled exception");
			liError = XML_SERIALIZE_UNHANDLED_EXCEPTION;
		}
	}  // (pDOMImpl != NULL)
	else
	{
		log(LOG_ERR, "OutgoingCommand::serialize: implementation not supported");
		liError = XML_SERIALIZE_IMPL_NOT_SUPPORTED;
	}

	/* ************************************************************************************************************
	 * Build a HttpConnection and post the answer to the server
	 * ************************************************************************************************************
	 */
	if ( liError == XML_SERIALIZE_SUCCESS )
	{
		bRetValue = true;
	}
	else
	{
		strOutString.clear();
		bRetValue = false;
	}

	return bRetValue;
}
