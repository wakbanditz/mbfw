/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ProtocolValidator.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the ProtocolValidator class.
 @details

 ****************************************************************************
*/
#ifndef PROTOCOLVALIDATOR_H

#define PROTOCOLVALIDATOR_H

#include "Loggable.h"
#include <string>
#include <vector>

class IncomingCommand;
class OutgoingCommand;

class DOMTreeErrorReporter;
class PressureSettings;
class CameraSettings;

namespace xercesc_3_2
{
	class XercesDOMParser;
}

class ProtocolValidator : public Loggable
{

	public:

		/*! ***********************************************************************************************************
		 * @brief ProtocolValidator		void constructor
		 * ************************************************************************************************************
		 */
		ProtocolValidator();

		/*! ***********************************************************************************************************
		 * @brief ~ProtocolValidator	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~ProtocolValidator();

		/*! ***********************************************************************************************************
		 * @brief init					initialize the xerces infrastructure, create the xerces xml DOM parser,
		 *								then attach an error handler to it. The parser will call back to methods of
		 *								the ErrorHandler if it discovers errors while parsing the XML buffer.
		 *								This method must be called before any other class method and just once
		 * @return						true upon succesfull initialization, false otherwise
		 * ************************************************************************************************************
		 */
		bool init(void);

		/*! ***********************************************************************************************************
		 * @brief parse					build an IncomingCommand by parsing an XML buffer string. The XML must be well
		 *								formatted in order to be correctly interpreted. Upon succesfull parsing the
		 *								IncomingCommand object associated to the XML is allocated, it is up to the
		 *								user to deallocate it.
		 * @param strXmlBuffer			the input XML buffer string from wich the IncomingCommand shall be built
         * @param pCmd [out]			pointer to an IncomingCommand allocated upon proper parsing, if the parsing
		 *								fails this pointer is set to 0
		 * @return						true upon successful parsing (i.e. the strXmlBuffer is a valid command), false
		 *								otherwise
		 * ************************************************************************************************************
		 */
		bool parse(const std::string& strXmlBuffer, IncomingCommand*& pCmd);

		/*! ***********************************************************************************************************
		 * @brief serialize				serialize an Outgoing command into an XML formatted string
		 *								(to be eventually sent to a client)
		 * @param pCmd					OutgoingCommand pointer that must be serialized into an XML buffer string
         * @param strOutString [out]	the serialized string form of the command
		 * @return						true upon succesful serialization, false otherwise
		 *								(in this case strOutString is empty)
		 * ************************************************************************************************************
		 */
		bool serialize(OutgoingCommand* pCmd, std::string& strOutString);

		/*! ***********************************************************************************************************
		 * @brief parseXMLtoPressureSettings		parse an XML buffer string to a PressureSettings object
		 *								The XML must be well
		 *								formatted in order to be correctly interpreted. Upon succesfull parsing the
		 *								PressureSettings object is created and set.
		 * @param strXmlBuffer			the input XML buffer string
         * @param pPressureSettings	[out]	pointer to a valid PressureSetting object
		 * @return						true upon successful parsing (i.e. the strXmlBuffer is a valid command), false
		 *								otherwise
		 * ************************************************************************************************************
		 */
		bool parseXMLtoPressureSettings(const std::string& strXmlBuffer, PressureSettings*& pPressureSettings);

		/*! ***********************************************************************************************************
		 * @brief parseXMLtoCameraSettings	parse an XML buffer string to a CameraSettings struct
		 *								The XML must be well formatted in order to be correctly interpreted. Upon succesfull parsing the
		 *								PressureSettings object is created and set.
		 * @param strXmlBuffer			the input XML buffer string
		 * @param pCameraSettings [out]	pointer to a valid CameraSettings struct
		 * @return						true upon successful parsing (i.e. the strXmlBuffer is a valid command), false
		 *								otherwise
		 * ************************************************************************************************************
		 */
		bool parseXMLtoCameraSettings(const std::vector<std::pair<std::string, std::string>>&vXmlBuffer, CameraSettings*pCameraSettings);

		/*! ***********************************************************************************************************
		 * @brief serializeDataToXml	serialize a Data Group for the RUN reply into an XML formatted string
		 *								(to be eventually sent to a client)
		 * @param section				the involved section
		 * @param strip					the involved strip
		 * @param strDataType			the data type string identifier
         * @param strOutString [out]	the serialized string form of the command
		 * @return						true upon succesful serialization, false otherwise
		 *								(in this case strOutString is empty)
		 * ************************************************************************************************************
		 */
		bool serializeDataToXML(uint8_t section, uint8_t strip, std::string strDataType, std::string& strOutString);

    private:

        DOMTreeErrorReporter* m_pDomErrorReporter;
        xercesc_3_2::XercesDOMParser* m_pXercesDOMParser;

};

#endif // PROTOCOLVALIDATOR_H
