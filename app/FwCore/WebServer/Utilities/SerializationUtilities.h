/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SerializationUtilities.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SerializationUtilities class.
 @details

 ****************************************************************************
*/

#ifndef OUTGOINGCOMMANDUTILITIES_H
#define OUTGOINGCOMMANDUTILITIES_H

#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMDocument.hpp>

#include "OutCmdLogin.h"
#include "OutCmdLogout.h"
#include "OutCmdGetSerial.h"
#include "OutCmdGetVersions.h"
#include "OutCmdGetSensor.h"
#include "OutCmdVidasEp.h"
#include "OutCmdSetLed.h"
#include "OutCmdReadSec.h"
#include "OutCmdSecRoute.h"
#include "OutCmdMove.h"
#include "OutCmdMonitorPressure.h"
#include "OutCmdGetCalibration.h"
#include "OutCmdGetParameter.h"
#include "OutCmdSetParameter.h"
#include "OutCmdSaveCalibration.h"
#include "OutCmdSetMotorActivation.h"
#include "OutCmdInit.h"
#include "OutCmdCaptureImage.h"
#include "OutCmdRunwl.h"
#include "OutCmdCancelSection.h"
#include "OutCmdGetMotorActivation.h"
#include "OutCmdTrayRead.h"
#include "OutCmdGetPressureOffsets.h"
#include "OutCmdGetPressureSettings.h"
#include "OutCmdSetMaintenanceMode.h"
#include "OutCmdSSCalibrate.h"
#include "OutCmdSHCalibrate.h"
#include "OutCmdGetSSReference.h"
#include "OutCmdSetSerial.h"
#include "OutCmdSetTime.h"
#include "OutCmdOPTCalibrate.h"
#include "OutCmdOPTCheck.h"
#include "OutCmdAirCalibrate.h"
#include "OutCmdShutdown.h"
#include "OutCmdDisable.h"
#include "OutCmdSleep.h"
#include "OutCmdAutoCalibrate.h"
#include "OutCmdSetGlobalSettings.h"
#include "OutCmdGetGlobalSettings.h"
#include "OutCmdUnknown.h"
#include "OutCmdSignalRead.h"
#include "OutCmdSHForceLed.h"
#include "OutCmdGetCameraRoi.h"
#include "OutCmdGetCounter.h"
#include "OutCmdSetCounter.h"
#include "OutCmdGetFanSettings.h"
#include "OutCmdSetFanSettings.h"
#include "OutCmdSaveCameraRoi.h"
#include "OutCmdFwUpdate.h"
#include "OutCmdPump.h"

using namespace xercesc_3_2;

bool buildDOMfromOutgoingCommand(OutgoingCommand* pCmd, DOMDocument* &pOutDoc);

bool fillOutCmdLogin(OutCmdLogin* &pOutCmdLogin, DOMDocument*& pOutDoc);
bool fillOutCmdLogout(OutCmdLogout* &pOutCmdLogout, DOMDocument*& pOutDoc);
bool fillOutCmdGetSerial(OutCmdGetSerial* &pOutCmdGetSerial, DOMDocument*& pOutDoc);
bool fillOutCmdGetVersions(OutCmdGetVersions* &pOutCmdGetVersions, DOMDocument*& pOutDoc);
bool fillOutCmdGetSensor(OutCmdGetSensor* &pOutCmdGetSensor, DOMDocument*& pOutDoc);
bool fillOutCmdVidasEp(OutCmdVidasEp* &pOutCmdVidasEp, DOMDocument*& pOutDoc);
bool fillOutCmdSetLed(OutCmdSetLed* &pOutCmdSetLed, DOMDocument*& pOutDoc);
bool fillOutCmdReadSec(OutCmdReadSec*& pOutCmdReadSec, DOMDocument*& pOutDoc);
bool fillOutCmdSecRoute(OutCmdSecRoute*& pOutCmdSecRoute, DOMDocument*& pOutDoc);
bool fillOutCmdMove(OutCmdMove*& pOutCmdMove, DOMDocument*& pOutDoc);
bool fillOutCmdMonitorPressure(OutCmdMonitorPressure*& pOutCmdMonitorPressure, DOMDocument*& pOutDoc);
bool fillOutCmdGetPosition(OutCmdMove*& pOutCmdGetPosition, DOMDocument*& pOutDoc);
bool fillOutCmdGetCalibration(OutCmdGetCalibration*& pOutCmdGetCalibration, DOMDocument*& pOutDoc);
bool fillOutCmdGetParameter(OutCmdGetParameter*& pOutCmdGetParameter, DOMDocument*& pOutDoc);
bool fillOutCmdSetParameter(OutCmdSetParameter*& pOutCmdSetParameter, DOMDocument*& pOutDoc);
bool fillOutCmdSaveCalibration(OutCmdSaveCalibration*& pOutCmdSaveCalibration, DOMDocument*& pOutDoc);
bool fillOutCmdSetMotorActivation(OutCmdSetMotorActivation*& pOutCmdSetMotorActivation, DOMDocument*& pOutDoc);
bool fillOutCmdInit(OutCmdInit*& pOutCmdInit, DOMDocument*& pOutDoc);
bool fillOutCmdCaptureImage(OutCmdCaptureImage*& pOutCmdCaptureImage, DOMDocument*& pOutDoc);
bool fillOutCmdRunwl(OutCmdRunwl*& pOutCmdRunwl, DOMDocument*& pOutDoc);
bool fillOutCmdCancelSection(OutCmdCancelSection*& pOutCmdCancelSection, DOMDocument*& pOutDoc);
bool fillOutCmdGetMotorActivation(OutCmdGetMotorActivation*& pOutCmdGetMotorActivation, DOMDocument*& pOutDoc);
bool fillOutCmdTrayRead(OutCmdTrayRead*& pOutCmdTrayRead, DOMDocument*& pOutDoc);
bool fillOutCmdGetPressureOffsets(OutCmdGetPressureOffsets*& pOutCmdGetPressureOffsets, DOMDocument*& pOutDoc);
bool fillOutCmdGetPressureSettings(OutCmdGetPressureSettings*& pOutCmdGetPressureSettings, DOMDocument*& pOutDoc);
bool fillOutCmdSetMaintenanceMode(OutCmdSetMaintenanceMode*& pOutCmdSetMaintenanceMode, DOMDocument*& pOutDoc);
bool fillOutCmdSSCalibrate(OutCmdSSCalibrate*& pOutCmdSSCalibrate, DOMDocument*& pOutDoc);
bool fillOutCmdSHCalibrate(OutCmdSHCalibrate*& pOutCmdSHCalibrate, DOMDocument*& pOutDoc);
bool fillOutCmdGetSSReference(OutCmdGetSSReference*& pOutCmdGetSSReference, DOMDocument*& pOutDoc);
bool fillOutCmdSetSerial(OutCmdSetSerial* &pOutCmdSetSerial, DOMDocument*& pOutDoc);
bool fillOutCmdSetTime(OutCmdSetTime* &pOutCmdSetTime, DOMDocument*& pOutDoc);
bool fillOutCmdOptCalibrate(OutCmdOPTCalibrate*& pOutCmdOPTCalibrate, DOMDocument*& pOutDoc);
bool fillOutCmdOptCheck(OutCmdOPTCheck*& pOutCmdOPTCheck, DOMDocument*& pOutDoc);
bool fillOutCmdAirCalibrate(OutCmdAirCalibrate*& pOutCmdAirCalibrate, DOMDocument*& pOutDoc);
bool fillOutCmdShutdown(OutCmdShutdown* &pOutCmdShutdown, DOMDocument*& pOutDoc);
bool fillOutCmdDisable(OutCmdDisable* &pOutCmdDisable, DOMDocument*& pOutDoc);
bool fillOutCmdSleep(OutCmdSleep* &pOutCmdSleep, DOMDocument*& pOutDoc);
bool fillOutCmdAutoCalibrate(OutCmdAutoCalibrate* &pOutCmdAutoCalibrate, DOMDocument*& pOutDoc);
bool fillOutCmdSetGlobalSettings(OutCmdSetGlobalSettings* &pOutCmdGlobalSettings, DOMDocument*& pOutDoc);
bool fillOutCmdGetGlobalSettings(OutCmdGetGlobalSettings* &pOutCmdGetGlobalSettings, DOMDocument*& pOutDoc);
bool fillOutCmdOutCmdSignalRead(OutCmdSignalRead* &pOutCmdSignalRead, DOMDocument*& pOutDoc);
bool fillOutCmdOutCmdSHForceLed(OutCmdSHForceLed* &pOutCmdSHForceLed, DOMDocument*& pOutDoc);
bool fillOutCmdGetCameraRoi(OutCmdGetCameraRoi* &pOutCmdGetCameraRoi, DOMDocument*& pOutDoc);
bool fillOutCmdOutGetCounter(OutCmdGetCounter* &pOutCmdGetCounter, DOMDocument*& pOutDoc);
bool fillOutCmdOutSetCounter(OutCmdSetCounter* &pOutCmdSetCounter, DOMDocument*& pOutDoc);
bool fillOutCmdOutGetFanSettings(OutCmdGetFanSettings* &pOutCmdGetFanSettings, DOMDocument*& pOutDoc);
bool fillOutCmdOutSetFanSettings(OutCmdSetFanSettings* &pOutCmdSetFanSettings, DOMDocument*& pOutDoc);
bool fillOutCmdSaveCameraRoi(OutCmdSaveCameraRoi* &pOutCmdSaveCameraRoi, DOMDocument*& pOutDoc);
bool fillOutCmdFwUpdate(OutCmdFwUpdate* &pOutCmdFwUpdate, DOMDocument*& pOutDoc);
bool fillOutCmdPump(OutCmdPump* &pOutCmdPump, DOMDocument*& pOutDoc);

bool fillOutCmdUnknown(OutCmdUnknown* &pOutCmdUnknown, DOMDocument*& pOutDoc);

bool buildDOMfromData(uint8_t section, uint8_t strip, std::string strDataType, DOMDocument*& pOutDoc);

std::string dom2String(DOMDocument* pDocument, DOMImplementation *pDOMImplementation = 0);

#endif // OUTGOINGCOMMANDUTILITIES_H
