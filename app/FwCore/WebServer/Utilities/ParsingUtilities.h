/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ParsingUtilities.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the ParsingUtilities class.
 @details

 ****************************************************************************
*/

#ifndef PARSINGUTILITIES_H
#define PARSINGUTILITIES_H

#include <xercesc/dom/DOMElement.hpp>

#include "InCmdHello.h"
#include "InCmdLogin.h"
#include "InCmdLogout.h"
#include "InCmdGetSerial.h"
#include "InCmdGetVersions.h"
#include "InCmdGetSensor.h"
#include "InCmdVidasEp.h"
#include "InCmdSetLed.h"
#include "InCmdReadSec.h"
#include "InCmdSecRoute.h"
#include "InCmdMove.h"
#include "InCmdMonitorPressure.h"
#include "InCmdCaptureImage.h"
#include "InCmdGetPosition.h"
#include "InCmdGetCalibration.h"
#include "InCmdGetParameter.h"
#include "InCmdSetParameter.h"
#include "InCmdSaveCalibration.h"
#include "InCmdSetMotorActivation.h"
#include "InCmdGetMotorActivation.h"
#include "InCmdInit.h"
#include "InCmdRunwl.h"
#include "InCmdCancelSection.h"
#include "InCmdTrayRead.h"
#include "InCmdGetPressureOffsets.h"
#include "InCmdGetPressureSettings.h"
#include "InCmdSetMaintenanceMode.h"
#include "InCmdSSCalibrate.h"
#include "InCmdSHCalibrate.h"
#include "InCmdGetSSReference.h"
#include "InCmdSetSerial.h"
#include "InCmdSetTime.h"
#include "InCmdOPTCalibrate.h"
#include "InCmdAirCalibrate.h"
#include "InCmdShutdown.h"
#include "InCmdDisable.h"
#include "InCmdSleep.h"
#include "InCmdAutoCalibrate.h"
#include "InCmdSetGlobalSettings.h"
#include "InCmdGetGlobalSettings.h"
#include "InCmdUnknown.h"
#include "InCmdSignalRead.h"
#include "InCmdSHForceLed.h"
#include "InCmdGetCameraRoi.h"
#include "InCmdGetCounter.h"
#include "InCmdSetCounter.h"
#include "InCmdGetFanSettings.h"
#include "InCmdSetFanSettings.h"
#include "InCmdSaveCameraRoi.h"
#include "InCmdFwUpdate.h"
#include "InCmdPump.h"
#include "InCmdOPTCheck.h"

#include "PressureSettings.h"
#include "CameraSettings.h"

using namespace xercesc_3_2;

bool buildIncomingCommandFromRootNode(DOMElement* pXmlRootNode, IncomingCommand*& pIncomingCommand);
bool fillInCmdLogin(DOMElement* pXmlRootNode, InCmdLogin*& pInCmdLogin);
bool fillInCmdGetSensor(DOMElement* pXmlRootNode, InCmdGetSensor*& pInCmdGetSensor);
bool fillInCmdSetLed(DOMElement* pXmlRootNode, InCmdSetLed*& pInCmdSetLed);
bool fillInCmdReadSec(DOMElement* pXmlRootNode, InCmdReadSec*& pInCmdReadSec);
bool fillInCmdSecRoute(DOMElement* pXmlRootNode, InCmdSecRoute*& pInCmdSecRoute);
bool fillInCmdMove(DOMElement* pXmlRootNode, InCmdMove*& pInCmdMove);
bool fillInCmdMonitorPressure(DOMElement* pXmlRootNode, InCmdMonitorPressure*& pInCmdMonitorPressure);
bool fillInCmdGetPosition(DOMElement* pXmlRootNode, InCmdGetPosition*& pInCmdGetPosition);
bool fillInCmdGetCalibration(DOMElement* pXmlRootNode, InCmdGetCalibration*& pInCmdGetCalibration);
bool fillInCmdGetParameter(DOMElement* pXmlRootNode, InCmdGetParameter*& pInCmdGetParameter);
bool fillInCmdSetParameter(DOMElement* pXmlRootNode, InCmdSetParameter*& pInCmdSetParameter);
bool fillInCmdSaveCalibration(DOMElement* pXmlRootNode, InCmdSaveCalibration*& pInCmdSaveCalibration);
bool fillInCmdSetMotorActivation(DOMElement* pXmlRootNode, InCmdSetMotorActivation*& pInCmdSetMotorActivation);
bool fillInCmdInit(DOMElement* pXmlRootNode, InCmdInit*& pInCmdInit);
bool fillInCmdCaptureImage(DOMElement* pXmlRootNode, InCmdCaptureImage*& pInCmdCaptureImage);
bool fillInCmdRunwl(DOMElement* pXmlRootNode, InCmdRunwl*& pInCmdRunwl);
bool fillInCmdCancelSection(DOMElement* pXmlRootNode, InCmdCancelSection*& pInCmdCancelSection);
bool fillInCmdGetMotorActivation(DOMElement* pXmlRootNode, InCmdGetMotorActivation*& pInCmdGetMotorActivation);
bool fillInCmdTrayRead(DOMElement* pXmlRootNode, InCmdTrayRead*& pInCmdTrayRead);
bool fillInCmdGetPressureOffsets(DOMElement* pXmlRootNode, InCmdGetPressureOffsets*& pInCmdGetPressureOffsets);
bool fillInCmdGetPressureSettings(DOMElement* pXmlRootNode, InCmdGetPressureSettings*& pInCmdGetPressureSettings);
bool fillInCmdSetMaintenanceMode(DOMElement* pXmlRootNode, InCmdSetMaintenanceMode*& pInCmdSetMaintenanceMode);
bool fillInCmdSHCalibrate(DOMElement* pXmlRootNode, InCmdSHCalibrate*& pInCmdSHCalibrate);
bool fillInCmdSetSerial(DOMElement* pXmlRootNode, InCmdSetSerial*& pInCmdSetSerial);
bool fillInCmdSetTime(DOMElement* pXmlRootNode, InCmdSetTime*& pInCmdSetTime);
bool fillInCmdOPTCalibrate(DOMElement* pXmlRootNode, InCmdOPTCalibrate*& pInCmdOPTCalibrate);
bool fillInCmdOPTCheck(DOMElement* pXmlRootNode, InCmdOPTCheck*& pInCmdOPTCheck);
bool fillInCmdAirCalibrate(DOMElement* pXmlRootNode, InCmdAirCalibrate*& pInCmdAirCalibrate);
bool fillInCmdShutdown(DOMElement* pXmlRootNode, InCmdShutdown*& pInCmdShutdown);
bool fillInCmdDisable(DOMElement* pXmlRootNode, InCmdDisable*& pInCmdDisable);
bool fillInCmdSleep(DOMElement* pXmlRootNode, InCmdSleep*& pInCmdSleep);
bool fillInCmdSetGlobalSettings(DOMElement* pXmlRootNode, InCmdSetGlobalSettings*& pInCmdGlobalSettings);
bool fillInCmdUnknown(DOMElement* pXmlRootNode, InCmdUnknown*& pInCmdUnknown, std::string strCmd);
bool fillInCmdSignalRead(DOMElement* pXmlRootNode, InCmdSignalRead*& pInCmdSignalRead);
bool fillInCmdSHForceLed(DOMElement* pXmlRootNode, InCmdSHForceLed*& pInCmdSHForceLed);
bool fillInCmdGetCameraRoi(DOMElement* pXmlRootNode, InCmdGetCameraRoi*& pInCmdGetCameraRoi);
bool fillInCmdGetCounter(DOMElement* pXmlRootNode, InCmdGetCounter*& pInCmdGetCounter);
bool fillInCmdSetCounter(DOMElement* pXmlRootNode, InCmdSetCounter*& pInCmdSetCounter);
bool fillInCmdSetFanSettings(DOMElement* pXmlRootNode, InCmdSetFanSettings*& pInCmdSetFanSettings);
bool fillInCmdSaveCameraRoi(DOMElement* pXmlRootNode, InCmdSaveCameraRoi*& pInCmdSaveCameraRoi);
bool fillInCmdFwUpdate(DOMElement* pXmlRootNode, InCmdFwUpdate*& pInCmdFwUpdate);
bool fillInCmdPump(DOMElement* pXmlRootNode, InCmdPump*& pInCmdPump);

bool buildPressureSettingsFromRootNode(DOMElement* pXmlRootNode, PressureSettings*& pPressureSettings);
bool buildCameraSettingsFromRootNode(DOMElement* pXmlRootNode, int liSettingId, CameraSettings*pCameraSettings);

#endif // PARSINGUTILITIES_H
