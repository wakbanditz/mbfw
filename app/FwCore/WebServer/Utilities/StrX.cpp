/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    StrX.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the StrX class.
 @details

 ****************************************************************************
*/

#include "StrX.h"

StrX::StrX(const XMLCh* const toTranscode)
{
	// Call the private transcoding method
	m_sArrayOfChars = XMLString::transcode(toTranscode);
}

StrX::~StrX()
{
	XMLString::release(&m_sArrayOfChars);
}

const char* StrX::getString() const
{
	return m_sArrayOfChars;
}

XERCES_STD_QUALIFIER ostream& operator<<(XERCES_STD_QUALIFIER ostream& target, const StrX& toDump)
{
	target << toDump.getString();
	return target;
}
