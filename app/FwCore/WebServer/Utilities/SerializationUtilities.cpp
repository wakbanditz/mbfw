/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SerializationUtilities.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SerializationUtilities class.
 @details

 ****************************************************************************
*/

#include "SerializationUtilities.h"
#include "CommonInclude.h"
#include "SectionInclude.h"
#include "WebServerAndProtocolInclude.h"
#include "MainExecutor.h"
#include "ErrorUtilities.h"

#include "XStr.h"
#include "StrX.h"

#include "FailureCause.h"
#include "GetSerialPayload.h"
#include "GetSensorPayload.h"
#include "GetVersionsPayload.h"
#include "GetVidasEpPayload.h"
#include "ReadSecPayload.h"
#include "SecRoutePayload.h"
#include "MovePayload.h"
#include "MonitorPressurePayload.h"
#include "GetCalibrationPayload.h"
#include "GetParameterPayload.h"
#include "SaveCalibrationPayload.h"
#include "SetMotorActivationPayload.h"
#include "InitPayload.h"
#include "CaptureImagePayload.h"
#include "GetMotorActivationPayload.h"
#include "TrayReadPayload.h"
#include "GetPressureOffsetsPayload.h"
#include "GetPressureSettingsPayload.h"
#include "SetMaintenanceModePayload.h"
#include "SSCalibratePayload.h"
#include "SHCalibratePayload.h"
#include "GetSSReferencePayload.h"
#include "OPTCalibratePayload.h"
#include "OPTCheckPayload.h"
#include "DisablePayload.h"
#include "SleepPayload.h"
#include "RunwlPayload.h"
#include "AutoCalibratePayload.h"
#include "SignalReadPayload.h"
#include "SHForceLedPayload.h"
#include "GetCounterPayload.h"
#include "GetGlobalSettingsPayload.h"
#include "SetParameterPayload.h"
#include "GetFanSettingsPayload.h"
#include "GetCameraRoiPayload.h"
#include "SaveCameraRoiPayload.h"
#include "FwUpdatePayload.h"

#include <xercesc/framework/MemBufFormatTarget.hpp>

bool fillOutCmdStatusTag(OutgoingCommand* &pCmd, DOMElement* &pRootElem);
bool fillOutCmdUsageTag(OutgoingCommand* &pCmd, DOMElement* &pRootElem);
bool fillOutCmdIdTag(OutgoingCommand* &pCmd, DOMElement* &pRootElem);
void fillOutCmdFailureCauseTag(OutgoingCommand* &pCmd, DOMDocument*& pOutDoc);
bool fillOutCmdStatusFailure(OutgoingCommand*& pOutCmd, DOMDocument*& pOutDoc, DOMElement*& pRootElem);

/* ********************************************************************************************************************
 * Serialization utilities implementation
 * ********************************************************************************************************************
 */
bool buildDOMfromOutgoingCommand(OutgoingCommand* pCmd, DOMDocument*& pOutDoc)
{
	if ( ( !pCmd ) || ( !pOutDoc ) )
		return false;

	DOMElement* pRootElem = 0;
	pRootElem = pOutDoc->getDocumentElement();
	if ( !pRootElem )
		return false;

	//<vminst:hello
	//	xmlns:vminst="http://biomerieux.com/vidas/instrument/protocol/v1/vminst"
	//	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />
	//pRootElem->setAttribute(X(WEB_CMD_ATTR_SCHEMA_URL_NAME), X(WEB_CMD_ATTR_SCHEMA_URL_VAL));
	bool bRetValue = true;
	if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_HELLO_OUT) == 0 )
	{
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_LOGIN_OUT) == 0 )
	{
		bRetValue = fillOutCmdLogin((OutCmdLogin*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_LOGOUT_OUT) == 0 )
	{
		bRetValue = fillOutCmdLogout((OutCmdLogout*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GETSENSOR_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetSensor((OutCmdGetSensor*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GETSERIAL_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetSerial((OutCmdGetSerial*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GETVERSIONS_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetVersions((OutCmdGetVersions*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_VIDASEP_OUT) == 0 )
	{
		bRetValue = fillOutCmdVidasEp((OutCmdVidasEp*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SETLED_OUT) == 0 )
	{
		bRetValue = fillOutCmdSetLed((OutCmdSetLed*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_READSEC_OUT) == 0 )
	{
		bRetValue = fillOutCmdReadSec((OutCmdReadSec*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_ROUTE_OUT) == 0 )
	{
		bRetValue = fillOutCmdSecRoute((OutCmdSecRoute*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_MOVE_OUT) == 0 )
	{
		bRetValue = fillOutCmdMove((OutCmdMove*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_MONITOR_PRESSURE_OUT) == 0 )
	{
		bRetValue = fillOutCmdMonitorPressure((OutCmdMonitorPressure*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GETPOSITION_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetPosition((OutCmdMove*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GETCALIBRATION_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetCalibration((OutCmdGetCalibration*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GETPARAMETER_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetParameter((OutCmdGetParameter*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SETPARAMETER_OUT) == 0 )
	{
		bRetValue = fillOutCmdSetParameter((OutCmdSetParameter*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SAVECALIBRATION_OUT) == 0 )
	{
		bRetValue = fillOutCmdSaveCalibration((OutCmdSaveCalibration*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SETMOTORACTIVATION_OUT) == 0 )
	{
		bRetValue = fillOutCmdSetMotorActivation((OutCmdSetMotorActivation*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GETMOTORACTIVATION_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetMotorActivation((OutCmdGetMotorActivation*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_INIT_OUT) == 0 )
	{
		bRetValue = fillOutCmdInit((OutCmdInit*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_CAM_CAPTURE_IMAGE_OUT) == 0  )
	{
		bRetValue = fillOutCmdCaptureImage((OutCmdCaptureImage*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_RUNWL_OUT) == 0 )
	{
		bRetValue = fillOutCmdRunwl((OutCmdRunwl*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_CANCELSECTION_OUT) == 0 )
	{
		bRetValue = fillOutCmdCancelSection((OutCmdCancelSection*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_TRAY_READ_OUT) == 0 )
	{
		bRetValue = fillOutCmdTrayRead((OutCmdTrayRead*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GET_PRESSURE_OFFSETS_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetPressureOffsets((OutCmdGetPressureOffsets*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GET_PRESSURE_SETTINGS_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetPressureSettings((OutCmdGetPressureSettings*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SET_MAINTENANCE_MODE_OUT) == 0 )
	{
		bRetValue = fillOutCmdSetMaintenanceMode((OutCmdSetMaintenanceMode*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SS_CALIBRATE_OUT) == 0 )
	{
		bRetValue = fillOutCmdSSCalibrate((OutCmdSSCalibrate*&)pCmd, pOutDoc);
	}
    else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SH_CALIBRATE_OUT) == 0 )
	{
		bRetValue = fillOutCmdSHCalibrate((OutCmdSHCalibrate*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GET_SS_REFERENCE_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetSSReference((OutCmdGetSSReference*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SETSERIAL_OUT) == 0 )
	{
		bRetValue = fillOutCmdSetSerial((OutCmdSetSerial*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SET_TIME_OUT) == 0 )
	{
		bRetValue = fillOutCmdSetTime((OutCmdSetTime*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_OPT_CALIBRATE_OUT) == 0 )
	{
		bRetValue = fillOutCmdOptCalibrate((OutCmdOPTCalibrate*&)pCmd, pOutDoc);
	}
    else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_OPT_CHECK_OUT) == 0 )
    {
        bRetValue = fillOutCmdOptCheck((OutCmdOPTCheck*&)pCmd, pOutDoc);
    }
    else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_AIR_CALIBRATE_OUT) == 0 )
	{
		bRetValue = fillOutCmdAirCalibrate((OutCmdAirCalibrate*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SHUTDOWN_OUT) == 0 )
	{
		bRetValue = fillOutCmdShutdown((OutCmdShutdown*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_DISABLE_OUT) == 0 )
	{
		bRetValue = fillOutCmdDisable((OutCmdDisable*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SLEEP_OUT) == 0 )
	{
		bRetValue = fillOutCmdSleep((OutCmdSleep*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_AUTO_CALIBRATE_OUT) == 0 )
	{
		bRetValue = fillOutCmdAutoCalibrate((OutCmdAutoCalibrate*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SET_GLOBAL_SETTINGS_OUT) == 0 )
	{
		bRetValue = fillOutCmdSetGlobalSettings((OutCmdSetGlobalSettings*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GET_GLOBAL_SETTINGS_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetGlobalSettings((OutCmdGetGlobalSettings*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SIGNAL_READ_OUT) == 0 )
	{
		bRetValue = fillOutCmdOutCmdSignalRead((OutCmdSignalRead*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SH_FORCE_LED_OUT) == 0 )
	{
		bRetValue = fillOutCmdOutCmdSHForceLed((OutCmdSHForceLed*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GET_CAMERA_ROI_OUT) == 0 )
	{
		bRetValue = fillOutCmdGetCameraRoi((OutCmdGetCameraRoi*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SAVE_CAMERA_ROI_OUT) == 0 )
	{
		bRetValue = fillOutCmdSaveCameraRoi((OutCmdSaveCameraRoi*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_FW_UPDATE_OUT) == 0 )
	{
		bRetValue = fillOutCmdFwUpdate((OutCmdFwUpdate*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GET_COUNTER_OUT) == 0 )
	{
		bRetValue = fillOutCmdOutGetCounter((OutCmdGetCounter*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SET_COUNTER_OUT) == 0 )
	{
		bRetValue = fillOutCmdOutSetCounter((OutCmdSetCounter*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_GET_FAN_SETTINGS_OUT) == 0 )
	{
		bRetValue = fillOutCmdOutGetFanSettings((OutCmdGetFanSettings*&)pCmd, pOutDoc);
	}
	else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_SET_FAN_SETTINGS_OUT) == 0 )
	{
		bRetValue = fillOutCmdOutSetFanSettings((OutCmdSetFanSettings*&)pCmd, pOutDoc);
	}
    else if ( strcmp(pCmd->getName().c_str(), WEB_CMD_NAME_PUMP_OUT) == 0 )
    {
        bRetValue = fillOutCmdPump((OutCmdPump*&)pCmd, pOutDoc);
    }
    else
	{
		// command in input not recognized
		bRetValue = fillOutCmdUnknown((OutCmdUnknown*&)pCmd, pOutDoc);
	}

	return bRetValue;
}

bool fillOutCmdStatusTag(OutgoingCommand* &pCmd, DOMElement* &pRootElem)
{
	// First fill with the status
	std::string strStatus(pCmd->getStatus());
	if ( strStatus.length() > 0 )
	{
		XStr xstrStatusName(XML_ATTR_STATUS_NAME);
		XStr xstrStatusValue(strStatus.c_str());
		pRootElem->setAttribute(xstrStatusName.unicodeForm(), xstrStatusValue.unicodeForm());
		return true;
	}
	return false;
}

bool fillOutCmdUsageTag(OutgoingCommand* &pCmd, DOMElement* &pRootElem)
{
	// fill with the Usage string
	XStr xstrUsageName(WEB_CMD_ATTR_USAGE);
	std::string strUsage(pCmd->getUsage());
	if ( strUsage.length() > 0 )
	{
		XStr xstrUsageValue(strUsage.c_str());
		pRootElem->setAttribute(xstrUsageName.unicodeForm(), xstrUsageValue.unicodeForm());
	}
	else
	{
		// if no Usage is specified -> default = COMMERCIAL
		XStr xstrUsageValue(WEB_COMMERCIAL_CONNECTION);
		pRootElem->setAttribute(xstrUsageName.unicodeForm(), xstrUsageValue.unicodeForm());
	}
	return true;
}

bool fillOutCmdIdTag(OutgoingCommand* &pCmd, DOMElement* &pRootElem)
{
	// fill with the ID string (if present)
	XStr xstrUsageName(WEB_CMD_ATTR_COMMAND_ID);
	std::string strID(pCmd->getID());
	if ( strID.length() > 0 )
	{
		XStr xstrUsageValue(strID.c_str());
		pRootElem->setAttribute(xstrUsageName.unicodeForm(), xstrUsageValue.unicodeForm());
	}
	return true;
}

void fillOutCmdFailureCauseTag(OutgoingCommand* &pCmd,  DOMDocument* &pOutDoc)
{
	// Fill with the FailureCause
	FailureCause* pFailureCause = 0;
	pFailureCause = pCmd->getFailureCause();
	if ( pFailureCause )
	{
		std::string strError;
		DOMElement* pRootElem = pOutDoc->getDocumentElement();
		int liFailureCauseCode = pFailureCause->getCodeInt();


		if(liFailureCauseCode > 0)
		{
			// the FailureCause is a MasterBoard error that has to be formatted as an error
			strError = getMasterErrorInfoFromCode(liFailureCauseCode);
		}
		else
		{
			// the Failure string is directly set in the object
			strError = pFailureCause->getCodeString();
		}

        std::vector<std::string> splitErrorString;
		splitErrorString = splitString(strError, HASHTAG_SEPARATOR);

        if(splitErrorString.size() >= 4)
		{
			std::string timestamp;
			// build timestamp as yyyy-mm-ddThh:mm:ss
            timestamp = formatTimestamp(splitErrorString.at(1));

			DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_FAILURECAUSE_NAME));
			pRootElem->appendChild(pElemTmp2);

			XStr xstrId(XML_ATTR_ID_NAME);
            XStr xstrIdValue(splitErrorString.at(1).c_str());
			pElemTmp2->setAttribute(xstrId.unicodeForm(), xstrIdValue.unicodeForm());

			DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_ATTR_ERROR_CODE_NAME));
			pElemTmp2->appendChild(pElemTmp3);
            pElemTmp3->setTextContent(X(splitErrorString.at(2).c_str()));

			pElemTmp3 = pOutDoc->createElement(X(XML_ATTR_ERROR_GROUP_NAME));
			pElemTmp2->appendChild(pElemTmp3);
            pElemTmp3->setTextContent(X(splitErrorString.at(3).c_str()));

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_COMPONENT_NAME));
			pElemTmp2->appendChild(pElemTmp3);
            pElemTmp3->setTextContent(X(splitErrorString.at(0).c_str()));

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TIMESTAMP_NAME));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(timestamp.c_str()));
		}
	}
}

bool fillOutCmdLogin(OutCmdLogin* &pOutCmdLogin, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	return fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdLogin, pOutDoc, pRootElem);
}

bool fillOutCmdLogout(OutCmdLogout*& pOutCmdLogout, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	return fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdLogout, pOutDoc, pRootElem);
}

bool fillOutCmdGetSensor(OutCmdGetSensor*& pOutCmdGetSensor, DOMDocument* &pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetSensor, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdGetSensor->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdGetSensor->getPayload() != 0)
	{
		GetSensorPayload * pGetSensorPayload = (GetSensorPayload *)pOutCmdGetSensor->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pGetSensorPayload->composePayload(pOutDoc, pRootElem);

	}
	return true;
}

bool fillOutCmdGetSerial(OutCmdGetSerial*& pOutCmdGetSerial, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetSerial, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdGetSerial->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdGetSerial->getPayload() != 0)
	{
		GetSerialPayload * pGetSerialPayload = (GetSerialPayload *)pOutCmdGetSerial->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pGetSerialPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdGetVersions(OutCmdGetVersions*& pOutCmdGetVersions, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetVersions, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdGetVersions->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdGetVersions->getPayload() != 0)
	{
		GetVersionsPayload * pGetVersionsPayload = (GetVersionsPayload *)pOutCmdGetVersions->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pGetVersionsPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdVidasEp(OutCmdVidasEp*& pOutCmdVidasEp, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdVidasEp, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdVidasEp->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdVidasEp->getPayload() != 0)
	{
		GetVidasEpPayload * pGetVidasEpPayload = (GetVidasEpPayload *)pOutCmdVidasEp->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pGetVidasEpPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdSetLed(OutCmdSetLed*& pOutCmdSetLed, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	return fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSetLed, pOutDoc, pRootElem);
}

bool fillOutCmdReadSec(OutCmdReadSec*& pOutCmdReadSec, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdReadSec, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdReadSec->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdReadSec->getPayload() != 0)
	{
		ReadSecPayload * pReadSecPayload = (ReadSecPayload *)pOutCmdReadSec->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pReadSecPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdSecRoute(OutCmdSecRoute*& pOutCmdSecRoute, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSecRoute, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdSecRoute->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdSecRoute->getPayload() != 0)
	{
		SecRoutePayload * pSecRoutePayload = (SecRoutePayload *)pOutCmdSecRoute->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pSecRoutePayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdMove(OutCmdMove*& pOutCmdMove, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdMove, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdMove->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdMove->getPayload() != 0)
	{
		MovePayload * pMovePayload = (MovePayload *)pOutCmdMove->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pMovePayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdMonitorPressure(OutCmdMonitorPressure*& pOutCmdMonitorPressure, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdMonitorPressure, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdMonitorPressure->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdMonitorPressure->getPayload() != 0)
	{
		MonitorPressurePayload * pMonitorPressurePayload = (MonitorPressurePayload *)pOutCmdMonitorPressure->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pMonitorPressurePayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdGetPosition(OutCmdMove*& pOutCmdGetPosition, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetPosition, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdGetPosition->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdGetPosition->getPayload() != 0)
	{
		MovePayload * pMovePayload = (MovePayload *)pOutCmdGetPosition->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pMovePayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdGetCalibration(OutCmdGetCalibration*& pOutCmdGetCalibration, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetCalibration, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdGetCalibration->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdGetCalibration->getPayload() != 0)
	{
		GetCalibrationPayload * pPayload = (GetCalibrationPayload *)pOutCmdGetCalibration->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdGetParameter(OutCmdGetParameter*& pOutCmdGetParameter, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetParameter, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdGetParameter->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdGetParameter->getPayload() != 0)
	{
		GetParameterPayload * pPayload = (GetParameterPayload *)pOutCmdGetParameter->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdSetParameter(OutCmdSetParameter*& pOutCmdSetParameter, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSetParameter, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdSetParameter->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdSetParameter->getPayload() != 0)
	{
		SetParameterPayload * pPayload = (SetParameterPayload *)pOutCmdSetParameter->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdGetMotorActivation(OutCmdGetMotorActivation*& pOutCmdGetMotorActivation, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetMotorActivation, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdGetMotorActivation->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdGetMotorActivation->getPayload() != 0)
	{
		GetMotorActivationPayload * pPayload = (GetMotorActivationPayload *)pOutCmdGetMotorActivation->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdSaveCalibration(OutCmdSaveCalibration*& pOutCmdSaveCalibration, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSaveCalibration, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdSaveCalibration->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdSaveCalibration->getPayload() != 0)
	{
		SaveCalibrationPayload * pPayload = (SaveCalibrationPayload *)pOutCmdSaveCalibration->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdSetMotorActivation(OutCmdSetMotorActivation*& pOutCmdSetMotorActivation, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSetMotorActivation, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdSetMotorActivation->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdSetMotorActivation->getPayload() != 0)
	{
		SetMotorActivationPayload * pPayload = (SetMotorActivationPayload *)pOutCmdSetMotorActivation->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdInit(OutCmdInit*& pOutCmdInit, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdInit, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdInit->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdInit->getPayload() != 0)
	{
		InitPayload * pPayload = (InitPayload *)pOutCmdInit->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdCaptureImage(OutCmdCaptureImage*& pOutCmdCaptureImage, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if ( ! fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdCaptureImage, pOutDoc, pRootElem) )
	{
		return false;
	}

	if ( ( ! strcmp(pOutCmdCaptureImage->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdCaptureImage->getPayload() )
	{
		CaptureImagePayload* pGetPicturePayload = (CaptureImagePayload*) pOutCmdCaptureImage->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pGetPicturePayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdRunwl(OutCmdRunwl*& pOutCmdRunwl, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdRunwl, pOutDoc, pRootElem))
		return false;

	if(pOutCmdRunwl->getPayload() == 0) return true; // no payload to add

	RunwlPayload* pRunwlPayload = (RunwlPayload*) pOutCmdRunwl->getPayload();

	// the asynchronous final reply is different according to the protocol status ->
	// fill all parameters (if the payload is valid)
	pRunwlPayload->composePayload(pOutDoc, pRootElem);

	return true;
}

bool fillOutCmdCancelSection(OutCmdCancelSection*& pOutCmdCancelSection, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdCancelSection, pOutDoc, pRootElem))
		return false;

	return true;
}

bool fillOutCmdTrayRead(OutCmdTrayRead*& pOutCmdTrayRead, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if ( ! fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdTrayRead, pOutDoc, pRootElem) )
	{
		return false;
	}

	if ( ( ! strcmp(pOutCmdTrayRead->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdTrayRead->getPayload() )
	{
		TrayReadPayload* pTrayReadPayload = (TrayReadPayload*) pOutCmdTrayRead->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pTrayReadPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdGetPressureOffsets(OutCmdGetPressureOffsets*& pOutCmdGetPressureOffsets, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetPressureOffsets, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdGetPressureOffsets->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdGetPressureOffsets->getPayload() != 0)
	{
		GetPressureOffsetsPayload * pPayload = (GetPressureOffsetsPayload *)pOutCmdGetPressureOffsets->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdGetPressureSettings(OutCmdGetPressureSettings*& pOutCmdGetPressureSettings, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetPressureSettings, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdGetPressureSettings->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdGetPressureSettings->getPayload() != 0)
	{
		GetPressureSettingsPayload * pPayload = (GetPressureSettingsPayload *)pOutCmdGetPressureSettings->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdSetMaintenanceMode(OutCmdSetMaintenanceMode*& pOutCmdSetMaintenanceMode, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSetMaintenanceMode, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdSetMaintenanceMode->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdSetMaintenanceMode->getPayload() != 0)
	{
		SetMaintenanceModePayload * pPayload = (SetMaintenanceModePayload *)pOutCmdSetMaintenanceMode->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdSSCalibrate(OutCmdSSCalibrate*& pOutCmdSSCalibrate, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSSCalibrate, pOutDoc, pRootElem))
		return false;

	if ( strcmp(pOutCmdSSCalibrate->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdSSCalibrate->getPayload() != 0)
	{
		SSCalibratePayload * pPayload = (SSCalibratePayload *)pOutCmdSSCalibrate->getPayload();
		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdSHCalibrate(OutCmdSHCalibrate*& pOutCmdSHCalibrate, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if ( ! fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSHCalibrate, pOutDoc, pRootElem) )
		return false;

	if ( strcmp(pOutCmdSHCalibrate->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdSHCalibrate->getPayload() != 0)
	{
		SHCalibratePayload * pPayload = (SHCalibratePayload *)pOutCmdSHCalibrate->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdGetSSReference(OutCmdGetSSReference*& pOutCmdGetSSReference, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if ( !fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetSSReference, pOutDoc, pRootElem) )
		return false;

	if ( strcmp(pOutCmdGetSSReference->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdGetSSReference->getPayload() != 0)
	{
		GetSSReferencePayload * pPayload = (GetSSReferencePayload *)pOutCmdGetSSReference->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdSetSerial(OutCmdSetSerial*& pOutCmdSetSerial, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSetSerial, pOutDoc, pRootElem))
		return false;

	// no payload to add

	return true;
}

bool fillOutCmdSetTime(OutCmdSetTime*& pOutCmdSetTime, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSetTime, pOutDoc, pRootElem))
		return false;

	// no payload to add

	return true;
}

bool fillOutCmdOptCalibrate(OutCmdOPTCalibrate*& pOutCmdOPTCalibrate, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if ( ! fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdOPTCalibrate, pOutDoc, pRootElem) )
		return false;

	if ( strcmp(pOutCmdOPTCalibrate->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
		 pOutCmdOPTCalibrate->getPayload() != 0)
	{
		OPTCalibratePayload * pPayload = (OPTCalibratePayload *)pOutCmdOPTCalibrate->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pPayload->composePayload(pOutDoc, pRootElem);
	}
	return true;
}

bool fillOutCmdOptCheck(OutCmdOPTCheck *&pOutCmdOPTCheck, DOMDocument *&pOutDoc)
{
    DOMElement* pRootElem = 0;

    if ( ! fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdOPTCheck, pOutDoc, pRootElem) )
        return false;

    if ( strcmp(pOutCmdOPTCheck->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) == 0 &&
         pOutCmdOPTCheck->getPayload() != 0)
    {
        OPTCheckPayload * pPayload = (OPTCheckPayload *)pOutCmdOPTCheck->getPayload();

        // if the status command is SUCCESFULL it's the asynchronous final reply ->
        // fill all parameters (if the payload is valid)
        pPayload->composePayload(pOutDoc, pRootElem);
    }
    return true;
}

bool fillOutCmdAirCalibrate(OutCmdAirCalibrate*& pOutCmdAirCalibrate, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	return fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdAirCalibrate, pOutDoc, pRootElem);
}

bool fillOutCmdShutdown(OutCmdShutdown*& pOutCmdShutdown, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdShutdown, pOutDoc, pRootElem))
		return false;

	// no payload to add

	return true;
}

bool fillOutCmdDisable(OutCmdDisable*& pOutCmdDisable, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdDisable, pOutDoc, pRootElem))
		return false;

	if ( ( ! strcmp(pOutCmdDisable->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdDisable->getPayload() )
	{
		DisablePayload* pDisablePayload = (DisablePayload*) pOutCmdDisable->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pDisablePayload->composePayload(pOutDoc, pRootElem);
	}

	return true;

}

bool fillOutCmdSleep(OutCmdSleep*& pOutCmdSleep, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSleep, pOutDoc, pRootElem))
		return false;

	if ( ( ! strcmp(pOutCmdSleep->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdSleep->getPayload() )
	{
		SleepPayload* pSleepPayload = (SleepPayload*) pOutCmdSleep->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pSleepPayload->composePayload(pOutDoc, pRootElem);
	}

	return true;
}

bool fillOutCmdAutoCalibrate(OutCmdAutoCalibrate* &pOutCmdAutoCalibrate, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdAutoCalibrate, pOutDoc, pRootElem))
		return false;

	if ( ( ! strcmp(pOutCmdAutoCalibrate->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdAutoCalibrate->getPayload() )
	{
		AutoCalibratePayload* pAutoCalibratePayload = (AutoCalibratePayload*) pOutCmdAutoCalibrate->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pAutoCalibratePayload->composePayload(pOutDoc, pRootElem);
	}

	return true;
}

bool fillOutCmdSetGlobalSettings(OutCmdSetGlobalSettings*& pOutCmdGlobalSettings, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGlobalSettings, pOutDoc, pRootElem))
		return false;

	// no payload to add

	return true;
}

bool fillOutCmdGetGlobalSettings(OutCmdGetGlobalSettings*& pOutCmdGetGlobalSettings, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetGlobalSettings, pOutDoc, pRootElem))
		return false;

	if ( ( ! strcmp(pOutCmdGetGlobalSettings->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdGetGlobalSettings->getPayload() )
	{
		GetGlobalSettingsPayload* pGetSettingsPayload = (GetGlobalSettingsPayload*) pOutCmdGetGlobalSettings->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pGetSettingsPayload->composePayload(pOutDoc, pRootElem);
	}

	return true;
}

bool fillOutCmdOutCmdSignalRead(OutCmdSignalRead*& pOutCmdSignalRead, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSignalRead, pOutDoc, pRootElem))
		return false;

	if ( ( ! strcmp(pOutCmdSignalRead->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdSignalRead->getPayload() )
	{
		SignalReadPayload* pSignalReadPayload = (SignalReadPayload*) pOutCmdSignalRead->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pSignalReadPayload->composePayload(pOutDoc, pRootElem);
	}

	return true;
}

bool fillOutCmdOutCmdSHForceLed(OutCmdSHForceLed*& pOutCmdSHForceLed, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSHForceLed, pOutDoc, pRootElem))
		return false;

	if ( ( ! strcmp(pOutCmdSHForceLed->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdSHForceLed->getPayload() )
	{
		SHForceLedPayload* pSHForceLedPayload = (SHForceLedPayload*) pOutCmdSHForceLed->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pSHForceLedPayload->composePayload(pOutDoc, pRootElem);
	}

	return true;
}

bool fillOutCmdGetCameraRoi(OutCmdGetCameraRoi*& pOutCmdGetCameraRoi, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if ( !fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetCameraRoi, pOutDoc, pRootElem) )
		return false;

	if ( ( ! strcmp(pOutCmdGetCameraRoi->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdGetCameraRoi->getPayload() )
	{
		GetCameraRoiPayload* pGetCameraRoiPayload = (GetCameraRoiPayload*) pOutCmdGetCameraRoi->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pGetCameraRoiPayload->composePayload(pOutDoc, pRootElem);
	}

	return true;
}

bool fillOutCmdOutGetCounter(OutCmdGetCounter*& pOutCmdGetCounter, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetCounter, pOutDoc, pRootElem))
		return false;

	if ( ( ! strcmp(pOutCmdGetCounter->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdGetCounter->getPayload() )
	{
		GetCounterPayload* pGetCounterPayload = (GetCounterPayload*) pOutCmdGetCounter->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pGetCounterPayload->composePayload(pOutDoc, pRootElem);
	}

	return true;
}

bool fillOutCmdSaveCameraRoi(OutCmdSaveCameraRoi*& pOutCmdSaveCameraRoi, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if ( !fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSaveCameraRoi, pOutDoc, pRootElem) )
		return false;

	if ( ( ! strcmp(pOutCmdSaveCameraRoi->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdSaveCameraRoi->getPayload() )
	{
		SaveCameraRoiPayload* pSaveCameraRoiPayload = (SaveCameraRoiPayload*) pOutCmdSaveCameraRoi->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pSaveCameraRoiPayload->composePayload(pOutDoc, pRootElem);
	}

	return true;
}

bool fillOutCmdOutSetCounter(OutCmdSetCounter*& pOutCmdSetCounter, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSetCounter, pOutDoc, pRootElem))
		return false;

	// no payload to add

	return true;
}

bool fillOutCmdOutGetFanSettings(OutCmdGetFanSettings*& pOutCmdGetFanSettings, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdGetFanSettings, pOutDoc, pRootElem))
		return false;

	if ( ( ! strcmp(pOutCmdGetFanSettings->getStatus().c_str(), XML_ATTR_STATUS_SUCCESSFULL) ) &&
		 pOutCmdGetFanSettings->getPayload() )
	{
		GetFanSettingsPayload* pGetFanSettingsPayload = (GetFanSettingsPayload*) pOutCmdGetFanSettings->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pGetFanSettingsPayload->composePayload(pOutDoc, pRootElem);
	}

	return true;
}

bool fillOutCmdFwUpdate(OutCmdFwUpdate*& pOutCmdFwUpdate, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if ( !fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdFwUpdate, pOutDoc, pRootElem) )
		return false;

    // the FWUPDATE command has progress messages tagged as ACCEPTED containing a payload
    if (pOutCmdFwUpdate->getPayload() )
	{
		FwUpdatePayload* pFwUpdatePayload = (FwUpdatePayload*)pOutCmdFwUpdate->getPayload();

		// if the status command is SUCCESFULL it's the asynchronous final reply ->
		// fill all parameters (if the payload is valid)
		pFwUpdatePayload->composePayload(pOutDoc, pRootElem);
	}

	return true;
}

bool fillOutCmdOutSetFanSettings(OutCmdSetFanSettings*& pOutCmdSetFanSettings, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdSetFanSettings, pOutDoc, pRootElem))
		return false;

	// no payload to add

	return true;
}

bool fillOutCmdPump(OutCmdPump *&pOutCmdPump, DOMDocument *&pOutDoc)
{
    DOMElement* pRootElem = 0;

    if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdPump, pOutDoc, pRootElem))
        return false;

    // no payload to add

    return true;
}

// --------------------------------------------------------------------------------------------
// functions to build the Temperature, Pressure, Optics payload in Run reply
// --------------------------------------------------------------------------------------------

bool buildDOMfromData(uint8_t section, uint8_t strip, std::string strDataType, DOMDocument*& pOutDoc)
{
	if ( strDataType.empty() || (pOutDoc == 0) ||
		 (section >= SCT_NUM_TOT_SECTIONS) || (strip >= SCT_NUM_TOT_SLOTS))
	{
		return false;
	}

	DOMElement* pRootElem = 0;
	pRootElem = pOutDoc->getDocumentElement();
	if ( !pRootElem )
		return false;

	bool bRetValue = true;

	if(strDataType.compare(WEB_CMD_DATA_TEMPERATURE) == 0)
	{
		bRetValue = buildTemperaturePayload(section, pOutDoc, pRootElem);
	}
	else if(strDataType.compare(WEB_CMD_DATA_TREATED_PRESSURE) == 0)
	{
		bRetValue = buildTreatedPressurePayload(section, strip, pOutDoc, pRootElem);
	}
	else if(strDataType.compare(WEB_CMD_DATA_ELABORATED_PRESSURE) == 0)
	{
		bRetValue = buildElaboratedPressurePayload(section, strip, pOutDoc, pRootElem);
	}
	else if(strDataType.compare(WEB_CMD_DATA_OPTICS) == 0)
	{
		bRetValue = buildOpticPayload(section, pOutDoc, pRootElem);
	}

	return bRetValue;
}

// ---------------------------------------------------------------------------------------------- //
//     service functions
// ---------------------------------------------------------------------------------------------- //

std::string dom2String(DOMDocument* pDocument, DOMImplementation *pDOMImplementation)
{

	std::string strOut("");

	DOMImplementation *pLocalDOMImplementation = 0;
	DOMLSSerializer *pSerializer = 0;
	DOMLSOutput* theOutput = 0;
	/* ****************************************************************************************************************
	 * Return the first registered implementation that
	 * has the desired features. In this case, we are after
	 * a DOM implementation that has the LS feature... or Load/Save.
	 * ****************************************************************************************************************
	 */
	if ( pDOMImplementation == 0 )
	{
		pLocalDOMImplementation = DOMImplementationRegistry::getDOMImplementation(X("LS"));
	}
	else
	{
		pLocalDOMImplementation = pDOMImplementation;
	}

	/* ****************************************************************************************************************
	 * From the DOMImplementation, create a DOMWriter.
	 * DOMWriters are used to serialize a DOM tree [back] into an XML document.
	 * ****************************************************************************************************************
	 */
	pSerializer = ((DOMImplementationLS*)pLocalDOMImplementation)->createLSSerializer();
	theOutput = ((DOMImplementationLS*)pLocalDOMImplementation)->createLSOutput();
	if ( pSerializer == 0 ) return strOut;

	/* ****************************************************************************************************************
	 * This line is optional. It just sets a feature of the Serializer to make the output more human-readable
	 * by inserting line-feeds, without actually inserting any new elements/nodes into the DOM tree.
	 * (There are many different features to set.) Comment it out and see the difference.
	 * ****************************************************************************************************************
	 */
	DOMConfiguration* pDomConfiguration = pSerializer->getDomConfig();
	pDomConfiguration->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);

	/* ****************************************************************************************************************
	 * Serialize the DOM into a string
	 * ****************************************************************************************************************
	 */
	XMLCh tempStr[100];
	XMLString::transcode("UTF-8", tempStr, 99);
	theOutput->setEncoding(tempStr);
	MemBufFormatTarget memTarget;
	theOutput->setByteStream(&memTarget);
	pSerializer->write(pDocument, theOutput);
	const XMLByte* utf8str = memTarget.getRawBuffer();
	strOut.assign((char*)utf8str);
	theOutput->release();
	pSerializer->release();
	return strOut;
}


bool fillOutCmdStatusFailure(OutgoingCommand*& pOutCmd, DOMDocument*& pOutDoc, DOMElement*& pRootElem)
{
	if ( ( !pOutCmd ) || ( !pOutDoc ) )
		return false;

	pRootElem = 0;
	pRootElem = pOutDoc->getDocumentElement();
	if ( !pRootElem )
		return false;

	// insert the Usage string as read from the incoming command
	fillOutCmdUsageTag(pOutCmd, pRootElem);
	fillOutCmdIdTag(pOutCmd, pRootElem);

	// fill with the status and failure code (if present)
	if(fillOutCmdStatusTag(pOutCmd, pRootElem))
	{
		fillOutCmdFailureCauseTag(pOutCmd, pOutDoc);
		return true;
	}
	return false;
}

bool fillOutCmdUnknown(OutCmdUnknown*& pOutCmdUnknown, DOMDocument*& pOutDoc)
{
	DOMElement* pRootElem = 0;

	if(!fillOutCmdStatusFailure((OutgoingCommand*&)pOutCmdUnknown, pOutDoc, pRootElem))
		return false;

	// no payload to add
	return true;
}


