/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    XStr.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the XStr class.
 @details

 ****************************************************************************
*/

#include "XStr.h"

XStr::XStr(const char* const sStringToTranscode)
{
	// Call the private transcoding method
	fUnicodeForm = XMLString::transcode(sStringToTranscode);
}

XStr::~XStr()
{
	XMLString::release(&fUnicodeForm);
}

const XMLCh* XStr::unicodeForm() const
{
	return fUnicodeForm;
}
