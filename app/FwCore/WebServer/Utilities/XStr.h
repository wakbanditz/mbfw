/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    XStr.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the XStr class.
 @details

 ****************************************************************************
*/
#ifndef XSTR_H

#define XSTR_H

#include <xercesc/util/XMLString.hpp>

#if defined(XERCES_NEW_IOSTREAMS)
#include <iostream>
#else
#include <iostream.h>
#endif


XERCES_CPP_NAMESPACE_USE

/*! *******************************************************************************************************************
 * @brief The XStr class	simple class for easy transcoding char* data into XMLCh data.
 * ********************************************************************************************************************
 */
class XStr
{

	public :

		/*! ***********************************************************************************************************
		 * @brief XStr
		 * @param sStringToTranscode	pointer to a standard C null terminated string XMLCh object to be transcoded
		 *								into a XMLCh
		 * ************************************************************************************************************
		 */
		XStr(const char* const sStringToTranscode);

		/*! ***********************************************************************************************************
		 * @brief ~XStr			destructor, deallocates the internal XMLCh object
		 * ************************************************************************************************************
		 */
		~XStr();

		/*! ***********************************************************************************************************
		 * @brief unicodeForm	get the unicode representation of the standard C string specified in the constructor
		 * @return				the XMLCh* object representing the standard C string specified in the constructor
		 * ************************************************************************************************************
		 */
		const XMLCh* unicodeForm() const;

private :

    XMLCh*   fUnicodeForm;		// This is the Unicode XMLCh format of the string

};

#define X(str) XStr(str).unicodeForm()

#endif // XSTR_H
