/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ParsingUtilities.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the ParsingUtilities class.
 @details

 ****************************************************************************
*/

#include "ParsingUtilities.h"
#include "CommonInclude.h"
#include "WebServerAndProtocolInclude.h"
#include "StrX.h"
#include "XStr.h"
#include "MainExecutor.h"

#include "xercesc/dom/DOMNodeList.hpp"
#include "xercesc/dom/DOMNamedNodeMap.hpp"

#include "THAutoLogout.h"
#include "Payload.h"

bool buildIncomingCommandFromRootNode(DOMElement* pXmlRootNode, IncomingCommand*& pIncomingCommand)
{
	if ( !pXmlRootNode )
		return false;

	SAFE_DELETE(pIncomingCommand);

	bool bRetValue = true;

	StrX strTagCommandName(pXmlRootNode->getTagName());
	StrX strUsageValue(pXmlRootNode->getAttribute(X(WEB_CMD_ATTR_USAGE)));
	StrX strIdValue(pXmlRootNode->getAttribute(X(WEB_CMD_ATTR_COMMAND_ID)));

	// record on event file the received command
    // (HELLO and GETSENSORS are excluded because sent periodically so not relevant)
    if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_HELLO_IN_FULL) != 0 &&
         strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GETSENSOR_IN_FULL) != 0 )
    {
        registerCommandEvent(strTagCommandName.getString());
    }

	if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_HELLO_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdHello;
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_LOGIN_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdLogin(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdLogin(pXmlRootNode, (InCmdLogin*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_LOGOUT_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdLogout(strUsageValue.getString(), strIdValue.getString());
		// no need to have a fillInCmdLogout as the Logout command does nolt have any input info
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GET_SERIAL_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetSerial(strUsageValue.getString(), strIdValue.getString());
		// no need to have a fillInCmdGetSerial as the GetSerial command does not have any input info
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GET_VERSIONS_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetVersions(strUsageValue.getString(), strIdValue.getString());
		// no need to have a fillInCmdGetVersions as the GetVersions command does not have any input info
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_VIDASEP_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdVidasEp(strUsageValue.getString(), strIdValue.getString());
		// no need to have a fillInCmdVidasEp as the VidasEp command does not have any input info
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GETSENSOR_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetSensor(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdGetSensor(pXmlRootNode, (InCmdGetSensor*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SETLED_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSetLed(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSetLed(pXmlRootNode, (InCmdSetLed*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_READSEC_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdReadSec(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdReadSec(pXmlRootNode, (InCmdReadSec*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_ROUTE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSecRoute(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSecRoute(pXmlRootNode, (InCmdSecRoute*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_MOVE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdMove(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdMove(pXmlRootNode, (InCmdMove*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_MONITOR_PRESSURE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdMonitorPressure(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdMonitorPressure(pXmlRootNode, (InCmdMonitorPressure*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GETPOSITION_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetPosition(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdGetPosition(pXmlRootNode, (InCmdGetPosition*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GETCALIBRATION_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetCalibration(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdGetCalibration(pXmlRootNode, (InCmdGetCalibration*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GETPARAMETER_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetParameter(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdGetParameter(pXmlRootNode, (InCmdGetParameter*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SETPARAMETER_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSetParameter(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSetParameter(pXmlRootNode, (InCmdSetParameter*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SAVECALIBRATION_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSaveCalibration(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSaveCalibration(pXmlRootNode, (InCmdSaveCalibration*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_CAM_CAPTURE_IMAGE_FULL) == 0 )
	{
		pIncomingCommand = new InCmdCaptureImage(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdCaptureImage(pXmlRootNode, (InCmdCaptureImage*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SETMOTORACTIVATION_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSetMotorActivation(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSetMotorActivation(pXmlRootNode, (InCmdSetMotorActivation*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GETMOTORACTIVATION_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetMotorActivation(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdGetMotorActivation(pXmlRootNode, (InCmdGetMotorActivation*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_INIT_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdInit(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdInit(pXmlRootNode, (InCmdInit*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_RUNWL_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdRunwl(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdRunwl(pXmlRootNode, (InCmdRunwl*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_CANCELSECTION_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdCancelSection(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdCancelSection(pXmlRootNode, (InCmdCancelSection*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_TRAY_READ_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdTrayRead(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdTrayRead(pXmlRootNode, (InCmdTrayRead*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GET_PRESSURE_OFFSETS_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetPressureOffsets(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdGetPressureOffsets(pXmlRootNode, (InCmdGetPressureOffsets*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GET_PRESSURE_SETTINGS_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetPressureSettings(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdGetPressureSettings(pXmlRootNode, (InCmdGetPressureSettings*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SET_MAINTENANCE_MODE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSetMaintenanceMode(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSetMaintenanceMode(pXmlRootNode, (InCmdSetMaintenanceMode*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SS_CALIBRATE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSSCalibrate(strUsageValue.getString(), strIdValue.getString());
		// no need to have a fillInCmdSSCalibrate as the SSCalibrate command does not have any input info
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SH_CALIBRATE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSHCalibrate(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSHCalibrate(pXmlRootNode, (InCmdSHCalibrate*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GET_SS_REFERENCE_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetSSReference(strUsageValue.getString(), strIdValue.getString());
		// no need to have a fillInCmdGetSSReference as the Get_SS_Reference command does not have any input info
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SETSERIAL_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSetSerial(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSetSerial(pXmlRootNode, (InCmdSetSerial*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SET_TIME_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSetTime(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSetTime(pXmlRootNode, (InCmdSetTime*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_OPT_CALIBRATE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdOPTCalibrate(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdOPTCalibrate(pXmlRootNode, (InCmdOPTCalibrate*&)pIncomingCommand);
	}
    else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_OPT_CHECK_IN_FULL) == 0 )
    {
        pIncomingCommand = new InCmdOPTCheck(strUsageValue.getString(), strIdValue.getString());
        bRetValue = fillInCmdOPTCheck(pXmlRootNode, (InCmdOPTCheck*&)pIncomingCommand);
    }
    else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_AIR_CALIBRATE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdAirCalibrate(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdAirCalibrate(pXmlRootNode, (InCmdAirCalibrate*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SHUTDOWN_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdShutdown(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdShutdown(pXmlRootNode, (InCmdShutdown*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_DISABLE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdDisable(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdDisable(pXmlRootNode, (InCmdDisable*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SLEEP_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSleep(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSleep(pXmlRootNode, (InCmdSleep*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_AUTO_CALIBRATE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdAutoCalibrate(strUsageValue.getString(), strIdValue.getString());
		// no need to have a fillIInCmdAutoCalibrate as the AutoCalibrate command does not have any input info
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SET_GLOBAL_SETTINGS_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSetGlobalSettings(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSetGlobalSettings(pXmlRootNode, (InCmdSetGlobalSettings*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GET_GLOBAL_SETTINGS_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetGlobalSettings(strUsageValue.getString(), strIdValue.getString());
		// no need to have a fillIInCmdGetGlobalSettings as the GetGlobalSettings command does not have any input info
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SIGNAL_READ_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSignalRead(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSignalRead(pXmlRootNode, (InCmdSignalRead*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SH_FORCE_LED_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSHForceLed(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSHForceLed(pXmlRootNode, (InCmdSHForceLed*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GET_CAMERA_ROI_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetCameraRoi(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdGetCameraRoi(pXmlRootNode, (InCmdGetCameraRoi*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GET_COUNTER_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetCounter(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdGetCounter(pXmlRootNode, (InCmdGetCounter*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SET_COUNTER_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSetCounter(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSetCounter(pXmlRootNode, (InCmdSetCounter*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_GET_FAN_SETTINGS_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdGetFanSettings(strUsageValue.getString(), strIdValue.getString());
		// no need to have a fillIInCmdGetFanSettings as the command does not have any input info
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SET_FAN_SETTINGS_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSetFanSettings(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSetFanSettings(pXmlRootNode, (InCmdSetFanSettings*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_SAVE_CAMERA_ROI_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdSaveCameraRoi(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdSaveCameraRoi(pXmlRootNode, (InCmdSaveCameraRoi*&)pIncomingCommand);
	}
	else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_FW_UPDATE_IN_FULL) == 0 )
	{
		pIncomingCommand = new InCmdFwUpdate(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdFwUpdate(pXmlRootNode, (InCmdFwUpdate*&)pIncomingCommand);
	}
    else if ( strcmp(strTagCommandName.getString(), WEB_CMD_NAME_PUMP_IN_FULL) == 0 )
    {
        pIncomingCommand = new InCmdPump(strUsageValue.getString(), strIdValue.getString());
        bRetValue = fillInCmdPump(pXmlRootNode, (InCmdPump*&)pIncomingCommand);
    }
    else
	{
		// command not defined
		pIncomingCommand = new InCmdUnknown(strUsageValue.getString(), strIdValue.getString());
		bRetValue = fillInCmdUnknown(pXmlRootNode, (InCmdUnknown*&)pIncomingCommand, strTagCommandName.getString());
	}

	// this function simply reset the hello timeout to prevent disconnection
	THAutoLogout::getInstance()->resetConnection(strUsageValue.getString());

	return bRetValue;
}

bool fillInCmdLogin(DOMElement* pXmlRootNode, InCmdLogin*& pInCmdLogin)
{
	if( ( !pXmlRootNode ) || ( !pInCmdLogin ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		std::string strNodeName(sNodeName.getString());
		if ( strNodeName.length() > 0 )
		{
			StrX sVal(l->item(i)->getTextContent());
			std::string strNodeVal(sVal.getString());
			if ( strNodeName.compare(XML_TAG_ID_NAME) == 0 )
			{
				pInCmdLogin->setId(strNodeVal);
			}
			else if ( strNodeName.compare(XML_TAG_PWD_NAME) == 0 )
			{
				pInCmdLogin->setPwd(strNodeVal);
			}
		}
	}
	return true;

}

bool fillInCmdGetSensor(DOMElement* pXmlRootNode, InCmdGetSensor*& pInCmdGetSensor)
{
	if( ( !pXmlRootNode ) || ( !pInCmdGetSensor ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if ( strNodeName.compare(XML_TAG_CATEGORY_NAME) == 0 )
			{
				pInCmdGetSensor->setCategory(strNodeVal);
			}
			else if ( strNodeName.compare(XML_TAG_COMPONENT_NAME) == 0 )
			{
				pInCmdGetSensor->setComponent(strNodeVal);
			}
			else if ( strNodeName.compare(XML_TAG_ID_NAME) == 0 )
			{
				pInCmdGetSensor->setId(strNodeVal);
			}
		}
	}

	return true;
}

bool fillInCmdMove(DOMElement* pXmlRootNode, InCmdMove*& pInCmdMove)
{
	if( ( !pXmlRootNode ) || ( !pInCmdMove ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_COMPONENT_NAME) == 0)
			{
				pInCmdMove->setComponent(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_MOTOR_NAME) == 0)
			{
				pInCmdMove->setMotor(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_MOVE_NAME) == 0)
			{				
				DOMNodeList* p = l->item(i)->getChildNodes();
				int lNumSubChild = p->getLength();

				for ( int lcount = 0; lcount < lNumSubChild; lcount++ )
				{
					StrX sSubNodeName(p->item(lcount)->getNodeName());
					string strSubNodeName(sSubNodeName.getString());
					if (strSubNodeName.length() > 0)
					{
						StrX sSubVal(p->item(lcount)->getTextContent());
						string strSubNodeVal(sSubVal.getString());
						if (strSubNodeName.compare(XML_TAG_MOVE_VALUE_RELATIVE) == 0)
						{
							pInCmdMove->setMove(strSubNodeName,strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_MOVE_VALUE_ABSOLUTE) == 0)
						{
							pInCmdMove->setMove(strSubNodeName,strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_MOVE_VALUE_PREDEFINED) == 0)
						{
							pInCmdMove->setMove(strSubNodeName,strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_MOVE_VALUE_HOME) == 0)
						{
							pInCmdMove->setMove(strSubNodeName,strSubNodeVal);
						}
						else
						{
						}
					}
				}
			}
			else
			{

			}
		}
	}
	return true;
}

bool fillInCmdSetLed(DOMElement* pXmlRootNode, InCmdSetLed*& pInCmdSetLed)
{
	if( ( !pXmlRootNode ) || ( !pInCmdSetLed ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		std::string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			std::string strNodeVal(sVal.getString());
			if ( strNodeName.compare(XML_TAG_ID_NAME) == 0 )
			{
				pInCmdSetLed->setComponent(strNodeVal);
			}
			else if ( strNodeName.compare(XML_TAG_COLOR_NAME) == 0 )
			{
				pInCmdSetLed->setLed(strNodeVal);
			}
			else if ( strNodeName.compare(XML_TAG_MODE_NAME) == 0 )
			{
				pInCmdSetLed->setMode(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdReadSec(DOMElement* pXmlRootNode, InCmdReadSec*& pInCmdReadSec)
{
	if( ( !pXmlRootNode ) || ( !pInCmdReadSec ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if ( strNodeName.length() > 0 )
		{
			if ( strNodeName.compare(XML_TAG_SECTION_NAME) == 0 )
			{
				StrX sVal(l->item(i)->getTextContent());
				string strNodeVal(sVal.getString());
				// section involved in ReadSec
				pInCmdReadSec->setSection(strNodeVal);
			}
			else if ( strNodeName.compare(XML_TAG_DETECTION_SETTINGS_NAME) == 0 )
			{
				// settings involved in ReadSec TODO all
				string strId = "1"; //default
				DOMNamedNodeMap * pSettingsMap = (l->item(i)->getAttributes());
				if ( pSettingsMap )
				{
					DOMNode * pDomNode;
					XStr sAttr1(XML_TAG_PROTO_ID);
					pDomNode = pSettingsMap->getNamedItem(sAttr1.unicodeForm());
					if(pDomNode)
					{
						// strip barcode acquisition
						StrX sValAttr(pDomNode->getNodeValue());
						strId = (sValAttr.getString());
					}
				}

				StrX sVal(l->item(i)->getTextContent());
				string strNodeVal(sVal.getString());
				pInCmdReadSec->setSettings(strNodeVal, strId);
			}
			else if ( strNodeName.compare(XML_TAG_SLOT_NAME) == 0 )
			{
				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if ( pNamedNodeMap )
				{
					DOMNode * pDomNode;
					XStr sAttr1(XML_ATTR_NUMBER_NAME);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());

					string strSlotNum("");
					if ( pDomNode )
					{
						// strip barcode acquisition
						StrX sValAttr(pDomNode->getNodeValue());
						strSlotNum = sValAttr.getString();
						pInCmdReadSec->setStrip(strSlotNum);
					}

					DOMNodeList* pReadSecNodeList = (l->item(i)->getChildNodes());
					int liNumChilds = pReadSecNodeList->getLength();
					for ( int i = 0; i != liNumChilds; ++i )
					{
						StrX sIntraNodeName(pReadSecNodeList->item(i)->getNodeName());
						string strIntraNodeName(sIntraNodeName.getString());
						if ( strIntraNodeName.length() > 0 )
						{
							if ( ! strIntraNodeName.compare(XML_ATTR_STRIP_NAME) )
							{
								// strip barcode acquisition
								StrX sValAttr(pReadSecNodeList->item(i)->getTextContent());
								string strValAttr(sValAttr.getString());
								pInCmdReadSec->setStripCheck(strSlotNum, strValAttr);

								DOMNamedNodeMap* pInfoNodeMap = (pReadSecNodeList->item(i)->getAttributes());
								if ( pInfoNodeMap )
								{
									DOMNode* pImageDomNode;
									XStr sAttrImage(XML_TAG_IMAGE_NAME);
									pImageDomNode = pInfoNodeMap->getNamedItem(sAttrImage.unicodeForm());
									if ( pImageDomNode )
									{
										// section string
										StrX sValAttr(pImageDomNode->getNodeValue());
										string strValAttr(sValAttr.getString());
										pInCmdReadSec->setStripImage(strSlotNum, strValAttr);
									}
								}
							}
							else if ( ! strIntraNodeName.compare(XML_ATTR_SPR_NAME) )
							{
								// spr datamatrix acquisition
								StrX sValAttr(pReadSecNodeList->item(i)->getTextContent());
								string strValAttr(sValAttr.getString());
								pInCmdReadSec->setSprCheck(strSlotNum, strValAttr);

								DOMNamedNodeMap* pInfoNodeMap = (pReadSecNodeList->item(i)->getAttributes());
								if ( pInfoNodeMap )
								{
									DOMNode* pImageDomNode;
									XStr sAttrImage(XML_TAG_IMAGE_NAME);
									pImageDomNode = pInfoNodeMap->getNamedItem(sAttrImage.unicodeForm());
									if ( pImageDomNode )
									{
										// section string
										StrX sValAttr(pImageDomNode->getNodeValue());
										string strValAttr(sValAttr.getString());
										pInCmdReadSec->setSprImage(strSlotNum, strValAttr);
									}
								}
							}
							else if ( ! strIntraNodeName.compare(XML_ATTR_SUBSTRATE_NAME) )
							{
								// substrate check (strip presence)
								StrX sValAttr(pReadSecNodeList->item(i)->getTextContent());
								string strValAttr(sValAttr.getString());
								pInCmdReadSec->setSubstrateCheck(strSlotNum, strValAttr);
							}
							else if ( ! strIntraNodeName.compare(XML_ATTR_SAMPLE_NAME) )
							{
								// sample detection
								StrX sValAttr(pReadSecNodeList->item(i)->getTextContent());
								string strSampleEnable(sValAttr.getString());

								DOMNamedNodeMap* pInfoNodeMap = (pReadSecNodeList->item(i)->getAttributes());
								if ( pInfoNodeMap )
								{
									DOMNode* pImageDomNode;
									XStr sAttrWell(XML_TAG_WELL_NAME);
									string strWell("");
									pImageDomNode = pInfoNodeMap->getNamedItem(sAttrWell.unicodeForm());
									if ( pImageDomNode )
									{
										// section string
										StrX sValAttr(pImageDomNode->getNodeValue());
										strWell.assign(sValAttr.getString());
										pInCmdReadSec->setSampleCheck(strSlotNum, strWell, strSampleEnable);
									}

									if ( strWell.empty() )	continue;

									XStr sAttrImage(XML_TAG_IMAGE_NAME);
									pImageDomNode = pInfoNodeMap->getNamedItem(sAttrImage.unicodeForm());
									if ( pImageDomNode )
									{
										// section string
										StrX sValAttr(pImageDomNode->getNodeValue());
										string strValAttr(sValAttr.getString());
										pInCmdReadSec->setSampleImage(strSlotNum, strWell, strValAttr);
									}

									XStr sAttrSettings(XML_TAG_DETECTION_SETTINGS_NAME);
									pImageDomNode = pInfoNodeMap->getNamedItem(sAttrSettings.unicodeForm());
									if ( pImageDomNode )
									{
										// section string
										StrX sValAttr(pImageDomNode->getNodeValue());
										string strValAttr(sValAttr.getString());
										pInCmdReadSec->setSampleDetectionSettingsId(strSlotNum, strWell, strValAttr);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return true;
}

bool fillInCmdSecRoute(DOMElement* pXmlRootNode, InCmdSecRoute*& pInCmdSecRoute)
{
	if( ( !pXmlRootNode ) || ( !pInCmdSecRoute ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		std::string strNodeName(sNodeName.getString());
		if ( strNodeName.length() > 0 )
		{
			StrX sVal(l->item(i)->getTextContent());
			std::string strNodeVal(sVal.getString());

			if ( strNodeName.compare(XML_TAG_COMPONENT_NAME) == 0 )
			{
				// section involved in SecRoute
				pInCmdSecRoute->setComponent(strNodeVal);
			}
			else if ( strNodeName.compare(XML_TAG_COMMAND_NAME) == 0 )
			{
				// section command involved in SecRoute
				pInCmdSecRoute->setCommand(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdMonitorPressure(DOMElement* pXmlRootNode, InCmdMonitorPressure*& pInCmdMonitorPressure)
{
	if( ( !pXmlRootNode ) || ( !pInCmdMonitorPressure ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		std::string strNodeName(sNodeName.getString());
		if ( strNodeName.length() > 0 )
		{
			StrX sVal(l->item(i)->getTextContent());
			std::string strNodeVal(sVal.getString());

			if ( strNodeName.compare(XML_TAG_SECTION_NAME) == 0 )
			{
				// section involved in MonitorPressure
				pInCmdMonitorPressure->setAction(strNodeVal);
				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if(pNamedNodeMap)
				{
					DOMNode * pDomNode;
					XStr sAttr1(XML_ATTR_LETTER_NAME);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
					if(pDomNode)
					{
						// section string
						StrX sValAttr(pDomNode->getNodeValue());
						std::string strValAttr(sValAttr.getString());
						pInCmdMonitorPressure->setSection(strValAttr);
					}
				}
			}
		}
	}
	return true;
}

bool fillInCmdGetPosition(DOMElement* pXmlRootNode, InCmdGetPosition*& pInCmdGetPosition)
{
	if( ( !pXmlRootNode ) || ( !pInCmdGetPosition ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_COMPONENT_NAME) == 0)
			{
				pInCmdGetPosition->setComponent(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_MOTOR_NAME) == 0)
			{
				pInCmdGetPosition->setMotor(strNodeVal);
			}
			else
			{

			}
		}
	}
	return true;
}

bool fillInCmdGetCalibration(DOMElement* pXmlRootNode, InCmdGetCalibration*& pInCmdGetCalibration)
{
	if( ( !pXmlRootNode ) || ( !pInCmdGetCalibration ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_COMPONENT_NAME) == 0)
			{
				pInCmdGetCalibration->setComponent(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_MOTOR_NAME) == 0)
			{
				pInCmdGetCalibration->setMotor(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_LABEL_NAME) == 0)
			{
				pInCmdGetCalibration->setLabel(strNodeVal);
			}
			else
			{

			}
		}
	}
	return true;
}

bool fillInCmdGetParameter(DOMElement* pXmlRootNode, InCmdGetParameter*& pInCmdGetParameter)
{
	if( ( !pXmlRootNode ) || ( !pInCmdGetParameter ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_COMPONENT_NAME) == 0)
			{
				pInCmdGetParameter->setComponent(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_MOTOR_NAME) == 0)
			{
				pInCmdGetParameter->setMotor(strNodeVal);
			}
			else
			{

			}
		}
	}
	return true;
}

bool fillInCmdSaveCalibration(DOMElement* pXmlRootNode, InCmdSaveCalibration*& pInCmdSaveCalibration)
{
	structSaveCalibration	stSaveCalibration;
	if( ( !pXmlRootNode ) || ( !pInCmdSaveCalibration ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			if (strNodeName.compare(XML_TAG_CALIBRATION_NAME) == 0)
			{
				stSaveCalibration.strType = XML_TAG_CALIBRATION_NAME;
				stSaveCalibration.strValue.clear();
			}
			else if (strNodeName.compare(XML_TAG_POSITION_NAME) == 0)
			{
				stSaveCalibration.strType = XML_TAG_POSITION_NAME;

				StrX sVal(l->item(i)->getTextContent());
				string strNodeVal(sVal.getString());
				stSaveCalibration.strValue = strNodeVal;
			}
			else
			{
				continue;
			}

			DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
			if(pNamedNodeMap)
			{
				DOMNode * pDomNode;
				XStr sAttr1(XML_ATTR_COMPONENT_NAME);
				pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
				if (pDomNode)
				{
					StrX sValAttr(pDomNode->getNodeValue());
					string strValAttr(sValAttr.getString());
					stSaveCalibration.strComponent = strValAttr;

				}
				XStr sAttr2(XML_ATTR_MOTOR_NAME);
				pDomNode = pNamedNodeMap->getNamedItem(sAttr2.unicodeForm());
				if (pDomNode)
				{
					StrX sValAttr(pDomNode->getNodeValue());
					string strValAttr(sValAttr.getString());
					stSaveCalibration.strMotor = strValAttr;

				}
				XStr sAttr3(XML_ATTR_LABEL_NAME);
				pDomNode = pNamedNodeMap->getNamedItem(sAttr3.unicodeForm());
				if (pDomNode)
				{
					StrX sValAttr(pDomNode->getNodeValue());
					string strValAttr(sValAttr.getString());
					stSaveCalibration.strLabel = strValAttr;
				}
			}

			// fill the structure containing the calibration request parameters
			pInCmdSaveCalibration->setCalibration(stSaveCalibration);

		}
	}
	return true;
}

bool fillInCmdSetMotorActivation(DOMElement* pXmlRootNode, InCmdSetMotorActivation*& pInCmdSetMotorActivation)
{
	if( ( !pXmlRootNode ) || ( !pInCmdSetMotorActivation ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_COMPONENT_NAME) == 0)
			{
				pInCmdSetMotorActivation->setComponent(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_MOTOR_NAME) == 0)
			{
				pInCmdSetMotorActivation->setMotor(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_ACTIVATION_NAME) == 0)
			{
				pInCmdSetMotorActivation->setActivation(strNodeVal);
			}
			else
			{

			}
		}
	}
	return true;
}

bool fillInCmdInit(DOMElement* pXmlRootNode, InCmdInit*& pInCmdInit)
{
	if( ( !pXmlRootNode ) || ( !pInCmdInit ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_COMPONENT_NAME) == 0)
			{
				pInCmdInit->setComponent(strNodeVal);
			}
			else
			{

			}
		}
	}
	return true;
}

bool fillInCmdCaptureImage(DOMElement* pXmlRootNode, InCmdCaptureImage*& pInCmdCaptureImage)
{
	if ( pXmlRootNode == nullptr )			return false;
	if ( pInCmdCaptureImage == nullptr )	return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();
	int liImagesNum = 0;

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());

		if ( strNodeName.length() > 0 )
		{
			if ( ! strNodeName.compare(XML_TAG_ACTION_NAME) )
			{
				StrX sVal(l->item(i)->getTextContent());
				string strNodeVal(sVal.getString());
				pInCmdCaptureImage->setPictureAction(liImagesNum, strNodeVal);
			}
			else if ( ! strNodeName.compare(XML_TAG_IMAGE_NAME) )
			{
				DOMNamedNodeMap* pImageNodeMap = (l->item(i)->getAttributes());
				if ( pImageNodeMap )
				{
					DOMNode* pImageDomNode;
					XStr sAttr1(XML_ATTR_IMAGE_QUALITY_NAME);
					pImageDomNode = pImageNodeMap->getNamedItem(sAttr1.unicodeForm());
					if ( pImageDomNode )
					{
						// section string
						StrX sValAttr(pImageDomNode->getNodeValue());
						string strValAttr(sValAttr.getString());
						pInCmdCaptureImage->setImageQuality(liImagesNum, strValAttr);
					}

					XStr sAttr2(XML_ATTR_IMAGE_LIGHT_NAME);
					pImageDomNode = pImageNodeMap->getNamedItem(sAttr2.unicodeForm());
					if ( pImageDomNode )
					{
						// section string
						StrX sValAttr(pImageDomNode->getNodeValue());
						string strValAttr(sValAttr.getString());
						pInCmdCaptureImage->setImageLight(liImagesNum, strValAttr);
					}

					XStr sAttr3(XML_ATTR_IMAGE_FORMAT_NAME);
					pImageDomNode = pImageNodeMap->getNamedItem(sAttr3.unicodeForm());
					if ( pImageDomNode )
					{
						// section string
						StrX sValAttr(pImageDomNode->getNodeValue());
						string strValAttr(sValAttr.getString());
						pInCmdCaptureImage->setImageFormat(liImagesNum, strValAttr);
					}

					XStr sAttr4(XML_ATTR_IMAGE_FOCUS_NAME);
					pImageDomNode = pImageNodeMap->getNamedItem(sAttr4.unicodeForm());
					if ( pImageDomNode )
					{
						// section string
						StrX sValAttr(pImageDomNode->getNodeValue());
						string strValAttr(sValAttr.getString());
						pInCmdCaptureImage->setImageFocus(liImagesNum, strValAttr);
					}

					DOMNodeList* pImageIntraNodeList = (l->item(i)->getChildNodes());
					int liNumChilds = pImageIntraNodeList->getLength();
					for ( int i = 0; i != liNumChilds; ++i )
					{
						StrX sIntraNodeName(pImageIntraNodeList->item(i)->getNodeName());
						string strIntraNodeName(sIntraNodeName.getString());
						if ( strIntraNodeName.length() > 0 )
						{
							if ( ! strIntraNodeName.compare(XML_TAG_ROI_NAME) )
							{
								DOMNodeList* pImageInfoNodeList = (pImageIntraNodeList->item(i)->getChildNodes());
								int liNumInfo = pImageInfoNodeList->getLength();
								for ( int i = 0; i != liNumInfo; ++i )
								{
									StrX sInfoNodeName(pImageInfoNodeList->item(i)->getNodeName());
									string strInfoNodeName(sInfoNodeName.getString());
									if ( strInfoNodeName.length() > 0 )
									{
										if ( ! strInfoNodeName.compare(XML_TAG_TOP_NAME) )
										{
											StrX sVal(pImageInfoNodeList->item(i)->getTextContent());
											string strNodeVal(sVal.getString());
											pInCmdCaptureImage->setTopVal(liImagesNum, strNodeVal);
										}
										else if ( ! strInfoNodeName.compare(XML_TAG_LEFT_NAME) )
										{
											StrX sVal(pImageInfoNodeList->item(i)->getTextContent());
											string strNodeVal(sVal.getString());
											pInCmdCaptureImage->setLeftVal(liImagesNum, strNodeVal);
										}
										else if ( ! strInfoNodeName.compare(XML_TAG_BOTTOM_NAME) )
										{
											StrX sVal(pImageInfoNodeList->item(i)->getTextContent());
											string strNodeVal(sVal.getString());
											pInCmdCaptureImage->setBottomVal(liImagesNum, strNodeVal);
										}
										else if ( ! strInfoNodeName.compare(XML_TAG_RIGHT_NAME) )
										{
											StrX sVal(pImageInfoNodeList->item(i)->getTextContent());
											string strNodeVal(sVal.getString());
											pInCmdCaptureImage->setRightVal(liImagesNum, strNodeVal);
										}
									}
								}
							}
                            else if ( ! strIntraNodeName.compare(XML_TAG_PREDEFINED_ROI_NAME) )
                            {
                                DOMNamedNodeMap* pRoiNodeMap = (pImageIntraNodeList->item(i)->getAttributes());
                                if ( pRoiNodeMap )
                                {
                                    DOMNode* pRoiDomNode;
                                    XStr sRoiAttr(XML_TAG_NAME_NAME);
                                    pRoiDomNode = pRoiNodeMap->getNamedItem(sRoiAttr.unicodeForm());
                                    if ( pRoiDomNode )
									{
                                        StrX sValRoiAttr(pRoiDomNode->getNodeValue());
                                        string strValRoiAttr(sValRoiAttr.getString());
                                        pInCmdCaptureImage->setPredefinedRoi(liImagesNum, strValRoiAttr);
                                    }

									XStr sEnableCrop(XML_ATTR_CROP_ENABLE_NAME);
									pRoiDomNode = pRoiNodeMap->getNamedItem(sEnableCrop.unicodeForm());
									if ( pRoiDomNode )
									{
										StrX sValEnableAttr(pRoiDomNode->getNodeValue());
										string strValEnableAttr(sValEnableAttr.getString());
										pInCmdCaptureImage->setCropEnabled(liImagesNum, strValEnableAttr);
									}
                                }
                            }
							else if ( ! strIntraNodeName.compare(XML_TAG_MOTOR_POSITIONS_NAME) )
							{
								DOMNodeList* pInfoNodeList = (pImageIntraNodeList->item(i)->getChildNodes());
								int liNumInfo = pInfoNodeList->getLength();
								for ( int i = 0; i != liNumInfo; ++i )
								{
									StrX sInfoNodeName(pInfoNodeList->item(i)->getNodeName());
									string strInfoNodeName(sInfoNodeName.getString());
									if ( strInfoNodeName.length() > 0 )
									{
										if ( ! strInfoNodeName.compare(XML_TAG_POSITION_NAME) )
										{
											DOMNamedNodeMap* pInfoNodeMap = (pInfoNodeList->item(i)->getAttributes());
											if ( pInfoNodeMap )
											{
												DOMNode* pImageDomNode;
												XStr sAttr4(XML_ATTR_MOTOR_NAME);
												pImageDomNode = pInfoNodeMap->getNamedItem(sAttr4.unicodeForm());
												if ( pImageDomNode )
												{
													// section string
													StrX sValAttr(pImageDomNode->getNodeValue());
													string strValAttr(sValAttr.getString());
													pInCmdCaptureImage->setMotor(liImagesNum, strValAttr);
												}

												XStr sAttr5(XML_ATTR_COMPONENT_NAME);
												pImageDomNode = pInfoNodeMap->getNamedItem(sAttr5.unicodeForm());
												if ( pImageDomNode )
												{
													// section string
													StrX sValAttr(pImageDomNode->getNodeValue());
													string strValAttr(sValAttr.getString());
													pInCmdCaptureImage->setComponent(liImagesNum, strValAttr);
												}

												StrX sVal(pInfoNodeList->item(i)->getTextContent());
												string strNodeVal(sVal.getString());
												pInCmdCaptureImage->setAttribute(liImagesNum, strNodeVal);
											}
										}
									}
								}
							}
						}
					}
					liImagesNum++;
				}
			}
			else
			{

			}
		}
		pInCmdCaptureImage->setPicturesNum(liImagesNum);
		// Done
	}
	return true;
}

bool fillInCmdRunwl(DOMElement* pXmlRootNode, InCmdRunwl*& pInCmdRunwl)
{
	int8_t protoCounter = -1;

	if( ( !pXmlRootNode ) || ( !pInCmdRunwl ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());

			// Protocol definition
			if (strNodeName.compare(XML_TAG_PROTOCOL) == 0)
			{
				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if(pNamedNodeMap)
				{
					DOMNode * pDomNode;
					XStr sAttr1(XML_TAG_PROTO_ID);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
					if (pDomNode)
					{
						++protoCounter;

						StrX sValAttr(pDomNode->getNodeValue());
						string strValAttr(sValAttr.getString());
						pInCmdRunwl->setProtocolInfoId(protoCounter, strValAttr);
					}
				}

				DOMNodeList* p = l->item(i)->getChildNodes();
				int lNumSubChild = p->getLength();
				for ( int lcount = 0; lcount < lNumSubChild; lcount++ )
				{
					StrX sSubNodeName(p->item(lcount)->getNodeName());
					string strSubNodeName(sSubNodeName.getString());
					if (strSubNodeName.length() > 0)
					{
						StrX sSubVal(p->item(lcount)->getTextContent());
						string strSubNodeVal(sSubVal.getString());

						if (strSubNodeName.compare(XML_TAG_PROTO_NAME) == 0)
						{
							pInCmdRunwl->setProtocolInfoName(protoCounter, strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_PROTO_GLOBALS) == 0)
						{
							pInCmdRunwl->setProtoGlobals(protoCounter, strSubNodeVal.c_str());
						}
						else if (strSubNodeName.compare(XML_TAG_PROTO_LOCALS) == 0)
						{
							pInCmdRunwl->setProtoLocals(protoCounter, strSubNodeVal.c_str());
						}
						else if (strSubNodeName.compare(XML_TAG_PROTO_READINGS) == 0)
						{

							DOMNodeList* q = p->item(lcount)->getChildNodes();
							int lNumSubSubChild = q->getLength();

							for ( int lSubCount = 0; lSubCount < lNumSubSubChild; lSubCount++ )
							{
								StrX sSubSubNodeName(q->item(lSubCount)->getNodeName());
								string strSubSubNodeName(sSubSubNodeName.getString());

								if (strSubSubNodeName.length() > 0)
								{
									StrX sSubSubVal(q->item(lSubCount)->getTextContent());
									if (strSubSubNodeName.compare(XML_TAG_PROTO_READING) == 0)
									{
										int8_t indexReading = -1;

										DOMNamedNodeMap * pNamedSubNodeMap = (q->item(lSubCount)->getAttributes());
										if(pNamedSubNodeMap)
										{
											DOMNode * pDomNode;
											XStr sAttr1(XML_TAG_PROTO_INDEX);
											pDomNode = pNamedSubNodeMap->getNamedItem(sAttr1.unicodeForm());
											if (pDomNode)
											{
												// index of the Reading Phase
												StrX sValAttr(pDomNode->getNodeValue());
												string strValAttr(sValAttr.getString());
												indexReading = atoi(strValAttr.c_str());
											}
										}


										if(indexReading >= 0)
										{

											DOMNodeList* s = q->item(lSubCount)->getChildNodes();
											int lNumSubSubSubChild = s->getLength();

											for ( int llSubCount = 0; llSubCount < lNumSubSubSubChild; llSubCount++ )
											{
												StrX sSubSubSubNodeName(s->item(llSubCount)->getNodeName());
												string strSubSubSubNodeName(sSubSubSubNodeName.getString());

												if (strSubSubSubNodeName.length() > 0)
												{
													StrX sSubSubSubVal(s->item(llSubCount)->getTextContent());
													string strSubSubSubNodeVal(sSubSubSubVal.getString());

													if (strSubSubSubNodeName.compare(XML_TAG_PROTO_MIN) == 0)
													{
														pInCmdRunwl->setProtoMinMaxJ(protoCounter,
																					 atoi(strSubSubSubNodeVal.c_str()),
																					 indexReading, 0);
													}
													else if (strSubSubSubNodeName.compare(XML_TAG_PROTO_MAX) == 0)
													{
														pInCmdRunwl->setProtoMinMaxJ(protoCounter,
																					 atoi(strSubSubSubNodeVal.c_str()),
																					 indexReading, 1);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			// Acquisition settings definition
			else if (strNodeName.compare(XML_TAG_PROTO_SETTINGS) == 0)
			{
				std::string acquisitionID;
				acquisitionID.clear();

				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if(pNamedNodeMap)
				{
					DOMNode * pDomNode;
					XStr sAttr1(XML_TAG_PROTO_ID);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
					if (pDomNode)
					{
						// assign the AcquisitionSettings reference string
						StrX sValAttr(pDomNode->getNodeValue());
						acquisitionID.assign(sValAttr.getString());
					}
				}

				if(acquisitionID.empty() == false)
				{
					// the string has to be decoded rbase64
					std::string strDecoded;
					if(decodeDataBase64(strNodeVal, strDecoded))
					{
						// now we have another XML structure to parser:
						// to not corrupt the actual XML copy to the structure, we will decode it later
						pInCmdRunwl->addPressureSettings(acquisitionID, strDecoded);
					}
				}

			}
			// Running per each section
			else if (strNodeName.compare(XML_TAG_SECTION_NAME) == 0)
			{
				uint8_t sectionIndex = SCT_NUM_TOT_SECTIONS;

				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if(pNamedNodeMap)
				{
					DOMNode * pDomNode;
					XStr sAttr1(XML_TAG_PROTO_LETTER);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
					if (pDomNode)
					{
						StrX sValAttr(pDomNode->getNodeValue());
						string strValAttr(sValAttr.getString());
						// assign the sectionIndex that will be used to set values
						if(strcmp(strValAttr.c_str(), XML_VALUE_SECTION_A) == 0)
						{
							sectionIndex = SCT_A_ID;
						}
						else if(strcmp(strValAttr.c_str(), XML_VALUE_SECTION_B) == 0)
						{
							sectionIndex = SCT_B_ID;
						}
						pInCmdRunwl->setSection(sectionIndex, strValAttr);
						pInCmdRunwl->setSectionEnabled(sectionIndex);
					}
				}

				DOMNodeList* p = l->item(i)->getChildNodes();
				int lNumSubChild = p->getLength();
				for ( int lcount = 0; lcount < lNumSubChild; lcount++ )
				{
					StrX sSubNodeName(p->item(lcount)->getNodeName());
					string strSubNodeName(sSubNodeName.getString());
					if (strSubNodeName.length() > 0)
					{
						StrX sSubVal(p->item(lcount)->getTextContent());
						string strSubNodeVal(sSubVal.getString());

						if (strSubNodeName.compare(XML_TAG_PROTOCOL) == 0)
						{
							pInCmdRunwl->setSectionProtoId(sectionIndex, strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_PROTO_MONITORING) == 0)
						{
							DOMNodeList* q = p->item(lcount)->getChildNodes();
							int lNumSubSubChild = q->getLength();

							for ( int llcount = 0; llcount < lNumSubSubChild; llcount++ )
							{
								StrX sSubSubNodeName(q->item(llcount)->getNodeName());
								string strSubSubNodeName(sSubSubNodeName.getString());
								if (strSubSubNodeName.length() > 0)
								{
									StrX sSubSubVal(q->item(llcount)->getTextContent());
									string strSubSubNodeVal(sSubSubVal.getString());

									if (strSubSubNodeName.compare(XML_TAG_PROTO_TEMPERATURE) == 0)
									{
										pInCmdRunwl->setSectionMonitoringTemperature(sectionIndex, atoi(strSubSubNodeVal.c_str()));
									}
									else if (strSubSubNodeName.compare(XML_TAG_PROTO_AUTOCHECK) == 0)
									{
										pInCmdRunwl->setSectionMonitoringAutocheck(sectionIndex, atoi(strSubSubNodeVal.c_str()));
									}
								}
							}
						}
						else if (strSubNodeName.compare(XML_TAG_PROTO_SLOT) == 0)
						{
							int indexStrip = SCT_NUM_TOT_SLOTS;
							DOMNamedNodeMap * pNamedNodeMap = (p->item(lcount)->getAttributes());
							if(pNamedNodeMap)
							{
								DOMNode * pDomNode;
								XStr sAttr1(XML_TAG_PROTO_NUMBER);
								pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
								if (pDomNode)
								{
									StrX sValAttr(pDomNode->getNodeValue());
									string strValAttr(sValAttr.getString());
									// NOTE the command enums 1 - 6, the software 0 -5
									indexStrip = atoi(strValAttr.c_str()) - 1;
								}
							}

							if(indexStrip != SCT_NUM_TOT_SLOTS)
							{
								DOMNodeList* q = p->item(lcount)->getChildNodes();
								int lNumSubSubChild = q->getLength();

								for ( int llcount = 0; llcount < lNumSubSubChild; llcount++ )
								{
									StrX sSubSubNodeName(q->item(llcount)->getNodeName());
									string strSubSubNodeName(sSubSubNodeName.getString());
									if (strSubSubNodeName.length() > 0)
									{
										StrX sSubSubVal(q->item(llcount)->getTextContent());
										string strSubSubNodeVal(sSubSubVal.getString());
										if (strSubSubNodeName.compare(XML_TAG_PROTO_ID) == 0)
										{
											pInCmdRunwl->setSectionStripId(sectionIndex, indexStrip, strSubSubNodeVal);
										}
										else if (strSubSubNodeName.compare(XML_TAG_PROTO_PROFILE) == 0)
										{
											pInCmdRunwl->setSectionStripLiquidProfile(sectionIndex, indexStrip, strSubSubNodeVal);
										}
										else if (strSubSubNodeName.compare(XML_TAG_PROTO_CHECK) == 0)
										{
											pInCmdRunwl->setSectionStripPressureProfile(sectionIndex, indexStrip, strSubSubNodeVal);
										}
										else if (strSubSubNodeName.compare(XML_TAG_PROTO_RFU) == 0)
										{
                                            pInCmdRunwl->setSectionStripRfuconv(sectionIndex, indexStrip, atoi(strSubSubNodeVal.c_str()));
										}
										else if (strSubSubNodeName.compare(XML_TAG_PROTO_SETTINGS) == 0)
										{
											pInCmdRunwl->setSectionStripSettings(sectionIndex, indexStrip, strSubSubNodeVal);
										}
										else if (strSubSubNodeName.compare(XML_TAG_PROTO_MEASUREAIR) == 0)
										{
											pInCmdRunwl->setSectionStripMeasureAir(sectionIndex, indexStrip, atoi(strSubSubNodeVal.c_str()));
										}
										else if (strSubSubNodeName.compare(XML_TAG_PROTO_MONITORING) == 0)
										{
											DOMNodeList* s = q->item(llcount)->getChildNodes();
											int lNumSubSubChild = s->getLength();

											for ( int lllcount = 0; lllcount < lNumSubSubChild; lllcount++ )
											{
												StrX sSubSubSubNodeName(s->item(lllcount)->getNodeName());
												string strSubSubSubNodeName(sSubSubSubNodeName.getString());
												if (strSubSubSubNodeName.length() > 0)
												{
													StrX sSubSubSubVal(s->item(lllcount)->getTextContent());
													string strSubSubSubNodeVal(sSubSubSubVal.getString());

													if (strSubSubSubNodeName.compare(XML_TAG_PROTO_TREATED) == 0)
													{
														pInCmdRunwl->setSectionStripTreatedPressure(sectionIndex, indexStrip, atoi(strSubSubSubNodeVal.c_str()));
													}
													else if (strSubSubSubNodeName.compare(XML_TAG_PROTO_ELABORATED) == 0)
													{
														pInCmdRunwl->setSectionStripElaboratedPressure(sectionIndex, indexStrip, atoi(strSubSubSubNodeVal.c_str()));
													}
												}
											}
										}
									}
								}
							}
						}
						else if (strSubNodeName.compare(XML_TAG_PROTO_CONSTRAINT) == 0)
						{
							DOMNodeList* q = p->item(lcount)->getChildNodes();
							int lNumSubSubChild = q->getLength();

							for ( int llcount = 0; llcount < lNumSubSubChild; llcount++ )
							{
								StrX sSubSubNodeName(q->item(llcount)->getNodeName());
								string strSubSubNodeName(sSubSubNodeName.getString());
								if (strSubSubNodeName.length() > 0)
								{
									StrX sSubSubVal(q->item(llcount)->getTextContent());
									string strSubSubNodeVal(sSubSubVal.getString());
									if (strSubSubNodeName.compare(XML_TAG_PROTO_CONSTRAINT_UNIT) == 0)
									{
										pInCmdRunwl->setSectionUnitConstraint(sectionIndex, strSubSubNodeVal);
									}
									else if (strSubSubNodeName.compare(XML_TAG_PROTO_CONSTRAINT_MIN) == 0)
									{
										pInCmdRunwl->setSectionMinConstraint(sectionIndex, atoi(strSubSubNodeVal.c_str()));
									}
									else if (strSubSubNodeName.compare(XML_TAG_PROTO_CONSTRAINT_MAX) == 0)
									{
										pInCmdRunwl->setSectionMaxConstraint(sectionIndex, atoi(strSubSubNodeVal.c_str()));

									}
								}
							}
						}
					}
				}
			}
		}
	}
	return true;
}

bool fillInCmdCancelSection(DOMElement* pXmlRootNode, InCmdCancelSection*& pInCmdCancelSection)
{
	if( ( !pXmlRootNode ) || ( !pInCmdCancelSection ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_SECTION_NAME) == 0)
			{
				pInCmdCancelSection->setSection(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdGetMotorActivation(DOMElement* pXmlRootNode, InCmdGetMotorActivation*& pInCmdGetMotorActivation)
{
	if( ( !pXmlRootNode ) || ( !pInCmdGetMotorActivation ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_COMPONENT_NAME) == 0)
			{
				pInCmdGetMotorActivation->setComponent(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_MOTOR_NAME) == 0)
			{
				pInCmdGetMotorActivation->setMotor(strNodeVal);
			}
			else
			{

			}
		}
	}
	return true;
}

bool fillInCmdTrayRead(DOMElement* pXmlRootNode, InCmdTrayRead*& pInCmdTrayRead)
{
	if( ( !pXmlRootNode ) || ( !pInCmdTrayRead ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_SECTION_NAME) == 0)
			{
				pInCmdTrayRead->setSection(strNodeVal);
			}
			else
			{

			}
		}
	}
	return true;
}

bool fillInCmdGetPressureOffsets(DOMElement* pXmlRootNode, InCmdGetPressureOffsets*& pInCmdGetPressureOffsets)
{
	if( ( !pXmlRootNode ) || ( !pInCmdGetPressureOffsets ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_SECTION_NAME) == 0)
			{
				pInCmdGetPressureOffsets->setSection(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdGetPressureSettings(DOMElement* pXmlRootNode, InCmdGetPressureSettings*& pInCmdGetPressureSettings)
{
	if( ( !pXmlRootNode ) || ( !pInCmdGetPressureSettings ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_SECTION_NAME) == 0)
			{
				pInCmdGetPressureSettings->setSection(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdSetMaintenanceMode(DOMElement* pXmlRootNode, InCmdSetMaintenanceMode*& pInCmdSetMaintenanceMode)
{
	if( ( !pXmlRootNode ) || ( !pInCmdSetMaintenanceMode ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_ACTIVATION_NAME) == 0)
			{
				pInCmdSetMaintenanceMode->setActivation(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdSHCalibrate(DOMElement* pXmlRootNode, InCmdSHCalibrate*& pInCmdSHCalibrate)
{
	if ( ! pXmlRootNode )				return false;
	if ( ! pInCmdSHCalibrate )	return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for (int i = 0; i < liNumRootChildren; i++)
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if ( strNodeName.length() > 0 )
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());

			if ( ! strNodeName.compare(XML_TAG_SECTION_NAME) )
			{
				pInCmdSHCalibrate->setSection(strNodeVal);
			}
			else if ( ! strNodeName.compare(XML_TAG_SLOT_NAME) )
			{
				pInCmdSHCalibrate->setSlot(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdSetSerial(DOMElement* pXmlRootNode, InCmdSetSerial*& pInCmdSetSerial)
{
	if( ( !pXmlRootNode ) || ( !pInCmdSetSerial ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_INSTRUMENT_NAME) == 0)
			{
				pInCmdSetSerial->setSerialInstrument(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_NSH_NAME) == 0)
			{
				pInCmdSetSerial->setSerialNSH(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_MASTERBOARD_NAME) == 0)
			{
				pInCmdSetSerial->setSerialMasterboard(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_SECTIONA_NAME) == 0)
			{
				pInCmdSetSerial->setSerialSectionA(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_SECTIONB_NAME) == 0)
			{
				pInCmdSetSerial->setSerialSectionB(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdSetTime(DOMElement* pXmlRootNode, InCmdSetTime*& pInCmdSetTime)
{
	if( ( !pXmlRootNode ) || ( !pInCmdSetTime ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_TIMESTAMP_NAME) == 0)
			{
				pInCmdSetTime->setTimeString(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdOPTCalibrate(DOMElement* pXmlRootNode, InCmdOPTCalibrate*& pInCmdOPTCalibrate)
{
	if ( ! pXmlRootNode )		return false;
	if ( ! pInCmdOPTCalibrate ) return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());

			if (strNodeName.compare(XML_TAG_SECTION_NAME) == 0)
			{
				pInCmdOPTCalibrate->setSection(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_SLOT_NAME) == 0)
			{
				pInCmdOPTCalibrate->setSlot(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_TARGET_NAME) == 0)
			{
				pInCmdOPTCalibrate->setTarget(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdOPTCheck(DOMElement *pXmlRootNode, InCmdOPTCheck *&pInCmdOPTCheck)
{
    if ( ! pXmlRootNode )	return false;
    if ( ! pInCmdOPTCheck ) return false;

    DOMNodeList* l = pXmlRootNode->getChildNodes();
    int liNumRootChildren = l->getLength();

    for ( int i = 0; i < liNumRootChildren; i++ )
    {
        StrX sNodeName(l->item(i)->getNodeName());
        string strNodeName(sNodeName.getString());
        if (strNodeName.length() > 0)
        {
            StrX sVal(l->item(i)->getTextContent());
            string strNodeVal(sVal.getString());

            if (strNodeName.compare(XML_TAG_SLOT_NAME) == 0)
            {
                std::string strSection;

                DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
                if(pNamedNodeMap)
                {
                    DOMNode * pDomNode;
                    XStr sAttr1(XML_TAG_SECTION_NAME);
                    pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
                    if(pDomNode)
                    {
                        // section string
                        StrX sValAttr(pDomNode->getNodeValue());
                        std::string strValAttr(sValAttr.getString());
                        strSection.assign(strValAttr);
                    }
                }

                if(strSection.empty() == false)
                {
                    pInCmdOPTCheck->setSlot(strSection, strNodeVal);
                }

            }
            else if (strNodeName.compare(XML_TAG_RFU_TARGET_OPT) == 0)
            {
                pInCmdOPTCheck->setTarget(atoi(strNodeVal.c_str()));
            }
            else if (strNodeName.compare(XML_TAG_RFU_TOLERANCE_OPT) == 0)
            {
                pInCmdOPTCheck->setTolerance(atoi(strNodeVal.c_str()));
            }
        }
    }
    return true;
}

bool fillInCmdAirCalibrate(DOMElement* pXmlRootNode, InCmdAirCalibrate*& pInCmdAirCalibrate)
{
	if ( ! pXmlRootNode )		return false;
	if ( ! pInCmdAirCalibrate ) return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_TARGET_NAME) == 0)
			{
				pInCmdAirCalibrate->setTarget(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdDisable(DOMElement* pXmlRootNode, InCmdDisable*& pInCmdDisable)
{
	if( ( !pXmlRootNode ) || ( !pInCmdDisable ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_INSTRUMENT_NAME) == 0)
			{
				pInCmdDisable->setDisableInstrument(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_NSH_NAME) == 0)
			{
				pInCmdDisable->setDisableNSH(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_CAMERA_NAME) == 0)
			{
				pInCmdDisable->setDisableCamera(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_SECTION_NAME) == 0)
			{
				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if(pNamedNodeMap)
				{
					DOMNode * pDomNode;
					XStr sAttr1(XML_ATTR_LETTER_NAME);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
					if(pDomNode)
					{
						// section string
						StrX sValAttr(pDomNode->getNodeValue());
						std::string strValAttr(sValAttr.getString());
						pInCmdDisable->setDisableSection(strValAttr, strNodeVal);
					}
				}
			}
		}
	}
	return true;
}

bool fillInCmdSleep(DOMElement* pXmlRootNode, InCmdSleep*& pInCmdSleep)
{
	if( ( !pXmlRootNode ) || ( !pInCmdSleep ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_INSTRUMENT_NAME) == 0)
			{
				pInCmdSleep->setSleepInstrument(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdShutdown(DOMElement* pXmlRootNode, InCmdShutdown*& pInCmdShutdown)
{
	if( ( !pXmlRootNode ) || ( !pInCmdShutdown ) )
		return false;

	return true;
}

bool fillInCmdSetGlobalSettings(DOMElement* pXmlRootNode, InCmdSetGlobalSettings*& pInCmdGlobalSettings)
{
	if( ( !pXmlRootNode ) || ( !pInCmdGlobalSettings ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{

			if (strNodeName.compare(XML_ATTR_FORCE_NAME) == 0)
			{
				std::string strForce;
				bool bForce = false;

				StrX sSubVal(l->item(i)->getTextContent());
				strForce.assign(sSubVal.getString());

				if(strForce.compare(XML_VALUE_TRUE))
				{
					bForce = true;
				}

				pInCmdGlobalSettings->setForceTemperature(bForce);

			}
            else if (strNodeName.compare(XML_TAG_PROTO_DURATION_NAME) == 0)
            {
                std::string strDuration;

                StrX sSubVal(l->item(i)->getTextContent());
                strDuration.assign(sSubVal.getString());

                pInCmdGlobalSettings->setDuration((uint16_t)atoi(strDuration.c_str()));
            }
            else if (strNodeName.compare(XML_TAG_SPRBLOCK_NAME) == 0)
			{
				std::string strSection;
				strSection.clear();

				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if(pNamedNodeMap)
				{
					DOMNode * pDomNode;
					XStr sAttr1(XML_TAG_SECTION_NAME);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
					if(pDomNode)
					{
						// section reference
						StrX sValAttr(pDomNode->getNodeValue());
						strSection.assign(sValAttr.getString());
					}
				}

				std::string strMin, strMax, strTarget, strTolerance;
				strMin.clear();	strMax.clear();	strTarget.clear();	strTolerance.clear();

				DOMNodeList* p = l->item(i)->getChildNodes();
				int lNumSubChild = p->getLength();

				for ( int lcount = 0; lcount < lNumSubChild; lcount++ )
				{
					StrX sSubNodeName(p->item(lcount)->getNodeName());
					string strSubNodeName(sSubNodeName.getString());

					StrX sSubVal(p->item(lcount)->getTextContent());

					if (strSubNodeName.length() > 0)
					{
						if (strSubNodeName.compare(XML_TAG_TEMP_MIN_NAME) == 0)
						{
							strMin.assign(sSubVal.getString());
						}
						else if (strSubNodeName.compare(XML_TAG_TEMP_MAX_NAME) == 0)
						{
							strMax.assign(sSubVal.getString());
						}
						else if (strSubNodeName.compare(XML_TAG_TEMP_TARGET_NAME) == 0)
						{
							strTarget.assign(sSubVal.getString());
						}
						else if (strSubNodeName.compare(XML_TAG_TEMP_TOLERANCE_NAME) == 0)
						{
							strTolerance.assign(sSubVal.getString());
						}
					}
				}

				uint8_t firstSection = 0, lastSection = 0;
				uint8_t section = SCT_NONE_ID;

				if(strSection.empty())
				{
					firstSection = SCT_A_ID;
					lastSection = SCT_NUM_TOT_SECTIONS;
				}
				else if(strSection.compare(XML_VALUE_SECTION_A) == 0)
				{
					firstSection = SCT_A_ID;
					lastSection = SCT_A_ID + 1;
				}
				else if(strSection.compare(XML_VALUE_SECTION_B) == 0)
				{
					firstSection = SCT_B_ID;
					lastSection = SCT_B_ID + 1;
				}

				for(section = firstSection; section < lastSection; section++)
				{
					if(strMin.empty() == false)
						pInCmdGlobalSettings->setSprMinTemperature(section, atoi(strMin.c_str()));

					if(strMax.empty() == false)
						pInCmdGlobalSettings->setSprMaxTemperature(section, atoi(strMax.c_str()));

					if(strTarget.empty() == false)
						pInCmdGlobalSettings->setSprTargetTemperature(section, atoi(strTarget.c_str()));

					if(strTolerance.empty() == false)
						pInCmdGlobalSettings->setSprToleranceTemperature(section, atoi(strTolerance.c_str()));
				}

			}
			else if (strNodeName.compare(XML_TAG_TRAYBLOCK_NAME) == 0)
			{
				std::string strSection;
				strSection.clear();

				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if(pNamedNodeMap)
				{
					DOMNode * pDomNode;
					XStr sAttr1(XML_TAG_SECTION_NAME);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
					if(pDomNode)
					{
						// section reference
						StrX sValAttr(pDomNode->getNodeValue());
						strSection.assign(sValAttr.getString());
					}
				}

				std::string strMin, strMax, strTarget, strTolerance;
				strMin.clear();	strMax.clear();	strTarget.clear();	strTolerance.clear();

				DOMNodeList* p = l->item(i)->getChildNodes();
				int lNumSubChild = p->getLength();

				for ( int lcount = 0; lcount < lNumSubChild; lcount++ )
				{
					StrX sSubNodeName(p->item(lcount)->getNodeName());
					string strSubNodeName(sSubNodeName.getString());

					StrX sSubVal(p->item(lcount)->getTextContent());

					if (strSubNodeName.length() > 0)
					{
						if (strSubNodeName.compare(XML_TAG_TEMP_MIN_NAME) == 0)
						{
							strMin.assign(sSubVal.getString());
						}
						else if (strSubNodeName.compare(XML_TAG_TEMP_MAX_NAME) == 0)
						{
							strMax.assign(sSubVal.getString());
						}
						else if (strSubNodeName.compare(XML_TAG_TEMP_TARGET_NAME) == 0)
						{
							strTarget.assign(sSubVal.getString());
						}
						else if (strSubNodeName.compare(XML_TAG_TEMP_TOLERANCE_NAME) == 0)
						{
							strTolerance.assign(sSubVal.getString());
						}
					}
				}

				uint8_t firstSection = 0, lastSection = 0;
				uint8_t section = SCT_NONE_ID;

				if(strSection.empty())
				{
					firstSection = SCT_A_ID;
					lastSection = SCT_NUM_TOT_SECTIONS;
				}
				else if(strSection.compare(XML_VALUE_SECTION_A) == 0)
				{
					firstSection = SCT_A_ID;
					lastSection = SCT_A_ID + 1;
				}
				else if(strSection.compare(XML_VALUE_SECTION_B) == 0)
				{
					firstSection = SCT_B_ID;
					lastSection = SCT_B_ID + 1;
				}

				for(section = firstSection; section < lastSection; section++)
				{
					if(strMin.empty() == false)
						pInCmdGlobalSettings->setTrayMinTemperature(section, atoi(strMin.c_str()));

					if(strMax.empty() == false)
						pInCmdGlobalSettings->setTrayMaxTemperature(section, atoi(strMax.c_str()));

					if(strTarget.empty() == false)
						pInCmdGlobalSettings->setTrayTargetTemperature(section, atoi(strTarget.c_str()));

					if(strTolerance.empty() == false)
						pInCmdGlobalSettings->setTrayToleranceTemperature(section, atoi(strTolerance.c_str()));
				}

			}
			else if (strNodeName.compare(XML_TAG_INTERNAL_NAME) == 0)
			{
				DOMNodeList* p = l->item(i)->getChildNodes();
				int lNumSubChild = p->getLength();

				for ( int lcount = 0; lcount < lNumSubChild; lcount++ )
				{
					StrX sSubNodeName(p->item(lcount)->getNodeName());
					string strSubNodeName(sSubNodeName.getString());

					StrX sSubVal(p->item(lcount)->getTextContent());
					string strSubNodeVal(sSubVal.getString());

					if (strSubNodeName.length() > 0)
					{
						if (strSubNodeName.compare(XML_TAG_TEMP_MIN_NAME) == 0)
						{
							pInCmdGlobalSettings->setInternalMinTemperature(atoi(strSubNodeVal.c_str()));
						}
						else if (strSubNodeName.compare(XML_TAG_TEMP_MAX_NAME) == 0)
						{
							pInCmdGlobalSettings->setInternalMaxTemperature(atoi(strSubNodeVal.c_str()));
						}
					}
				}
			}
		}
	}
	return true;
}

bool fillInCmdUnknown(DOMElement* pXmlRootNode, InCmdUnknown*& pInCmdUnknown, std::string strCmd)
{
	if( ( !pXmlRootNode ) || ( !pInCmdUnknown ) )
		return false;

	size_t pos = 0;
	std::string token;

	token.assign(WEB_CMD_VIDAS_MODULAR_NS);
	if((pos = strCmd.find(WEB_CMD_VIDAS_MODULAR_NS)) != string::npos)
	{
		strCmd = strCmd.substr(pos + token.size());
	}

	// set the command string in input
	pInCmdUnknown->setCommandString(strCmd);
	return true;
}

bool fillInCmdSignalRead(DOMElement* pXmlRootNode, InCmdSignalRead*& pInCmdSignalRead)
{
	if ( ! pXmlRootNode )		return false;
	if ( ! pInCmdSignalRead ) return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_PROTO_REPETITIONS_NAME) == 0)
			{
				pInCmdSignalRead->setRepetitions(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_PROTO_INTERVAL_NAME) == 0)
			{
				pInCmdSignalRead->setInterval(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdSHForceLed(DOMElement* pXmlRootNode, InCmdSHForceLed*& pInCmdSHForceLed)
{
	if ( ! pXmlRootNode )		return false;
	if ( ! pInCmdSHForceLed ) return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_PROTO_DURATION_NAME) == 0)
			{
				pInCmdSHForceLed->setDuration(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdGetCameraRoi(DOMElement* pXmlRootNode, InCmdGetCameraRoi*& pInCmdGetCameraRoi)
{
	if ( ! pXmlRootNode )		return false;
	if ( ! pInCmdGetCameraRoi ) return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if ( strNodeName.length() > 0 )
		{
			if ( strNodeName.compare(XML_TAG_ROI_NAME) == 0 )
			{
				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if ( pNamedNodeMap )
				{
					DOMNode* pDomNode;
					XStr sAttr1(XML_TAG_PROTO_NAME);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
					if ( pDomNode )
					{
						// strip barcode acquisition
						StrX sValAttr(pDomNode->getNodeValue());
						string strValAttr(sValAttr.getString());
						pInCmdGetCameraRoi->setCurrentTarget(strValAttr);
					}
				}
			}
		}
	}

	return true;
}

bool fillInCmdSaveCameraRoi(DOMElement* pXmlRootNode, InCmdSaveCameraRoi*& pInCmdSaveCameraRoi)
{
	if ( ! pXmlRootNode )		return false;
	if ( ! pInCmdSaveCameraRoi ) return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if ( strNodeName.length() > 0 )
		{
			if ( strNodeName.compare(XML_TAG_ROI_NAME) == 0 )
			{
				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if ( pNamedNodeMap )
				{
					DOMNode* pDomNode;
					XStr sAttr1(XML_TAG_PROTO_NAME);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
					if ( pDomNode )
					{
						// strip barcode acquisition
						StrX sValAttr(pDomNode->getNodeValue());
						string strValAttr(sValAttr.getString());
						pInCmdSaveCameraRoi->setTarget(strValAttr);
					}
				}
				DOMNodeList* childL = l->item(i)->getChildNodes();
				int liNumChildren = childL->getLength();
				for ( int i = 0; i < liNumChildren; i++ )
				{
					StrX sNodeName(childL->item(i)->getNodeName());
					string strNodeName(sNodeName.getString());
					if ( strNodeName.length() > 0 )
					{
						StrX sVal(childL->item(i)->getTextContent());
						string strNodeVal(sVal.getString());
						if ( strNodeName.compare(XML_TAG_TOP_NAME) == 0 )
						{
							pInCmdSaveCameraRoi->setTop(strNodeVal);
						}
						else if ( strNodeName.compare(XML_TAG_BOTTOM_NAME) == 0 )
						{
							pInCmdSaveCameraRoi->setBottom(strNodeVal);
						}
						else if ( strNodeName.compare(XML_TAG_LEFT_NAME) == 0 )
						{
							pInCmdSaveCameraRoi->setLeft(strNodeVal);
						}
						else if ( strNodeName.compare(XML_TAG_RIGHT_NAME) == 0 )
						{
							pInCmdSaveCameraRoi->setRight(strNodeVal);
						}
					}
				}
			}
		}
	}

	return true;
}

bool fillInCmdFwUpdate(DOMElement* pXmlRootNode, InCmdFwUpdate*& pInCmdFwUpdate)
{
	if ( ! pXmlRootNode )		return false;
	if ( ! pInCmdFwUpdate ) return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if ( strNodeName.length() > 0 )
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if ( ! strNodeName.compare(XML_TAG_PROTO_FILE_URL_NAME) )
			{
				pInCmdFwUpdate->setUrl(strNodeVal);
			}
			else if ( ! strNodeName.compare(XML_TAG_PROTO_FORCE_NAME) )
			{
				pInCmdFwUpdate->setVersioningCheck(strNodeVal);
			}
			else if ( ! strNodeName.compare(XML_TAG_PROTO_COMPONENT_NAME) )
			{
				pInCmdFwUpdate->setComponent(strNodeVal);
			}
			else if ( ! strNodeName.compare(XML_TAG_PROTO_CRC_NAME) )
			{
				pInCmdFwUpdate->setCRC(strNodeVal);
			}
		}
	}

	return true;
}

bool fillInCmdGetCounter(DOMElement* pXmlRootNode, InCmdGetCounter*& pInCmdGetCounter)
{
	if( ( !pXmlRootNode ) || ( !pInCmdGetCounter ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_PROTO_ID) == 0)
			{
				pInCmdGetCounter->setCounter(strNodeVal);
			}
		}
	}
	return true;
}

bool fillInCmdSetCounter(DOMElement* pXmlRootNode, InCmdSetCounter*& pInCmdSetCounter)
{
	if( ( !pXmlRootNode ) || ( !pInCmdSetCounter ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_PROTO_ID) == 0)
			{
				pInCmdSetCounter->setCounter(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_VALUE_NAME) == 0)
			{
				pInCmdSetCounter->setValue(strNodeVal);
			}
		}
	}
	return true;
}

bool buildPressureSettingsFromRootNode(DOMElement* pXmlRootNode, PressureSettings*& pPressureSettings)
{
	if(!pXmlRootNode)		return false;

	pPressureSettings = new PressureSettings();

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_PRESSURE_VERSION) == 0)
			{
				pPressureSettings->setVersion(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_PRESSURE_ALGORITHM) == 0)
			{
				pPressureSettings->setAlgorithm(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_PRESSURE_THRESHOLD) == 0)
			{
				std::string strSpeed, strVolume, strLow, strHigh;

				strSpeed.clear();	strVolume.clear();
				strLow.clear();		strHigh.clear();

				DOMNodeList* p = l->item(i)->getChildNodes();
				int lNumSubChild = p->getLength();

				for ( int lcount = 0; lcount < lNumSubChild; lcount++ )
				{
					StrX sSubNodeName(p->item(lcount)->getNodeName());
					string strSubNodeName(sSubNodeName.getString());

					StrX sSubVal(p->item(lcount)->getTextContent());
					string strSubNodeVal(sSubVal.getString());

					if (strSubNodeName.length() > 0)
					{
						if (strSubNodeName.compare(XML_TAG_PRESSURE_ATTRIBUTE) == 0)
						{
							DOMNodeList* q = p->item(lcount)->getChildNodes();
							int lNumSubSubChild = q->getLength();

							std::string strKey, strValue;

							for ( int llcount = 0; llcount < lNumSubSubChild; llcount++ )
							{
								StrX sSubSubNodeName(q->item(llcount)->getNodeName());
								string strSubSubNodeName(sSubSubNodeName.getString());

								if (strSubSubNodeName.length() > 0)
								{
									StrX sSubSubVal(q->item(llcount)->getTextContent());
									string strSubSubNodeVal(sSubSubVal.getString());

									if (strSubSubNodeName.compare(XML_TAG_PRESSURE_KEY) == 0)
									{
										strKey.assign(strSubSubNodeVal);
									}
									else if (strSubSubNodeName.compare(XML_TAG_PRESSURE_VALUE) == 0)
									{
										strValue.assign(strSubSubNodeVal);
									}
								}
							}

							if(strKey.compare(XML_TAG_PRESSURE_SPEED) == 0)
							{
								strSpeed.assign(strValue);
							}
							else if(strKey.compare(XML_TAG_PRESSURE_VOLUME) == 0)
							{
								strVolume.assign(strValue);
							}
						}
                        else if (strSubNodeName.compare(XML_TAG_PRESSURE_TH_CLIN_LOW) == 0)
						{
							strLow.assign(strSubNodeVal);
						}
                        else if (strSubNodeName.compare(XML_TAG_PRESSURE_TH_CLIN_HIGH) == 0)
						{
							strHigh.assign(strSubNodeVal);
						}
					}
				}

				// the thresshold must be defined in all its parameters
				if(strSpeed.empty() == false && strVolume.empty() == false &&
					strHigh.empty() == false && strLow.empty() == false	)
				{
					pPressureSettings->setSpeedVolumeThresholds(strSpeed.front(), strVolume.front(),
																atol(strLow.c_str()), atol(strHigh.c_str()));
				}
			}
			else if (strNodeName.compare(XML_TAG_PRESSURE_SETTING) == 0)
			{

				std::string strKey;
				std::string strValue;

				strKey.clear();
				strValue.clear();

				DOMNodeList* p = l->item(i)->getChildNodes();
				int lNumSubChild = p->getLength();

				for ( int lcount = 0; lcount < lNumSubChild; lcount++ )
				{
					StrX sSubNodeName(p->item(lcount)->getNodeName());
					string strSubNodeName(sSubNodeName.getString());

					StrX sSubVal(p->item(lcount)->getTextContent());
					string strSubNodeVal(sSubVal.getString());

					if (strSubNodeName.length() > 0)
					{
						if (strSubNodeName.compare(XML_TAG_PRESSURE_KEY) == 0)
						{
							strKey.assign(strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_PRESSURE_VALUE) == 0)
						{
							strValue.assign(strSubNodeVal);
						}
					}
				}
				if(strKey.empty() == false && strValue.empty() == false)
				{
					// if both key and value are present -> set the item
					if(strKey.compare(XML_TAG_PRESSURE_AREARATIO) == 0)
						pPressureSettings->setAreaRatio(atoi(strValue.c_str()));
					else if(strKey.compare(XML_TAG_PRESSURE_MINPOS) == 0)
						pPressureSettings->setMinimumPositionPercentage(atoi(strValue.c_str()));
					else if(strKey.compare(XML_TAG_PRESSURE_SLOPEPERC) == 0)
						pPressureSettings->setFinalSlopePercentage(atoi(strValue.c_str()));
					else if(strKey.compare(XML_TAG_PRESSURE_SLOPETH) == 0)
						pPressureSettings->setFinalSlopeThreshold(atoi(strValue.c_str()));
                    else if(strKey.compare(XML_TAG_PRESSURE_TH_IND_LOW) == 0)
						pPressureSettings->setIndustryThresholdLow(atol(strValue.c_str()));
                    else if(strKey.compare(XML_TAG_PRESSURE_TH_IND_HIGH) == 0)
						pPressureSettings->setIndustryThresholdHigh(atol(strValue.c_str()));
					else if(strKey.compare(XML_TAG_PRESSURE_CYCLESPERC) == 0)
						pPressureSettings->setIndustryCyclesPercentage(atoi(strValue.c_str()));
					else if(strKey.compare(XML_TAG_PRESSURE_CYCLESMIN) == 0)
						pPressureSettings->setIndustryCyclesMinimum(atoi(strValue.c_str()));
					else if(strKey.compare(XML_TAG_PRESSURE_CYCLESTH) == 0)
						pPressureSettings->setIndustryCyclesThreshold(atoi(strValue.c_str()));
				}
			}
		}
	}

	return true;
}

bool buildCameraSettingsFromRootNode(DOMElement* pXmlRootNode, int liSettingId, CameraSettings* pCameraSettings)
{
	if ( pXmlRootNode == nullptr )		return false;
	if ( pCameraSettings == nullptr )	return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if ( strNodeName.length() > 0 )
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if ( ! strNodeName.compare(XML_TAG_PRESSURE_VERSION) )
			{
				pCameraSettings->setVersion(strNodeVal, liSettingId);
			}
			else if ( ! strNodeName.compare(XML_TAG_PRESSURE_ALGORITHM) )
			{
				pCameraSettings->setAlgorithm(strNodeVal, liSettingId);
			}
			else if ( ! strNodeName.compare(XML_TAG_PRESSURE_SETTING) )
			{
				string strKey("");
				string strValue("");

				DOMNodeList* p = l->item(i)->getChildNodes();
				int lNumSubChild = p->getLength();
				for ( int lcount = 0; lcount < lNumSubChild; lcount++ )
				{
					StrX sSubNodeName(p->item(lcount)->getNodeName());
					string strSubNodeName(sSubNodeName.getString());

					StrX sSubVal(p->item(lcount)->getTextContent());
					string strSubNodeVal(sSubVal.getString());

					if (strSubNodeName.length() > 0)
					{
						if (strSubNodeName.compare(XML_TAG_PRESSURE_KEY) == 0)
						{
							strKey.assign(strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_PRESSURE_VALUE) == 0)
						{
							strValue.assign(strSubNodeVal);
						}
					}
				}

				if ( strKey.size() && strValue.size() )
				{
					pCameraSettings->addSettingPair(strKey, strValue, liSettingId);
				}
			}
		}
	}

	return true;
}

bool fillInCmdSetParameter(DOMElement* pXmlRootNode, InCmdSetParameter*& pInCmdSetParameter)
{
	if( ( !pXmlRootNode ) || ( !pInCmdSetParameter ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if (strNodeName.compare(XML_TAG_COMPONENT_NAME) == 0)
			{
				pInCmdSetParameter->setComponent(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_MOTOR_NAME) == 0)
			{
				pInCmdSetParameter->setMotor(strNodeVal);
			}
			else if (strNodeName.compare(XML_TAG_SETTING) == 0)
			{
				DOMNodeList* p = l->item(i)->getChildNodes();
				int lNumSubChild = p->getLength();

				for ( int lcount = 0; lcount < lNumSubChild; lcount++ )
				{
					StrX sSubNodeName(p->item(lcount)->getNodeName());
					string strSubNodeName(sSubNodeName.getString());
					if (strSubNodeName.length() > 0)
					{
						StrX sSubVal(p->item(lcount)->getTextContent());
						string strSubNodeVal(sSubVal.getString());

						if (strSubNodeName.compare(XML_TAG_MICROSTEP_RES_NAME) == 0)
						{
							pInCmdSetParameter->setMicrostep(strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_ACCELERATION_FACTOR_NAME) == 0)
						{
							pInCmdSetParameter->setAcceleration(strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_OPERATING_CURR_NAME) == 0)
						{
							pInCmdSetParameter->setHighCurrent(strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_STANDBY_CURR_NAME) == 0)
						{
							pInCmdSetParameter->setLowCurrent(strSubNodeVal);
						}
						else if (strSubNodeName.compare(XML_TAG_CONV_FACTOR_NAME) == 0)
						{
							pInCmdSetParameter->setConversion(strSubNodeVal);
						}
					}
				}
			}
			else
			{

			}
		}
	}
	return true;
}

bool fillInCmdSetFanSettings(DOMElement* pXmlRootNode, InCmdSetFanSettings*& pInCmdSetFanSettings)
{
	if( ( !pXmlRootNode ) || ( !pInCmdSetFanSettings ) )
		return false;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	int liNumRootChildren = l->getLength();

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());

		StrX sVal(l->item(i)->getTextContent());
		string strNodeVal(sVal.getString());

		if (strNodeName.length() > 0)
		{

			if (strNodeName.compare(XML_TAG_POWER_NAME) == 0)
			{
				std::string strSection;
				strSection.clear();

				DOMNamedNodeMap * pNamedNodeMap = (l->item(i)->getAttributes());
				if(pNamedNodeMap)
				{
					DOMNode * pDomNode;
					XStr sAttr1(XML_ATTR_COMPONENT_NAME);
					pDomNode = pNamedNodeMap->getNamedItem(sAttr1.unicodeForm());
					if(pDomNode)
					{
						// section string
						StrX sValAttr(pDomNode->getNodeValue());
						strSection.assign(sValAttr.getString());
					}
				}

				if(strSection.empty() == false)
				{
					pInCmdSetFanSettings->setFan(strSection, strNodeVal);
				}
			}
			else if (strNodeName.compare(XML_TAG_PROTO_DURATION_NAME) == 0)
			{
				pInCmdSetFanSettings->setDuration(strNodeVal);
			}

		}
	}
	return true;
}

bool fillInCmdPump(DOMElement *pXmlRootNode, InCmdPump *&pInCmdPump)
{
    if( ( !pXmlRootNode ) || ( !pInCmdPump ) )
        return false;

    DOMNodeList* l = pXmlRootNode->getChildNodes();
    int liNumRootChildren = l->getLength();

    for ( int i = 0; i < liNumRootChildren; i++ )
    {
        StrX sNodeName(l->item(i)->getNodeName());
        string strNodeName(sNodeName.getString());

        StrX sVal(l->item(i)->getTextContent());
        string strNodeVal(sVal.getString());

        if (strNodeName.length() > 0)
        {

            if (strNodeName.compare(XML_TAG_SECTION_NAME) == 0)
            {
                pInCmdPump->setComponent(strNodeVal);
            }
            else if (strNodeName.compare(XML_TAG_ACTION_NAME) == 0)
            {
                pInCmdPump->setAction(strNodeVal);
            }
            else if (strNodeName.compare(XML_TAG_VOLUME_NAME) == 0)
            {
                pInCmdPump->setVolume((uint16_t)atoi(strNodeVal.c_str()));
            }
        }
    }
    return true;
}


