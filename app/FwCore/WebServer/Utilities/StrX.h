/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    StrX.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the StrX class.
 @details

 ****************************************************************************
*/

#ifndef STRX_H
#define STRX_H

#include <xercesc/util/XMLString.hpp>

#if defined(XERCES_NEW_IOSTREAMS)
#include <iostream>
#else
#include <iostream.h>
#endif

XERCES_CPP_NAMESPACE_USE

/*! *******************************************************************************************************************
 * @brief The StrX class	simple class for easy transcoding XMLCh data into a standard C null terminated string
 * ********************************************************************************************************************
 */
class StrX
{

	public:

		/*! ***********************************************************************************************************
		 * @brief StrX			constructor
		 * @param toTranscode	pointer to XMLCh object to be transcoded in a standard C null terminated string
		 * ************************************************************************************************************
		 */
		StrX(const XMLCh* const toTranscode);

		/*! ***********************************************************************************************************
		 * @brief ~StrX			destructor, deallocates the internal C string
		 * ************************************************************************************************************
		 */
		virtual ~StrX();

		/*! ***********************************************************************************************************
		 * @brief getString		get the standard C string built from the XMLCh* object specified in the constructor
		 * @return				the standard C string conversion of the XMLCh* object specified in the constructor
		 * ************************************************************************************************************
		 */
		const char* getString() const;

		friend XERCES_STD_QUALIFIER ostream& operator<<(XERCES_STD_QUALIFIER ostream& target, const StrX& toDump);

    private:

        char* m_sArrayOfChars;	// This is the local C string obtained from the conversion of an XMLCh object

};


#endif // STRX_H
