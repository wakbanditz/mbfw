/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WebServerAndProtocolInclude.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the WebServer classes.
 @details

 ****************************************************************************
*/

#ifndef XMLPROTOCOLINCLUDE_H
#define XMLPROTOCOLINCLUDE_H


/*! *******************************************************************************************************************
 * Web server management
 * ********************************************************************************************************************
 */
#define WEB_SERVER_DEFAULT_LISTEN_ADDRESS			"all"
#define WEB_SERVER_DEFAULT_LISTEN_PORT				8050
#define WEB_SERVER_MAX_NUM_CLIENTS					1
#define WEB_PAGE_PROCESS_COMMAND_NAME				"/api/v1/instrument/processCommand"

#define WEB_XML_IMPLEMENTATION_VERSION				"XML 1.0 Traversal 2.0"
#define WEB_XML_NAMESPACE_TAG						"xmlns:"
// PROTOCOL URL
#define WEB_CMD_ATTR_PROTOCOL_URL_NAME				WEB_XML_NAMESPACE_TAG "ns2"
#define WEB_CMD_ATTR_PROTOCOL_URL_VAL				"http://biomerieux.com/vidas/instrument/protocol/v1/vminst"
// SCHEMA URL
#define WEB_CMD_ATTR_SCHEMA_URL_NAME				WEB_XML_NAMESPACE_TAG "ns2"
#define WEB_CMD_ATTR_SCHEMA_URL_VAL					"http://www.w3.org/2001/XMLSchema-instance"

#define WEB_CMD_ATTR_USAGE							"Usage"
#define WEB_CMD_ATTR_COMMAND_ID						"CommandId"

#define WEB_HEADER_SERIAL_NUMBER_NAME				"serial-number"
#define WEB_HEADER_SERIAL_NUMBER_VALUE				"VM0123456789"

// for the Temperature/Pressure/Optics XML structure
#define WEB_CMD_MODULAR_TEMPERATURE					"vmtd"
#define WEB_CMD_MODULAR_PRESSURE					"vmpd"
#define WEB_CMD_MODULAR_OPTICS						"vmad"

#define WEB_CMD_DATA_TEMPERATURE					"TemperatureData"
#define WEB_CMD_DATA_TREATED_PRESSURE				"TreatedPressureData"
#define WEB_CMD_DATA_ELABORATED_PRESSURE			"ElaboratedPressureData"
#define WEB_CMD_DATA_OPTICS							"AutocheckData"
#define WEB_CMD_DATA_PRESSURE						"PressureData"

#define WEB_CMD_ATTR_TEMPERATURE_URL_VAL			"http://biomerieux.com/vidas/instrument/contract/v1/vmtd"
#define WEB_CMD_ATTR_PRESSURE_URL_VAL				"http://biomerieux.com/vidas/instrument/contract/v1/vmpd"
#define WEB_CMD_ATTR_OPTICS_URL_VAL					"http://biomerieux.com/vidas/instrument/contract/v1/vmad"

#define WEB_CMD_VIDAS_MODULAR_NS					"ns2:"
#define WEB_CMD_VIDAS_MODULAR_NS_OUT				"vminst:"

#define WEB_HEADER_CONTENT_TYPE						"Content-Type"
#define WEB_HEADER_ACCEPT_TYPE						"Accept"
#define WEB_HEADER_APPLICATION_XML_TYPE				"application/xml"

#define WEB_HEADER_CONNECTION_TYPE					"Connection"
#define WEB_HEADER_CONNECTION_KEEPALIVE				"Keep-Alive"

#define WEB_HEADER_EXPECT_TYPE						"Expect"

#define WEB_CMD_NAME_HELLO_IN						"HELLO"
#define WEB_CMD_NAME_HELLO_OUT						"hello"
#define WEB_CMD_NAME_HELLO_IN_FULL					WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_HELLO_IN
#define WEB_CMD_NAME_HELLO_OUT_FULL					WEB_CMD_VIDAS_MODULAR_NS_OUT WEB_CMD_NAME_HELLO_OUT

#define WEB_CMD_NAME_LOGIN_IN						"LOGIN"
#define WEB_CMD_NAME_LOGIN_OUT						"login"
#define WEB_CMD_NAME_LOGIN_IN_FULL					WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_LOGIN_IN

#define WEB_CMD_NAME_LOGOUT_IN						"LOGOUT"
#define WEB_CMD_NAME_LOGOUT_OUT						"logout"
#define WEB_CMD_NAME_LOGOUT_IN_FULL					WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_LOGOUT_IN

#define WEB_CMD_NAME_GETSENSOR_IN					"GETSENSOR"
#define WEB_CMD_NAME_GETSENSOR_OUT					"getsensor"
#define WEB_CMD_NAME_GETSENSOR_IN_FULL				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GETSENSOR_IN

#define WEB_CMD_NAME_GETSERIAL_IN					"GETSERIAL"
#define WEB_CMD_NAME_GETSERIAL_OUT					"getserial"
#define WEB_CMD_NAME_GET_SERIAL_IN_FULL				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GETSERIAL_IN

#define WEB_CMD_NAME_SETSERIAL_IN					"SETSERIAL"
#define WEB_CMD_NAME_SETSERIAL_OUT					"setserial"
#define WEB_CMD_NAME_SETSERIAL_IN_FULL				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SETSERIAL_IN

#define WEB_CMD_NAME_GETVERSIONS_IN					"GETVERSIONS"
#define WEB_CMD_NAME_GETVERSIONS_OUT				"getversions"
#define WEB_CMD_NAME_GET_VERSIONS_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GETVERSIONS_IN

#define WEB_CMD_NAME_VIDASEP_IN						"VIDASEP"
#define WEB_CMD_NAME_VIDASEP_OUT					"vidasep"
#define WEB_CMD_NAME_VIDASEP_IN_FULL				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_VIDASEP_IN

#define WEB_CMD_NAME_SETLED_IN						"SETLED"
#define WEB_CMD_NAME_SETLED_OUT						"setled"
#define WEB_CMD_NAME_SETLED_IN_FULL					WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SETLED_IN

#define WEB_CMD_NAME_READSEC_IN						"READSEC"
#define WEB_CMD_NAME_READSEC_OUT					"readsec"
#define WEB_CMD_NAME_READSEC_IN_FULL				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_READSEC_IN

#define WEB_CMD_NAME_ROUTE_IN						"ROUTE"
#define WEB_CMD_NAME_ROUTE_OUT						"route"
#define WEB_CMD_NAME_ROUTE_IN_FULL					WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_ROUTE_IN

#define WEB_CMD_NAME_MOVE_IN						"MOVE"
#define WEB_CMD_NAME_MOVE_OUT						"move"
#define WEB_CMD_NAME_MOVE_IN_FULL					WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_MOVE_IN

#define WEB_CMD_NAME_MONITOR_PRESSURE_IN			"MONITOR_PRESSURE"
#define WEB_CMD_NAME_MONITOR_PRESSURE_OUT			"monitor_pressure"
#define WEB_CMD_NAME_MONITOR_PRESSURE_IN_FULL		WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_MONITOR_PRESSURE_IN

#define WEB_CMD_NAME_GETPOSITION_IN					"GETPOSITION"
#define WEB_CMD_NAME_GETPOSITION_OUT				"getposition"
#define WEB_CMD_NAME_GETPOSITION_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GETPOSITION_IN

#define WEB_CMD_NAME_CAM_CAPTURE_IMAGE_IN			"CAPTURE_IMAGE"
#define WEB_CMD_NAME_CAM_CAPTURE_IMAGE_OUT			"capture_image"
#define WEB_CMD_NAME_CAM_CAPTURE_IMAGE_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_CAM_CAPTURE_IMAGE_IN

#define WEB_CMD_NAME_GETCALIBRATION_IN				"GETCALIBRATION"
#define WEB_CMD_NAME_GETCALIBRATION_OUT				"getcalibration"
#define WEB_CMD_NAME_GETCALIBRATION_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GETCALIBRATION_IN

#define WEB_CMD_NAME_SAVECALIBRATION_IN				"SAVECALIBRATION"
#define WEB_CMD_NAME_SAVECALIBRATION_OUT			"savecalibration"
#define WEB_CMD_NAME_SAVECALIBRATION_IN_FULL		WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SAVECALIBRATION_IN

#define WEB_CMD_NAME_GETPARAMETER_IN				"GETPARAMETER"
#define WEB_CMD_NAME_GETPARAMETER_OUT				"getparameter"
#define WEB_CMD_NAME_GETPARAMETER_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GETPARAMETER_IN

#define WEB_CMD_NAME_SETPARAMETER_IN				"SETPARAMETER"
#define WEB_CMD_NAME_SETPARAMETER_OUT				"setparameter"
#define WEB_CMD_NAME_SETPARAMETER_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SETPARAMETER_IN

#define WEB_CMD_NAME_SETMOTORACTIVATION_IN			"SETMOTORACTIVATION"
#define WEB_CMD_NAME_SETMOTORACTIVATION_OUT			"setmotoractivation"
#define WEB_CMD_NAME_SETMOTORACTIVATION_IN_FULL		WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SETMOTORACTIVATION_IN

#define WEB_CMD_NAME_GETMOTORACTIVATION_IN			"GETMOTORACTIVATION"
#define WEB_CMD_NAME_GETMOTORACTIVATION_OUT			"getmotoractivation"
#define WEB_CMD_NAME_GETMOTORACTIVATION_IN_FULL		WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GETMOTORACTIVATION_IN

#define WEB_CMD_NAME_INIT_IN						"INIT"
#define WEB_CMD_NAME_INIT_OUT						"init"
#define WEB_CMD_NAME_INIT_IN_FULL					WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_INIT_IN

#define WEB_CMD_NAME_RUNWL_IN						"RUN"
#define WEB_CMD_NAME_RUNWL_OUT						"run"
#define WEB_CMD_NAME_RUNWL_IN_FULL					WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_RUNWL_IN

#define WEB_CMD_NAME_CANCELSECTION_IN				"CANCELSECTION"
#define WEB_CMD_NAME_CANCELSECTION_OUT				"cancelsection"
#define WEB_CMD_NAME_CANCELSECTION_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_CANCELSECTION_IN

#define WEB_CMD_NAME_TRAY_READ_IN					"TRAY_READ"
#define WEB_CMD_NAME_TRAY_READ_OUT					"tray_read"
#define WEB_CMD_NAME_TRAY_READ_IN_FULL				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_TRAY_READ_IN

#define WEB_CMD_NAME_GET_PRESSURE_OFFSETS_IN		"GET_PRESSURE_OFFSETS"
#define WEB_CMD_NAME_GET_PRESSURE_OFFSETS_OUT		"get_pressure_offsets"
#define WEB_CMD_NAME_GET_PRESSURE_OFFSETS_IN_FULL	WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GET_PRESSURE_OFFSETS_IN

#define WEB_CMD_NAME_GET_PRESSURE_SETTINGS_IN		"GET_PRESSURE_SETTINGS"
#define WEB_CMD_NAME_GET_PRESSURE_SETTINGS_OUT		"get_pressure_settings"
#define WEB_CMD_NAME_GET_PRESSURE_SETTINGS_IN_FULL	WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GET_PRESSURE_SETTINGS_IN

#define WEB_CMD_NAME_SET_MAINTENANCE_MODE_IN		"SETMAINTENANCEMODE"
#define WEB_CMD_NAME_SET_MAINTENANCE_MODE_OUT		"setmaintenancemode"
#define WEB_CMD_NAME_SET_MAINTENANCE_MODE_IN_FULL	WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SET_MAINTENANCE_MODE_IN

#define WEB_CMD_NAME_SS_CALIBRATE_IN				"SS_CALIBRATE"
#define WEB_CMD_NAME_SS_CALIBRATE_OUT				"ss_calibrate"
#define WEB_CMD_NAME_SS_CALIBRATE_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SS_CALIBRATE_IN

#define WEB_CMD_NAME_SH_CALIBRATE_IN				"SH_CALIBRATE"
#define WEB_CMD_NAME_SH_CALIBRATE_OUT				"sh_calibrate"
#define WEB_CMD_NAME_SH_CALIBRATE_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SH_CALIBRATE_IN

#define WEB_CMD_NAME_GET_SS_REFERENCE_IN			"GET_SS_REFERENCE"
#define WEB_CMD_NAME_GET_SS_REFERENCE_OUT			"get_ss_reference"
#define WEB_CMD_NAME_GET_SS_REFERENCE_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GET_SS_REFERENCE_IN

#define WEB_CMD_NAME_SET_TIME_IN					"SETTIME"
#define WEB_CMD_NAME_SET_TIME_OUT					"settime"
#define WEB_CMD_NAME_SET_TIME_IN_FULL				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SET_TIME_IN

#define WEB_CMD_NAME_OPT_CALIBRATE_IN				"OPT_CALIBRATE"
#define WEB_CMD_NAME_OPT_CALIBRATE_OUT				"opt_calibrate"
#define WEB_CMD_NAME_OPT_CALIBRATE_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_OPT_CALIBRATE_IN

#define WEB_CMD_NAME_OPT_CHECK_IN           		"OPT_CHECK"
#define WEB_CMD_NAME_OPT_CHECK_OUT                  "opt_check"
#define WEB_CMD_NAME_OPT_CHECK_IN_FULL              WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_OPT_CHECK_IN

#define WEB_CMD_NAME_AIR_CALIBRATE_IN				"AIR_CALIBRATE"
#define WEB_CMD_NAME_AIR_CALIBRATE_OUT				"air_calibrate"
#define WEB_CMD_NAME_AIR_CALIBRATE_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_AIR_CALIBRATE_IN

#define WEB_CMD_NAME_SHUTDOWN_IN					"SHUTDOWN"
#define WEB_CMD_NAME_SHUTDOWN_OUT					"shutdown"
#define WEB_CMD_NAME_SHUTDOWN_IN_FULL				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SHUTDOWN_IN

#define WEB_CMD_NAME_DISABLE_IN						"DISABLE"
#define WEB_CMD_NAME_DISABLE_OUT					"disable"
#define WEB_CMD_NAME_DISABLE_IN_FULL				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_DISABLE_IN

#define WEB_CMD_NAME_SLEEP_IN						"SLEEP"
#define WEB_CMD_NAME_SLEEP_OUT						"sleep"
#define WEB_CMD_NAME_SLEEP_IN_FULL					WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SLEEP_IN

#define WEB_CMD_NAME_AUTO_CALIBRATE_IN				"AUTO_CALIBRATE"
#define WEB_CMD_NAME_AUTO_CALIBRATE_OUT				"auto_calibrate"
#define WEB_CMD_NAME_AUTO_CALIBRATE_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_AUTO_CALIBRATE_IN

#define WEB_CMD_NAME_GET_GLOBAL_SETTINGS_IN			"GET_GLOBAL_SETTINGS"
#define WEB_CMD_NAME_GET_GLOBAL_SETTINGS_OUT		"get_global_settings"
#define WEB_CMD_NAME_GET_GLOBAL_SETTINGS_IN_FULL	WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GET_GLOBAL_SETTINGS_IN

#define WEB_CMD_NAME_SET_GLOBAL_SETTINGS_IN			"SET_GLOBAL_SETTINGS"
#define WEB_CMD_NAME_SET_GLOBAL_SETTINGS_OUT		"set_global_settings"
#define WEB_CMD_NAME_SET_GLOBAL_SETTINGS_IN_FULL	WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SET_GLOBAL_SETTINGS_IN

#define WEB_CMD_NAME_GET_FAN_SETTINGS_IN			"GET_FAN_SETTINGS"
#define WEB_CMD_NAME_GET_FAN_SETTINGS_OUT			"get_fan_settings"
#define WEB_CMD_NAME_GET_FAN_SETTINGS_IN_FULL		WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GET_FAN_SETTINGS_IN

#define WEB_CMD_NAME_SET_FAN_SETTINGS_IN			"SET_FAN_SETTINGS"
#define WEB_CMD_NAME_SET_FAN_SETTINGS_OUT			"set_fan_settings"
#define WEB_CMD_NAME_SET_FAN_SETTINGS_IN_FULL		WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SET_FAN_SETTINGS_IN

#define WEB_CMD_NAME_UNKNOWN						"UNKNOWN"

#define WEB_CMD_NAME_SIGNAL_READ_IN					"SIGNAL_READ"
#define WEB_CMD_NAME_SIGNAL_READ_OUT				"signal_read"
#define WEB_CMD_NAME_SIGNAL_READ_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SIGNAL_READ_IN

#define WEB_CMD_NAME_SH_FORCE_LED_IN				"SH_FORCE_LED"
#define WEB_CMD_NAME_SH_FORCE_LED_OUT				"sh_force_led"
#define WEB_CMD_NAME_SH_FORCE_LED_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SH_FORCE_LED_IN

#define WEB_CMD_NAME_GET_CAMERA_ROI_IN				"GETCAMERAROI"
#define WEB_CMD_NAME_GET_CAMERA_ROI_OUT				"getcameraroi"
#define WEB_CMD_NAME_GET_CAMERA_ROI_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GET_CAMERA_ROI_IN

#define WEB_CMD_NAME_GET_COUNTER_IN					"GETCOUNTER"
#define WEB_CMD_NAME_GET_COUNTER_OUT				"getcounter"
#define WEB_CMD_NAME_GET_COUNTER_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_GET_COUNTER_IN

#define WEB_CMD_NAME_SET_COUNTER_IN					"SETCOUNTER"
#define WEB_CMD_NAME_SET_COUNTER_OUT				"setcounter"
#define WEB_CMD_NAME_SET_COUNTER_IN_FULL			WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SET_COUNTER_IN

#define WEB_CMD_NAME_SAVE_CAMERA_ROI_IN				"SAVECAMERAROI"
#define WEB_CMD_NAME_SAVE_CAMERA_ROI_OUT			"savecameraroi"
#define WEB_CMD_NAME_SAVE_CAMERA_ROI_IN_FULL		WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_SAVE_CAMERA_ROI_IN

#define WEB_CMD_NAME_FW_UPDATE_IN					"FWUPDATE"
#define WEB_CMD_NAME_FW_UPDATE_OUT					"fwupdate"
#define WEB_CMD_NAME_FW_UPDATE_IN_FULL				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_FW_UPDATE_IN

#define WEB_CMD_NAME_PUMP_IN    					"PUMP"
#define WEB_CMD_NAME_PUMP_OUT   					"pump"
#define WEB_CMD_NAME_PUMP_IN_FULL   				WEB_CMD_VIDAS_MODULAR_NS WEB_CMD_NAME_PUMP_IN

/*! *******************************************************************************************************************
 * Web client management
 * ********************************************************************************************************************
 */
#define WEB_DEFAULT_GATEWAY_BASE_URL		"http://192.168.0.1:29091"
#define WEB_DEFAULT_GATEWAY_PAGE			"api/v1/instrument/processResponse"
#define WEB_DEFAULT_PERSIST_SUFFIX          "?persist=true"

/*! *******************************************************************************************************************
 * PROTOCOL VALIDATOR DEFINES
 * ********************************************************************************************************************
 */
#define XML_SERIALIZE_SUCCESS				0
#define XML_SERIALIZE_BAD_FORMAT			1
#define XML_SERIALIZE_OUT_OF_MEMORY			2
#define XML_SERIALIZE_DOM_EXCEPTION			3
#define XML_SERIALIZE_UNHANDLED_EXCEPTION	4
#define XML_SERIALIZE_IMPL_NOT_SUPPORTED	5

/*! *******************************************************************************************************************
 * TAGS NAMES
 * ********************************************************************************************************************
 */
#define XML_TAG_FAILURECAUSE_NAME		"FailureCause"
#define XML_TAG_PAYLOAD_NAME			"Payload"
#define XML_TAG_CATEGORY_NAME			"Category"
#define XML_TAG_COMPONENT_NAME			"Component"
#define XML_TAG_MOTOR_NAME				"Motor"
#define XML_TAG_MOVE_NAME				"Move"
#define XML_TAG_LABEL_NAME				"Label"
#define XML_TAG_ACTIVATION_NAME			"Activation"

#define XML_TAG_ID_NAME					"Id"
#define XML_TAG_PWD_NAME				"Pwd"

#define XML_TAG_COLOR_NAME				"Color"
#define XML_TAG_MODE_NAME				"Mode"
#define XML_TAG_SECTIONA_LED_NAME		"SECTION_A"
#define XML_TAG_SECTIONB_LED_NAME		"SECTION_B"
#define XML_TAG_GLOBAL_LED_NAME			"GLOBAL"

#define XML_TAG_MODULE_NAME             "Module"
#define XML_TAG_INSTRUMENT_NAME			"Instrument"
#define XML_TAG_MASTERBOARD_NAME		"MasterBoard"
#define XML_TAG_NSH_NAME				"NSH"
#define XML_TAG_SECTION_NAME			"Section"
#define XML_TAG_SLOT_NAME				"Slot"
#define XML_TAG_WELL_NAME				"Well"
#define XML_TAG_SECTIONA_NAME			"SectionA"
#define XML_TAG_SECTIONB_NAME			"SectionB"
#define XML_TAG_CAMERA_NAME				"Camera"
#define XML_TAG_SPR_NAME				"Spr"
#define XML_TAG_TRAY_NAME				"Tray"
#define XML_TAG_STRIP_NAME				"Strip"

#define XML_TAG_SENSOR_NAME				"Sensor"
#define XML_TAG_ERRORS_NAME				"Errors"
#define XML_TAG_ERROR_NAME				"Error"

#define XML_TAG_VERSIONS_NAME			"Versions"
#define XML_TAG_APPLICATION_NAME		"Application"
#define XML_TAG_MINIKERNEL_NAME			"MiniKernel"
#define XML_TAG_KERNEL_NAME				"Kernel"
#define XML_TAG_UBOOT_NAME				"UBoot"
#define XML_TAG_BOOTLOADER_NAME			"BootLoader"
#define XML_TAG_FPGA_NAME				"Fpga"
#define XML_TAG_MOVE_POSITION			"Position"

#define XML_TAG_STATE_NAME				"State"
#define XML_TAG_DOOR_NAME				"Door"
#define XML_TAG_TEMPERATURE_NAME		"Temperature"
#define XML_TAG_ENDRUNTIME_NAME			"EndRunTime"
#define XML_TAG_TIMESTAMP_NAME			"Timestamp"
#define XML_TAG_START_BUTTON_NAME		"StartButton"

#define XML_TAG_COMMAND_NAME			"Command"
#define XML_TAG_ANSWER_NAME				"Answer"

#define XML_TAG_ATTRIBUTE				"Attribute"
#define XML_TAG_KEY						"Key"
#define XML_TAG_VALUE					"Value"
#define XML_TAG_SETTING					"Setting"

#define XML_TAG_MOTOR_POSITIONS_NAME	"MotorPositions"

#define XML_TAG_POSITION_NAME			"Position"
#define XML_TAG_CALIBRATION_NAME		"Calibration"
#define XML_TAG_PARAMETER_NAME			"Parameter"
#define XML_TAG_ACTIVATION_NAME			"Activation"

#define XML_TAG_MICROSTEP_RES_NAME			"MicrostepResolution"
#define XML_TAG_ACCELERATION_FACTOR_NAME	"AccelerationFactor"
#define XML_TAG_OPERATING_CURR_NAME			"OperatingCurrent"
#define XML_TAG_STANDBY_CURR_NAME			"StandbyCurrent"
#define XML_TAG_CONV_FACTOR_NAME			"StepConversionFactor"

#define XML_TAG_VOLUME_NAME             "Volume"
#define XML_TAG_PUMP_ACTION_ASPIRATE    "ASPIRATE"
#define XML_TAG_PUMP_ACTION_DISPENSE    "DISPENSE"

#define XML_TAGS_ERROR_NAME				"Error"
#define XML_TAG_ACTION_NAME				"Action"
#define XML_TAG_DETECTION_SETTINGS_NAME	"DetectionSettings"
#define XML_TAG_IMAGE_NAME				"Image"
#define XML_TAG_PREDEFINED_ROI_NAME     "PredefinedROI"
#define XML_TAG_ROI_NAME				"ROI"
#define XML_TAG_TOP_NAME				"Top"
#define XML_TAG_LEFT_NAME				"Left"
#define XML_TAG_BOTTOM_NAME				"Bottom"
#define XML_TAG_RIGHT_NAME				"Right"
#define XML_TAG_CENTER_NAME				"Center"
#define XML_TAG_DATA_NAME				"Data"
#define XML_TAG_BARCODE_NAME			"Barcode"
#define XML_TAG_DETECTION_NAME			"Detection"

#define XML_TAG_MEAN_LOWCASE			"mean"
#define XML_TAG_LEFT_LOWCASE			"left"
#define XML_TAG_CENTER_LOWCASE			"center"
#define XML_TAG_RIGHT_LOWCASE			"right"

#define XML_TAG_START_INDEX_NAME		"StartIndex"
#define XML_TAG_START_TIME_NAME         "StartTime"
#define XML_TAG_VALUES_NAME				"Values"
#define XML_TAG_NAME_NAME				"Name"
#define XML_TAG_SAMPLING_PERIOD			"SamplingPeriod"
#define XML_TAG_SENDING_RATE			"SendingPeriod"
#define XML_TAG_TYPE_NAME				"Type"

#define XML_TAG_WELL_NAME				"Well"
#define XML_TAG_VOLUME_NAME				"Volume"
#define XML_TAG_SPEED_NAME				"Speed"
#define XML_TAG_COMMAND_NAME			"Command"
#define XML_TAG_REPETITION_NAME			"Repetition"

#define XML_TAG_PROTOCOL				"Protocol"
#define XML_TAG_PROTO_ID				"Id"
#define XML_TAG_PROTO_LETTER			"Letter"
#define XML_TAG_PROTO_INDEX				"Index"
#define XML_TAG_PROTO_NAME				"Name"
#define XML_TAG_PROTO_TIME				"TotalTime"
#define XML_TAG_PROTO_GLOBALS			"Globals"
#define XML_TAG_PROTO_LOCALS			"Locals"
#define XML_TAG_PROTO_READINGS			"Readings"
#define XML_TAG_PROTO_READING			"Reading"
#define XML_TAG_PROTO_READING_TIME		"ReadingTime"
#define XML_TAG_PROTO_AIR_READING_TIME	"AirReadingTime"
#define XML_TAG_PROTO_MIN				"Min"
#define XML_TAG_PROTO_MAX				"Max"
#define XML_TAG_PROTO_SLOT				"Slot"
#define XML_TAG_PROTO_NUMBER			"Number"
#define XML_TAG_PROTO_PRINDEX			"ProtoIndex"
#define XML_TAG_PROTO_PROFILE			"LiquidProfile"
#define XML_TAG_PROTO_CHECK				"PressureProfile"
#define XML_TAG_PROTO_RFU				"RfuConversion"
#define XML_TAG_PROTO_RFU_NAME			"Rfu"
#define XML_TAG_PROTO_GAIN_NAME			"Gain"
#define XML_TAG_PROTO_CONSTRAINT		"Constraint"
#define XML_TAG_PROTO_CONSTRAINT_UNIT	"Unit"
#define XML_TAG_PROTO_CONSTRAINT_MIN	"MinDelay"
#define XML_TAG_PROTO_CONSTRAINT_MAX	"MaxDelay"
#define XML_TAG_PROTOCOL_STATUS			"ProtocolStatus"

#define XML_TAG_PROTO_MONITORING		"Monitoring"
#define XML_TAG_PROTO_TEMPERATURE		"Temperature"
#define XML_TAG_PROTO_AUTOCHECK			"Autocheck"
#define XML_TAG_PROTO_SETTINGS			"AcquisitionSettings"
#define XML_TAG_PROTO_MEASUREAIR		"MeasureOnAir"
#define XML_TAG_PROTO_TREATED			"TreatedPressure"
#define XML_TAG_PROTO_ELABORATED		"ElaboratedPressure"
#define XML_TAG_PROTO_SSVALUE_NAME		"SSValue"
#define XML_TAG_PROTO_DURATION_NAME		"Duration"
#define XML_TAG_PROTO_REPETITIONS_NAME	"Repetitions"
#define XML_TAG_PROTO_INTERVAL_NAME		"Interval"
#define XML_TAG_PROTO_R1_NAME			"R1"
#define XML_TAG_PROTO_R2_NAME			"R2"
#define XML_TAG_PROTO_R3_NAME			"R3"
#define XML_TAG_PROTO_FINAL_NAME		"Final"
#define XML_TAG_PROTO_VOLTAGE_NAME		"Voltage"
#define XML_TAG_PROTO_FLUO_NAME			"FLUO"
#define XML_TAG_PROTO_REF_NAME			"REF"
#define XML_TAG_PROTO_AIR_VALUE			"AirReading"
#define XML_TAG_PROTO_AIR_TIME			"AirTime"
#define XML_TAG_PROTO_FW_VERSION		"FwVersion"
#define XML_TAG_PROTO_START_TIME		"ProtocolStart"
#define XML_TAG_PROTO_AIR_INDEX     	"AIR"

#define XML_TAG_PROTO_CAMERA_SETTINGS	"CameraSettings"
#define XML_TAG_PROTO_PRESSURE_SETTINGS	"PressureSettings"
#define XML_TAG_PRESSURE_VERSION		"Version"
#define XML_TAG_PRESSURE_ALGORITHM		"Algorithm"
#define XML_TAG_PRESSURE_THRESHOLD		"Threshold"
#define XML_TAG_PRESSURE_ATTRIBUTE		"Attribute"
#define XML_TAG_PRESSURE_KEY			"Key"
#define XML_TAG_PRESSURE_VALUE			"Value"
#define XML_TAG_PRESSURE_SPEED			"Speed"
#define XML_TAG_PRESSURE_VOLUME			"Volume"
#define XML_TAG_PRESSURE_SETTING		"Setting"
#define XML_TAG_PRESSURE_AREARATIO		"ARight/ALeft_TH"
#define XML_TAG_PRESSURE_MINPOS			"MinSamplePos/TotalSample_TH"
#define XML_TAG_PRESSURE_SLOPEPERC		"FinalSlopePercentage_TH"
#define XML_TAG_PRESSURE_SLOPETH		"FinalSlope_TH"
#define XML_TAG_PRESSURE_TH_CLIN_LOW	"Low"
#define XML_TAG_PRESSURE_TH_CLIN_HIGH	"High"
#define XML_TAG_PRESSURE_CYCLESPERC		"CyclesPercentage_TH_IND"
#define XML_TAG_PRESSURE_CYCLESMIN		"CyclesMinimumNumber_TH_IND"
#define XML_TAG_PRESSURE_CYCLESTH		"Cycles_TH_IND"
#define XML_TAG_PRESSURE_TH_AREA_LOW	"AREALow_TH"
#define XML_TAG_PRESSURE_TH_AREA_HIGH	"AREAHigh_TH"

#define XML_TAG_PRESSURE_ALGORITHM_NAME	"PressureAlgorithm"
#define XML_TAG_PRESSURE_CONVERSION_NAME "ConversionFactor"
#define XML_TAG_PRESSURE_OFFSET_NAME	"Offset"
#define XML_TAG_PRESSURE_ASPIRATION_DATA "AspirationPressureData"
#define XML_TAG_PRESSURE_PHASE			"Phase"

#define XML_TAG_PRESSURE_TH_IND_LOW     "LowTH_IND"
#define XML_TAG_PRESSURE_TH_IND_HIGH	"HighTH_IND"
#define XML_TAG_PRESSURE_EMPTY_WELL		"EmptyWell"
#define XML_TAG_PRESSURE_THRESHOLD_CHK	"CheckHighThreshold"
#define XML_TAG_PRESSURE_TOTAL_SAMPLE	"TotalSample"
#define XML_TAG_PRESSURE_WELL			"Well"
#define XML_TAG_PRESSURE_RESULT_FLAG	"ResultFlag"
#define XML_TAG_PRESSURE_AREA			"Area"
#define XML_TAG_PRESSURE_RIGHT_AREA		"RightArea"
#define XML_TAG_PRESSURE_LEFT_AREA		"LeftArea"
#define XML_TAG_PRESSURE_INTEGRAL		"Integral"
#define XML_TAG_PRESSURE_FIRST_SAMPLE_VAL	"FirstSampleVal"
#define XML_TAG_PRESSURE_LAST_SAMPLE_VAL	"LastSampleVal"
#define XML_TAG_PRESSURE_MAX_SAMPLE_VAL		"MaxSampleVal"
#define XML_TAG_PRESSURE_MAX_SAMPLE_POS		"MaxSamplePos"
#define XML_TAG_PRESSURE_MIN_SAMPLE_VAL		"MinSampleVal"
#define XML_TAG_PRESSURE_MIN_SAMPLE_POS		"MinSamplePos"
#define XML_TAG_PRESSURE_FINAL_SLOPE		"FinalSlope"
#define XML_TAG_PRESSURE_VOLUME			"Volume"
#define XML_TAG_PRESSURE_SPEED			"Speed"
#define XML_TAG_PRESSURE_COMMAND		"Command"
#define XML_TAG_PRESSURE_REPETITION		"Repetition"
#define XML_TAG_PRESSURE_MAXMIN_DIFFERENCE	"MinMaxDifference_IND"
#define XML_TAG_PRESSURE_SUPER_MAX		"SuperMax_IND"
#define XML_TAG_PRESSURE_SUPER_MIN		"SuperMin_IND"
#define XML_TAG_PRESSURE_AIR_COUNTER	"AirCounter_IND"

#define XML_TAG_TEMPERATURE_SENSOR_DATA	"SensorData"
#define XML_TAG_TEMPERATURE_SENSOR_STRIP	"STRIP"
#define XML_TAG_TEMPERATURE_SENSOR_SPR		"SPR"
#define XML_TAG_TEMPERATURE_SENSOR_INTERNAL	"INTERNAL"

#define XML_TAG_OPTIC_SOLID_STD_GOLDEN	"SolidStandardGolden"
#define	XML_TAG_OPTIC_SOLID_STD_VALUE	"SolidStandardValue"
#define XML_TAG_OPTIC_READING			"Reading"

#define XML_TAG_TARGET_NAME				"Target"
#define XML_TAG_POWER_NAME				"Power"

#define XML_TAG_THEORIC_POSITION		"Theoric"
#define XML_TAG_REAL_POSITION			"Real"

#define XML_TAG_ALL						"ALL"

#define XML_TAG_CONVERSION_FACTOR_NAME	"ConversionFactor"
#define XML_TAG_VALUE_NAME				"Value"

#define XML_VALUE_ENABLE				"ENABLE"
#define XML_VALUE_DISABLE				"DISABLE"

#define XML_TAG_SPRBLOCK_NAME			"SprBlock"
#define XML_TAG_TRAYBLOCK_NAME			"StripTray"
#define XML_TAG_INTERNAL_NAME			"Internal"
#define XML_TAG_TEMP_MIN_NAME			"Min"
#define XML_TAG_TEMP_MAX_NAME			"Max"
#define XML_TAG_TEMP_TARGET_NAME		"Target"
#define XML_TAG_TEMP_TOLERANCE_NAME		"SectionToSectionTolerance"

#define XML_TAG_PROTO_FILE_URL_NAME		"FileURI"
#define XML_TAG_PROTO_FORCE_NAME		"ForceUpdate"
#define XML_TAG_PROTO_COMPONENT_NAME	"Component"
#define XML_TAG_PROTO_CRC_NAME			"CRC"

#define XML_TAG_RFU_TARGET_OPT          "Target"
#define XML_TAG_RFU_TOLERANCE_OPT       "Tolerance"

#define XML_TAG_PROGRESS                "Progress"

#define XML_TAG_OPT_ID                  "OptAdjustmentId"

/*! *******************************************************************************************************************
 * TAG VALUES NAMES
 * ********************************************************************************************************************
 */

#define XML_TAG_MOTOR_NAME_TOWER		"Tower"
#define XML_TAG_MOTOR_NAME_PUMP			"Pump"
#define XML_TAG_MOTOR_NAME_TRAY			"Tray"
#define XML_TAG_MOTOR_NAME_SPR			"SPR"
#define XML_TAG_MOTOR_NAME_NSH			"NSH"

#define XML_TAG_MOVE_VALUE_RELATIVE		"Relative"
#define XML_TAG_MOVE_VALUE_ABSOLUTE		"Absolute"
#define XML_TAG_MOVE_VALUE_PREDEFINED	"Predefined"
#define XML_TAG_MOVE_VALUE_HOME			"Home"

/*! *******************************************************************************************************************
 * ATTRIBUTES NAMES
 * ********************************************************************************************************************
 */
#define XML_ATTR_STATUS_NAME			"Status"
#define XML_ATTR_CODE_NAME				"Code"
#define XML_ATTR_LETTER_NAME			"Letter"
#define XML_ATTR_INDEX_NAME				"Index"
#define XML_ATTR_NUMBER_NAME			"Number"
#define XML_TAG_DATA_NAME				"Data"
#define XML_ATTR_PHASE_NAME				"Phase"
#define XML_ATTR_PROG_NAME				"Prog"
#define XML_ATTR_TIME_NAME				"Time"
#define XML_ATTR_FORCE_NAME				"Force"

#define XML_ATTR_SLOT_NAME				"Slot"
#define XML_ATTR_STRIP_NAME				"Strip"
#define XML_ATTR_SPR_NAME				"Spr"
#define XML_ATTR_SUBSTRATE_NAME			"Substrate"
#define XML_ATTR_SAMPLE_NAME			"Sample"
#define XML_ATTR_SCORE_NAME				"Score"

#define XML_ATTR_COMPONENT_NAME			"Component"
#define XML_ATTR_MOTOR_NAME				"Motor"
#define XML_ATTR_LABEL_NAME				"Label"

#define XML_ATTR_ID_NAME				"Id"
#define XML_ATTR_CATEGORY_NAME			"Category"
#define XML_ATTR_UNIT_NAME				"Unit"
#define XML_ATTR_OFFSET_NAME			"Offset"
#define XML_ATTR_DECIMALS_NAME			"Decimals"

#define XML_ATTR_ERROR_DEVICE_NAME		"Device"
#define XML_ATTR_ERROR_CODE_NAME		"Code"
#define XML_ATTR_ERROR_GROUP_NAME		"GroupCode"
#define XML_ATTR_ERROR_LEVEL_NAME		"Level"
#define XML_ATTR_ERROR_DESCRIPTION_NAME	"Description"
#define XML_ATTR_ERROR_TIMESTAMP_NAME	"Timestamp"

#define XML_ATTR_PAYLOAD_IDX_NAME		"Index"
#define XML_ATTR_IMAGE_QUALITY_NAME		"Quality"
#define XML_ATTR_IMAGE_LIGHT_NAME		"Light"
#define XML_ATTR_IMAGE_FORMAT_NAME		"Format"
#define XML_ATTR_IMAGE_FOCUS_NAME		"Focus"
#define XML_ATTR_CROP_ENABLE_NAME		"CropEnabled"

#define XML_ATTR_POSITION_NAME			"Position"

#define XML_ATTR_SAMPLING_PERIOD		"SamplingPeriod"

/*! *******************************************************************************************************************
 * ATTRIBUTES VALUES
 * ********************************************************************************************************************
 */
#define XML_ATTR_STATUS_ACCEPTED		"ACCEPTED"
#define XML_ATTR_STATUS_REJECTED		"REJECTED"
#define XML_ATTR_STATUS_SUCCESSFULL		"SUCCESSFUL"
#define XML_ATTR_STATUS_FAILED			"FAILED"

#define XML_ATTR_STATUS_ENABLED			"ENABLED"
#define XML_ATTR_STATUS_DISABLED		"DISABLED"

#define XML_VALUE_SECTION_A				"A"
#define XML_VALUE_SECTION_B				"B"

#define XML_VALUE_STRIP_1				"1"
#define XML_VALUE_STRIP_2				"2"
#define XML_VALUE_STRIP_3				"3"
#define XML_VALUE_STRIP_4				"4"
#define XML_VALUE_STRIP_5				"5"
#define XML_VALUE_STRIP_6				"6"

#define XML_VALUE_TRUE					"true"
#define XML_VALUE_FALSE					"false"
#define XML_VALUE_0						"0"
#define XML_VALUE_1						"1"

#define XML_VALUE_WELL_X0				"X0"
#define XML_VALUE_WELL_X3				"X3"
#define XML_VALUE_WELL_X0_ID			"0"
#define XML_VALUE_WELL_X3_ID			"3"

#define XML_VALUE_START_LOWCASE			"Start"
#define XML_VALUE_START					"START"
#define XML_VALUE_STOP					"STOP"

#define XML_VALUE_PROTOCOL_START		"PROTOCOL_START"
#define XML_VALUE_PROTOCOL_START_NORAW	"PROTOCOL_START_NORAW"
#define XML_VALUE_PROTOCOL_STOP			"PROTOCOL_STOP"

#define XML_SAMPLING_PERIOD_VALUE		"4"

/*! *******************************************************************************************************************
 * GETSENSOR command LABELS
 * ********************************************************************************************************************
 */

#define SENSOR_COMPONENT_INSTRUMENT		"Instrument"
#define SENSOR_COMPONENT_SECTION_A		"SectionA"
#define SENSOR_COMPONENT_SECTION_B		"SectionB"
#define SENSOR_COMPONENT_NSH			"NSH"

#define SENSOR_UNIT_RPM					"RPM"
#define SENSOR_UNIT_DEGREES				"CELSIUS"
#define SENSOR_UNIT_UNDEFINED			"UNDEFINED"
#define SENSOR_UNIT_VOLT				"VOLT"

#define SENSOR_CATEGORY_FAN				"FAN"
#define SENSOR_CATEGORY_TEMPERATURE		"TEMPERATURE"
#define SENSOR_CATEGORY_HOME			"HOME"
#define SENSOR_CATEGORY_DOOR			"DOOR"
#define SENSOR_CATEGORY_VOLTAGE			"VOLTAGE"

#define SENSOR_ID_PREFIX_SECTION_A		"SA"
#define SENSOR_ID_PREFIX_SECTION_B		"SB"
#define SENSOR_ID_PREFIX_NSH			"NSH"
#define SENSOR_ID_PREFIX_INSTRUMENT		"Instr"

#define SENSOR_ID_NAME_FAN				"Fan"
#define SENSOR_ID_NAME_NTC				"Ntc"
#define SENSOR_ID_NAME_HOME				"Home"
#define SENSOR_ID_NAME_DOOR				"Door"

#define SENSOR_ID_NAME_24V				"24V0"
#define SENSOR_ID_NAME_5V				"5V0"
#define SENSOR_ID_NAME_3V3				"3V3"
#define SENSOR_ID_NAME_1V2				"1V2"

#endif // XMLPROTOCOLINCLUDE_H
