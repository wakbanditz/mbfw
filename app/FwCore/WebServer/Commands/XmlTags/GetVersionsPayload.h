/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetVersionsPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetVersionsPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETVERSIONSPAYLOAD_H
#define GETVERSIONSPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETVERSIONS command
 * ********************************************************************************************************************
 */

class GetVersionsPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief GetVersionsPayload default constructor
		 * ***********************************************************************************************************
		 */
		GetVersionsPayload();

		/*! ***********************************************************************************************************
		 * @brief getVersionMasterApplication		retrieves the MasterBoard Application release
		 * @return					the MasterBoard Application release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionMasterApplication(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionMasterApplication	set the MasterBoard Application release
		 * @param strName			std::string that represents MasterBoard Application release
		 * ***********************************************************************************************************
		 */
		void setVersionMasterApplication(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionMasterMiniKernel		retrieves the MasterBoard MiniKernel release
		 * @return					the MasterBoard MiniKernel release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionMasterMiniKernel(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionMasterMiniKernel	set the MasterBoard MiniKernel release
		 * @param strName			std::string that represents MasterBoard MiniKernel release
		 * ***********************************************************************************************************
		 */
		void setVersionMasterMiniKernel(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionMasterKernel		retrieves the MasterBoard Kernel release
		 * @return					the MasterBoard Kernel release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionMasterKernel(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionMasterKernel	set the MasterBoard Kernel release
		 * @param strName			std::string that represents MasterBoard Kernel release
		 * ***********************************************************************************************************
		 */
		void setVersionMasterKernel(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionMasterBoot		retrieves the MasterBoard Boot release
		 * @return					the MasterBoard Boot release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionMasterBoot(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionMasterBoot	set the MasterBoard Boot release
		 * @param strName			std::string that represents MasterBoard Boot release
		 * ***********************************************************************************************************
		 */
		void setVersionMasterBoot(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionNshApplication		retrieves the Nsh Application release
		 * @return					the Nsh Application release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionNshApplication(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionNshApplication	set the Nsh Application release
		 * @param strName			std::string that represents Nsh Application release
		 * ***********************************************************************************************************
		 */
		void setVersionNshApplication(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionNshBoot		retrieves the Nsh Boot release
		 * @return					the Nsh Boot release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionNshBoot(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionNshBoot	set the Nsh Boot release
		 * @param strName			std::string that represents Nsh Boot release
		 * ***********************************************************************************************************
		 */
		void setVersionNshBoot(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionSectionAApplication		retrieves the SectionA Application release
		 * @return					the SectionA Application release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionSectionAApplication(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionSectionAApplication	set the SectionA Application release
		 * @param strName			std::string that represents SectionA Application release
		 * ***********************************************************************************************************
		 */
		void setVersionSectionAApplication(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionSectionAFpga		retrieves the SectionA Fpga release
		 * @return					the SectionA Fpga release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionSectionAFpga(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionSectionAFpga	set the SectionA Fpga release
		 * @param strName			std::string that represents SectionA Fpga release
		 * ***********************************************************************************************************
		 */
		void setVersionSectionAFpga(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionSectionABoot		retrieves the SectionA Boot release
		 * @return					the SectionA Boot release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionSectionABoot(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionSectionABoot	set the SectionA Boot release
		 * @param strName			std::string that represents SectionA Boot release
		 * ***********************************************************************************************************
		 */
		void setVersionSectionABoot(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionSectionBApplication		retrieves the SectionB Application release
		 * @return					the SectionB Application release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionSectionBApplication(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionSectionBApplication	set the SectionB Application release
		 * @param strName			std::string that represents SectionB Application release
		 * ***********************************************************************************************************
		 */
		void setVersionSectionBApplication(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionSectionBFpga		retrieves the SectionB Fpga release
		 * @return					the SectionB Fpga release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionSectionBFpga(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionSectionBFpga	set the SectionB Fpga release
         * @param strName			std::string that represents SectionB Fpga release
		 * ***********************************************************************************************************
		 */
		void setVersionSectionBFpga(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionSectionBBoot		retrieves the SectionB Boot release
		 * @return					the SectionB Boot release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionSectionBBoot(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionSectionBBoot	set the SectionB Boot release
		 * @param strName			std::string that represents SectionB Boot release
		 * ***********************************************************************************************************
		 */
		void setVersionSectionBBoot(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getVersionCameraApplication		retrieves the Camera Application release
		 * @return					the Camera Application release as a std::string
		 * ************************************************************************************************************
		 */
		std::string getVersionCameraApplication(void);

		/*! ***********************************************************************************************************
		 * @brief setVersionCameraApplication	set the Camera Application release
		 * @param strName			std::string that represents Camera Application release
		 * ***********************************************************************************************************
		 */
		void setVersionCameraApplication(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);


	private:

		std::string m_strMasterApplication;
		std::string m_strMasterMiniKernel;
		std::string m_strMasterKernel;
		std::string m_strMasterBoot;

		std::string m_strNshApplication;
		std::string m_strNshBoot;

		std::string m_strSectionAApplication;
		std::string m_strSectionAFpga;
		std::string m_strSectionABoot;

		std::string m_strSectionBApplication;
		std::string m_strSectionBFpga;
		std::string m_strSectionBBoot;

		std::string m_strCameraApplication;
};

#endif // GETVERSIONSPAYLOAD_H

