/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetVersionsPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetVersionsPayload class.
 @details

 ****************************************************************************
*/

#include "GetVersionsPayload.h"
#include "WebServerAndProtocolInclude.h"

GetVersionsPayload::GetVersionsPayload()
{
	m_strMasterApplication.clear();
	m_strMasterMiniKernel.clear();
	m_strMasterKernel.clear();
	m_strMasterBoot.clear();

	m_strNshApplication.clear();
	m_strNshBoot.clear();

	m_strSectionAApplication.clear();
	m_strSectionAFpga.clear();
	m_strSectionABoot.clear();

	m_strSectionBApplication.clear();
	m_strSectionBFpga.clear();
	m_strSectionBBoot.clear();

	m_strCameraApplication.clear();
}

std::__cxx11::string GetVersionsPayload::getVersionMasterApplication()
{
	return m_strMasterApplication;
}

void GetVersionsPayload::setVersionMasterApplication(std::__cxx11::string strName)
{
	m_strMasterApplication.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionMasterMiniKernel()
{
	return m_strMasterMiniKernel;
}

void GetVersionsPayload::setVersionMasterMiniKernel(std::__cxx11::string strName)
{
	m_strMasterMiniKernel.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionMasterKernel()
{
	return m_strMasterKernel;
}

void GetVersionsPayload::setVersionMasterKernel(std::__cxx11::string strName)
{
	m_strMasterKernel.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionMasterBoot()
{
	return m_strMasterBoot;
}

void GetVersionsPayload::setVersionMasterBoot(std::__cxx11::string strName)
{
	m_strMasterBoot.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionNshApplication()
{
	return m_strNshApplication;
}

void GetVersionsPayload::setVersionNshApplication(std::__cxx11::string strName)
{
	m_strNshApplication.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionNshBoot()
{
	return m_strNshBoot;
}

void GetVersionsPayload::setVersionNshBoot(std::__cxx11::string strName)
{
	m_strNshBoot.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionSectionAApplication()
{
	return m_strSectionAApplication;
}

void GetVersionsPayload::setVersionSectionAApplication(std::__cxx11::string strName)
{
	m_strSectionAApplication.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionSectionAFpga()
{
	return m_strSectionAFpga;
}

void GetVersionsPayload::setVersionSectionAFpga(std::__cxx11::string strName)
{
	m_strSectionAFpga.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionSectionABoot()
{
	return m_strSectionABoot;
}

void GetVersionsPayload::setVersionSectionABoot(std::__cxx11::string strName)
{
	m_strSectionABoot.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionSectionBApplication()
{
	return m_strSectionBApplication;
}

void GetVersionsPayload::setVersionSectionBApplication(std::__cxx11::string strName)
{
	m_strSectionBApplication.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionSectionBFpga()
{
	return m_strSectionBFpga;
}

void GetVersionsPayload::setVersionSectionBFpga(std::__cxx11::string strName)
{
	m_strSectionBFpga.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionSectionBBoot()
{
	return m_strSectionBBoot;
}

void GetVersionsPayload::setVersionSectionBBoot(std::__cxx11::string strName)
{
	m_strSectionBBoot.assign(strName);
}

std::__cxx11::string GetVersionsPayload::getVersionCameraApplication()
{
	return m_strCameraApplication;
}

void GetVersionsPayload::setVersionCameraApplication(std::__cxx11::string strName)
{
	m_strCameraApplication.assign(strName);
}

bool GetVersionsPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement*  pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VERSIONS_NAME));
	pElemTmp1->appendChild(pElemTmp2);

	DOMElement*  pElemTmp3 = pOutDoc->createElement(X(XML_TAG_MASTERBOARD_NAME));
	pElemTmp2->appendChild(pElemTmp3);

	DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_APPLICATION_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionMasterApplication().c_str()));

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_MINIKERNEL_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionMasterMiniKernel().c_str()));

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_KERNEL_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionMasterKernel().c_str()));

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_UBOOT_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionMasterBoot().c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_NSH_NAME));
	pElemTmp2->appendChild(pElemTmp3);

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_APPLICATION_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionNshApplication().c_str()));

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_BOOTLOADER_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionNshBoot().c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_SECTIONA_NAME));
	pElemTmp2->appendChild(pElemTmp3);

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_APPLICATION_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionSectionAApplication().c_str()));

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_FPGA_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionSectionAFpga().c_str()));

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_BOOTLOADER_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionSectionABoot().c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_SECTIONB_NAME));
	pElemTmp2->appendChild(pElemTmp3);

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_APPLICATION_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionSectionBApplication().c_str()));

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_FPGA_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionSectionBFpga().c_str()));

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_BOOTLOADER_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionSectionBBoot().c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_CAMERA_NAME));
	pElemTmp2->appendChild(pElemTmp3);

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_APPLICATION_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	pElemTmp4->setTextContent(X(getVersionCameraApplication().c_str()));

	return true;
}
