/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetFanSettingsPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetFanSettingsPayload class.
 @details

 ****************************************************************************
*/

#include "GetFanSettingsPayload.h"
#include "WebServerAndProtocolInclude.h"

GetFanSettingsPayload::GetFanSettingsPayload()
{
	for(uint8_t i = 0; i < eFanNone; i++)	m_fanPower[i] = 0;
}


bool GetFanSettingsPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	if(pOutDoc == 0 || pRootElem == 0)
		return false;

	std::string strTmp;

	DOMElement*  pElemTmp = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp);

	DOMElement*  pElemTmp2 = pOutDoc->createElement(X(XML_TAG_POWER_NAME));
	pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setAttribute(X(XML_ATTR_COMPONENT_NAME), X(XML_TAG_SECTIONA_NAME));
	strTmp = std::to_string(m_fanPower[eFanSectionA]);
	pElemTmp2->setTextContent(X(strTmp.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_POWER_NAME));
	pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setAttribute(X(XML_ATTR_COMPONENT_NAME), X(XML_TAG_SECTIONB_NAME));
	strTmp = std::to_string(m_fanPower[eFanSectionB]);
	pElemTmp2->setTextContent(X(strTmp.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_POWER_NAME));
	pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setAttribute(X(XML_ATTR_COMPONENT_NAME), X(XML_TAG_INSTRUMENT_NAME));
	strTmp = std::to_string(m_fanPower[eFanSupply]);
	pElemTmp2->setTextContent(X(strTmp.c_str()));

	return true;
}

void GetFanSettingsPayload::setFanPower(eModuleFanId index, uint8_t value)
{
	if(index < eFanNone)
	{
		m_fanPower[index] = value;
	}
}

