/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetCameraRoiPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetCameraRoiPayload class.
 @details

 ****************************************************************************
*/

#include "GetCameraRoiPayload.h"
#include "WebServerAndProtocolInclude.h"

GetCameraRoiPayload::GetCameraRoiPayload()
{
	m_mapItems.clear();
}

GetCameraRoiPayload::~GetCameraRoiPayload()
{

}

void GetCameraRoiPayload::setAttributes(unordered_map<string, structRect>& mConfig)
{
	m_mapItems = mConfig;
}

bool GetCameraRoiPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	for ( const auto &it : m_mapItems )
	{
		// Need to convert the coordinates. SSW expects top - left - bottom - right
		// I have x0, y0, width, height

		int liTop = it.second.y;
		int liBottom = it.second.y + it.second.height;
		int liLeft = it.second.x;
		int liRight = it.second.x + it.second.width;

		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ROI_NAME));
		pElemTmp1->appendChild(pElemTmp2);

		XStr xstrComponentName(XML_TAG_PROTO_NAME);
		XStr xstrComponentValue(it.first.c_str());
		pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

		DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TOP_NAME));
		pElemTmp2->appendChild(pElemTmp3);
		pElemTmp3->setTextContent(X(to_string(liTop).c_str()));

		DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_LEFT_NAME));
		pElemTmp2->appendChild(pElemTmp4);
		pElemTmp4->setTextContent(X(to_string(liLeft).c_str()));

		DOMElement* pElemTmp5 = pOutDoc->createElement(X(XML_TAG_BOTTOM_NAME));
		pElemTmp2->appendChild(pElemTmp5);
		pElemTmp5->setTextContent(X(to_string(liBottom).c_str()));

		DOMElement* pElemTmp6 = pOutDoc->createElement(X(XML_TAG_RIGHT_NAME));
		pElemTmp2->appendChild(pElemTmp6);
		pElemTmp6->setTextContent(X(to_string(liRight).c_str()));
	}

	return true;
}
