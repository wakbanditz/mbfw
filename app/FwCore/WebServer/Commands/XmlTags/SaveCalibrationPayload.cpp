/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SaveCalibrationPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SaveCalibrationPayload class.
 @details

 ****************************************************************************
*/

#include "SaveCalibrationPayload.h"
#include "WebServerAndProtocolInclude.h"

SaveCalibrationPayload::SaveCalibrationPayload()
{
	m_vecSaveCalibration.clear();
}

SaveCalibrationPayload::~SaveCalibrationPayload()
{
	/* Nothing to do yet */
}

void SaveCalibrationPayload::setOutElement(structSaveCalibration &stCalib)
{
	m_vecSaveCalibration.push_back(stCalib);
}

bool SaveCalibrationPayload::composePayload(DOMDocument *&pOutDoc, DOMElement *pRootElem)
{
	//Payload element
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	for (size_t i=0 ; i<m_vecSaveCalibration.size(); i++)
	{
		//Sensor element
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_POSITION_NAME));
		pElemTmp1->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(m_vecSaveCalibration[i].strValue.c_str()));

		//Sensor attributesl
		XStr xstrComponentName(XML_ATTR_COMPONENT_NAME);
		XStr xstrComponentValue(m_vecSaveCalibration[i].strComponent.c_str());
		pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

		XStr xstrMotorName(XML_ATTR_MOTOR_NAME);
		XStr xstrMotorValue(m_vecSaveCalibration[i].strMotor.c_str());
		pElemTmp2->setAttribute(xstrMotorName.unicodeForm(), xstrMotorValue.unicodeForm());

		XStr xstrLabelName(XML_ATTR_LABEL_NAME);
		XStr xstrLabelValue(m_vecSaveCalibration[i].strLabel.c_str());
		pElemTmp2->setAttribute(xstrLabelName.unicodeForm(), xstrLabelValue.unicodeForm());
	}

	return true;
}
