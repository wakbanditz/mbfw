/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    RunwlPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the RunwlPayload class.
 @details

 ****************************************************************************
*/

#ifndef RUNWLPAYLOAD_H
#define RUNWLPAYLOAD_H

#include "Payload.h"
#include "CommonInclude.h"

#define PROTOCOL_STATUS_NOT_STARTED		"PROTO_NOT_STARTED"
#define PROTOCOL_STATUS_PARTIALLY_DONE	"PROTO_PARTIALLY_DONE"
#define PROTOCOL_STATUS_COMPLETED		"PROTO_DONE"

class RunwlInfo;

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the RUN command
 * ********************************************************************************************************************
 */

class RunwlPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		RunwlPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~RunwlPayload();

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

		/*! ***********************************************************************************************************
		 * @brief setSectionRunwl set the pointer to the global runwl object to be included in reply
		 * @param pRunwlInfo valid pointer to the Runwl object
		 * @param section the section index
		 * @return true if succesfull
		 * ***********************************************************************************************************
		 */
		bool setSectionRunwl(uint8_t section, RunwlInfo * pRunwlInfo);

		/*! ***********************************************************************************************************
		 * @brief setProtocolStatus set the protocol status string to be included in reply
		 * @param strStatus the protocol status string as defined in this header
		 * @return true if succesfull
		 * ***********************************************************************************************************
		 */
		bool setProtocolStatus(std::string& strStatus);

		/*! ***********************************************************************************************************
		 * @brief setTemperature set the temperature string to be encoded and included in reply
		 * @param strTemperatureEncoded temperature string (empty if not requested)
		 * @return true if succesfull
		 * ***********************************************************************************************************
		 */
		bool setTemperature(std::string& strTemperatureEncoded);

		/*! ***********************************************************************************************************
		 * @brief setOptics set the Optics string to be encoded and included in reply
		 * @param strOptics optic string (empty if not requested)
		 * @return true if succesfull
		 * ***********************************************************************************************************
		 */
		bool setOptics(std::string& strOptics);

		/*! ***********************************************************************************************************
		 * @brief setTreatedPressure set the treated pressure string to be encoded and included in reply
		 * @param strTreatedPressure pressure data string (empty if not requested)
		 * @param strip the strip index
		 * @return true if succesfull
		 * ***********************************************************************************************************
		 */
		bool setTreatedPressure(uint8_t strip, std::string& strTreatedPressure);

		/*! ***********************************************************************************************************
		 * @brief setElaboratedPressure set the elaborated pressure string to be encoded and included in reply
		 * @param strElaboratedPressure pressure data string (empty if not requested)
		 * @param strip the strip index
		 * @return true if succesfull
		 * ***********************************************************************************************************
		 */
		bool setElaboratedPressure(uint8_t strip, std::string& strElaboratedPressure);

		/*! ***********************************************************************************************************
		 * @brief setPressureAlgorithm set for the strip in processing the Pressure Algorithm
		 * @param strAlgorithm the Algorithm string
		 * ***********************************************************************************************************
		 */
		void setPressureAlgorithm(std::string& strAlgorithm);

		/*! ***********************************************************************************************************
		 * @brief setPressureOffset set for the strip in processing the Pressure Offset
		 * @param offset the offset value
		 * ***********************************************************************************************************
		 */
		void setPressureOffset(int32_t offset);

		/*! ***********************************************************************************************************
		 * @brief setPressureFactor set for the strip in processing the Pressure Conversion Factor
		 * @param factor the pressure-digits factor value
		 * ***********************************************************************************************************
		 */
		void setPressureFactor(int32_t factor);

        /*! ***********************************************************************************************************
         * @brief setPressureSendingRate set the pressure data sending rate
         * @param factor the pressure rate
         * ***********************************************************************************************************
         */
        void setPressureSendingRate(int32_t factor);

    private:

        uint8_t m_section;
        RunwlInfo * m_pRunwlInfo;
        std::string m_strProtocolStatus;

        std::string m_strTemperature;
        std::string m_strOptics;
        std::vector<std::string> m_strTreatedPressure;
        std::vector<std::string> m_strElaboratedPressure;

};


/*! ***********************************************************************************************************
 * @brief buildTemperaturePayload function called to build the Temperature payload structure
 * @param section the section involved
 * @param pOutDoc valid pointer to the XML Doc
 * @param pRootElem valid pointer to the XML DOM element
 * ***********************************************************************************************************
 */
bool buildTemperaturePayload(uint8_t section, DOMDocument*& pOutDoc, DOMElement* pRootElem);

/*! ***********************************************************************************************************
 * @brief buildOpticPayload function called to build the Optic payload structure
 * @param section the section involved
 * @param pOutDoc valid pointer to the XML Doc
 * @param pRootElem valid pointer to the XML DOM element
 * ***********************************************************************************************************
 */
bool buildOpticPayload(uint8_t section, DOMDocument*& pOutDoc, DOMElement* pRootElem);

/*! ***********************************************************************************************************
 * @brief buildTreatedPressurePayload function called to build the TreatedPressure payload structure
 * @param section the section involved
 * @param strip the strip involved
 * @param pOutDoc valid pointer to the XML Doc
 * @param pRootElem valid pointer to the XML DOM element
 * ***********************************************************************************************************
 */
bool buildTreatedPressurePayload(uint8_t section, uint8_t strip, DOMDocument*& pOutDoc, DOMElement* pRootElem);

/*! ***********************************************************************************************************
 * @brief buildElaboratedPressurePayload function called to build the ElaboratedPressure payload structure
 * @param section the section involved
 * @param strip the strip involved
 * @param pOutDoc valid pointer to the XML Doc
 * @param pRootElem valid pointer to the XML DOM element
 * ***********************************************************************************************************
 */
bool buildElaboratedPressurePayload(uint8_t section, uint8_t strip, DOMDocument*& pOutDoc, DOMElement* pRootElem);

#endif // RUNWLPAYLOAD_H
