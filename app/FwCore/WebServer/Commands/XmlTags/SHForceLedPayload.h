/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SHForceLedPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SHForceLedPayload class.
 @details

 ****************************************************************************
*/

#ifndef SHFORCELEDPAYLOAD_H
#define SHFORCELEDPAYLOAD_H

#include "Payload.h"
#include "MainExecutor.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the SHFORCELED command
 * ********************************************************************************************************************
 */

class SHForceLedPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief SignalReadPayload constructor
		 * ***********************************************************************************************************
		 */
		SHForceLedPayload();

		/*! ***********************************************************************************************************
		 * @brief ~SignalReadPayload destructor
		 * ***********************************************************************************************************
		 */
		virtual ~SHForceLedPayload();

		/*! ***********************************************************************************************************
		 * @brief setOutParams set parameters
		 * @param strStart start
		 * @param strDuration duration of the command
		 * ***********************************************************************************************************
		 */
		void setOutParams(string& strStart, string& strDuration);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        string	m_strStart;
        string	m_strDuration;

};

#endif // SHFORCELEDPAYLOAD_H
