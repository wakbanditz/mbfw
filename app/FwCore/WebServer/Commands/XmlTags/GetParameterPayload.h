/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetParameterPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetParameterPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETPARAMATERPAYLOAD_H
#define GETPARAMATERPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETPARAMETER command
 * ********************************************************************************************************************
 */

class GetParameterPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		GetParameterPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~GetParameterPayload();

		/*! ***********************************************************************************************************
		 * @brief setOutValues set all the data to be included in the payload
         * @param strComponent the component identifier
         * @param strMotor     the motor identifier
         * @param strMicroRes   the microstep resolution
         * @param strAccFact    the accerleration factor
         * @param strOperCurr   the moving current
         * @param strStandbyCurr the still current
         * @param strStepFact   the step-mm conversion factor
		 * ***********************************************************************************************************
		 */
		void setOutValues(std::vector<std::string>  strComponent, std::vector<std::string> strMotor,
						  std::vector<std::string>  strMicroRes, std::vector<std::string> strAccFact,
						  std::vector<std::string>  strOperCurr, std::vector<std::string> strStandbyCurr,
						  std::vector<std::string>  strStepFact);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:
        std::vector<std::string> m_strComponent;
        std::vector<std::string> m_strMotor;
        std::vector<std::string> m_strMicrostepResolution;
        std::vector<std::string> m_strAccelerationFactor;
        std::vector<std::string> m_strOperatingCurrent;
        std::vector<std::string> m_strStandbyCurrent;
        std::vector<std::string> m_strStepConversionFactor;

};

#endif // GETPARAMATERPAYLOAD_H
