/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InitPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InitPayload class.
 @details

 ****************************************************************************
*/

#include "InitPayload.h"
#include "WebServerAndProtocolInclude.h"

InitPayload::InitPayload()
{
	m_strComponent.clear();
}

InitPayload::~InitPayload()
{
	/* Nothing to do yet */
}

void InitPayload::setComponent(const string &strComponent)
{
	m_strComponent.assign(strComponent);
}

bool InitPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_COMPONENT_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strComponent.c_str()));

	return true;
}
