/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SecRoutePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SecRoutePayload class.
 @details

 ****************************************************************************
*/

#include "SecRoutePayload.h"
#include "CommonInclude.h"
#include "WebServerAndProtocolInclude.h"

SecRoutePayload::SecRoutePayload()
{
	m_strReply.clear();
	m_strSection.clear();
}

std::__cxx11::string SecRoutePayload::getSectionReply()
{
	return m_strReply;
}

void SecRoutePayload::setSectionReply(std::__cxx11::string strReply)
{
	m_strReply.assign(strReply);
}

std::__cxx11::string SecRoutePayload::getComponent()
{
	return m_strSection;
}

void SecRoutePayload::setComponent(int section)
{
	if(section == SCT_A_ID)
	{
		m_strSection.assign(XML_TAG_SECTIONA_NAME);
	}
	else if(section == SCT_B_ID)
	{
		m_strSection.assign(XML_TAG_SECTIONB_NAME);
	}
	else if(section == NSH_ID)
	{
		m_strSection.assign(XML_TAG_NSH_NAME);
	}
	else if(section == CAMERA_ID)
	{
		m_strSection.assign(XML_TAG_CAMERA_NAME);
	}
	else if(section == INSTRUMENT_ID)
	{
		m_strSection.assign(XML_TAG_MASTERBOARD_NAME);
	}
}


bool SecRoutePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement*  pElemTmp2 = pOutDoc->createElement(X(XML_TAG_COMPONENT_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(getComponent().c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ANSWER_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(getSectionReply().c_str()));

	return true;
}

