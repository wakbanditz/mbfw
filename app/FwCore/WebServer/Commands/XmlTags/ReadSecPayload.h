/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ReadSecPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the ReadSEcPayload class.
 @details

 ****************************************************************************
*/

#ifndef READSECPAYLOAD_H
#define READSECPAYLOAD_H

#include "Payload.h"
#include "OpReadSec.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the READSEC command
 * ********************************************************************************************************************
 */

struct structReadPayload
{
	bool bIsEnabled;
	string strCode;
	string strImage;
};

struct structSampleReadPayload
{
	bool bIsEnabled;
	string strWell;
	string strScore;
	string strSampleIsPresent;
	string strImage;
};

struct structSubstratePayload
{
	bool bIsEnabled;
	string strSubsRead;
};

struct structReadSlotPayload
{
	bool bIsEnabled; // change with a struct of enabled elements all initialized to false.
	structReadPayload Spr;
	structReadPayload Strip;
	structSubstratePayload Substrate;
	structSampleReadPayload W0;
	structSampleReadPayload W3;
};

class ReadSecPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief GetSerialPayload default constructor
		 * ***********************************************************************************************************
		 */
		ReadSecPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~ReadSecPayload();


		void setParams(const string& strSection, vector<structReadSlot>* pSlotsInfo);


		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

	private:


		string m_strSection;
		vector<structReadSlotPayload> m_vPayload;
};

#endif // READSECPAYLOAD_H
