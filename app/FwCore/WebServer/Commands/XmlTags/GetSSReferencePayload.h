/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetSSReferencePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetSSReferencePayload class.
 @details

 ****************************************************************************
*/
#ifndef GETSSREFERENCEPAYLOAD_H
#define GETSSREFERENCEPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETSSREFERENCE command
 * ********************************************************************************************************************
 */

class GetSSReferencePayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		GetSSReferencePayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~GetSSReferencePayload();

		void setParameters(const string& strRFU, const string& strGain);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        string m_strRFU;
        string m_strGain;

};

#endif // GETSSREFERENCEPAYLOAD_H
