/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    XmlTag.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the XmlTag class.
 @details

 ****************************************************************************
*/

#ifndef XMLTAG_H
#define XMLTAG_H

#include <string>

/*! *******************************************************************************************************************
 * @brief basic class XmlTag
 * ********************************************************************************************************************
 */

class XmlTag
{
	public:
		XmlTag(std::string strName);
		virtual ~XmlTag();

    private:
        std::string m_strName;

};

#endif // XMLTAG_H
