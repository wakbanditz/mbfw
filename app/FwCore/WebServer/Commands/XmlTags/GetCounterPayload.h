/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetCounterPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetCounterPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETCOUNTERPAYLOAD_H
#define GETCOUNTERPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETCOUNTER command
 * ********************************************************************************************************************
 */

class GetCounterPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief GetCounterPayload default constructor
		 * ***********************************************************************************************************
		 */
		GetCounterPayload();


		/*! ***********************************************************************************************************
		 * @brief setCounter	set the Counter string
		 * @param strCounter			std::string that represents the Camera serial number
		 * ***********************************************************************************************************
		 */
		void setCounter(std::string strCounter);

		/*! ***********************************************************************************************************
		 * @brief setValue	set the Counter value
		 * @param value the Counter number
		 * ***********************************************************************************************************
		 */
		void setValue(uint16_t value);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

	private:
		std::string m_strCounter;
		uint16_t m_intValue;

};

#endif // GETCOUNTERPAYLOAD_H
