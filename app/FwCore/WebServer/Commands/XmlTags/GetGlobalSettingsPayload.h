/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetGlobalSettingsPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetGlobalSettingsPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETGLOBALSETTINGSPAYLOAD_H
#define GETGLOBALSETTINGSPAYLOAD_H

#include "CommonInclude.h"

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GET_GLOBAL_SETTINGS command
 * ********************************************************************************************************************
 */

class GetGlobalSettingsPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief GetGlobalSettingsPayload default constructor
		 * ***********************************************************************************************************
		 */
		GetGlobalSettingsPayload();


		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

		/*! *************************************************************************************************
		 * @brief TEMPERATURE DATA SETTERS as coming from GLOBAL_SETTINGS
		 * **************************************************************************************************
		 */
		void setSprMinTemperature(uint8_t section, int value);
		void setSprMaxTemperature(uint8_t section, int value);
		void setSprTargetTemperature(uint8_t section, int value);
		void setSprToleranceTemperature(uint8_t section, int value);

		void setTrayMinTemperature(uint8_t section, int value);
		void setTrayMaxTemperature(uint8_t section, int value);
		void setTrayTargetTemperature(uint8_t section, int value);
		void setTrayToleranceTemperature(uint8_t section, int value);

		void setInternalMinTemperature(int value);
		void setInternalMaxTemperature(int value);

        void setForce(bool status);

	private:

		int m_SprMin[SCT_NUM_TOT_SECTIONS], m_SprMax[SCT_NUM_TOT_SECTIONS];
		int m_SprTarget[SCT_NUM_TOT_SECTIONS], m_SprTolerance[SCT_NUM_TOT_SECTIONS];
		int m_TrayMin[SCT_NUM_TOT_SECTIONS], m_TrayMax[SCT_NUM_TOT_SECTIONS];
		int m_TrayTarget[SCT_NUM_TOT_SECTIONS], m_TrayTolerance[SCT_NUM_TOT_SECTIONS];
		int m_InternalMin, m_InternalMax;
        bool m_bForce;
};

#endif // GETGLOBALSETTINGSPAYLOAD_H
