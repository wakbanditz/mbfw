/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CaptureImagePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CaptureImagePayload class.
 @details

 ****************************************************************************
*/
#ifndef CAPTUREIMAGEPAYLOAD_H
#define CAPTUREIMAGEPAYLOAD_H

#include "Payload.h"
#include "WFCaptureImage.h"

//struct structCameraParams;

typedef struct
{
	string vFormat;
	string strLight;
	string strQuality;
	string strFocus;
	string strTop;
	string strLeft;
	string strBottom;
	string strRight;
	string strData;
} structGetPictureAttributes;

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the CAPTUREIMAGE command
 * ********************************************************************************************************************
 */

class CaptureImagePayload : public Payload
{
	public:

		CaptureImagePayload();

		virtual ~CaptureImagePayload();

		void setGetPictureAttr(int liIdx, structCameraParams& sParams, const vector<vector<unsigned char> >& vData);

		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        string m_strIdx;
        vector<structGetPictureAttributes> m_vAttributes;

};

#endif // CAPTUREIMAGEPAYLOAD_H
