/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SetMaintenanceModePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SetMaintenanceModePayload class.
 @details

 ****************************************************************************
*/

#include "SetMaintenanceModePayload.h"
#include "WebServerAndProtocolInclude.h"

SetMaintenanceModePayload::SetMaintenanceModePayload()
{
	m_strActivation.clear();
}

void SetMaintenanceModePayload::setActivation(std::__cxx11::string strActivation)
{
	m_strActivation.assign(strActivation);
}

bool SetMaintenanceModePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement*  pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ACTIVATION_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strActivation.c_str()));

	return true;
}

