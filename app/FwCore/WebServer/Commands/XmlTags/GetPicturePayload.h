#ifndef GETPICTUREPAYLOAD_H
#define GETPICTUREPAYLOAD_H

#include "Payload.h"
#include "InCmdGetPicture.h"

typedef struct
{
	string strQuality;
	string strTop;
	string strLeft;
	string strBottom;
	string strRight;
	string strData;
} structGetPictureAttributes;

class GetPicturePayload : public Payload
{
	private:

		vector<structGetPictureAttributes> m_vAttributes;
		string m_strIdx;

	public:

		GetPicturePayload();

		virtual ~GetPicturePayload();

		void setGetPictureAttr(int liIdx, int liImagesNum, int* liImageQuality, structCoordinates* pCoord, const vector<vector<unsigned char> >& vData);

		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

	private:

		// This type of encoding consists in dividing the streams in group of 6bites to be rapresented with a
		// base 64 logic ( 2^6 ). It is important to avoid NULL char to be sent.
		bool encodeDataBase64(const unsigned char* pucData, int liBufLen, string& strEncoded);
};

#endif // GETPICTUREPAYLOAD_H
