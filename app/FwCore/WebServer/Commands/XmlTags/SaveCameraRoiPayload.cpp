/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SaveCameraRoiPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SaveCameraRoiPayload class.
 @details

 ****************************************************************************
*/

#include "SaveCameraRoiPayload.h"
#include "WebServerAndProtocolInclude.h"

SaveCameraRoiPayload::SaveCameraRoiPayload()
{
	m_mapSubSet.clear();
}

SaveCameraRoiPayload::~SaveCameraRoiPayload()
{

}

void SaveCameraRoiPayload::setAttributes(const unordered_map<string, structRect>& mConfig)
{
	m_mapSubSet = mConfig;
}

bool SaveCameraRoiPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	for ( const auto &it : m_mapSubSet )
	{
		// Need to convert the coordinates. SSW expects top - left - bottom - right
		// I have x0, y0, width, height

		int liTop = it.second.y;
		int liBottom = liTop + it.second.height;
		int liLeft = it.second.x;
		int liRight = liLeft + it.second.width;

		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ROI_NAME));
		pElemTmp1->appendChild(pElemTmp2);

		XStr xstrIdNameReal(XML_TAG_PROTO_NAME);
		XStr xstrIdValueReal(it.first.c_str());
		pElemTmp2->setAttribute(xstrIdNameReal.unicodeForm(), xstrIdValueReal.unicodeForm());

		DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TOP_NAME));
		pElemTmp2->appendChild(pElemTmp3);
		pElemTmp3->setTextContent(X(to_string(liTop).c_str()));

		DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_LEFT_NAME));
		pElemTmp2->appendChild(pElemTmp4);
		pElemTmp4->setTextContent(X(to_string(liLeft).c_str()));

		DOMElement* pElemTmp5 = pOutDoc->createElement(X(XML_TAG_BOTTOM_NAME));
		pElemTmp2->appendChild(pElemTmp5);
		pElemTmp5->setTextContent(X(to_string(liBottom).c_str()));

		DOMElement* pElemTmp6 = pOutDoc->createElement(X(XML_TAG_RIGHT_NAME));
		pElemTmp2->appendChild(pElemTmp6);
		pElemTmp6->setTextContent(X(to_string(liRight).c_str()));
	}

	return true;
}
