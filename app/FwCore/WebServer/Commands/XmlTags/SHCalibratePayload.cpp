/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SHCalibratePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SHCalibratePayload class.
 @details

 ****************************************************************************
*/

#include "SHCalibratePayload.h"
#include "WebServerAndProtocolInclude.h"

SHCalibratePayload::SHCalibratePayload()
{
	m_strSection.clear();
	m_vsTheoricItem.clear();
	m_vsRealItem.clear();
}

SHCalibratePayload::~SHCalibratePayload()
{

}

bool SHCalibratePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);
	XStr xstrName(XML_TAG_SECTION_NAME);
	XStr xstrValue(m_strSection.c_str());
	pElemTmp1->setAttribute(xstrName.unicodeForm(), xstrValue.unicodeForm());

	// First for theoric values
	for(uint8_t i = 0; i < m_vsTheoricItem.size(); i++)
	{
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_THEORIC_POSITION));
		pElemTmp1->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(m_vsTheoricItem[i].second.c_str())); // qui second di theoric item

		XStr xstrComponentName(XML_ATTR_POSITION_NAME);
		XStr xstrComponentValue(m_vsTheoricItem[i].first.c_str());
		pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm()); // qui first di theoric name
	}

	// then for real values
	for(uint8_t i = 0; i < m_vsRealItem.size(); i++)
	{
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_REAL_POSITION));
		pElemTmp1->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(m_vsRealItem[i].second.c_str())); // qui second di theoric item

		XStr xstrComponentName(XML_ATTR_POSITION_NAME);
		XStr xstrComponentValue(m_vsRealItem[i].first.c_str());
		pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm()); // qui first di theoric name
	}

	return true;
}

void SHCalibratePayload::setOutParams(string& strSection, const vector<pair<string, string>>& vTheoricItem, const vector<pair<string, string>>& vRealItem)
{
	m_strSection.assign(strSection);
	m_vsTheoricItem = vTheoricItem;
	m_vsRealItem = vRealItem;
}
