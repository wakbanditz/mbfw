/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetCounterPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetCounterPayload class.
 @details

 ****************************************************************************
*/

#include "GetCounterPayload.h"
#include "WebServerAndProtocolInclude.h"

GetCounterPayload::GetCounterPayload()
{
	m_strCounter.clear();
	m_intValue = 0;
}

void GetCounterPayload::setCounter(string strCounter)
{
	m_strCounter.assign(strCounter);
}

void GetCounterPayload::setValue(uint16_t value)
{
	m_intValue = value;
}

bool GetCounterPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	if(pOutDoc == 0 || pRootElem == 0)
		return false;

	DOMElement*  pElemTmp = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp);

	DOMElement*  pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PROTO_ID));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strCounter.c_str()));

	std::string strValue;
	strValue.assign(std::to_string(m_intValue));
	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE_NAME));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(strValue.c_str()));

	return true;
}
