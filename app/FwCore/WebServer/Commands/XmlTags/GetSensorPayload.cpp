/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetSensorPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetSensorPayload class.
 @details

 ****************************************************************************
*/

#include "GetSensorPayload.h"
#include "WebServerAndProtocolInclude.h"

GetSensorPayload::GetSensorPayload()
{
	m_vecAttribute.clear();
	m_strCategory.clear();
	m_strComponent.clear();
	m_strId.clear();
}

GetSensorPayload::~GetSensorPayload()
{
	m_vecAttribute.clear();
}

void GetSensorPayload::setSensorValuesSection(const int section,
											  const structPayloadSectionSensorValues &sensorValues)
{
	string strSectionPrefix;
	string strSection;

	if(section == SCT_A_ID)
	{
		strSectionPrefix.assign(SENSOR_ID_PREFIX_SECTION_A);
		strSection.assign(SENSOR_COMPONENT_SECTION_A);
	}
	else
	{
		strSectionPrefix.assign(SENSOR_ID_PREFIX_SECTION_B);
		strSection.assign(SENSOR_COMPONENT_SECTION_B);
	}

	addFieldCmd(m_vecAttribute, strSectionPrefix + XML_TAG_MOTOR_NAME_SPR + SENSOR_ID_NAME_HOME, SENSOR_CATEGORY_HOME,
				strSection, SENSOR_UNIT_UNDEFINED, "", "", sensorValues.strSprHome);
	addFieldCmd(m_vecAttribute, strSectionPrefix + XML_TAG_MOTOR_NAME_PUMP + SENSOR_ID_NAME_HOME, SENSOR_CATEGORY_HOME,
				strSection, SENSOR_UNIT_UNDEFINED, "", "", sensorValues.strPumpHome);
	addFieldCmd(m_vecAttribute, strSectionPrefix + XML_TAG_MOTOR_NAME_TRAY + SENSOR_ID_NAME_HOME, SENSOR_CATEGORY_HOME,
				strSection, SENSOR_UNIT_UNDEFINED, "", "", sensorValues.strTrayHome);
	addFieldCmd(m_vecAttribute, strSectionPrefix + XML_TAG_MOTOR_NAME_TOWER + SENSOR_ID_NAME_HOME, SENSOR_CATEGORY_HOME,
				strSection, SENSOR_UNIT_UNDEFINED, "", "", sensorValues.strTowerHome);

	addFieldCmd(m_vecAttribute, strSectionPrefix + XML_TAG_MOTOR_NAME_TRAY + SENSOR_ID_NAME_NTC, SENSOR_CATEGORY_TEMPERATURE,
				strSection, SENSOR_UNIT_DEGREES, "0", "2", sensorValues.strTrayNtc);
	addFieldCmd(m_vecAttribute, strSectionPrefix + XML_TAG_MOTOR_NAME_SPR + SENSOR_ID_NAME_NTC, SENSOR_CATEGORY_TEMPERATURE,
				strSection, SENSOR_UNIT_DEGREES, "0", "2", sensorValues.strSprNtc);

	addFieldCmd(m_vecAttribute, strSectionPrefix + SENSOR_ID_NAME_DOOR, SENSOR_CATEGORY_DOOR,
				strSection, SENSOR_UNIT_UNDEFINED, "", "", sensorValues.strDoor);
}

void GetSensorPayload::setVoltageValuesSection(const int section,
											  const structPayloadSectionVoltageValues &voltageValues)
{
	string strSectionPrefix;
	string strSection;

	if(section == SCT_A_ID)
	{
		strSectionPrefix.assign(SENSOR_ID_PREFIX_SECTION_A);
		strSection.assign(SENSOR_COMPONENT_SECTION_A);
	}
	else
	{
		strSectionPrefix.assign(SENSOR_ID_PREFIX_SECTION_B);
		strSection.assign(SENSOR_COMPONENT_SECTION_B);
	}

	addFieldCmd(m_vecAttribute, strSectionPrefix + SENSOR_ID_NAME_24V, SENSOR_CATEGORY_VOLTAGE,
				strSection, SENSOR_UNIT_VOLT, "", "3", voltageValues.str24V);
	addFieldCmd(m_vecAttribute, strSectionPrefix + SENSOR_ID_NAME_5V, SENSOR_CATEGORY_VOLTAGE,
				strSection, SENSOR_UNIT_VOLT, "", "3", voltageValues.str5V);
	addFieldCmd(m_vecAttribute, strSectionPrefix + SENSOR_ID_NAME_3V3, SENSOR_CATEGORY_VOLTAGE,
				strSection, SENSOR_UNIT_VOLT, "", "3", voltageValues.str3V3);
	addFieldCmd(m_vecAttribute, strSectionPrefix + SENSOR_ID_NAME_1V2, SENSOR_CATEGORY_VOLTAGE,
				strSection, SENSOR_UNIT_VOLT, "", "3", voltageValues.str1V2);

}

void GetSensorPayload::setSensorValuesNsh(const structPayloadNshSensorValues& sensorValues)
{
	addFieldCmd(m_vecAttribute, std::string(SENSOR_ID_PREFIX_NSH) + std::string(SENSOR_ID_NAME_HOME),
				SENSOR_CATEGORY_HOME, SENSOR_COMPONENT_NSH, SENSOR_UNIT_UNDEFINED, "", "", sensorValues.strNSHHome);
}

void GetSensorPayload::setSensorValuesInstrument(const structPayloadInstrumentSensorValues& sensorValues)
{
	addFieldCmd(m_vecAttribute, std::string(SENSOR_ID_PREFIX_SECTION_A) + std::string(SENSOR_ID_NAME_FAN),
				SENSOR_CATEGORY_FAN, SENSOR_COMPONENT_INSTRUMENT, SENSOR_UNIT_RPM, "0", "0", sensorValues.strSAFan);
	addFieldCmd(m_vecAttribute, std::string(SENSOR_ID_PREFIX_SECTION_A) + std::string(SENSOR_ID_NAME_NTC),
				SENSOR_CATEGORY_TEMPERATURE, SENSOR_COMPONENT_INSTRUMENT, SENSOR_UNIT_DEGREES, "0", "2", sensorValues.strSANtc);

	addFieldCmd(m_vecAttribute, std::string(SENSOR_ID_PREFIX_SECTION_B) + std::string(SENSOR_ID_NAME_FAN),
				SENSOR_CATEGORY_FAN, SENSOR_COMPONENT_INSTRUMENT, SENSOR_UNIT_RPM, "0", "0", sensorValues.strSBFan);
	addFieldCmd(m_vecAttribute, std::string(SENSOR_ID_PREFIX_SECTION_B) + std::string(SENSOR_ID_NAME_NTC),
				SENSOR_CATEGORY_TEMPERATURE, SENSOR_COMPONENT_INSTRUMENT, SENSOR_UNIT_DEGREES, "0", "2", sensorValues.strSBNtc);

	addFieldCmd(m_vecAttribute, std::string(SENSOR_ID_PREFIX_INSTRUMENT) + std::string(SENSOR_ID_NAME_FAN),
				SENSOR_CATEGORY_FAN, SENSOR_COMPONENT_INSTRUMENT, SENSOR_UNIT_RPM, "0", "0", sensorValues.strInstrFan);
	addFieldCmd(m_vecAttribute, std::string(SENSOR_ID_PREFIX_INSTRUMENT) + std::string(SENSOR_ID_NAME_NTC),
				SENSOR_CATEGORY_TEMPERATURE, SENSOR_COMPONENT_INSTRUMENT, SENSOR_UNIT_DEGREES, "0", "2", sensorValues.strInstrNtc);
}


void GetSensorPayload::addFieldCmd(vector<structGetSensorAttribute>& cmd, const string& strId,
									const string& strCategory, const string& strComponent,
									const string& strUnit, const string& strOffset,
									const string& strDecimals, const string &strVal )
{
	structGetSensorAttribute	stTmpSensorAttr;

	stTmpSensorAttr.strId.assign(strId);
	stTmpSensorAttr.strCategory.assign(strCategory);
	stTmpSensorAttr.strComponent.assign(strComponent);
	stTmpSensorAttr.strUnit.assign(strUnit);
	stTmpSensorAttr.strOffset.assign(strOffset);
	stTmpSensorAttr.strDecimals.assign(strDecimals);

	stTmpSensorAttr.strValue.assign(strVal);

	cmd.push_back(stTmpSensorAttr);
}

void GetSensorPayload::setOutCmdByParameterRequested(const string &strCategory,
													 const string &strComponent, const string &strId)
{
	m_strCategory.assign(strCategory);
	m_strComponent.assign(strComponent);
	m_strId.assign(strId);
}

bool GetSensorPayload::composePayload(DOMDocument *&pOutDoc, DOMElement *pRootElem)
{
	//Payload element
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	for (size_t i = 0 ; i < m_vecAttribute.size(); i++)
	{
		if(m_strCategory.compare("ALL") == 0 || m_strCategory.empty() ||
				m_strCategory.compare(m_vecAttribute.at(i).strCategory) == 0)
		{
			if(m_strComponent.compare("ALL") == 0 || m_strComponent.empty() ||
					m_strComponent.compare(m_vecAttribute.at(i).strComponent) == 0)
			{
				if(m_strId.compare("ALL") == 0 || m_strId.empty() ||
						m_strId.compare(m_vecAttribute.at(i).strId) == 0)
				{
					//Sensor element
					DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SENSOR_NAME));
					pElemTmp1->appendChild(pElemTmp2);
					pElemTmp2->setTextContent(X(m_vecAttribute[i].strValue.c_str()));

					//Sensor attributes
					XStr xstrIdName(XML_ATTR_ID_NAME);
					XStr xstrIdValue(m_vecAttribute[i].strId.c_str());
					pElemTmp2->setAttribute(xstrIdName.unicodeForm(), xstrIdValue.unicodeForm());

					XStr xstrCategoryName(XML_ATTR_CATEGORY_NAME);
					XStr xstrCategoryValue(m_vecAttribute[i].strCategory.c_str());
					pElemTmp2->setAttribute(xstrCategoryName.unicodeForm(), xstrCategoryValue.unicodeForm());

					XStr xstrComponentName(XML_ATTR_COMPONENT_NAME);
					XStr xstrComponentValue(m_vecAttribute[i].strComponent.c_str());
					pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

					XStr xstrUnitName(XML_ATTR_UNIT_NAME);
					XStr xstrUnitValue(m_vecAttribute[i].strUnit.c_str());
					pElemTmp2->setAttribute(xstrUnitName.unicodeForm(), xstrUnitValue.unicodeForm());

					if(m_vecAttribute[i].strOffset.empty() == false)
					{
						XStr xstrOffsetName(XML_ATTR_OFFSET_NAME);
						XStr xstrOffsetValue(m_vecAttribute[i].strOffset.c_str());
						pElemTmp2->setAttribute(xstrOffsetName.unicodeForm(), xstrOffsetValue.unicodeForm());
					}

					if(m_vecAttribute[i].strDecimals.empty() == false)
					{
						XStr xstrDecName(XML_ATTR_DECIMALS_NAME);
						XStr xstrDecValue(m_vecAttribute[i].strDecimals.c_str());
						pElemTmp2->setAttribute(xstrDecName.unicodeForm(), xstrDecValue.unicodeForm());
					}
				}
			}
		}
	}
	return true;
}
