/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OPTCheckPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OPTCheckPayload class.
 @details

 ****************************************************************************
*/

#include "OPTCheckPayload.h"
#include "WebServerAndProtocolInclude.h"

// Payload not used !

OPTCheckPayload::OPTCheckPayload()
{
    m_intReadA = m_intReadB = 0;
}

OPTCheckPayload::~OPTCheckPayload()
{

}

void OPTCheckPayload::setAttributes(const int readA, const int readB)
{
    m_intReadA = readA;
    m_intReadB = readB;
}

bool OPTCheckPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
    DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
    pRootElem->appendChild(pElemTmp1);

    if(m_intReadA > 0)
    {
        DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
        pElemTmp1->appendChild(pElemTmp2);

        XStr xstrComponentName(XML_ATTR_LETTER_NAME);
        XStr xstrComponentValue(XML_VALUE_SECTION_A);
        pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

        DOMElement* pElemTmp3 = pOutDoc->createElement(X("Value"));
        pElemTmp2->appendChild(pElemTmp3);
        pElemTmp3->setTextContent(X(to_string(m_intReadA).c_str()));

    }

    if(m_intReadB > 0)
    {
        DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
        pElemTmp1->appendChild(pElemTmp2);

        XStr xstrComponentName(XML_ATTR_LETTER_NAME);
        XStr xstrComponentValue(XML_VALUE_SECTION_B);
        pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

        DOMElement* pElemTmp3 = pOutDoc->createElement(X("Value"));
        pElemTmp2->appendChild(pElemTmp3);
        pElemTmp3->setTextContent(X(to_string(m_intReadB).c_str()));

    }

    return true;
}

