/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SleepPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SleepPayload class.
 @details

 ****************************************************************************
*/

#include "SleepPayload.h"
#include "WebServerAndProtocolInclude.h"

SleepPayload::SleepPayload()
{
	m_bSleep = false;
}

SleepPayload::~SleepPayload()
{
}

bool SleepPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_INSTRUMENT_NAME));
	pElemTmp1->appendChild(pElemTmp2);

	std::string strStatus;
	if(m_bSleep)
	{
		strStatus.assign(XML_ATTR_STATUS_ENABLED);
	}
	else
	{
		strStatus.assign(XML_ATTR_STATUS_DISABLED);
	}
	pElemTmp2->setTextContent(X(strStatus.c_str()));

	return true;
}

void SleepPayload::setSleepActive(bool status)
{
	m_bSleep = status;
}
