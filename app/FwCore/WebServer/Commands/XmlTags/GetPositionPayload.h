/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetPositionPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetPositionPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETPOSITIONPAYLOAD_H
#define GETPOSITIONPAYLOAD_H

#include "Payload.h"

typedef struct
{
    string	strComponent;
    string	strMotor;
    string	strPosition;

} structMoveCmdOutValues;

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETPOSITION command
 * ********************************************************************************************************************
 */

class GetPositionPayload : public Payload
{
    public:

        /*! ***********************************************************************************************************
         * @brief GetPositionPayload constructor
         * ***********************************************************************************************************
         */
        GetPositionPayload();

        /*! ***********************************************************************************************************
         * @brief ~MovePayload destructor
         * ***********************************************************************************************************
         */
        virtual ~GetPositionPayload();

        /*! ***********************************************************************************************************
         * @brief setOutValues
         * @param valuesMoveOut the structure containing data
         * ***********************************************************************************************************
         */
        void setOutValuesStruct(const structMoveCmdOutValues& valuesMoveOut);

        /*! ***********************************************************************************************************
         * @brief setOutValues to set in the payload object the vectors to be included in the message
         * @param strComponent the Component vector
         * @param strMotor the Motor vector
         * @param strPosition the Position vector
         * ***********************************************************************************************************
         */
        void setOutValues(std::vector<std::string> strComponent,
                          std::vector<std::string> strMotor,
                          std::vector<std::string> strPosition);


        /*! ***********************************************************************************************************
         * @brief composePayload function called to build the payload structure
         * @param pOutDoc valid pointer to the XML Doc
         * @param pRootElem valid pointer to the XML DOM element
         * ***********************************************************************************************************
         */
        bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:
        std::vector<std::string>	m_strComponent;
        std::vector<std::string>	m_strMotor;
        std::vector<std::string>	m_strPosition;

};


#endif // GETPOSITIONPAYLOAD_H
