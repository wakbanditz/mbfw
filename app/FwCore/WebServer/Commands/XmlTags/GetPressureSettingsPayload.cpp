/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetPressureSettingsPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetPressureSettingsPayload class.
 @details

 ****************************************************************************
*/

#include "GetPressureSettingsPayload.h"
#include "WebServerAndProtocolInclude.h"
#include "CommonInclude.h"

GetPressureSettingsPayload::GetPressureSettingsPayload()
{
	m_strComponent.clear();
	m_strConversions.clear();
	m_strPressures.clear();
	m_strConversionFactor.clear();
	m_strPressureFactor.clear();
}

GetPressureSettingsPayload::~GetPressureSettingsPayload()
{
	m_strComponent.clear();
	m_strConversions.clear();
	m_strPressures.clear();
}

void GetPressureSettingsPayload::setOutValues(std::vector<std::string> strComponent,
											  std::vector<std::string> strConversions,
											  std::vector<std::string> strPressures,
											  std::string strConversionFactor,
											  std::string strPressureFactor)
{
	m_strComponent = strComponent;
	m_strConversions = strConversions;
	m_strPressures = strPressures;
	m_strConversionFactor.assign(strConversionFactor);
	m_strPressureFactor.assign(strPressureFactor);
}

bool GetPressureSettingsPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	uint8_t counter;

	for(counter = 0; counter < m_strComponent.size(); counter++)
	{
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
		pElemTmp1->appendChild(pElemTmp2);

		XStr xstrComponentName(XML_ATTR_LETTER_NAME);
		XStr xstrComponentValue(m_strComponent.at(counter).c_str());
		pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

		for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
		{
			DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_SLOT_NAME));
			pElemTmp2->appendChild(pElemTmp3);

			std::string strChannel = std::to_string(channel + 1);

			XStr xstrSlotName(XML_TAG_PROTO_NUMBER);
			XStr xstrSlotValue(strChannel.c_str());
			pElemTmp3->setAttribute(xstrSlotName.unicodeForm(), xstrSlotValue.unicodeForm());

			DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_CONVERSION_FACTOR_NAME));
			pElemTmp3->appendChild(pElemTmp4);

			XStr xstrDecimalName1(XML_ATTR_DECIMALS_NAME);
			XStr xstrDecimalValue1(m_strConversionFactor.c_str());
			pElemTmp4->setAttribute(xstrDecimalName1.unicodeForm(), xstrDecimalValue1.unicodeForm());

			XStr xstrConversionValue(m_strConversions.at(counter * SCT_NUM_TOT_SLOTS + channel).c_str());
			pElemTmp4->setTextContent(xstrConversionValue.unicodeForm());

			DOMElement* pElemTmp5 = pOutDoc->createElement(X(XML_TAG_VALUE_NAME));
			pElemTmp3->appendChild(pElemTmp5);

			XStr xstrDecimalName2(XML_ATTR_DECIMALS_NAME);
			XStr xstrDecimalValue2(m_strPressureFactor.c_str());
			pElemTmp5->setAttribute(xstrDecimalName2.unicodeForm(), xstrDecimalValue2.unicodeForm());

			XStr xstrPressureValue(m_strPressures.at(counter * SCT_NUM_TOT_SLOTS + channel).c_str());
			pElemTmp5->setTextContent(xstrPressureValue.unicodeForm());
		}
	}

	return true;
}



