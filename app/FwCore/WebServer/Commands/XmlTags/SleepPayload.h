/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SleepPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SleepPayload class.
 @details

 ****************************************************************************
*/

#ifndef SLEEPPAYLOAD_H
#define SLEEPPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the SLEEP command
 * ********************************************************************************************************************
 */

class SleepPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		SleepPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~SleepPayload();

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

		/*! ***********************************************************************************************************
		 * @brief setSleepActive	set the action to perform
		 * @param status true -> goto sleep, false -> exit from sleep
		 * ************************************************************************************************************
		 */
		void setSleepActive(bool status);

    private:

        bool m_bSleep;


};

#endif // SLEEPPAYLOAD_H
