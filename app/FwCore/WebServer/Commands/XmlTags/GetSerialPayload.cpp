/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetSerialPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetSerialPayload class.
 @details

 ****************************************************************************
*/

#include "GetSerialPayload.h"
#include "WebServerAndProtocolInclude.h"

GetSerialPayload::GetSerialPayload()
{
	m_strSerialInstrument.clear();
	m_strSerialMasterBoard.clear();
	m_strSerialNSH.clear();
	m_strSerialSectionA.clear();
	m_strSerialSectionB.clear();
	m_strSerialCamera.clear();
}

std::__cxx11::string GetSerialPayload::getSerialInstrument()
{
	return m_strSerialInstrument;
}

void GetSerialPayload::setSerialInstrument(std::__cxx11::string strName)
{
	m_strSerialInstrument.assign(strName);
}

std::__cxx11::string GetSerialPayload::getSerialMasterBoard()
{
	return m_strSerialMasterBoard;
}

void GetSerialPayload::setSerialMasterBoard(std::__cxx11::string strName)
{
	m_strSerialMasterBoard.assign(strName);
}

std::__cxx11::string GetSerialPayload::getSerialNSH()
{
	return m_strSerialNSH;
}

void GetSerialPayload::setSerialNSH(std::__cxx11::string strName)
{
	m_strSerialNSH.assign(strName);
}

std::__cxx11::string GetSerialPayload::getSerialSectionA()
{
	return m_strSerialSectionA;
}

void GetSerialPayload::setSerialSectionA(std::__cxx11::string strName)
{
	m_strSerialSectionA.assign(strName);
}

std::__cxx11::string GetSerialPayload::getSerialSectionB()
{
	return m_strSerialSectionB;
}

void GetSerialPayload::setSerialSectionB(std::__cxx11::string strName)
{
	m_strSerialSectionB.assign(strName);
}

std::__cxx11::string GetSerialPayload::getSerialCamera()
{
	return m_strSerialCamera;
}

void GetSerialPayload::setSerialCamera(std::__cxx11::string strName)
{
	m_strSerialCamera.assign(strName);
}

bool GetSerialPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	if(pOutDoc == 0 || pRootElem == 0)
		return false;

	DOMElement*  pElemTmp = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp);

	DOMElement*  pElemTmp2 = pOutDoc->createElement(X(XML_TAG_INSTRUMENT_NAME));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strSerialInstrument.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_MASTERBOARD_NAME));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strSerialMasterBoard.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_NSH_NAME));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strSerialNSH.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTIONA_NAME));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strSerialSectionA.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTIONB_NAME));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strSerialSectionB.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_CAMERA_NAME));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strSerialCamera.c_str()));

	return true;
}
