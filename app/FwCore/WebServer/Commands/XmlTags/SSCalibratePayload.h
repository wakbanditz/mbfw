/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SSCalibratePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SSCalibratePayload class.
 @details

 ****************************************************************************
*/
#ifndef SSCALIBRATEPAYLOAD_H

#define SSCALIBRATEPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the SSCALIBRATE command
 * ********************************************************************************************************************
 */

class SSCalibratePayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		SSCalibratePayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~SSCalibratePayload();

		void setPositions(const string& strName, const string& strTheorPos, const string strRealPos);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        string m_strTheorPos;
        string m_strRealPos;
        string m_strName;

};

#endif // SSCALIBRATEPAYLOAD_H
