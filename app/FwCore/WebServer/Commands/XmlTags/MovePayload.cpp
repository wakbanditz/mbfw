/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    MovePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the MovePayload class.
 @details

 ****************************************************************************
*/

#include "MovePayload.h"
#include "WebServerAndProtocolInclude.h"

MovePayload::MovePayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strPosition.clear();
}

MovePayload::~MovePayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strPosition.clear();
}

void MovePayload::setOutValuesStruct(const structMoveCmdOutValues &valuesMoveOut)
{	
	m_strComponent.push_back(valuesMoveOut.strComponent);
	m_strMotor.push_back(valuesMoveOut.strMotor);
	m_strPosition.push_back(valuesMoveOut.strPosition);
}

void MovePayload::setOutValues(std::vector<string> strComponent, std::vector<string> strMotor,
							   std::vector<string> strPosition)
{
	m_strComponent = strComponent;
	m_strMotor = strMotor;
	m_strPosition = strPosition;
}


bool MovePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	uint8_t counter;

	for(counter = 0; counter < m_strComponent.size(); counter++)
	{
		DOMElement* pElemTmp2;

		pElemTmp2 = pOutDoc->createElement(X(XML_ATTR_COMPONENT_NAME));
		pElemTmp1->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(m_strComponent.at(counter).c_str()));

		pElemTmp2 = pOutDoc->createElement(X(XML_ATTR_MOTOR_NAME));
		pElemTmp1->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(m_strMotor.at(counter).c_str()));

		pElemTmp2 = pOutDoc->createElement(X(XML_ATTR_POSITION_NAME));
		pElemTmp1->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(m_strPosition.at(counter).c_str()));
	}
	return true;
}
