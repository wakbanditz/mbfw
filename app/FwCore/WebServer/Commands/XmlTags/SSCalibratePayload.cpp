/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SSCalibratePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SSCalibratePayload class.
 @details

 ****************************************************************************
*/

#include "SSCalibratePayload.h"
#include "WebServerAndProtocolInclude.h"

SSCalibratePayload::SSCalibratePayload()
{
	m_strTheorPos.clear();
	m_strRealPos.clear();
	m_strName.clear();
}

SSCalibratePayload::~SSCalibratePayload()
{

}

void SSCalibratePayload::setPositions(const string& strName, const string& strTheorPos, const string strRealPos)
{
	m_strTheorPos = strTheorPos;
	m_strRealPos = strRealPos;
	m_strName = strName;
}

bool SSCalibratePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_THEORIC_POSITION));
	pElemTmp1->appendChild(pElemTmp2);
	XStr xstrIdName(XML_ATTR_POSITION_NAME);
	XStr xstrIdValue(m_strName.c_str());
	pElemTmp2->setAttribute(xstrIdName.unicodeForm(), xstrIdValue.unicodeForm());
	pElemTmp2->setTextContent(X(m_strTheorPos.c_str()));

	DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_REAL_POSITION));
	pElemTmp1->appendChild(pElemTmp3);
	XStr xstrIdNameReal(XML_ATTR_POSITION_NAME);
	XStr xstrIdValueReal(m_strName.c_str());
	pElemTmp3->setAttribute(xstrIdNameReal.unicodeForm(), xstrIdValueReal.unicodeForm());
	pElemTmp3->setTextContent(X(m_strRealPos.c_str()));

	return true;
}
