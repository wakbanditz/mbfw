/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    DisablePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the DisablePayload class.
 @details

 ****************************************************************************
*/
#ifndef DISABLEPAYLOAD_H
#define DISABLEPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the DISABLE command
 * ********************************************************************************************************************
 */

class DisablePayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		DisablePayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~DisablePayload();

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

		/*! ***********************************************************************************************************
		 * @brief setInstrumentDisabled set the Disabled flag on instrument
		 * @param status true if component disabled, false if enabled
		 * ***********************************************************************************************************
		 */
		void setInstrumentDisabled(bool status);

		/*! ***********************************************************************************************************
		 * @brief setSectionADisabled set the Disabled flag on instrument
		 * @param status true if component disabled, false if enabled
		 * ***********************************************************************************************************
		 */
		void setSectionADisabled(bool status);

		/*! ***********************************************************************************************************
		 * @brief setSectionBDisabled set the Disabled flag on instrument
		 * @param status true if component disabled, false if enabled
		 * ***********************************************************************************************************
		 */
		void setSectionBDisabled(bool status);

		/*! ***********************************************************************************************************
		 * @brief setCameraDisabled set the Disabled flag on instrument
		 * @param status true if component disabled, false if enabled
		 * ***********************************************************************************************************
		 */
		void setCameraDisabled(bool status);

		/*! ***********************************************************************************************************
		 * @brief setNshDisabled set the Disabled flag on instrument
		 * @param status true if component disabled, false if enabled
		 * ***********************************************************************************************************
		 */
		void setNshDisabled(bool status);

    private:

        bool m_bInstrumentDisabled, m_bSectionADisabled, m_bSectionBDisabled, m_bCameraDisabled, m_bNshDisabled;

};


#endif // DISABLEPAYLOAD_H
