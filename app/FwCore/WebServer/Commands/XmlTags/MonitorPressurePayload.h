/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    MonitorPressurePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the MonitorPressurePayload class.
 @details

 ****************************************************************************
*/

#ifndef MONITORPRESSUREPAYLOAD_H
#define MONITORPRESSUREPAYLOAD_H

#include <deque>

#include "CommonInclude.h"
#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the MONITOR_PRESSURE command
 * ********************************************************************************************************************
 */
class MonitorPressurePayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief MonitorPressurePayload default constructor
		 * ***********************************************************************************************************
		 */
		MonitorPressurePayload();


		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

		/*! ***********************************************************************************************************
		 * @brief setSection set the involved section
		 * @param section the section index
		 * ***********************************************************************************************************
		 */
		void setSection(int section);

		/*! ***********************************************************************************************************
		 * @brief assignPressureData copy the data vector to the local array
		 * @param channel the channel index
		 * @param pDataPressureChannel pointer to the input array
		 * @return true if success, false otherwise
		 * ***********************************************************************************************************
		 */
		bool assignPressureData(int channel, std::deque<uint32_t> * pDataPressureChannel);

	private:

		int m_section;
		std::deque<uint32_t> m_dataPressureChannel[SCT_NUM_TOT_SLOTS];

};


#endif // MONITORPRESSUREPAYLOAD_H
