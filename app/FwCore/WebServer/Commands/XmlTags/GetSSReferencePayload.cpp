/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetSSReferencePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetSSReferencePayload class.
 @details

 ****************************************************************************
*/

#include "GetSSReferencePayload.h"
#include "WebServerAndProtocolInclude.h"

GetSSReferencePayload::GetSSReferencePayload()
{
	m_strGain.clear();
	m_strRFU.clear();
}

GetSSReferencePayload::~GetSSReferencePayload()
{

}

void GetSSReferencePayload::setParameters(const string& strRFU, const string& strGain)
{
	m_strRFU = strRFU;
	m_strGain = strGain;
}

bool GetSSReferencePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PROTO_RFU_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strRFU.c_str()));

	DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_PROTO_GAIN_NAME));
	pElemTmp1->appendChild(pElemTmp3);
	XStr xstrIdNameReal(XML_ATTR_DECIMALS_NAME); // Or integer ??

	string strDecNum = "2"; // ask Fero for dec num
	XStr xstrIdValueReal(strDecNum.c_str());
	pElemTmp3->setAttribute(xstrIdNameReal.unicodeForm(), xstrIdValueReal.unicodeForm());
	pElemTmp3->setTextContent(X(m_strGain.c_str()));

	return true;
}
