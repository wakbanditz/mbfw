/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetVidasEpPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetVidasEpPayload class.
 @details

 ****************************************************************************
*/

#include "GetVidasEpPayload.h"
#include "WebServerAndProtocolInclude.h"
#include "CommonInclude.h"
#include "ErrorUtilities.h"


GetVidasEpPayload::GetVidasEpPayload()
{
	m_strNshStatus.clear();
	m_strSectionAStatus.clear();
	m_strSectionBStatus.clear();
	m_strCameraStatus.clear();
	m_strInstrumentStatus.clear();

	m_intSectionADoor = 0;
	m_intSectionBDoor = 0;

	m_intSectionASprTemperature = 0;
	m_intSectionBSprTemperature = 0;
	m_intSectionATrayTemperature = 0;
	m_intSectionBTrayTemperature = 0;
	m_intInstrumentTemperature = 0;

	m_strSectionAEndruntime.clear();
	m_strSectionBEndruntime.clear();

	m_strTimestamp.clear();

	m_strStartButtonA.clear();
	m_strStartButtonB.clear();

	m_vectorErrors.clear();

    m_strOPTid.clear();
}

void GetVidasEpPayload::setInstrumentStatus(string strStatus)
{
	m_strInstrumentStatus.assign(strStatus);
}

void GetVidasEpPayload::setNshStatus(std::string strStatus)
{
	m_strNshStatus.assign(strStatus);
}

void GetVidasEpPayload::setCameraStatus(std::string strStatus)
{
	m_strCameraStatus.assign(strStatus);
}

void GetVidasEpPayload::setSectionAStatus(std::string strStatus)
{
	m_strSectionAStatus.assign(strStatus);
}

void GetVidasEpPayload::setSectionBStatus(std::string strStatus)
{
	m_strSectionBStatus.assign(strStatus);
}

void GetVidasEpPayload::setTimestamp(std::__cxx11::string strTime)
{
	m_strTimestamp.assign(strTime);
}

void GetVidasEpPayload::setSectionEndTime(uint8_t section, string strTime)
{
	if(section == SCT_A_ID)
	{
		m_strSectionAEndruntime.assign(strTime);
	}
	else if(section == SCT_B_ID)
	{
		m_strSectionBEndruntime.assign(strTime);
	}
}

void GetVidasEpPayload::setSectionTemperature(uint8_t section, uint16_t temperatureTray, uint16_t temperatureSpr)
{
	// temperature is converted from double to integer in 0.01 degrees
	if(section == SCT_A_ID)
	{
		m_intSectionASprTemperature = temperatureSpr;
		m_intSectionATrayTemperature = temperatureTray;
	}
	else if(section == SCT_B_ID)
	{
		m_intSectionBSprTemperature = temperatureSpr;
		m_intSectionBTrayTemperature = temperatureTray;
	}
}

void GetVidasEpPayload::setInstrumentTemperature(uint16_t temperature)
{
	// temperature is converted from double to integer in 0.01 degrees
    m_intInstrumentTemperature = temperature;
}

void GetVidasEpPayload::setSectionDoor(uint8_t section, uint8_t status)
{
	if(section == SCT_A_ID)
	{
		m_intSectionADoor = status;
	}
	else if(section == SCT_B_ID)
	{
		m_intSectionBDoor = status;
	}
}

void GetVidasEpPayload::setSectionStartButton(uint8_t section, string strStatus)
{
	if(section == SCT_A_ID)
	{
		m_strStartButtonA.assign(strStatus);
	}
	else if(section == SCT_B_ID)
	{
		m_strStartButtonB.assign(strStatus);
    }
}

void GetVidasEpPayload::setOPTid(const string strID)
{
    m_strOPTid.assign(strID);
}

int GetVidasEpPayload::setErrorList(std::vector<string>* pVectorList, uint8_t section)
{
    // the error Context is the Section or the Slot if the error comes from Section
    // while for all others (NSH, CAMERA, INSTRUMENT) is Module
	for(uint8_t i = 0; i < pVectorList->size(); i++)
	{
		std::string strError;
		strError.assign(pVectorList->at(i));
		strError.append(HASHTAG_SEPARATOR);
		if(section == SCT_A_ID)
		{
			strError.append(XML_TAG_SECTIONA_NAME);
		}
		else if(section == SCT_B_ID)
		{
			strError.append(XML_TAG_SECTIONB_NAME);
		}
        else if(section == INSTRUMENT_ID)
        {
            strError.append(XML_TAG_INSTRUMENT_NAME);
        }
        else if(section == NSH_ID)
        {
            strError.append(XML_TAG_NSH_NAME);
        }
        else if(section == CAMERA_ID)
        {
            strError.append(XML_TAG_CAMERA_NAME);
        }

        m_vectorErrors.push_back(strError);
	}
	return m_vectorErrors.size();
}

bool GetVidasEpPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	if(pOutDoc == 0 || pRootElem == 0)
		return false;

	std::string tmpStr;
	DOMElement *pElemTmp, *pElemTmp2, *pElemTmp3, *pElemTmp4;

	pElemTmp = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_INSTRUMENT_NAME));
	pElemTmp->appendChild(pElemTmp2);

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_STATE_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	pElemTmp3->setTextContent(X(m_strInstrumentStatus.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMPERATURE_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	tmpStr = std::to_string(m_intInstrumentTemperature);
	pElemTmp3->setTextContent(X(tmpStr.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_NSH_NAME));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strNshStatus.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTIONA_NAME));
	pElemTmp->appendChild(pElemTmp2);

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_STATE_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	pElemTmp3->setTextContent(X(m_strSectionAStatus.c_str()));
/*
	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_START_BUTTON_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	pElemTmp3->setTextContent(X(m_strStartButtonA.c_str()));
*/
	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_DOOR_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	tmpStr = std::to_string(m_intSectionADoor);
	pElemTmp3->setTextContent(X(tmpStr.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMPERATURE_NAME));
	pElemTmp2->appendChild(pElemTmp3);

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_SPR_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	tmpStr = std::to_string(m_intSectionASprTemperature);
	pElemTmp4->setTextContent(X(tmpStr.c_str()));

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_TRAY_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	tmpStr = std::to_string(m_intSectionATrayTemperature);
	pElemTmp4->setTextContent(X(tmpStr.c_str()));

	if(m_strSectionAEndruntime.empty() == false)
	{
        pElemTmp3 = pOutDoc->createElement(X(XML_TAG_ENDRUNTIME_NAME));
        pElemTmp2->appendChild(pElemTmp3);
        pElemTmp3->setTextContent(X(m_strSectionAEndruntime.c_str()));
	}

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTIONB_NAME));
	pElemTmp->appendChild(pElemTmp2);

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_STATE_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	pElemTmp3->setTextContent(X(m_strSectionBStatus.c_str()));
/*
	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_START_BUTTON_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	pElemTmp3->setTextContent(X(m_strStartButtonB.c_str()));
*/
	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_DOOR_NAME));
	pElemTmp2->appendChild(pElemTmp3);
    tmpStr = std::to_string(m_intSectionBDoor);
	pElemTmp3->setTextContent(X(tmpStr.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMPERATURE_NAME));
	pElemTmp2->appendChild(pElemTmp3);

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_SPR_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	tmpStr = std::to_string(m_intSectionBSprTemperature);
	pElemTmp4->setTextContent(X(tmpStr.c_str()));

	pElemTmp4 = pOutDoc->createElement(X(XML_TAG_TRAY_NAME));
	pElemTmp3->appendChild(pElemTmp4);
	tmpStr = std::to_string(m_intSectionBTrayTemperature);
	pElemTmp4->setTextContent(X(tmpStr.c_str()));

	if(m_strSectionBEndruntime.empty() == false)
	{
        pElemTmp3 = pOutDoc->createElement(X(XML_TAG_ENDRUNTIME_NAME));
        pElemTmp2->appendChild(pElemTmp3);
        pElemTmp3->setTextContent(X(m_strSectionBEndruntime.c_str()));
	}

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_CAMERA_NAME));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strCameraStatus.c_str()));

	for(uint8_t i = 0; i < m_vectorErrors.size(); i++)
	{
		// the error string is formatted as:
		// timestamp(identifier)#code#category#description(#channel#well)#section
		std::vector<std::string> splitErrorString;
		splitErrorString = splitString(m_vectorErrors.at(i), HASHTAG_SEPARATOR);

		if(splitErrorString.size() >= 5)
		{
			std::string timestamp;
			// build timestamp as yyyy-mm-ddThh:mm:ss
			timestamp = formatTimestamp(splitErrorString.at(0));

			pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ERROR_NAME));
			pElemTmp->appendChild(pElemTmp2);

			XStr xstrId(XML_ATTR_ID_NAME);
			XStr xstrIdValue(splitErrorString.at(0).c_str());
			pElemTmp2->setAttribute(xstrId.unicodeForm(), xstrIdValue.unicodeForm());

			pElemTmp3 = pOutDoc->createElement(X(XML_ATTR_ERROR_CODE_NAME));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(splitErrorString.at(1).c_str()));

			pElemTmp3 = pOutDoc->createElement(X(XML_ATTR_ERROR_GROUP_NAME));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(splitErrorString.at(2).c_str()));

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_COMPONENT_NAME));
			pElemTmp2->appendChild(pElemTmp3);
			if(splitErrorString.size() == 5)
			{
				pElemTmp3->setTextContent(X(splitErrorString.at(4).c_str()));
			}
			else
			{
				// Pressure Error -> set Slot and Well
				pElemTmp3->setTextContent(X(splitErrorString.at(6).c_str()));

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_SLOT_NAME));
				pElemTmp2->appendChild(pElemTmp3);
				pElemTmp3->setTextContent(X(splitErrorString.at(4).c_str()));

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_WELL_NAME));
				pElemTmp2->appendChild(pElemTmp3);
				pElemTmp3->setTextContent(X(splitErrorString.at(5).c_str()));
			}

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TIMESTAMP_NAME));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(timestamp.c_str()));
		}
	}

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_TIMESTAMP_NAME));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strTimestamp.c_str()));

    if(m_strOPTid.empty() == false)
    {
        pElemTmp2 = pOutDoc->createElement(X(XML_TAG_OPT_ID));
        pElemTmp->appendChild(pElemTmp2);
        pElemTmp2->setTextContent(X(m_strOPTid.c_str()));
    }

    return true;
}

