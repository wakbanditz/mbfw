/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetCameraRoiPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetCameraRoiPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETCAMERAROIPAYLOAD_H
#define GETCAMERAROIPAYLOAD_H

#include "Payload.h"
#include "CameraConfiguration.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETCAMERAROI command
 * ********************************************************************************************************************
 */

class GetCameraRoiPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		GetCameraRoiPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~GetCameraRoiPayload();

		/*! ***********************************************************************************************************
		 * @brief setOutValues set all the data to be included in the payload
         * @param mConfig
		 * ***********************************************************************************************************
		 */
		void setAttributes(unordered_map<string, structRect>& mConfig);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        unordered_map<string, structRect> m_mapItems;

};

#endif // GETCAMERAROIPAYLOAD_H
