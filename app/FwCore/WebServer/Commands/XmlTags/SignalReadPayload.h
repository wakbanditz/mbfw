/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SignalReadPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SignalReadPayload class.
 @details

 ****************************************************************************
*/

#ifndef SIGNALREADPAYLOAD_H
#define SIGNALREADPAYLOAD_H

#include "Payload.h"
#include "MainExecutor.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the SIGNALREAD command
 * ********************************************************************************************************************
 */

class SignalReadPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief SignalReadPayload constructor
		 * ***********************************************************************************************************
		 */
		SignalReadPayload();

		/*! ***********************************************************************************************************
		 * @brief ~SignalReadPayload destructor
		 * ***********************************************************************************************************
		 */
		virtual ~SignalReadPayload();

		/*!
		 * @brief setOutParams
         * @param vFluoReadStruct
		 * @param vTimeStamps
		 * @param vFluoMv
		 * @param vRefMv
		 */
		void setOutParams(vector<string>& vFluoReadStruct, vector<string>& vTimeStamps, vector<string>& vFluoMv, vector<string>& vRefMv);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        vector<string>	m_vFluoRead;
        vector<string>	m_vTimeStamps;
        vector<string>	m_vFluoMv;
        vector<string>	m_vRefMv;

};

#endif // SIGNALREADPAYLOAD_H
