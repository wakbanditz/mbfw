/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetCalibrationPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetCalibrationPayload class.
 @details

 ****************************************************************************
*/

#include "GetCalibrationPayload.h"
#include "WebServerAndProtocolInclude.h"

GetCalibrationPayload::GetCalibrationPayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strLabel.clear();
	m_strPosition.clear();
}

GetCalibrationPayload::~GetCalibrationPayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strLabel.clear();
	m_strPosition.clear();
}

void GetCalibrationPayload::setOutValues(std::vector<std::string> strComponent,
										 std::vector<std::string> strMotor,
										 std::vector<std::string> strLabel,
										 std::vector<std::string> strCalibrations)
{
	m_strComponent = strComponent;
	m_strMotor = strMotor;
	m_strLabel = strLabel;
	m_strPosition = strCalibrations;
}


bool GetCalibrationPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	for(uint8_t i = 0; i < m_strComponent.size(); i++)
	{
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_POSITION_NAME));
		pElemTmp1->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(m_strPosition.at(i).c_str()));

		XStr xstrComponentName(XML_ATTR_COMPONENT_NAME);
		XStr xstrComponentValue(m_strComponent.at(i).c_str());
		pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

		XStr xstrMotorName(XML_ATTR_MOTOR_NAME);
		XStr xstrMotorValue(m_strMotor.at(i).c_str());
		pElemTmp2->setAttribute(xstrMotorName.unicodeForm(), xstrMotorValue.unicodeForm());

		XStr xstrLabelName(XML_ATTR_LABEL_NAME);
		XStr xstrLabelValue(m_strLabel.at(i).c_str());
		pElemTmp2->setAttribute(xstrLabelName.unicodeForm(), xstrLabelValue.unicodeForm());
	}
	return true;
}
