/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SHCalibratePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SHCalibratePayload class.
 @details

 ****************************************************************************
*/

#ifndef SHCALIBRATEPAYLOAD_H
#define SHCALIBRATEPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the SHCALIBRATE command
 * ********************************************************************************************************************
 */

class SHCalibratePayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief SHCalibratePayload constructor
		 * ***********************************************************************************************************
		 */
		SHCalibratePayload();

		/*! ***********************************************************************************************************
		 * @brief ~SHCalibratePayload destructor
		 * ***********************************************************************************************************
		 */
		virtual ~SHCalibratePayload();

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

		/*!
		 * @brief setOutParams set out the parameters
		 * @param strSection
		 * @param vTheoricItem
		 * @param vRealItem
		 */
		void setOutParams(string& strSection, const vector<pair<string, string> >& vTheoricItem, const vector<pair<string, string> >& vRealItem);

    private:

        string m_strSection;

        vector<pair<string, string>> m_vsTheoricItem;
        vector<pair<string, string>> m_vsRealItem;

};

#endif // SHCALIBRATEPAYLOAD_H
