/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetPressureOffsetsPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetPressureOffsetsPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETPRESSUREOFFSETSPAYLOAD_H
#define GETPRESSUREOFFSETSPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETPRESSUREOFFSETS command
 * ********************************************************************************************************************
 */

class GetPressureOffsetsPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		GetPressureOffsetsPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~GetPressureOffsetsPayload();

		/*! ***********************************************************************************************************
		 * @brief setOutValues to set in the payload object the vectors to be included in the message
		 * @param strComponent the Component vector
		 * @param strOffsets the Activation vector
		 * ***********************************************************************************************************
		 */
		void setOutValues(std::vector<std::string> strComponent,
						  std::vector<std::string> strOffsets);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:
        std::vector<std::string>	m_strComponent;
        std::vector<std::string>	m_strOffsets;

};


#endif // GETPRESSUREOFFSETSPAYLOAD_H
