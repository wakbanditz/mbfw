/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SaveCameraRoiPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SaveCameraRoiPayload class.
 @details

 ****************************************************************************
*/

#ifndef SAVECAMERAROIPAYLOAD_H
#define SAVECAMERAROIPAYLOAD_H

#include "Payload.h"
#include "CameraConfiguration.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the SAVECAMERAROI command
 * ********************************************************************************************************************
 */

class SaveCameraRoiPayload : public Payload
{

	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		SaveCameraRoiPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~SaveCameraRoiPayload();

		/*! ***********************************************************************************************************
		 * @brief setAttributes set command attributes
         * @param mConfig pointer to new config parts
		 * ***********************************************************************************************************
		 */
		void setAttributes(const unordered_map<string, structRect>& mConfig);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        unordered_map<string, structRect> m_mapSubSet;
};

#endif // SAVECAMERAROIPAYLOAD_H
