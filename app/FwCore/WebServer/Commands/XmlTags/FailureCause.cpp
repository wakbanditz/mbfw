/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FailureCause.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the FailureCausePayload class.
 @details

 ****************************************************************************
*/

#include "FailureCause.h"

#include "WebServerAndProtocolInclude.h"



FailureCause::FailureCause(int liCode) : XmlTag(XML_TAG_FAILURECAUSE_NAME)
{
	m_liCode = liCode;
	m_strCode.clear();
}

FailureCause::~FailureCause()
{

}

int FailureCause::getCodeInt() const
{
	return m_liCode;
}

std::__cxx11::string FailureCause::getCodeString()
{
	return m_strCode;
}

void FailureCause::setCodeInt(int liCode)
{
	m_liCode = liCode;
}

void FailureCause::setCodeString(std::string& strError)
{
	m_strCode.assign(strError);
}
