/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    TrayReadPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the TrayReadPayload class.
 @details

 ****************************************************************************
*/

#include "TrayReadPayload.h"
#include "CommonInclude.h"
#include "WebServerAndProtocolInclude.h"

TrayReadPayload::TrayReadPayload()
{
	m_vRFUValues.clear();
}

TrayReadPayload::~TrayReadPayload()
{

}

void TrayReadPayload::setTrayReadAttributes(vector<int> vRFUValues, int8_t strSecId)
{
	m_vRFUValues = vRFUValues;

	if ( strSecId == SCT_A_ID )
		m_strSecId = XML_VALUE_SECTION_A;
	else
		m_strSecId = XML_VALUE_SECTION_B;
}

bool TrayReadPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strSecId.c_str()));

	for ( int i = 0; i != SCT_NUM_TOT_SLOTS; ++i )
	{
		DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_PROTO_SLOT));
		pElemTmp1->appendChild(pElemTmp3);
		XStr xstrIdName(XML_ATTR_NUMBER_NAME);
		XStr xstrIdValue(to_string(i+1).c_str());
		pElemTmp3->setAttribute(xstrIdName.unicodeForm(), xstrIdValue.unicodeForm());

		DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_PROTO_RFU_NAME));
		pElemTmp3->appendChild(pElemTmp4);

		DOMElement* pElemTmp5 = pOutDoc->createElement(X(XML_TAG_LEFT_LOWCASE));
		pElemTmp4->appendChild(pElemTmp5);
		pElemTmp5->setTextContent(X(to_string(m_vRFUValues[i*3]).c_str()));

		DOMElement* pElemTmp6 = pOutDoc->createElement(X(XML_TAG_CENTER_LOWCASE));
		pElemTmp4->appendChild(pElemTmp6);
		pElemTmp6->setTextContent(X(to_string(m_vRFUValues[i*3+1]).c_str()));

		DOMElement* pElemTmp7 = pOutDoc->createElement(X(XML_TAG_RIGHT_LOWCASE));
		pElemTmp4->appendChild(pElemTmp7);
		pElemTmp7->setTextContent(X(to_string(m_vRFUValues[i*3+2]).c_str()));


		// Get the final RFU value
		int liMinValue = INT_MAX;
		float fTmpMeanValue = 0.0f;
		for ( int j = 0; j != 3; ++j )
		{
			if ( m_vRFUValues[i*3+j] <= liMinValue )	liMinValue = m_vRFUValues[i*3+j];
			fTmpMeanValue += m_vRFUValues[i*3+j];
		}

		fTmpMeanValue -= liMinValue;
		fTmpMeanValue /= 2;

		DOMElement* pElemTmp8 = pOutDoc->createElement(X(XML_TAG_MEAN_LOWCASE));
		pElemTmp4->appendChild(pElemTmp8);
        pElemTmp8->setTextContent(X(to_string((int)fTmpMeanValue).c_str()));
	}

	return true;
}
