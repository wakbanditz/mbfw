/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetPressureSettingsPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetPressureSettingsPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETPRESSURESETTINGSPAYLOAD_H
#define GETPRESSURESETTINGSPAYLOAD_H


#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETPRESSURESETTINGS command
 * ********************************************************************************************************************
 */

class GetPressureSettingsPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		GetPressureSettingsPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~GetPressureSettingsPayload();

		/*! ***********************************************************************************************************
		 * @brief setOutValues to set in the payload object the vectors to be included in the message
		 * @param strComponent the Component vector
         * @param strConversions the Conversions factor vector
         * @param strPressures the Pressures vector
         * @param strConversionFactor the conversion storage factor (100000)
         * @param strPressureFactor fake value to be coupled with the previous one
		 * ***********************************************************************************************************
		 */
		void setOutValues(std::vector<std::string> strComponent,
						  std::vector<std::string> strConversions,
						  std::vector<std::string> strPressures,
						  std::string strConversionFactor,
						  std::string strPressureFactor);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:
        std::vector<std::string> m_strComponent;
        std::vector<std::string> m_strConversions;
        std::vector<std::string> m_strPressures;
        std::string m_strConversionFactor;
        std::string m_strPressureFactor;

};


#endif // GETPRESSURESETTINGSPAYLOAD_H
