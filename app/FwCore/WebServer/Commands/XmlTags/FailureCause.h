/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FailureCause.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the FailureCause class.
 @details

 ****************************************************************************
*/
#ifndef FAILURECAUSE_H
#define FAILURECAUSE_H

#include "XmlTag.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag to contain the info for the Failure cause of any command
 * ********************************************************************************************************************
 */

class FailureCause : public XmlTag
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
         * @param liCode the error code identifier
		 * ***********************************************************************************************************
		 */
		FailureCause(int liCode);

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~FailureCause();

		/*! ***********************************************************************************************************
		 * @brief Data Setters
		 * ***********************************************************************************************************
		 */
		void setCodeInt(int liCode);

		void setCodeString(std::string& strError);

		/*! ***********************************************************************************************************
		 * @brief Data Getters
		 * ***********************************************************************************************************
		 */
		int getCodeInt() const;

		std::string getCodeString();

    private:

        int m_liCode;
        std::string m_strCode;

};

#endif // FAILURECAUSE_H
