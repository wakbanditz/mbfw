/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetVidasEpPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetVidasEpPayload class.
 @details

 ****************************************************************************
*/

#ifndef GETVIDASEPPAYLOAD_H
#define GETVIDASEPPAYLOAD_H

#include <string>

#include "StatusInclude.h"
#include "SectionInclude.h"
#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the VIDASEP command
 * ********************************************************************************************************************
 */

class GetVidasEpPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief GetVidasEpPayload default constructor
		 * **********************false*************************************************************************************
		 */
		GetVidasEpPayload();

		/*! ***********************************************************************************************************
		 * @brief setInstrumentStatus	set the Instrument status as defined in CommonInclude.h
         * @param strStatus			the status as string
		 * ***********************************************************************************************************
		 */
		void setInstrumentStatus(std::string strStatus);

		/*! ***********************************************************************************************************
		 * @brief setNshStatus	set the Nsh status as defined in CommonInclude.h
         * @param strStatus			the status as string
         * ***********************************************************************************************************
		 */
		void setNshStatus(std::string strStatus);

		/*! ***********************************************************************************************************
		 * @brief setCameraStatus	set the Camera status as defined in CommonInclude.h
         * @param strStatus			the status as string
         * ***********************************************************************************************************
		 */
		void setCameraStatus(std::string strStatus);

		/*! ***********************************************************************************************************
		 * @brief setSectionAStatus	set the SectionA status as defined in CommonInclude.h
         * @param strStatus			the status as string
         * ***********************************************************************************************************
		 */
		void setSectionAStatus(std::string strStatus);

		/*! ***********************************************************************************************************
		 * @brief setSectionBStatus	set the SectionA status as defined in CommonInclude.h
         * @param strStatus			the status as string
         * ***********************************************************************************************************
		 */
		void setSectionBStatus(std::string strStatus);

		/*! ***********************************************************************************************************
		 * @brief setSectionEndTime	set the endtime as 2019-02-26T11:01:04
		 * @param section		the section index
         * @param strTime	the endtime as string
		 * ***********************************************************************************************************
		 */
		void setSectionEndTime(uint8_t section, std::string strTime);

		/*! ***********************************************************************************************************
		 * @brief setTimestamp	set the timestamp as 2019-02-26T11:01:04
         * @param strTime			the time as string
		 * ***********************************************************************************************************
		 */
		void setTimestamp(std::string strTime);

		/*! ***********************************************************************************************************
		 * @brief setSectionTemperature	set the Section temperatures
		 * @param section		the section index
		 * @param temperatureTray the Tray temperature
		 * @param temperatureSpr the Spr temperature
		 * ***********************************************************************************************************
		 */
		void setSectionTemperature(uint8_t section, uint16_t temperatureTray, uint16_t temperatureSpr);

		/*! ***********************************************************************************************************
		 * @brief setInstrumentTemperature	set the Instrument temperature
		 * @param temperature the Instrument temperature
		 * ***********************************************************************************************************
		 */
        void setInstrumentTemperature(uint16_t temperature);

		/*! ***********************************************************************************************************
		 * @brief setSectionDoor	set the Section door Status
		 * @param section		the section index
		 * @param status the door status as integer
		 * ***********************************************************************************************************
		 */
		void setSectionDoor(uint8_t section, uint8_t status);

		/*! ***********************************************************************************************************
		 * @brief setSectionStartButton	set the Section door Status
		 * @param section		the section index
		 * @param strStatus the button status as string
		 * ***********************************************************************************************************
		 */
		void setSectionStartButton(uint8_t section, std::string strStatus);

        /*! ***********************************************************************************************************
         * @brief setOPTid	set the OPT identifier
         * @param strID the OPT string
         * ***********************************************************************************************************
         */
        void setOPTid(const std::string strID);


		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

		/*! ***********************************************************************************************************
		 * @brief setErrorList set the errors active on a device: each error corresponds to an element
		 *			in the vector: the string is formatted as timestamp#code#category#description
		 * @param pVectorList pointer to the list
		 * @param section the index of the section to build the "context" of error
		 * @return the number of elements in the vector
		 * ************************************************************************************************************
		 */
		int setErrorList(std::vector<std::string> * pVectorList, uint8_t section);

	private:

		std::string m_strNshStatus, m_strSectionAStatus, m_strSectionBStatus,
					m_strCameraStatus, m_strInstrumentStatus;
		uint8_t m_intSectionADoor, m_intSectionBDoor;

		uint16_t m_intSectionASprTemperature, m_intSectionBSprTemperature,
			m_intSectionATrayTemperature, m_intSectionBTrayTemperature,
			m_intInstrumentTemperature;

		std::string m_strSectionAEndruntime, m_strSectionBEndruntime;
		std::string m_strTimestamp;
		std::string m_strStartButtonA, m_strStartButtonB;

        std::string m_strOPTid;

		std::vector<std::string> m_vectorErrors;

};

#endif // GETVIDASEPPAYLOAD_H
