/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetGlobalSettingsPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetGlobalSettingsPayload class.
 @details

 ****************************************************************************
*/

#include "GetGlobalSettingsPayload.h"
#include "WebServerAndProtocolInclude.h"

GetGlobalSettingsPayload::GetGlobalSettingsPayload()
{
    m_bForce = false;
}


void GetGlobalSettingsPayload::setSprMinTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
        if(value < 0) value = 0;
		m_SprMin[section] = value;
	}
}

void GetGlobalSettingsPayload::setSprMaxTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
        if(value < 0) value = 0;
        m_SprMax[section] = value;
	}
}

void GetGlobalSettingsPayload::setSprTargetTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
        if(value < 0) value = 0;
        m_SprTarget[section] = value;
	}
}

void GetGlobalSettingsPayload::setSprToleranceTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
        if(value < 0) value = 0;
        m_SprTolerance[section] = value;
	}
}

void GetGlobalSettingsPayload::setTrayMinTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
        if(value < 0) value = 0;
        m_TrayMin[section] = value;
	}
}

void GetGlobalSettingsPayload::setTrayMaxTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
        if(value < 0) value = 0;
        m_TrayMax[section] = value;
	}
}

void GetGlobalSettingsPayload::setTrayTargetTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
        if(value < 0) value = 0;
        m_TrayTarget[section] = value;
	}
}

void GetGlobalSettingsPayload::setTrayToleranceTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
        if(value < 0) value = 0;
        m_TrayTolerance[section] = value;
	}
}

void GetGlobalSettingsPayload::setInternalMinTemperature(int value)
{
    if(value < 0) value = 0;
    m_InternalMin = value;
}

void GetGlobalSettingsPayload::setInternalMaxTemperature(int value)
{
    if(value < 0) value = 0;
    m_InternalMax = value;
}

void GetGlobalSettingsPayload::setForce(bool status)
{
    m_bForce = status;
}



bool GetGlobalSettingsPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	if(pOutDoc == 0 || pRootElem == 0)
		return false;

	std::string strTmp;

	DOMElement*  pElemTmp = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp);

    DOMElement*  pElemTmp2 = pOutDoc->createElement(X(XML_ATTR_FORCE_NAME));
    pElemTmp->appendChild(pElemTmp2);
    if(m_bForce == true)
        strTmp.assign(XML_VALUE_TRUE);
    else
        strTmp.assign(XML_VALUE_FALSE);
    pElemTmp2->setTextContent(X(strTmp.c_str()));

    pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SPRBLOCK_NAME));
	pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setAttribute(X(XML_TAG_SECTION_NAME), X(XML_VALUE_SECTION_A));

	DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_MIN_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_SprMin[SCT_A_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_MAX_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_SprMax[SCT_A_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_TARGET_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_SprTarget[SCT_A_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_TOLERANCE_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_SprTolerance[SCT_A_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));


	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SPRBLOCK_NAME));
	pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setAttribute(X(XML_TAG_SECTION_NAME), X(XML_VALUE_SECTION_B));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_MIN_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_SprMin[SCT_B_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_MAX_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_SprMax[SCT_B_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_TARGET_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_SprTarget[SCT_B_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_TOLERANCE_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_SprTolerance[SCT_B_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));


	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_TRAYBLOCK_NAME));
	pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setAttribute(X(XML_TAG_SECTION_NAME), X(XML_VALUE_SECTION_A));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_MIN_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_TrayMin[SCT_A_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_MAX_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_TrayMax[SCT_A_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_TARGET_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_TrayTarget[SCT_A_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_TOLERANCE_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_TrayTolerance[SCT_A_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));


	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_TRAYBLOCK_NAME));
	pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setAttribute(X(XML_TAG_SECTION_NAME), X(XML_VALUE_SECTION_B));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_MIN_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_TrayMin[SCT_B_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_MAX_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_TrayMax[SCT_B_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_TARGET_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_TrayTarget[SCT_B_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_TOLERANCE_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_TrayTolerance[SCT_B_ID]);
	pElemTmp3->setTextContent(X(strTmp.c_str()));


	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_INTERNAL_NAME));
	pElemTmp->appendChild(pElemTmp2);

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_MIN_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_InternalMin);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	pElemTmp3 = pOutDoc->createElement(X(XML_TAG_TEMP_MAX_NAME));
	pElemTmp2->appendChild(pElemTmp3);
	strTmp = std::to_string(m_InternalMax);
	pElemTmp3->setTextContent(X(strTmp.c_str()));

	return true;
}

