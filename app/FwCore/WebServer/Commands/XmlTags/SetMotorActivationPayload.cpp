/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SetMotorActivationPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SetMotorActivationPayload class.
 @details

 ****************************************************************************
*/

#include "SetMotorActivationPayload.h"
#include "WebServerAndProtocolInclude.h"

SetMotorActivationPayload::SetMotorActivationPayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strActivation.clear();
}

SetMotorActivationPayload::~SetMotorActivationPayload()
{
	/* Nothing to do yet */
}

void SetMotorActivationPayload::setOutParams(string &strComponent, string &strMotor, string &strActivation)
{
	m_strComponent.assign(strComponent);
	m_strMotor.assign(strMotor);
	m_strActivation.assign(strActivation);
}

bool SetMotorActivationPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

    DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ACTIVATION_NAME));
    pElemTmp1->appendChild(pElemTmp2);

    XStr xstrComponentName(XML_ATTR_COMPONENT_NAME);
    XStr xstrComponentValue(m_strComponent.c_str());
    pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

    XStr xstrMotorName(XML_ATTR_MOTOR_NAME);
    XStr xstrMotorValue(m_strMotor.c_str());
    pElemTmp2->setAttribute(xstrMotorName.unicodeForm(), xstrMotorValue.unicodeForm());

    pElemTmp2->setTextContent(X(m_strActivation.c_str()));

    return true;
}
