/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SetParameterPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SetPArameterPayload class.
 @details

 ****************************************************************************
*/
#ifndef SETPARAMETERPAYLOAD_H

#define SETPARAMETERPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the SETPARAMETER command
 * ********************************************************************************************************************
 */

class SetParameterPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		SetParameterPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~SetParameterPayload();

		/*! ***********************************************************************************************************
		 * @brief setOutValues to set in the payload object the vectors to be included in the message
		 * @param strComponent the Component vector
		 * @param strMotor the Motor vector
		 * ***********************************************************************************************************
		 */
		void setOutValues(std::string strComponent, std::string strMotor);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:
        std::string	m_strComponent;
        std::string	m_strMotor;

};

#endif // SETPARAMETERPAYLOAD_H
