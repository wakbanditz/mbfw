/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SaveCalibrationPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SaveCalibrationPayload class.
 @details

 ****************************************************************************
*/

#ifndef SAVECALIBRATIONPAYLOAD_H
#define SAVECALIBRATIONPAYLOAD_H

#include "Payload.h"
#include "InCmdSaveCalibration.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the SAVECALIBRATION command
 * ********************************************************************************************************************
 */

class SaveCalibrationPayload : public Payload
{
	public:
		SaveCalibrationPayload();
		virtual ~SaveCalibrationPayload();

		bool composePayload(DOMDocument *&pOutDoc, DOMElement *pRootElem);
		void setOutElement(structSaveCalibration &stCalib);

    private:
        vector<structSaveCalibration>	m_vecSaveCalibration;

};

#endif // SAVECALIBRATIONPAYLOAD_H
