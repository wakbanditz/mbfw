/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetFanSettingsPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetFanSettingsPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETFANSETTINGSPAYLOAD_H
#define GETFANSETTINGSPAYLOAD_H


#include "CommonInclude.h"
#include "ModuleInclude.h"

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GET_FAN_SETTINGS command
 * ********************************************************************************************************************
 */

class GetFanSettingsPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief GetFanSettingsPayload default constructor
		 * ***********************************************************************************************************
		 */
		GetFanSettingsPayload();

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

		/*! ***********************************************************************************************************
		 * @brief setFanPower function called to set the specific fan power
		 * @param index the fan index
		 * @param value the fan power
		 * ***********************************************************************************************************
		 */
		void setFanPower(eModuleFanId index, uint8_t value);

	private:

		uint8_t		m_fanPower[eFanNone];
};

#endif // GETFANSETTINGSPAYLOAD_H
