/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    DisablePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the DisablePayload class.
 @details

 ****************************************************************************
*/

#include "DisablePayload.h"
#include "WebServerAndProtocolInclude.h"

DisablePayload::DisablePayload()
{
	m_bInstrumentDisabled = false;
	m_bSectionADisabled = false;
	m_bSectionBDisabled = false;
	m_bCameraDisabled = false;
	m_bNshDisabled = false;
}

DisablePayload::~DisablePayload()
{
}

bool DisablePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	std::string strDisabled;

	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_INSTRUMENT_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	if(m_bInstrumentDisabled)
	{
		strDisabled.assign(XML_ATTR_STATUS_DISABLED);
	}
	else
	{
		strDisabled.assign(XML_ATTR_STATUS_ENABLED);
	}
	pElemTmp2->setTextContent(X(strDisabled.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_CAMERA_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	if(m_bCameraDisabled)
	{
		strDisabled.assign(XML_ATTR_STATUS_DISABLED);
	}
	else
	{
		strDisabled.assign(XML_ATTR_STATUS_ENABLED);
	}
	pElemTmp2->setTextContent(X(strDisabled.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_NSH_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	if(m_bNshDisabled)
	{
		strDisabled.assign(XML_ATTR_STATUS_DISABLED);
	}
	else
	{
		strDisabled.assign(XML_ATTR_STATUS_ENABLED);
	}
	pElemTmp2->setTextContent(X(strDisabled.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	XStr xstrLetter(XML_ATTR_LETTER_NAME);
	XStr xstrLetterValueA(XML_VALUE_SECTION_A);
	pElemTmp2->setAttribute(xstrLetter.unicodeForm(), xstrLetterValueA.unicodeForm());
	if(m_bSectionADisabled)
	{
		strDisabled.assign(XML_ATTR_STATUS_DISABLED);
	}
	else
	{
		strDisabled.assign(XML_ATTR_STATUS_ENABLED);
	}
	pElemTmp2->setTextContent(X(strDisabled.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	XStr xstrLetterValueB(XML_VALUE_SECTION_B);
	pElemTmp2->setAttribute(xstrLetter.unicodeForm(), xstrLetterValueB.unicodeForm());
	if(m_bSectionBDisabled)
	{
		strDisabled.assign(XML_ATTR_STATUS_DISABLED);
	}
	else
	{
		strDisabled.assign(XML_ATTR_STATUS_ENABLED);
	}
	pElemTmp2->setTextContent(X(strDisabled.c_str()));

	return true;
}

void DisablePayload::setInstrumentDisabled(bool status)
{
	m_bInstrumentDisabled = status;
}

void DisablePayload::setSectionADisabled(bool status)
{
	m_bSectionADisabled = status;
}

void DisablePayload::setSectionBDisabled(bool status)
{
	m_bSectionBDisabled = status;
}

void DisablePayload::setCameraDisabled(bool status)
{
	m_bCameraDisabled = status;
}

void DisablePayload::setNshDisabled(bool status)
{
	m_bNshDisabled = status;
}
