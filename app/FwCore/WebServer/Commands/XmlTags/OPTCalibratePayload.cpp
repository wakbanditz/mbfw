/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OPTCalibratePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OPTCalibratePayload class.
 @details

 ****************************************************************************
*/

#include "OPTCalibratePayload.h"
#include "WebServerAndProtocolInclude.h"

OPTCalibratePayload::OPTCalibratePayload()
{
	m_strSection.clear();
	m_strSlot.clear();
	m_strGain.clear();
	m_strSSVal.clear();
    m_strID.clear();
}

OPTCalibratePayload::~OPTCalibratePayload()
{

}

void OPTCalibratePayload::setAttributes(const string& strSection, const string& strSlot, const string& strGain,
                                        const string& strSSVal, const string& strID)
{
	m_strSection = strSection;
	m_strSlot = strSlot;
	m_strGain = strGain;
	m_strSSVal = strSSVal;
    m_strID = strID;
}

bool OPTCalibratePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strSection.c_str()));

	DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_SLOT_NAME));
	pElemTmp1->appendChild(pElemTmp3);
	pElemTmp3->setTextContent(X(m_strSlot.c_str()));

	DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_PROTO_GAIN_NAME));
	pElemTmp1->appendChild(pElemTmp4);

	XStr xstrIdNameReal(XML_ATTR_DECIMALS_NAME); // Or integer ??
	string strDecNum = "2"; // ask Fero for dec num
	XStr xstrIdValueReal(strDecNum.c_str());
	pElemTmp4->setAttribute(xstrIdNameReal.unicodeForm(), xstrIdValueReal.unicodeForm());
	pElemTmp4->setTextContent(X(m_strGain.c_str()));

	DOMElement* pElemTmp5 = pOutDoc->createElement(X(XML_TAG_PROTO_SSVALUE_NAME));
	pElemTmp1->appendChild(pElemTmp5);
	pElemTmp5->setTextContent(X(m_strSSVal.c_str()));

    pElemTmp5 = pOutDoc->createElement(X(XML_TAG_OPT_ID));
    pElemTmp1->appendChild(pElemTmp5);
    pElemTmp5->setTextContent(X(m_strID.c_str()));

    return true;
}
