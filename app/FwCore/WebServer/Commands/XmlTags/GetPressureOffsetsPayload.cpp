/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetPressureOffsetsPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetPressureOffsetsPayload class.
 @details

 ****************************************************************************
*/

#include "GetPressureOffsetsPayload.h"
#include "WebServerAndProtocolInclude.h"
#include "CommonInclude.h"

GetPressureOffsetsPayload::GetPressureOffsetsPayload()
{
	m_strComponent.clear();
	m_strOffsets.clear();
}

GetPressureOffsetsPayload::~GetPressureOffsetsPayload()
{
	m_strComponent.clear();
	m_strOffsets.clear();
}

void GetPressureOffsetsPayload::setOutValues(std::vector<std::string> strComponent,
											std::vector<std::string> strOffsets)
{
	m_strComponent = strComponent;
	m_strOffsets = strOffsets;
}

bool GetPressureOffsetsPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	uint8_t counter;

	for(counter = 0; counter < m_strComponent.size(); counter++)
	{
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
		pElemTmp1->appendChild(pElemTmp2);

		XStr xstrComponentName(XML_ATTR_LETTER_NAME);
		XStr xstrComponentValue(m_strComponent.at(counter).c_str());
		pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

		for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
		{
			DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_SLOT_NAME));
			pElemTmp2->appendChild(pElemTmp3);

			std::string strChannel = std::to_string(channel + 1);

			XStr xstrMotorName(XML_TAG_PROTO_NUMBER);
			XStr xstrMotorValue(strChannel.c_str());
			pElemTmp3->setAttribute(xstrMotorName.unicodeForm(), xstrMotorValue.unicodeForm());

			pElemTmp3->setTextContent(X(m_strOffsets.at(counter * SCT_NUM_TOT_SLOTS + channel).c_str()));
		}
	}

	return true;
}



