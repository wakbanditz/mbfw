/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    RunwlPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the RunwlPayload class.
 @details

 ****************************************************************************
*/

#include "RunwlPayload.h"
#include "RunwlInfo.h"
#include "MainExecutor.h"
#include "ErrorUtilities.h"

#include "WebServerAndProtocolInclude.h"

#include "SectionInclude.h"

// these data are requested for the Pressure data payload to be set apart
// (I know it's awful but as long I don't have a better idea ...)
static std::string m_strAlgorithm;
static int32_t m_intOffset;
static int32_t m_intFactor;
static int32_t m_intSendingRate;

// defines for the Temperature, Pressure data payload
#define XML_DATA_VERSION			"1"
#define XML_PRESSURE_TREATED_NAME           "TREATED"
#define XML_PRESSURE_ELABORATED_NAME		"ELABORATED"

#define XML_TAG_TEMPERATURE_SAMPLING_VALUE	"10000"
#define XML_TAG_PRESSURE_SAMPLING_VALUE		"4"

// ---------------------------------------------------------------------------

RunwlPayload::RunwlPayload() : Payload()
{
    m_section = SCT_NONE_ID;
    m_pRunwlInfo = 0;
    m_strProtocolStatus.clear();

    m_strTemperature.clear();
    m_strOptics.clear();

    m_strTreatedPressure.resize(SCT_NUM_TOT_SLOTS);
    m_strElaboratedPressure.resize(SCT_NUM_TOT_SLOTS);
    for(uint8_t strip = 0; strip > SCT_NUM_TOT_SLOTS; strip++)
    {
            m_strTreatedPressure[strip].clear();
            m_strElaboratedPressure[strip].clear();
    }
    m_intSendingRate = 0;
}

RunwlPayload::~RunwlPayload()
{

}

bool RunwlPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	if(pOutDoc == 0 || pRootElem == 0)
    {
        return false;
    }

	if(m_section == SCT_NONE_ID || m_pRunwlInfo == 0 || m_strProtocolStatus.empty() == true)
    {
        return false;
    }

	std::string tmpStr;
	DOMElement *pElemTmp, *pElemTmp2, *pElemTmp3, *pElemTmp4;

	// section record out of Payload block
	pElemTmp = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
	pRootElem->appendChild(pElemTmp);
	if(m_section == SCT_A_ID)
	{
        tmpStr.assign(XML_VALUE_SECTION_A);
	}
	else
	{
        tmpStr.assign(XML_VALUE_SECTION_B);
	}
	pElemTmp->setTextContent(X(tmpStr.c_str()));


	// if the protocol has been stopped the payload is "limited"
	if(m_strProtocolStatus.compare(PROTOCOL_STATUS_COMPLETED) != 0)
	{
		pElemTmp = pOutDoc->createElement(X(XML_TAG_PROTOCOL_STATUS));
		pRootElem->appendChild(pElemTmp);
		pElemTmp->setTextContent(X(m_strProtocolStatus.c_str()));

		// timestamp record out of Payload block
		pElemTmp = pOutDoc->createElement(X(XML_TAG_TIMESTAMP_NAME));
		pRootElem->appendChild(pElemTmp);
		tmpStr = getFormattedTime(true);
		pElemTmp->setTextContent(X(tmpStr.c_str()));

		return true;
	}


	pElemTmp = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PROTO_FW_VERSION));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(infoSingleton()->getMasterApplicationRelease().c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PROTO_START_TIME));
	pElemTmp->appendChild(pElemTmp2);
	uint64_t startTime = m_pRunwlInfo->getStartTime();
	tmpStr.assign(getFormattedTimeFromSeconds(startTime, true));
	pElemTmp2->setTextContent(X(tmpStr.c_str()));


	for(uint8_t index = 0; index < J_MAXMIN_VECTOR_SIZE; index++)
	{
		if(m_pRunwlInfo->getReadingValueJ(index) > 0)
		{
			// extract the reading time value from a valid strip
			for(uint8_t strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
			{
				// filter the strips checking the ID
				if(m_pRunwlInfo->getStripId(strip).empty() == false)
				{
					uint64_t value = m_pRunwlInfo->getStripReadingIndexTime(strip, index);
					if(value > 0)
					{
						pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PROTO_READING_TIME));
						pElemTmp->appendChild(pElemTmp2);

						tmpStr.assign(std::to_string(index));
						XStr xstrNumber(tmpStr.c_str());
						pElemTmp2->setAttribute(X(XML_TAG_PROTO_INDEX), xstrNumber.unicodeForm());

						tmpStr.assign(std::to_string(value + 1));
						pElemTmp2->setTextContent(X(tmpStr.c_str()));
					}
					break;
				}
			}
		}
	}

    for(uint8_t strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
    {
        // if at least one slot requires the Air reading report the corresponding time
        if(m_pRunwlInfo->getProtoMeasureAir(strip) == 1)
        {
            pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PROTO_AIR_READING_TIME));
            pElemTmp->appendChild(pElemTmp2);

            // the final Air read time
            tmpStr = std::to_string(m_pRunwlInfo->getStripReadingAirTime(strip));
            pElemTmp2->setTextContent(X(tmpStr.c_str()));
            break;
        }
    }

	for(uint8_t strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
	{
		// filter the strips checking the ID
		std::string strID;

		strID.assign(m_pRunwlInfo->getStripId(strip));

		if(strID.empty() == false)
		{
			// processed strip -> add data
			pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SLOT_NAME));
			pElemTmp->appendChild(pElemTmp2);

			tmpStr.assign(std::to_string(strip + 1));
			XStr xstrNumber(tmpStr.c_str());
			pElemTmp2->setAttribute(X(XML_ATTR_NUMBER_NAME), xstrNumber.unicodeForm());

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_ID_NAME));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(strID.c_str()));

			for(uint8_t index = 0; index < J_MAXMIN_VECTOR_SIZE; index++)
			{
				if(m_pRunwlInfo->getReadingValueJ(index) > 0)
				{
					if(m_pRunwlInfo->getStripReadingIndexTime(strip, index) > 0)
					{
						pElemTmp3 = pOutDoc->createElement(X(XML_TAG_PROTO_READING));
						pElemTmp2->appendChild(pElemTmp3);

						tmpStr.assign(std::to_string(index));
						XStr xstrIndex(tmpStr.c_str());
						pElemTmp3->setAttribute(X(XML_TAG_PROTO_INDEX), xstrIndex.unicodeForm());

						// extract the reading rfu value (only the mean one)
						int32_t value = 0;
						m_pRunwlInfo->getStripReadingIndexValues(strip, index, 0, 0, &value);
						tmpStr.assign(std::to_string(value + 1));
						pElemTmp3->setTextContent(X(tmpStr.c_str()));
					}
				}
			}

			if(m_pRunwlInfo->getProtoMeasureAir(strip) == 1)
			{
				// the final Air read value
				tmpStr = std::to_string(m_pRunwlInfo->getStripReadingAir(strip));

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_PROTO_AIR_VALUE));
				pElemTmp2->appendChild(pElemTmp3);
				pElemTmp3->setTextContent(X(tmpStr.c_str()));
			}

            if(m_pRunwlInfo->getProtoTreatedPressure(strip) > 0 && m_strTreatedPressure[strip].empty() == false)
			{
				// add in payload the treated pressure (formatted base64)
				encodeDataBase64((const unsigned char*)m_strTreatedPressure[strip].c_str(),
								 m_strTreatedPressure[strip].size(), tmpStr);

				if(tmpStr.empty() == false)
				{
					pElemTmp3 = pOutDoc->createElement(X(XML_TAG_PROTO_TREATED));
					pElemTmp2->appendChild(pElemTmp3);
					pElemTmp3->setTextContent(X(tmpStr.c_str()));
				}
			}

			if(m_pRunwlInfo->getProtoElaboratedPressure(strip) == 1 && m_strElaboratedPressure[strip].empty() == false)
			{
                // add in payload the treated pressure (formatted base64)
				encodeDataBase64((const unsigned char*)m_strElaboratedPressure[strip].c_str(),
								 m_strElaboratedPressure[strip].size(), tmpStr);

                char buffer[64];
                printf(buffer, "RunPayload: strip %d Elab %d -> %d", strip, m_strElaboratedPressure[strip].size(), tmpStr.size());

                if(tmpStr.empty() == false)
				{
					pElemTmp3 = pOutDoc->createElement(X(XML_TAG_PROTO_ELABORATED));
					pElemTmp2->appendChild(pElemTmp3);
					pElemTmp3->setTextContent(X(tmpStr.c_str()));
				}
			}

			// set strip specific errors (Pressures)
			std::vector<std::string> vectorErrors;
			vectorErrors.clear();

			sectionBoardManagerSingleton(m_section)->getPressureErrorListPerStripWell(&vectorErrors, strip + ZERO_CHAR);

			for(uint8_t i = 0; i < vectorErrors.size(); i++)
			{
				// the error string is formatted as:
				// timestamp(identifier)#code#category#description#channel#well#section
				std::vector<std::string> splitErrorString;
				splitErrorString = splitString(vectorErrors.at(i), HASHTAG_SEPARATOR);

                if(splitErrorString.size() >= 6)	// number of elements in the pressure error
				{
					std::string timestamp;
					// build timestamp as yyyy-mm-ddThh:mm:ss
					timestamp = formatTimestamp(splitErrorString.at(0));

					pElemTmp3 = pOutDoc->createElement(X(XML_TAG_ERROR_NAME));
					pElemTmp2->appendChild(pElemTmp3);

					XStr xstrId(XML_ATTR_ID_NAME);
					XStr xstrIdValue(splitErrorString.at(0).c_str());
					pElemTmp3->setAttribute(xstrId.unicodeForm(), xstrIdValue.unicodeForm());

					pElemTmp4 = pOutDoc->createElement(X(XML_ATTR_ERROR_CODE_NAME));
					pElemTmp3->appendChild(pElemTmp4);
					pElemTmp4->setTextContent(X(splitErrorString.at(1).c_str()));

					pElemTmp4 = pOutDoc->createElement(X(XML_ATTR_ERROR_GROUP_NAME));
					pElemTmp3->appendChild(pElemTmp4);
					pElemTmp4->setTextContent(X(splitErrorString.at(2).c_str()));

					pElemTmp4 = pOutDoc->createElement(X(XML_TAG_SLOT_NAME));
					pElemTmp3->appendChild(pElemTmp4);
					pElemTmp4->setTextContent(X(splitErrorString.at(4).c_str()));

					pElemTmp4 = pOutDoc->createElement(X(XML_TAG_WELL_NAME));
					pElemTmp3->appendChild(pElemTmp4);
					pElemTmp4->setTextContent(X(splitErrorString.at(5).c_str()));

					pElemTmp4 = pOutDoc->createElement(X(XML_TAG_TIMESTAMP_NAME));
					pElemTmp3->appendChild(pElemTmp4);
					pElemTmp4->setTextContent(X(timestamp.c_str()));
				}
			}

		}
	}

	if(m_pRunwlInfo->getMonitoringTemperature() == true && m_strTemperature.empty() == false)
	{
		// add in payload the temperature (formatted base64)
		encodeDataBase64((const unsigned char*)m_strTemperature.c_str(), m_strTemperature.size(), tmpStr);

		if(tmpStr.empty() == false)
		{
			pElemTmp2 = pOutDoc->createElement(X(XML_TAG_TEMPERATURE_NAME));
			pElemTmp->appendChild(pElemTmp2);
			pElemTmp2->setTextContent(X(tmpStr.c_str()));
		}
	}

	if(m_pRunwlInfo->getMonitoringAutocheck() == true)
	{
		// add in payload the optical info (formatted base64)
		encodeDataBase64((const unsigned char*)m_strOptics.c_str(), m_strOptics.size(), tmpStr);

		if(tmpStr.empty() == false)
		{
			pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PROTO_AUTOCHECK));
			pElemTmp->appendChild(pElemTmp2);
			pElemTmp2->setTextContent(X(tmpStr.c_str()));
		}
	}

    // OPT identifier
    std::string strOPT;
    strOPT.assign(infoSingleton()->getOPTid());
    pElemTmp2 = pOutDoc->createElement(X(XML_TAG_OPT_ID));
    pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setTextContent(X(strOPT.c_str()));

	// timestamp record out of Payload block
	pElemTmp = pOutDoc->createElement(X(XML_TAG_TIMESTAMP_NAME));
	pRootElem->appendChild(pElemTmp);
	tmpStr = getFormattedTime(true);
	pElemTmp->setTextContent(X(tmpStr.c_str()));

	return true;
}

bool RunwlPayload::setSectionRunwl(uint8_t section, RunwlInfo* pRunwlInfo)
{
	if(section >= SCT_NUM_TOT_SECTIONS || pRunwlInfo == 0) return false;

	m_section = section;
	m_pRunwlInfo = pRunwlInfo;
	return true;
}

bool RunwlPayload::setProtocolStatus(string& strStatus)
{
	m_strProtocolStatus.assign(strStatus);
	return true;
}

bool RunwlPayload::setTemperature(std::string& strTemperatureEncoded)
{
	m_strTemperature.assign(strTemperatureEncoded);
	return true;
}

bool RunwlPayload::setOptics(string& strOptics)
{
	m_strOptics.assign(strOptics);
	return true;
}

bool RunwlPayload::setTreatedPressure(uint8_t strip, string& strTreatedPressure)
{
	if(strip >= SCT_NUM_TOT_SLOTS) return false;

	m_strTreatedPressure[strip].assign(strTreatedPressure);

	return true;
}

bool RunwlPayload::setElaboratedPressure(uint8_t strip, string& strElaboratedPressure)
{
	if(strip >= SCT_NUM_TOT_SLOTS) return false;

	m_strElaboratedPressure[strip].assign(strElaboratedPressure);

	return true;
}

void RunwlPayload::setPressureAlgorithm(string& strAlgorithm)
{
	m_strAlgorithm.assign(strAlgorithm);
}

void RunwlPayload::setPressureOffset(int32_t offset)
{
	m_intOffset = offset;
}

void RunwlPayload::setPressureFactor(int32_t factor)
{
    m_intFactor = factor;
}

void RunwlPayload::setPressureSendingRate(int32_t factor)
{
    m_intSendingRate = factor;
}

// functions to build the embedded data payload

bool buildTemperaturePayload(uint8_t section, DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	if(pOutDoc == 0 || pRootElem == 0)
		return false;

	// add in payload the temperature (formatted base64)
	char nameFile[64];
	sprintf(nameFile, "%s%s%c%s", RUN_FOLDER, PROTO_TEMPERATURE_PREFIX, SECTION_CHAR_A + section, PROTO_TEMPERATURE_SUFFIX);

	// read the temperature values from file and split them: room - spr - tray

	ifstream inputFile;
	// open input file, calculate lenght and read it
	inputFile.open(nameFile, ios_base::binary);
	if(inputFile.fail())
	{
		return false;
	}
	inputFile.seekg(0, ios_base::end);
	int lenght = inputFile.tellg();
	inputFile.seekg(0, ios_base::beg);
	if(lenght == 0)
	{
		inputFile.close();
		return false;
	}

	std::vector<unsigned char> tmpBuffer(lenght);
	inputFile.read((char*) &tmpBuffer[0], lenght);
	inputFile.close();

    // the array is composed of uint16 in the sequence room-spr-tray
	// set lenght so that all have the same number of samples
    uint8_t lenTemperature = sizeof(uint16_t);

    while(((lenght / lenTemperature) % 3) != 0)
	{
        if(lenght > lenTemperature)	lenght -= lenTemperature;
		else	break;
	}
	if(lenght == 0)
	{
		return false;
	}

	std::vector<unsigned char> tmpRoom;
	std::vector<unsigned char> tmpSpr;
	std::vector<unsigned char> tmpTray;

    for(int32_t i = 0; i < lenght; )
	{
        int8_t j;

        // big endian coding (MSB first)
        // for(j = 0; j < lenTemperature; j++, i++)
        for(j = lenTemperature - 1; j >= 0; j--)
        {
            tmpRoom.push_back(tmpBuffer.at(i + j));
		}
        i += lenTemperature;
		if(i >= lenght) break;

        for(j = lenTemperature - 1; j >= 0; j--)
        {
            tmpSpr.push_back(tmpBuffer.at(i + j));
        }
        i += lenTemperature;
        if(i >= lenght) break;

        for(j = lenTemperature - 1; j >= 0; j--)
        {
            tmpTray.push_back(tmpBuffer.at(i + j));
        }
        i += lenTemperature;
	}

	// now encode rbase64 the string
	std::string strRoom, strSpr, strTray;

	encodeDataBase64(tmpRoom.data(), tmpRoom.size(), strRoom);
	encodeDataBase64(tmpSpr.data(), tmpSpr.size(), strSpr);
	encodeDataBase64(tmpTray.data(), tmpTray.size(), strTray);

	tmpBuffer.clear();
	tmpRoom.clear();
	tmpSpr.clear();
	tmpTray.clear();

	// build the XML payload
	DOMElement *pElemTmp, *pElemTmp2;

    XStr xstrStartIndex("0");

	// section record out of Payload block
	pElemTmp = pOutDoc->createElement(X(XML_TAG_PRESSURE_VERSION));
	pRootElem->appendChild(pElemTmp);
	pElemTmp->setTextContent(X(XML_DATA_VERSION));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_SAMPLING_PERIOD));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_TEMPERATURE_SAMPLING_VALUE));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_TEMPERATURE_SENSOR_DATA));
	pRootElem->appendChild(pElemTmp);
	XStr xstrStrip(XML_TAG_TEMPERATURE_SENSOR_STRIP);
	pElemTmp->setAttribute(X(XML_TAG_NAME_NAME), xstrStrip.unicodeForm());

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUES_NAME));
	pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setAttribute(X(XML_TAG_START_TIME_NAME), xstrStartIndex.unicodeForm());
	pElemTmp2->setTextContent(X(strTray.c_str()));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_TEMPERATURE_SENSOR_DATA));
	pRootElem->appendChild(pElemTmp);
	XStr xstrSpr(XML_TAG_TEMPERATURE_SENSOR_SPR);
	pElemTmp->setAttribute(X(XML_TAG_NAME_NAME), xstrSpr.unicodeForm());

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUES_NAME));
	pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setAttribute(X(XML_TAG_START_TIME_NAME), xstrStartIndex.unicodeForm());
	pElemTmp2->setTextContent(X(strSpr.c_str()));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_TEMPERATURE_SENSOR_DATA));
	pRootElem->appendChild(pElemTmp);
	XStr xstrRoom(XML_TAG_TEMPERATURE_SENSOR_INTERNAL);
	pElemTmp->setAttribute(X(XML_TAG_NAME_NAME), xstrRoom.unicodeForm());

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUES_NAME));
	pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setAttribute(X(XML_TAG_START_TIME_NAME), xstrStartIndex.unicodeForm());
	pElemTmp2->setTextContent(X(strRoom.c_str()));

	return true;
}

bool buildTreatedPressurePayload(uint8_t section, uint8_t strip, DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	if(pOutDoc == 0 || pRootElem == 0)
		return false;

	char nameFile[64];
	sprintf(nameFile, "%s%s%c%c%s", RUN_FOLDER, PROTO_TREATED_PREFIX, SECTION_CHAR_A + section, ZERO_CHAR + strip +1, PROTO_TREATED_SUFFIX);

	// read all the pressure values from file and split them in phases

	ifstream inputFile;
	// open input file, calculate lenght and read it
	inputFile.open(nameFile, ios_base::binary);
	if(inputFile.fail())
	{
		return false;
	}
	inputFile.seekg(0, ios_base::end);
	int lenght = inputFile.tellg();
	inputFile.seekg(0, ios_base::beg);
	if(lenght == 0)
	{
		inputFile.close();
		return false;
	}

	std::vector<unsigned char> tmpBuffer(lenght);
	inputFile.read((char*) &tmpBuffer[0], lenght);
	inputFile.close();


	std::vector<uint32_t> tmpData;

	for(uint32_t k = 0; k < (uint32_t)lenght; k += 4)
	{
		uint32_t value = (uint32_t)(tmpBuffer.at(k + 3) << 24) + (uint32_t)(tmpBuffer.at(k + 2) << 16) +
					   (uint32_t)(tmpBuffer.at(k + 1) << 8) + (uint32_t)(tmpBuffer.at(k));
		tmpData.push_back(value);
	}

	tmpBuffer.clear();

	// build the XML payload

	// header
	std::string tmpStr;
	DOMElement *pElemTmp, *pElemTmp2;

	// section record out of Payload block
	pElemTmp = pOutDoc->createElement(X(XML_TAG_PRESSURE_VERSION));
	pRootElem->appendChild(pElemTmp);
	pElemTmp->setTextContent(X(XML_DATA_VERSION));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_TYPE_NAME));
	pRootElem->appendChild(pElemTmp);
	pElemTmp->setTextContent(X(XML_PRESSURE_TREATED_NAME));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_SAMPLING_PERIOD));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_SAMPLING_VALUE));

    pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
    pRootElem->appendChild(pElemTmp);

    pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
    pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setTextContent(X(XML_TAG_SENDING_RATE));

    pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
    pElemTmp->appendChild(pElemTmp2);
    tmpStr = std::to_string(m_intSendingRate);
    pElemTmp2->setTextContent(X(tmpStr.c_str()));

    pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_ALGORITHM_NAME));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strAlgorithm.c_str()));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_CONVERSION_NAME));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
	pElemTmp->appendChild(pElemTmp2);
	tmpStr = std::to_string(m_intFactor);
	pElemTmp2->setTextContent(X(tmpStr.c_str()));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_OFFSET_NAME));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
	pElemTmp->appendChild(pElemTmp2);
	tmpStr = std::to_string(m_intOffset);
	pElemTmp2->setTextContent(X(tmpStr.c_str()));

	// end of header -> start parsing the data

	uint32_t counter = 0;
    uint16_t phaseCounter = 1;
    char bufferStartTime[12];

	DOMElement *pElemTmp3;
	std::vector<unsigned char> tmpValues;

	while(true)
	{
		// identify the beginning of a sequence
		if(tmpData.at(counter) >= RAW_DATA_HEADER_ADC_VALUE)
		{
			uint32_t dato;
			char buffer[12], buffer1[12], buffer2[12], buffer3[12], buffer4[12], buffer5[12], buffer6[12];

			dato = (tmpData.at(counter) - RAW_DATA_HEADER_ADC_VALUE);
			sprintf(buffer6, "%c", (char)dato);	// command code

			counter++;

			// next value includes many info:
			dato = tmpData.at(counter);
			dato &= 0xFF000000;
			dato >>= 24;
            sprintf(buffer1, "%d", (dato + 1));	// current cycle

			dato = tmpData.at(counter);
			dato &= 0x00FF0000;
			dato >>= 16;
            // well 9 is missing in the strip
            if(dato < 10)
                sprintf(buffer2, "%d", (dato + 1));	// well index
            else
                sprintf(buffer2, "%d", dato);	// well index

			dato = tmpData.at(counter);
			dato &= 0x0000FF00;
			dato >>= 8;
			sprintf(buffer3, "%c", dato);	// volume char

			dato = tmpData.at(counter);
			dato &= 0x000000FF;
			dato >>= 0;
			sprintf(buffer4, "%c", dato);	// speed char

			counter++;

			// next value is the start time
			dato = tmpData.at(counter);
			sprintf(buffer5, "%d", dato);	// start time
            strcpy(bufferStartTime, buffer5);

			pElemTmp = pOutDoc->createElement(X(XML_TAG_PRESSURE_ASPIRATION_DATA));
			pRootElem->appendChild(pElemTmp);

			sprintf(buffer, "%d", phaseCounter);
			XStr xstrPhase(buffer);
			pElemTmp->setAttribute(X(XML_TAG_PRESSURE_PHASE), xstrPhase.unicodeForm());

            XStr xstrStart(bufferStartTime);
            pElemTmp->setAttribute(X(XML_TAG_START_TIME_NAME), xstrStart.unicodeForm());

			pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
			pElemTmp->appendChild(pElemTmp2);

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(XML_TAG_WELL_NAME));

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(buffer2));

			pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
			pElemTmp->appendChild(pElemTmp2);

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(XML_TAG_VOLUME_NAME));

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(buffer3));

			pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
			pElemTmp->appendChild(pElemTmp2);

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(XML_TAG_SPEED_NAME));

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(buffer4));

			pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
			pElemTmp->appendChild(pElemTmp2);

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(XML_TAG_COMMAND_NAME));

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(buffer6));

			pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
			pElemTmp->appendChild(pElemTmp2);

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(XML_TAG_REPETITION_NAME));

			pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
			pElemTmp2->appendChild(pElemTmp3);
			pElemTmp3->setTextContent(X(buffer1));

			tmpValues.clear();

		}
		else if(tmpData.at(counter) == 0)
		{
			// end of the sequence -> encode list of data and add it
			std::string strValues;
			encodeDataBase64(tmpValues.data(), tmpValues.size(), strValues);

			pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUES_NAME));
			pElemTmp->appendChild(pElemTmp2);

            XStr xstrStart(bufferStartTime);
            pElemTmp2->setAttribute(X(XML_TAG_START_TIME_NAME), xstrStart.unicodeForm());

            pElemTmp2->setTextContent(X(strValues.c_str()));

			phaseCounter++;
		}
		else
		{
            // value to be added to sequence : split the uint32 in chars from high to low (Big Endian)
			uint32_t dato;

			dato = tmpData.at(counter);
            dato &= 0xFF000000;
            dato >>= 24;
            tmpValues.push_back((char)dato);

            dato = tmpData.at(counter);
            dato &= 0x00FF0000;
            dato >>= 16;
            tmpValues.push_back((char)dato);

            dato = tmpData.at(counter);
            dato &= 0x0000FF00;
            dato >>= 8;
            tmpValues.push_back((char)dato);

            dato = tmpData.at(counter);
            dato &= 0x000000FF;
            dato >>= 0;
            tmpValues.push_back((char)dato);
		}


		if(++counter >= tmpData.size()) break;
	}


	tmpData.clear();
	return true;
}

bool buildElaboratedPressurePayload(uint8_t section, uint8_t strip, DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	if(pOutDoc == 0 || pRootElem == 0)
		return false;

	// local structures replicating the Section ones
	well_data_struct well_data;
	well_sampling_struct well_sampling;

	char nameFile[64];
	sprintf(nameFile, "%s%s%c%c%s", RUN_FOLDER, PROTO_ELABORATED_PREFIX, SECTION_CHAR_A + section,
			ZERO_CHAR + strip + 1, PROTO_ELABORATED_SUFFIX);

	// read all the pressure values from file and split them in phases

	ifstream inputFile;
	// open input file, calculate lenght and read it
	inputFile.open(nameFile, ios_base::binary);
	if(inputFile.fail())
	{
		return false;
	}
	inputFile.seekg(0, ios_base::end);
	int lenght = inputFile.tellg();
	inputFile.seekg(0, ios_base::beg);
	if(lenght == 0)
	{
		inputFile.close();
		return false;
	}

	// build the XML payload

	// header
	std::string tmpStr;
	DOMElement *pElemTmp, *pElemTmp2, *pElemTmp3;

	// section record out of Payload block
	pElemTmp = pOutDoc->createElement(X(XML_TAG_PRESSURE_VERSION));
	pRootElem->appendChild(pElemTmp);
	pElemTmp->setTextContent(X(XML_DATA_VERSION));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_TYPE_NAME));
	pRootElem->appendChild(pElemTmp);
	pElemTmp->setTextContent(X(XML_PRESSURE_ELABORATED_NAME));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_SAMPLING_PERIOD));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_SAMPLING_VALUE));

    pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
    pRootElem->appendChild(pElemTmp);

    pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
    pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setTextContent(X(XML_TAG_SENDING_RATE));

    pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
    pElemTmp->appendChild(pElemTmp2);
    tmpStr = std::to_string(m_intSendingRate);
    pElemTmp2->setTextContent(X(tmpStr.c_str()));

    pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_ALGORITHM_NAME));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strAlgorithm.c_str()));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_OFFSET_NAME));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
	pElemTmp->appendChild(pElemTmp2);
	tmpStr = std::to_string(m_intOffset);
	pElemTmp2->setTextContent(X(tmpStr.c_str()));

	pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
	pRootElem->appendChild(pElemTmp);

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
	pElemTmp->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_CONVERSION_NAME));

	pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
	pElemTmp->appendChild(pElemTmp2);
	tmpStr = std::to_string(m_intFactor);
	pElemTmp2->setTextContent(X(tmpStr.c_str()));

	// include the pressure settings parameters as set by RUN command

	PressureSettings * pSettings;
	pSettings = pressureSettingsPointer(section, strip);

	if(pSettings)
	{
		pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
		pRootElem->appendChild(pElemTmp);

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
		pElemTmp->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_AREARATIO));

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
		pElemTmp->appendChild(pElemTmp2);
		tmpStr = std::to_string(pSettings->getAreaRatio());
		pElemTmp2->setTextContent(X(tmpStr.c_str()));

		pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
		pRootElem->appendChild(pElemTmp);

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
		pElemTmp->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_MINPOS));

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
		pElemTmp->appendChild(pElemTmp2);
		tmpStr = std::to_string(pSettings->getMinimumPositionPercentage());
		pElemTmp2->setTextContent(X(tmpStr.c_str()));

		pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
		pRootElem->appendChild(pElemTmp);

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
		pElemTmp->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_SLOPEPERC));

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
		pElemTmp->appendChild(pElemTmp2);
		tmpStr = std::to_string(pSettings->getFinalSlopePercentage());
		pElemTmp2->setTextContent(X(tmpStr.c_str()));

		pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
		pRootElem->appendChild(pElemTmp);

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
		pElemTmp->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_SLOPETH));

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
		pElemTmp->appendChild(pElemTmp2);
		tmpStr = std::to_string(pSettings->getFinalSlopeThreshold());
		pElemTmp2->setTextContent(X(tmpStr.c_str()));
	}

	// end of common header if the setting is Industry add the others parameters

	if(m_strAlgorithm.compare(INDUSTRY_ALGO_STRING) == 0)
	{
		pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
		pRootElem->appendChild(pElemTmp);

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
		pElemTmp->appendChild(pElemTmp2);
        pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_TH_IND_LOW));

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
		pElemTmp->appendChild(pElemTmp2);
		tmpStr = std::to_string(pSettings->getIndustryThresholdLow());
		pElemTmp2->setTextContent(X(tmpStr.c_str()));

		pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
		pRootElem->appendChild(pElemTmp);

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
		pElemTmp->appendChild(pElemTmp2);
        pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_TH_IND_HIGH));

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
		pElemTmp->appendChild(pElemTmp2);
		tmpStr = std::to_string(pSettings->getIndustryThresholdHigh());
		pElemTmp2->setTextContent(X(tmpStr.c_str()));

		pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
		pRootElem->appendChild(pElemTmp);

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
		pElemTmp->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_CYCLESPERC));

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
		pElemTmp->appendChild(pElemTmp2);
		tmpStr = std::to_string(pSettings->getIndustryCyclesPercentage());
		pElemTmp2->setTextContent(X(tmpStr.c_str()));

		pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
		pRootElem->appendChild(pElemTmp);

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
		pElemTmp->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_CYCLESMIN));

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
		pElemTmp->appendChild(pElemTmp2);
		tmpStr = std::to_string(pSettings->getIndustryCyclesMinimum());
		pElemTmp2->setTextContent(X(tmpStr.c_str()));

		pElemTmp = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
		pRootElem->appendChild(pElemTmp);

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
		pElemTmp->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(XML_TAG_PRESSURE_CYCLESTH));

		pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
		pElemTmp->appendChild(pElemTmp2);
		tmpStr = std::to_string(pSettings->getIndustryCyclesThreshold());
		pElemTmp2->setTextContent(X(tmpStr.c_str()));
	}

    // end of header -> start parsing the data
	uint16_t phaseCounter = 1;

	std::vector<unsigned char> tmpBuffer(lenght);
	inputFile.read((char*) &tmpBuffer[0], lenght);
	inputFile.close();

	uint8_t status = 0;
	uint32_t header = 0, footer = 0;

	for(uint32_t k = 0; k < (uint32_t)lenght;)
	{
		if(status == 0)
		{
			// search the beginning of the structure
			uint8_t j;
			for(j = 0; j < 8; j++)
			{
				if(tmpBuffer[k + j] != elab_header_chars[j])
					break;
			}
			if(j == 8)
			{
				// that's the header
				header = k + 8;
				status = 1;
				k += 8;
			}
			else
			{
				// re-enter the for at next position
				k++;
			}
		}
		else if(status == 1)
		{
			// header found: search the footer
			uint8_t j;
			for(j = 0; j < 8; j++)
			{
				if(tmpBuffer[k + j] != elab_footer_chars[j])
					break;
			}
			if(j == 8)
			{
				// that's the footer
				footer = k;
				status = 2;
			}
			else
			{
				// re-enter the for at next position
				k++;
			}
		}
		else if(status == 2)
		{
			// the chars between header and footer are the structures
			uint8_t sizeofWellDataStruct = sizeof(well_data_struct);
			uint8_t sizeofWellSamplingStruct = sizeof(well_sampling_struct);

			uint32_t numOfChars = sizeofWellDataStruct + sizeofWellSamplingStruct;

			// the Section sends data padded at 7 bytes so ....
			if((numOfChars % 7) > 0)
			{
				uint8_t count = ((numOfChars / 7) + 1) * 7;
				numOfChars = count;
			}

			if(numOfChars == (footer - header))
			{
				// the data size it's correct -> extract data to the 2 structures

				// this structure has only unsigned char members
				memcpy(&well_data, &tmpBuffer[header], sizeofWellDataStruct);

				// copy structure by element
				uint32_t pointer = header + sizeofWellDataStruct;
				well_sampling.area = (int64_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(int64_t));
				pointer += sizeof(int64_t);
				well_sampling.integral = (int64_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(int64_t));
				pointer += sizeof(int64_t);
//				well_sampling.area_dx = (int64_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(int64_t));
//				pointer += sizeof(int64_t);
				well_sampling.area_sx = (int64_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(int64_t));
				pointer += sizeof(int64_t);

				well_sampling.start_time = (uint32_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint32_t));
				pointer += sizeof(uint32_t);
				well_sampling.first_sample_val = (uint32_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint32_t));
				pointer += sizeof(uint32_t);
				well_sampling.last_sample_val = (uint32_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint32_t));
				pointer += sizeof(uint32_t);
				well_sampling.max_sample_val = (uint32_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint32_t));
				pointer += sizeof(uint32_t);
				well_sampling.min_sample_val = (uint32_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint32_t));
				pointer += sizeof(uint32_t);
				well_sampling.max_min_difference = (uint32_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint32_t));
				pointer += sizeof(uint32_t);
//				well_sampling.max_cycles_val = (uint32_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint32_t));
//				pointer += sizeof(uint32_t);
				well_sampling.min_cycles_val = (uint32_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint32_t));
				pointer += sizeof(uint32_t);
				well_sampling.low_threshold = (uint32_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint32_t));
				pointer += sizeof(uint32_t);
				well_sampling.high_threshold = (uint32_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint32_t));
				pointer += sizeof(uint32_t);

                well_sampling.max_sample_pos = (uint16_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint16_t));
                pointer += sizeof(uint16_t);
                well_sampling.min_sample_pos = (uint16_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint16_t));
                pointer += sizeof(uint16_t);
                well_sampling.tot_sample = (uint16_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint16_t));
				pointer += sizeof(uint16_t);

                well_sampling.air_area_counter = (uint8_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint8_t));
                pointer += sizeof(uint8_t);
                well_sampling.curve_slope = (int8_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(int8_t));
                pointer += sizeof(int8_t);
				well_sampling.result_flag = *((uint8_t *)(&tmpBuffer[pointer]));
                pointer += sizeof(uint8_t);
                well_sampling.algorithm = (uint8_t)convertCharsToNumber(&tmpBuffer[pointer], sizeof(uint8_t));

				// insert in payload

				char buffer[12];

				pElemTmp = pOutDoc->createElement(X(XML_TAG_PRESSURE_ASPIRATION_DATA));
				pRootElem->appendChild(pElemTmp);

				sprintf(buffer, "%d", phaseCounter);
				XStr xstrPhase(buffer);
				pElemTmp->setAttribute(X(XML_TAG_PRESSURE_PHASE), xstrPhase.unicodeForm());

				sprintf(buffer, "%d", well_sampling.start_time);
				XStr xstrStart(buffer);
                pElemTmp->setAttribute(X(XML_TAG_START_TIME_NAME), xstrStart.unicodeForm());

				// add records

				pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
				pElemTmp->appendChild(pElemTmp2);

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
				pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_TH_AREA_LOW));

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
				pElemTmp2->appendChild(pElemTmp3);
				sprintf(buffer, "%d", well_sampling.low_threshold);
				pElemTmp3->setTextContent(X(buffer));

				pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
				pElemTmp->appendChild(pElemTmp2);

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
				pElemTmp2->appendChild(pElemTmp3);
                                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_TH_AREA_HIGH));

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
				pElemTmp2->appendChild(pElemTmp3);
				sprintf(buffer, "%d", well_sampling.high_threshold);
				pElemTmp3->setTextContent(X(buffer));

				// to get these values we need to access the runwl object
				RunwlInfo * pRunwl = runwlInfoPointer(section);
				if(pRunwl)
				{
                    pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                    pElemTmp->appendChild(pElemTmp2);

                    pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                    pElemTmp2->appendChild(pElemTmp3);
                    pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_EMPTY_WELL));

                    pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                    pElemTmp2->appendChild(pElemTmp3);
                    sprintf(buffer, "%c", pRunwl->getStripWellProfile(strip, well_data.pos));
                    pElemTmp3->setTextContent(X(buffer));

                    pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                    pElemTmp->appendChild(pElemTmp2);

                    pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                    pElemTmp2->appendChild(pElemTmp3);
                    pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_THRESHOLD_CHK));

                    pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                    pElemTmp2->appendChild(pElemTmp3);
                    sprintf(buffer, "%c", pRunwl->getStripWellCheck(strip, well_data.pos));
                    pElemTmp3->setTextContent(X(buffer));
				}

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
				pElemTmp->appendChild(pElemTmp2);

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
				pElemTmp2->appendChild(pElemTmp3);
				pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_TOTAL_SAMPLE));

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
				pElemTmp2->appendChild(pElemTmp3);
				sprintf(buffer, "%d", well_sampling.tot_sample);
				pElemTmp3->setTextContent(X(buffer));

				pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
				pElemTmp->appendChild(pElemTmp2);

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
				pElemTmp2->appendChild(pElemTmp3);
				pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_WELL));

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
				pElemTmp2->appendChild(pElemTmp3);
                // well 9 is missing in the strip
                if(well_data.pos < 10)
                    sprintf(buffer, "%d", (well_data.pos + 1));	// well index
                else
                    sprintf(buffer, "%d", well_data.pos);	// well index
				pElemTmp3->setTextContent(X(buffer));

				pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
				pElemTmp->appendChild(pElemTmp2);

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
				pElemTmp2->appendChild(pElemTmp3);
				pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_RESULT_FLAG));

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
				pElemTmp2->appendChild(pElemTmp3);
				sprintf(buffer, "%d", well_sampling.result_flag);
				pElemTmp3->setTextContent(X(buffer));

				pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
				pElemTmp->appendChild(pElemTmp2);

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
				pElemTmp2->appendChild(pElemTmp3);
				pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_AREA));

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
				pElemTmp2->appendChild(pElemTmp3);
				sprintf(buffer, "%lld", well_sampling.area);
				pElemTmp3->setTextContent(X(buffer));

				pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
				pElemTmp->appendChild(pElemTmp2);

				pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
				pElemTmp2->appendChild(pElemTmp3);
				pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_RIGHT_AREA));

                // calculate area_dx:
                int64_t area_dx = 0;
                area_dx = (well_sampling.integral - well_sampling.area_sx -
                           (int64_t)(well_sampling.min_sample_val * well_sampling.tot_sample));
                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%lld", area_dx);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_LEFT_AREA));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%lld", well_sampling.area_sx);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_INTEGRAL));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%lld", well_sampling.integral);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_FIRST_SAMPLE_VAL));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_sampling.first_sample_val);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_LAST_SAMPLE_VAL));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_sampling.last_sample_val);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_MAX_SAMPLE_VAL));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_sampling.max_sample_val);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_MAX_SAMPLE_POS));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_sampling.max_sample_pos);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_MIN_SAMPLE_VAL));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_sampling.min_sample_val);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_MIN_SAMPLE_POS));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_sampling.min_sample_pos);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_FINAL_SLOPE));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_sampling.curve_slope);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_VOLUME));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%c", well_data.volume_char);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_SPEED));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%c", well_data.speed_char);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_COMMAND));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%c", well_data.motion_type);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_REPETITION));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_data.motion_num);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_MAXMIN_DIFFERENCE));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_sampling.max_min_difference);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_SUPER_MAX));

                // calculate max_cycle_val
                uint32_t max_cycles_val = 0;
                max_cycles_val = (well_sampling.max_min_difference + well_sampling.min_cycles_val);
                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", max_cycles_val);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_SUPER_MIN));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_sampling.min_cycles_val);
                pElemTmp3->setTextContent(X(buffer));

                pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ATTRIBUTE));
                pElemTmp->appendChild(pElemTmp2);

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
                pElemTmp2->appendChild(pElemTmp3);
                pElemTmp3->setTextContent(X(XML_TAG_PRESSURE_AIR_COUNTER));

                pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
                pElemTmp2->appendChild(pElemTmp3);
                sprintf(buffer, "%d", well_sampling.air_area_counter);
                pElemTmp3->setTextContent(X(buffer));

                phaseCounter++;

            }
            // reset status and search for the new header if present
            status = 0;
            header = 0;
            footer = 0;
            k++;
        }
    }
	tmpBuffer.clear();
    return true;
}

bool buildOpticPayload(uint8_t section, DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
    (void)section;

    if(pOutDoc == 0 || pRootElem == 0)
    {
        return false;
    }

    std::ifstream inFile;
    inFile.open(NSH_SS_FILE);
    if(inFile.fail())
    {
        //File does not exist code here
        return false;
    }

    std::string strRead;
    std::string strGolden;
    std::vector<std::string> strIndex;
    std::vector<std::string> strValue;
    std::vector<std::string> strGain;

    getline(inFile, strGolden);

    while(!inFile.eof())
    {
        getline(inFile, strRead);
        if(atoi(strRead.c_str()) == 0)
        {
            break;
        }
        strIndex.push_back(strRead);
        if(!inFile.eof())
        {
            getline(inFile, strRead);
            strValue.push_back(strRead);
            if(!inFile.eof())
            {
                getline(inFile, strRead);
                strGain.push_back(strRead);
            }
        }
    }
    inFile.close();

    // build the XML payload
    std::string tmpStr;
    DOMElement *pElemTmp, *pElemTmp2, *pElemTmp3;

    // section record out of Payload block
    pElemTmp = pOutDoc->createElement(X(XML_TAG_PRESSURE_VERSION));
    pRootElem->appendChild(pElemTmp);
    pElemTmp->setTextContent(X(XML_DATA_VERSION));

    pElemTmp = pOutDoc->createElement(X(XML_TAG_DATA_NAME));
    pRootElem->appendChild(pElemTmp);

    pElemTmp2 = pOutDoc->createElement(X(XML_TAG_KEY));
    pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setTextContent(X(XML_TAG_OPTIC_SOLID_STD_GOLDEN));

    pElemTmp2 = pOutDoc->createElement(X(XML_TAG_VALUE));
    pElemTmp->appendChild(pElemTmp2);
    pElemTmp2->setTextContent(X(strGolden.c_str()));

    uint8_t numRepetitions = strIndex.size();

    for(uint8_t n = 0; n < numRepetitions; n++)
    {
        pElemTmp = pOutDoc->createElement(X(XML_TAG_OPTIC_READING));
        pRootElem->appendChild(pElemTmp);
        XStr xstrIndex(strIndex.at(n).c_str());
        pElemTmp->setAttribute(X(XML_ATTR_INDEX_NAME), xstrIndex.unicodeForm());

        pElemTmp2 = pOutDoc->createElement(X(XML_TAG_DATA_NAME));
        pElemTmp->appendChild(pElemTmp2);

        pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
        pElemTmp2->appendChild(pElemTmp3);
        pElemTmp3->setTextContent(X(XML_TAG_PROTO_GAIN_NAME));

        pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
        pElemTmp2->appendChild(pElemTmp3);
        pElemTmp3->setTextContent(X(strGain.at(n).c_str()));

        pElemTmp2 = pOutDoc->createElement(X(XML_TAG_DATA_NAME));
        pElemTmp->appendChild(pElemTmp2);

        pElemTmp3 = pOutDoc->createElement(X(XML_TAG_KEY));
        pElemTmp2->appendChild(pElemTmp3);
        pElemTmp3->setTextContent(X(XML_TAG_OPTIC_SOLID_STD_VALUE));

        pElemTmp3 = pOutDoc->createElement(X(XML_TAG_VALUE));
        pElemTmp2->appendChild(pElemTmp3);
        pElemTmp3->setTextContent(X(strValue.at(n).c_str()));
    }
    return true;
}
