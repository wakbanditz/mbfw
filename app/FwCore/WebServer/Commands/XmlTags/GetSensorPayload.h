/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetSensorPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetSensorsPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETSENSORPAYLOAD_H
#define GETSENSORPAYLOAD_H

#include "Payload.h"
#include "CommonInclude.h"
#include "vector"

typedef struct
{
	string		strSprHome;
	string		strPumpHome;
	string		strTrayHome;
	string		strTowerHome;
	string		strTrayNtc;
	string		strSprNtc;
	string		strDoor;

} structPayloadSectionSensorValues;

typedef struct
{
	string	strNSHHome;
} structPayloadNshSensorValues;

typedef struct
{
	string		strSAFan;
	string		strSBFan;
	string		strSANtc;
	string		strSBNtc;
	string		strInstrFan;
	string		strInstrNtc;

} structPayloadInstrumentSensorValues;

typedef struct
{
	string strId;
	string strCategory;
	string strComponent;
	string strUnit;
	string strOffset;
	string strDecimals;
	string strValue;

} structGetSensorAttribute;

typedef struct
{
	string		str24V;
	string		str3V3;
	string		str5V;
	string		str1V2;

} structPayloadSectionVoltageValues;


/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETSENSOR command
 * ********************************************************************************************************************
 */

class GetSensorPayload : public Payload
{

	public:
		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		GetSensorPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~GetSensorPayload();

		/*!
		 * @brief setSensorValuesSection	Sets and formats all the values in the m_vecAttribute vector list.
		 * The formatting of the payload is related to XML standard.
		 * @param section
		 * @param sensorValues
		 */
		void setSensorValuesSection(const int section,
									const structPayloadSectionSensorValues &sensorValues);

		/*!
		 * @brief setVoltageValuesSection	Sets and formats all the values in the m_vecAttribute vector list.
		 * The formatting of the payload is related to XML standard.
		 * @param section
		 * @param voltageValues
		 */
		void setVoltageValuesSection(const int section,
									  const structPayloadSectionVoltageValues &voltageValues);


		/*!
		 * @brief setSensorValuesNsh Sets and formats all the values in the m_vecAttribute vector list.
		 * The formatting of the payload is related to XML standard.
		 * @param sensorValues
		 */
		void setSensorValuesNsh(const structPayloadNshSensorValues& sensorValues);

		/*!
		 * @brief setSensorValuesInstrument	Sets and formats all the values in the m_vecAttribute vector list.
		 * The formatting of the payload is related to XML standard.
		 * @param sensorValues
		 */
		void setSensorValuesInstrument(const structPayloadInstrumentSensorValues &sensorValues);

		/*!
		 * @brief setOutCmdByParameterRequested	Filter the element value present in m_vecAttribute based on the
		 * passed parameters and then copy the selected elements in m_vecOutAttribute;
		 * @param strCategory
		 * @param strComponent
		 * @param strId
		 */
		void setOutCmdByParameterRequested(const string &strCategory, const string &strComponent, const string &strId);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

	private:

		/*!
		 * @brief addFieldCmd Sets the passed values in the vector list.
		 * @param cmd
		 * @param strId
		 * @param strCategory
		 * @param strComponent
		 * @param strUnit
		 * @param strOffset
		 * @param strDecimals
		 * @param strVal
		 */
		void addFieldCmd(vector<structGetSensorAttribute>& cmd, const string& strId,
							const string& strCategory, const string& strComponent,
							const string& strUnit, const string& strOffset,
							const string& strDecimals, const string &strVal);

    private:

        vector<structGetSensorAttribute>	m_vecAttribute;
        string m_strCategory, m_strComponent, m_strId;
};

#endif // GETSENSORPAYLOAD_H
