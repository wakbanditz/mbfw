/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SHForceLedPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SHForceLedPayload class.
 @details

 ****************************************************************************
*/

#include "SHForceLedPayload.h"
#include "WebServerAndProtocolInclude.h"

SHForceLedPayload::SHForceLedPayload()
{
	m_strStart.clear();
	m_strDuration.clear();
}

SHForceLedPayload::~SHForceLedPayload()
{

}

void SHForceLedPayload::setOutParams(string& strStart, string& strDuration)
{
	m_strStart = strStart;
	m_strDuration = strDuration;
}

bool SHForceLedPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_VALUE_START_LOWCASE));
	pElemTmp1->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strStart.c_str()));

	DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_PROTO_DURATION_NAME));
	pElemTmp1->appendChild(pElemTmp3);
	pElemTmp3->setTextContent(X(m_strDuration.c_str()));

	return true;
}
