/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    TrayReadPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the TrayReadPayload class.
 @details

 ****************************************************************************
*/
#ifndef TRAYREADPAYLOAD_H

#define TRAYREADPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the TRAYREAD command
 * ********************************************************************************************************************
 */

class TrayReadPayload : public Payload
{
	public:

		TrayReadPayload();

		virtual ~TrayReadPayload();

		void setTrayReadAttributes(vector<int> vRFUValues, int8_t strSecId);

		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        vector<int> m_vRFUValues;
        string m_strSecId;

};

#endif // TRAYREADPAYLOAD_H
