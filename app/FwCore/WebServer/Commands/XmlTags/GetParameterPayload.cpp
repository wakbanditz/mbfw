/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetParameterPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetParameterPayload class.
 @details

 ****************************************************************************
*/

#include "GetParameterPayload.h"
#include "WebServerAndProtocolInclude.h"

GetParameterPayload::GetParameterPayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strMicrostepResolution.clear();
	m_strAccelerationFactor.clear();
	m_strOperatingCurrent.clear();
	m_strStandbyCurrent.clear();
	m_strStepConversionFactor.clear();
}

GetParameterPayload::~GetParameterPayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strMicrostepResolution.clear();
	m_strAccelerationFactor.clear();
	m_strOperatingCurrent.clear();
	m_strStandbyCurrent.clear();
	m_strStepConversionFactor.clear();
}

void GetParameterPayload::setOutValues(std::vector<string> strComponent, std::vector<string> strMotor,
									   std::vector<string> strMicroRes, std::vector<string> strAccFact,
									   std::vector<string> strOperCurr, std::vector<string> strStandbyCurr,
									   std::vector<string> strStepFact)
{
	m_strComponent = strComponent;
	m_strMotor = strMotor;
	m_strMicrostepResolution = strMicroRes;
	m_strAccelerationFactor = strAccFact;
	m_strOperatingCurrent = strOperCurr;
	m_strStandbyCurrent = strStandbyCurr;
	m_strStepConversionFactor = strStepFact;
}


bool GetParameterPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	uint8_t counter;

	for(counter = 0; counter < m_strComponent.size(); counter++)
	{
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PARAMETER_NAME));
		pElemTmp1->appendChild(pElemTmp2);

		XStr xstrComponentName(XML_ATTR_COMPONENT_NAME);
		XStr xstrComponentValue(m_strComponent.at(counter).c_str());
		pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

		XStr xstrMotorName(XML_ATTR_MOTOR_NAME);
		XStr xstrMotorValue(m_strMotor.at(counter).c_str());
		pElemTmp2->setAttribute(xstrMotorName.unicodeForm(), xstrMotorValue.unicodeForm());

		DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_MICROSTEP_RES_NAME));
		pElemTmp2->appendChild(pElemTmp3);
		pElemTmp3->setTextContent(X(m_strMicrostepResolution.at(counter).c_str()));

		DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_ACCELERATION_FACTOR_NAME));
		pElemTmp2->appendChild(pElemTmp4);
		pElemTmp4->setTextContent(X(m_strAccelerationFactor.at(counter).c_str()));

		DOMElement* pElemTmp5 = pOutDoc->createElement(X(XML_TAG_OPERATING_CURR_NAME));
		pElemTmp2->appendChild(pElemTmp5);
		pElemTmp5->setTextContent(X(m_strOperatingCurrent.at(counter).c_str()));

		DOMElement* pElemTmp6 = pOutDoc->createElement(X(XML_TAG_STANDBY_CURR_NAME));
		pElemTmp2->appendChild(pElemTmp6);
		pElemTmp6->setTextContent(X(m_strStandbyCurrent.at(counter).c_str()));

		DOMElement* pElemTmp7 = pOutDoc->createElement(X(XML_TAG_CONV_FACTOR_NAME));
		pElemTmp2->appendChild(pElemTmp7);
		pElemTmp7->setTextContent(X(m_strStepConversionFactor.at(counter).c_str()));
	}
	return true;
}
