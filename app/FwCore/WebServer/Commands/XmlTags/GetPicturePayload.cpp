#include "GetPicturePayload.h"
#include "WebServerAndProtocolInclude.h"

#include <iostream>
#include <fstream>

static const string base64Chars = {	 "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
									 "abcdefghijklmnopqrstuvwxyz"
									 "0123456789+/" };

GetPicturePayload::GetPicturePayload()
{
	m_vAttributes.clear();
	m_strIdx.clear();
}


GetPicturePayload::~GetPicturePayload()
{

}

void GetPicturePayload::setGetPictureAttr(int liIdx, int liImagesNum, int* liImageQuality,
										  structCoordinates* pCoord, const vector<vector<unsigned char>>& vData)
{
	m_strIdx.assign(to_string(liIdx));
	m_vAttributes.resize(liImagesNum);
	for ( int i = 0; i != liImagesNum; i++ )
	{
		m_vAttributes[i].strQuality.assign(to_string(liImageQuality[i]));
		m_vAttributes[i].strBottom.assign(to_string(pCoord->liBottom));
		m_vAttributes[i].strLeft.assign(to_string(pCoord->liLeft));
		m_vAttributes[i].strRight.assign(to_string(pCoord->liRight));
		m_vAttributes[i].strTop.assign(to_string(pCoord->liTop));
		// Encoding base 64
//		encodeDataBase64(vData[i].data(), vData[i].size(), m_vAttributes[i].strData); // need to be decommented with the new version of SSW

		string strTmp("");
		encodeDataBase64(vData[i].data(), vData[i].size(), strTmp);
		encodeDataBase64((const unsigned char*)strTmp.c_str(), strTmp.size(), m_vAttributes[i].strData);
	}
}

bool GetPicturePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);
	XStr xstrIdName(XML_ATTR_PAYLOAD_IDX_NAME);
	XStr xstrIdValue(m_strIdx.c_str());
	pElemTmp1->setAttribute(xstrIdName.unicodeForm(), xstrIdValue.unicodeForm());

	for ( uint32_t i = 0; i != m_vAttributes.size(); i++ )
	{
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_IMAGE_NAME));
		pElemTmp1->appendChild(pElemTmp2);
		XStr xstrIdName(XML_ATTR_IMAGE_QUALITY_NAME);
		XStr xstrIdValue(m_vAttributes[i].strQuality.c_str());
		pElemTmp2->setAttribute(xstrIdName.unicodeForm(), xstrIdValue.unicodeForm());

		DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_TAG_ROI_NAME));
		pElemTmp2->appendChild(pElemTmp3);

		DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_TOP_NAME));
		pElemTmp3->appendChild(pElemTmp4);
		pElemTmp4->setTextContent(X(m_vAttributes[i].strTop.c_str()));

		DOMElement* pElemTmp5 = pOutDoc->createElement(X(XML_TAG_BOTTOM_NAME));
		pElemTmp3->appendChild(pElemTmp5);
		pElemTmp5->setTextContent(X(m_vAttributes[i].strBottom.c_str()));

		DOMElement* pElemTmp6 = pOutDoc->createElement(X(XML_TAG_LEFT_NAME));
		pElemTmp3->appendChild(pElemTmp6);
		pElemTmp6->setTextContent(X(m_vAttributes[i].strLeft.c_str()));

		DOMElement* pElemTmp7 = pOutDoc->createElement(X(XML_TAG_RIGHT_NAME));
		pElemTmp3->appendChild(pElemTmp7);
		pElemTmp7->setTextContent(X(m_vAttributes[i].strRight.c_str()));

		DOMElement* pElemTmp8 = pOutDoc->createElement(X(XML_TAG_DATA_NAME));
		pElemTmp2->appendChild(pElemTmp8);
		pElemTmp8->setTextContent(X((const char*)m_vAttributes[i].strData.c_str()));
	}

	return true;
}

bool GetPicturePayload::encodeDataBase64(const unsigned char* pucData, int liBufLen, string& strEncoded)
{
	if ( pucData == nullptr )	return false;

	strEncoded.clear();
	int i = 0;
	int j = 0;
	unsigned char ucArray3[3];
	unsigned char ucArray4[4];

	while ( liBufLen-- )
	{
		ucArray3[i++] = *(pucData++);

		if ( i == 3 )
		{
			ucArray4[0] = (ucArray3[0] & 0xfc) >> 2;
			ucArray4[1] = ((ucArray3[0] & 0x03) << 4) + ((ucArray3[1] & 0xf0) >> 4);
			ucArray4[2] = ((ucArray3[1] & 0x0f) << 2) + ((ucArray3[2] & 0xc0) >> 6);
			ucArray4[3] = ucArray3[2] & 0x3f;

			for ( i = 0; i < 4; i++ )
			{
				unsigned char ucTmp = base64Chars[ucArray4[i]];
				strEncoded += ucTmp;
			}

			i = 0;
		}
	}

	if ( i )
	{
		for ( j = i; j < 3; j++ )
		{
			ucArray3[j] = '\0';
		}

		ucArray4[0] = (ucArray3[0] & 0xfc) >> 2;
		ucArray4[1] = ((ucArray3[0] & 0x03) << 4) + ((ucArray3[1] & 0xf0) >> 4);
		ucArray4[2] = ((ucArray3[1] & 0x0f) << 2) + ((ucArray3[2] & 0xc0) >> 6);
		ucArray4[3] = ucArray3[2] & 0x3f;

		for ( j = 0; j < i+1; j++ )
		{
			strEncoded += base64Chars[ucArray4[j]];
		}

		while ( ( i++ < 3 ) )
		{
			strEncoded += '=';
		}
	}

	return true;
}
