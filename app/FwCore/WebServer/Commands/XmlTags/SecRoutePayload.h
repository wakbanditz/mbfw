/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SecRoutePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SecRoutePayload class.
 @details

 ****************************************************************************
*/

#ifndef SECROUTEPAYLOAD_H
#define SECROUTEPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the ROUTE command
 * ********************************************************************************************************************
 */

class SecRoutePayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief SecRoutePayload default constructor
		 * ***********************************************************************************************************
		 */
		SecRoutePayload();

		/*! ***********************************************************************************************************
		 * @brief getSectionReply	retrieves the string reply sent from the instrument
		 * @return					the reply as a std::string
		 * ************************************************************************************************************
		 */
		std::string getSectionReply(void);

		/*! ***********************************************************************************************************
		 * @brief setSectionReply	set the string reply sent from the instrument
		 * @param strReply			std::string that represents the section reply
		 * ***********************************************************************************************************
		 */
		void setSectionReply(std::string strReply);

		/*! ***********************************************************************************************************
		 * @brief getComponent		retrieves the component involved in procedure
		 * @return					the component as a std::string
		 * ************************************************************************************************************
		 */
		std::string getComponent();

		/*! ***********************************************************************************************************
		 * @brief setComponent	set the component involved
		 * @param section		index of the component, converted in a string
		 * ***********************************************************************************************************
		 */
		void setComponent(int section);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);


	private:

		std::string m_strReply;
		std::string m_strSection;
};


#endif // SECROUTEPAYLOAD_H
