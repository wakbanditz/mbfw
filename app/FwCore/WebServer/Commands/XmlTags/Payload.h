/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    Payload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the Payload class.
 @details

 ****************************************************************************
*/

#ifndef PAYLOAD_H
#define PAYLOAD_H

#include <vector>

#include "XStr.h"
#include "StrX.h"

#include "XmlTag.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMDocument.hpp>

using namespace std;
using namespace xercesc_3_2;

/*! *******************************************************************************************************************
 * @brief class derived from XmlTag	containing the payload of a outgoing command reply
 * ********************************************************************************************************************
 */

class Payload : public XmlTag
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Payload default constructor
		 * ***********************************************************************************************************
		 */
		Payload();

		/*! ***********************************************************************************************************
		 * @brief Payload default destructor
		 * ***********************************************************************************************************
		 */
		virtual ~Payload();

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure, specific of each command
         * @param pOutDoc pointer to DOM document
         * @param pRootElem pointer to DOM root
		 * ***********************************************************************************************************
		 */
		virtual bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

		/*! ***********************************************************************************************************
		 * @brief encodeDataBase64FromFile read the input file as binary and pass the content toi the encoding function
		 *		encodeDataBase64, returning the encode string
		 * @param nameFile the complete filename (included path)
		 * @return the encoded string (empty in case of error)
		 * ***********************************************************************************************************
		 */
		std::string encodeDataBase64FromFile(const char * nameFile);

};

/*! ***********************************************************************************************************
 * @brief This type of encoding consists in dividing the streams in group of 6bites to be rapresented with a
 *		base 64 logic ( 2^6 ). It is important to avoid NULL char to be sent.
 * ***********************************************************************************************************
 */
bool encodeDataBase64(const unsigned char* pucData, int liBufLen, string& strEncoded);

/*! ***********************************************************************************************************
 * @brief This type of decoding consists in dividing the streams in group of 6bites to be rapresented with a
 *		base 64 logic ( 2^6 ). It is important to avoid NULL char to be sent.
 * ***********************************************************************************************************
 */
bool decodeDataBase64(std::string const& encoded_string, string& strDecoded);

/*! ***********************************************************************************************************
 * @brief convertCharsToNumber convert the buffer to a number of any size
 *		the LSB is the last one, the MSB is the first one of the buffer
 * ***********************************************************************************************************
 */
int64_t convertCharsToNumber(unsigned char * buffer, uint8_t numberSize);

#endif // PAYLOAD_H
