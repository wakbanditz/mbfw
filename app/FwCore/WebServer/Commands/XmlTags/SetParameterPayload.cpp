/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SetParameterPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SetParameterPayload class.
 @details

 ****************************************************************************
*/

#include "SetParameterPayload.h"
#include "WebServerAndProtocolInclude.h"

SetParameterPayload::SetParameterPayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
}

SetParameterPayload::~SetParameterPayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
}

void SetParameterPayload::setOutValues(std::string strComponent, std::string strMotor)
{
	m_strComponent.assign(strComponent);
	m_strMotor.assign(strMotor);
}

bool SetParameterPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);


	DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_ATTR_COMPONENT_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strComponent.c_str()));

	pElemTmp2 = pOutDoc->createElement(X(XML_ATTR_MOTOR_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	pElemTmp2->setTextContent(X(m_strMotor.c_str()));

	return true;
}


