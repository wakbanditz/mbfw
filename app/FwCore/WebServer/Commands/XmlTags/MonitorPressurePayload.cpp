/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    MonitorPressurePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the MonitorPressurePayload class.
 @details

 ****************************************************************************
*/

#include "MonitorPressurePayload.h"
#include "WebServerAndProtocolInclude.h"

#define RAW_DATA_HEADER	0x00000091


MonitorPressurePayload::MonitorPressurePayload()
{

}

bool MonitorPressurePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);
	pElemTmp1->setAttribute(X(XML_ATTR_SAMPLING_PERIOD), X(XML_SAMPLING_PERIOD_VALUE));

	DOMElement*  pElemTmp2 = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
	pElemTmp1->appendChild(pElemTmp2);
	if(m_section == SCT_A_ID)
		pElemTmp2->setAttribute(X(XML_ATTR_LETTER_NAME), X(XML_VALUE_SECTION_A));
	else
		pElemTmp2->setAttribute(X(XML_ATTR_LETTER_NAME), X(XML_VALUE_SECTION_B));

	char bufferTmp[64];
	std::string strTmp1;

	for(uint8_t channel = 0; channel < SCT_NUM_TOT_SLOTS; channel++)
	{
		DOMElement*  pElemTmp3 = pOutDoc->createElement(X(XML_TAG_SLOT_NAME));
		pElemTmp2->appendChild(pElemTmp3);

		sprintf(bufferTmp, "%d", channel + 1);
		strTmp1.assign(bufferTmp);
		pElemTmp3->setAttribute(X(XML_ATTR_NUMBER_NAME), X(strTmp1.c_str()));

		for(uint16_t index = 0; index < m_dataPressureChannel[channel].size(); index++)
		{
			DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_DATA_NAME));
			pElemTmp3->appendChild(pElemTmp4);

			sprintf(bufferTmp, "%d", m_dataPressureChannel[channel].at(index));
			strTmp1.assign(bufferTmp);
			pElemTmp4->setAttribute(X(XML_ATTR_INDEX_NAME), X(strTmp1.c_str()));

			if(++index >= m_dataPressureChannel[channel].size()) break;

			sprintf(bufferTmp, "%d", m_dataPressureChannel[channel].at(index));
			strTmp1.assign(bufferTmp);
			pElemTmp4->setTextContent(X(strTmp1.c_str()));
		}
		m_dataPressureChannel[channel].clear();
	}
	return true;
}

void MonitorPressurePayload::setSection(int section)
{
	m_section = section;
}

bool MonitorPressurePayload::assignPressureData(int channel, std::deque<uint32_t>* pDataPressureChannel)
{
    if(pDataPressureChannel->size() == 0)
    {
        return false;
    }

    m_dataPressureChannel[channel].assign(pDataPressureChannel->begin(), pDataPressureChannel->end());
	return true;
}
