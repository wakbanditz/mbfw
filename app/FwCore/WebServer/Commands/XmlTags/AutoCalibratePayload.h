/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    AutoCalibratePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the AutoCalibratePayload class.
 @details

 ****************************************************************************
*/

#ifndef AUTOCALIBRATEPAYLOAD_H
#define AUTOCALIBRATEPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the AUTOCALIBRATE command
 * ********************************************************************************************************************
 */

class AutoCalibratePayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		AutoCalibratePayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~AutoCalibratePayload();


		void setParams(const string& strRFUVal, const string& strGain);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        string m_strRFUVal;
        string m_strGain;

};

#endif // AUTOCALIBRATEPAYLOAD_H
