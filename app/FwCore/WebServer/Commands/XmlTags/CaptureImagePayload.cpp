/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CaptureImagePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CaptureImagePayload class.
 @details

 ****************************************************************************
*/

#include "CaptureImagePayload.h"
#include "WebServerAndProtocolInclude.h"

#include "WFCaptureImage.h"

#include <iostream>
#include <fstream>

CaptureImagePayload::CaptureImagePayload()
{
	m_vAttributes.clear();
	m_strIdx.clear();
}


CaptureImagePayload::~CaptureImagePayload()
{

}

void CaptureImagePayload::setGetPictureAttr(int liIdx, structCameraParams& sParams, const vector<vector<unsigned char>>& vData)
{
	m_strIdx.assign(to_string(liIdx));
	m_vAttributes.resize(sParams.liPicturesNum);

	for ( int i = 0; i != sParams.liPicturesNum; i++ )
	{
		m_vAttributes[i].vFormat.assign(sParams.vFormat[i]);
		m_vAttributes[i].strLight.assign(to_string(sParams.vLight[i]));
		if ( ! sParams.vFormat[i].compare("RAW") )
		{
			m_vAttributes[i].strFocus.assign(sParams.vFocus[i]);
		}
		else
		{
			m_vAttributes[i].strQuality.assign(to_string(sParams.vQuality[i]));
		}

		// Encoding base 64
		encodeDataBase64(vData[i].data(), vData[i].size(), m_vAttributes[i].strData);
	}
}

bool CaptureImagePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);
	XStr xstrIdName(XML_ATTR_PAYLOAD_IDX_NAME);
	XStr xstrIdValue(m_strIdx.c_str());
	pElemTmp1->setAttribute(xstrIdName.unicodeForm(), xstrIdValue.unicodeForm());

	for ( uint32_t i = 0; i != m_vAttributes.size(); i++ )
	{
        DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_IMAGE_NAME));
        pElemTmp1->appendChild(pElemTmp2);

		XStr xstrIdFormat(XML_ATTR_IMAGE_FORMAT_NAME);
		XStr xstrIdFormatValue(m_vAttributes[i].vFormat.c_str());
		pElemTmp2->setAttribute(xstrIdFormat.unicodeForm(), xstrIdFormatValue.unicodeForm());

		if ( ! m_vAttributes[i].vFormat.compare("JPEG") )
		{
			if ( ! m_vAttributes[i].strQuality.compare("-1") )	m_vAttributes[i].strQuality = "1";

			XStr xstrIdQuality(XML_ATTR_IMAGE_QUALITY_NAME);
			XStr xstrIdQualityValue(m_vAttributes[i].strQuality.c_str());
			pElemTmp2->setAttribute(xstrIdQuality.unicodeForm(), xstrIdQualityValue.unicodeForm());
		}
		else if ( ! m_vAttributes[i].strFocus.empty() )
		{
			XStr xstrIdFocus(XML_ATTR_IMAGE_FOCUS_NAME);
			string strFocus(m_vAttributes[i].strFocus);
			XStr xstrIdFocusValue(strFocus.c_str());
			pElemTmp2->setAttribute(xstrIdFocus.unicodeForm(), xstrIdFocusValue.unicodeForm());
		}

		if ( m_vAttributes[i].strLight.compare("-1") )
		{
			XStr xstrIdLight(XML_ATTR_IMAGE_LIGHT_NAME);
			XStr xstrIdLightValue(m_vAttributes[i].strLight.c_str());
			pElemTmp2->setAttribute(xstrIdLight.unicodeForm(), xstrIdLightValue.unicodeForm());
		}

        DOMElement* pElemTmp9 = pOutDoc->createElement(X(XML_TAG_DATA_NAME));
        pElemTmp2->appendChild(pElemTmp9);
		pElemTmp9->setTextContent(X(m_vAttributes[i].strData.c_str()));
    }

	return true;
}

