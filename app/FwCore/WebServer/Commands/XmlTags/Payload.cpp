/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    Payload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the Payload class.
 @details

 ****************************************************************************
*/

#include "Payload.h"
#include "WebServerAndProtocolInclude.h"

#include <fstream>

static const string base64Chars = {	 "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
									 "abcdefghijklmnopqrstuvwxyz"
									 "0123456789+/" };


static inline bool is_base64(unsigned char c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}

Payload::Payload(): XmlTag(XML_TAG_PAYLOAD_NAME)
{

}

Payload::~Payload()
{

}

bool Payload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	// just to avoid warnings ....
	(void)pOutDoc;
	(void)pRootElem;

	return false;
}

std::string Payload::encodeDataBase64FromFile(const char * nameFile)
{
	std::string strEncoded;

	strEncoded.clear();

	ifstream inputFile;

	// open input file, calculate lenght and read it
	inputFile.open(nameFile, ios_base::binary);
	if(inputFile.fail())
	{
		return strEncoded;
	}


	inputFile.seekg(0, ios_base::end);
	int lenght = inputFile.tellg();
	inputFile.seekg(0, ios_base::beg);

	if(lenght == 0)
	{
		inputFile.close();
		return strEncoded;
	}

	unsigned char * tmpBuffer = new unsigned char[lenght];

    if(tmpBuffer)
    {
        // copy the value of the pointer to delete it later
        unsigned char * tmpDeleteBuffer = tmpBuffer;

        inputFile.read((char *)tmpBuffer, lenght);

        inputFile.close();

        if(encodeDataBase64((const unsigned char *)tmpBuffer, lenght, strEncoded) == false)
        {
            strEncoded.clear();
        }

        delete[] tmpDeleteBuffer;
    }
	return strEncoded;
}

bool encodeDataBase64(const unsigned char* pucData, int liBufLen, string& strEncoded)
{
	strEncoded.clear();

	if ( pucData == nullptr )	return false;

	int i = 0;
	int j = 0;
	unsigned char ucArray3[3];
	unsigned char ucArray4[4];

	while ( liBufLen-- )
	{
		ucArray3[i++] = *(pucData++);

		if ( i == 3 )
		{
			ucArray4[0] = (ucArray3[0] & 0xfc) >> 2;
			ucArray4[1] = ((ucArray3[0] & 0x03) << 4) + ((ucArray3[1] & 0xf0) >> 4);
			ucArray4[2] = ((ucArray3[1] & 0x0f) << 2) + ((ucArray3[2] & 0xc0) >> 6);
			ucArray4[3] = ucArray3[2] & 0x3f;

			for ( i = 0; i < 4; i++ )
			{
				unsigned char ucTmp = base64Chars[ucArray4[i]];
				strEncoded += ucTmp;
			}

			i = 0;
		}
	}

	if ( i )
	{
		for ( j = i; j < 3; j++ )
		{
			ucArray3[j] = '\0';
		}

		ucArray4[0] = (ucArray3[0] & 0xfc) >> 2;
		ucArray4[1] = ((ucArray3[0] & 0x03) << 4) + ((ucArray3[1] & 0xf0) >> 4);
		ucArray4[2] = ((ucArray3[1] & 0x0f) << 2) + ((ucArray3[2] & 0xc0) >> 6);
		ucArray4[3] = ucArray3[2] & 0x3f;

		for ( j = 0; j < i+1; j++ )
		{
			strEncoded += base64Chars[ucArray4[j]];
		}

		while ( ( i++ < 3 ) )
		{
			strEncoded += '=';
		}
	}

	return true;
}

bool decodeDataBase64(std::string const& encoded_string, string& strDecoded)
{
  int in_len = encoded_string.size();
  int i = 0;
  int j = 0;
  int in_ = 0;
  unsigned char char_array_4[4], char_array_3[3];
  std::vector<unsigned char> retVector;

  retVector.clear();

  while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
	char_array_4[i++] = encoded_string[in_]; in_++;
	if (i ==4) {
	  for (i = 0; i <4; i++)
		char_array_4[i] = base64Chars.find(char_array_4[i]);

	  char_array_3[0] = ( char_array_4[0] << 2       ) + ((char_array_4[1] & 0x30) >> 4);
	  char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
	  char_array_3[2] = ((char_array_4[2] & 0x3) << 6) +   char_array_4[3];

	  for (i = 0; (i < 3); i++)
		retVector.push_back(char_array_3[i]);
	  i = 0;
	}
  }

  if (i)
  {
	for (j = i; j < 4; j++)
		   char_array_4[j] = 0;

	for (j = 0; j < i; j++)
	  char_array_4[j] = base64Chars.find(char_array_4[j]);

	char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
	char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
	char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];


	for (j = 0; (j < i - 1); j++) retVector.push_back(char_array_3[j]);
  }

  std::string ret(retVector.begin(), retVector.end());

  strDecoded.assign(ret);

  return (strDecoded.size() > 0);
}

int64_t convertCharsToNumber(unsigned char * buffer, uint8_t numberSize)
{
	uint8_t counter;
	int64_t retValue = 0;

	for(counter = 0; counter < numberSize; counter++)
	{
		uint8_t addendum = *(buffer + numberSize - (counter + 1));
		retValue += (int64_t)(addendum) << ( 8 * counter);
	}

	return retValue;
}
