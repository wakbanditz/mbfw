/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OPTCalibratePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OPTCalibratePayload class.
 @details

 ****************************************************************************
*/

#ifndef OPTCALIBRATEPAYLOAD_H
#define OPTCALIBRATEPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the OPTCALIBRATE command
 * ********************************************************************************************************************
 */

class OPTCalibratePayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		OPTCalibratePayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~OPTCalibratePayload();

        void setAttributes(const string& strSection, const string& strSlot, const string& strGain,
                           const string& strSSVal, const string& strID);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        string m_strSection;
        string m_strSlot;
        string m_strGain;
        string m_strSSVal;
        string m_strID;

};

#endif // OPTCALIBRATEPAYLOAD_H
