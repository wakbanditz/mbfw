/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SetMotorActivationPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SetMotorActivationPayload class.
 @details

 ****************************************************************************
*/

#ifndef SETMOTORACTIVATIONPAYLOAD_H
#define SETMOTORACTIVATIONPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the SETMOTORACTIVATION command
 * ********************************************************************************************************************
 */

class SetMotorActivationPayload : public Payload
{
	public:
		/*! ***********************************************************************************************************
		 * @brief MovePayload constructor
		 * ***********************************************************************************************************
		 */
		SetMotorActivationPayload();

		/*! ***********************************************************************************************************
		 * @brief ~MovePayload destructor
		 * ***********************************************************************************************************
		 */
		virtual ~SetMotorActivationPayload();

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

		/*!
		 * @brief setOutParams	set the out parameters
		 * @param strComponent
		 * @param strMotor
		 * @param strActivation
		 */
		void setOutParams(string &strComponent, string &strMotor, string &strActivation);

    private:
        string m_strComponent;
        string m_strMotor;
        string m_strActivation;

};

#endif // SETMOTORACTIVATIONPAYLOAD_H
