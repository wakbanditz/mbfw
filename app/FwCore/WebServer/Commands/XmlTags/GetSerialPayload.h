/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetSerialPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetSerialPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETSERIALPAYLOAD_H
#define GETSERIALPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETSERIAL command
 * ********************************************************************************************************************
 */

class GetSerialPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief GetSerialPayload default constructor
		 * ***********************************************************************************************************
		 */
		GetSerialPayload();

		/*! ***********************************************************************************************************
		 * @brief getSerialInstrument			retrieves the instrument serial number
		 * @return					the instrument serial number as a std::string
		 * ************************************************************************************************************
		 */
		std::string getSerialInstrument(void);

		/*! ***********************************************************************************************************
		 * @brief setSerialInstrument	set the instrument serial number
		 * @param strName			std::string that represents the instrument serial number
		 * ***********************************************************************************************************
		 */
		void setSerialInstrument(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getSerialMasterBoard			retrieves the MasterBoard serial number
		 * @return					the MasterBoard serial number as a std::string
		 * ************************************************************************************************************
		 */
		std::string getSerialMasterBoard(void);

		/*! ***********************************************************************************************************
		 * @brief setSerialMasterBoard	set the MasterBoard serial number
		 * @param strName			std::string that represents the MasterBoard serial number
		 * ***********************************************************************************************************
		 */
		void setSerialMasterBoard(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getSerialNSH			retrieves the NSH serial number
		 * @return					the NSH serial number as a std::string
		 * ************************************************************************************************************
		 */
		std::string getSerialNSH(void);

		/*! ***********************************************************************************************************
		 * @brief setSerialNSH	set the NSH serial number
		 * @param strName			std::string that represents the NSH serial number
		 * ***********************************************************************************************************
		 */
		void setSerialNSH(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getSerialSectionA			retrieves the SectionA serial number
		 * @return					the SectionA serial number as a std::string
		 * ************************************************************************************************************
		 */
		std::string getSerialSectionA(void);

		/*! ***********************************************************************************************************
		 * @brief setSerialSectionA	set the SectionA serial number
		 * @param strName			std::string that represents the SectionA serial number
		 * ***********************************************************************************************************
		 */
		void setSerialSectionA(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getSerialSectionB			retrieves the SectionB serial number
		 * @return					the SectionB serial number as a std::string
		 * ************************************************************************************************************
		 */
		std::string getSerialSectionB(void);

		/*! ***********************************************************************************************************
		 * @brief setSerialSectionB	set the SectionB serial number
		 * @param strName			std::string that represents the SectionB serial number
		 * ***********************************************************************************************************
		 */
		void setSerialSectionB(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief getSerialCamera			retrieves the Camera serial number
		 * @return					the Camera serial number as a std::string
		 * ************************************************************************************************************
		 */
		std::string getSerialCamera(void);

		/*! ***********************************************************************************************************
		 * @brief setSerialCamera	set the Camera serial number
		 * @param strName			std::string that represents the Camera serial number
		 * ***********************************************************************************************************
		 */
		void setSerialCamera(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

	private:
		std::string m_strSerialInstrument;
		std::string m_strSerialMasterBoard;
		std::string m_strSerialNSH;
		std::string m_strSerialSectionA;
		std::string m_strSerialSectionB;
		std::string m_strSerialCamera;

};

#endif // GETSERIALPAYLOAD_H
