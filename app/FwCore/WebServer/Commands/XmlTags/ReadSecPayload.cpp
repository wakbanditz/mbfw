/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ReadSecPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the ReadSecPayload class.
 @details

 ****************************************************************************
*/

#include "ReadSecPayload.h"
#include "WebServerAndProtocolInclude.h"

#define SLOTS_NUM	6

ReadSecPayload::ReadSecPayload()
{
	m_strSection.clear();
	m_vPayload.resize(SLOTS_NUM);
}

ReadSecPayload::~ReadSecPayload()
{

}

void ReadSecPayload::setParams(const string& strSection, vector<structReadSlot>* pSlotsInfo)
{
	if ( pSlotsInfo == nullptr )	return;

	m_strSection.assign(strSection);

	for ( size_t s = 0; s < pSlotsInfo->size(); ++s )
	{
		m_vPayload[s].bIsEnabled = pSlotsInfo->at(s).bIsEnabled;
		if ( ! pSlotsInfo->at(s).bIsEnabled )	continue;

		m_vPayload[s].Spr.bIsEnabled = pSlotsInfo->at(s).Spr.bIsEnabled;
		if ( pSlotsInfo->at(s).Spr.bIsEnabled )
		{
			m_vPayload[s].Spr.strCode = pSlotsInfo->at(s).Spr.strCode;
			if ( ! pSlotsInfo->at(s).Spr.vImage.empty() )
			{
				encodeDataBase64(pSlotsInfo->at(s).Spr.vImage.data(), pSlotsInfo->at(s).Spr.vImage.size(),
								 m_vPayload[s].Spr.strImage);
			}
		}

		m_vPayload[s].Strip.bIsEnabled = pSlotsInfo->at(s).Strip.bIsEnabled;
		if ( pSlotsInfo->at(s).Strip.bIsEnabled )
		{
			m_vPayload[s].Strip.strCode = pSlotsInfo->at(s).Strip.strCode;
			if ( ! pSlotsInfo->at(s).Strip.vImage.empty() )
			{
				encodeDataBase64(pSlotsInfo->at(s).Strip.vImage.data(), pSlotsInfo->at(s).Strip.vImage.size(),
								 m_vPayload[s].Strip.strImage);
			}
		}

		m_vPayload[s].Substrate.bIsEnabled = pSlotsInfo->at(s).Substrate.bIsEnabled;
		if ( pSlotsInfo->at(s).Substrate.bIsEnabled )
		{
			m_vPayload[s].Substrate.strSubsRead = to_string(pSlotsInfo->at(s).Substrate.liSubsRead);
		}

		m_vPayload[s].W0.bIsEnabled = pSlotsInfo->at(s).W0.bIsEnabled;
		if ( pSlotsInfo->at(s).W0.bIsEnabled )
		{
			m_vPayload[s].W0.strWell = to_string(pSlotsInfo->at(s).W0.liWell);
			m_vPayload[s].W0.strScore = to_string(pSlotsInfo->at(s).W0.liScore);
			m_vPayload[s].W0.strSampleIsPresent = to_string(pSlotsInfo->at(s).W0.bSampleIsPresent);
			if ( ! pSlotsInfo->at(s).W0.vImage.empty() )
			{
				encodeDataBase64(pSlotsInfo->at(s).W0.vImage.data(), pSlotsInfo->at(s).W0.vImage.size(),
								 m_vPayload[s].W0.strImage);
			}
		}

		m_vPayload[s].W3.bIsEnabled = pSlotsInfo->at(s).W3.bIsEnabled;
		if ( pSlotsInfo->at(s).W3.bIsEnabled )
		{
			m_vPayload[s].W3.strWell = to_string(pSlotsInfo->at(s).W3.liWell);
			m_vPayload[s].W3.strScore = to_string(pSlotsInfo->at(s).W3.liScore);
			m_vPayload[s].W3.strSampleIsPresent = to_string(pSlotsInfo->at(s).W3.bSampleIsPresent);
			if ( ! pSlotsInfo->at(s).W3.vImage.empty() )
			{
				encodeDataBase64(pSlotsInfo->at(s).W3.vImage.data(), pSlotsInfo->at(s).W3.vImage.size(),
								 m_vPayload[s].W3.strImage);
			}
		}
	}

	return;
}


bool ReadSecPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	if (pOutDoc == nullptr || pRootElem == nullptr )            return false;

    DOMElement*  pElemTmp = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
    pRootElem->appendChild(pElemTmp);

	DOMElement* pSectionElem = pOutDoc->createElement(X(XML_TAG_SECTION_NAME));
	pElemTmp->appendChild(pSectionElem);
	pSectionElem->setTextContent(X(m_strSection.c_str()));

	for ( size_t i = 0; i != m_vPayload.size(); ++i )
	{
		if ( ! m_vPayload[i].bIsEnabled )	continue;

		DOMElement* pSlotElem = pOutDoc->createElement(X(XML_TAG_SLOT_NAME));
		pElemTmp->appendChild(pSlotElem);
		XStr xstrId(XML_ATTR_NUMBER_NAME);
		XStr xstrIdValue(to_string(i+1).c_str());
		pSlotElem->setAttribute(xstrId.unicodeForm(), xstrIdValue.unicodeForm());

		if ( m_vPayload[i].Spr.bIsEnabled )
		{
			DOMElement* pSprElem = pOutDoc->createElement(X(XML_TAG_SPR_NAME));
			pSlotElem->appendChild(pSprElem);

			DOMElement* pBarcodeElem = pOutDoc->createElement(X(XML_TAG_BARCODE_NAME));
			pSprElem->appendChild(pBarcodeElem);
			pBarcodeElem->setTextContent(X(m_vPayload[i].Spr.strCode.c_str()));

			if ( ! m_vPayload[i].Spr.strImage.empty() )
			{
				DOMElement* pDmImageElem = pOutDoc->createElement(X(XML_TAG_IMAGE_NAME));
				pSprElem->appendChild(pDmImageElem);
				pDmImageElem->setTextContent(X(m_vPayload[i].Spr.strImage.c_str()));
			}
		}

		if ( m_vPayload[i].Strip.bIsEnabled )
		{
			DOMElement* pStripElem = pOutDoc->createElement(X(XML_TAG_STRIP_NAME));
			pSlotElem->appendChild(pStripElem);

			DOMElement* pBarcodeElem = pOutDoc->createElement(X(XML_TAG_BARCODE_NAME));
			pStripElem->appendChild(pBarcodeElem);
			pBarcodeElem->setTextContent(X(m_vPayload[i].Strip.strCode.c_str()));

			if ( ! m_vPayload[i].Strip.strImage.empty() )
			{
				DOMElement* pBcImageElem = pOutDoc->createElement(X(XML_TAG_IMAGE_NAME));
				pStripElem->appendChild(pBcImageElem);
				pBcImageElem->setTextContent(X(m_vPayload[i].Strip.strImage.c_str()));
			}
		}

		if ( m_vPayload[i].Substrate.bIsEnabled )
		{
			DOMElement* pSubstrElem = pOutDoc->createElement(X(XML_ATTR_SUBSTRATE_NAME));
			pSlotElem->appendChild(pSubstrElem);
			pSubstrElem->setTextContent(X(m_vPayload[i].Substrate.strSubsRead.c_str()));
		}

		if ( m_vPayload[i].W0.bIsEnabled )
		{
			DOMElement* pSample0Elem = pOutDoc->createElement(X(XML_ATTR_SAMPLE_NAME));
			pSlotElem->appendChild(pSample0Elem);

			XStr xstrIdScore(XML_ATTR_SCORE_NAME);
			XStr xstrIdScoreValue(m_vPayload[i].W0.strScore.c_str());
			pSample0Elem->setAttribute(xstrIdScore.unicodeForm(), xstrIdScoreValue.unicodeForm());
			XStr xstrIdWell(XML_TAG_WELL_NAME);
			XStr xstrIdWellValue(m_vPayload[i].W0.strWell.c_str());
			pSample0Elem->setAttribute(xstrIdWell.unicodeForm(), xstrIdWellValue.unicodeForm());

			DOMElement* pDetectionElem = pOutDoc->createElement(X(XML_TAG_DETECTION_NAME));
			pSample0Elem->appendChild(pDetectionElem);
			pDetectionElem->setTextContent(X(m_vPayload[i].W0.strSampleIsPresent.c_str()));

			if ( ! m_vPayload[i].W0.strImage.empty() )
			{
				DOMElement* pSampleImageElem = pOutDoc->createElement(X(XML_TAG_IMAGE_NAME));
				pSample0Elem->appendChild(pSampleImageElem);
				pSampleImageElem->setTextContent(X(m_vPayload[i].W0.strImage.c_str()));
			}
		}

		if ( m_vPayload[i].W3.bIsEnabled )
		{
			DOMElement* pSample3Elem = pOutDoc->createElement(X(XML_ATTR_SAMPLE_NAME));
			pSlotElem->appendChild(pSample3Elem);

			XStr xstrIdScore(XML_ATTR_SCORE_NAME);
			XStr xstrIdScoreValue(m_vPayload[i].W3.strScore.c_str());
			pSample3Elem->setAttribute(xstrIdScore.unicodeForm(), xstrIdScoreValue.unicodeForm());
			XStr xstrIdWell(XML_TAG_WELL_NAME);
			XStr xstrIdWellValue(m_vPayload[i].W3.strWell.c_str());
			pSample3Elem->setAttribute(xstrIdWell.unicodeForm(), xstrIdWellValue.unicodeForm());

			DOMElement* pDetectionElem = pOutDoc->createElement(X(XML_TAG_DETECTION_NAME));
			pSample3Elem->appendChild(pDetectionElem);
			pDetectionElem->setTextContent(X(m_vPayload[i].W3.strSampleIsPresent.c_str()));

			if ( ! m_vPayload[i].W3.strImage.empty() )
			{
				DOMElement* pSampleImageElem = pOutDoc->createElement(X(XML_TAG_IMAGE_NAME));
				pSample3Elem->appendChild(pSampleImageElem);
				pSampleImageElem->setTextContent(X(m_vPayload[i].W3.strImage.c_str()));
			}
		}
	}

    return true;
}

