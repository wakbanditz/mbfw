/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OPTCheckPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OPTCalibratePayload class.
 @details

 ****************************************************************************
*/

#ifndef OPTCHECKPAYLOAD_H
#define OPTCHECKPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the OPT_CHECK command
 * ********************************************************************************************************************
 */

class OPTCheckPayload : public Payload
{
    public:

        /*! ***********************************************************************************************************
         * @brief Constructor
         * ***********************************************************************************************************
         */
        OPTCheckPayload();

        /*! ***********************************************************************************************************
         * @brief Destructor
         * ***********************************************************************************************************
         */
        virtual ~OPTCheckPayload();

        /*! ***********************************************************************************************************
         * @brief setAttributes to store the read values to be included in the reply
         * @param readA value for section A (if = 0 -> section A not read)
         * @param readB value for section B (if = 0 -> section B not read)
         * ***********************************************************************************************************
         */
        void setAttributes(const int readA, const int readB);

        /*! ***********************************************************************************************************
         * @brief composePayload function called to build the payload structure
         * @param pOutDoc valid pointer to the XML Doc
         * @param pRootElem valid pointer to the XML DOM element
         * ***********************************************************************************************************
         */
        bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        int m_intReadA, m_intReadB;

};


#endif // OPTCHECKPAYLOAD_H
