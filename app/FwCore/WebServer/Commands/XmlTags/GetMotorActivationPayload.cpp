/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetMotorActivationPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GetMotorActivationPayload class.
 @details

 ****************************************************************************
*/

#include "GetMotorActivationPayload.h"
#include "WebServerAndProtocolInclude.h"

GetMotorActivationPayload::GetMotorActivationPayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strActivation.clear();
}

GetMotorActivationPayload::~GetMotorActivationPayload()
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strActivation.clear();
}

void GetMotorActivationPayload::setOutValues(std::vector<std::string> strComponent,
											std::vector<std::string> strMotor,
											std::vector<std::string> strActivation)
{
	m_strComponent = strComponent;
	m_strMotor = strMotor;
	m_strActivation = strActivation;
}

bool GetMotorActivationPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	uint8_t counter;

	for(counter = 0; counter < m_strComponent.size(); counter++)
	{
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_ACTIVATION_NAME));
		pElemTmp1->appendChild(pElemTmp2);

		XStr xstrComponentName(XML_ATTR_COMPONENT_NAME);
		XStr xstrComponentValue(m_strComponent.at(counter).c_str());
		pElemTmp2->setAttribute(xstrComponentName.unicodeForm(), xstrComponentValue.unicodeForm());

		XStr xstrMotorName(XML_ATTR_MOTOR_NAME);
		XStr xstrMotorValue(m_strMotor.at(counter).c_str());
		pElemTmp2->setAttribute(xstrMotorName.unicodeForm(), xstrMotorValue.unicodeForm());

		pElemTmp2->setTextContent(X(m_strActivation.at(counter).c_str()));
	}

	return true;
}

