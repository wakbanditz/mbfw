/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FwUpdatePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the FwUpdatePayload class.
 @details

 ****************************************************************************
*/
#ifndef FWUPDATEPAYLOAD_H
#define FWUPDATEPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the FWUPDATE command
 * ********************************************************************************************************************
 */

class FwUpdatePayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		FwUpdatePayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~FwUpdatePayload();

		/*! ***********************************************************************************************************
		 * @brief setOutValues set all the data to be included in the payload
         * @param vUpd list of components
         * @param percentage percentage of progress
		 * ***********************************************************************************************************
		 */
        void setAttributes(vector<string>& vUpd, uint8_t percentage);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:

        vector<string> m_vUpdatedDevices;
        uint8_t m_percentage;

};

#endif // FWUPDATEPAYLOAD_H
