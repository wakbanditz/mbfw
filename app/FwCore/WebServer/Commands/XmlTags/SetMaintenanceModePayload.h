/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SetMaintenanceModePayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SetMaintenanceModePayload class.
 @details

 ****************************************************************************
*/

#ifndef SETMAINTENANCEMODEPAYLOAD_H
#define SETMAINTENANCEMODEPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the SETMAINTENANCEMODE command
 * ********************************************************************************************************************
 */

class SetMaintenanceModePayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief SetMaintenanceModePayload default constructor
		 * ***********************************************************************************************************
		 */
		SetMaintenanceModePayload();

		/*! ***********************************************************************************************************
		 * @brief setActivation	set the Activation string
		 * @param strActivation		std::string that represents the Activation string
		 * ***********************************************************************************************************
		 */
		void setActivation(std::string strActivation);

		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);


	private:

		std::string m_strActivation;
};

#endif // SETMAINTENANCEMODEPAYLOAD_H
