/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GetCalibrationPayload.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GetCalibrationPayload class.
 @details

 ****************************************************************************
*/
#ifndef GETCALIBRATIONPAYLOAD_H
#define GETCALIBRATIONPAYLOAD_H

#include "Payload.h"

/*! *******************************************************************************************************************
 * @brief class	derived from XmlTag -> Payload to contain the info for the asynchronous reply of the GETCALIBRATION command
 * ********************************************************************************************************************
 */

class GetCalibrationPayload : public Payload
{
	public:

		/*! ***********************************************************************************************************
		 * @brief Constructor
		 * ***********************************************************************************************************
		 */
		GetCalibrationPayload();

		/*! ***********************************************************************************************************
		 * @brief Destructor
		 * ***********************************************************************************************************
		 */
		virtual ~GetCalibrationPayload();

		/*! ***********************************************************************************************************
		 * @brief setOutValues to set in the payload object the vectors to be included in the message
		 * @param strComponent the Component vector
		 * @param strMotor the Motor vector
		 * @param strLabel the Calibration label
         * @param strCalibrations the Calibration list
		 * ***********************************************************************************************************
		 */
		void setOutValues(std::vector<std::string> strComponent,
						  std::vector<std::string> strMotor,
						  std::vector<std::string> strLabel,
						  std::vector<std::string> strCalibrations);


		/*! ***********************************************************************************************************
		 * @brief composePayload function called to build the payload structure
		 * @param pOutDoc valid pointer to the XML Doc
		 * @param pRootElem valid pointer to the XML DOM element
		 * ***********************************************************************************************************
		 */
		bool composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem);

    private:
        std::vector<std::string> m_strComponent;
        std::vector<std::string> m_strMotor;
        std::vector<std::string> m_strLabel;
        std::vector<std::string> m_strPosition;

};

#endif // GETCALIBRATIONPAYLOAD_H
