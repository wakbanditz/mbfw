/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FwUpdatePayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the FwUpdatePayload class.
 @details

 ****************************************************************************
*/

#include "FwUpdatePayload.h"
#include "WebServerAndProtocolInclude.h"

FwUpdatePayload::FwUpdatePayload()
{
	m_vUpdatedDevices.clear();
    m_percentage = 0;
}

FwUpdatePayload::~FwUpdatePayload()
{

}

void FwUpdatePayload::setAttributes(vector<string>& vUpd, uint8_t percentage)
{
	m_vUpdatedDevices = vUpd;
    m_percentage = percentage;
}

bool FwUpdatePayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

    if(m_percentage > 0)
    {
        DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PROGRESS));
        pElemTmp1->appendChild(pElemTmp2);

        pElemTmp2->setTextContent(X(to_string(m_percentage).c_str()));
    }

    for(uint8_t i = 0; i < m_vUpdatedDevices.size(); i++)
	{
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_ATTR_COMPONENT_NAME));
		pElemTmp1->appendChild(pElemTmp2);
		pElemTmp2->setTextContent(X(m_vUpdatedDevices.at(i).c_str()));
	}
	return true;
}
