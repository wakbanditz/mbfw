/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SignalReadPayload.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SignalReadPayload class.
 @details

 ****************************************************************************
*/

#include "SignalReadPayload.h"
#include "WebServerAndProtocolInclude.h"

SignalReadPayload::SignalReadPayload()
{
	m_vFluoRead.clear();
	m_vTimeStamps.clear();
	m_vFluoMv.clear();
	m_vRefMv.clear();
}

SignalReadPayload::~SignalReadPayload()
{

}

void SignalReadPayload::setOutParams(vector<string>& vFluoReadStruct, vector<string>& vTimeStamps, vector<string>& vFluoMv, vector<string>& vRefMv)
{
	m_vFluoRead = vFluoReadStruct;
	m_vTimeStamps = vTimeStamps;
	m_vFluoMv = vFluoMv;
	m_vRefMv = vRefMv;
}

bool SignalReadPayload::composePayload(DOMDocument*& pOutDoc, DOMElement* pRootElem)
{
	DOMElement*  pElemTmp1 = pOutDoc->createElement(X(XML_TAG_PAYLOAD_NAME));
	pRootElem->appendChild(pElemTmp1);

	int liCyclesNum = m_vFluoRead.size();

	// TODO.. this is the new version
//	for ( int i = 0; i < liCyclesNum; i++ )
//	{
//		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PROTO_READING));
//		pElemTmp1->appendChild(pElemTmp2);

//		DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_ATTR_ERROR_TIMESTAMP_NAME));
//		pElemTmp2->appendChild(pElemTmp3);
//		pElemTmp3->setTextContent(X(m_vTimeStamps[i].c_str()));

//		DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_PROTO_RFU_NAME));
//		pElemTmp2->appendChild(pElemTmp4);
//		pElemTmp4->setTextContent(X(m_vFluoRead[i].c_str()));

//		DOMElement* pElemTmp5 = pOutDoc->createElement(X(XML_TAG_PROTO_VOLTAGE_NAME));
//		pElemTmp2->appendChild(pElemTmp5);

//		DOMElement* pElemTmp6 = pOutDoc->createElement(X(XML_TAG_PROTO_FLUO_NAME));
//		pElemTmp5->appendChild(pElemTmp6);
//		pElemTmp6->setTextContent(X(m_vFluoMv[i].c_str()));

//		DOMElement* pElemTmp7 = pOutDoc->createElement(X(XML_TAG_PROTO_REF_NAME));
//		pElemTmp5->appendChild(pElemTmp7);
//		pElemTmp7->setTextContent(X(m_vRefMv[i].c_str()));
//	}

	for ( int i = 0; i < liCyclesNum; i++ )
	{
		DOMElement* pElemTmp2 = pOutDoc->createElement(X(XML_TAG_PROTO_READING));
		pElemTmp1->appendChild(pElemTmp2);

		DOMElement* pElemTmp3 = pOutDoc->createElement(X(XML_ATTR_ERROR_TIMESTAMP_NAME));
		pElemTmp2->appendChild(pElemTmp3);
		pElemTmp3->setTextContent(X(m_vTimeStamps[i].c_str()));


		DOMElement* pElemTmp4 = pOutDoc->createElement(X(XML_TAG_PROTO_RFU_NAME));
		pElemTmp2->appendChild(pElemTmp4);

		DOMElement* pElemTmp8 = pOutDoc->createElement(X("R1"));
		pElemTmp4->appendChild(pElemTmp8);
		pElemTmp8->setTextContent(X(m_vFluoRead[i].c_str()));

		DOMElement* pElemTmp9 = pOutDoc->createElement(X("R2"));
		pElemTmp4->appendChild(pElemTmp9);
		pElemTmp9->setTextContent(X(m_vFluoRead[i].c_str()));

		DOMElement* pElemTmp10 = pOutDoc->createElement(X("R3"));
		pElemTmp4->appendChild(pElemTmp10);
		pElemTmp10->setTextContent(X(m_vFluoRead[i].c_str()));

		DOMElement* pElemTmp11 = pOutDoc->createElement(X("Final"));
		pElemTmp4->appendChild(pElemTmp11);
		pElemTmp11->setTextContent(X(m_vFluoRead[i].c_str()));

		DOMElement* pElemTmp5 = pOutDoc->createElement(X(XML_TAG_PROTO_VOLTAGE_NAME));
		pElemTmp2->appendChild(pElemTmp5);

		DOMElement* pElemTmp6 = pOutDoc->createElement(X(XML_TAG_PROTO_FLUO_NAME));
		pElemTmp5->appendChild(pElemTmp6);
		pElemTmp6->setTextContent(X(m_vFluoMv[i].c_str()));

		DOMElement* pElemTmp7 = pOutDoc->createElement(X(XML_TAG_PROTO_REF_NAME));
		pElemTmp5->appendChild(pElemTmp7);
		pElemTmp7->setTextContent(X(m_vRefMv[i].c_str()));
	}

	return true;
}
