/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetSerial.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdGetSerial class.
 @details

 ****************************************************************************
*/

#include "OutCmdGetSerial.h"
#include "WebServerAndProtocolInclude.h"

OutCmdGetSerial::OutCmdGetSerial() : OutgoingCommand(WEB_CMD_NAME_GETSERIAL_OUT)
{

}

OutCmdGetSerial::~OutCmdGetSerial()
{

}
