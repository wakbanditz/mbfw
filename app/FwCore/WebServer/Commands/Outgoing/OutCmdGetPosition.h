/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetPosition.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetPosition class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETPOSITION_H
#define OUTCMDGETPOSITION_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the GETPOSITION command
 * ********************************************************************************************************************
 */
class OutCmdGetPosition : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetPosition	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetPosition();

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetPosition	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetPosition();
};

#endif // OUTCMDGETPOSITION_H
