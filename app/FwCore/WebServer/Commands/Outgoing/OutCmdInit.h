/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdInit.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdInit class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDINIT_H
#define OUTCMDINIT_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the INIT command
 * ********************************************************************************************************************
 */
class OutCmdInit : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdInit	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdInit();

		/*! ***********************************************************************************************************
		 * @brief OutCmdInit	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdInit();
};

#endif // OUTCMDINIT_H
