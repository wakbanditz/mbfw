/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSHCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSHCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSHCALIBRATE_H
#define OUTCMDSHCALIBRATE_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SHCALIBRATION command
 * ********************************************************************************************************************
 */
class OutCmdSHCalibrate : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutSHCalibrate	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSHCalibrate();

		/*! ***********************************************************************************************************
		 * @brief ~OutSHCalibrate	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSHCalibrate();
};

#endif // OUTCMDSHCALIBRATE_H
