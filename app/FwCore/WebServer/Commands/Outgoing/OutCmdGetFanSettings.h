/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetFanSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetFanSettings class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETFANSETTINGS_H
#define OUTCMDGETFANSETTINGS_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the GETFANSETTINGS command
 * ********************************************************************************************************************
 */
class OutCmdGetFanSettings : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdGetFanSettings	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetFanSettings();

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetFanSettings	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetFanSettings();
};

#endif // OUTCMDGETFANSETTINGS_H
