/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdOPTCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdOPTCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDOPTCALIBRATE_H
#define OUTCMDOPTCALIBRATE_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the OPTCALIBRATE command
 * ********************************************************************************************************************
 */
class OutCmdOPTCalibrate : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdOPTCalibrate	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdOPTCalibrate();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdOPTCalibrate	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdOPTCalibrate();
};

#endif // OUTCMDOPTCALIBRATE_H
