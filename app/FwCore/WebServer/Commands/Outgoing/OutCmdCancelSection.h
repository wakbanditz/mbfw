/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdCancelSection.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdCancelSection class.
 @details

 ****************************************************************************
*/


#ifndef OUTCMDCANCELSECTION_H
#define OUTCMDCANCELSECTION_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the CANCELSECTION command
 * ********************************************************************************************************************
 */
class OutCmdCancelSection : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdCancelSection	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdCancelSection();

		/*! ***********************************************************************************************************
		 * @brief OutCmdCancelSection	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdCancelSection();
};


#endif // OUTCMDCANCELSECTION_H
