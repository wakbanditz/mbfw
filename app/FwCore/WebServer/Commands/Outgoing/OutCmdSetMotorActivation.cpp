/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetMotorActivation.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdSetMotorActivation class.
 @details

 ****************************************************************************
*/

#include "OutCmdSetMotorActivation.h"
#include "WebServerAndProtocolInclude.h"

OutCmdSetMotorActivation::OutCmdSetMotorActivation() : OutgoingCommand(WEB_CMD_NAME_SETMOTORACTIVATION_OUT)
{
	/* Nothing to do yet */
}

OutCmdSetMotorActivation::~OutCmdSetMotorActivation()
{
	/* Nothing to do yet */
}
