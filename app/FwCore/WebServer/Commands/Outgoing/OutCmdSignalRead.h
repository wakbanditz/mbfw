/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSignalRead.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSignalRead class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSIGNALREAD_H
#define OUTCMDSIGNALREAD_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SIGNALREAD command
 * ********************************************************************************************************************
 */
class OutCmdSignalRead : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdSignalRead	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSignalRead();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdSignalRead	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSignalRead();
};

#endif // OUTCMDSIGNALREAD_H
