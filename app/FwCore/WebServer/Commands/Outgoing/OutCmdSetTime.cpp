/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetTime.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdSetTime class.
 @details

 ****************************************************************************
*/

#include "OutCmdSetTime.h"
#include "WebServerAndProtocolInclude.h"

OutCmdSetTime::OutCmdSetTime() : OutgoingCommand(WEB_CMD_NAME_SET_TIME_OUT)
{

}

OutCmdSetTime::~OutCmdSetTime()
{

}
