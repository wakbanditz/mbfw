/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetCounter.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSetCounter class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSETCOUNTER_H
#define OUTCMDSETCOUNTER_H


#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SETCOUNTER command
 * ********************************************************************************************************************
 */

class OutCmdSetCounter : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdSetCounter	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSetCounter();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdSetCounter	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSetCounter();

};

#endif // OUTCMDSETCOUNTER_H
