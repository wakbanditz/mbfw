/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdPump.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdPump class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDPUMP_H
#define OUTCMDPUMP_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief class derived from OutgoingCommand to manage the (a)synchronous reply of the PUMP command
 * ********************************************************************************************************************
 */
class OutCmdPump : public OutgoingCommand
{
    public:
        /*! ***********************************************************************************************************
         * @brief OutCmdPump	default constructor
         * ************************************************************************************************************
         */
        OutCmdPump();

        /*! ***********************************************************************************************************
         * @brief OutCmdPump	default destructor
         * ************************************************************************************************************
         */
        virtual ~OutCmdPump();
};

#endif // OUTCMDPUMP_H
