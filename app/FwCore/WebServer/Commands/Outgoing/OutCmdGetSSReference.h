/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetSSReference.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetSSReference class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETSSREFERENCE_H
#define OUTCMDGETSSREFERENCE_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the GETSSREFERENCE command
 * ********************************************************************************************************************
 */
class OutCmdGetSSReference : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetSSReference	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetSSReference();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdGetSSReference	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetSSReference();
};

#endif // OUTCMDGETSSREFERENCE_H
