/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdRunwl.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdRunwl class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDRUNWL_H
#define OUTCMDRUNWL_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the RUN command
 * ********************************************************************************************************************
 */
class OutCmdRunwl : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdRunwl	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdRunwl();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdRunwl	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdRunwl();

};

#endif // OUTCMDRUNWL_H
