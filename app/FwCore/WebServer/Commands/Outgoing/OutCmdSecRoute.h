/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSecRoute.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSecRoute class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSECROUTE_H
#define OUTCMDSECROUTE_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the ROUTE command
 * ********************************************************************************************************************
 */
class OutCmdSecRoute : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdSecRoute	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSecRoute();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdSecRoute	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSecRoute();
};

#endif // OUTCMDSECROUTE_H
