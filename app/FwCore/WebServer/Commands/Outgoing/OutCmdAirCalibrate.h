/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdAirCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdAirCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDAIRCALIBRATE_H
#define OUTCMDAIRCALIBRATE_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the AIRCALIBRATE command
 * ********************************************************************************************************************
 */
class OutCmdAirCalibrate : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdAirCalibrate	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdAirCalibrate();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdAirCalibrate	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdAirCalibrate();
};

#endif // OUTCMDAIRCALIBRATE_H
