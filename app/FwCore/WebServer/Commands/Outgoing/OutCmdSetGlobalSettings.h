/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetGlobalSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSetGlobalSettings class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGLOBALSETTINGS_H
#define OUTCMDGLOBALSETTINGS_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SETGLOBALSETTINGS command
 * ********************************************************************************************************************
 */
class OutCmdSetGlobalSettings : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdGlobalSettings	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSetGlobalSettings();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdGlobalSettings	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSetGlobalSettings();
};


#endif // OUTCMDGLOBALSETTINGS_H
