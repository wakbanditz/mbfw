/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetParameter.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSetParameter class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSETPARAMETER_H
#define OUTCMDSETPARAMETER_H


#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SETPARAMETER command
 * ********************************************************************************************************************
 */
class OutCmdSetParameter : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdSetParameter	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSetParameter();

		/*! ***********************************************************************************************************
		 * @brief OutCmdSetParameter	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSetParameter();
};

#endif // OUTCMDSETPARAMETER_H
