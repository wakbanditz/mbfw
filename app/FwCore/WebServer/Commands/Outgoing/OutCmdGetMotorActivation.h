/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetMotorActivation.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetMotorActivation class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETMOTORACTIVATION_H
#define OUTCMDGETMOTORACTIVATION_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the GETMOTORACTIVATION command
 * ********************************************************************************************************************
 */
class OutCmdGetMotorActivation : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdGetMotorActivation	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetMotorActivation();

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetMotorActivation	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetMotorActivation();
};


#endif // OUTCMDGETMOTORACTIVATION_H
