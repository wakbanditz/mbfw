/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetTime.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSetTime class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSETTIME_H
#define OUTCMDSETTIME_H


#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SETTIME command
 * ********************************************************************************************************************
 */

class OutCmdSetTime : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdSetTime	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSetTime();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdSetTime	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSetTime();

};


#endif // OUTCMDSETTIME_H
