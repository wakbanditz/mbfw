/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSleep.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdSleep class.
 @details

 ****************************************************************************
*/

#include "OutCmdSleep.h"
#include "WebServerAndProtocolInclude.h"

OutCmdSleep::OutCmdSleep() : OutgoingCommand(WEB_CMD_NAME_SLEEP_OUT)
{
}

OutCmdSleep::~OutCmdSleep()
{}


