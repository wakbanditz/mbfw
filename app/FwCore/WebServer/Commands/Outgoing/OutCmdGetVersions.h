/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetVersions.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetVersions class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETVERSIONS_H
#define OUTCMDGETVERSIONS_H


#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the GETVERSIONS command
 * ********************************************************************************************************************
 */

class OutCmdGetVersions : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetVersions	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetVersions();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdGetVersions	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetVersions();

};

#endif // OUTCMDGETVERSIONS_H
