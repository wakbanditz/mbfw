/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSaveCameraRoi.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSaveCameraRoi class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSAVECAMERAROI_H
#define OUTCMDSAVECAMERAROI_H

#include "OutgoingCommand.h"


/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SAVECAMERAROI command
 * ********************************************************************************************************************
 */
class OutCmdSaveCameraRoi : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdSaveCameraRoi	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSaveCameraRoi();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdSaveCameraRoi	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSaveCameraRoi();
};

#endif // OUTCMDSAVECAMERAROI_H
