/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdDisable.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdDisable class.
 @details

 ****************************************************************************
*/

#include "OutCmdDisable.h"
#include "WebServerAndProtocolInclude.h"

OutCmdDisable::OutCmdDisable() : OutgoingCommand(WEB_CMD_NAME_DISABLE_OUT)
{
}

OutCmdDisable::~OutCmdDisable()
{}


