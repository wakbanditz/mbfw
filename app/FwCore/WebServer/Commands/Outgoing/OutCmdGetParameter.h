/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetParameter.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetPArameter class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETPARAMETER_H
#define OUTCMDGETPARAMETER_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief class derived from OutgoingCommand to manage the (a)synchronous reply of the GETPARAMETER command
 * ********************************************************************************************************************
 */
class OutCmdGetParameter : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetParameter	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetParameter();

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetParameter	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetParameter();
};

#endif // OUTCMDGETPARAMETER_H
