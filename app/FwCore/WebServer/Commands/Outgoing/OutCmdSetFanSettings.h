/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetFanSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSetFanSettings class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSETFANSETTINGS_H
#define OUTCMDSETFANSETTINGS_H


#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SETFANSETTINGS command
 * ********************************************************************************************************************
 */
class OutCmdSetFanSettings : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdSetFanSettings	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSetFanSettings();

		/*! ***********************************************************************************************************
		 * @brief OutCmdSetFanSettings	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSetFanSettings();
};

#endif // OUTCMDSETFANSETTINGS_H
