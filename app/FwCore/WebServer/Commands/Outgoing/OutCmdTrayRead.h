/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdTrayRead.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdTrayRead class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDTRAYREAD_H
#define OUTCMDTRAYREAD_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the TRAYREAD command
 * ********************************************************************************************************************
 */
class OutCmdTrayRead : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdTrayRead	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdTrayRead();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdTrayRead	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdTrayRead();
};

#endif // OUTCMDTRAYREAD_H
