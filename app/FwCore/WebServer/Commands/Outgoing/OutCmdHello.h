/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdHello.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdHello class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDHELLO_H
#define OUTCMDHELLO_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the HELLO command
 * ********************************************************************************************************************
 */
class OutCmdHello : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdHello	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdHello();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdHello	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdHello();
};

#endif // OUTCMDHELLO_H
