/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdUnknown.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdUnknown class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDUNKNOWN_H
#define OUTCMDUNKNOWN_H


#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the UNKNOWN command
 * ********************************************************************************************************************
 */
class OutCmdUnknown : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdUnknown	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdUnknown();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdUnknown	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdUnknown();

		/*! ***********************************************************************************************************
		 * @brief to set the command string not recognized
		 * @param strCmd the command string
		 * ************************************************************************************************************
		 */
		void setCommandString(std::string strCmd);

	private :

		std::string m_strCmd;
};

#endif // OUTCMDUNKNOWN_H
