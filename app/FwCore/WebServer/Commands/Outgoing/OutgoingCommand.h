/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutgoingCommand.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutgoingCommand class.
 @details

 ****************************************************************************
*/

#ifndef OUTGOINGCOMMAND_H
#define OUTGOINGCOMMAND_H

#include <string>

class WebServerConnection;
class FailureCause;
class Payload;

/*! *******************************************************************************************************************
 * @brief The OutgoingCommand class represent a command to send to a client. Should be derived and filled with
 *									proper internal members. This object, once correctly built, can be serialized using
 *									the ProtocolValidator::serialize() method
 * ********************************************************************************************************************
 */
class OutgoingCommand
{
	private:

		std::string m_strName;
		std::string m_strStatus;
		std::string m_strUsage;
		std::string m_strID;
		FailureCause* m_pFailureCause;
		Payload* m_pPayload;

	public:

		/*! ***********************************************************************************************************
		 * @brief OutgoingCommand	default constructor
		 * @param strName			the command name as recognized by the ProtocolValidator
		 * ************************************************************************************************************
		 */
		OutgoingCommand(std::string strName);

		/*! ***********************************************************************************************************
		 * @brief ~OutgoingCommand	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutgoingCommand();

		/*! ***********************************************************************************************************
		 * @brief reset				void all internal members and deallocates resources
		 * ***********************************************************************************************************
		 */
		void reset(void);

		/*! ***********************************************************************************************************
		 * @brief getName			retrieves the command name
		 * @return					the command name as a std::string
		 * ************************************************************************************************************
		 */
		std::string getName(void);

		/*! ***********************************************************************************************************
		 * @brief setName			set the command name
		 * @param strName			std::string that represents the command name
		 * ***********************************************************************************************************
		 */
		void setName(const std::string& strName);

		/*! ***********************************************************************************************************
		 * @brief getStatus			retrieves status of the command
		 * @return					the command status as a std::string
		 * ***********************************************************************************************************
		 */
		std::string getStatus() const;

		/*! ***********************************************************************************************************
		 * @brief setUsage			set the Usage connection
		 * @param strUsage			std::string that represents the Usage connection (COMMERCIAL, SERVICE)
		 * ***********************************************************************************************************
		 */
		void setUsage(const std::string& strUsage);

		/*! ***********************************************************************************************************
		 * @brief getUsage			retrieves the Usage connection
		 * @return					the Usage connection as a std::string
		 * ***********************************************************************************************************
		 */
		std::string getUsage() const;

		/*! ***********************************************************************************************************
		 * @brief setID			set the CommandId
		 * @param strId			std::string that represents the CommandId
		 * ***********************************************************************************************************
		 */
		void setID(const std::string& strId);

		/*! ***********************************************************************************************************
		 * @brief getID			retrieves the CommandId
		 * @return				the CommandId as a std::string
		 * ***********************************************************************************************************
		 */
		std::string getID() const;

		/*! ***********************************************************************************************************
		 * @brief setStatus			set the command status
		 * @param strStatus			std::string that represents the command status (ACCEPTED, REJECTED, SUCCESFULL...)
		 * ***********************************************************************************************************
		 */
		void setStatus(const std::string& strStatus);

		/*! ***********************************************************************************************************
		 * @brief getFailureCause	retrieves the FailureCause of this command, if there is any
		 * @return					pointer to the internal FailureCause object, can be 0
		 * ***********************************************************************************************************
		 */
		FailureCause* getFailureCause() const;

		/*! ***********************************************************************************************************
		 * @brief setFailureCause	set the FailureCause of this command
		 * @param pFailureCause		pointer to be assigned to the internal FailureCause object
		 * ***********************************************************************************************************
		 */
		void setFailureCause(FailureCause* pFailureCause);

		/*! ***********************************************************************************************************
		 * @brief getPayload		retrieves the Payload of this command, if there is any
		 * @return					pointer to the internal Payload object, can be 0
		 * ***********************************************************************************************************
		 */
		Payload* getPayload() const;

		/*! ***********************************************************************************************************
		 * @brief setPayload		set the Payload of this command
		 * @param pPayload			pointer to be assigned to the internal Payload object
		 * ***********************************************************************************************************
		 */
		void setPayload(Payload* pPayload);

};

#endif // OUTGOINGCOMMAND_H
