/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetMaintenanceMode.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSetMainenanceMode class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSETMAINTENANCEMODE_H
#define OUTCMDSETMAINTENANCEMODE_H


#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SETMAINTENANCEMODE command
 * ********************************************************************************************************************
 */

class OutCmdSetMaintenanceMode : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdSetMaintenanceMode	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSetMaintenanceMode();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdSetMaintenanceMode	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSetMaintenanceMode();

};

#endif // OUTCMDSETMAINTENANCEMODE_H
