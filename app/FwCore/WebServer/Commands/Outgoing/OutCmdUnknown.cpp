/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdUnknown.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdUnknown class.
 @details

 ****************************************************************************
*/

#include "OutCmdUnknown.h"
#include "WebServerAndProtocolInclude.h"

OutCmdUnknown::OutCmdUnknown() : OutgoingCommand(WEB_CMD_NAME_UNKNOWN)
{

}

OutCmdUnknown::~OutCmdUnknown()
{

}

void OutCmdUnknown::setCommandString(std::__cxx11::string strCmd)
{
	// change the command name
	m_strCmd.assign(strCmd);
	setName(m_strCmd);
}
