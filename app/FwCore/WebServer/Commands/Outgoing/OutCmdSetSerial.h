/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetSerial.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSetSerial class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSETSERIAL_H
#define OUTCMDSETSERIAL_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SETSERIAL command
 * ********************************************************************************************************************
 */

class OutCmdSetSerial : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdSetSerial	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSetSerial();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdSetSerial	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSetSerial();

};


#endif // OUTCMDSETSERIAL_H
