/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdReadSec.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdReadSec class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDREADSEC_H
#define OUTCMDREADSEC_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the READSEC command
 * ********************************************************************************************************************
 */
class OutCmdReadSec : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdReadSec	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdReadSec();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdReadSec	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdReadSec();
};

#endif // OUTCMDREADSEC_H
