/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetSensor.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetSensor class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETSENSOR_H
#define OUTCMDGETSENSOR_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the GETSENSOR command
 * ********************************************************************************************************************
 */
class OutCmdGetSensor : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetSensor	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetSensor();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdGetSensor	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetSensor();
};

#endif // OUTCMDGETSENSOR_H
