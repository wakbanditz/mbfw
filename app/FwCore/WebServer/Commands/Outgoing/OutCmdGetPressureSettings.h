/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetPressureSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetPressureSettings class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETPRESSURESETTINGS_H
#define OUTCMDGETPRESSURESETTINGS_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the GETPRESSURESETTINGS command
 * ********************************************************************************************************************
 */
class OutCmdGetPressureSettings : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdGetPressureSettings	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetPressureSettings();

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetPressureSettings	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetPressureSettings();
};

#endif // OUTCMDGETPRESSURESETTINGS_H
