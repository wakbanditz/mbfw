/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSHForceLed.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSHForceLed class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSHFORCELED_H
#define OUTCMDSHFORCELED_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SHFORCELED command
 * ********************************************************************************************************************
 */
class OutCmdSHForceLed : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdSHForceLed	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSHForceLed();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdSHForceLed	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSHForceLed();
};

#endif // OUTCMDSHFORCELED_H
