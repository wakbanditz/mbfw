/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdLogin.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdLogin class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDLOGIN_H
#define OUTCMDLOGIN_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the LOGIN command
 * ********************************************************************************************************************
 */
class OutCmdLogin : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdLogin	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdLogin();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdLogin	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdLogin();

};

#endif // OUTCMDLOGIN_H
