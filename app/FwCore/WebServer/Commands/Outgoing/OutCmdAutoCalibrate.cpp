/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdAutoCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdAutoCalibrate class.
 @details

 ****************************************************************************
*/

#include "OutCmdAutoCalibrate.h"
#include "WebServerAndProtocolInclude.h"

OutCmdAutoCalibrate::OutCmdAutoCalibrate() : OutgoingCommand(WEB_CMD_NAME_AUTO_CALIBRATE_OUT)
{
	/* Nothing to do yet */
}

OutCmdAutoCalibrate::~OutCmdAutoCalibrate()
{
	/* Nothing to do yet */
}
