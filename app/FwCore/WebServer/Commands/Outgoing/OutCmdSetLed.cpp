/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetLed.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdSetLed class.
 @details

 ****************************************************************************
*/

#include "OutCmdSetLed.h"
#include "WebServerAndProtocolInclude.h"

OutCmdSetLed::OutCmdSetLed() : OutgoingCommand(WEB_CMD_NAME_SETLED_OUT)
{

}

OutCmdSetLed::~OutCmdSetLed()
{

}
