/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdOPTCheck.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdOPTCheck class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDOPTCHECK_H
#define OUTCMDOPTCHECK_H


#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the OPT_CHECK command
 * ********************************************************************************************************************
 */
class OutCmdOPTCheck : public OutgoingCommand
{
    public:

        /*! ***********************************************************************************************************
         * @brief OutCmdOPTCheck	default constructor
         * ************************************************************************************************************
         */
        OutCmdOPTCheck();

        /*! ***********************************************************************************************************
         * @brief ~OutCmdOPTCheck	default destructor
         * ************************************************************************************************************
         */
        virtual ~OutCmdOPTCheck();
};

#endif // OUTCMDOPTCHECK_H
