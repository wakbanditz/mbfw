/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetLed.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSetLed class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSETLED_H
#define OUTCMDSETLED_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SETLED command
 * ********************************************************************************************************************
 */

class OutCmdSetLed : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdSetLed	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSetLed();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdSetLed	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSetLed();

};


#endif // OUTCMDSETLED_H
