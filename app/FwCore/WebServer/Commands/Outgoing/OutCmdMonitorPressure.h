/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdMonitorPressure.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdMonitorPressure class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDMONITORPRESSURE_H
#define OUTCMDMONITORPRESSURE_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the MONITOR_PRESSURE command
 * ********************************************************************************************************************
 */
class OutCmdMonitorPressure : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdMonitorPressure	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdMonitorPressure();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdMonitorPressure	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdMonitorPressure();
};


#endif // OUTCMDMONITORPRESSURE_H
