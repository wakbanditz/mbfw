/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetGlobalSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetGlobalSettings class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETGLOBALSETTINGS_H
#define OUTCMDGETGLOBALSETTINGS_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the reply of the GET_GLOBAL_SETTINGS command
 * ********************************************************************************************************************
 */

class OutCmdGetGlobalSettings : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetGlobalSettings	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetGlobalSettings();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdGetGlobalSettings	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetGlobalSettings();

};


#endif // OUTCMDGETGLOBALSETTINGS_H
