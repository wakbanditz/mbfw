/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdLogout.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdLogout class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDLOGOUT_H
#define OUTCMDLOGOUT_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the LOGOUT command
 * ********************************************************************************************************************
 */
class OutCmdLogout : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdLogout	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdLogout();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdLogout	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdLogout();

};

#endif // OUTCMDLOGOUT_H
