/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetParameter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdSetParameter class.
 @details

 ****************************************************************************
*/

#include "OutCmdSetParameter.h"
#include "WebServerAndProtocolInclude.h"

OutCmdSetParameter::OutCmdSetParameter() : OutgoingCommand(WEB_CMD_NAME_SETPARAMETER_OUT)
{
	/* Nothing to do yet */
}

OutCmdSetParameter::~OutCmdSetParameter()
{
	/* Nothing to do yet */
}
