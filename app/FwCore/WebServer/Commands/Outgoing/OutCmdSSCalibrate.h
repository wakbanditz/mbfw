/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSSCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSSCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSSCALIBRATE_H
#define OUTCMDSSCALIBRATE_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SSCALIBRATE command
 * ********************************************************************************************************************
 */
class OutCmdSSCalibrate : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdSSCalibrate	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSSCalibrate();

		/*! ***********************************************************************************************************
		 * @brief OutCmdSSCalibrate	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSSCalibrate();
};

#endif // OUTCMDSSCALIBRATE_H
