/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdVidasEp.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdVidasEp class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDVIDASEP_H
#define OUTCMDVIDASEP_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the VIDASEP command
 * ********************************************************************************************************************
 */

class OutCmdVidasEp : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdVidasEp	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdVidasEp();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdVidasEp	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdVidasEp();

};

#endif // OUTCMDVIDASEP_H
