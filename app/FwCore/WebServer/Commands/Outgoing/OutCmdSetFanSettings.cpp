/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetFanSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdSetFanSettings class.
 @details

 ****************************************************************************
*/

#include "OutCmdSetFanSettings.h"
#include "WebServerAndProtocolInclude.h"

OutCmdSetFanSettings::OutCmdSetFanSettings() : OutgoingCommand(WEB_CMD_NAME_SET_FAN_SETTINGS_OUT)
{
	/* Nothing to do yet */
}

OutCmdSetFanSettings::~OutCmdSetFanSettings()
{
	/* Nothing to do yet */
}
