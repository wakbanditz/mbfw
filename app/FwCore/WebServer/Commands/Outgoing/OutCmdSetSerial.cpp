/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetSerial.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdSetSerial class.
 @details

 ****************************************************************************
*/

#include "OutCmdSetSerial.h"
#include "WebServerAndProtocolInclude.h"

OutCmdSetSerial::OutCmdSetSerial() : OutgoingCommand(WEB_CMD_NAME_SETSERIAL_OUT)
{

}

OutCmdSetSerial::~OutCmdSetSerial()
{

}

