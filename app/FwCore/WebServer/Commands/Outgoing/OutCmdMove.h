/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdMove.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdMove class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDMOVE_H
#define OUTCMDMOVE_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief class derived from OutgoingCommand to manage the (a)synchronous reply of the MOVE command
 * ********************************************************************************************************************
 */
class OutCmdMove : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdMove	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdMove();

		/*! ***********************************************************************************************************
		 * @brief OutCmdMove	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdMove();
};

#endif // OUTCMDMOVE_H
