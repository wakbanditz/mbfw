/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetCounter.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetCounter class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETCOUNTER_H
#define OUTCMDGETCOUNTER_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the GETCOUNTER command
 * ********************************************************************************************************************
 */

class OutCmdGetCounter : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetCounter	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetCounter();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdGetCounter	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetCounter();

};

#endif // OUTCMDGETCOUNTER_H
