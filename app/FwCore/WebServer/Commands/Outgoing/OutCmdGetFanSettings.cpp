/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetFanSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdGetFanSettings class.
 @details

 ****************************************************************************
*/

#include "OutCmdGetFanSettings.h"
#include "WebServerAndProtocolInclude.h"

OutCmdGetFanSettings::OutCmdGetFanSettings() : OutgoingCommand(WEB_CMD_NAME_GET_FAN_SETTINGS_OUT)
{
	/* Nothing to do yet */
}

OutCmdGetFanSettings::~OutCmdGetFanSettings()
{
	/* Nothing to do yet */
}
