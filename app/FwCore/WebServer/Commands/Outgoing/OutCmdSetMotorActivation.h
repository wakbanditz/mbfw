/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSetMotorActivation.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSetMotorActivation class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSETMOTORACTIVATION_H
#define OUTCMDSETMOTORACTIVATION_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SETMOTORACTIVATION command
 * ********************************************************************************************************************
 */
class OutCmdSetMotorActivation : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdSetMotorActivation	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSetMotorActivation();

		/*! ***********************************************************************************************************
		 * @brief OutCmdSetMotorActivation	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSetMotorActivation();
};

#endif // OUTCMDSETMOTORACTIVATION_H
