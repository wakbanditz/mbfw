/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdSleep.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdSleep class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSLEEP_H
#define OUTCMDSLEEP_H


#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SLEEP command
 * ********************************************************************************************************************
 */
class OutCmdSleep : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdSleep	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdSleep();

		/*! ***********************************************************************************************************
		 * @brief OutCmdSleep	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdSleep();
};

#endif // OUTCMDSLEEP_H
