/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutgoingCommand.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutgoingCommand class.
 @details

 ****************************************************************************
*/

#include "OutgoingCommand.h"
#include "CommonInclude.h"
#include "FailureCause.h"
#include "Payload.h"

OutgoingCommand::OutgoingCommand(std::string strName) : m_strName(strName)
{
	m_strStatus.clear();
	m_strUsage.clear();
	m_strID.clear();
	m_pFailureCause = 0;
	m_pPayload = 0;
}

OutgoingCommand::~OutgoingCommand()
{
    reset();
}

void OutgoingCommand::reset(void)
{
	m_strStatus.clear();
	SAFE_DELETE(m_pFailureCause);
	SAFE_DELETE(m_pPayload);
}

std::string OutgoingCommand::getName(void)
{
	return m_strName;
}

void OutgoingCommand::setName(const std::string& strName)
{
	m_strName.assign(strName);
}

std::string OutgoingCommand::getStatus() const
{
	return m_strStatus;
}

void OutgoingCommand::setUsage(const string& strUsage)
{
	m_strUsage.assign(strUsage);
}

string OutgoingCommand::getUsage() const
{
	return m_strUsage;
}

void OutgoingCommand::setID(const string& strId)
{
	m_strID.assign(strId);
}

string OutgoingCommand::getID() const
{
	return m_strID;
}

void OutgoingCommand::setStatus(const std::string& strStatus)
{
	m_strStatus.assign(strStatus);
}

FailureCause* OutgoingCommand::getFailureCause() const
{
	return m_pFailureCause;
}

void OutgoingCommand::setFailureCause(FailureCause* pFailureCause)
{
	m_pFailureCause = pFailureCause;
}

Payload* OutgoingCommand::getPayload() const
{
	return m_pPayload;
}

void OutgoingCommand::setPayload(Payload* pPayload)
{
	m_pPayload = pPayload;
}
