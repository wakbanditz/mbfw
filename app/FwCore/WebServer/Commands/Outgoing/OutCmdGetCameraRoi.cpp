/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetCameraRoi.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the OutCmdGetCameraRoi class.
 @details

 ****************************************************************************
*/

#include "OutCmdGetCameraRoi.h"
#include "WebServerAndProtocolInclude.h"

OutCmdGetCameraRoi::OutCmdGetCameraRoi() : OutgoingCommand(WEB_CMD_NAME_GET_CAMERA_ROI_OUT)
{

}

OutCmdGetCameraRoi::~OutCmdGetCameraRoi()
{

}
