/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdShutdown.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdShutdown class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDSHUTDOWN_H
#define OUTCMDSHUTDOWN_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the SHUTDOWN command
 * ********************************************************************************************************************
 */
class OutCmdShutdown : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdShutdown	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdShutdown();

		/*! ***********************************************************************************************************
		 * @brief OutCmdShutdown	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdShutdown();
};


#endif // OUTCMDSHUTDOWN_H
