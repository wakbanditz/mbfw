/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdDisable.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdDisable class.
 @details

 ****************************************************************************
*/


#ifndef OUTCMDDISABLE_H
#define OUTCMDDISABLE_H


#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the DISABLE command
 * ********************************************************************************************************************
 */
class OutCmdDisable : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdDisable	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdDisable();

		/*! ***********************************************************************************************************
		 * @brief OutCmdDisable	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdDisable();
};



#endif // OUTCMDDISABLE_H
