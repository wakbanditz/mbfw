/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdCaptureImage.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdCaptureImage class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDCAPTUREIMAGE_H
#define OUTCMDCAPTUREIMAGE_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the CAPTUREIMAGE command
 * ********************************************************************************************************************
 */
class OutCmdCaptureImage : public OutgoingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetPicture	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdCaptureImage();

		/*! ***********************************************************************************************************
		 * @brief ~OutCmdGetPicture	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdCaptureImage();
};

#endif // OUTCMDCAPTUREIMAGE_H
