/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetPressureOffsets.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetPressureOffsets class.
 @details

 ****************************************************************************
*/

#ifndef OUTCMDGETPRESSUREOFFSETS_H
#define OUTCMDGETPRESSUREOFFSETS_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the GETPRESSUREOFFSETS command
 * ********************************************************************************************************************
 */
class OutCmdGetPressureOffsets : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdGetPressureOffsets	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetPressureOffsets();

		/*! ***********************************************************************************************************
		 * @brief OutCmdGetPressureOffsets	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetPressureOffsets();
};

#endif // OUTCMDGETPRESSUREOFFSETS_H
