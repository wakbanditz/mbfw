/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    OutCmdGetCalibration.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the OutCmdGetCalibration class.
 @details

 ****************************************************************************
*/


#ifndef OUTCMDGETCALIBRATION_H
#define OUTCMDGETCALIBRATION_H

#include "OutgoingCommand.h"

/*! *******************************************************************************************************************
 * @brief classe derived from OutgoingCommand to manage the (a)synchronous reply of the GETCALIBRATION command
 * ********************************************************************************************************************
 */
class OutCmdGetCalibration : public OutgoingCommand
{
	public:
		/*! ***********************************************************************************************************
		 * @brief OutCmdGetCalibration	default constructor
		 * ************************************************************************************************************
		 */
		OutCmdGetCalibration();

		/*! ***********************************************************************************************************
		 * @brief OutCmdMove	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~OutCmdGetCalibration();
};

#endif // OUTCMDGETCALIBRATION_H
