/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSecRoute.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSecRoute class.
 @details

 ****************************************************************************
*/

#include "InCmdSecRoute.h"
#include "WFSecRoute.h"
#include "WebServerAndProtocolInclude.h"

InCmdSecRoute::InCmdSecRoute(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_ROUTE_IN, strUsage, strID)
{
	m_strCommand.clear();
	m_sectionRead = -1;
}

InCmdSecRoute::~InCmdSecRoute()
{

}

Workflow* InCmdSecRoute::getWorkflow(void)
{
	WFSecRoute* pWf = new WFSecRoute();
	return pWf;
}

bool InCmdSecRoute::setComponent(std::string strSection)
{
	if(strcmp(strSection.c_str(), XML_TAG_SECTIONA_NAME) == 0)
	{
		m_sectionRead = SCT_A_ID;
	}
	else if(strcmp(strSection.c_str(), XML_TAG_SECTIONB_NAME) == 0)
	{
		m_sectionRead = SCT_B_ID;
	}
	else if(strcmp(strSection.c_str(), XML_TAG_NSH_NAME) == 0)
	{
		m_sectionRead = NSH_ID;
	}
	else if(strcmp(strSection.c_str(), XML_TAG_CAMERA_NAME) == 0)
	{
		m_sectionRead = CAMERA_ID;
	}
	else if (strcmp(strSection.c_str(), XML_TAG_MASTERBOARD_NAME) == 0)
	{
		m_sectionRead = INSTRUMENT_ID;
	}
	return (m_sectionRead != -1);
}

int InCmdSecRoute::getSection()
{
	return m_sectionRead;
}

void InCmdSecRoute::setCommand(std::string strCommand)
{
	m_strCommand.assign(strCommand);
}

std::string InCmdSecRoute::getCommand()
{
	return m_strCommand;
}

