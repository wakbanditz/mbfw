/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetCameraRoi.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetCameraRoi class.
 @details

 ****************************************************************************
*/

#include "InCmdGetCameraRoi.h"
#include "WFGetCameraRoi.h"
#include "WebServerAndProtocolInclude.h"

InCmdGetCameraRoi::InCmdGetCameraRoi(string strUsage, string strID) :
				   IncomingCommand(WEB_CMD_NAME_GET_CAMERA_ROI_IN, strUsage, strID)
{
	m_vTargets.clear();
}

InCmdGetCameraRoi::~InCmdGetCameraRoi()
{

}

Workflow* InCmdGetCameraRoi::getWorkflow()
{
	WFGetCameraRoi* pWf = new WFGetCameraRoi();
	return pWf;
}

void InCmdGetCameraRoi::setCurrentTarget(string strTarget)
{
	m_vTargets.push_back(strTarget);
}

vector<string>& InCmdGetCameraRoi::getTargets()
{
	return m_vTargets;
}

