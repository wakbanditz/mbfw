/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSHForceLed.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSHForceLed class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSHFORCELED_H
#define INCMDSHFORCELED_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SHFORCELED command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSHForceLed : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdSHForceLed contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSHForceLed(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSHForceLed destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSHForceLed();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the Force Led command e retrieves the pointer
         * @return Pointer to WFSHForceLed object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
         * @brief setDuration set the time duration
         * @param strDuration
		 * ************************************************************************************************************
		 */
		void setDuration(string& strDuration);

		/*! ***********************************************************************************************************
         * @brief getDuration get the time duration
         * @return the time duration
		 * ************************************************************************************************************
		 */
		string getDuration(void);

    private:

        string m_strDuration;

};

#endif // INCMDSHFORCELED_H
