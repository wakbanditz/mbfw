/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetTime.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSetTime class.
 @details

 ****************************************************************************
*/

#include "InCmdSetTime.h"
#include "WFSetTime.h"
#include "WebServerAndProtocolInclude.h"

InCmdSetTime::InCmdSetTime(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_SET_TIME_IN, strUsage, strID)
{
	m_strTime.clear();
}

InCmdSetTime::~InCmdSetTime()
{

}

Workflow* InCmdSetTime::getWorkflow(void)
{
	WFSetTime* pWf = new WFSetTime();
	return pWf;
}


string InCmdSetTime::getTimeString() const
{
	return m_strTime;
}

void InCmdSetTime::setTimeString(const string& strTime)
{
	m_strTime.assign(strTime);
}
