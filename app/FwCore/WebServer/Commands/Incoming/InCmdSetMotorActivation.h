/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetMotorActivation.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSetMotorActivation class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSETMOTORACTIVATION_H
#define INCMDSETMOTORACTIVATION_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SETMOTORACTIVATION command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSetMotorActivation : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdSetMotorActivation contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSetMotorActivation(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSetMotorActivation destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSetMotorActivation();

		/*! ************************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the SetMotorActivation command e retrieves the pointer
         * @return Pointer to WFSetMotoActivation object
		 * *************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief setComponent	Sets the name of the component to take in considerations for the cmd
		 * @param strComponent
		 * *************************************************************************************************
		 */
		void setComponent( const string &strComponent );

		/*! *************************************************************************************************
		 * @brief getSection	Retrieves the name of the set section
		 * @return m_strSection
		 * *************************************************************************************************
		 */
		string getComponent( void );

		/*! *************************************************************************************************
		 * @brief setMotor	Sets the name of the motor to take in considerations for the cmd
		 * @param strMotor
		 * *************************************************************************************************
		 */
		void setMotor( const string &strMotor );

		/*! *************************************************************************************************
		 * @brief getMotor	Retrieves the name of the set motor
		 * @return m_strMotor
		 * *************************************************************************************************
		 */
		string getMotor( void );

		/*! *************************************************************************************************
         * @brief setActivation	Sets the name of the activation to take in considerations for the cmd
         * @param strActivation
		 * *************************************************************************************************
		 */
		void setActivation( const string &strActivation );

		/*! *************************************************************************************************
         * @brief getActivation	Retrieves the activation status of the set motor
		 * @return m_strActivation
		 * *************************************************************************************************
		 */
		string getActivation( void );

    private:

        string m_strComponent;
        string m_strMotor;
        string m_strActivation;

};

#endif // INCMDSETMOTORACTIVATION_H
