/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdTrayRead.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdTrayRead class.
 @details

 ****************************************************************************
*/
#ifndef INCMDTRAYREAD_H

#define INCMDTRAYREAD_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the TRAYREAD command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdTrayRead : public IncomingCommand
{
	public:

		InCmdTrayRead(string strUsage, string strID);

		virtual ~InCmdTrayRead();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the CancelSection command e retrieves the pointer
		 * @return Pointer to WFGetCalibration object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief setSection set the Section read from the incoming command
		 * @param strSection the section string (A or B)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSection(std::string strSection);

		/*! *************************************************************************************************
		 * @brief getSection get the Section read from the incoming command
		 * @return the section index
		 * **************************************************************************************************
		 */
		uint8_t getSection();

    private:

        uint8_t m_section;

};

#endif // INCMDTRAYREAD_H
