/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdHello.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdHello class.
 @details

 ****************************************************************************
*/

#ifndef INCMDHELLO_H
#define INCMDHELLO_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the HELLO command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdHello : public IncomingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdHello constructor
		 * ************************************************************************************************************
		 */
		InCmdHello();

		/*! ***********************************************************************************************************
		 * @brief ~InCmdHello	destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdHello();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);
};

#endif // INCMDHELLO_H
