/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetMotorActivation.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetMotorActivation class.
 @details

 ****************************************************************************
*/

#include "InCmdGetMotorActivation.h"
#include "WFGetMotorActivation.h"
#include "WebServerAndProtocolInclude.h"

InCmdGetMotorActivation::InCmdGetMotorActivation(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_GETMOTORACTIVATION_IN, strUsage, strID)
{
	m_strComponent.clear();
	m_strMotor.clear();
}

InCmdGetMotorActivation::~InCmdGetMotorActivation()
{
	/* Nothing to do yet */
}

Workflow* InCmdGetMotorActivation::getWorkflow(void)
{
	WFGetMotorActivation* pWf = new WFGetMotorActivation();

	return pWf;
}

void InCmdGetMotorActivation::setComponent(const string &strComponent)
{
	m_strComponent.assign(strComponent);
}

string InCmdGetMotorActivation::getComponent( void )
{
	return m_strComponent;
}

void InCmdGetMotorActivation::setMotor(const string &strMotor)
{
	m_strMotor.assign(strMotor);
}

string InCmdGetMotorActivation::getMotor( void )
{
	return m_strMotor;
}

