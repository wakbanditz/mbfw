/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSaveCalibration.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSaveCalibration class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSAVECALIBRATION_H
#define INCMDSAVECALIBRATION_H

#include "vector"

#include "SectionInclude.h"

#include "IncomingCommand.h"


class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SAVECALIBRATION command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSaveCalibration : public IncomingCommand
{

	public:

		/*! *************************************************************************************************
		 * @brief InCmdSaveCalibration contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSaveCalibration(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSaveCalibration destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSaveCalibration();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the SaveCalibration command e retrieves the pointer
         * @return Pointer to WFSaveCalibration object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		void setCalibration(structSaveCalibration &stCalibration);

		void getCalibrationToSave(vector<structSaveCalibration> &vecSaveCal);

    private:
        vector<structSaveCalibration>	m_vecSaveCalibration;

};

#endif // INCMDSAVECALIBRATION_H
