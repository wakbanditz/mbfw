/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetCounter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetCounter class.
 @details

 ****************************************************************************
*/

#include "InCmdGetCounter.h"
#include "WFGetCounter.h"
#include "WebServerAndProtocolInclude.h"

InCmdGetCounter::InCmdGetCounter(string strUsage, string strID) :
				   IncomingCommand(WEB_CMD_NAME_GET_COUNTER_IN, strUsage, strID)
{
	m_strCounter.clear();
}

InCmdGetCounter::~InCmdGetCounter()
{

}

Workflow* InCmdGetCounter::getWorkflow()
{
	WFGetCounter* pWf = new WFGetCounter();
	return pWf;
}

string InCmdGetCounter::getCounter()
{
	return m_strCounter;
}

void InCmdGetCounter::setCounter(const string& strCounter)
{
	m_strCounter.assign(strCounter);
}
