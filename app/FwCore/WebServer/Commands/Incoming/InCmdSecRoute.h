/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSecRoute.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSecRoute class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSECROUTE_H
#define INCMDSECROUTE_H

#include <string>
#include <string.h>

#include "CommonInclude.h"

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the ROUTE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSecRoute : public IncomingCommand
{

	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdSecRoute constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdSecRoute(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdSecRoute destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdSecRoute();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
		 * @brief setSection set the section involved in the SectionRoute procedure
		 * @param strSection the section string associated to the received command
		 * @return true if section string is valid, false otherwise
		 * ************************************************************************************************************
		 */
		bool setComponent(std::string strSection);

		/*! ***********************************************************************************************************
		 * @brief getSection get the section involved in the SectionRoute procedure
		 * @return the index of the section if defined, -1 otherwise
		 * ************************************************************************************************************
		 */
		int getSection();

		/*! ***********************************************************************************************************
		 * @brief setCommand set the command involved in the SectionRoute procedure
		 * @param strCommand the command as string to be sent to section
		 * ************************************************************************************************************
		 */
		void setCommand(std::string strCommand);

		/*! ***********************************************************************************************************
		 * @brief getCommand get the command involved in the SectionRoute procedure
		 * @return the command as string to be sent to section
		 * ************************************************************************************************************
		 */
		std::string getCommand();


	private:

		std::string m_strCommand;
		int m_sectionRead;
};

#endif // INCMDSECROUTE_H
