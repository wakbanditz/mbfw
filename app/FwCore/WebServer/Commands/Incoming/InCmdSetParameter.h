/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetParameter.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSetParameter class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSETPARAMETER_H
#define INCMDSETPARAMETER_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SETPARAMETER command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSetParameter : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdSetParameter contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSetParameter(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSetParameter destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSetParameter();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the CancelSection command e retrieves the pointer
		 * @return Pointer to WFGetCalibration object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);


		/*! *************************************************************************************************
		 * @brief DATA SETTERS
		 * **************************************************************************************************
		 */
		void setComponent(std::string strValue);
		void setMotor(std::string strValue);
		void setMicrostep(std::string strValue);
		void setConversion(std::string strValue);
		void setHighCurrent(std::string strValue);
		void setLowCurrent(std::string strValue);
		void setAcceleration(std::string strValue);

		/*! *************************************************************************************************
		 * @brief DATA GETTERS
		 * **************************************************************************************************
		 */
		std::string getComponent();
		std::string getMotor();
		std::string getMicrostep();
		std::string getConversion();
		std::string getHighCurrent();
		std::string getLowCurrent();
		std::string getAcceleration();

    private:

        std::string m_strComponent, m_strMotor;
        std::string m_strMicrostep, m_strHighCurrent, m_strLowCurrent;
        std::string m_strAcceleration, m_strConversion;

};


#endif // INCMDSETPARAMETER_H
