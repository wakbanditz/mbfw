/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdVidasEp.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdVidasEp class.
 @details

 ****************************************************************************
*/

#ifndef INCMDVIDASEP_H
#define INCMDVIDASEP_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the VIDASEP command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdVidasEp : public IncomingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdVidasEp constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdVidasEp(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdVidasEp constructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdVidasEp();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);
};

#endif // INCMDVIDASEP_H
