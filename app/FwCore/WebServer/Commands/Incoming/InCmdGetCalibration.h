/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetCalibration.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetCalibration class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETCALIBRATION_H
#define INCMDGETCALIBRATION_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETCALIBRATION command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetCalibration : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdGetCalibration contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdGetCalibration(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdGetCalibration destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdGetCalibration();


		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the GetCalibration command e retrieves the pointer
         * @return pointer to WFGetCalibration object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief getComponent Retrieves the name of the component
		 * @return Name of the component
		 * *************************************************************************************************
		 */
		string getComponent( void );

		/*! *************************************************************************************************
		 * @brief getMotor Retrieves the name of the motor
		 * @return Name of the motor
		 * *************************************************************************************************
		 */
		string getMotor( void );

		/*! *************************************************************************************************
		 * @brief getMove Retrieves the label to calibrate
		 * @return Name of the label
		 * *************************************************************************************************
		 */
		string getLabel( void );

		/*! *************************************************************************************************
		 * @brief setComponent	Sets the name of the component to take in considerations for GetCalibration cmd
		 * @param strComponent
		 * *************************************************************************************************
		 */
		void setComponent( const string& strComponent );

		/*! *************************************************************************************************
		 * @brief setMotor	Sets the name of the motor to take in considerations for GetCalibration cmd
		 * @param strMotor
		 * *************************************************************************************************
		 */
		void setMotor( const string& strMotor );

		/*! *************************************************************************************************
		 * @brief setLabel	Sets the name of the movement to take in considerations for GetCalibration cmd
         * @param strLabel
		 * *************************************************************************************************
		 */
		void setLabel(const string& strLabel);

    private:

        string	m_strComponent;
        string	m_strMotor;
        string	m_strLabel;

};

#endif // INCMDGETCALIBRATION_H
