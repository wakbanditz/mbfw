/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdOPTCheck.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdOPTCheck class.
 @details

 ****************************************************************************
*/

#include "InCmdOPTCheck.h"
#include "WFOPTCheck.h"
#include "WebServerAndProtocolInclude.h"

InCmdOPTCheck::InCmdOPTCheck(string strUsage, string strID) :
                   IncomingCommand(WEB_CMD_NAME_OPT_CHECK_IN, strUsage, strID)
{
    uint8_t i, j;

    for(i = SCT_A_ID; i < SCT_NUM_TOT_SECTIONS; i++)
    {
        for(j = 0; j < SCT_NUM_TOT_SLOTS; j++)
        {
         m_bSectionSlotEnable[i][j] = false;
        }
    }
    m_intTarget = m_intTolerance = 0;
}

InCmdOPTCheck::~InCmdOPTCheck()
{

}

Workflow*InCmdOPTCheck::getWorkflow()
{
    WFOPTCheck* pWf = new WFOPTCheck();
    return pWf;
}

bool InCmdOPTCheck::isSectionEnabled(uint8_t section)
{
    uint8_t j;

    if(section >= SCT_NUM_TOT_SECTIONS) return false;

    for(j = 0; j < SCT_NUM_TOT_SLOTS; j++)
    {
        if(m_bSectionSlotEnable[section][j] == true)
        {
            return true;
        }
    }
    return false;
}

bool InCmdOPTCheck::isSlotEnabled(uint8_t section, uint8_t slot)
{
    if(section >= SCT_NUM_TOT_SECTIONS) return false;
    if(slot >= SCT_NUM_TOT_SLOTS) return false;

    return m_bSectionSlotEnable[section][slot];
}

int InCmdOPTCheck::getTarget()
{
    return m_intTarget;
}

int InCmdOPTCheck::getTolerance()
{
    return m_intTolerance;
}

void InCmdOPTCheck::setSlot(const string &strSection, const string &strSlot)
{
    uint8_t section = SCT_NUM_TOT_SECTIONS;

    if(strSection.compare(XML_VALUE_SECTION_A) == 0)
    {
        section = SCT_A_ID;
    }
    else if(strSection.compare(XML_VALUE_SECTION_B) == 0)
    {
        section = SCT_B_ID;
    }
    if(section >= SCT_NUM_TOT_SECTIONS) return;

    // the slot is labeled as 1-6 so .... minus 1
    uint8_t slot = atoi(strSlot.c_str()) - 1;
    if(slot >= SCT_NUM_TOT_SLOTS) return;

    m_bSectionSlotEnable[section][slot] = true;
}

void InCmdOPTCheck::setTarget(int target)
{
    m_intTarget = target;
}

void InCmdOPTCheck::setTolerance(int tolerance)
{
    m_intTolerance = tolerance;
}

