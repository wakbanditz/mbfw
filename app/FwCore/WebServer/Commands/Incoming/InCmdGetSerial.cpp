/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetSerial.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetSerial class.
 @details

 ****************************************************************************
*/

#include "InCmdGetSerial.h"
#include "WFGetSerial.h"
#include "WebServerAndProtocolInclude.h"

InCmdGetSerial::InCmdGetSerial(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_GETSERIAL_IN, strUsage, strID)
{

}


InCmdGetSerial::~InCmdGetSerial()
{

}

Workflow* InCmdGetSerial::getWorkflow(void)
{
	WFGetSerial* pWf = new WFGetSerial();
	return pWf;
}

