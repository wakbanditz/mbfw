/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdRunwl.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdRunwl class.
 @details

 ****************************************************************************
*/

#include "InCmdRunwl.h"
#include "WFRunwl.h"
#include "RunwlInfo.h"
#include "ProtocolInfo.h"
#include "PressureSettings.h"
#include "WebServerAndProtocolInclude.h"

InCmdRunwl::InCmdRunwl(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_RUNWL_IN, strUsage, strID)
{
	m_pRunwlInfo[SCT_A_ID] = new RunwlInfo();
	m_pRunwlInfo[SCT_B_ID] = new RunwlInfo();

	m_pProtocolInfo[0] = 0;
	m_pProtocolInfo[1] = 0;

	m_sectionRunwl[SCT_A_ID] = false;
	m_sectionRunwl[SCT_B_ID] = false;
}

InCmdRunwl::~InCmdRunwl()
{
	SAFE_DELETE(m_pRunwlInfo[SCT_A_ID]);
	SAFE_DELETE(m_pRunwlInfo[SCT_B_ID]);

	m_pressureSettings.clear();
}

Workflow* InCmdRunwl::getWorkflow(void)
{
	WFRunwl* pWf = new WFRunwl();
	return pWf;
}

bool InCmdRunwl::setProtocolInfoId(uint8_t index, string strId)
{
	// create the protocol object (that should not exist yet)
	if((index > 1) || m_pProtocolInfo[index] != 0)
	{
		return false;
	}

	m_pProtocolInfo[index] = new ProtocolInfo();
	m_pProtocolInfo[index]->setProtoId(strId);
	return true;
}

bool InCmdRunwl::setProtocolInfoName(uint8_t index, string strName)
{
	// create the protocol object (that should exist)
	if((index > 1) || m_pProtocolInfo[index] == 0)
	{
		return false;
	}

	m_pProtocolInfo[index]->setProtoName(strName);
	return true;
}

bool InCmdRunwl::setProtoGlobals(uint8_t index, const char* strGlobals)
{
	// check the protocol object (that should exist)
	if((index > 1) || m_pProtocolInfo[index] == 0)
	{
		return false;
	}

	m_pProtocolInfo[index]->setProtoGlobals(strGlobals);
	return true;
}

bool InCmdRunwl::setProtoLocals(uint8_t index, const char* strLocals)
{
	// check the protocol object (that should exist)
	if((index > 1) || m_pProtocolInfo[index] == 0)
	{
		return false;
	}

	m_pProtocolInfo[index]->setProtoLocals(strLocals);
	return true;
}

bool InCmdRunwl::setProtoMinMaxJ(uint8_t protoIndex, uint32_t value, uint8_t index, uint8_t isMax)
{
	// check the protocol object (that should exist)
	if((protoIndex > 1) || m_pProtocolInfo[protoIndex] == 0)
	{
		return false;
	}

	m_pProtocolInfo[protoIndex]->setProtoMinMaxJ(value, index, isMax);
	return true;
}

string InCmdRunwl::getProtocolInfoId(uint8_t index)
{
	// check the protocol object (that should exist)
	if((index > 1) || m_pProtocolInfo[index] == 0)
	{
		return "";
	}
	return m_pProtocolInfo[index]->getProtoId();
}

string InCmdRunwl::getProtocolInfoName(uint8_t index)
{
	// check the protocol object (that should exist)
	if((index > 1) || m_pProtocolInfo[index] == 0)
	{
		return "";
	}
	return m_pProtocolInfo[index]->getProtoName();
}

bool InCmdRunwl::getProtoGlobals(int8_t index, char* strGlobals)
{
	// check the protocol object (that should exist)
	if((index > 1) || m_pProtocolInfo[index] == 0)
	{
		return false;
	}
	m_pProtocolInfo[index]->getProtoGlobals(strGlobals);
	return true;
}

bool InCmdRunwl::getProtoLocals(int8_t index, char* strLocals)
{
	// check the protocol object (that should exist)
	if((index > 1) || m_pProtocolInfo[index] == 0)
	{
		return false;
	}
	m_pProtocolInfo[index]->getProtoLocals(strLocals);
	return true;
}

uint32_t InCmdRunwl::getProtoMinMaxJ(int8_t protoIndex, uint8_t index, uint8_t isMax)
{
	// check the protocol object (that should exist)
	if((protoIndex > 1) || m_pProtocolInfo[protoIndex] == 0)
	{
		return false;
	}
	return m_pProtocolInfo[protoIndex]->getProtoMinMaxJ(index, isMax);
}

ProtocolInfo* InCmdRunwl::getProtocolInfoPointer(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_pProtocolInfo[section];
}


RunwlInfo* InCmdRunwl::getRunwlPointer(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_pRunwlInfo[section];
}

bool InCmdRunwl::setSectionEnabled(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	m_sectionRunwl[section] = true;
	return true;
}

bool InCmdRunwl::isSectionEnabled(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	return m_sectionRunwl[section];
}

bool InCmdRunwl::setSection(uint8_t section, string strSection)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setSection(strSection);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionProtoId(uint8_t section, string strId)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setProtoId(strId);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionUnitConstraint(uint8_t section, string strUnitConstraint)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setUnitConstraint(strUnitConstraint);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionMaxConstraint(uint8_t section, uint32_t maxConstraint)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setMaxConstraint(maxConstraint);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionMinConstraint(uint8_t section, uint32_t minConstraint)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setMinConstraint(minConstraint);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionStripId(uint8_t section, int strip, string strId)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setStripId(strip, strId);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionStripLiquidProfile(uint8_t section, int strip, string strProfile)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setStripProfile(strip, strProfile);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionStripPressureProfile(uint8_t section, int strip, string strCheck)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setStripCheck(strip, strCheck);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionStripRfuconv(uint8_t section, int strip, uint8_t value)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
        m_pRunwlInfo[section]->setStripRfuconv(strip, value);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionStripSettings(uint8_t section, int strip, string strSettings)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setProtoSettings(strip, strSettings);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionStripMeasureAir(uint8_t section, int strip, uint8_t value)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setProtoMeasureAir(strip, value);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionStripTreatedPressure(uint8_t section, int strip, uint8_t value)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setProtoTreatedPressure(strip, value);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionStripElaboratedPressure(uint8_t section, int strip, uint8_t value)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setProtoElaboratedPressure(strip, value);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionMonitoringTemperature(uint8_t section, uint8_t value)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setMonitoringTemperature(value);
		return true;
	}
	return false;
}

bool InCmdRunwl::setSectionMonitoringAutocheck(uint8_t section, uint8_t value)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		m_pRunwlInfo[section]->setMonitoringAutocheck(value);
		return true;
	}
	return false;
}

uint8_t InCmdRunwl::getSection(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return SCT_NONE_ID;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getSection();
	}
	return SCT_NONE_ID;
}

string InCmdRunwl::getSectionProtoId(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getProtoId();
	}
	return "";
}

string InCmdRunwl::getSectionUnitConstraint(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return "";

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getUnitConstraint();
	}
	return "";
}

uint32_t InCmdRunwl::getSectionMinimumConstraint(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getMinimumConstraint();
	}
	return 0;
}

uint32_t InCmdRunwl::getSectionMaximumConstraint(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getMaximumConstraint();
	}
	return 0;
}

string InCmdRunwl::getSectionStripId(uint8_t section, int strip)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return "";

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getStripId(strip);
	}
	return "";
}

uint8_t InCmdRunwl::getSectionStripRfuconv(uint8_t section, int strip)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getStripRfuconv(strip);
	}
	return 0;
}

string InCmdRunwl::getSectionStripSettings(uint8_t section, int strip)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getProtoSettings(strip);
	}
	return "";
}

uint8_t InCmdRunwl::getSectionStripMeasureAir(uint8_t section, int strip)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getProtoMeasureAir(strip);
	}
	return 0;
}

uint8_t InCmdRunwl::getSectionStripTreatedPressure(uint8_t section, int strip)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getProtoTreatedPressure(strip);
	}
	return 0;
}

uint8_t InCmdRunwl::getSectionStripElaboratedPressure(uint8_t section, int strip)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getProtoElaboratedPressure(strip);
	}
	return 0;
}

uint8_t InCmdRunwl::getStripWellProfile(uint8_t section, int strip, int well)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getStripWellProfile(strip, well);
	}
	return 0;
}

uint8_t InCmdRunwl::getStripWellCheck(uint8_t section, int strip, int well)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getStripWellCheck(strip, well);
	}
	return 0;
}

bool InCmdRunwl::getSectionMonitoringTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getMonitoringTemperature();
	}
	return false;
}

bool InCmdRunwl::getSectionMonitoringAutocheck(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->getMonitoringAutocheck();
	}
	return false;
}

bool InCmdRunwl::isAtLeastOneStripEnabled(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->isAtLeastOneStripEnabled();
	}
	return false;
}

bool InCmdRunwl::decodeConstraint(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return false;

	if(m_pRunwlInfo[section])
	{
		return m_pRunwlInfo[section]->decodeConstraint();
	}
	return false;
}

bool InCmdRunwl::addPressureSettings(string strID, string strSettings)
{
	m_pressureSettings.emplace_back(strID, strSettings);

	return (m_pressureSettings.size() > 0);
}

bool InCmdRunwl::getPressureSettings(uint8_t index, string * strID, string * strSettings)
{
	if(index >= m_pressureSettings.size()) return false;

	strID->assign(m_pressureSettings.at(index).first);
	strSettings->assign(m_pressureSettings.at(index).second);

	return true;
}
