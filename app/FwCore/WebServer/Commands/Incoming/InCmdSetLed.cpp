/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetLed.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSetLed class.
 @details

 ****************************************************************************
*/

#include "InCmdSetLed.h"
#include "WFSetLed.h"
#include "WebServerAndProtocolInclude.h"

InCmdSetLed::InCmdSetLed(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_SETLED_IN, strUsage, strID)
{
	m_strComponent.clear();
	m_strMode.clear();
	m_strLed.clear();
}


InCmdSetLed::~InCmdSetLed()
{

}

Workflow* InCmdSetLed::getWorkflow(void)
{
	WFSetLed* pWf = new WFSetLed();
	return pWf;
}

void InCmdSetLed::setComponent(string strComponent)
{
	m_strComponent.assign(strComponent);
}

void InCmdSetLed::setLed(string strLed)
{
	m_strLed.assign(strLed);
}

void InCmdSetLed::setMode(string strMode)
{
	m_strMode.assign(strMode);
}

string InCmdSetLed::getComponent()
{
	return m_strComponent;
}

string InCmdSetLed::getLed()
{
	return m_strLed;
}

string InCmdSetLed::getMode()
{
	return m_strMode;
}

