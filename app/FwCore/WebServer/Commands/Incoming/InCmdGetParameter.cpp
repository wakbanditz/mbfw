/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetParameter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetParameter class.
 @details

 ****************************************************************************
*/

#include "InCmdGetParameter.h"
#include "WebServerAndProtocolInclude.h"
#include "WFGetParameter.h"

InCmdGetParameter::InCmdGetParameter(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_GETPARAMETER_IN, strUsage, strID)
{
	m_strComponent.clear();
	m_strMotor.clear();
}

InCmdGetParameter::~InCmdGetParameter()
{
	/* Nothing to do yet */
}

Workflow* InCmdGetParameter::getWorkflow(void)
{
	WFGetParameter* pWf = new WFGetParameter();
	return pWf;
}

string InCmdGetParameter::getComponent()
{
	return m_strComponent;
}

string InCmdGetParameter::getMotor()
{
	return m_strMotor;
}

void InCmdGetParameter::setComponent(const string &strComponent)
{
	m_strComponent.assign(strComponent);
}

void InCmdGetParameter::setMotor(const string &strMotor)
{
	m_strMotor.assign(strMotor);
}
