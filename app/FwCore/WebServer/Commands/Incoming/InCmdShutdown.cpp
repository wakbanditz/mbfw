/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdShutdown.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdShutdown class.
 @details

 ****************************************************************************
*/

#include "InCmdShutdown.h"
#include "WFShutdown.h"
#include "WebServerAndProtocolInclude.h"

InCmdShutdown::InCmdShutdown(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_SHUTDOWN_IN, strUsage, strID)
{
}

InCmdShutdown::~InCmdShutdown()
{

}

Workflow* InCmdShutdown::getWorkflow(void)
{
	WFShutdown* pWf = new WFShutdown();
	return pWf;
}

