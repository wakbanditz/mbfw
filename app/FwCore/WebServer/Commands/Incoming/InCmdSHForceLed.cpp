/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSHForceLed.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSHForceLed class.
 @details

 ****************************************************************************
*/

#include "InCmdSHForceLed.h"
#include "WebServerAndProtocolInclude.h"
#include "WFSHForceLed.h"

InCmdSHForceLed::InCmdSHForceLed(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_SH_FORCE_LED_IN, strUsage, strID)
{
	m_strDuration.clear();
}

InCmdSHForceLed::~InCmdSHForceLed()
{

}

Workflow* InCmdSHForceLed::getWorkflow()
{
	WFSHForceLed* pWf = new WFSHForceLed();
	return pWf;
}

void InCmdSHForceLed::setDuration(string& strDuration)
{
	m_strDuration = strDuration;
}

string InCmdSHForceLed::getDuration()
{
	return m_strDuration;
}
