/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdOPTCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdOPTCalibrate class.
 @details

 ****************************************************************************
*/

#include "InCmdOPTCalibrate.h"
#include "WFOPTCalibrate.h"
#include "WebServerAndProtocolInclude.h"

InCmdOPTCalibrate::InCmdOPTCalibrate(string strUsage, string strID) :
				   IncomingCommand(WEB_CMD_NAME_OPT_CALIBRATE_IN, strUsage, strID)
{
	m_strSection.clear();
	m_strSlot.clear();
	m_strTarget.clear();
}

InCmdOPTCalibrate::~InCmdOPTCalibrate()
{

}

Workflow*InCmdOPTCalibrate::getWorkflow()
{
	WFOPTCalibrate* pWf = new WFOPTCalibrate();
	return pWf;
}

string InCmdOPTCalibrate::getSection()
{
	return m_strSection;
}

string InCmdOPTCalibrate::getSlot()
{
	return m_strSlot;
}

string InCmdOPTCalibrate::getTarget()
{
	return m_strTarget;
}

void InCmdOPTCalibrate::setSection(const string& strSection)
{
	m_strSection.assign(strSection);
}

void InCmdOPTCalibrate::setSlot(const string& strSlot)
{
	m_strSlot.assign(strSlot);
}

void InCmdOPTCalibrate::setTarget(const string& strTarget)
{
	m_strTarget.assign(strTarget);
}
