/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdCaptureImage.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdCaptureImage class.
 @details

 ****************************************************************************
*/

#ifndef INCMDCAPTUREIMAGE_H
#define INCMDCAPTUREIMAGE_H

#include "IncomingCommand.h"

class Workflow;

typedef struct
{
	int liTop;
	int liBottom;
	int liLeft;
	int liRight;
} structCoordinates;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the CAPTUREIMAGE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdCaptureImage : public IncomingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdGetPicture constructor
		 * @param strID the CommandId string
         * @param strUsage the Connection string
		 * ************************************************************************************************************
		 */
		InCmdCaptureImage(string strUsage, string strID);

		/*! ***********************************************************************************************************
		 * @brief ~InCmdGetPicture	destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdCaptureImage(void);

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		void setCropEnabled(int liIdx, string& strEnabled);
		void setAttribute(int liIdx, string& strAttr);
		void setMotor(int liIdx, string& strMotor);
		void setComponent(int liIdx, string& strComponent);
		void setPictureAction(int liIdx, string& strAction);
        void setPredefinedRoi(int liIdx, string& strRoiName);
		void setImageFormat(int liIdx, string& strFormat);
		void setImageFocus(int liIdx, string& strFocus);
		bool setImageLight(int liIdx, string& strLight);
		bool setImageQuality(int liIdx, string& strQualityVal);
		bool setTopVal(int liIdx, string& strTopVal);
		bool setBottomVal(int liIdx, string& strBottomVal);
		bool setLeftVal(int liIdx, string& strLeftVal);
		bool setRightVal(int liIdx, string& strRightVal);
		void setPicturesNum(int liNum);
		vector<string> getImageFocus(void);
		vector<int> getImageQuality(void);
		vector<int> getImageLight(void);
		int getPicturesNum(void);
		vector<string> getImageFormat(void);
		vector<vector<string>> getAttribute(void);
		vector<vector<string>> getMotor(void);
		vector<vector<string>> getComponent(void);
		vector<structCoordinates> getCoordinates(void);
        vector<string> getPredefinedRoi(void);
		vector<bool> getCropEnabled(void);
		vector<string> getAction(void);

    private:

		int				m_nPicturesNum;
		vector<bool>	m_vCropEnabled;
        vector<int>		m_vImageQuality;
		vector<int>		m_vImageLight;
		vector<string>	m_vAction;
		vector<string>		m_vRoiName;
		vector<string>		m_vFormat;
		vector<string>		m_vFocus;
		vector<vector<string>>		m_vAttribute;
		vector<vector<string>>		m_vMotor;
		vector<vector<string>>		m_vComponent;
		vector<structCoordinates>	m_vCoordinates;

};

#endif // INCMDCAPTUREIMAGE_H
