/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSSCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSSCalibrate class.
 @details

 ****************************************************************************
*/

#include "InCmdSSCalibrate.h"
#include "WebServerAndProtocolInclude.h"
#include "WFSSCalibrate.h"

InCmdSSCalibrate::InCmdSSCalibrate(string strUsage, string strID) :
				  IncomingCommand(WEB_CMD_NAME_SS_CALIBRATE_IN, strUsage, strID)
{

}

InCmdSSCalibrate::~InCmdSSCalibrate()
{
	/* Nothing to do yet */
}

Workflow* InCmdSSCalibrate::getWorkflow()
{
	WFSSCalibrate* pWf = new WFSSCalibrate();
	return pWf;
}
