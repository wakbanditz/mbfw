/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetFanSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetFanSettings class.
 @details

 ****************************************************************************
*/

#include "InCmdGetFanSettings.h"
#include "WFGetFanSettings.h"
#include "WebServerAndProtocolInclude.h"

InCmdGetFanSettings::InCmdGetFanSettings(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_GET_FAN_SETTINGS_IN, strUsage, strID)
{
}

InCmdGetFanSettings::~InCmdGetFanSettings()
{
	/* Nothing to do yet */
}

Workflow* InCmdGetFanSettings::getWorkflow(void)
{
	WFGetFanSettings* pWf = new WFGetFanSettings();
	return pWf;
}


