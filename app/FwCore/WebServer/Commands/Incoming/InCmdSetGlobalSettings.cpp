/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetGlobalSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSetGlobalSettings class.
 @details

 ****************************************************************************
*/

#include "InCmdSetGlobalSettings.h"
#include "WFSetGlobalSettings.h"
#include "WebServerAndProtocolInclude.h"

InCmdSetGlobalSettings::InCmdSetGlobalSettings(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_SET_GLOBAL_SETTINGS_IN, strUsage, strID)
{
	for(uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
        m_SprMin[section] = m_SprMax[section] = -1;
        m_SprTarget[section] = -1;
        m_TrayMin[section] = m_TrayMax[section] = -1;
        m_TrayTarget[section] = -1;
        m_SprTolerance[section] = m_TrayTolerance[section] = 0;
    }
	m_bForce = false;
    m_Duration = 0;
    m_InternalMax = m_InternalMin = -1;
}

InCmdSetGlobalSettings::~InCmdSetGlobalSettings()
{
	/* Nothing to do yet */
}

Workflow* InCmdSetGlobalSettings::getWorkflow(void)
{
	WFSetGlobalSettings* pWf = new WFSetGlobalSettings();
	return pWf;
}

void InCmdSetGlobalSettings::setSprMinTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprMin[section] = value;
	}
}

void InCmdSetGlobalSettings::setSprMaxTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprMax[section] = value;
	}
}

void InCmdSetGlobalSettings::setSprTargetTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprTarget[section] = value;
	}
}

void InCmdSetGlobalSettings::setSprToleranceTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprTolerance[section] = value;
	}
}

void InCmdSetGlobalSettings::setTrayMinTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayMin[section] = value;
	}
}

void InCmdSetGlobalSettings::setTrayMaxTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayMax[section] = value;
	}
}

void InCmdSetGlobalSettings::setTrayTargetTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayTarget[section] = value;
	}
}

void InCmdSetGlobalSettings::setTrayToleranceTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayTolerance[section] = value;
	}
}

void InCmdSetGlobalSettings::setInternalMinTemperature(int value)
{
	m_InternalMin = value;
}

void InCmdSetGlobalSettings::setInternalMaxTemperature(int value)
{
	m_InternalMax = value;
}

void InCmdSetGlobalSettings::setForceTemperature(bool status)
{
    m_bForce = status;
}

void InCmdSetGlobalSettings::setDuration(uint16_t duration)
{
    m_Duration = duration;
}

int InCmdSetGlobalSettings::getSprMinTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprMin[section];
}

int InCmdSetGlobalSettings::getSprMaxTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprMax[section];
}

int InCmdSetGlobalSettings::getSprTargetTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprTarget[section];
}

int InCmdSetGlobalSettings::getSprToleranceTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprTolerance[section];
}

int InCmdSetGlobalSettings::getTrayMinTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayMin[section];
}

int InCmdSetGlobalSettings::getTrayMaxTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayMax[section];
}

int InCmdSetGlobalSettings::getTrayTargetTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayTarget[section];
}

int InCmdSetGlobalSettings::getTrayToleranceTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayTolerance[section];
}

int InCmdSetGlobalSettings::getInternalMinTemperature()
{
	return m_InternalMin;
}

int InCmdSetGlobalSettings::getInternalMaxTemperature()
{
	return m_InternalMax;
}

bool InCmdSetGlobalSettings::getForceTemperature()
{
    return m_bForce;
}

uint16_t InCmdSetGlobalSettings::getDuration()
{
    return m_Duration;
}
