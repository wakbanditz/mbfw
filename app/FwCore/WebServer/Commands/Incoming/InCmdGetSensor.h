/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetSensor.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetSensor class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETSENSOR_H
#define INCMDGETSENSOR_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETSENSOR command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetSensor : public IncomingCommand
{

	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdGetSensor constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdGetSensor(string strUsage, string strID);

		virtual ~InCmdGetSensor();

		Workflow* getWorkflow(void);
		string getCategory() const;
		void setCategory(const string& strCategory);
		string getComponent() const;
		void setComponent(const string& strComponent);
		string getId() const;
		void setId(const string& strId);


    private:

        string m_strCategory;
        string m_strComponent;
        string m_strId;
};


#endif // INCMDGETSENSOR_H
