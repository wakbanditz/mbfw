/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetCounter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSetCounter class.
 @details

 ****************************************************************************
*/

#include "InCmdSetCounter.h"
#include "WFSetCounter.h"
#include "WebServerAndProtocolInclude.h"

InCmdSetCounter::InCmdSetCounter(string strUsage, string strID) :
				   IncomingCommand(WEB_CMD_NAME_SET_COUNTER_IN, strUsage, strID)
{
	m_strCounter.clear();
	m_strValue.clear();
}

InCmdSetCounter::~InCmdSetCounter()
{

}

Workflow* InCmdSetCounter::getWorkflow()
{
	WFSetCounter* pWf = new WFSetCounter();
	return pWf;
}

string InCmdSetCounter::getCounter()
{
	return m_strCounter;
}

void InCmdSetCounter::setCounter(const string& strCounter)
{
	m_strCounter.assign(strCounter);
}

string InCmdSetCounter::getValue()
{
	return m_strValue;
}

void InCmdSetCounter::setValue(const string& strValue)
{
	m_strValue.assign(strValue);
}

