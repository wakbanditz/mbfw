/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdInit.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdInit class.
 @details

 ****************************************************************************
*/

#include "InCmdInit.h"
#include "WFInit.h"
#include "WebServerAndProtocolInclude.h"

InCmdInit::InCmdInit(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_INIT_IN, strUsage, strID)
{
	m_strComponent.clear();
}

InCmdInit::~InCmdInit()
{
	/* Nothing to do yet */
}

Workflow* InCmdInit::getWorkflow(void)
{
	WFInit* pWf = new WFInit();
	return pWf;
}

void InCmdInit::setComponent(const string &strComponent)
{
	m_strComponent.assign(strComponent);
}

string InCmdInit::getComponent()
{
	return m_strComponent;
}
