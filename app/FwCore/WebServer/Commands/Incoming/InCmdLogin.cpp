/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdLogin.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdLogin class.
 @details

 ****************************************************************************
*/

#include "InCmdLogin.h"
#include "WFLogin.h"
#include "WebServerAndProtocolInclude.h"

InCmdLogin::InCmdLogin(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_LOGIN_IN, strUsage, strID)
{
	// no login is (obviously) requested
	m_strId.clear();
	m_strPwd.clear();
}

InCmdLogin::~InCmdLogin()
{

}

Workflow* InCmdLogin::getWorkflow(void)
{
	WFLogin* pWf = new WFLogin();
	return pWf;
}

std::string InCmdLogin::getId() const
{
	return m_strId;
}

void InCmdLogin::setId(const std::string& strId)
{
	m_strId.assign(strId);
}


std::string InCmdLogin::getPwd() const
{
	return m_strPwd;
}

void InCmdLogin::setPwd(const std::string& strPwd)
{
	m_strPwd.assign(strPwd);
}

