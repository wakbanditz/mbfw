/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSleep.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSleep class.
 @details

 ****************************************************************************
*/

#include "InCmdSleep.h"
#include "WFSleep.h"
#include "WebServerAndProtocolInclude.h"

InCmdSleep::InCmdSleep(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_SLEEP_IN, strUsage, strID)
{
	m_bSleep = false;
}

InCmdSleep::~InCmdSleep()
{
	/* Nothing to do yet */
}

Workflow* InCmdSleep::getWorkflow(void)
{
	WFSleep* pWf = new WFSleep();
	return pWf;
}

bool InCmdSleep::setSleepInstrument(string strEnabled)
{
	if(strEnabled.compare(XML_VALUE_ENABLE) == 0)
	{
		m_bSleep = true;
	}
	return true;
}

bool InCmdSleep::getSleepInstrument()
{
	return m_bSleep;
}
