/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdUnknown.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdUnknown class.
 @details

 ****************************************************************************
*/

#include "InCmdUnknown.h"
#include "WFUnknown.h"
#include "WebServerAndProtocolInclude.h"

InCmdUnknown::InCmdUnknown(string strUsage, string strID) :
				   IncomingCommand(WEB_CMD_NAME_UNKNOWN, strUsage, strID)
{
	m_strCmd.clear();
}

InCmdUnknown::~InCmdUnknown()
{

}

Workflow* InCmdUnknown::getWorkflow()
{
	WFUnknown* pWf = new WFUnknown();
	return pWf;
}

void InCmdUnknown::setCommandString(string strCmd)
{
	m_strCmd.assign(strCmd);
}

string InCmdUnknown::getCommandString()
{
	return m_strCmd;
}
