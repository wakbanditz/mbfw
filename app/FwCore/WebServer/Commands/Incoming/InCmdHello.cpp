/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdHello.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdHello class.
 @details

 ****************************************************************************
*/

#include "InCmdHello.h"
#include "WFHello.h"
#include "WebServerAndProtocolInclude.h"

InCmdHello::InCmdHello() : IncomingCommand(WEB_CMD_NAME_HELLO_IN)
{
	// no login is requested
}

InCmdHello::~InCmdHello()
{

}

Workflow* InCmdHello::getWorkflow(void)
{
	WFHello* pWf = new WFHello();
	return pWf;
}
