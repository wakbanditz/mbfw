/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSHCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSHCalibrate class.
 @details

 ****************************************************************************
*/

#include "InCmdSHCalibrate.h"
#include "WebServerAndProtocolInclude.h"
#include "WFSHCalibrate.h"


InCmdSHCalibrate::InCmdSHCalibrate(string strUsage, string strID) :
				  IncomingCommand(WEB_CMD_NAME_SS_CALIBRATE_IN, strUsage, strID)
{
	m_strSection.clear();
	m_strSlot.clear();
}

InCmdSHCalibrate::~InCmdSHCalibrate()
{

}

Workflow* InCmdSHCalibrate::getWorkflow()
{
	WFSHCalibrate* pWf = new WFSHCalibrate();
	return pWf;
}

void InCmdSHCalibrate::setSection(string& strSection)
{
	m_strSection = strSection;

}

string InCmdSHCalibrate::getSection()
{
	return m_strSection;
}

void InCmdSHCalibrate::setSlot(string& strSlot)
{
	m_strSlot = strSlot;
}

string InCmdSHCalibrate::getSlot()
{
	return m_strSlot;
}
