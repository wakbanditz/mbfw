/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetLed.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSetLed class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSETLED_H
#define INCMDSETLED_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SETLED command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSetLed : public IncomingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdSetLed constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdSetLed(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdSetLed destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdSetLed();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
		 * @brief setComponent set the component string
		 * @param strComponent the argument string
		 * ************************************************************************************************************
		 */
		void setComponent(std::string strComponent);

		/*! ***********************************************************************************************************
		 * @brief setLed set the led string
		 * @param strLed the argument string
		 * ************************************************************************************************************
		 */
		void setLed(std::string strLed);

		/*! ***********************************************************************************************************
		 * @brief setMode set the mode string
		 * @param strMode the argument string
		 * ************************************************************************************************************
		 */
		void setMode(std::string strMode);

		/*! ***********************************************************************************************************
		 * @brief getComponent get the Component string
		 * @return the argument string
		 * ************************************************************************************************************
		 */
		std::string getComponent();

		/*! ***********************************************************************************************************
		 * @brief getLed get the Led string
		 * @return the argument string
		 * ************************************************************************************************************
		 */
		std::string getLed();

		/*! ***********************************************************************************************************
		 * @brief getMode get the Mode string
		 * @return the argument string
		 * ************************************************************************************************************
		 */
		std::string getMode();

	private :

		std::string m_strComponent, m_strMode, m_strLed;
};


#endif // INCMDSETLED_H
