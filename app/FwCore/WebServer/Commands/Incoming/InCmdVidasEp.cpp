/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdVidasEp.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdVidasEp class.
 @details

 ****************************************************************************
*/

#include "InCmdVidasEp.h"
#include "WFVidasEp.h"
#include "WebServerAndProtocolInclude.h"

InCmdVidasEp::InCmdVidasEp(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_VIDASEP_IN, strUsage, strID)
{

}


InCmdVidasEp::~InCmdVidasEp()
{

}

Workflow* InCmdVidasEp::getWorkflow(void)
{
	WFVidasEp* pWf = new WFVidasEp();
	return pWf;
}

