/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetGlobalSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSetGlobalSettings class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGLOBALSETTINGS_H
#define INCMDGLOBALSETTINGS_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SETGLOBALSETTINGS command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSetGlobalSettings : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdGlobalSettings contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSetGlobalSettings(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdGlobalSettings destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSetGlobalSettings();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the CancelSection command e retrieves the pointer
		 * @return Pointer to WFGetCalibration object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);


		/*! *************************************************************************************************
		 * @brief DATA SETTERS
		 * **************************************************************************************************
		 */
		void setSprMinTemperature(uint8_t section, int value);
		void setSprMaxTemperature(uint8_t section, int value);
		void setSprTargetTemperature(uint8_t section, int value);
		void setSprToleranceTemperature(uint8_t section, int value);

		void setTrayMinTemperature(uint8_t section, int value);
		void setTrayMaxTemperature(uint8_t section, int value);
		void setTrayTargetTemperature(uint8_t section, int value);
		void setTrayToleranceTemperature(uint8_t section, int value);

		void setInternalMinTemperature(int value);
		void setInternalMaxTemperature(int value);

		void setForceTemperature(bool status);
        void setDuration(uint16_t duration);

		/*! *************************************************************************************************
		 * @brief DATA GETTERS
		 * **************************************************************************************************
		 */
		int getSprMinTemperature(uint8_t section);
		int getSprMaxTemperature(uint8_t section);
		int getSprTargetTemperature(uint8_t section);
		int getSprToleranceTemperature(uint8_t section);

		int getTrayMinTemperature(uint8_t section);
		int getTrayMaxTemperature(uint8_t section);
		int getTrayTargetTemperature(uint8_t section);
		int getTrayToleranceTemperature(uint8_t section);

		int getInternalMinTemperature();
		int getInternalMaxTemperature();

		bool getForceTemperature();
        uint16_t getDuration();

    private:

        int m_SprMin[SCT_NUM_TOT_SECTIONS], m_SprMax[SCT_NUM_TOT_SECTIONS];
        int m_SprTarget[SCT_NUM_TOT_SECTIONS], m_SprTolerance[SCT_NUM_TOT_SECTIONS];
        int m_TrayMin[SCT_NUM_TOT_SECTIONS], m_TrayMax[SCT_NUM_TOT_SECTIONS];
        int m_TrayTarget[SCT_NUM_TOT_SECTIONS], m_TrayTolerance[SCT_NUM_TOT_SECTIONS];
        bool m_bForce;
        int m_InternalMin, m_InternalMax;
        uint16_t m_Duration;

};

#endif // INCMDGLOBALSETTINGS_H
