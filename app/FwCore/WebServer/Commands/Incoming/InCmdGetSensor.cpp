/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetSensor.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetSensor class.
 @details

 ****************************************************************************
*/

#include "InCmdGetSensor.h"
#include "WFGetSensor.h"
#include "WebServerAndProtocolInclude.h"


InCmdGetSensor::InCmdGetSensor(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_GETSENSOR_IN, strUsage, strID)
{
	m_strCategory.clear();
	m_strComponent.clear();
	m_strId.clear();
}

InCmdGetSensor::~InCmdGetSensor()
{

}

Workflow* InCmdGetSensor::getWorkflow(void)
{
	WFGetSensor* pWf = new WFGetSensor();
	return pWf;
}


string InCmdGetSensor::getCategory() const
{
	return m_strCategory;
}

void InCmdGetSensor::setCategory(const string& strCategory)
{
	m_strCategory.assign(strCategory);
}

string InCmdGetSensor::getComponent() const
{
	return m_strComponent;
}

void InCmdGetSensor::setComponent(const string& strComponent)
{
	m_strComponent.assign(strComponent);
}

string InCmdGetSensor::getId() const
{
	return m_strId;
}

void InCmdGetSensor::setId(const string& strId)
{
	m_strId.assign(strId);
}
