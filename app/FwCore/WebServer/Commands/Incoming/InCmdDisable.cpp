/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdDisable.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdDisable class.
 @details

 ****************************************************************************
*/

#include "InCmdDisable.h"
#include "WFDisable.h"
#include "WebServerAndProtocolInclude.h"

InCmdDisable::InCmdDisable(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_DISABLE_IN, strUsage, strID)
{
	m_strDisableInstrument.clear();
	m_strDisableNsh.clear();
	m_strDisableCamera.clear();
	m_strDisableSectionA.clear();
	m_strDisableSectionB.clear();
}

InCmdDisable::~InCmdDisable()
{
	/* Nothing to do yet */
}

Workflow* InCmdDisable::getWorkflow(void)
{
	WFDisable* pWf = new WFDisable();
	return pWf;
}

bool InCmdDisable::setDisableInstrument(string strEnable)
{
	m_strDisableInstrument.assign(strEnable);
	return true;
}

bool InCmdDisable::setDisableNSH(string strEnable)
{
	m_strDisableNsh.assign(strEnable);
	return true;
}

bool InCmdDisable::setDisableCamera(string strEnable)
{
	m_strDisableCamera.assign(strEnable);
	return true;
}

bool InCmdDisable::setDisableSection(string strSection, string strEnable)
{
	if(strSection.compare(XML_VALUE_SECTION_A) == 0)
	{
		m_strDisableSectionA.assign(strEnable);
	}
	else if(strSection.compare(XML_VALUE_SECTION_B) == 0)
	{
		m_strDisableSectionB.assign(strEnable);
	}
	return true;
}

string InCmdDisable::getDisableInstrument()
{
	return m_strDisableInstrument;
}

string InCmdDisable::getDisableNSH()
{
	return m_strDisableNsh;
}

string InCmdDisable::getDisableCamera()
{
	return m_strDisableCamera;
}

string InCmdDisable::getDisableSection(uint8_t section)
{
	std::string strRet;

	strRet.clear();

	if(section == SCT_A_ID)
	{
		strRet.assign(m_strDisableSectionA);
	}
	else if(section == SCT_B_ID)
	{
		strRet.assign(m_strDisableSectionB);
	}
	return strRet;
}
