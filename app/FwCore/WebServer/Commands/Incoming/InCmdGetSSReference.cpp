/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetSSReference.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetSSReference class.
 @details

 ****************************************************************************
*/

#include "InCmdGetSSReference.h"
#include "WebServerAndProtocolInclude.h"
#include "WFGetSSReference.h"

InCmdGetSSReference::InCmdGetSSReference(string strUsage, string strID) :
					 IncomingCommand(WEB_CMD_NAME_GET_SS_REFERENCE_IN, strUsage, strID)
{

}

InCmdGetSSReference::~InCmdGetSSReference()
{

}

Workflow*InCmdGetSSReference::getWorkflow()
{
	WFGetSSReference* pWf = new WFGetSSReference();
	return pWf;
}
