/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSignalRead.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSignalRead class.
 @details

 ****************************************************************************
*/

#include "InCmdSignalRead.h"
#include "WebServerAndProtocolInclude.h"
#include "WFSignalRead.h"

InCmdSignalRead::InCmdSignalRead(string strUsage, string strID) :
				 IncomingCommand(WEB_CMD_NAME_SIGNAL_READ_IN, strUsage, strID)
{
	m_strInterval.clear();
	m_strRepetitions.clear();
}

InCmdSignalRead::~InCmdSignalRead()
{

}

Workflow* InCmdSignalRead::getWorkflow()
{
	WFSignalRead* pWf = new WFSignalRead();
	return pWf;
}

void InCmdSignalRead::setRepetitions(string& strRepetitions)
{
	m_strRepetitions = strRepetitions;
}

string InCmdSignalRead::getRepetitions()
{
	return m_strRepetitions;
}

void InCmdSignalRead::setInterval(string& strInterval)
{
	m_strInterval = strInterval;
}

string InCmdSignalRead::getInterval()
{
	return m_strInterval;
}
