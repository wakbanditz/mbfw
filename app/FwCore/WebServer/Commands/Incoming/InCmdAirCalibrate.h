/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdAirCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdAirCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef INCMDAIRCALIBRATE_H
#define INCMDAIRCALIBRATE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the AIRCALIBRATE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdAirCalibrate : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdAirCalibrate contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdAirCalibrate(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdAirCalibrate destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdAirCalibrate();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the InCmdAirCalibrate command e retrieves
		 *		  the pointer
         * @return Pointer to WFAirCalibration object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief getMove Retrieves the label to calibrate
		 * @return Name of the label
		 * *************************************************************************************************
		 */
		string getTarget(void);

		/*! *************************************************************************************************
		 * @brief setTarget	Sets the name of the movement to take in considerations for GetCalibration cmd
         * @param strTarget the target as string
		 * *************************************************************************************************
		 */
		void setTarget(const string& strTarget);

    private:

        string	m_strTarget;

};

#endif // INCMDAIRCALIBRATE_H
