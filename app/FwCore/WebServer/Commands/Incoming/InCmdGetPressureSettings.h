/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetPressureSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetPressure class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETPRESSURESETTINGS_H
#define INCMDGETPRESSURESETTINGS_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETPRESSURESETTINGS command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetPressureSettings : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdGetPressureSettings contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdGetPressureSettings(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdGetPressureSettings destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdGetPressureSettings();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the CancelSection command e retrieves the pointer
		 * @return Pointer to WFGetPressureOffsets object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief setSection set the Section read from the incoming command
		 * @param strSection the section string (A or B)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSection(std::string strSection);

		/*! *************************************************************************************************
		 * @brief getSection get the Section read from the incoming command
		 * @return the section index
		 * **************************************************************************************************
		 */
		uint8_t getSection(std::vector<uint8_t> * pSection);


	private:

		std::vector<uint8_t> m_intSection;

};



#endif // INCMDGETPRESSURESETTINGS_H
