/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdUnknown.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdUnknown class.
 @details

 ****************************************************************************
*/
#ifndef INCMDUNKNOWN_H

#define INCMDUNKNOWN_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the UNKNOWN command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdUnknown : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdUnknown contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdUnknown(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdUnknown destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdUnknown();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the CancelSection command e retrieves the pointer
		 * @return Pointer to WFGetCalibration object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief setCommandString store the command string not recognized
		 * @param strCmd the command string
		 * **************************************************************************************************
		 */
		void setCommandString(std::string strCmd);

		/*! *************************************************************************************************
		 * @brief getCommandString retrieve the command string not recognized
		 * @return the command string
		 * **************************************************************************************************
		 */
		std::string getCommandString();

    private :

        std::string m_strCmd;

};




#endif // INCMDUNKNOWN_H
