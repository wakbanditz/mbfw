/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSaveCalibration.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSaveCalibration class.
 @details

 ****************************************************************************
*/

#include "InCmdSaveCalibration.h"
#include "WFSaveCalibration.h"
#include "WebServerAndProtocolInclude.h"

InCmdSaveCalibration::InCmdSaveCalibration(string strUsage, string strID)
	: IncomingCommand(WEB_CMD_NAME_SAVECALIBRATION_IN, strUsage, strID)
{
	m_vecSaveCalibration.clear();
}

InCmdSaveCalibration::~InCmdSaveCalibration()
{
	/* Nothing to do yet */
}

Workflow* InCmdSaveCalibration::getWorkflow(void)
{
	WFSaveCalibration* pWf = new WFSaveCalibration();
	return pWf;
}

void InCmdSaveCalibration::setCalibration(structSaveCalibration &stCalibration)
{
	printf("Type: %s, Component: %s, Motor: %s, Label: %s Value: %s\n",
		   stCalibration.strType.c_str(),
		   stCalibration.strComponent.c_str(),
		   stCalibration.strMotor.c_str(),
		   stCalibration.strLabel.c_str(),
		   stCalibration.strValue.c_str());

	m_vecSaveCalibration.push_back(stCalibration);
}


void InCmdSaveCalibration::getCalibrationToSave(vector<structSaveCalibration> &vecSaveCal)
{
	vecSaveCal = m_vecSaveCalibration;
}
