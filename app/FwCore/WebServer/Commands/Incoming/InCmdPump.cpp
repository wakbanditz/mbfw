/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdPump.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdPump class.
 @details

 ****************************************************************************
*/

#include "InCmdPump.h"
#include "WFPump.h"
#include "WebServerAndProtocolInclude.h"

InCmdPump::InCmdPump(string strUsage, string strID) :
    IncomingCommand(WEB_CMD_NAME_PUMP_IN, strUsage, strID)
{
    m_strComponent.clear();
    m_strAction.clear();
    m_intVolume = 0;
}

InCmdPump::~InCmdPump()
{
    /* Nothing to do yet */
}

Workflow* InCmdPump::getWorkflow(void)
{
    WFPump* pWf = new WFPump();
    return pWf;
}

string InCmdPump::getComponent()
{
    return m_strComponent;
}

string InCmdPump::getAction()
{
    return m_strAction;
}

uint16_t InCmdPump::getVolume()
{
    return m_intVolume;
}

void InCmdPump::setComponent(const string &strComponent)
{
    m_strComponent.assign(strComponent);
}

void InCmdPump::setAction(const string &strAction)
{
    m_strAction.assign(strAction);
}

void InCmdPump::setVolume(const uint16_t &volume)
{
    m_intVolume = volume;
}

