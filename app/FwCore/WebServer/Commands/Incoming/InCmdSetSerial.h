/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetSerial.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSetSerial class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSETSERIAL_H
#define INCMDSETSERIAL_H


#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SETSERIAL command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSetSerial : public IncomingCommand
{

	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdSetSerial constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdSetSerial(string strUsage, string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdSetSerial destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdSetSerial();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow creates and return the Workflow object linked to the command
		 * @return the Workflow object pointer
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
		 * @brief getSerialInstrument gets the Instrument Serial string
		 * @return the Instrument Serial string
		 * ************************************************************************************************************
		 */
		string getSerialInstrument() const;

		/*! ***********************************************************************************************************
		 * @brief setSerialInstrument sets the Instrument Serial string
		 * @param strSerial the Instrument Serial string
		 * ************************************************************************************************************
		 */
		void setSerialInstrument(const string& strSerial);

		/*! ***********************************************************************************************************
		 * @brief getSerialMasterboard gets the Masterboard Serial string
		 * @return the Masterboard Serial string
		 * ************************************************************************************************************
		 */
		string getSerialMasterboard() const;

		/*! ***********************************************************************************************************
		 * @brief setSerialMasterboard sets the Masterboard Serial string
		 * @param strSerial the Masterboard Serial string
		 * ************************************************************************************************************
		 */
		void setSerialMasterboard(const string& strSerial);

		/*! ***********************************************************************************************************
		 * @brief getSerialNSH gets the NSH Serial string
		 * @return the NSH Serial string
		 * ************************************************************************************************************
		 */
		string getSerialNSH() const;

		/*! ***********************************************************************************************************
		 * @brief setSerialNSH sets the NSH Serial string
		 * @param strSerial the NSH Serial string
		 * ************************************************************************************************************
		 */
		void setSerialNSH(const string& strSerial);

		/*! ***********************************************************************************************************
		 * @brief getSerialSectionA gets the SectionA Serial string
		 * @return the SectionA Serial string
		 * ************************************************************************************************************
		 */
		string getSerialSectionA() const;

		/*! ***********************************************************************************************************
		 * @brief setSerialSectionA sets the SectionA Serial string
		 * @param strSerial the SectionA Serial string
		 * ************************************************************************************************************
		 */
		void setSerialSectionA(const string& strSerial);

		/*! ***********************************************************************************************************
		 * @brief getSerialSectionA gets the SectionA Serial string
		 * @return the SectionA Serial string
		 * ************************************************************************************************************
		 */
		string getSerialSectionB() const;

		/*! ***********************************************************************************************************
		 * @brief setSerialSectionB sets the SectionA Serial string
		 * @param strSerial the SectionB Serial string
		 * ************************************************************************************************************
		 */
		void setSerialSectionB(const string& strSerial);

		/*! ***********************************************************************************************************
		 * @brief isCommandValid check if at least one of the strings has been set
		 * @return true if a string is set
		 * ************************************************************************************************************
		 */
		bool isCommandValid();

    private:

        string m_strSerialInstrument;
        string m_strSerialMasterboard;
        string m_strSerialNSH;
        string m_strSerialSectionA;
        string m_strSerialSectionB;

};


#endif // INCMDSETSERIAL_H
