/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetPosition.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetPosition class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETPOSITION_H
#define INCMDGETPOSITION_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETPOSITION command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetPosition : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdGetPosition contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdGetPosition(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdGetPosition destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdGetPosition();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the GetPosition command e retrieves the pointer
         * @return Pointer to WFGetPosition object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief getComponent Retrieves the name of the motor component to know the position of
		 * @return Name of the motor
		 * *************************************************************************************************
		 */
		string getComponent( void );

		/*! *************************************************************************************************
		 * @brief getMotor Retrieves the name of the motor to know the position of
		 * @return Name of the motor
		 * *************************************************************************************************
		 */
		string getMotor( void );

		/*! *************************************************************************************************
		 * @brief setComponent	Sets the name of the component to take in considerations for GetPosition cmd
		 * @param strComponent
		 * *************************************************************************************************
		 */
		void setComponent( const string& strComponent );

		/*! *************************************************************************************************
		 * @brief setMotor	Sets the name of the motor to take in considerations for GetPosition cmd
		 * @param strMotor
		 * *************************************************************************************************
		 */
		void setMotor( const string& strMotor );


    private:

        string	m_strComponent;
        string	m_strMotor;

};

#endif // INCMDGETPOSITION_H
