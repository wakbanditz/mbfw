/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdCaptureImage.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdCaptureImage class.
 @details

 ****************************************************************************
*/

#include "InCmdCaptureImage.h"
#include "WFCaptureImage.h"
#include "WebServerAndProtocolInclude.h"

#define MAX_IMAGE_NUM	2

InCmdCaptureImage::InCmdCaptureImage(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_CAM_CAPTURE_IMAGE_IN, strUsage, strID)
{
	m_nPicturesNum = 0;
	m_vCropEnabled.resize(MAX_IMAGE_NUM);
	fill(m_vCropEnabled.begin(), m_vCropEnabled.end(), true);
	m_vAction.resize(MAX_IMAGE_NUM);
	m_vImageQuality.resize(MAX_IMAGE_NUM);
	fill(m_vImageQuality.begin(), m_vImageQuality.end(), -1);
	m_vImageLight.resize(MAX_IMAGE_NUM);
	fill(m_vImageLight.begin(), m_vImageLight.end(), -1);
	m_vFormat.resize(MAX_IMAGE_NUM);
	m_vMotor.resize(MAX_IMAGE_NUM);
	m_vComponent.resize(MAX_IMAGE_NUM);
	m_vFocus.resize(MAX_IMAGE_NUM);
	m_vAttribute.resize(MAX_IMAGE_NUM);
	m_vCoordinates.resize(MAX_IMAGE_NUM);
	m_vRoiName.resize(MAX_IMAGE_NUM);
}

InCmdCaptureImage::~InCmdCaptureImage()
{

}

Workflow* InCmdCaptureImage::getWorkflow(void)
{
	WFCaptureImage* pWf = new WFCaptureImage();
	return pWf;
}

void InCmdCaptureImage::setCropEnabled(int liIdx, string& strEnabled)
{
	if ( liIdx >= MAX_IMAGE_NUM )	return;
	m_vCropEnabled[liIdx] = strEnabled.compare("false");
}

void InCmdCaptureImage::setAttribute(int liIdx, string& strAttr)
{
	if ( liIdx >= MAX_IMAGE_NUM )	return;
	m_vAttribute[liIdx].push_back(strAttr);
}

void InCmdCaptureImage::setMotor(int liIdx, string& strMotor)
{
	if ( liIdx >= MAX_IMAGE_NUM )	return;
	m_vMotor[liIdx].push_back(strMotor);
}

void InCmdCaptureImage::setComponent(int liIdx, string& strComponent)
{
	if ( liIdx >= MAX_IMAGE_NUM )	return;
	m_vComponent[liIdx].push_back(strComponent);
}

void InCmdCaptureImage::setPictureAction(int liIdx,  string& strAction)
{
	if ( liIdx >= MAX_IMAGE_NUM )	return;
	m_vAction[liIdx] = strAction;
}

void InCmdCaptureImage::setPredefinedRoi(int liIdx, string &strRoiName)
{
	if ( liIdx >= MAX_IMAGE_NUM )	return;
    m_vRoiName[liIdx] = strRoiName;
}

void InCmdCaptureImage::setImageFormat(int liIdx, string& strFormat)
{
	if ( liIdx >= MAX_IMAGE_NUM )	return;
	m_vFormat[liIdx] = strFormat;
}

void InCmdCaptureImage::setImageFocus(int liIdx, string& strFocus)
{
	if ( liIdx >= MAX_IMAGE_NUM )	return;
	m_vFocus[liIdx] = strFocus;
}

bool InCmdCaptureImage::setImageLight(int liIdx, string& strLight)
{
	int liLightVal;
	stringstream ss(strLight);
	ss >> liLightVal;

	if ( liIdx < 0 || liIdx > 1 )					return false;
	if ( ( liLightVal < 0) || ( liLightVal > 100) )	return false;

	m_vImageLight[liIdx] = stoi(strLight);
	return true;
}

bool InCmdCaptureImage::setImageQuality(int liIdx, string& strQualityVal)
{
	int liQualityVal;
	stringstream ss(strQualityVal);
	ss >> liQualityVal;

	if ( liIdx < 0 || liIdx > 1 )					return false;
	if ( liQualityVal < 0 || liQualityVal > 100 )	return false;

	m_vImageQuality[liIdx] = liQualityVal;
	return true;
}

bool InCmdCaptureImage::setTopVal(int liIdx, string& strTopVal)
{
	int liTopVal;
	stringstream ss(strTopVal);
	ss >> liTopVal;

	if ( liIdx < 0 || liIdx > 1 )	return false;
	if ( liTopVal < 0 )				return false;

	m_vCoordinates[liIdx].liTop = liTopVal;
	return true;
}

bool InCmdCaptureImage::setBottomVal(int liIdx, string& strBottomVal)
{
	int liBottomVal;
	stringstream ss(strBottomVal);
	ss >> liBottomVal;

	if ( liIdx < 0 || liIdx > 1 )	return false;
	if ( liBottomVal < 0 )			return false;

	m_vCoordinates[liIdx].liBottom = liBottomVal;
	return true;
}

bool InCmdCaptureImage::setLeftVal(int liIdx, string& strLeftVal)
{
	int liLeftVal;
	stringstream ss(strLeftVal);
	ss >> liLeftVal;

	if ( liIdx < 0 || liIdx > 1 )	return false;
	if ( liLeftVal < 0 )			return false;

	m_vCoordinates[liIdx].liLeft = liLeftVal;
	return true;
}

bool InCmdCaptureImage::setRightVal(int liIdx, string& strRightVal)
{
	int liRightVal;
	stringstream ss(strRightVal);
	ss >> liRightVal;

	if ( liIdx < 0 || liIdx > 1 )	return false;
	if ( liRightVal < 0 )			return false;

	m_vCoordinates[liIdx].liRight = liRightVal;
	return true;
}

void InCmdCaptureImage::setPicturesNum(int liNum)
{
	m_nPicturesNum = liNum;
}

vector<string> InCmdCaptureImage::getImageFocus()
{
	return m_vFocus;
}

vector<int> InCmdCaptureImage::getImageQuality()
{
	return m_vImageQuality;
}

vector<int> InCmdCaptureImage::getImageLight()
{
	return m_vImageLight;
}

int InCmdCaptureImage::getPicturesNum()
{
	return m_nPicturesNum;
}

vector<string> InCmdCaptureImage::getImageFormat()
{
	return m_vFormat;
}

vector<vector<string>> InCmdCaptureImage::getAttribute()
{
	return m_vAttribute;
}

vector<vector<string>> InCmdCaptureImage::getMotor()
{
	return m_vMotor;
}

vector<vector<string>> InCmdCaptureImage::getComponent()
{
	return m_vComponent;
}

vector<structCoordinates> InCmdCaptureImage::getCoordinates()
{
    return m_vCoordinates;
}

vector<string> InCmdCaptureImage::getPredefinedRoi()
{
	return m_vRoiName;
}

vector<bool> InCmdCaptureImage::getCropEnabled()
{
	return m_vCropEnabled;
}

vector<string> InCmdCaptureImage::getAction()
{
	return m_vAction;
}
