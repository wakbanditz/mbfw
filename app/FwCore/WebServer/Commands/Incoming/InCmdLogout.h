/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdLogout.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdLogout class.
 @details

 ****************************************************************************
*/

#ifndef INCMDLOGOUT_H
#define INCMDLOGOUT_H


#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the LOGOUT command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdLogout : public IncomingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdLogout constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdLogout(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief ~InCmdLogout	destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdLogout();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

};

#endif // INCMDLOGOUT_H
