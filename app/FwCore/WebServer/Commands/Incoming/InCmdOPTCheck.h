/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdOPTCheck.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdOPTCheck class.
 @details

 ****************************************************************************
*/

#ifndef INCMDOPTCHECK_H
#define INCMDOPTCHECK_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the OPT_CHECK command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdOPTCheck : public IncomingCommand
{
    public:

        /*! *************************************************************************************************
         * @brief InCmdOPTheck contructor
         * @param strUsage	the connection string associated to the received command
         * @param strID the CommandId string
         * **************************************************************************************************
         */
        InCmdOPTCheck(string strUsage, string strID);

        /*! *************************************************************************************************
         * @brief InCmdOPTheck destructor
         * **************************************************************************************************
         */
        virtual ~InCmdOPTCheck();


        /*! *************************************************************************************************
         * @brief getWorkflow Creates the Workflow associated to the InCmdOPTheck command e retrieves
         *		  the pointer
         * @return Pointer to WFOPTCheck object
         * **************************************************************************************************
         */
        Workflow* getWorkflow(void);

        /*! *************************************************************************************************
         * @brief isSectionEnabled checks if the section has to be used
         * @param section the section index
         * @return true if section enabled
         * *************************************************************************************************
         */
        bool isSectionEnabled(uint8_t section);

        /*! *************************************************************************************************
         * @brief isSlotEnabled checks if the section-slot has to be used
         * @param section the section index
         * @param slot the slot index
         * @return true if section-slot enabled
         * *************************************************************************************************
         */
        bool isSlotEnabled(uint8_t section, uint8_t slot);

        /*! *************************************************************************************************
         * @brief getTarget Retrieves the target value of OPT
         * @return OPT target
         * *************************************************************************************************
         */
        int getTarget(void);

        /*! *************************************************************************************************
         * @brief getTolerance Retrieves the tolerance value of OPT
         * @return OPT tolerance
         * *************************************************************************************************
         */
        int getTolerance(void);

        /*! *************************************************************************************************
         * @brief setSlot enable the section-slot
         * @param strSection the section index as string
         * @param strSlot the slot index as string
         * *************************************************************************************************
         */
        void setSlot(const string& strSection, const string& strSlot);

        /*! *************************************************************************************************
         * @brief setTarget	Sets the OPT target value
         * @param target OPT target value
         * *************************************************************************************************
         */
        void setTarget(int target);

        /*! *************************************************************************************************
         * @brief setTolerance	Sets the OPT tolerance value
         * @param tolerance OPT tolerance value
         * *************************************************************************************************
         */
        void setTolerance(int tolerance);

    private:

        bool m_bSectionSlotEnable[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];
        int	m_intTarget, m_intTolerance;

};

#endif // INCMDOPTCHECK_H
