/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdMove.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdMove class.
 @details

 ****************************************************************************
*/

#include "InCmdMove.h"
#include "WFMove.h"
#include "WebServerAndProtocolInclude.h"

InCmdMove::InCmdMove(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_MOVE_IN, strUsage, strID)
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strMove.clear();
	m_strMoveValue.clear();
}

InCmdMove::~InCmdMove()
{
	/* Nothing to do yet */
}

Workflow* InCmdMove::getWorkflow(void)
{
	WFMove* pWf = new WFMove();
	return pWf;
}

string InCmdMove::getComponent()
{
	return m_strComponent;
}

string InCmdMove::getMotor()
{
	return m_strMotor;
}

string InCmdMove::getMove()
{
	return m_strMove;
}

string InCmdMove::getMoveValue()
{
	return m_strMoveValue;
}

void InCmdMove::setComponent(const string &strComponent)
{
	m_strComponent.assign(strComponent);
}

void InCmdMove::setMotor(const string &strMotor)
{
	m_strMotor.assign(strMotor);
}

void InCmdMove::setMove(const string &strMove, const string &strValue)
{
	m_strMove.assign(strMove);
	m_strMoveValue.assign(strValue);
}
