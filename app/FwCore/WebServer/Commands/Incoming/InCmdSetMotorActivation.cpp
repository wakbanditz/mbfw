/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetMotorActivation.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSetMotorActivation class.
 @details

 ****************************************************************************
*/

#include "InCmdSetMotorActivation.h"
#include "WFSetMotorActivation.h"
#include "WebServerAndProtocolInclude.h"

InCmdSetMotorActivation::InCmdSetMotorActivation(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_SETMOTORACTIVATION_IN, strUsage, strID)
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strActivation.clear();
}

InCmdSetMotorActivation::~InCmdSetMotorActivation()
{
	/* Nothing to do yet */
}

Workflow* InCmdSetMotorActivation::getWorkflow(void)
{
	WFSetMotorActivation* pWf = new WFSetMotorActivation();

	return pWf;
}

void InCmdSetMotorActivation::setComponent(const string &strComponent)
{
	m_strComponent.assign(strComponent);
}

string InCmdSetMotorActivation::getComponent( void )
{
	return m_strComponent;
}

void InCmdSetMotorActivation::setMotor(const string &strMotor)
{
	m_strMotor.assign(strMotor);
}

string InCmdSetMotorActivation::getMotor( void )
{
	return m_strMotor;
}

void InCmdSetMotorActivation::setActivation(const string &strActivation)
{
	m_strActivation.assign(strActivation);
}

string InCmdSetMotorActivation::getActivation( void )
{
	return m_strActivation;
}
