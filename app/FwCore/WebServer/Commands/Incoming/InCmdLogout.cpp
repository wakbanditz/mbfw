/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdLogout.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdLogout class.
 @details

 ****************************************************************************
*/

#include "InCmdLogout.h"
#include "WFLogout.h"
#include "WebServerAndProtocolInclude.h"

InCmdLogout::InCmdLogout(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_LOGOUT_IN, strUsage, strID)
{
	// no login is (obviously) requested
}

InCmdLogout::~InCmdLogout()
{

}

Workflow* InCmdLogout::getWorkflow(void)
{
	WFLogout* pWf = new WFLogout();
	return pWf;
}

