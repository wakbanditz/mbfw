/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetGlobalSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetGlobalSettings class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETGLOBALSETTINGS_H
#define INCMDGETGLOBALSETTINGS_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETGLOBALSETTINGS command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetGlobalSettings : public IncomingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdGetGlobalSettings constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdGetGlobalSettings(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdGetGlobalSettings constructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdGetGlobalSettings();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);
};


#endif // INCMDGETGLOBALSETTINGS_H
