/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetMaintenanceMode.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSetMaintenanceMode class.
 @details

 ****************************************************************************
*/
#ifndef INCMDSETMAINTENANCEMODE_H

#define INCMDSETMAINTENANCEMODE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SETMAINTENANCEMODE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSetMaintenanceMode : public IncomingCommand
{

	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdSetMaintenanceMode constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdSetMaintenanceMode(string strUsage, string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdSetMaintenanceMode destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdSetMaintenanceMode();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow creates and return the Workflow object linked to the command
		 * @return the Workflow object pointer
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
		 * @brief getActivation gets the Activation string
		 * @return the Activation string
		 * ************************************************************************************************************
		 */
		string getActivation() const;

		/*! ***********************************************************************************************************
		 * @brief setActivation sets the Activation string
		 * @param strActivation the Activation string
		 * ************************************************************************************************************
		 */
		void setActivation(const string& strActivation);


    private:

        string m_strActivation;

};


#endif // INCMDSETMAINTENANCEMODE_H
