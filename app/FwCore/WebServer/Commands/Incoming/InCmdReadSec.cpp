/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdReadSec.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdReadSec class.
 @details

 ****************************************************************************
*/

#include "InCmdReadSec.h"
#include "WFReadSec.h"
#include "WebServerAndProtocolInclude.h"
#include "Payload.h"


InCmdReadSec::InCmdReadSec(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_READSEC_IN, strUsage, strID)
{
	int i;

	for(i = 0; i < SCT_NUM_TOT_SLOTS; i++)
	{
		m_stripReadSection[i].m_bEnabled = false;
		m_stripReadSection[i].m_bStripCheck = false;
		m_stripReadSection[i].m_bSprCheck = false;
		m_stripReadSection[i].m_bSubstrateCheck = false;
		m_stripReadSection[i].m_bSprImage = false;
		m_stripReadSection[i].m_bStripImage = false;
		m_stripReadSection[i].m_SampleDetection.m_bCheckX0 = false;
		m_stripReadSection[i].m_SampleDetection.m_bCheckX3 = false;
		m_stripReadSection[i].m_SampleDetection.m_nSettingsX0 = -1;
		m_stripReadSection[i].m_SampleDetection.m_nSettingsX3 = -1;
		m_stripReadSection[i].m_SampleDetection.m_bImageX0 = false;
		m_stripReadSection[i].m_SampleDetection.m_bImageX3 = false;
	}
	m_sectionRead = SCT_NONE_ID;
	m_vSettings.clear();
}

InCmdReadSec::~InCmdReadSec()
{

}

Workflow* InCmdReadSec::getWorkflow(void)
{
	WFReadSec* pWf = new WFReadSec();
	return pWf;
}

int InCmdReadSec::getStripIndex(std::string strStrip)
{
	int index = -1;

	if(strcmp(strStrip.c_str(), XML_VALUE_STRIP_1) == 0)
	{
		index = 0;
	}
	else if(strcmp(strStrip.c_str(), XML_VALUE_STRIP_2) == 0)
	{
		index = 1;
	}
	else if(strcmp(strStrip.c_str(), XML_VALUE_STRIP_3) == 0)
	{
		index = 2;
	}
	else if(strcmp(strStrip.c_str(), XML_VALUE_STRIP_4) == 0)
	{
		index = 3;
	}
	else if(strcmp(strStrip.c_str(), XML_VALUE_STRIP_5) == 0)
	{
		index = 4;
	}
	else if(strcmp(strStrip.c_str(), XML_VALUE_STRIP_6) == 0)
	{
		index = 5;
	}
	return index;
}

bool InCmdReadSec::setSection(std::string strSection)
{
	if(strcmp(strSection.c_str(), XML_VALUE_SECTION_A) == 0)
	{
		m_sectionRead = SCT_A_ID;
	}
	else if(strcmp(strSection.c_str(), XML_VALUE_SECTION_B) == 0)
	{
		m_sectionRead = SCT_B_ID;
	}
	return (m_sectionRead != SCT_NONE_ID);
}

int InCmdReadSec::getSection()
{
	return m_sectionRead;
}

void InCmdReadSec::setSettings(string& strSettings, string& strId)
{
	string strDecodedSettings("");
	decodeDataBase64(strSettings, strDecodedSettings);

	cout << endl << strDecodedSettings << endl << endl;

	m_vSettings.push_back(make_pair(strId, strDecodedSettings));
}

vector<pair<string, string>>& InCmdReadSec::getSettings()
{
	return m_vSettings;
}

bool InCmdReadSec::setStrip(std::string strStrip)
{
	int index = getStripIndex(strStrip);

	if ( index == -1 )
	{
		return false;
	}

	m_stripReadSection[index].m_bEnabled = true;
	return true;
}

bool InCmdReadSec::setStripCheck(string strStrip, string strValAttr)
{
	int index = getStripIndex(strStrip);

	if(index == -1)
	{
		return false;
	}

	if ( ! strcmp(strValAttr.c_str(), XML_VALUE_1) )
	{
		m_stripReadSection[index].m_bStripCheck = true;
	}
	return true;
}

bool InCmdReadSec::setStripImage(string strStrip, string strValAttr)
{
	int index = getStripIndex(strStrip);
	if ( index == -1 )	return false;

	if ( ! strcmp(strValAttr.c_str(), XML_VALUE_1) )
	{
		m_stripReadSection[index].m_bStripImage = true;
	}
	return true;
}

bool InCmdReadSec::setSprImage(string strStrip, string strValAttr)
{
	int index = getStripIndex(strStrip);
	if ( index == -1 )	return false;

	if ( ! strcmp(strValAttr.c_str(), XML_VALUE_1) )
	{
		m_stripReadSection[index].m_bSprImage = true;
	}
	return true;
}

bool InCmdReadSec::setSampleCheck(string strStrip, string strWell, string strValAttr)
{
	if ( strValAttr.empty() )
	{
		strValAttr = "1"; // default
	}

	int index = getStripIndex(strStrip);
	if ( index == -1 )		return false;

	bool bIsEnabled = ( strcmp(strValAttr.c_str(), XML_VALUE_0) ) ? true : false;
	if ( ! strcmp(strWell.c_str(), XML_VALUE_WELL_X0_ID) )
	{
		m_stripReadSection[index].m_SampleDetection.m_bCheckX0 = bIsEnabled;
	}
	else
	{
		m_stripReadSection[index].m_SampleDetection.m_bCheckX3 = bIsEnabled;
	}
	return true;
}

bool InCmdReadSec::setSampleDetectionSettingsId(string strStrip, string strWell, string strValAttr)
{
	if ( strValAttr.empty() )	return false;

	int index = getStripIndex(strStrip);
	if ( index == -1 )		return false;

	int liSettingId = stoi(strValAttr);
	if ( ! strcmp(strWell.c_str(), XML_VALUE_WELL_X0_ID) )
	{
		m_stripReadSection[index].m_SampleDetection.m_nSettingsX0 = liSettingId;
	}
	else
	{
		m_stripReadSection[index].m_SampleDetection.m_nSettingsX3 = liSettingId;
	}
	return true;
}

bool InCmdReadSec::setSampleImage(string strStrip, string strWell, string strValAttr)
{
	int index = getStripIndex(strStrip);
	if ( index == -1 )		return false;

	bool bIsEnabled = ( strcmp(strValAttr.c_str(), XML_VALUE_0) ) ? true : false;
	if ( ! strcmp(strWell.c_str(), XML_VALUE_WELL_X0_ID) )
	{
		m_stripReadSection[index].m_SampleDetection.m_bImageX0 = bIsEnabled;
	}
	else
	{
		m_stripReadSection[index].m_SampleDetection.m_bImageX3 = bIsEnabled;
	}
	return true;
}

bool InCmdReadSec::setSprCheck(string strStrip, string strValAttr)
{
	int index = getStripIndex(strStrip);

	if ( index == -1 )
	{
		return false;
	}

	if ( ! strcmp(strValAttr.c_str(), XML_VALUE_1) )
	{
		m_stripReadSection[index].m_bSprCheck = true;
	}
	return true;
}

bool InCmdReadSec::setSubstrateCheck(string strStrip, string strValAttr)
{
	int index = getStripIndex(strStrip);

	if ( index == -1 )
	{
		return false;
	}
	if ( ! strcmp(strValAttr.c_str(), XML_VALUE_1) )
	{
		m_stripReadSection[index].m_bSubstrateCheck = true;
	}
	return true;
}

stripReadSection_tag * InCmdReadSec::getDataStructure()
{
	return &m_stripReadSection[0];
}

bool InCmdReadSec::isAtLeastOneStripEnabled()
{
	if ( m_sectionRead == SCT_NONE_ID )				return false;

	for ( int i = 0; i < SCT_NUM_TOT_SLOTS; i++ )
	{
		if ( m_stripReadSection[i].m_bEnabled )		return true;
	}

	return false;
}

