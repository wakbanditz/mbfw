/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    IncomingCommand.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the IncomingCommand class.
 @details

 ****************************************************************************
*/
#ifndef INCOMINGCOMMAND_H

#define INCOMINGCOMMAND_H

#include "CommonInclude.h"
#include "SectionInclude.h"

#include <string>
#include <vector>

#include "StrX.h"
#include "Loggable.h"

using namespace std;

class Workflow;
class WebServerConnection;

/*! ***********************************************************************************************************
 * @brief base class for all incoming commands
 * ************************************************************************************************************
 */
class IncomingCommand
{

	public:

		/*! ***********************************************************************************************************
		 * @brief IncomingCommand	default constructor
		 * @param strName			the command name as recognized by a ProtocolValidator
		 * @param strUsage			the USAGE attribute of the received command
		 * @param strID				the CommandId attribute of the received command
		 * ************************************************************************************************************
		 */
		IncomingCommand(std::string strName, std::string strUsage = "", std::string strID = "");

		/*! ***********************************************************************************************************
		 * @brief ~IncomingCommand	default destructor
		 * ************************************************************************************************************
		 */
		virtual ~IncomingCommand();

		/*! ***********************************************************************************************************
		 * @brief readCommandConfiguration	open and read a file named as m_strName.cfg in the working folder
		 *							and extracts the lines formatted as:
		 *							login_requested = true / false to set the m_bLoginRequested value (default = true)
		 *							states = STATE1, STATE2, etc or ALL to build the state mask that identifies
		 *								the Module/Device states in which the command is allowed
		 * @return					true if file is correctly opened, false otherwise
		 * ************************************************************************************************************
		 */
		bool readCommandConfiguration();

		/*! ***********************************************************************************************************
		 * @brief associateStatus	search the single status string read from the m_strName.cfg file in the status
		 *							string array as define in StatusInclude.h and, if found, set the status bit mask
		 *							with the bit corresponding open (defined in StatusInclude.h)
		 * @param strRead			the status string read from file
		 * @return					true if association is succesfull, false otherwise
		 * ************************************************************************************************************
		 */
		bool associateStatus(const std::__cxx11::string strRead);

		/*! ***********************************************************************************************************
		 * @brief getLoginRequested		get if the login is necessary to execute the current command
		 * @return					true if login is requested (default), false if not (LOGIN, LOGOUT, HELLO)
		 * ************************************************************************************************************
		 */
		bool getLoginRequested();

		/*! ***********************************************************************************************************
		 * @brief getMaskStates		get the allowable states bitmask of the command
		 * @return					the value of m_intMaskStates
		 * ************************************************************************************************************
		 */
		unsigned short getMaskStates();

		/*! ***********************************************************************************************************
		 * @brief setTargetState	set the device state after the execution of the command
		 *						this override is used when the target status depends on command parameter
		 *						(DISABLE or SLEEP)
		 * @param	strState	the value of m_strTargetState
		 * ************************************************************************************************************
		 */
		void setTargetState(std::string strState);

		/*! ***********************************************************************************************************
		 * @brief getTargetState	get the device state after the execution of the
		 * @return					the value of m_strTargetState as read from command file
		 * ************************************************************************************************************
		 */
		std::string getTargetState();

		/*! ***********************************************************************************************************
		 * @brief getCallbackUrl	get the url to which address the reply (used also as login identifier)
		 * @return					the callback url as string
		 * ************************************************************************************************************
		 */
		std::string getCallbackUrl();

		/*! ***********************************************************************************************************
		 * @brief getUsage	get the USAGE (connection identifier COMMERCIAL vs SERVICE) received from command
		 * @return					the USAGE as string
		 * ************************************************************************************************************
		 */
		std::string getUsage();

		/*! ***********************************************************************************************************
		 * @brief getID	get the ID (command index, optional) received from command
		 * @return					the ID as string
		 * ************************************************************************************************************
		 */
		std::string getID();

		/*! ***********************************************************************************************************
		 * @brief getConnection	get the Connection allowed (ALL vs COMMERCIAL vs SERVICE) as read from command file
		 * @return					the m_strConnection as string
		 * ************************************************************************************************************
		 */
		std::string getConnection();

		/*! ***********************************************************************************************************
		 * @brief isConnectionAllowed	compares the connection received in the command with the values read from file
		 * @return		true if the connection received is among the connections allowed, false otherwise
		 * ************************************************************************************************************
		 */
		bool isConnectionAllowed();

		/*! ***********************************************************************************************************
		 * @brief setWebServerConnection	set the connection object to be further forwarded to other objects that
		 *									need it, for instance an Outgoing Command
		 * @param rWebServerConnection		reference to a WebServerConnection object
		 * ************************************************************************************************************
		 */
		void setWebServerConnection(WebServerConnection &rWebServerConnection);

		/*! ***********************************************************************************************************
		 * @brief getWebServerConnection	retrieves the internal WebServerConnection object
		 * @return							reference to the internal WebServerConnection object
		 * ************************************************************************************************************
		 */
		WebServerConnection* getWebServerConnection(void);

		/*! ***********************************************************************************************************
		 * @brief execute		routine that executes the command by doing the following steps:
		 *							- retrieve the Workflow associated to this command by calling the getWorkflow()
		 *							  method, implemented in every son of this;
		 *							- call the Workflow::setup() routine in order to retrieve a synchronous
		 *							  OutgoingCommand to be sent to the client (i.e. accepted/rejected or a generic
		 *							  synchronous answer) and a possible Operation to be queued to the
		 *							  OperationManager
		 *							- send the synchronous OutgoingCommand to the client
		 *							- queue the Operation to the OperationManager, it's up to the OperationManager
		 *							  to destroy it
		 *							- destroy the workflow
		 * @return				true if at least the OutgoingCommand has been correctly sent to the client, false
		 *						otherwise
		 * ************************************************************************************************************
		 */
		bool execute(void);

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	every IncomingCommand must have its own Workflow that can be retrieved from anybody
		 *						who wants to execute it. This routine creates the Workflow object associated to this
		 *						command, the user must deallocate it.
		 * @return				pointer to a new created Workflow
		 * ************************************************************************************************************
		 */
		virtual Workflow* getWorkflow(void) = 0;

    private:

        std::string m_strName;
        std::string m_strUsage;
        std::string m_strID;
        bool m_bLoginRequested;
        WebServerConnection* m_pWebServerConnection;
        std::string m_strCallBackUrl;
        unsigned short m_intMaskStates;
        std::string m_strTargetState;
        std::string m_strConnection;

};

#endif // INCOMINGCOMMAND_H
