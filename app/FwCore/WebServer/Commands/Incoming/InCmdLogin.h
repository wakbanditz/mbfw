/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdLogin.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdLogin class.
 @details

 ****************************************************************************
*/

#ifndef INCMDLOGIN_H
#define INCMDLOGIN_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the LOGIN command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdLogin : public IncomingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdLogin constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdLogin(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief ~InCmdLogin	destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdLogin();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
		 * @brief getId	to get the ID login string extracted from the input command
		 * @return the ID as string
		 * ************************************************************************************************************
		 */
		std::string getId() const;

		/*! ***********************************************************************************************************
		 * @brief setId	to set the ID login string extracted from the input command
		 * @param  strId the ID as string
		 * ************************************************************************************************************
		 */
		void setId(const std::string& strId);

		/*! ***********************************************************************************************************
		 * @brief getId	to get the PASSWORD login string extracted from the input command
		 * @return the PASSWORD as string
		 * ************************************************************************************************************
		 */
		std::string getPwd() const;

		/*! ***********************************************************************************************************
		 * @brief setPwd to set the PASSWORD login string extracted from the input command
		 * @param  strPwd the PASSWORD as string
		 * ************************************************************************************************************
		 */
		void setPwd(const std::string& strPwd);

    private:
        std::string m_strId;
        std::string m_strPwd;

};

#endif // INCMDLOGIN_H
