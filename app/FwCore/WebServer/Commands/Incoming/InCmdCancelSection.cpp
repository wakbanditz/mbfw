/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdCancelSection.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdCancelSection class.
 @details

 ****************************************************************************
*/

#include "InCmdCancelSection.h"
#include "WFCancelSection.h"
#include "WebServerAndProtocolInclude.h"

InCmdCancelSection::InCmdCancelSection(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_CANCELSECTION_IN, strUsage, strID)
{
	m_section = SCT_NUM_TOT_SECTIONS;
}

InCmdCancelSection::~InCmdCancelSection()
{
}

Workflow* InCmdCancelSection::getWorkflow(void)
{
	WFCancelSection* pWf = new WFCancelSection();
	return pWf;
}

bool InCmdCancelSection::setSection(string strSection)
{
	if(strcmp(strSection.c_str(), XML_VALUE_SECTION_A) == 0)
	{
		m_section = SCT_A_ID;
	}
	else if(strcmp(strSection.c_str(), XML_VALUE_SECTION_B) == 0)
	{
		m_section = SCT_B_ID;
	}
	else
	{
		return false;
	}
	return true;
}

uint8_t InCmdCancelSection::getSection()
{
	return m_section;
}


