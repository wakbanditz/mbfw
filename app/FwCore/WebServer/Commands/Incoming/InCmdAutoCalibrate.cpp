/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdAutoCalibrate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdAutoCalibrate class.
 @details

 ****************************************************************************
*/

#include "InCmdAutoCalibrate.h"
#include "WFAutoCalibrate.h"
#include "WebServerAndProtocolInclude.h"

InCmdAutoCalibrate::InCmdAutoCalibrate(string strUsage, string strID) :
					IncomingCommand(WEB_CMD_NAME_AUTO_CALIBRATE_IN, strUsage, strID)
{

}

InCmdAutoCalibrate::~InCmdAutoCalibrate()
{

}

Workflow* InCmdAutoCalibrate::getWorkflow()
{
	WFAutoCalibrate* pWf = new WFAutoCalibrate();
	return pWf;
}
