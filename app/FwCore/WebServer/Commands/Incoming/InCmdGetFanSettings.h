/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetFanSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetFanSettings class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETFANSETTINGS_H
#define INCMDGETFANSETTINGS_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETFANSETTINGS command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetFanSettings : public IncomingCommand
{

	public:

		/*! *************************************************************************************************
		 * @brief InCmdGetFanSettings contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdGetFanSettings(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdGetFanSettings destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdGetFanSettings();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the CancelSection command e retrieves the pointer
		 * @return Pointer to WFGetFanSettings object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);


};


#endif // INCMDGETFANSETTINGS_H
