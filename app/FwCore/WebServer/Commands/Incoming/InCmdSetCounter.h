/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetCounter.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSetCounter class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSETCOUNTER_H
#define INCMDSETCOUNTER_H


#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SETCOUNTER command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSetCounter : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdSetCounter contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSetCounter(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSetCounter destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSetCounter();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the InCmdAirCalibrate command e retrieves
		 *		  the pointer
         * @return Pointer to WFSetCounter object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief getCounter Retrieves the counter label
		 * @return Name of the label
		 * *************************************************************************************************
		 */
		string getCounter(void);

		/*! *************************************************************************************************
		 * @brief setCounter	Sets the name of the counter
		 * @param strCounter the label string
		 * *************************************************************************************************
		 */
		void setCounter(const string& strCounter);

		/*! *************************************************************************************************
		 * @brief getValue Retrieves the counter label
		 * @return value of the label
		 * *************************************************************************************************
		 */
		string getValue(void);

		/*! *************************************************************************************************
		 * @brief setValue	Sets the value of the counter
		 * @param strValue the value string
		 * *************************************************************************************************
		 */
		void setValue(const string& strValue);

    private:

        string	m_strCounter, m_strValue;

};

#endif // INCMDSETCOUNTER_H
