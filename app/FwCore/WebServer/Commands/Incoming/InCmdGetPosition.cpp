/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetPosition.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetPosition class.
 @details

 ****************************************************************************
*/

#include "InCmdGetPosition.h"
#include "WFGetPosition.h"
#include "WebServerAndProtocolInclude.h"

InCmdGetPosition::InCmdGetPosition(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_GETPOSITION_IN, strUsage, strID)
{
	m_strComponent.clear();
	m_strMotor.clear();
}

InCmdGetPosition::~InCmdGetPosition()
{
	/* Nothing to do yet */
}

Workflow* InCmdGetPosition::getWorkflow(void)
{
	WFGetPosition* pWf = new WFGetPosition();
	return pWf;
}

string InCmdGetPosition::getComponent()
{
	return m_strComponent;
}

string InCmdGetPosition::getMotor()
{
	return m_strMotor;
}


void InCmdGetPosition::setComponent(const string &strComponent)
{
	m_strComponent.assign(strComponent);
}

void InCmdGetPosition::setMotor(const string &strMotor)
{
	m_strMotor.assign(strMotor);
}
