/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSaveCameraRoi.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSaveCameraRoi class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSAVECAMERAROI_H
#define INCMDSAVECAMERAROI_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SAVECAMERAROI command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSaveCameraRoi : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdSaveCameraRoi contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSaveCameraRoi(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSHCalibrate destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSaveCameraRoi();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the Save camera ROI command e retrieves the pointer
         * @return Pointer to WFSaveCameraROI object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		void setTarget(string& strTarget);

		void setTop(string& strTop);

		void setBottom(string& strBottom);

		void setLeft(string& strLeft);

		void setRight(string& strRight);

		vector<string>& getTarget(void);

		vector<string>& getTop(void);

		vector<string>& getBottom(void);

		vector<string>& getLeft(void);

		vector<string>& getRight(void);

    private:

        vector<string> m_vTarget;
        vector<string> m_vTop;
        vector<string> m_vBottom;
        vector<string> m_vLeft;
        vector<string> m_vRight;

};

#endif // INCMDSAVECAMERAROI_H
