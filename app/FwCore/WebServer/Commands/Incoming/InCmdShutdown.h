/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdShutdown.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdShutdown class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSHUTDOWN_H
#define INCMDSHUTDOWN_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SHUTDOWN command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdShutdown : public IncomingCommand
{

	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdShutdown constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdShutdown(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief ~InCmdShutdown	destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdShutdown();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);


};

#endif // INCMDSHUTDOWN_H
