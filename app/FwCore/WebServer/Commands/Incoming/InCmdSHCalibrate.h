/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSHCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSHCalibrate class.
 @details

 ****************************************************************************
*/
#ifndef INCMDSHCALIBRATE_H

#define INCMDSHCALIBRATE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SHCALIBRATE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSHCalibrate : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdSHCalibrate contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSHCalibrate(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSHCalibrate destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSHCalibrate();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the SH calibrate command e retrieves the pointer
         * @return Pointer to WFSHCalibrate object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
		 * @brief setSection set the section involved
		 * @param strSection the string A/B
		 * ************************************************************************************************************
		 */
		void setSection(string& strSection);

		/*! ***********************************************************************************************************
		 * @brief getSection get the section involved
		 * @return the section SCT_A_ID / SCT_B_ID
		 * ************************************************************************************************************
		 */
		string getSection(void);

		/*! ***********************************************************************************************************
         * @brief setSlot set the slot involved
         * @param strSlot
		 * ************************************************************************************************************
		 */
		void setSlot(string& strSlot);

		/*! ***********************************************************************************************************
         * @brief getSlot get the slot involved
         * @return the slot string
		 * ************************************************************************************************************
		 */
		string getSlot(void);

    private:

        string m_strSection;
        string m_strSlot;

};

#endif // INCMDSHCALIBRATE_H
