/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetCalibration.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetCalibration class.
 @details

 ****************************************************************************
*/

#include "InCmdGetCalibration.h"
#include "WFGetCalibration.h"
#include "WebServerAndProtocolInclude.h"

InCmdGetCalibration::InCmdGetCalibration(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_GETCALIBRATION_IN, strUsage, strID)
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strLabel.clear();
}

InCmdGetCalibration::~InCmdGetCalibration()
{
	/* Nothing to do yet */
}

Workflow* InCmdGetCalibration::getWorkflow(void)
{
	WFGetCalibration* pWf = new WFGetCalibration();
	return pWf;
}

string InCmdGetCalibration::getComponent()
{
	return m_strComponent;
}

string InCmdGetCalibration::getMotor()
{
	return m_strMotor;
}

string InCmdGetCalibration::getLabel()
{
	return m_strLabel;
}

void InCmdGetCalibration::setComponent(const string &strComponent)
{
	m_strComponent.assign(strComponent);
}

void InCmdGetCalibration::setMotor(const string &strMotor)
{
	m_strMotor.assign(strMotor);
}

void InCmdGetCalibration::setLabel(const string &strLabel)
{
	m_strLabel.assign(strLabel);
}
