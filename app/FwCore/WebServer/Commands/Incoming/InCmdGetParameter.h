/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetParameter.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetParameter class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETPARAMETER_H
#define INCMDGETPARAMETER_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETPARAMETER command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetParameter : public IncomingCommand
{
	public:
		/*! *************************************************************************************************
         * @brief InCmdGetParameter contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdGetParameter(string strUsage, string strID);

		/*! *************************************************************************************************
         * @brief InCmdGetParameter destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdGetParameter();


		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the command e retrieves the pointer to that WF
         * @return Pointer to WFGetParameter object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief getComponent Retrieves the name of the component to request
		 * @return Name of the component
		 * *************************************************************************************************
		 */
		string getComponent( void );

		/*! *************************************************************************************************
		 * @brief getMotor Retrieves the name of the motor to request
		 * @return Name of the motor
		 * *************************************************************************************************
		 */
		string getMotor( void );


		/*! *************************************************************************************************
		 * @brief setComponent	Sets the name of the component to take in considerations for GetPosition cmd
		 * @param strComponent
		 * *************************************************************************************************
		 */
		void setComponent( const string& strComponent );

		/*! *************************************************************************************************
		 * @brief setMotor	Sets the name of the motor to take in considerations for the cmd
		 * @param strMotor
		 * *************************************************************************************************
		 */
		void setMotor( const string& strMotor );

    private:
        string	m_strComponent;
        string	m_strMotor;

};

#endif // INCMDGETPARAMETER_H
