/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdPump.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdPump class.
 @details

 ****************************************************************************
*/

#ifndef INCMDPUMP_H
#define INCMDPUMP_H

#include "IncomingCommand.h"

class Workflow;


/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the PUMP command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdPump : public IncomingCommand
{

    public:

        /*! *************************************************************************************************
         * @brief InCmdPump contructor
         * @param strUsage	the connection string associated to the received command
         * @param strID the CommandId string
         * **************************************************************************************************
         */
        InCmdPump(string strUsage, string strID);

        /*! *************************************************************************************************
         * @brief InCmdPump destructor
         * **************************************************************************************************
         */
        virtual ~InCmdPump();

        /*! *************************************************************************************************
         * @brief getWorkflow Creates the Workflow associated to the Move command e retrieves the pointer
         * @return Pointer to WFMove object
         * **************************************************************************************************
         */
        Workflow* getWorkflow(void);

        /*! *************************************************************************************************
         * @brief getComponent Retrieves the name of the component to move
         * @return Name of the component
         * *************************************************************************************************
         */
        string getComponent( void );

        /*! *************************************************************************************************
         * @brief getAction Retrieves the action for the pump
         * @return action fot the pump
         * *************************************************************************************************
         */
        string getAction( void );

        /*! *************************************************************************************************
         * @brief getVolume Retrieves the pump volume
         * @return pump volume
         * *************************************************************************************************
         */
        uint16_t getVolume( void );

        /*! *************************************************************************************************
         * @brief setComponent	Sets the name of the component to take in considerations for Pump cmd
         * @param strComponent
         * *************************************************************************************************
         */
        void setComponent( const string& strComponent );

        /*! *************************************************************************************************
         * @brief setAction	Sets the pump action
         * @param strAction the action to perform
         * *************************************************************************************************
         */
        void setAction( const string& strAction );

        /*! *************************************************************************************************
         * @brief setMove	Sets the pump volume
         * @param volume the pump volume
         * *************************************************************************************************
         */
        void setVolume(const uint16_t& volume);

    private:

        string	m_strComponent;
        string	m_strAction;
        uint16_t	m_intVolume;

};

#endif // INCMDPUMP_H
