/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetSerial.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSetSerial class.
 @details

 ****************************************************************************
*/

#include "InCmdSetSerial.h"
#include "WFSetSerial.h"
#include "WebServerAndProtocolInclude.h"

InCmdSetSerial::InCmdSetSerial(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_SETSERIAL_IN, strUsage, strID)
{
	m_strSerialInstrument.clear();
	m_strSerialMasterboard.clear();
	m_strSerialNSH.clear();
	m_strSerialSectionA.clear();
	m_strSerialSectionB.clear();
}

InCmdSetSerial::~InCmdSetSerial()
{

}

Workflow* InCmdSetSerial::getWorkflow(void)
{
	WFSetSerial* pWf = new WFSetSerial();
	return pWf;
}

string InCmdSetSerial::getSerialInstrument() const
{
	return m_strSerialInstrument;
}

void InCmdSetSerial::setSerialInstrument(const string& strSerial)
{
	m_strSerialInstrument.assign(strSerial);
}

string InCmdSetSerial::getSerialMasterboard() const
{
	return m_strSerialMasterboard;
}

void InCmdSetSerial::setSerialMasterboard(const string& strSerial)
{
	m_strSerialMasterboard.assign(strSerial);
}

string InCmdSetSerial::getSerialNSH() const
{
	return m_strSerialNSH;
}

void InCmdSetSerial::setSerialNSH(const string& strSerial)
{
	m_strSerialNSH.assign(strSerial);
}

string InCmdSetSerial::getSerialSectionA() const
{
	return m_strSerialSectionA;
}

void InCmdSetSerial::setSerialSectionA(const string& strSerial)
{
	m_strSerialSectionA.assign(strSerial);
}

string InCmdSetSerial::getSerialSectionB() const
{
	return m_strSerialSectionB;
}

void InCmdSetSerial::setSerialSectionB(const string& strSerial)
{
	m_strSerialSectionB.assign(strSerial);
}

bool InCmdSetSerial::isCommandValid()
{
	if(m_strSerialInstrument.empty() &&
			m_strSerialMasterboard.empty() &&
			m_strSerialNSH.empty() &&
			m_strSerialSectionA.empty() &&
			m_strSerialSectionB.empty() )
	{
		return false;
	}

	return true;
}

