/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetGlobalSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetGlobalSettings class.
 @details

 ****************************************************************************
*/

#include "InCmdGetGlobalSettings.h"
#include "WFGetGlobalSettings.h"
#include "WebServerAndProtocolInclude.h"

InCmdGetGlobalSettings::InCmdGetGlobalSettings(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_GET_GLOBAL_SETTINGS_IN, strUsage, strID)
{

}


InCmdGetGlobalSettings::~InCmdGetGlobalSettings()
{

}

Workflow* InCmdGetGlobalSettings::getWorkflow(void)
{
	WFGetGlobalSettings* pWf = new WFGetGlobalSettings();
	return pWf;
}

