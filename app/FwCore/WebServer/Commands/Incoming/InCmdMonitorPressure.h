/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdMonitorPressure.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdMonitorPressure class.
 @details

 ****************************************************************************
*/

#ifndef INCMDMONITORPRESSURE_H
#define INCMDMONITORPRESSURE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the MONITOR_PRESSURE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdMonitorPressure : public IncomingCommand
{

	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdMonitorPressure constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdMonitorPressure(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdMonitorPressure destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdMonitorPressure();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow get the pointer to the associated workflow
		 * @return pointer to workflow object
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
		 * @brief setAction set the action start/stop
		 * @param strAction the string START/STOP
		 * ************************************************************************************************************
		 */
		void setAction(std::string strAction);

		/*! ***********************************************************************************************************
		 * @brief setSection set the section involved
		 * @param strSection the string A/B
		 * ************************************************************************************************************
		 */
		void setSection(std::string strSection);

		/*! ***********************************************************************************************************
		 * @brief getAction get the action start/stop
		 * @return the string START = 1 / STOP = 0
		 * ************************************************************************************************************
		 */
		int getAction();

		/*! ***********************************************************************************************************
		 * @brief getSection get the section involved
		 * @return the section SCT_A_ID / SCT_B_ID
		 * ************************************************************************************************************
		 */
		int getSection();

    private:

        int m_Action;
        int m_Section;

};

#endif // INCMDMONITORPRESSURE_H
