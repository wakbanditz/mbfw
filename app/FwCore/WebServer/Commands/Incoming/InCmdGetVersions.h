/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetVersions.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetVersions class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETVERSIONS_H
#define INCMDGETVERSIONS_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETVERSIONS command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetVersions : public IncomingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdGetVersions constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdGetVersions(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdGetVersions constructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdGetVersions();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);
};

#endif // INCMDGETVERSIONS_H
