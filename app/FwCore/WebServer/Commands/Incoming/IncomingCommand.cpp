/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    IncomingCommand.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the IncomingCommand class.
 @details

 ****************************************************************************
*/

#include "IncomingCommand.h"
#include "Workflow.h"
#include "OperationManager.h"
#include "OutgoingCommand.h"
#include "MainExecutor.h"
#include "WebServerAndProtocolInclude.h"
#include "StatusInclude.h"

#include <fstream>

#define COMMAND_FILE_EXT		".cfg"
#define LOGIN_REQUESTED_STRING	"loginRequested="
#define LOGIN_REQUESTED_TRUE	"true"
#define LOGIN_REQUESTED_FALSE	"false"
#define STATES_ALLOWED_STRING	"statesAllowed="
#define STATE_TARGET_STRING		"stateTarget="
#define CONNECTION_REQUESTED_STRING	"connectionRequested="

IncomingCommand::IncomingCommand(std::string strName, std::string strUsage, std::string strID) :
	m_strName(strName),
	m_strUsage(strUsage),
	m_strID(strID)
{
	m_pWebServerConnection = 0;
	m_strCallBackUrl.clear();
	m_strConnection.clear();
	m_strTargetState.assign(NONE_STATUS_DEVICE);
	m_bLoginRequested = true;
	m_intMaskStates = 0;

	readCommandConfiguration();
}

IncomingCommand::~IncomingCommand()
{

}

bool IncomingCommand::readCommandConfiguration()
{
	if(m_strName.empty()) return false;

	std::string fileName;
	std::string strRead, strSubRead;
	std::ifstream inFile;

	fileName.assign(COMMAND_FOLDER);
	fileName.append(m_strName);
	fileName.append(COMMAND_FILE_EXT);

	inFile.open(fileName);
	if(inFile.fail())
	{
		//File does not exist code here
		printf("LoginManager::init-->%s", strerror(errno));
		return false;
	}

	while(!inFile.eof())
	{
		getline(inFile, strRead);
		if(!strRead.empty())
		{
			// lines beginning with # are comments
			if(strRead.front() != '#')
			{
				//RBB Changed with static cast in order to cancel warning
				//int posStr = 0;
				std::vector<std::string>::size_type posStr = 0;
				if((posStr = strRead.find(LOGIN_REQUESTED_STRING)) != std::string::npos)
				{
					// extract the value for the m_bLoginRequested variable
					posStr = strRead.find("=");
					strSubRead = strRead.substr(posStr + 1);
					if(strcmp(strSubRead.c_str(), LOGIN_REQUESTED_FALSE) == 0)
						m_bLoginRequested = false;
				}
				else if((posStr = strRead.find(STATES_ALLOWED_STRING)) != std::string::npos)
				{
					// extract the value for the m_intMaskStates variable
					posStr = strRead.find("=");
					strSubRead = strRead.substr(posStr + 1);
					// isolates the substrings separated by comma
					while((posStr = strSubRead.find(",")) != std::string::npos)
					{
						strRead = strSubRead.substr(0, posStr);
						associateStatus(strRead);
						strSubRead.erase(0, posStr + 1);
					}
					// last value to extract
					associateStatus(strSubRead);
				}
				else if((posStr = strRead.find(STATE_TARGET_STRING)) != std::string::npos)
				{
					// extract the value for the m_strTargetState variable
					posStr = strRead.find("=");
					m_strTargetState.assign(strRead.substr(posStr + 1));
				}
				else if((posStr = strRead.find(CONNECTION_REQUESTED_STRING)) != std::string::npos)
				{
					// extract the value for the m_strConnection variable
					posStr = strRead.find("=");
					m_strConnection.assign(strRead.substr(posStr + 1));
				}
			}
		}
	}
	inFile.close();
	return true;
}

bool IncomingCommand::associateStatus(const std::string strRead)
{
	bool bFound = false;

	// search the corresponding status and set the bitmask
	vector<string>::const_iterator strIter;
	int i;
	for(i = 0, strIter = global_strDeviceStatus.begin(); strIter != global_strDeviceStatus.end(); i++, strIter++)
	{
		if(strcmp(strIter->c_str(), strRead.c_str()) == 0)
		{
			m_intMaskStates |= (0x01 << i);
			bFound = true;
			break;
		}
	}
	if(strIter == global_strDeviceStatus.end())
	{
		// no corrispondence to a single state -> try with ALL
		if(strcmp(ALL_STATUS_DEVICE, strRead.c_str()) == 0)
		{
			m_intMaskStates = 0xFF;
			bFound = true;
		}
	}
	return bFound;
}

bool IncomingCommand::getLoginRequested()
{
	return m_bLoginRequested;
}

unsigned short IncomingCommand::getMaskStates()
{
	return m_intMaskStates;
}

void IncomingCommand::setTargetState(string strState)
{
	m_strTargetState.assign(strState);
}

std::string IncomingCommand::getTargetState()
{
	return m_strTargetState;
}

std::string IncomingCommand::getCallbackUrl()
{
	return m_strCallBackUrl;
}

std::string IncomingCommand::getUsage()
{
	return m_strUsage;
}

std::string IncomingCommand::getID()
{
	return m_strID;
}

string IncomingCommand::getConnection()
{
	return m_strConnection;
}

bool IncomingCommand::isConnectionAllowed()
{
	if(strcmp(m_strConnection.c_str(), WEB_ALL_CONNECTION) == 0)
		return true;

	if(strcmp(m_strConnection.c_str(), m_strUsage.c_str()) == 0)
		return true;

	return false;
}

void IncomingCommand::setWebServerConnection(WebServerConnection &rWebServerConnection)
{
	m_pWebServerConnection = &rWebServerConnection;

	// extract the reply address page
	m_strCallBackUrl = m_pWebServerConnection->FindRequestHeader("response-callback");
	if ( m_strCallBackUrl.length() == 0 )
	{
		m_strCallBackUrl.assign(WEB_DEFAULT_GATEWAY_BASE_URL);
	}
	if ( (*(m_strCallBackUrl.cend()--)) != '/' )
		m_strCallBackUrl.append("/");
	m_strCallBackUrl.append(WEB_DEFAULT_GATEWAY_PAGE);
}

WebServerConnection* IncomingCommand::getWebServerConnection(void)
{
	return m_pWebServerConnection;
}

bool IncomingCommand::execute(void)
{

	/* ********************************************************************************************
	 * Retrieve the workflow associated to the IncomingCommand
	 * ********************************************************************************************
	 */
	Workflow* pWF = NULL;
	pWF = getWorkflow();    // Every IncomingCommand shall be able to create its own Workflow
	if ( pWF == NULL )
	{
		return false;
	}
	pWF->setIncomingCommand(this);  // Once the Workflow is created it must be initialized with
									// the received command arguments in order to take decisions

	/* ********************************************************************************************
	 * Call the Workflow::setup() routine in order to retrieve
	 *		- a synchronous OutgoingCommand to be serialized and sent to the client
	 *		(i.e. accepted/rejected or a generic synchronous answer).
	 *		- an optional Operation to be queued to the OperationManager
	 * ********************************************************************************************
	 */
	OutgoingCommand* pSyncOutCmd = NULL;
	Operation* pAsyncOp = NULL;
	pWF->setup(pSyncOutCmd, pAsyncOp);

	/* ********************************************************************************************
	 * Send the synchronous OutgoingCommand to the client
	 * ********************************************************************************************
	 */
	bool bRetValue = true;
	std::string strOutCommand;
	if ( pSyncOutCmd != 0 )
	{
		bool bSerializeResult = protocolSerialize(pSyncOutCmd, strOutCommand);
		if ( ( ! bSerializeResult ) || ( !m_pWebServerConnection ) )
		{
			bRetValue = false;
		}
		else
		{
			strOutCommand.push_back('\r');
			m_pWebServerConnection->SendString(strOutCommand);
		}
		SAFE_DELETE(pSyncOutCmd);
	}
	else
	{
		bRetValue = false;
	}

	/* ********************************************************************************************
	 * Queue the asynchronous Operation, it's up to the AsyncOperationManager to destroy it
	 * ********************************************************************************************
	 */
	if ( pAsyncOp != 0 )	// The workflow has an async operation
	{
        if(pAsyncOp->getPersistent() == true)
        {
            // the command requires the http reply to be "persistent" -> add the suffix
            m_strCallBackUrl.append(WEB_DEFAULT_PERSIST_SUFFIX);
        }
		pAsyncOp->setWebResponseUrl(m_strCallBackUrl);
		pAsyncOp->setLogger(MainExecutor::getInstance()->getLogger());
		operationSingleton()->addOperation(pAsyncOp);
	}

	/* ********************************************************************************************
	 * Destroy the workflow
	 * ********************************************************************************************
	 */
    SAFE_DELETE(pWF);

	return bRetValue;

}
