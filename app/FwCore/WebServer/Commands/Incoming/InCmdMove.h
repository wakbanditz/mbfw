/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdMove.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdMove class.
 @details

 ****************************************************************************
*/

#ifndef INCMDMOVE_H
#define INCMDMOVE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the MOVE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdMove : public IncomingCommand
{

	public:

		/*! *************************************************************************************************
		 * @brief InCmdMove contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdMove(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdMove destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdMove();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the Move command e retrieves the pointer
         * @return Pointer to WFMove object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief getComponent Retrieves the name of the component to move
		 * @return Name of the component
		 * *************************************************************************************************
		 */
		string getComponent( void );

		/*! *************************************************************************************************
		 * @brief getMotor Retrieves the name of the motor to move
		 * @return Name of the motor
		 * *************************************************************************************************
		 */
		string getMotor( void );

		/*! *************************************************************************************************
		 * @brief getMove Retrieves the movement to be executed
		 * @return Name of the motor
		 * *************************************************************************************************
		 */
		string getMove( void );

		/*! *************************************************************************************************
		 * @brief getMoveValue	Retrieves value of the movement to be executed if present
		 * @return value in string
		 * *************************************************************************************************
		 */
		string getMoveValue( void );

		/*! *************************************************************************************************
		 * @brief setComponent	Sets the name of the component to take in considerations for Move cmd
		 * @param strComponent
		 * *************************************************************************************************
		 */
		void setComponent( const string& strComponent );

		/*! *************************************************************************************************
		 * @brief setMotor	Sets the name of the motor to take in considerations for Move cmd
		 * @param strMotor
		 * *************************************************************************************************
		 */
		void setMotor( const string& strMotor );

		/*! *************************************************************************************************
		 * @brief setMove	Sets the name of the movement to take in considerations for Move cmd
		 * @param strMove
		 * @param strValue
		 * *************************************************************************************************
		 */
		void setMove(const string& strMove , const string &strValue);

    private:

        string	m_strComponent;
        string	m_strMotor;
        string	m_strMove;
        string	m_strMoveValue;

};

#endif // INCMDMOVE_H
