/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdReadSec.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdReadSec class.
 @details

 ****************************************************************************
*/
#ifndef INCMDREADSEC_H
#define INCMDREADSEC_H

#include "CommonInclude.h"

#include "IncomingCommand.h"

class Workflow;

struct sampleDetection_tag
{
	int	 m_nSettingsX0;
	int	 m_nSettingsX3;
	bool m_bCheckX0;
	bool m_bCheckX3;
	bool m_bImageX0;
	bool m_bImageX3;
};

struct stripReadSection_tag
{
	bool m_bEnabled;
	bool m_bStripCheck;
	bool m_bSprCheck;
	bool m_bStripImage;
	bool m_bSprImage;
	bool m_bSubstrateCheck;
	sampleDetection_tag m_SampleDetection;
};

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the READSEC command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdReadSec : public IncomingCommand
{

	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdReadSec constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdReadSec(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdReadSec destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdReadSec();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
		 * @brief setSection set the section involved in the ReadSection procedure
		 * @param strSection the section string associated to the received command
		 * @return true if section string is valid, false otherwise
		 * ************************************************************************************************************
		 */
		bool setSection(std::string strSection);

		/*! ***********************************************************************************************************
		 * @brief getSection get the section involved in the ReadSection procedure
		 * @return the index of the section if defined, -1 otherwise
		 * ************************************************************************************************************
		 */
		int getSection();

		void setSettings(string& strSettings, string&strId);
		vector<pair<string, string>>& getSettings(void);

		/*! ***********************************************************************************************************
		 * @brief setStrip set the strip involved in the ReadSection procedure
		 * @param strStrip ththe index of the section if defined, -1 otherwisee strip string associated to the received command
		 * @return true if strip string is valid, false otherwise
		 * ************************************************************************************************************
		 */
		bool setStrip(std::string strStrip);

		/*! ***********************************************************************************************************
		 * @brief setStripCheck set the barcode acquisition of the strip involved in the ReadSection procedure
		 * @param strStrip the strip string associated to the received command
		 * @param strValAttr the string value (true / false)
		 * @return true if strip string is valid, false otherwise
		 * ************************************************************************************************************
		 */
		bool setStripCheck(std::string strStrip, std::string strValAttr);
		bool setStripImage(string strStrip, string strValAttr);
		bool setSprImage(string strStrip, string strValAttr);
		bool setSampleCheck(string strStrip, string strWell, string strValAttr);
		bool setSampleDetectionSettingsId(string strStrip, string strWell, string strValAttr);
		bool setSampleImage(string strStrip, string strWell, string strValAttr);

		/*! ***********************************************************************************************************
		 * @brief setSprCheck set the spr data matrix acquisition of the strip involved in the ReadSection procedure
		 * @param strStrip the strip string associated to the received command
		 * @param strValAttr the string value (true / false)
		 * @return true if strip string is valid, false otherwise
		 * ************************************************************************************************************
		 */
		bool setSprCheck(std::string strStrip, std::string strValAttr);

		/*! ***********************************************************************************************************
		 * @brief setSubstrateCheck set the substrate reading of the strip involved in the ReadSection procedure
		 * @param strStrip the strip string associated to the received command
		 * @param strValAttr the string value (true / false)
		 * @return true if strip string is valid, false otherwise
		 * ************************************************************************************************************
		 */
		bool setSubstrateCheck(std::string strStrip, std::string strValAttr);

		/*! ***********************************************************************************************************
		 * @brief getDataStructure get the whole data structure involved in the ReadSection procedure
		 * @return the m_stripReadSection structure
		 * ************************************************************************************************************
		 */
		stripReadSection_tag * getDataStructure();

		/*! *************************************************************************************************
		 * @brief  isAtLeastOneStripEnabled to check if at least one strip of the section is enabled
		 * @return false if no strip is enable, true otherwise
		 * **************************************************************************************************
		 */
		bool isAtLeastOneStripEnabled(void);

	private:

		/*! ***********************************************************************************************************
		 * @brief getStripIndex returns the index associated to the string strStrip
		 * @param strStrip the strip string (1 - 6)
		 * @return the strip index (0 - 5) if input string is valid, -1 otherwise
		 * ************************************************************************************************************
		 */
		int getStripIndex(std::string strStrip);

	private:

		stripReadSection_tag m_stripReadSection[SCT_NUM_TOT_SLOTS];
		int m_sectionRead;
		vector<pair<string, string>> m_vSettings;

};

#endif // INCMDREADSEC_H
