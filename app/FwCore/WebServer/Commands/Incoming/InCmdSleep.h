/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSleep.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSleep class.
 @details

 ****************************************************************************
*/
#ifndef INCMDSLEEP_H

#define INCMDSLEEP_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SLEEP command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSleep : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdSleep contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSleep(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSleep destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSleep();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the CancelSection command e retrieves the pointer
         * @return Pointer to WFSleep object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief setSleepInstrument set the Instrument Sleep Status ON / OFF
         * @param strEnabled the status string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
        bool setSleepInstrument(std::string strEnabled);

		/*! *************************************************************************************************
		 * @brief getSleepInstrument get the Instrument Sleep Status ON / OFF
		 * @return true if sleep, false if wakeup
		 * **************************************************************************************************
		 */
		bool getSleepInstrument();


    private:

        bool m_bSleep;

};


#endif // INCMDSLEEP_H
