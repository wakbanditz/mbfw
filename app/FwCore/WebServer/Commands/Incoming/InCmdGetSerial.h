/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetSensor.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetSensor class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETSERIAL_H
#define INCMDGETSERIAL_H


#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETSERIAL command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetSerial : public IncomingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdGetSerial constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdGetSerial(std::string strUsage, std::string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdGetSerial constructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdGetSerial();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow	to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);
};

#endif // INCMDGETSERIAL_H
