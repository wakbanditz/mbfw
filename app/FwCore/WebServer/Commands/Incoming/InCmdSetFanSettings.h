/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetFanSettings.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSetFanSettings class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSETFANSETTINGS_H
#define INCMDSETFANSETTINGS_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SETFANSETTINGS command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSetFanSettings : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdSetFanSettings contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSetFanSettings(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSetFanSettings destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSetFanSettings();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the CancelSection command e retrieves the pointer
		 * @return Pointer to WFSetFanSettings object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief setFan set a couple component-power
		 * @param strComponent	the component string associated to the received command
		 * @param strPower the percentage power string
		 * **************************************************************************************************
		 */
		void setFan(std::string strComponent, std::string strPower);

		/*! *************************************************************************************************
		 * @brief setDuration set the time for settings
		 * @param strDuration	the duration time string associated to the received command
		 * **************************************************************************************************
		 */
		void setDuration(std::string strDuration);

		/*! *************************************************************************************************
		 * @brief getDuration retrieve the time for settings
		 * @return	the duration time string associated to the received command
		 * **************************************************************************************************
		 */
		std::string getDuration();

		/*! *************************************************************************************************
		 * @brief getFan retrieve a couple component-power
		 * @param index the index in the vector
		 * @return the component+power string associated to the received command
		 * **************************************************************************************************
		 */
		std::pair<std::string, std::string> getFan(uint8_t index);

    private :

        std::vector<std::pair<std::string, std::string>> m_FanSettings;
        std::string m_strDuration;

};


#endif // INCMDSETFANSETTINGS_H
