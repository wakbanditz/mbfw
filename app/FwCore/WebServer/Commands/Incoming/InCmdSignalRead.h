/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSignalRead.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSignalRead class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSIGNALREAD_H
#define INCMDSIGNALREAD_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SIGNALREAD command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSignalRead : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdSignalRead contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSignalRead(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSHCalibrate destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSignalRead();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the Signal read command e retrieves the pointer
         * @return Pointer to WFSignalRead object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
         * @brief setRepetitions set the repetitions number
         * @param strRepetitions
		 * ************************************************************************************************************
		 */
		void setRepetitions(string& strRepetitions);

		/*! ***********************************************************************************************************
         * @brief getRepetitions get the repetitions
         * @return the repetitions string
		 * ************************************************************************************************************
		 */
		string getRepetitions(void);

		/*! ***********************************************************************************************************
         * @brief setInterval set the time interval
         * @param strInterval
		 * ************************************************************************************************************
		 */
		void setInterval(string& strInterval);

		/*! ***********************************************************************************************************
         * @brief getInterval get the time interval
         * @return the time interval string
		 * ************************************************************************************************************
		 */
		string getInterval(void);

    private:

        string m_strRepetitions;
        string m_strInterval;

};

#endif // INCMDSIGNALREAD_H
