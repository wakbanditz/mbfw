/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetFanSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSetFanSettings class.
 @details

 ****************************************************************************
*/

#include "InCmdSetFanSettings.h"
#include "WFSetFanSettings.h"
#include "WebServerAndProtocolInclude.h"

#include <utility>

InCmdSetFanSettings::InCmdSetFanSettings(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_SET_FAN_SETTINGS_IN, strUsage, strID)
{
	m_FanSettings.clear();
	m_strDuration.clear();
}

InCmdSetFanSettings::~InCmdSetFanSettings()
{
	m_FanSettings.clear();
}

Workflow* InCmdSetFanSettings::getWorkflow(void)
{
	WFSetFanSettings* pWf = new WFSetFanSettings();
	return pWf;
}

void InCmdSetFanSettings::setFan(string strComponent, string strPower)
{
	m_FanSettings.emplace_back(strComponent, strPower);
}

void InCmdSetFanSettings::setDuration(string strDuration)
{
	m_strDuration.assign(strDuration);
}

string InCmdSetFanSettings::getDuration()
{
	return m_strDuration;
}

std::pair<string, string> InCmdSetFanSettings::getFan(uint8_t index)
{
	std::pair<std::string, std::string> pairFan;

	if(index >= m_FanSettings.size())
	{
		pairFan = std::make_pair("", "");
	}
	else
	{
		pairFan = m_FanSettings.at(index);
	}
	return pairFan;
}


