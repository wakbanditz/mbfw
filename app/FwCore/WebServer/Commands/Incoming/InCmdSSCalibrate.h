/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSSCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSSCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSSCALIBRATE_H
#define INCMDSSCALIBRATE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SSCALIBRATE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSSCalibrate : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdSSCalibrate contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdSSCalibrate(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdSSCalibrate destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdSSCalibrate();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the SS calibrate command e retrieves the pointer
         * @return Pointer to WFSSCalibrate object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);
};

#endif // INCMDSSCALIBRATE_H
