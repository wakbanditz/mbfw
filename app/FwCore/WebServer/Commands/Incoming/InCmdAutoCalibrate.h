/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdAutoCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdAutoCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef INCMDAUTOCALIBRATE_H
#define INCMDAUTOCALIBRATE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the AUTOCALIBRATE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdAutoCalibrate : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdAutoCalibrate constructor
		 * **************************************************************************************************
		 */
		InCmdAutoCalibrate(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdAutoCalibrate destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdAutoCalibrate();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the InCmdAirCalibrate command e retrieves
		 *		  the pointer
         * @return pointer to WFAutoCalibration object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);
};

#endif // INCMDAUTOCALIBRATE_H
