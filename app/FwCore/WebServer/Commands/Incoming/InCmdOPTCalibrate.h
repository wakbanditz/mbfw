/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdOPTCalibrate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdOPTCalibrate class.
 @details

 ****************************************************************************
*/

#ifndef INCMDOPTCALIBRATE_H
#define INCMDOPTCALIBRATE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the OPTCALIBRATE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdOPTCalibrate : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdOPTCalibrate contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdOPTCalibrate(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdOPTCalibrate destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdOPTCalibrate();


		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the InCmdOPTCalibrate command e retrieves
		 *		  the pointer
         * @return Pointer to WFOPTCalibration object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
         * @brief getSection Retrieves the section to use
         * @return section as string
		 * *************************************************************************************************
		 */
		string getSection(void);

		/*! *************************************************************************************************
         * @brief getSlot Retrieves the slot to use
         * @return slot as string
		 * *************************************************************************************************
		 */
		string getSlot(void);

		/*! *************************************************************************************************
         * @brief getTarget Retrieves the OPT value
         * @return OPT value
		 * *************************************************************************************************
		 */
		string getTarget(void);

		/*! *************************************************************************************************
         * @brief setSection Sets the name of the section to use
         * @param strSection section as string
		 * *************************************************************************************************
		 */
		void setSection(const string& strSection);

		/*! *************************************************************************************************
         * @brief setSlot	Sets the name of the slot to use
         * @param strSlot slot as string
		 * *************************************************************************************************
		 */
		void setSlot(const string& strSlot);

		/*! *************************************************************************************************
         * @brief setTarget	Sets the target value of OPT
         * @param strTarget OPT value as string
		 * *************************************************************************************************
		 */
		void setTarget(const string& strTarget);

    private:

        string	m_strSection;
        string	m_strSlot;
        string	m_strTarget;

};

#endif // INCMDOPTCALIBRATE_H
