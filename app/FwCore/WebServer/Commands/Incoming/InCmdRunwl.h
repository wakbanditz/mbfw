/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdRunwl.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdRunwl class.
 @details

 ****************************************************************************
*/

#ifndef INCMDRUNWL_H
#define INCMDRUNWL_H

#include "IncomingCommand.h"

class Workflow;
class RunwlInfo;
class ProtocolInfo;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the RUN command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdRunwl : public IncomingCommand
{

	public:

		/*! *************************************************************************************************
		 * @brief InCmdRunwl contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdRunwl(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdRunwl destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdRunwl();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the GetCalibration command e retrieves the pointer
         * @return Pointer to WFRunwl object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief setProtocolInfoId create the protocolInfo object of index and assign the id
		 * @param index the protocol index (0 or 1)
		 * @param strId the protocol ID string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtocolInfoId(uint8_t index, std::string strId);

		/*! *************************************************************************************************
		 * @brief setProtocolInfoName assign the name
		 * @param index the protocol index (0 or 1)
		 * @param strName the protocol name string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtocolInfoName(uint8_t index, std::string strName);

		/*! *************************************************************************************************
		 * @brief setProtoGlobals set the Protocol Globals read from the incoming command
		 * @param index the protocol index (0 or 1)
		 * @param strGlobals the protocol Globals string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoGlobals(uint8_t index, const char * strGlobals);

		/*! *************************************************************************************************
		 * @brief setProtoLocals set the Protocol Locals read from the incoming command
		 * @param index the protocol index (0 or 1)
		 * @param strLocals the protocol Locals string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoLocals(uint8_t index, const char * strLocals);

		/*! *************************************************************************************************
		 * @brief setProtoMinMaxJ set the Protocol J min / max read from the incoming command
		 * @param protoIndex the protocol index (0 o 1)
		 * @param value the J time value
		 * @param index the J index
		 * @param isMax if = 1 -> Max, if = 0 -> Min
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setProtoMinMaxJ(uint8_t protoIndex, uint32_t value, uint8_t index, uint8_t isMax);


		/*! *************************************************************************************************
		 * @brief getProtocolInfoId get the id
		 * @param index the protocol index (0 or 1)
		 * @return the protocol ID string
		 * **************************************************************************************************
		 */
		std::string getProtocolInfoId(uint8_t index);

		/*! *************************************************************************************************
		 * @brief getProtocolInfoName get the name
		 * @param index the protocol index (0 or 1)
		 * @return the protocol name string
		 * **************************************************************************************************
		 */
		std::string getProtocolInfoName(uint8_t index);


		/*! *************************************************************************************************
		 * @brief getProtoGlobals get the Protocol Globals read from the incoming command
		 * @param index the protocol index (0 or 1)
         * @param strGlobals the Globals string
		 * @return the protocol Globals string
		 * **************************************************************************************************
		 */
		bool getProtoGlobals(int8_t index, char * strGlobals);

		/*! *************************************************************************************************
		 * @brief getProtoLocals get the Protocol Locals read from the incoming command
		 * @param index the protocol index (0 or 1)
         * @param strLocals the Locals string
         * @return the protocol Locals string
		 * **************************************************************************************************
		 */
		bool getProtoLocals(int8_t index, char * strLocals);

		/*! *************************************************************************************************
		 * @brief getProtoMinMaxJ set the Protocol J min / max read from the incoming command
         * @param protoIndex the protocol index (0 or 1)
		 * @param index the J index
		 * @param isMax if = 1 -> Max, if = 0 -> Min
		 * @return the J value
		 * **************************************************************************************************
		 */
		uint32_t getProtoMinMaxJ(int8_t protoIndex, uint8_t index, uint8_t isMax);


		/*! *************************************************************************************************
		 * @brief getProtocolInfoPointer retrieves the pointer to the local object ProtocolInfo
		 * @param section the section index
		 * @return Pointer to ProtocolInfo object
		 * **************************************************************************************************
		 */
		ProtocolInfo * getProtocolInfoPointer(uint8_t section);

		/*! *************************************************************************************************
		 * @brief getRunwlPointer retrieves the pointer to the local object Runwl
		 * @param section the section index
		 * @return Pointer to RunwlInfo object
		 * **************************************************************************************************
		 */
		RunwlInfo * getRunwlPointer(uint8_t section);

		/*! *************************************************************************************************
		 * @brief setSectionEnabled activate the section if received from the incoming command
		 * @param section the section index (A or B)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionEnabled(uint8_t section);

		/*! *************************************************************************************************
		 * @brief isSectionEnabled get the activate of the section if received from the incoming command
		 * @param section the section index (A or B)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool isSectionEnabled(uint8_t section);

		/*! *************************************************************************************************
		 * @brief setSection set the Section read from the incoming command
         * @param section the section index (A or B)
         * @param strSection the section string (A or B)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSection(uint8_t section, std::string strSection);

		/*! *************************************************************************************************
		 * @brief setProtoId set the Protocol ID read from the incoming command
		 * @param section the section index
		 * @param strId the protocol ID
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionProtoId(uint8_t section, std::string strId);

		/*! *************************************************************************************************
		 * @brief setUnitConstraint set the Protocol Constraint Unit read from the incoming command
		 * @param section the section index
		 * @param unitConstraint the Constraint Unit string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionUnitConstraint(uint8_t section, std::string unitConstraint);

		/*! *************************************************************************************************
		 * @brief setMinConstraint set the Protocol Constraint Minimum read from the incoming command
		 * @param section the section index
		 * @param minConstraint the Constraint Minimum value
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionMinConstraint(uint8_t section, uint32_t minConstraint);

		/*! *************************************************************************************************
		 * @brief setMaxConstraint set the Protocol Constraint Maximum read from the incoming command
		 * @param section the section index
		 * @param maxConstraint the Constraint Maximum value
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionMaxConstraint(uint8_t section, uint32_t maxConstraint);

		/*! *************************************************************************************************
		 * @brief setStripId set the Strip Runid read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @param strId the RunId section string (Y or N)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionStripId(uint8_t section, int strip, std::string strId);

		/*! *************************************************************************************************
		 * @brief setStripProfile set the Strip Profile read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @param strProfile the Profile section string (1 or 0)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionStripLiquidProfile(uint8_t section, int strip, std::string strProfile);

		/*! *************************************************************************************************
		 * @brief setStripCheck set the Strip Check read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @param strCheck the Check section string (1 or 0)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionStripPressureProfile(uint8_t section, int strip, std::string strCheck);

		/*! *************************************************************************************************
		 * @brief setStripRfuconv set the Strip Rfuconv read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
         * @param value the rfuconv section flag (0 or 1)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
        bool setSectionStripRfuconv(uint8_t section, int strip, uint8_t value);

		/*! *************************************************************************************************
		 * @brief setProtoSettings set the Protocol Settings reference read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @param strSettings the protocol settings string to be linked to AcquisitionSettings
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionStripSettings(uint8_t section, int strip, std::string strSettings);

		/*! *************************************************************************************************
		 * @brief setProtoMeasureAir set the Protocol Measure Air flag read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @param value the flag value
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionStripMeasureAir(uint8_t section, int strip, uint8_t value);

		/*! *************************************************************************************************
		 * @brief setProtoTreatedPressure set the Protocol Treated Pressure flag read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @param value the flag value
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionStripTreatedPressure(uint8_t section, int strip, uint8_t value);

		/*! *************************************************************************************************
		 * @brief setProtoElaboratedPressure set the Protocol Elaborated Pressure flag read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @param value the flag value
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionStripElaboratedPressure(uint8_t section, int strip, uint8_t value);

		/*! *************************************************************************************************
		 * @brief setMonitoringTemperature set the Temperature Monitoring flag read from the incoming command
		 * @param section the section index
		 * @param value the flag (1 or 0)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionMonitoringTemperature(uint8_t section, uint8_t value);

		/*! *************************************************************************************************
		 * @brief setMonitoringAutocheck set the Autocheck Monitoring flag read from the incoming command
		 * @param section the section index
		 * @param value the flag (1 or 0)
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setSectionMonitoringAutocheck(uint8_t section, uint8_t value);

		/*! *************************************************************************************************
		 * @brief getSection get the Protocol Section read from the incoming command
		 * @param section the section index
		 * @return the protocol section
		 * **************************************************************************************************
		 */
		uint8_t getSection(uint8_t section);

		/*! *************************************************************************************************
		 * @brief getProtoId get the Protocol ID read from the incoming command
		 * @param section the section index
		 * @return the protocol id
		 * **************************************************************************************************
		 */
		std::string getSectionProtoId(uint8_t section);

		/*! *************************************************************************************************
		 * @brief getUnitConstraint get the Protocol Constraint Unit read from the incoming command
		 * @param section the section index
		 * @return the Constraint Unit string
		 * **************************************************************************************************
		 */
		std::string getSectionUnitConstraint(uint8_t section);

		/*! *************************************************************************************************
		 * @brief getMinimumConstraint return the Constrint minimum time (0 if not set)
		 * @param section the section index
		 * @return the minimum time constraint
		 * **************************************************************************************************
		 */
		uint32_t getSectionMinimumConstraint(uint8_t section);

		/*! *************************************************************************************************
		 * @brief getMaximumConstraint return the Constraint maximum time (0 if not set)
		 * @param section the section index
		 * @return the maximum time constraint
		 * **************************************************************************************************
		 */
		uint32_t getSectionMaximumConstraint(uint8_t section);

		/*! *************************************************************************************************
		 * @brief getStripId get the Strip ID read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @return the ID string
		 * **************************************************************************************************
		 */
		std::string getSectionStripId(uint8_t section, int strip);

		/*! *************************************************************************************************
		 * @brief getStripRfuconv get the Strip Rfuconv read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @return the Rfuconv flag
		 * **************************************************************************************************
		 */
		uint8_t getSectionStripRfuconv(uint8_t section, int strip);

		/*! *************************************************************************************************
		 * @brief getProtoSettings get the Protocol Settings reference read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @return the protocol settings string to be linked to AcquisitionSettings
		 * **************************************************************************************************
		 */
		std::string getSectionStripSettings(uint8_t section, int strip);

		/*! *************************************************************************************************
		 * @brief getProtoMeasureAir get the Protocol Measure Air flag read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @return the flag value
		 * **************************************************************************************************
		 */
		uint8_t getSectionStripMeasureAir(uint8_t section, int strip);

		/*! *************************************************************************************************
		 * @brief getProtoTreatedPressure get the Protocol Treated Pressure flag read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @return the flag value
		 * **************************************************************************************************
		 */
		uint8_t getSectionStripTreatedPressure(uint8_t section, int strip);

		/*! *************************************************************************************************
		 * @brief getProtoElaboratedPressure get the Protocol Elaborated Pressure flag read from the incoming command
		 * @param section the section index
		 * @param strip the strip index
		 * @return the flag value
		 * **************************************************************************************************
		 */
		uint8_t getSectionStripElaboratedPressure(uint8_t section, int strip);

		/*! *************************************************************************************************
		 * @brief getStripWellProfile get the Strip Profile read from the incoming command
         * @param section the section index
         * @param strip the strip index
         * @param well the well index
		 * @return the Profile string splitted per each well
		 * **************************************************************************************************
		 */
		uint8_t getStripWellProfile(uint8_t section, int strip, int well);

		/*! *************************************************************************************************
		 * @brief getStripWellCheck get the Strip Check read from the incoming command
         * @param section the section index
         * @param strip the strip index
         * @param well the well index
         * @return the Check string splitted per each well
		 * **************************************************************************************************
		 */
		uint8_t getStripWellCheck(uint8_t section, int strip, int well);

		/*! *************************************************************************************************
		 * @brief getMonitoringTemperature get the Temperature Monitoring flag read from the incoming command
		 * @param section the section index
		 * @return the falg value
		 * **************************************************************************************************
		 */
		bool getSectionMonitoringTemperature(uint8_t section);

		/*! *************************************************************************************************
		 * @brief getMonitoringAutocheck get the Autocheck Monitoring flag read from the incoming command
		 * @param section the section index
		 * @return the flag value
		 * **************************************************************************************************
		 */
		bool getSectionMonitoringAutocheck(uint8_t section);

		/*! *************************************************************************************************
		 * @brief isAtLeastOneStripEnabled to check if at least one strip of the section is enable
		 *					(RunId not empty)
         * @param section the section index
         * @return false if no strip is enable, true otherwise
		 * **************************************************************************************************
		 */
		bool isAtLeastOneStripEnabled(uint8_t section);

		/*! *************************************************************************************************
		 * @brief decodeConstraint decode the Constraint string received from command: only T or M are allowed
		 * @param section the section index
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool decodeConstraint(uint8_t section);

		/*! *************************************************************************************************
		 * @brief addPressureSettings create a new element in the vector PressureSettings and set the parameters
		 * @param strID the ID string
		 * @param strSettings the XML string
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool addPressureSettings(std::string strID, std::string strSettings);

		/*! *************************************************************************************************
		 * @brief getPressureSettings get the element in the vector PressureSettings and read the parameters
		 * @param index the element index
		 * @param strID pointer to the ID string (output)
		 * @param strSettings pointer to the XML string (output)
		 * @return true if succesfull, false otherwise
		 * **************************************************************************************************
		 */
		bool getPressureSettings(uint8_t index, string * strID, string * strSettings);

	private:

		RunwlInfo * m_pRunwlInfo[SCT_NUM_TOT_SECTIONS];
		ProtocolInfo * m_pProtocolInfo[SCT_NUM_TOT_SECTIONS];
		std::vector<std::pair<std::string, std::string>> m_pressureSettings;

		bool m_sectionRunwl[SCT_NUM_TOT_SECTIONS];

};


#endif // INCMDRUNWL_H
