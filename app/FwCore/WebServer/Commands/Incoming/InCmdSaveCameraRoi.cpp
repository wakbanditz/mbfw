/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSaveCameraRoi.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSaveCameraRoi class.
 @details

 ****************************************************************************
*/

#include "InCmdSaveCameraRoi.h"
#include "WebServerAndProtocolInclude.h"
#include "WFSaveCameraRoi.h"

InCmdSaveCameraRoi::InCmdSaveCameraRoi(string strUsage, string strID) :
					IncomingCommand(WEB_CMD_NAME_SAVE_CAMERA_ROI_IN, strUsage, strID)
{
	m_vTarget.clear();
	m_vTop.clear();
	m_vBottom.clear();
	m_vLeft.clear();
	m_vRight.clear();
}

InCmdSaveCameraRoi::~InCmdSaveCameraRoi()
{

}

Workflow* InCmdSaveCameraRoi::getWorkflow()
{
	WFSaveCameraRoi* pWf = new WFSaveCameraRoi();
	return pWf;
}

void InCmdSaveCameraRoi::setTarget(string& strTarget)
{
	m_vTarget.push_back(strTarget);
}

void InCmdSaveCameraRoi::setTop(string& strTop)
{
	m_vTop.push_back(strTop);
}

void InCmdSaveCameraRoi::setBottom(string& strBottom)
{
	m_vBottom.push_back(strBottom);
}

void InCmdSaveCameraRoi::setLeft(string& strLeft)
{
	m_vLeft.push_back(strLeft);
}

void InCmdSaveCameraRoi::setRight(string& strRight)
{
	m_vRight.push_back(strRight);
}

vector<string>& InCmdSaveCameraRoi::getTarget()
{
	return m_vTarget;
}

vector<string>& InCmdSaveCameraRoi::getTop()
{
	return m_vTop;
}

vector<string>& InCmdSaveCameraRoi::getBottom()
{
	return m_vBottom;
}

vector<string>& InCmdSaveCameraRoi::getLeft()
{
	return m_vLeft;
}

vector<string>& InCmdSaveCameraRoi::getRight()
{
	return m_vRight;
}
