/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdFwUpdate.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdFwUpdate class.
 @details

 ****************************************************************************
*/
#ifndef INCMDFWUPDATE_H

#define INCMDFWUPDATE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the FWUODATE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdFwUpdate : public IncomingCommand
{
	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdFwUpdate constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdFwUpdate(string strUsage, string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdFwUpdate virtual destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdFwUpdate();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow to get the associated Workflow
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		void setUrl(string& strUrl);
		void setVersioningCheck(string& strVersioningCheck);
		void setComponent(string& strComponent);
		void setCRC(string& strCRC32);

		string getUrl(void);
		string getVersioningCheck(void);
		string getCRC(void);
		vector<string> getComponents(void);


    private:

        string m_strUrl;
        string m_strVersCheck;
        string m_strCRC32;
        // We may have more than one component
        vector<string> m_vComponents;

};

#endif // INCMDFWUPDATE_H
