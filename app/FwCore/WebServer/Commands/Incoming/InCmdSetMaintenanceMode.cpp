/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetMaintenanceMode.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSetMaintenanceMode class.
 @details

 ****************************************************************************
*/

#include "InCmdSetMaintenanceMode.h"
#include "WFSetMaintenanceMode.h"
#include "WebServerAndProtocolInclude.h"

InCmdSetMaintenanceMode::InCmdSetMaintenanceMode(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_SET_MAINTENANCE_MODE_IN, strUsage, strID)
{
	m_strActivation.clear();
}

InCmdSetMaintenanceMode::~InCmdSetMaintenanceMode()
{

}

Workflow* InCmdSetMaintenanceMode::getWorkflow(void)
{
	WFSetMaintenanceMode* pWf = new WFSetMaintenanceMode();
	return pWf;
}


string InCmdSetMaintenanceMode::getActivation() const
{
	return m_strActivation;
}

void InCmdSetMaintenanceMode::setActivation(const string& strActivation)
{
	m_strActivation.assign(strActivation);
}
