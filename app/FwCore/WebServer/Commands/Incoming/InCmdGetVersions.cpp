/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetVersions.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetVersions class.
 @details

 ****************************************************************************
*/

#include "InCmdGetVersions.h"
#include "WFGetVersions.h"
#include "WebServerAndProtocolInclude.h"

InCmdGetVersions::InCmdGetVersions(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_GETVERSIONS_IN, strUsage, strID)
{

}


InCmdGetVersions::~InCmdGetVersions()
{

}

Workflow* InCmdGetVersions::getWorkflow(void)
{
	WFGetVersions* pWf = new WFGetVersions();
	return pWf;
}

