/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdInit.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdInit class.
 @details

 ****************************************************************************
*/

#ifndef INCMDINIT_H
#define INCMDINIT_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the INIT command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdInit : public IncomingCommand
{
	public:
		/*! *************************************************************************************************
		 * @brief InCmdInit contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdInit(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdInit destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdInit();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the INIT command e retrieves the pointer
         * @return Pointer to WFInit object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief setComponent	Sets the name of the component to take in considerations for INIT cmd
		 * @param strComponent
		 * *************************************************************************************************
		 */
		void setComponent( const string& strComponent );

		/*! *************************************************************************************************
		 * @brief getMotor Retrieves the name of the motor component to know the position of
		 * @return Name of the motor
		 * *************************************************************************************************
		 */
		string getComponent( void );

    private:

        string	m_strComponent;

};

#endif // INCMDINIT_H
