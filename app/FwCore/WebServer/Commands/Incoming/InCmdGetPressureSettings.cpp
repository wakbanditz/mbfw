/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetPressureSettings.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdGetPressureSettings class.
 @details

 ****************************************************************************
*/

#include "InCmdGetPressureSettings.h"
#include "WFGetPressureSettings.h"
#include "WebServerAndProtocolInclude.h"

InCmdGetPressureSettings::InCmdGetPressureSettings(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_GET_PRESSURE_SETTINGS_IN, strUsage, strID)
{
	m_intSection.clear();
}

InCmdGetPressureSettings::~InCmdGetPressureSettings()
{
}

Workflow* InCmdGetPressureSettings::getWorkflow(void)
{
	WFGetPressureSettings* pWf = new WFGetPressureSettings();
	return pWf;
}

bool InCmdGetPressureSettings::setSection(string strSection)
{
	uint8_t section = SCT_NONE_ID;

	if(strcmp(strSection.c_str(), XML_VALUE_SECTION_A) == 0)
	{
		section = SCT_A_ID;
	}
	else if(strcmp(strSection.c_str(), XML_VALUE_SECTION_B) == 0)
	{
		section = SCT_B_ID;
	}
	else
	{
		return false;
	}

	m_intSection.push_back(section);
	return true;
}

uint8_t InCmdGetPressureSettings::getSection(std::vector<uint8_t> * pSection)
{
	pSection->clear();

	for(uint8_t i = 0; i < m_intSection.size(); i++)
	{
		pSection->push_back(m_intSection.at(i));
	}

	return pSection->size();
}


