/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetTime.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdSetTime class.
 @details

 ****************************************************************************
*/

#ifndef INCMDSETTIME_H
#define INCMDSETTIME_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the SETTIME command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdSetTime : public IncomingCommand
{

	public:

		/*! ***********************************************************************************************************
		 * @brief InCmdSetTime constructor
		 * @param strUsage the connection string associated to the received command
		 * @param strID the CommandId string
		 * ************************************************************************************************************
		 */
		InCmdSetTime(string strUsage, string strID);

		/*! ***********************************************************************************************************
		 * @brief InCmdSetTime destructor
		 * ************************************************************************************************************
		 */
		virtual ~InCmdSetTime();

		/*! ***********************************************************************************************************
		 * @brief getWorkflow creates and return the Workflow object linked to the command
		 * @return the Workflow object pointer
		 * ************************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! ***********************************************************************************************************
		 * @brief getTimeString gets the Time string
		 * @return the Time string
		 * ************************************************************************************************************
		 */
		string getTimeString() const;

		/*! ***********************************************************************************************************
		 * @brief setTimeString sets the Time string
		 * @param strTime the Time string
		 * ************************************************************************************************************
		 */
		void setTimeString(const string& strTime);


    private:

        string m_strTime;

};


#endif // INCMDSETTIME_H
