/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetCameraRoi.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetCameraRoi class.
 @details

 ****************************************************************************
*/
#ifndef INCMDGETCAMERAROI_H

#define INCMDGETCAMERAROI_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETCAMERAROI command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetCameraRoi : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdGetCameraRoi contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdGetCameraRoi(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdGetCameraRoi destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdGetCameraRoi();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the InCmdGetCameraRoi command e retrieves
		 *		  the pointer
         * @return Pointer to WFGetCameraRoi object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief setCurrentTarget add target to he vector (can be more than one)
		 * @param strTarget parameter which we want to get the ROI
		 * **************************************************************************************************
		 */
		void setCurrentTarget(string strTarget);

		/*! *************************************************************************************************
		 * @brief  getTargets get all target we want t find the ROI
		 * @return vector of targets
		 * **************************************************************************************************
		 */
		vector<string>& getTargets(void);


    private:

        vector<string> m_vTargets;

};


#endif // INCMDGETCAMERAROI_H
