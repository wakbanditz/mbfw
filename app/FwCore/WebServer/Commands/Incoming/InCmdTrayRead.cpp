/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdTrayRead.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdTrayRead class.
 @details

 ****************************************************************************
*/

#include "InCmdTrayRead.h"
#include "WFTrayRead.h"
#include "WebServerAndProtocolInclude.h"

InCmdTrayRead::InCmdTrayRead(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_TRAY_READ_IN, strUsage, strID)
{
	m_section = SCT_NUM_TOT_SECTIONS;
}

InCmdTrayRead::~InCmdTrayRead()
{

}

Workflow* InCmdTrayRead::getWorkflow()
{
	WFTrayRead* pWf = new WFTrayRead();
	return pWf;
}

bool InCmdTrayRead::setSection(string strSection)
{
	if ( ! strcmp(strSection.c_str(), XML_VALUE_SECTION_A) )
	{
		m_section = SCT_A_ID;
	}
	else if ( ! strcmp(strSection.c_str(), XML_VALUE_SECTION_B) )
	{
		m_section = SCT_B_ID;
	}
	else
	{
		return false;
	}
	return true;
}

uint8_t InCmdTrayRead::getSection()
{
	return m_section;
}
