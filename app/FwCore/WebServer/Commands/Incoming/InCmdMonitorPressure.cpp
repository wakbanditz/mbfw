/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdMonitorPressure.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdMonitorPressure class.
 @details

 ****************************************************************************
*/

#include "InCmdMonitorPressure.h"
#include "WFMonitorPressure.h"
#include "WebServerAndProtocolInclude.h"


InCmdMonitorPressure::InCmdMonitorPressure(std::string strUsage, std::string strID) :
	IncomingCommand(WEB_CMD_NAME_MONITOR_PRESSURE_IN, strUsage, strID)
{
	m_Action = MONITOR_PRESSURE_NONE;
	m_Section = SCT_NONE_ID;
}

InCmdMonitorPressure::~InCmdMonitorPressure()
{

}

Workflow* InCmdMonitorPressure::getWorkflow(void)
{
	WFMonitorPressure* pWf = new WFMonitorPressure();
	return pWf;
}

void InCmdMonitorPressure::setAction(string strAction)
{
	if(strcmp(strAction.c_str(), XML_VALUE_START) == 0)
	{
		m_Action = MONITOR_PRESSURE_START;
	}
	else if(strcmp(strAction.c_str(), XML_VALUE_STOP) == 0)
	{
		m_Action = MONITOR_PRESSURE_STOP;
	}
}

void InCmdMonitorPressure::setSection(string strSection)
{
	if(strcmp(strSection.c_str(), XML_VALUE_SECTION_A) == 0)
	{
		m_Section = SCT_A_ID;
	}
	else if(strcmp(strSection.c_str(), XML_VALUE_SECTION_B) == 0)
	{
		m_Section = SCT_B_ID;
	}
}

int InCmdMonitorPressure::getAction()
{
	return m_Action;
}

int InCmdMonitorPressure::getSection()
{
	return m_Section;
}

