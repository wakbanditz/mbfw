/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetSSReference.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetSSReference class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETSSREFERENCE_H
#define INCMDGETSSREFERENCE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETSSREFEERENCE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetSSReference : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdGetSSReference contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdGetSSReference(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdGetSSReference destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdGetSSReference();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the SS calibrate command e retrieves the pointer
         * @return Pointer to Workflow object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);
};

#endif // INCMDGETSSREFERENCE_H
