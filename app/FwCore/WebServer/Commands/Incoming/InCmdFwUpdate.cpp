/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdFwUpdate.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdFwUpdate class.
 @details

 ****************************************************************************
*/

#include "InCmdFwUpdate.h"
#include "WFFwUpdate.h"
#include "WebServerAndProtocolInclude.h"


InCmdFwUpdate::InCmdFwUpdate(string strUsage, string strID) :
			   IncomingCommand(WEB_CMD_NAME_FW_UPDATE_IN, strUsage, strID)
{
	m_strUrl.clear();
	m_strVersCheck.clear();
	m_vComponents.clear();
	m_strCRC32.clear();
}

InCmdFwUpdate::~InCmdFwUpdate()
{

}

Workflow* InCmdFwUpdate::getWorkflow()
{
	WFFwUpdate* pWf = new WFFwUpdate();
	return pWf;
}

void InCmdFwUpdate::setUrl(string& strUrl)
{
	m_strUrl.assign(strUrl);
}

void InCmdFwUpdate::setVersioningCheck(string& strVersioningCheck)
{
	m_strVersCheck.assign(strVersioningCheck);
}

void InCmdFwUpdate::setComponent(string& strComponent)
{
	m_vComponents.push_back(strComponent);
}

void InCmdFwUpdate::setCRC(string& strCRC32)
{
	m_strCRC32.assign(strCRC32);
}

string InCmdFwUpdate::getUrl()
{
	return m_strUrl;
}

string InCmdFwUpdate::getVersioningCheck()
{
	return m_strVersCheck;
}

vector<string> InCmdFwUpdate::getComponents()
{
	return m_vComponents;
}

string InCmdFwUpdate::getCRC()
{
	return m_strCRC32;
}
