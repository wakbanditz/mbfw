/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdSetParameter.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the InCmdSetParameter class.
 @details

 ****************************************************************************
*/

#include "InCmdSetParameter.h"
#include "WFSetParameter.h"
#include "WebServerAndProtocolInclude.h"

InCmdSetParameter::InCmdSetParameter(string strUsage, string strID) :
	IncomingCommand(WEB_CMD_NAME_SETPARAMETER_IN, strUsage, strID)
{
	m_strComponent.clear();
	m_strMotor.clear();
	m_strMicrostep.clear();
	m_strHighCurrent.clear();
	m_strLowCurrent.clear();
	m_strAcceleration.clear();
	m_strConversion.clear();
}

InCmdSetParameter::~InCmdSetParameter()
{
	/* Nothing to do yet */
}

Workflow* InCmdSetParameter::getWorkflow(void)
{
	WFSetParameter* pWf = new WFSetParameter();
	return pWf;
}

void InCmdSetParameter::setComponent(string strValue)
{
	m_strComponent.assign(strValue);
}

void InCmdSetParameter::setMotor(string strValue)
{
	m_strMotor.assign(strValue);
}

void InCmdSetParameter::setMicrostep(string strValue)
{
	m_strMicrostep.assign(strValue);
}

void InCmdSetParameter::setConversion(string strValue)
{
	m_strConversion.assign(strValue);
}

void InCmdSetParameter::setHighCurrent(string strValue)
{
	m_strHighCurrent.assign(strValue);
}

void InCmdSetParameter::setLowCurrent(string strValue)
{
	m_strLowCurrent.assign(strValue);
}

void InCmdSetParameter::setAcceleration(string strValue)
{
	m_strAcceleration.assign(strValue);
}

string InCmdSetParameter::getComponent()
{
	return m_strComponent;
}

string InCmdSetParameter::getMotor()
{
	return m_strMotor;
}

string InCmdSetParameter::getMicrostep()
{
	return m_strMicrostep;
}

string InCmdSetParameter::getConversion()
{
	return m_strConversion;
}

string InCmdSetParameter::getHighCurrent()
{
	return m_strHighCurrent;
}

string InCmdSetParameter::getLowCurrent()
{
	return m_strLowCurrent;
}

string InCmdSetParameter::getAcceleration()
{
	return m_strAcceleration;
}
