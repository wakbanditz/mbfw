/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdDisable.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdDisable class.
 @details

 ****************************************************************************
*/

#ifndef INCMDDISABLE_H
#define INCMDDISABLE_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the DISABLE command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdDisable : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdDisable contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdDisable(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdDisable destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdDisable();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the Disable command e retrieves the pointer
		 * @return Pointer to WFGetCalibration object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief setDisableInstrument set the Instrument Disable Status ON / OFF
		 * @param strEnable the Enable string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setDisableInstrument(std::string strEnable);

		/*! *************************************************************************************************
		 * @brief setDisableNSH set the NSH Disable Status ON / OFF
		 * @param strEnable the Enable string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setDisableNSH(std::string strEnable);

		/*! *************************************************************************************************
		 * @brief setDisableCamera set the Camera Disable Status ON / OFF
		 * @param strEnable the Enable string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setDisableCamera(std::string strEnable);

		/*! *************************************************************************************************
		 * @brief setDisableSection set the Section Disable Status ON / OFF
		 * @param strSection the section string identifier
		 * @param strEnable the Enable string
		 * @return true if succesful, false otherwise
		 * **************************************************************************************************
		 */
		bool setDisableSection(std::string strSection, std::string strEnable);


		/*! *************************************************************************************************
		 * @brief getDisableInstrument get the Instrument Disable Status ON / OFF
		 * @return the Enable string
		 * **************************************************************************************************
		 */
		std::string getDisableInstrument();

		/*! *************************************************************************************************
		 * @brief getDisableNSH get the NSH Disable Status ON / OFF
		 * @return the Enable string
		 * **************************************************************************************************
		 */
		std::string getDisableNSH();

		/*! *************************************************************************************************
		 * @brief getDisableCamera get the Camera Disable Status ON / OFF
		 * @return the Enable string
		 * **************************************************************************************************
		 */
		std::string getDisableCamera();

		/*! *************************************************************************************************
		 * @brief getDisableSection get the Section Disable Status ON / OFF
		 * @param section the section index identifier
		 * @return the Enable string
		 * **************************************************************************************************
		 */
		std::string getDisableSection(uint8_t section);

    private:

        std::string m_strDisableInstrument, m_strDisableNsh, m_strDisableCamera,
                    m_strDisableSectionA, m_strDisableSectionB;

};


#endif // INCMDDISABLE_H
