/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InCmdGetCounter.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InCmdGetCounter class.
 @details

 ****************************************************************************
*/

#ifndef INCMDGETCOUNTER_H
#define INCMDGETCOUNTER_H

#include "IncomingCommand.h"

class Workflow;

/*! ***********************************************************************************************************
 * @brief class derived from IncomingCommand to implement the GETCOUNTER command
 *  (refer to IncomingCommand for redefined functions)
 * ************************************************************************************************************
 */
class InCmdGetCounter : public IncomingCommand
{
	public:

		/*! *************************************************************************************************
		 * @brief InCmdGetCounter contructor
		 * @param strUsage	the connection string associated to the received command
		 * @param strID the CommandId string
		 * **************************************************************************************************
		 */
		InCmdGetCounter(string strUsage, string strID);

		/*! *************************************************************************************************
		 * @brief InCmdGetCounter destructor
		 * **************************************************************************************************
		 */
		virtual ~InCmdGetCounter();

		/*! *************************************************************************************************
		 * @brief getWorkflow Creates the Workflow associated to the InCmdAirCalibrate command e retrieves
		 *		  the pointer
         * @return Pointer to WFGetCounter object
		 * **************************************************************************************************
		 */
		Workflow* getWorkflow(void);

		/*! *************************************************************************************************
		 * @brief getCounter Retrieves the counter label
		 * @return Name of the label
		 * *************************************************************************************************
		 */
		string getCounter(void);

		/*! *************************************************************************************************
		 * @brief setCounter	Sets the name of the counter
		 * @param strCounter the label string
		 * *************************************************************************************************
		 */
		void setCounter(const string& strCounter);

    private:

        string	m_strCounter;

};

#endif // INCMDGETCOUNTER_H
