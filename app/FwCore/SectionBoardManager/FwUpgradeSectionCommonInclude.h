#ifndef FWUPGRADESECTIONCOMMONINCLUDE_H
#define FWUPGRADESECTIONCOMMONINCLUDE_H
/*!***************************************************************************
* CAN_BinProt - Header file associated to the communication layer associated *
* to the FlexCAN devices. It implements a very simple interface with the    *
* lower level devices.                                                      *
*                                                                           *
* V. 0.0.0 2011-06-23                                                       *
*                                                                           *
*****************************************************************************/
#include <string>
#include "CommonInclude.h"
using namespace std;

#define SCT_UPG_PATH "/home/root/APPSW_MOD_20200525.BIN" // "/home/root/APPSW.BIN"

#define ID_SINGLEBLOCK 	0x0
#define ID_MULTIBLOCK 	0x1
#define ID_ACKNOWAIT	0x0
#define ID_ACKWAIT		0x1
#define ID_NOCHECKCRC	0x0
#define ID_CHECKCRC	0x1
#define ID_BINSINGLEPKT 0x0
#define ID_BINMULTIPKT 	0x1

#define ID_SINGLEBLOCK_MASK 	0x0
#define ID_MULTIBLOCK_MASK		0x80
#define ID_ACKNOWAIT_MASK		0x0
#define ID_ACKWAIT_MASK			0x40
#define ID_NOCHECKCRC_MASK		0x0
#define ID_CHECKCRC_MASK		0x20
#define ID_BINSINGLEPKT_MASK	0x0
#define ID_BINMULTIPKT_MASK 	0x10
#define ID_BINNOTIMESTAMP_MASK	0x0
#define ID_BINTIMESTAMP_MASK 	0x10

#define TYPE_BINPROT 0x0
#define TYPE_BINDATA 0x1
#define TYPE_BINSINGLEPKT 0x0
#define TYPE_BINMULTIPKT 0x1
#define TYPE_BINDATA_MASK		0x80
#define TYPE_BINMULTIPKT_MASK	0x40

#define TYPE_PROTACK	0x0
#define TYPE_PROTNACK	0x1
#define TYPE_PROTCRCOK	0x2
#define TYPE_PROTCRCERR	0x3
#define TYPE_PROTCNTERR	0x4
#define TYPE_PROTRETRY  0x5 //for debug
#define TYPE_PROTABORT	0xF //for debug

#define FWUPDATE_NOERROR	0x0
#define FWUPDATE_ERROPENFILE	0x100

#define TYPE_PKT_FILE_DATA 0x1A
#define ETX_BINPROT 0x3
#define CAN_BINPROT_STX	0x0
#define CAN_BINPROT_CON	0xFE
#define CAN_BINPROT_ETX	0xFF
#define MAX_CNT_CAN_BIN_PROT 255
#define DATA_LEN_BINBUFF 7
#define MAX_LEN_SPLIT_BLOCK 1024
#define CAN_BIN_DUMMY 0xAB
#define STX_CAN_BIN 0xEB90ABCD
#define RETRY_CMDBIN	3
#define MAX_RETRY_BLOCK 3
#define CAN0_UPGFW_STEP_FIRSTPKT '0'
#define CAN0_UPGFW_STEP_LASTPKT	 '1'
#define CAN0_UPGFW_STEP_CRCPKT	 '2'
#define UPGFW_STEP_FIRSTPKT 	 	0
#define UPGFW_STEP_LASTPKT	 		1
#define UPGFW_STEP_CRCPKT	 		2
#define UPGFW_PKT1K		 			1024
#define UPFW_SEND_BIN_FAILURE		-1
#define UPFW_SEND_BIN_SUCCESS		0
//#define DEBUG_RESET_MANUAL

/*...........................................................................
. Structures                                                                .
............................................................................*/

#ifdef MCF85XX
typedef struct CAN_BINBUFFIDtag{
	uint8_t SMBlock:1;
	uint8_t AckWait:1;
	uint8_t CrcCheck:1;
	uint8_t TimeStamp:1;
	uint8_t IDSrc:4;
}CAN_BINBUFFID;
#else
typedef struct CAN_BINBUFFIDtag{
	uint8_t IDSrc:4;
	uint8_t TimeStamp:1;
	uint8_t CrcCheck:1;
	uint8_t AckWait:1;
	uint8_t SMBlock:1;
}CAN_BINBUFFID;
#endif

#ifdef MCF85XX
typedef struct CAN_BINBUFFTYPEtag{
	uint8_t ProtData:1;
	uint8_t SMPackets:1;
	uint8_t TypePkt:6;
}CAN_BINBUFFTYPE;
#else
typedef struct CAN_BINBUFFTYPEtag{
	uint8_t TypePkt:6;
	uint8_t SMPackets:1;
	uint8_t ProtData:1;
}CAN_BINBUFFTYPE;
#endif

typedef union CAN_BINBUFFTIMEDATAtag{
	uint8_t Data[4];
	uint32_t TimeStamp;
}CAN_BINBUFFTIMEDATA;

#ifdef MCF85XX
typedef struct uCAN_BIN_BUFF_EXTtag{
	uint8_t Spare1:1;
	uint8_t Spare2:1;
	uint8_t Etx:2;
	uint8_t BytePadding:4;
}uCAN_BIN_BUFF_EXT;
#else
typedef struct uCAN_BIN_BUFF_EXTtag{
	uint8_t BytePadding:4;
	uint8_t Etx:2;
	uint8_t Spare2:1;
	uint8_t Spare1:1;
}uCAN_BIN_BUFF_EXT;
#endif

typedef union CAN_BINBUFF_ETXtag{
	uCAN_BIN_BUFF_EXT uEtxPkt;
	uint8_t EtxPkt;
}CAN_BINBUFF_ETX;

 // containing the binary protocol structure
typedef struct CAN_BINBUFF_HEADERtag{
	uint8_t   		CntSTX;    // Spare
	CAN_BINBUFFID 	ID;  	   // ID
	CAN_BINBUFFTYPE Type;      // Type
	uint8_t   		Spare;     // Spare
	CAN_BINBUFFTIMEDATA TimeData;  // ID format
}CAN_BINBUFF_HEADER;

 // containing the binary protocol structure
typedef struct CAN_BINBUFF_FOOTERtag{
	uint8_t   		CntETX;    // Spare
	CAN_BINBUFF_ETX	CanEtx;
	uint16_t		NumBlock;
	uint16_t		NumTotBlocks;
	uint16_t		CRC16;
}CAN_BINBUFF_FOOTER;

// containing the binary data structure
typedef struct CAN_BINBUFF_DATAtag{
   uint32_t  	low_threshold;
   uint32_t	high_threshold;
   uint16_t	tot_sample;
   uint16_t  	motion_num;
   uint8_t  	branch_type;
   uint8_t  	pos;
   uint8_t  	motion_type;
   uint8_t    	result_flag;
   uint64_t	area;
   int64_t	    integral;
   uint32_t	first_sample_val;
   uint32_t	last_sample_val;
   uint32_t	max_sample_val;
   uint32_t	max_sample_pos;
   uint32_t	min_sample_val;
   uint32_t	min_sample_pos;
   uint32_t	spare;
}CAN_BINBUFF_DATA;

// containing the binary data structure
typedef struct CAN_BINBUFF_STXtag{
   uint32_t  	Stx;
   uint32_t  	CanID;
   uint32_t  	TimeStamp;
   uint32_t	Len;
}CAN_BINBUFF_STX;

typedef struct CAN_BINBUFF_FILEDATAtag{
	uint8_t  	CntPkt;
	uint8_t  	Data[DATA_LEN_BINBUFF];
}CAN_BINBUFF_FILEDATA;

//structure in the INSTRUMENT DATA
 // containing the binary data structure
typedef struct CAN_BINBUFF_STOREtag{
	CAN_BINBUFF_STX	CanStx;
	CAN_BINBUFF_HEADER	CanHeader;
	CAN_BINBUFF_DATA	CanData;
	CAN_BINBUFF_FOOTER	CanFooter;
}CAN_BINBUFF_STORE;

 // containing the binary Protocol structure
typedef struct CAN_BINBUFF_PROTtag{
	CAN_BINBUFF_STX	CanStx;
	CAN_BINBUFF_HEADER	CanHeader;
}CAN_BINBUFF_PROT;

typedef enum
{
	FWUPG_CHECK_FILE	= 0,
	FWUPG_RESET_BOARD	= 1,
	FWUPG_STATUS_SCT	= 2,
	FWUPG_SEND_1PKT		= 3,
	FWUPG_SEND_FILEBIN	= 4,
	FWUPG_SEND_CRC16	= 5,
	FWUPG_SEND_CHECKANDRESET = 6,
	FWUPG_STATUS_END	= 7,
}enumFwUpgradeState;

#endif // FWUPGRADESECTIONCOMMONINCLUDE_H
