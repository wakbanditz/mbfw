#ifndef SECTIONBOARDMANAGER_H
#define SECTIONBOARDMANAGER_H

#include "Thread.h"
#include "StaticSingleton.h"
#include "Loggable.h"
#include "Semaphore.h"

#include "SectionInclude.h"
#include "CommonInclude.h"
#include "StatusInclude.h"
#include "FWUpdateCommonInclude.h"
#include "Operation.h"

class SectionBoardLink;

class SectionBoardManager :		public StaticSingleton<SectionBoardManager>,
								public Thread,
								public Loggable
{
	public:

		/*! ***********************************************************************************************************
		 * @brief SectionBoardManager constructor
		 * ************************************************************************************************************
		 */
		SectionBoardManager();

		/*! ***********************************************************************************************************
		 * @brief SectionBoardManager destructor
		 * ************************************************************************************************************
		 */
		virtual ~SectionBoardManager();

		/*! ***********************************************************************************************************
		 * @brief set the ID of the Section
		 * @param sectionId the ID as defined in CommonInclude.h
		 * ************************************************************************************************************
		 */
		void setSectionId(int sectionId);

		/*! ***********************************************************************************************************
		 * @brief set the communication link pointer of the Section
		 * @param pSectionLink the pointer to the SectionBoardLink object
		 * ************************************************************************************************************
		 */
		void setSectionLink(class SectionBoardLink * pSectionLink);

		/*! ***********************************************************************************************************
		 * @brief to manage the asynchronous events from the Section board
		 *		set as callback function for the subcomponents of the SectionLink object
		 * @param device the device that signal the event (as defined in SectionInclude.h)
		 * @param event the event identifier (as defined in SectionInclude.h)
		 * @return 0 in case of success, error code otherwise
		 * ************************************************************************************************************
		 */
		int manageSectionEvents(int device, int event);

		/*! ***********************************************************************************************************
		 * @brief to set the action the thread will execute
		 * @param action the action code
		 * @return true if action is accepted, false otherwise
		 * ************************************************************************************************************
		 */
		bool setSectionAction(int action);

		/*! ***********************************************************************************************************
		 * @brief to set the action the thread will execute
		 * @param action the action code (as defined in SectionInclude.h)
		 * @param pOp poiter to a valid Operation
		 * @return the new status of the operation
		 * ************************************************************************************************************
		 */
		int setSectionAction(int action, Operation* pOp);

		/*! ***********************************************************************************************************
		 * @brief to check if an action is already ongoing for the section
		 * @return eOperationWaitForBirth if no operation is active, eOperationError otherwise
		 * ************************************************************************************************************
		 */
		int checkIsPossibleSectionAction();

		bool isActionInProgress();

		bool isOperationInProgress();

		/*! ***********************************************************************************************************
		 * @brief to set the status of the section (and so also of the module)
		 * @param status the status (as defined in StatusInclude.h)
		 * ************************************************************************************************************
		 */
		void updateStatus(eDeviceStatus status);

		/*! ***********************************************************************************************************
		 * @brief updateErrorsTime to set the correct time of the errors occurred when time was not yet set
		 * @param currentMsec the startup time in msecs
		 * @param currentTime pointer to current time structure
		 * @return the number of changed items
		 * ************************************************************************************************************
		 */
		int updateErrorsTime(uint64_t currentMsec, struct tm * currentTime);

		/*! ***********************************************************************************************************
		 * @brief clearSectionPressureErrors clears all the pressure errors of the section
		 * ************************************************************************************************************
		 */
		void clearSectionPressureErrors();

		/*! ***********************************************************************************************************
		 * @brief getErrorList retrieve the errors active on the section: each error corresponds to an element
		 *			in the vector: the string is formatted as timestamp#code#category#description
		 * @param pVectorList pointer to the list
         * @param errorLevel the level error by which the errors are filtered
         * @return the number of elements in the vector
		 * ************************************************************************************************************
		 */
        int getErrorList(std::vector<std::string> * pVectorList, uint8_t errorLevel = GENERAL_ERROR_LEVEL_NONE);

		/*! ***********************************************************************************************************
		 * @brief getPressureErrorListPerStripWell retrieve the pressure errors active on the section
		 *				selecting the well and strip
		 *
		 * @param pVectorList pointer to the list
		 * @param strip the strip index (as char '0'-'5')
		 * @param well the strip index (as char '0'-'9')
		 * @return the number of elements in the vector
		 * ************************************************************************************************************
		 */
		int getPressureErrorListPerStripWell(std::vector<string>* pVectorList, uint8_t strip = SCT_NUM_TOT_SLOTS,
																				uint8_t well = SCT_SLOT_NUM_WELLS);

        /*! ***********************************************************************************************************
         * @brief requestCheckErrorsStatus set the action to check the erros on section (call to CheckErrorsStatus)
         *
         * @return 0 if success
         * ************************************************************************************************************
         */
        int requestCheckErrorsStatus();

        /*! ***********************************************************************************************************
         * @brief requestMoveSectionMotors called by ReadSec procedure to position the Section motors
         * @param vMotors array of motors - position list
         * @return true if success, false otherwise
         * ************************************************************************************************************
         */
        bool requestMoveSectionMotors(vector<pair<eSectionMotor, string>> vMotors);

        void test(void);
        int sendTestValue(uint8_t strip, uint8_t volume, uint8_t speed,
                        uint64_t low, uint64_t high, uint16_t usTimeoutMs=5000);

    protected:

		/*! ***********************************************************************************************************
		 * @brief workerThread neverending thread normally waiting on the semaphore : when the semaphore it's released
		 *		the thread executes the procedure
		 * @return 0 when thread ends
		 * ***********************************************************************************************************
		 */
		int workerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions before the start of the thread
		 * ***********************************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions after the end of the thread
		 * ***********************************************************************************************************
		 */
		void afterWorkerThread(void);

	private:

		/*! ***********************************************************************************************************
		 * @brief to get the status of the section
		 * @return the status (as defined in StatusInclude.h)
		 * ************************************************************************************************************
		 */
		eDeviceStatus getStatus();

		/*! ***********************************************************************************************************
		 * @brief actions executed at section startup wehn board ready
         * @return true if no erros detected, false otherwise
		 * ************************************************************************************************************
		 */
        bool  runInitializationSequence();

        /*! ***********************************************************************************************************
         * @brief checkErrorsStatus checks if errors are active for the section: if not it updates the section status
         *               and led and notifies to gateway
         *
         * @return the number of errors active
         * ************************************************************************************************************
         */
        int checkErrorsStatus();


		FwUpdateInfo* getUpdateInfo(Operation* pOp);

		void updateInstrInfoRelease(enumUpdType eType, string& strRel);

		int setUpdateOutcome(FwUpdateInfo* pInfo, bool bRes);

		int updateEeprom(Operation* pOp);

		int moveMotors(void);

    public:

        class SectionBoardLink * m_pSectionLink;

    private :

		int m_sectionId;
		int m_slSectionAction;
        bool m_bCheckErrorsRequest;
		Semaphore m_synchSem;
		Operation*	m_pOperation;
		vector<pair<eSectionMotor, string>> m_vMovementList;
};

/*! ***********************************************************************************************************
 * @brief wrapper to manage the asynchronous events from the Section board
 *		set as callback function for the subcomponents of the SectionLink object
 * @param section the section index to identify the object to be invoked
 * @param device the device that signal the event (as defined in SectionInclude.h)
 * @param event the event identifier (as defined in SectionInclude.h)
 * @return 0 in case of success, error code otherwise
 * ************************************************************************************************************
 */
int manageSectionsEvents(int section, int device, int event);


#endif // SECTIONBOARDMANAGER_H
