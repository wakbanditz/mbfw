/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FwUpgrade.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the FwUpgrade class.
 @details

 ****************************************************************************
*/

#include "FwUpgrade.h"
#include "MainExecutor.h"
#include "PCA9633.h"
#include "CanLink.h"
//#include "File.h"
#include "FolderManager.h"
#include "CRCFile.h"
#include "Common/crc16.h"
#include "Common/swap.h"

#define STATUS_REQUEST_TIMEOUT				100	// 10 * msec timeout for the status request
#define INFO_FL2_WAITUPGOPER				'C'

#define UPGFW_TYPE_BOOT						0
#define UPGFW_TYPE_APPL						1
#define UPGFW_TYPE_FPGA						2


#define UPGFW_STEP_FIRSTPKT					0
#define UPGFW_STEP_LASTPKT					1
#define UPGFW_STEP_CRCPKT					2
#define UPGFW_PKT1K							1024

FwUpgrade::FwUpgrade()
{
	m_eStateFwUpg = FWUPG_CHECK_FILE;
    m_strSourcePath.clear();
	m_pSectionLink = NULL;
	m_ulfileSize = 0;
	m_usnumBlocks = 0;
	m_crc16File = 0;

}

FwUpgrade::~FwUpgrade()
{

}

int FwUpgrade::setSectionLink(SectionBoardLink* pSectionLink)
{
	if (pSectionLink == NULL)
	{
		return -1;
	}
	else
	{
		m_pSectionLink = pSectionLink;
	}
	
	return 0;
}

int FwUpgrade::resetSectionUpg(uint8_t lSectionID)
{
    int res = 0;

    switch (lSectionID)
    {
        case SCT_A_ID:
        {
            res = MainExecutor::getInstance()->m_i2cBoard.resetSectionBoard(SCT_A_ID);
            log(LOG_INFO, "FwUpgrade::ResetSectionUpg-->RESET SECTION A DONE ");
        }
        break;

        case SCT_B_ID:
        {
            res = MainExecutor::getInstance()->m_i2cBoard.resetSectionBoard(SCT_B_ID);
            log(LOG_INFO, "FwUpgrade::ResetSectionUpg-->RESET SECTION B DONE ");
        }
        break;

        default:
            log(LOG_ERR, "FwUpgrade::ResetSectionUpg-->section ID <%l> not present ", lSectionID);
        break;
    }

    return res;
}

int FwUpgrade::checkFile(string &source_path, uint32_t &ulFileSize, uint16_t &crc16Res, uint32_t ulcrc32ToCheck )
{
	int   err;
	unsigned long crc32file;
	bool fileOK;
	uint16_t crc16;

    // ///////////////////////////
	//check file to upgrade
	fileOK=false;
    FolderManager fileImage;
//	fileImage.setLogger(m_pLogger);
	CRCFile fileImageCrc(m_pLogger);
	fileImage.loadFileName(source_path);
	if (fileImage.exists())
	{
		//check crc of the file
        err = fileImageCrc.calculateCRCFileMaxAlloc(source_path, crc32file, CRCFile::eMaxAllocMemoryFile);
        if (err == 0)
		{
            (void)ulcrc32ToCheck;
            // !!! TODO insert control with a passed crc32
//			if (crc32file == ulcrc32ToCheck)
//			{
                fileOK = true;
//			}
//			else
//			{
//				return -1;
//			}
		}
		else
		{
			//TODO
			return -1;
		}

		//Get size
		unsigned long ulSize = 0;
		fileImage.getFileLen(ulSize);
		ulFileSize = ulSize;
		log(LOG_INFO, "FwUpgrade::checkFile--> file size: %i", ulSize);
	}
	else
	{
        log(LOG_INFO, "FwUpgrade::checkFile--> file not present");
		return -1;
	}

	if (fileOK==true)
	{
		//calculate crc16 of the file in order to transmit to the processlane
		crc16=CRC16_INIT_VALUE0;//to debug initial value TODO
		err=fileImageCrc.CRC16FileMaxAlloc(source_path,crc16,CRCFile::eMaxAllocMemoryFile);

		if (err!=SUCCESS)
		{
			log(LOG_ERR, "FwUpgrade::checkFile--> CRC16 calc error");
			return -1;
		}

		crc16Res = crc16;
		log(LOG_INFO, "FwUpgrade::checkFile--> CRC16 file BIN: %i", crc16);
	}

	return 0;
}

int FwUpgrade::updFirmwareCmd(enumUpdType ubTypeMemory, uint8_t ubStep, uint32_t ulLenFile, uint16_t crc16,
                                                          uint16_t &pNumBlock, uint8_t &ubStatusRes)
{
	uint16_t	usNumPkt1K;
	uint8_t		ucStepAscii;
	uint8_t		ucTypeMemoryAscii;
	uint8_t		ubStatusUpg = 0;
	int			res;

	switch (ubTypeMemory)
	{
            case eBootLoader:
                ucTypeMemoryAscii=CAN0_UPGFW_TYPE_MEM_BOOT;
			break;
            case eApplicative:
                ucTypeMemoryAscii=CAN0_UPGFW_TYPE_MEM_APPL;
			break;
            case eFPGA:
                ucTypeMemoryAscii=CAN0_UPGFW_TYPE_MEM_FPGA;
			break;
            default:
                log(LOG_ERR, "FwUpgrade::updFirmwareCmd-->Type memory not present");
                return -1;
			break;
	}

	switch (ubStep)
	{
		case UPGFW_STEP_FIRSTPKT:
			ucStepAscii=CAN0_UPGFW_STEP_FIRSTPKT;
			usNumPkt1K=(ulLenFile/UPGFW_PKT1K);
			if ((ulLenFile%UPGFW_PKT1K)!=0)
			{
				usNumPkt1K++;
			}

			pNumBlock=usNumPkt1K;
			log(LOG_INFO, "FwUpgrade::updFirmwareCmd--> Type:%c, Step:%c, Blocks:%d",	ucTypeMemoryAscii,
																						ucStepAscii,
																						usNumPkt1K );
			res = m_pSectionLink->m_pGeneral->uploadFirmware(ucTypeMemoryAscii, ucStepAscii, usNumPkt1K, ubStatusUpg);
			ubStatusRes = ubStatusUpg;
			if (res != 0)
			{
				log(LOG_ERR, "FwUpgrade::updFirmwareCmd-->ERROR on step UPGFW_STEP_FIRSTPKT");
				return -1;
			}
			log(LOG_INFO, "FwUpgrade::updFirmwareCmd--> status response %c", ubStatusUpg);
			//TODO do the control of the status update response
			break;

		case UPGFW_STEP_LASTPKT:
			ucStepAscii=CAN0_UPGFW_STEP_LASTPKT;
			//TODO
			break;

		case UPGFW_STEP_CRCPKT:
			ucStepAscii=CAN0_UPGFW_STEP_CRCPKT;
			res = m_pSectionLink->m_pGeneral->uploadFirmware(ucTypeMemoryAscii, ucStepAscii, crc16, ubStatusUpg);
			ubStatusRes = ubStatusUpg;
			if (res != 0)
			{
				log(LOG_ERR, "FwUpgrade::updFirmwareCmd-->ERROR on step UPGFW_STEP_CRCPKT");
				return -1;
			}
			log(LOG_INFO, "FwUpgrade::updFirmwareCmd--> response status:%c", ubStatusUpg);
			break;

		default:
			return -1;
			break;
	}


	return 0;
}

int FwUpgrade::sendUpFwMesg(uint8_t * pubTxBuff, uint8_t ubwaitAck, uint16_t numLenToSend, uint8_t &uberrorCode)
{
	uint8_t	ubmsgAnsData[MQ_CANBOARD_MAX_MSG_SIZE];
	memset(ubmsgAnsData, 0, MQ_CANBOARD_MAX_MSG_SIZE);

	int res = m_pSectionLink->sendMsgBinProt(pubTxBuff, ubmsgAnsData, ubwaitAck, numLenToSend);
	if (res == 0)
	{
		if (ubwaitAck == ID_ACKWAIT)
		{
			uberrorCode = ubmsgAnsData[2];
		}
	}
	else
	{
		log(LOG_INFO, "FwUpgrade::sendUpFwMesg--> error on receive msg bin protocol");
		uberrorCode = TYPE_PROTABORT;
		return -1;
	}
	return 0;
}

int FwUpgrade::sendFileBin(int NumberBlock, string &pfilename, uint16_t &referr_code)
{
	int				err ;
	uint16_t		crc16;
	uint32_t		idxMemBuf;
	uint16_t		idxBlock;
	uint16_t		idxDataBlock;
	uint16_t		idxCntSubBlock;

	uint8_t			ubmsgPayload[MQ_CANBOARD_MAX_MSG_SIZE];
	memset(ubmsgPayload, 0, MQ_CANBOARD_MAX_MSG_SIZE);

	// All the hereunder storages are static for speed reasons
	FILE					*pFileUpgFW;
	uint32_t				lenFile,EndPerc,lastPerc,modLen,idxRetryBlock;
//	uint64_t				timeout;
	////////////////////////////////////////////////////////////
	//CTime					timePacket;
//	uint64_t				timePacketsec = 0;
	CAN_BINBUFF_HEADER		CanHeaderFWUPD;
	CAN_BINBUFF_FILEDATA 	*pCanDataFWUPD = new CAN_BINBUFF_FILEDATA[MAX_CNT_CAN_BIN_PROT];
	unsigned char			*pBufferSplitFileData = new unsigned char[MAX_LEN_SPLIT_BLOCK];
	CAN_BINBUFF_FOOTER		CanFooterFWUPD;
	////////////////////////////////////////////////////////////
	err=UPFW_SEND_BIN_FAILURE;
	/*..........................................................................*/
	referr_code=FWUPDATE_NOERROR;
	crc16=0;
	idxMemBuf=0;
	/*..........................................................................
	  . Check File to upload                                                   .
	  .........................................................................*/
	referr_code=FWUPDATE_ERROPENFILE;
	pFileUpgFW=NULL;
	if ( (pCanDataFWUPD!=NULL) &&		//check memory allocated for pCanDataFWUPD
		 (pBufferSplitFileData!=NULL) ) //check memory allocated for pBufferSplitFileData
	{
		pFileUpgFW = fopen(pfilename.c_str(),"rb");
	}

	if (pFileUpgFW!=NULL)
	{
		fseek(pFileUpgFW,0,SEEK_END);
		lenFile=ftell(pFileUpgFW);
		modLen=lenFile%MAX_LEN_SPLIT_BLOCK;
		fseek(pFileUpgFW,0,SEEK_SET);
		referr_code=FWUPDATE_NOERROR;
		if (lenFile==0)
		{
			log(LOG_ERR, "FwUpgrade::uploadFwManager-->Error opening file %s", pfilename.c_str());
			referr_code=FWUPDATE_ERROPENFILE;
		}
	}
	else
	{
		if (referr_code==FWUPDATE_ERROPENFILE)
		{
	//		fprintf(console,"Error opening file %s\n",pfilename.c_str());
		}
	}
	////////////////////////
	lastPerc=0;
	EndPerc=0;
	if ((referr_code==FWUPDATE_NOERROR)&&(pFileUpgFW!=NULL))
	{
		// memset(&fPosVect,0,sizeof(fPosVect));
		/*!***************************************************************************/
		for ( idxBlock=0; idxBlock<NumberBlock; idxBlock++ )
		{
            log(LOG_INFO,"FwUpgrade::sendFileBin-->NUM BLOCK %i", idxBlock);
			int idxDataFill=0;
			int readLenBlock;
			// prepare next block to send
			readLenBlock	= fread(pBufferSplitFileData,1,MAX_LEN_SPLIT_BLOCK,pFileUpgFW);
			idxMemBuf		= ftell(pFileUpgFW);
			EndPerc			= (uint32_t)((idxMemBuf*100)/lenFile);
			if ( lastPerc != EndPerc )
			{
				lastPerc=EndPerc;
			}

			if ( readLenBlock < 0 )
			{
				//error occurs
				referr_code		= TYPE_PROTABORT;
				idxRetryBlock	= MAX_RETRY_BLOCK;
				idxBlock		= NumberBlock;
				continue;
			}

			for ( idxRetryBlock=0; idxRetryBlock<MAX_RETRY_BLOCK; idxRetryBlock++)
			{
				/*!***************************************************************************
				 * HEADER
				 * ***************************************************************************/
				CanHeaderFWUPD.CntSTX=CAN_BINPROT_STX;
				CanHeaderFWUPD.ID.SMBlock=ID_MULTIBLOCK;
				CanHeaderFWUPD.ID.AckWait=ID_ACKWAIT;
				CanHeaderFWUPD.ID.CrcCheck=ID_CHECKCRC;
				CanHeaderFWUPD.ID.TimeStamp=1;
				CanHeaderFWUPD.ID.IDSrc=0;
				CanHeaderFWUPD.Type.ProtData=TYPE_BINDATA;
				CanHeaderFWUPD.Type.SMPackets=TYPE_BINMULTIPKT;
				CanHeaderFWUPD.Type.TypePkt=TYPE_PKT_FILE_DATA;
				struct timeval timeStamp;
				gettimeofday(&timeStamp, NULL);
				time_t curr_sec = timeStamp.tv_sec;
//				int	curr_msec = timeStamp.tv_usec / 1000;
				CanHeaderFWUPD.TimeData.TimeStamp=curr_sec;  // ID format
				Swap32((unsigned char*)&CanHeaderFWUPD.TimeData.TimeStamp);

				////transmit packet
				memcpy(&ubmsgPayload[0],&CanHeaderFWUPD, sizeof(CanHeaderFWUPD));
				sendUpFwMesg(ubmsgPayload, CanHeaderFWUPD.ID.AckWait, DIM_SECTIONLINK_BIN_PROTO_SIZE,(uint8_t&)referr_code);

				if ((referr_code==TYPE_PROTNACK) ||(referr_code==TYPE_PROTABORT))
				{
					// abort sequence problem on canbus
					err=UPFW_SEND_BIN_FAILURE;
					if (referr_code==TYPE_PROTABORT)
					{
						idxRetryBlock=MAX_RETRY_BLOCK;
						idxBlock=NumberBlock;
					}break;
				}

				/*!***************************************************************************
				 * DATA
				 * ***************************************************************************/
				// reset index of buffers
				idxCntSubBlock=1;
				idxDataFill=0;
				crc16=0;
				// copy data block in subblocks
				for (idxDataBlock=0; idxDataBlock<readLenBlock; idxDataBlock++)
				{
//					usleep(50);
					pCanDataFWUPD[idxCntSubBlock].CntPkt			= idxCntSubBlock;
					pCanDataFWUPD[idxCntSubBlock].Data[idxDataFill]	= pBufferSplitFileData[idxDataBlock];
					crc16_seq((short*)&crc16, pBufferSplitFileData[idxDataBlock]);
					CanFooterFWUPD.CRC16=crc16;
					idxDataFill++;
					if (idxDataFill>=DATA_LEN_BINBUFF)//7
					{
						//////////////////////////////////////////////////
	//					timeout=CProcessLaneBoardGeneral::eTimeoutCmdFwUpg;
	//					CTime::Convertms2Time(timeout,txMsg.Timeout);
						/////////////////////////////////////////////////
						////transmit packet
						memcpy(&ubmsgPayload[0],&pCanDataFWUPD[idxCntSubBlock], DIM_SECTIONLINK_BIN_PROTO_SIZE);
						sendUpFwMesg(ubmsgPayload, ID_ACKWAIT, DIM_SECTIONLINK_BIN_PROTO_SIZE,(uint8_t&)referr_code);
						if ((referr_code==TYPE_PROTNACK) ||(referr_code==TYPE_PROTABORT))
						{
							// abort sequence problem on canbus
							log(LOG_ERR,"FwUpgrade::sendFileBin--> ERROR referr_code 0x%x", referr_code);
							err=UPFW_SEND_BIN_FAILURE;
							break;
						}
						idxDataFill=0;
						// increase counter
						if (idxCntSubBlock>=CAN_BINPROT_CON)
							idxCntSubBlock=0;
						idxCntSubBlock++;
					}
				}
				if (referr_code!=TYPE_PROTACK)
				{
					referr_code=TYPE_PROTABORT;
					idxRetryBlock=MAX_RETRY_BLOCK;
					idxBlock=NumberBlock;
					continue;
				}
				/*!***************************************************************************
				 * LAST DATA PACKET
				 * ***************************************************************************/
				modLen=0;
				// rest of packets to complete the chunk 1k
				if (idxDataFill>0)
				{
					//////////////////////////////////////////////////
//					timeout=CProcessLaneBoardGeneral::eTimeoutCmdFwUpg
//					CTime::Convertms2Time(timeout,txMsg.Timeout);
					/////////////////////////////////////////////////
					////transmit packet
					memcpy(&ubmsgPayload[0],&pCanDataFWUPD[idxCntSubBlock], DIM_SECTIONLINK_BIN_PROTO_SIZE);
					sendUpFwMesg(ubmsgPayload, ID_ACKWAIT, (idxDataFill+1), (uint8_t&)referr_code);
					if ((referr_code==TYPE_PROTNACK) || (referr_code==TYPE_PROTABORT))
					{
						// abort sequence problem on canbus
						err=UPFW_SEND_BIN_FAILURE;
						break;
					}
					if (referr_code!=TYPE_PROTACK)
					{
						referr_code=TYPE_PROTABORT;
						idxRetryBlock=MAX_RETRY_BLOCK;
						idxBlock=NumberBlock;
						continue;
					}
				}
				/*!***************************************************************************
				 * FOOTER
				 * ***************************************************************************/
				CanFooterFWUPD.CntETX=CAN_BINPROT_ETX;    // Spare
				CanFooterFWUPD.CanEtx.EtxPkt=0;
				CanFooterFWUPD.CanEtx.uEtxPkt.Spare1=0;
				CanFooterFWUPD.CanEtx.uEtxPkt.Spare2=0;
				CanFooterFWUPD.CanEtx.uEtxPkt.Etx=ETX_BINPROT;
				CanFooterFWUPD.CanEtx.uEtxPkt.BytePadding=modLen;
				CanFooterFWUPD.NumBlock=(idxBlock+1);
				CanFooterFWUPD.NumTotBlocks=NumberBlock;
				// Swap necessary for Linux issues
				CanFooterFWUPD.NumBlock = swap16Linux(CanFooterFWUPD.NumBlock);
				CanFooterFWUPD.NumTotBlocks = swap16Linux(CanFooterFWUPD.NumTotBlocks);
				CanFooterFWUPD.CRC16 = swap16Linux(CanFooterFWUPD.CRC16);
				//
				// last block file
				////////////////////////////////////////
				////assemble packet
				//////////////////////////////////////////////////
	//			timeout=CProcessLaneBoardGeneral::eTimeoutCmdFwUpg;
	//			CTime::Convertms2Time(timeout,txMsg.Timeout);
				/////////////////////////////////////////////////
				////transmit packet
				memcpy(&ubmsgPayload[0],&CanFooterFWUPD, DIM_SECTIONLINK_BIN_PROTO_SIZE);
				sendUpFwMesg(ubmsgPayload, ID_ACKWAIT, DIM_SECTIONLINK_BIN_PROTO_SIZE, (uint8_t&)referr_code);
				if ((referr_code==TYPE_PROTCRCERR) ||(referr_code==TYPE_PROTABORT))
				{
					// abort sequence problem on canbus
					err=UPFW_SEND_BIN_FAILURE;
					if (referr_code==TYPE_PROTABORT)
					{
						idxRetryBlock=MAX_RETRY_BLOCK;
						idxBlock=NumberBlock;
						break;
					}
					if (referr_code==TYPE_PROTCRCERR)
					{
						//retry to send block
						if (idxRetryBlock>=(MAX_RETRY_BLOCK-1))
						{
							referr_code=TYPE_PROTABORT;
							idxRetryBlock=MAX_RETRY_BLOCK;
							idxBlock=NumberBlock;
							break;
						}
					}
				}
				else
				{
					if ((referr_code==TYPE_PROTACK)||(referr_code==TYPE_PROTCRCOK))
					{
						// exit rery block
						break;
					}
				}
			}// max retry block
		}
	}
	/////////////////////////
	//close file
	if (pFileUpgFW!=NULL)
	{
		fclose(pFileUpgFW);
		pFileUpgFW=NULL;
	}
	//deallocate memory buffers
	if (pCanDataFWUPD!=NULL)
	{
		delete [] pCanDataFWUPD;
		pCanDataFWUPD=NULL;
	}

	if (pBufferSplitFileData!=NULL)
	{
		delete [] pBufferSplitFileData;
		pBufferSplitFileData=NULL;
	}

	//completed
	if (referr_code==FWUPDATE_NOERROR)
	{
		err=UPFW_SEND_BIN_SUCCESS;
		log(LOG_INFO,"FwUpgrade::sendFileBin--> Upload Section completed");
	}
	else
	{
		log(LOG_ERR,"FwUpgrade::sendFileBin--> Upload Section ERROR");
	}
	return err;
}

int FwUpgrade::uploadFwManager(enumUpdType fwtype, string &source_path, long length, unsigned long crc32)
{
	int res = 0;
	int ierr = eFirmwareUpdateWait;
	uint8_t status;
	m_strSourcePath = source_path;

    // TODO length of file can be passed by parameters or can be retrieved by checkFile function
    (void) length;

	switch (m_eStateFwUpg)
	{
		case FWUPG_CHECK_FILE:
		{
			res = checkFile(m_strSourcePath, m_ulfileSize, m_crc16File, crc32);
			if (res == 0)
			{
				m_eStateFwUpg = FWUPG_RESET_BOARD;
				log(LOG_INFO, "FwUpgrade::uploadFwManager-->check file %s OK", m_strSourcePath.c_str());
			}
			else
			{
				ierr = eFirmwareUpdateErr;
			}
		}
		break;

		case FWUPG_RESET_BOARD:
		{
#ifdef DEBUG_RESET_MANUAL
			log(LOG_INFO, "FwUpgrade::uploadFwManager-->Push RESET button");
			msleep(2000);
			m_eStateFwUpg = FWUPG_STATUS_SCT;
			log(LOG_INFO, "FwUpgrade::uploadFwManager-->Reset section board ok");
#else
            res = resetSectionUpg(m_pSectionLink->getSectionBoardNumber());
			if (res == 0)
			{
				m_eStateFwUpg = FWUPG_STATUS_SCT;
				log(LOG_INFO, "FwUpgrade::uploadFwManager-->Reset section board ok");
			}
			else
			{
				ierr = eFirmwareUpdateErr;
			}
#endif
		}
		break;

		case FWUPG_STATUS_SCT:
		{
			// first ask status to the board
			res = m_pSectionLink->m_pGeneral->getInternalStatus(status, STATUS_REQUEST_TIMEOUT);
			if ( res == 0)
			{
				log(LOG_INFO, "FwUpgrade::uploadFwManager--> Internal Section status %c", status);
				if (status == ePclbStateWaitUpgOperativePort)
				{
					log(LOG_INFO, "FwUpgrade::uploadFwManager--> UPG state");
					m_eStateFwUpg = FWUPG_SEND_1PKT;
				}
                // TODO the others states
			}
			else
			{
				log(LOG_ERR, "FwUpgrade::uploadFwManager--> Internal Section status %c", status);
				ierr = eFirmwareUpdateErr;
			}

			msleep(1000);
		}
		break;

		case FWUPG_SEND_1PKT:
		{
            // TODO currently only application file, crc to insert
			log(LOG_INFO, "FwUpgrade::uploadFwManager--> FWUPG_SEND_1PKT state");
			res = updFirmwareCmd(fwtype, UPGFW_STEP_FIRSTPKT, m_ulfileSize, 0, m_usnumBlocks, status);
			if (res == 0)
			{
				log(LOG_INFO, "FwUpgrade::uploadFwManager--> UPFW 1Pkt sent");
				m_eStateFwUpg = FWUPG_SEND_FILEBIN;
			}
			else
			{
				log(LOG_ERR, "FwUpgrade::uploadFwManager--> UPFW 1Pkt didn't send");
				ierr = eFirmwareUpdateErr;
			}

            msleep(1000);
		}
		break;

		case FWUPG_SEND_FILEBIN:
		{
			// first ask status to the board
			log(LOG_INFO, "FwUpgrade::uploadFwManager--> FWUPG_SEND_FILEBIN state");
			uint16_t	userror;
			res = sendFileBin(m_usnumBlocks, m_strSourcePath, userror);
			if (res == 0)
			{
				m_eStateFwUpg = FWUPG_SEND_CRC16;
				log(LOG_INFO, "FwUpgrade::uploadFwManager--> go to FWUPG_SEND_CRC16 state");
			}
			else
			{
				log(LOG_ERR, "FwUpgrade::sendFileBin-->error");
				ierr = eFirmwareUpdateErr;
			}
			msleep(1000);
		}
		break;

		case FWUPG_SEND_CRC16:
		{
			log(LOG_INFO, "FwUpgrade::uploadFwManager--> FWUPG_SEND_CRC16 state");
			res = updFirmwareCmd(fwtype, UPGFW_STEP_CRCPKT, 0, m_crc16File, m_usnumBlocks, status);
			if (res == 0)
			{
				log(LOG_INFO, "FwUpgrade::uploadFwManager--> UPFW file BIN sent");

				switch (status)
				{
					case ePclUpgFwFlag2_CRCOK://eupUPG_FLG2_CRCOK:
					{
						/*......................................................................
						// flash burn is started
						.......................................................................*/
						//Can0_send_StatusErr(CAN0_UPD_COM_ID,LocalUpgFirmware.dest,LocalUpgFirmware.ans_dest,UPGFW_STARTED,0);
						log(LOG_INFO, "FwUpgrade::uploadFwManager-->UpgFirmware Flash Burned Started");
						m_eStateFwUpg = FWUPG_SEND_CHECKANDRESET;
					}
					break;
					case ePclUpgFwFlag2_CRCERROR://UPG_FLG2_CRCERROR:
					{
						//Can0_send_StatusErr(CAN0_UPD_COM_ID,LocalUpgFirmware.dest,LocalUpgFirmware.ans_dest,UPGFW_FAILED,CAN0_ERRCRC);
						log(LOG_INFO, "FwUpgrade::uploadFwManager-->CRCError");
					}
					break;
					default:
					{
						//Can0_send_StatusErr(CAN0_UPD_COM_ID,LocalUpgFirmware.dest,LocalUpgFirmware.ans_dest,UPGFW_FAILED,CAN0_ERRGENERAL);
						log(LOG_INFO, "FwUpgrade::uploadFwManager-->GeneralError");
					}break;
				}


				m_eStateFwUpg = FWUPG_SEND_CHECKANDRESET;
			}
			else
			{
				log(LOG_ERR, "FwUpgrade::uploadFwManager--> UPFW 1Pkt didn't send");
				ierr = eFirmwareUpdateErr;
			}

			msleep(1000);
		}
		break;

		case FWUPG_SEND_CHECKANDRESET:
		{
			/////////////////////////////////////
			//check status of the flashing procedure is completed
			// first ask status to the board
			int timeoutOpSection;
			int InfoStatusReceived = 0;
			status = 0;
			for ( timeoutOpSection = eTimeoutFlashBurn;timeoutOpSection>=0; timeoutOpSection--)
			{
				res = m_pSectionLink->m_pGeneral->getInternalStatus(status, STATUS_REQUEST_TIMEOUT);
				if ( res == 0)
				{
					log(LOG_INFO, "FwUpgrade::uploadFwManager--> FLASHING status %c", status);
					switch (status)
					{
						case ePclbStateWaitReset://INFO_FL2_WAITRESET:
						{

							//Can0_send_StatusErr(CAN0_UPD_COM_ID,LocalUpgFirmware.dest,LocalUpgFirmware.ans_dest,UPGFW_SUCCESSFUL,0);
							InfoStatusReceived=1;
							#ifdef DEBUG_RESET_MANUAL
										log(LOG_INFO, "FwUpgrade::uploadFwManager-->Push RESET button");
										msleep(2000);
										log(LOG_INFO, "FwUpgrade::uploadFwManager-->Reset section board ok");
							#else
                                        res = resetSectionUpg(m_pSectionLink->getSectionBoardNumber());
										if (res == 0)
										{
											m_eStateFwUpg = FWUPG_STATUS_SCT;
											log(LOG_INFO, "FwUpgrade::uploadFwManager-->Reset section board ok");
										}
										else
										{
											ierr = eFirmwareUpdateErr;
										}
							#endif
							log(LOG_INFO, "FwUpgrade::uploadFwManager-->Reset section board ok");
						}
						break;
						case ePclbStateFlashProgramming://INFO_FL2_FLASHPROG
						{
							log(LOG_INFO, "FwUpgrade::uploadFwManager-->upgrade flash in progress");
						}
						break;
						case ePclbStateBootFlashProgError://INFO_FL2_FLASHPROGERR)
						{
							log(LOG_INFO, "FwUpgrade::uploadFwManager-->error flash programming");
							InfoStatusReceived=-1;
						}
						break;
						case ePclbStateBootError://:INFO_FL2_BOOTERROR)
						{
							log(LOG_INFO, "FwUpgrade::uploadFwManager-->CAN0_ERRCRC to upload firware through CAN");
							InfoStatusReceived=-1;
						}
						break;
						case ePclbStateUploadError:
						{
							log(LOG_INFO, "FwUpgrade::uploadFwManager-->CAN0_ERRTRANFABORT through CAN");
							InfoStatusReceived=-1;
						}
						break;
						default:
						{
							log(LOG_INFO, "FwUpgrade::uploadFwManager-->CAN0_ERRTRANFABORT through CAN");
							InfoStatusReceived=-1;
						}
						break;
					}
				}
				else
				{
					log(LOG_ERR, "FwUpgrade::uploadFwManager--> getInternalStatus error");
				}

				/*......................................................................
				. Waits on the queue for any available message. Retrieve it and frees  .
				. the used buffer for late re-use.                                     .
				.......................................................................*/
				if (InfoStatusReceived!=0)
				{
					log(LOG_INFO, "FwUpgrade::uploadFwManager--> Status operation received: %i",InfoStatusReceived);
					break;
				}
				msleep(1000);
			}

			ierr=eFirmwareUpdateErr;
			if ((InfoStatusReceived==1)&&(timeoutOpSection>=0))
			{
				ierr = eFirmwareUpdateOK;
			}
		}
		break;

		default:
		{
			//TODO to delete the commet below
			log(LOG_INFO, "FwUpgrade::uploadFwManager--> Status unknown");
			msleep(1000);
		}
		break;
	}
	return ierr;
}


int FwUpgrade::updateFirmwareCan(enumUpdType fwtype, string &source_path, long length, unsigned long crc32)
{
	int ierr;

	ierr = eFirmwareUpdateWait;
	while (ierr == eFirmwareUpdateWait)
	{
		usleep(10);
		ierr = uploadFwManager(fwtype, source_path, length, crc32);
	}

	if (ierr == eFirmwareUpdateOK)
	{
		log(LOG_INFO, "FwUpgrade::updateFirmwareCan--> Update done");
	}
	else
	{
		log(LOG_INFO, "FwUpgrade::updateFirmwareCan--> Update ERROR");
	}


	return ierr;
}
