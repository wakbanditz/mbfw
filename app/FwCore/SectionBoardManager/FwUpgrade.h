#ifndef FWUPGRADE_H
#define FWUPGRADE_H

#include <string.h>

#include "FWUpdateCommonInclude.h"
#include "FwUpgradeSectionCommonInclude.h"
#include "Loggable.h"
#include "CommonInclude.h"

using namespace std;

class SectionBoardLink;

/*!
 * @brief The FwUpgrade class
 */
class FwUpgrade : public Loggable
{
	public:

        typedef enum {ePclbStateInit = '0',
                       ePclbStateIdle = '1',
                       ePclbStateMaint = '2',
                       ePclbStateProt = '3',
                       ePclbStateProtPaused = '4',
                       ePclbStateSpare1 = '5',
                       ePclbStateSpare2 = '6',
                       ePclbStateBootMode = '7',
                       ePclbStateUploadMode = '8',
                       ePclbStateOperativeMode = '9',
                       ePclbStateErrorMode = 'A',
                       ePclbStateWaitUpgDebugPort = 'B',
                       ePclbStateWaitUpgOperativePort = 'C',
                       ePclbStateFlashProgramming = 'D',
                       ePclbStateUploadError = 'E',
                       ePclbStateBootError = 'F',
                       ePclbStateWaitReset = 'G',
                       ePclbStateBootApplicationError = 'H',
                       ePclbStateBootRAMError = 'I',
                       ePclbStateBootFlashProgError = 'L',
                       ePclbStateSpare3 = 'M',
                       ePclbStateSpare4 = 'N'
		 }eTypePclbState;

        typedef enum {ePclUpgFwFlag2_INIT = '0',		//unit ready
                      ePclUpgFwFlag2_UNITNOTREADY = '1',//unit not ready
                      ePclUpgFwFlag2_UPGERROR = '2',	//Upgrade Error
                      ePclUpgFwFlag2_CRCOK = '3',		//CRC OK
                      ePclUpgFwFlag2_CRCERROR = '4'		//CRC Error
		}eTypePclbUpgFwFlag2;

        typedef enum {eWaitTimeGetStatus = 500,
                      eWaitBoardReset = 60000L,
                      eWaitTimeAfterReset = 2000,
                      eTimeoutRestart = (eWaitBoardReset / 1000),
                      eTimeoutFlashBurn = 240,
                      eTimeoutCmdFwUpg = 2000L
		  }eTypeFirmwareUpdate;

	private:
		enumFwUpgradeState	m_eStateFwUpg;
		string				m_strSourcePath;
		SectionBoardLink	*m_pSectionLink;

		uint32_t			m_ulfileSize;
		uint16_t			m_usnumBlocks;
		uint16_t			m_crc16File;

	public:
		/*!
		 * @brief FwUpgrade
		 */
		FwUpgrade();

		/*!
		 * @brief ~FwUpgrade
		 */
		virtual ~FwUpgrade();

		/*!
		 * @brief initSectionLink
		 * @param m_pSectionLink
		 * @return
		 */
		int setSectionLink(SectionBoardLink *m_pSectionLink);

		/*!
		 * @brief updateFirmwareCan
		 * @param fwtype
		 * @param source_path
		 * @param length
		 * @param crc32
		 * @return
		 */
        int updateFirmwareCan(enumUpdType fwtype, string &source_path, long length, unsigned long crc32);

	private:

		/*!
		 * @brief ResetSectionUpg
		 * @param IDSection
		 */
        int resetSectionUpg(uint8_t lSectionID);

		/*!
		 * @brief UploadFwManager
		 * @return
		 */
        int uploadFwManager(enumUpdType fwtype, string &source_path, long length, unsigned long crc32 );

		/*!
		 * @brief FwUpgrade::checkFile
		 * @param source_path
		 * @param crc32
		 * @return
		 */
		int checkFile(string &source_path, uint32_t &ulFileSize, uint16_t &crc16Res , uint32_t ulcrc32ToCheck);

		/*!
		 * @brief updFirmwareCmd
		 * @param ubTypeMemory
		 * @param ubStep
		 * @param ulLenFile
		 * @param crc16
		 * @param pNumBlock
		 * @return
		 */
        int updFirmwareCmd(enumUpdType ubTypeMemory, uint8_t ubStep, uint32_t ulLenFile, uint16_t crc16,
						   uint16_t &pNumBlock, uint8_t &ubStatusRes);

		/*!
		 * @brief sendFileBin
		 * @param NumberBlock
		 * @param pfilename
		 * @param referr_code
		 * @return
		 */
		int sendFileBin(int NumberBlock, string &pfilename, uint16_t &referr_code);

		/*!
		 * @brief sendUpFwMesg
		 * @param pubTxBuff
		 * @param uberrorCode
		 * @return
		 */
		int sendUpFwMesg(uint8_t * pubTxBuff, uint8_t ubwaitAck, uint16_t numLenToSend, uint8_t &uberrorCode);

};

#endif // FWUPGRADE_H
