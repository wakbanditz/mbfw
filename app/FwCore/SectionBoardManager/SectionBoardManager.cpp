/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardManager.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionBoardManager class.
 @details

 ****************************************************************************
*/

#include "Mutex.h"
#include "CanLink.h"
#include "MainExecutor.h"
#include "FwUpgrade.h"

#include "SectionBoardManager.h"

#define SEM_SECTION_A_ACTION_SYNCH_NAME			"/sem-sectionA-action-synch"
#define SEM_SECTION_B_ACTION_SYNCH_NAME			"/sem-sectionB-action-synch"
#define SEM_SECTION_ACTION_INIT_VALUE		0

#define STATUS_REQUEST_TIMEOUT				2000	// 2000 msec timeout for the status request
#define INIT_REQUEST_DELAY                  1	// sec delay while waiting for init from section
#define INIT_STATUS_TIMEOUT					((15 * 60) / INIT_REQUEST_DELAY)	// timeout during init status
#define STATUS_REQUEST_DELAY				2	// sec delay between 2 status requests
#define MECHANICAL_RESET_TIMEOUT			(60 * 1000)	// 1 mins timeout of mechanical reset (in msecs)
#define SECT_HOME_ERR_DISABLED				1

static Mutex	sMutex;
// ----------------------------------------------------------------------------------------- //

int manageSectionsEvents(int section, int device, int event)
{
	return sectionBoardManagerSingleton(section)->manageSectionEvents(device, event);
}

SectionBoardManager::SectionBoardManager()
{
	m_sectionId = SCT_NONE_ID;
	m_pSectionLink = 0;
	m_slSectionAction = SECTION_ACTION_NONE;
	m_vMovementList.clear();
    m_bCheckErrorsRequest = false;
}

SectionBoardManager::~SectionBoardManager()
{
	if (isRunning())
	{
		stopThread();
	}

	msleep(500);

	if (isStopped())
	{
		log(LOG_INFO, "SectionBoardManager thread closed");
	}
}

void SectionBoardManager::setSectionId(int sectionId)
{
	m_sectionId = sectionId;

	if(m_sectionId == SCT_A_ID)
	{
		m_synchSem.create(SEM_SECTION_A_ACTION_SYNCH_NAME, SEM_SECTION_ACTION_INIT_VALUE);
	}
	else
	{
		m_synchSem.create(SEM_SECTION_B_ACTION_SYNCH_NAME, SEM_SECTION_ACTION_INIT_VALUE);
	}
	m_synchSem.enableErrorPrint(true);

	// start the associated thread
	if (!isRunning() )
	{
		startThread();
	}
}

void SectionBoardManager::setSectionLink(SectionBoardLink* pSectionLink)
{
	m_pSectionLink = pSectionLink;

	m_pSectionLink->addCallbackEvent(manageSectionsEvents);
}

int SectionBoardManager::manageSectionEvents(int device, int event)
{
	// just to avoid warnings ....
	(void)device;
	(void)event;

	return 0;
}

int SectionBoardManager::checkIsPossibleSectionAction(void)
{
	if ( m_pOperation != 0 )
	{
		log(LOG_ERR, "SectionBoardManager::checkIsPossibleSectionAction: BAD! trying to start with an already running operation");
		return eOperationError;
	}

	return eOperationWaitForBirth;
}

bool SectionBoardManager::isActionInProgress()
{
	if(m_slSectionAction == SECTION_ACTION_NONE) return false;

	return true;
}

bool SectionBoardManager::isOperationInProgress()
{
    return ( m_pOperation != 0 );
}

bool SectionBoardManager::setSectionAction(int action)
{
	if(m_slSectionAction != SECTION_ACTION_NONE || m_pOperation != 0)
	{
		log(LOG_ERR, "SectionBoardManager::setSectionAction: trying to start when operation in progress");
        return false;
	}

	if (action == SECTION_ACTION_NONE)
	{
		// no action to do
		log(LOG_INFO, "SectionBoardManager:: no action");
		return false;
	}
	m_slSectionAction = action;

	int liRetVal = m_synchSem.release();
	if (liRetVal == SEM_FAILURE)
	{
		log(LOG_ERR, "SectionBoardManager:: unable to unlock thread synch semaphore");
		return false;
	}

    log(LOG_INFO, "SectionBoardManager:: section <%c> with operation <%d>", m_sectionId + SECTION_CHAR_A, m_slSectionAction);
	return true;
}

int SectionBoardManager::setSectionAction(int action, Operation* pOp)
{
    log(LOG_INFO, "SectionBoardManager::setSectionAction: <%c> ACTION = %d", m_sectionId + SECTION_CHAR_A, action);

	if(m_slSectionAction != SECTION_ACTION_NONE || m_pOperation != 0)
	{
		log(LOG_ERR, "SectionBoardManager::setSectionAction: trying to start when operation in progress");
		return eOperationError;
	}

	if ( pOp == 0 )
	{
		log(LOG_ERR, "SectionBoardManager::setSectionAction: trying to start with null operation");
		return eOperationError;
	}

	if (action == SECTION_ACTION_NONE)
	{
		// no action to do
		log(LOG_INFO, "SectionBoardManager:: no action");
		return eOperationWaitForDeath;
	}

	m_pOperation = pOp;
	m_slSectionAction = action;

	int liRetVal = m_synchSem.release();
	if (liRetVal == SEM_FAILURE)
	{
		log(LOG_ERR, "SectionBoardManager:: unable to unlock thread synch semaphore");
		return eOperationError;
	}
	else
	{
        log(LOG_INFO, "SectionBoardManager:: section <%c> with operation <%d>", m_sectionId + SECTION_CHAR_A, m_slSectionAction);
	}

	return eOperationRunning;
}

void SectionBoardManager::beforeWorkerThread(void)
{
    log(LOG_DEBUG, "SectionBoardManager::started");
}

int SectionBoardManager::workerThread(void)
{
	uint8_t status;
	int8_t result;
	uint16_t timeoutCnt;

	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "SectionBoardManager::th: unlocked but wait returned %d", liSynchRet);
			continue;
		}
		if (m_slSectionAction == SECTION_ACTION_NONE)
		{
			log(LOG_ERR, "SectionBoardManager::th: BAD unlocked without action");
			continue;
		}
        log(LOG_INFO, "SectionBoardManager::thread section <%c> operation <%d>", m_sectionId + SECTION_CHAR_A, m_slSectionAction);

		if (m_slSectionAction == SECTION_ACTION_REINIT)
		{
			// on Reset clear the flag of RunningStopped to the Section
			m_pSectionLink->m_pGeneral->setProtocolRunningFlag(0);

			m_slSectionAction = SECTION_ACTION_INIT;
            // continue action ....
		}

		if (m_slSectionAction == SECTION_ACTION_INIT)
		{
            log(LOG_INFO, "SectionBoardManager:: section <%c> going to reset", m_sectionId + SECTION_CHAR_A);

            // clean Section Errors
			m_pSectionLink->clearSectionsErrors();
			// reset the SectionReady counter
			m_pSectionLink->m_pGeneral->clearSectionReady();
            // cleanup all queues
            m_pSectionLink->emptyAllQueues();
            m_pSectionLink->m_pPIB->cleanQueues();
            m_pSectionLink->m_pSPR->cleanQueues();
            m_pSectionLink->m_pTray->cleanQueues();
            m_pSectionLink->m_pTower->cleanQueues();
            m_pSectionLink->m_pPump->cleanQueues();
            m_pSectionLink->m_pGeneral->cleanQueues();

			updateStatus(eStatusInit);

            uint8_t ucError = 0;

            // reset via hardware the Section Board
            if(resetSectionBoard(m_sectionId) == -1)
            {
                // error in request or timeout in init -> stop
                log(LOG_ERR, "SectionBoardManager:: reset failed <%c>", m_sectionId + SECTION_CHAR_A);

                // error resetting the section -> generates correponding error on section
                sectionBoardGeneralSingleton(m_sectionId)->setEventToDecode('M', '2');
                ucError = 1;
            }
            else
            {
                // cleanup all queues

                m_pSectionLink->emptyAllQueues();
                m_pSectionLink->m_pPIB->cleanQueues();
                m_pSectionLink->m_pSPR->cleanQueues();
                m_pSectionLink->m_pTray->cleanQueues();
                m_pSectionLink->m_pTower->cleanQueues();
                m_pSectionLink->m_pPump->cleanQueues();
                m_pSectionLink->m_pGeneral->cleanQueues();

                msleep(STATUS_REQUEST_DELAY * 1000);

                uint8_t retrySts = 0;
                while(ucError == 0)
                {
                    msleep(STATUS_REQUEST_DELAY * 1000);
                    // wait for the board to be reset
                    if(m_pSectionLink->m_pGeneral->getInternalStatus(status, (STATUS_REQUEST_TIMEOUT * 2), false) == -1)
                    {
                        // allow 2 retries on status request
                        if(++retrySts == 2)
                        {
                            // error -> stop
                            log(LOG_ERR, "SectionBoardManager:: status reset timeout <%c>", m_sectionId + SECTION_CHAR_A);
                            ucError = 2;
                            break;
                        }
                        else
                        {
                            status = eSectionOther;
                        }
                    }

                    if(status == eSectionWaitForCanUpgrade || status == eSectionWaitForSerialUpgrade)
                    {
                         // board has correctly executed the reset
                        log(LOG_INFO, "SectionBoardManager:: status reset <%c>", m_sectionId + SECTION_CHAR_A);
                        break;
                    }
                    else if(status == eSectionIdle)
                    {
                        // board has not performed the reset
                        // error -> stop
                        log(LOG_ERR, "SectionBoardManager:: status idle not reset <%c>", m_sectionId + SECTION_CHAR_A);
                        ucError = 1;
                    }
                }
            }

            // as long as the section has not completed the Init procedure
            timeoutCnt = 0;
            while(ucError == 0)
            {
                msleep(INIT_REQUEST_DELAY * 1000);

                if(m_pSectionLink->m_pGeneral->getSectionReady() == true)
                {
                    log(LOG_INFO, "SectionBoardManager:: section Ready <%c>", m_sectionId + SECTION_CHAR_A);
                    // and set the Section Led Off
                    m_pSectionLink->m_pGeneral->setLed(eLedGreen, eLedOff);
                    break;
                }
                else if(++timeoutCnt > INIT_STATUS_TIMEOUT)
                {
                    // error in request or timeout in init -> stop
                    log(LOG_ERR, "SectionBoardManager:: init timeout <%c>", m_sectionId + SECTION_CHAR_A);
                    ucError = 1;
                }
            }

            // first ask status to the board
            if(m_pSectionLink->m_pGeneral->getInternalStatus(status, STATUS_REQUEST_TIMEOUT) == -1)
            {
                // error -> stop
                log(LOG_ERR, "SectionBoardManager:: status timeout1 <%c>", m_sectionId + SECTION_CHAR_A);
                ucError = 2;
            }

            // in case of communication error do not proceed
            if(ucError < 2)
            {
                if((eSectionStatus)status == eSectionDisable)
				{
					// section is in DISABLE state -> stop init
                    log(LOG_ERR, "SectionBoardManager:: section disabled <%c>", m_sectionId + SECTION_CHAR_A);
                    updateStatus(eStatusDisable);
				}
				else if((eSectionStatus)status == eSectionSleep)
				{
					// section is in SLEEP state -> stop init
                    log(LOG_ERR, "SectionBoardManager:: section sleep <%c>", m_sectionId + SECTION_CHAR_A);
					updateStatus(eStatusSleep);
				}
				else if((eSectionStatus)status != eSectionIdle)
				{
                    // status is not Idle -> error
                    log(LOG_ERR, "SectionBoardManager:: section <%c> status ? %d", m_sectionId + SECTION_CHAR_A, getStatus());
                    ucError = 1;
                }
				else
				{
					// status in idle -> ask section to resend the initialization errors
                    // m_pSectionLink->m_pGeneral->sendInitializationEvents(STATUS_REQUEST_TIMEOUT);

                    msleep(2 * 1000);	// delay to let the Section send all error messages
                    // check errors sent from Section and set Section Status
                    log(LOG_INFO, "SectionBoardManager:: start sequence <%c>", m_sectionId + SECTION_CHAR_A);
                    if(runInitializationSequence() == false)
                    {
                        log(LOG_ERR, "SectionBoardManager:: error in initialization <%c>", m_sectionId + SECTION_CHAR_A);
                        ucError = 1;
                    }
                }
                // msleep(2 * 1000);	// delay to let the Section send all error messages
            }

            if(ucError > 0)
            {
                log(LOG_ERR, "SectionBoardManager:: set initialization error <%c> : %d", m_sectionId + SECTION_CHAR_A, ucError);
                updateStatus(eStatusError);
                // and set the Section Led -> Blinking Red
                m_pSectionLink->m_pGeneral->setLed(eLedRed, eLedBlinkSlow);
            }

			// end of initialization procedure
            log(LOG_INFO, "SectionBoardManager:: initialization completed <%c> status %X !", m_sectionId + SECTION_CHAR_A, getStatus());

            // send VidasEp and set the Module Led
			requestVidasEp();
            requestModuleLed();

            // if the Init was due to a INIT command -> notify operation completed
            if(m_pOperation)
            {
                m_pOperation->notifyCompleted(m_sectionId, (ucError == 0));
            }
        }
		else if (m_slSectionAction == SECTION_ACTION_END)
		{
			// on Reset clear the flag of RunningStopped to the Section
			m_pSectionLink->m_pGeneral->setProtocolRunningFlag(0);
			// section in Load position (perform the Mechanical Reset)
			result = m_pSectionLink->m_pGeneral->resetMechanic(MECHANICAL_RESET_TIMEOUT);
			// .. set Section status
			if(result == -1)
			{
				// exit from previous loop in error
				updateStatus(eStatusError);
			}
			else
			{
				// set section status (and so also module if possible) to idle
				updateStatus(eStatusIdle);
			}
			// end of initialization procedure -> notify to operation
			if(m_pOperation)
			{
				m_pOperation->executeCmd(m_sectionId);
			}
            log(LOG_INFO, "SectionBoardManager::end completed <%c> !", m_sectionId + SECTION_CHAR_A);
        }
		else if (m_slSectionAction == SECTION_ACTION_EXECUTE)
		{
			// request to perform an operation execute but not finalize the operation
			sMutex.lock();

            log(LOG_INFO, "SectionBoardManager::workerThread-->execute on section <%c>", m_sectionId + SECTION_CHAR_A);

			//executes ALL the actions related to the incoming command
			while(m_pOperation->executeCmd(m_sectionId) == 0) {};

			sMutex.unlock();
		}
		else if (m_slSectionAction == SECTION_ACTION_CMD)
		{
			sMutex.lock();

			//executes the actions related to the incoming command
			m_pOperation->executeCmd(m_sectionId);

			sMutex.unlock();

            log(LOG_INFO, "SectionBoardManager::workerThread-->wait END on section <%c>", m_sectionId + SECTION_CHAR_A);

			/* ********************************************************************************************
			 * Access the state machine
			 * ********************************************************************************************
			 */
			m_pOperation->restoreStateMachine();

			//compile and send the answer
			result = m_pOperation->compileSendOutCmd();

			/* ********************************************************************************************
			* Remove the operation from the OperationManager list
			* ********************************************************************************************
			*/
			if (result == eOperationEnabledForDeath)
			{
				/* ********************************************************************************************
				 * Reset variables and remove the common operation
				 * ********************************************************************************************
				 */
				operationSingleton()->removeOperation(m_pOperation);

                log(LOG_INFO, "SectionBoardManager::workerThread-->operation removed on section <%c>", m_sectionId + SECTION_CHAR_A);
			}
			else
			{
                log(LOG_ERR, "SectionBoardManager::workerThread-->NO operation removal on section <%c>", m_sectionId + SECTION_CHAR_A);
			}
		}		
		else if ( m_slSectionAction == SECTION_ACTION_MOVE )
		{
			sMutex.lock();
			// movement requested by Protocol Thread
			moveMotors();
			sMutex.unlock();
        }
		else if (m_slSectionAction  == SECTION_ACTION_FW_UPDATE)
		{
            log(LOG_INFO, "SectionBoardManager::workerThread-->wait FW UPD on section <%c>", m_sectionId + SECTION_CHAR_A);

			FwUpdateInfo* pInfo;
			pInfo = getUpdateInfo(m_pOperation);
			if ( pInfo != nullptr )
			{
				int liRes = -1;
				if ( pInfo->m_eFwType == eEeprom )
				{
					liRes = updateEeprom(m_pOperation);
				}
				else
				{
					FwUpgrade fwUpgrade;
					fwUpgrade.setLogger(MainExecutor::getInstance()->getLogger());
					fwUpgrade.setSectionLink(&(MainExecutor::getInstance()->m_sectionBoardLink[m_sectionId]));
					msleep(100);
					liRes = fwUpgrade.updateFirmwareCan(pInfo->m_eFwType, pInfo->m_strSourcePath,
												pInfo->m_lLength, pInfo->m_ullCrc32);
				}

                setUpdateOutcome(pInfo, (liRes == eFirmwareUpdateOK));
				updateInstrInfoRelease(pInfo->m_eFwType, pInfo->m_strNewFwVers);
			}
		}
        else if (m_slSectionAction == SECTION_ACTION_STATUS)
        {
            checkErrorsStatus();
        }

        m_slSectionAction = SECTION_ACTION_NONE;
        m_pOperation = 0;

        // before going back to "sleep" check the error situation (if requested)
        if(m_bCheckErrorsRequest == true)
        {
            m_bCheckErrorsRequest = false;
            checkErrorsStatus();
        }
	}
	return 0;
}

void SectionBoardManager::afterWorkerThread(void)
{
	m_synchSem.closeAndUnlink();
    log(LOG_DEBUG, "SectionBoardManager::stopped");
}

void SectionBoardManager::test(void)
{
    // request version ...
//    std::string strFirmware;
//    strFirmware = m_pSectionLink->m_pGeneral->getFirmwareVersion(eBootloader);
//    MainExecutor::getInstance()->m_instrumentInfo.setSectionBootRelease(m_sectionId, strFirmware);
//     log(LOG_INFO, "SectionBoardManager::BOOT 48, %s", strFirmware.c_str());
//     msleep(10);

//    strFirmware = m_pSectionLink->m_pGeneral->getFirmwareVersion(eApplication);
//    MainExecutor::getInstance()->m_instrumentInfo.setSectionApplicationRelease(m_sectionId, strFirmware);
//    log(LOG_INFO, "SectionBoardManager::application %s", strFirmware.c_str());
//     msleep(10);

//    strFirmware = m_pSectionLink->m_pGeneral->getFirmwareVersion(eFPGAbitstream);
//    MainExecutor::getInstance()->m_instrumentInfo.setSectionFPGARelease(m_sectionId, strFirmware);
//    log(LOG_INFO, "SectionBoardManager::FPGA 50, %s", strFirmware.c_str());
//    msleep(10);

//   sectionBoardGeneralSingleton(SCT_B_ID)->sendStripPressureThresholds(0,'A','8',0,0);
    sendTestValue(0,'A','8',111,222);

}
int SectionBoardManager::sendTestValue(uint8_t strip, uint8_t volume, uint8_t speed,
                uint64_t low, uint64_t high, uint16_t usTimeoutMs)
{
    uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
    uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

    memset(pPayLoad, 0, sizeof(pPayLoad));
    memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

    /* STX | d | 'G' | strip | volume | speed | Y0 | Y1 | Y2 | Y3 | Y4 | Y5 | Y6 | Y7 |
     *											 | Z0 | Z1 | Z2 | Z3 | Z4 | Z5 | Z6 | Z7 | ETX */
    pPayLoad[0] = SECTION_GENERAL_CHAR;
    pPayLoad[1] = 'G';
    pPayLoad[2] = strip + ZERO_CHAR;
    pPayLoad[3] = volume;
    m_pSectionLink->m_pGeneral->getSensors(usTimeoutMs);
    // ... spr sensor reading ...
    bool homeSpr;
 //   msleep(50);
    m_pSectionLink->m_pSPR->getSensor(homeSpr, usTimeoutMs);
    pPayLoad[4] = speed;

    // !!!! TODO verificare se sono valori sufficienti per le soglie
    sprintf((char*)&pPayLoad[5], "%08X", (uint32_t)low);
    sprintf((char*)&pPayLoad[13], "%08X", (uint32_t)high);

    int res = m_pSectionLink->sendMsg( pPayLoad, 21 );
    if (res < 0)
    {
        printf("ERRORE \n");
    }
    else
    {
//        printf("ERRORE \n");
    }
    usleep(500);

	return res;
}

bool SectionBoardManager::requestMoveSectionMotors(vector<pair<eSectionMotor, string>> vMotors)
{
	m_vMovementList = vMotors;

	if ( setSectionAction(SECTION_ACTION_MOVE) != eOperationRunning )
	{
		return false;
	}

	return true;
}

int SectionBoardManager::moveMotors()
{
	int liRes = 0;

	while ( m_vMovementList.size() )
	{
		SectionCommonMessage* pCommonMessage = nullptr;
		string strCoded("");
		// Added possibility to go back home
		bool bIsHome = ! m_vMovementList.back().second.compare("Home");

		switch ( m_vMovementList.back().first )
		{
			case eSectionMotor::eSpr:
				if ( bIsHome )
				{
					liRes += m_pSectionLink->m_pSPR->m_CommonMessage.moveToAbsolutePosition(10, 0);
					liRes += m_pSectionLink->m_pSPR->m_CommonMessage.searchHome(SECT_HOME_ERR_DISABLED);
				}
				else
				{
					strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pSPR->getCodedFromLabel(m_vMovementList.back().second);
					pCommonMessage = &sectionBoardLinkSingleton(m_sectionId)->m_pSPR->m_CommonMessage;
				}
			break;

			case eSectionMotor::eTray:
				if ( bIsHome )
				{
					 liRes += m_pSectionLink->m_pTray->m_CommonMessage.searchHome(SECT_HOME_ERR_DISABLED);
				}
				else
				{
					strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pTray->getCodedFromLabel(m_vMovementList.back().second);
					pCommonMessage = &sectionBoardLinkSingleton(m_sectionId)->m_pTray->m_CommonMessage;
				}
			break;

			case eSectionMotor::eTower:
				if ( bIsHome )
				{
					 liRes += m_pSectionLink->m_pTray->m_CommonMessage.searchHome(SECT_HOME_ERR_DISABLED);
				}
				else
				{
					strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pTower->getCodedFromLabel(m_vMovementList.back().second);
					pCommonMessage = &sectionBoardLinkSingleton(m_sectionId)->m_pTower->m_CommonMessage;
				}
			break;

			case eSectionMotor::ePump:
				if ( bIsHome )
				{
					liRes += m_pSectionLink->m_pTray->m_CommonMessage.searchHome(SECT_HOME_ERR_DISABLED);
				}
				else
				{
					strCoded = sectionBoardLinkSingleton(m_sectionId)->m_pPump->getCodedFromLabel(m_vMovementList.back().second);
					pCommonMessage = &sectionBoardLinkSingleton(m_sectionId)->m_pPump->m_CommonMessage;
				}
			break;
		}

		// Remove last element of the vector
		m_vMovementList.pop_back();

		if ( strCoded.empty() )		continue;

		char cPosCode = *strCoded.c_str();
        m_pLogger->log(LOG_DEBUG, "SectionBoardManager::moveMotors: <%c> CMD= %X", m_sectionId + SECTION_CHAR_A, cPosCode);
		liRes += pCommonMessage->moveToCodedPosition(cPosCode, 0);
	}

	return liRes;
}

bool SectionBoardManager::runInitializationSequence()
{
    // request version ...
    std::string strFirmware;
    strFirmware = m_pSectionLink->m_pGeneral->getFirmwareVersion(eBootloader);
    MainExecutor::getInstance()->m_instrumentInfo.setSectionBootRelease(m_sectionId, strFirmware);
    strFirmware = m_pSectionLink->m_pGeneral->getFirmwareVersion(eApplication);
    MainExecutor::getInstance()->m_instrumentInfo.setSectionApplicationRelease(m_sectionId, strFirmware);
    log(LOG_INFO, "SectionBoardManager::application <%c> %s", m_sectionId + SECTION_CHAR_A, strFirmware.c_str());
    strFirmware = m_pSectionLink->m_pGeneral->getFirmwareVersion(eFPGAbitstream);
    MainExecutor::getInstance()->m_instrumentInfo.setSectionFPGARelease(m_sectionId, strFirmware);
    // ... ntc parameters ....
    m_pSectionLink->m_pGeneral->getNTCParameters();

    // .... pressure parameters ...
    m_pSectionLink->m_pPIB->getPressureStorage();
    m_pSectionLink->m_pPIB->getPressureCalibration();
    m_pSectionLink->m_pPIB->getPressureConfiguration();
    // ... voltages ...
    m_pSectionLink->m_pGeneral->getVoltages();

    // ... get all calibration on all devices ...
    m_pSectionLink->m_pPump->m_CommonMessage.getAllCalibrations();
    m_pSectionLink->m_pTray->m_CommonMessage.getAllCalibrations();
    m_pSectionLink->m_pTower->m_CommonMessage.getAllCalibrations();
    m_pSectionLink->m_pSPR->m_CommonMessage.getAllCalibrations();

    // ... get motor parameters for all devices ...
    m_pSectionLink->m_pPump->m_CommonMessage.getAllMotorParameters();
    m_pSectionLink->m_pTray->m_CommonMessage.getAllMotorParameters();
    m_pSectionLink->m_pTower->m_CommonMessage.getAllMotorParameters();
    m_pSectionLink->m_pSPR->m_CommonMessage.getAllMotorParameters();

    // ... mechanical reset (if the section is not already in error, e.g. protocol stopped while running) ...
    if(getStatus() != eStatusError)
    {
        m_pSectionLink->m_pGeneral->resetMechanic(MECHANICAL_RESET_TIMEOUT);
    }

	// ... sensor reading ...
    m_pSectionLink->m_pGeneral->getSensors();
	// ... spr sensor reading ...
	bool homeSpr;
    m_pSectionLink->m_pSPR->getSensor(homeSpr);
    m_pSectionLink->m_pGeneral->setSensorSpr(homeSpr);

	// ... motor activation status (will be on but anyway) ...
	uint8_t activation;
    m_pSectionLink->m_pPump->m_CommonMessage.getMotorActivation(activation);
    m_pSectionLink->m_pTray->m_CommonMessage.getMotorActivation(activation);
    m_pSectionLink->m_pTower->m_CommonMessage.getMotorActivation(activation);
    m_pSectionLink->m_pSPR->m_CommonMessage.getMotorActivation(activation);

    // ... and the current position ....
	int32_t position;
    m_pSectionLink->m_pPump->m_CommonMessage.getAbsolutePosition(position);
    m_pSectionLink->m_pTray->m_CommonMessage.getAbsolutePosition(position);
    m_pSectionLink->m_pTower->m_CommonMessage.getAbsolutePosition(position);
    m_pSectionLink->m_pSPR->m_CommonMessage.getAbsolutePosition(position);

    // .... finally check if there are temperature settings active to apply ....
    if(infoSingleton()->getSprTargetTemperature(m_sectionId) != -1)
    {
        int target = infoSingleton()->getSprTargetTemperature(m_sectionId);
        if(target == 0)
        {
            // disable control
            m_pSectionLink->m_pSPR->setTemperature(0, 0, 0);
        }
        else
        {
            m_pSectionLink->m_pSPR->setTemperature(target,
                                                   infoSingleton()->getSprMinTemperature(m_sectionId),
                                                   infoSingleton()->getSprMaxTemperature(m_sectionId));
        }
    }
    if(infoSingleton()->getTrayTargetTemperature(m_sectionId) != -1)
    {
        int target = infoSingleton()->getTrayTargetTemperature(m_sectionId);
        if(target == 0)
        {
            // disable control
            m_pSectionLink->m_pTray->setTemperature(0, 0, 0);
        }
        else
        {
            m_pSectionLink->m_pTray->setTemperature(target,
                                                   infoSingleton()->getTrayMinTemperature(m_sectionId),
                                                   infoSingleton()->getTrayMaxTemperature(m_sectionId));
        }
    }


	// .. set Section status (if not in error due to asynchronous messages)
    if(getStatus() != eStatusError)
	{
		// set section status (and so also module if possible) to idle
		updateStatus(eStatusIdle);
		// and set the Section Led -> Green
		m_pSectionLink->m_pGeneral->setLed(eLedGreen, eLedOn);
	}
	else
	{
		// and set the Section Led -> Blinking Red
		m_pSectionLink->m_pGeneral->setLed(eLedRed, eLedBlinkSlow);
	}
    return (getStatus() == eStatusIdle);
}

void SectionBoardManager::updateStatus(eDeviceStatus status)
{
    log(LOG_DEBUG_PARANOIC, "SectionBoardManager:: set status <%c> : %d", m_sectionId + SECTION_CHAR_A, status);

    if(m_sectionId == SCT_A_ID)
		changeDeviceStatus(eIdSectionA, status);
	else
		changeDeviceStatus(eIdSectionB, status);
}

eDeviceStatus SectionBoardManager::getStatus()
{
	eDeviceStatus status;

	if(m_sectionId == SCT_A_ID)
		status = stateMachineSingleton()->getDeviceStatus(eIdSectionA);
	else
		status = stateMachineSingleton()->getDeviceStatus(eIdSectionB);

	return status;
}

void SectionBoardManager::clearSectionPressureErrors()
{
	m_pSectionLink->clearSectionPressureErrors();
}

int SectionBoardManager::updateErrorsTime(uint64_t currentMsec, tm* currentTime)
{
	int counter = 0;

	counter += m_pSectionLink->m_pGeneral->updateErrorsTime(currentMsec, currentTime);
	counter += m_pSectionLink->m_pPIB->updateErrorsTime(currentMsec, currentTime);
	counter += m_pSectionLink->m_pPump->updateErrorsTime(currentMsec, currentTime);
	counter += m_pSectionLink->m_pSPR->updateErrorsTime(currentMsec, currentTime);
	counter += m_pSectionLink->m_pTower->updateErrorsTime(currentMsec, currentTime);
	counter += m_pSectionLink->m_pTray->updateErrorsTime(currentMsec, currentTime);

	return counter;
}


int SectionBoardManager::getErrorList(std::vector<std::string> * pVectorList, uint8_t errorLevel)
{
	// NOTE don't clear the vector but add elements !

    m_pSectionLink->m_pGeneral->getErrorList(pVectorList, errorLevel);
    m_pSectionLink->m_pPIB->getErrorList(pVectorList, errorLevel);
    m_pSectionLink->m_pPump->getErrorList(pVectorList, errorLevel);
    m_pSectionLink->m_pSPR->getErrorList(pVectorList, errorLevel);
    m_pSectionLink->m_pTower->getErrorList(pVectorList, errorLevel);
    m_pSectionLink->m_pTray->getErrorList(pVectorList, errorLevel);

	return pVectorList->size();
}

int SectionBoardManager::getPressureErrorListPerStripWell(std::vector<string>* pVectorList, uint8_t strip, uint8_t well)
{
	// NOTE don't clear the vector but add elements !

	m_pSectionLink->m_pGeneral->getPressureErrorListPerStripWell(pVectorList, strip, well);
	m_pSectionLink->m_pPIB->getPressureErrorListPerStripWell(pVectorList, strip, well);
	m_pSectionLink->m_pPump->getPressureErrorListPerStripWell(pVectorList, strip, well);
	m_pSectionLink->m_pSPR->getPressureErrorListPerStripWell(pVectorList, strip, well);
	m_pSectionLink->m_pTower->getPressureErrorListPerStripWell(pVectorList, strip, well);
	m_pSectionLink->m_pTray->getPressureErrorListPerStripWell(pVectorList, strip, well);

    return pVectorList->size();
}

int SectionBoardManager::requestCheckErrorsStatus()
{
    // if possible set the action otherwise set the pending flag ...
    if(setSectionAction(SECTION_ACTION_STATUS) == false)
    {
        m_bCheckErrorsRequest = true;
    }
    return 0;
}

int SectionBoardManager::checkErrorsStatus()
{
    int errorNum = 0;
    std::vector<std::string> errorList;

    // retrieve the list (and the number) of errors still active on the devices
    errorNum += getErrorList(&errorList, GENERAL_ERROR_LEVEL_ERROR);
    errorNum += getErrorList(&errorList, GENERAL_ERROR_LEVEL_ALARM);

    m_pLogger->log(LOG_DEBUG, "SectionBoardManager::checkErrorsStatus: <%c> : Errors %d", m_sectionId + SECTION_CHAR_A, errorNum);

    if(errorNum == 0)
    {
        // no errors present at the moment -> reset the section status (only if in error)
        if(getStatus() == eStatusError)
        {
            m_pLogger->log(LOG_INFO, "SectionBoardManager::checkErrorsStatus: <%c> : resetStatus", m_sectionId + SECTION_CHAR_A);
            updateStatus(eStatusIdle);
            // and set the Section led to green
            m_pSectionLink->m_pGeneral->setLed(eLedGreen, eLedOn);
        }

        // update the LED and status of the Module and notify to Gateway
        requestModuleLed();
        requestVidasEp();
    }
    return errorNum;
}

FwUpdateInfo* SectionBoardManager::getUpdateInfo(Operation* pOp)
{
	if ( pOp == nullptr )	return nullptr;

	OpFwUpdate* pUpdOp = (OpFwUpdate*)pOp;
	return pUpdOp->getLastDeviceInfoForUpd();
}

void SectionBoardManager::updateInstrInfoRelease(enumUpdType eType, string& strRel)
{
	if ( eType == eBootLoader )
	{
		infoSingleton()->setSectionBootRelease(m_sectionId, strRel);
	}
	else if ( eType == eApplicative )
	{
		infoSingleton()->setSectionApplicationRelease(m_sectionId, strRel);
	}
	else if ( eType == eFPGA )
	{
		infoSingleton()->setSectionFPGARelease(m_sectionId, strRel);
	}
}

int SectionBoardManager::setUpdateOutcome(FwUpdateInfo* pInfo, bool bRes)
{
	if ( pInfo == nullptr )	return -1;

	OpFwUpdate* pUpdOp = (OpFwUpdate*)m_pOperation;
	return pUpdOp->writeUpdOutcomeLog(pInfo->m_eId, pInfo->m_eFwType, bRes);
}

int SectionBoardManager::updateEeprom(Operation* pOp)
{
	OpFwUpdate* pUpdOp = (OpFwUpdate*)pOp;
	FwUpdateEEprom* pEeprom = new FwUpdateEEprom();
	pEeprom = pUpdOp->getEepromInfoForUpd();

	int liRes = 0;

	for ( int i = 0; i < pEeprom->getVolumeTableSize(); ++i )
	{
		liRes = m_pSectionLink->m_pGeneral->writeEepromParameter(pEeprom->getVolumeIdx(i),
																 pEeprom->getVolumeValue(i));
		if ( liRes )
		{
			SAFE_DELETE(pEeprom);
			return -1;
		}
	}

	for ( int i = 0; i < pEeprom->getSpeedTableSize(); ++i )
	{
		liRes = m_pSectionLink->m_pGeneral->writeEepromParameter(pEeprom->getSpeedIdx(i),
																 pEeprom->getSpeedValue(i));
		if ( liRes )
		{
			SAFE_DELETE(pEeprom);
			return -1;
		}
	}

	SAFE_DELETE(pEeprom);
	return 0;
}
