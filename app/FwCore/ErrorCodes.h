#ifndef ERRORCODES_H
#define ERRORCODES_H

/*! *******************************************************************************************************************
 * Error management
 * ********************************************************************************************************************
 */

#define	ERR_NONE									0

// USB errors
#define ERR_CAMERA_NOT_DETECTED						1
#define ERR_USB_PERIPH_COMMUNICATION				2
#define ERR_USB_WRITE_DUE_TO_SIGNAL					3
#define ERR_USB_PERIPH_TIMEOUT						4
#define ERR_CR8062_READING_BARCODE					5
#define ERR_CR8062_READING_DATAMATRIX				6
#define ERR_CR8062_TAKING_PICTURE					7
#define ERR_CR8062_IN_PARAMETERS					8 // same as invalid command
#define ERR_ACCESSING_FILE							9
#define ERR_CR8062_NOT_READY_FOR_UPGRADE			10
#define ERR_CR8062_ERROR_IN_PARAMS_IN_UPGRADE		11
#define ERR_CR8062_CRC_ERR							12
#define ERR_CR8062_NOT_SUPPORTED_CMD				19
#define ERR_CR8062_FLASH_PROGR						32
#define ERR_UNDEFINED								9999

// NSH errors
#define ERR_NSH_MOTOR_HARD_STOPPED_FOR_ERR			(-1)
#define ERR_NSH_MOTOR_CMD_NOT_MANAGED				(-2)
#define ERR_NSH_MOTOR_COMMUNICATION					(-3)
#define ERR_NSH_MOTOR_CONFIG_NOK					(-4)
#define ERR_NSH_MOTOR_STEP_LOSS_W					(-5)
#define ERR_NSH_MOTOR_STEP_LOSS_A					(-6)
#define ERR_NSH_MOTOR_HOME_NOT_FOUND_UNKN			(-7)
#define ERR_NSH_MOTOR_HOME_NOT_FOUND_MOTION_DET		(-8)
#define ERR_NSH_MOTOR_HOME_NOT_FOUND_NO_MOTION		(-9)
#define ERR_NSH_MOTOR_WRONG_PARAM					(-10)
#define ERR_NSH_MOTOR_FAILURE						(-20)

// NSH Reader 10-30

#define ERR_NSH_ENCODER_COMMUNICATION				(-40)
#define ERR_NSH_ENCODER_BAD_PARAM					(-41)
#define ERR_NSH_ENCODER_CONFIG_NOK					(-42)

#define ERR_NSH_SS_NOT_FOUND                        159
#define WARN_NSH_EXCESSIVE_DRIFT              		160
#define ERR_NSH_EXCESSIVE_DRIFT           			161
#define ERR_NSH_OPT_CHECK                           162

#define ERR_NSH_SERIAL_NUMBER                       100

// ------------------------------------------------ //
// Error list linked to MASTERBOARD.err file
// ------------------------------------------------ //
#define ERR_COMMAND_PARAMETERS						990

#define ERR_LOGIN_NOT_EXECUTED						1000
#define ERR_LOGIN_NOT_VALID							1001

#define ERR_STATUS_NOT_CONGRUENT					1002
#define ERR_DEVICE_LOCKED							1003

#define ERR_CONNECTION_NOT_ALLOWED					1005
#define ERR_MAINTENANCE_MODE						1006
#define ERR_UNKNOWN_COMMAND							1007

#define ERR_MONITOR_NOT_ALLOWED						1010
#define ERR_OP_MANAGER_TOO_MANY_OPERATIONS			1011

#define ERR_PROTOCOL_DOOR_OPEN						1015
#define ERR_PROTOCOL_SCHEDULER						1016
#define ERR_PROTOCOL_TRANSFER						1017
#define ERR_PROTOCOL_TIMING							1018

#define ERR_PROTOCOL_SYNCHRONIZATION				1019
#define ERR_PROTOCOL_TIMEOUT						1020
#define ERR_PROTOCOL_SHUTDOWN						1022

#define ERR_SHUTDOWN_NOT_CORRECT					1030

#define ERR_TEMPERATURE_HIGH						1040
#define ERR_TEMPERATURE_LOW							1040
#define ERR_TEMPERATURE_NOT_AVAILABLE				1046

#define ERR_SUPPLY_FAN_FAIL                         1043
#define ERR_SECTIONA_FAN_FAIL                       1044
#define ERR_SECTIONB_FAN_FAIL                       1045

#define ERR_MODULE_LED                              1060

// --------------------------------------------------- //

#endif // ERRORCODES_H
