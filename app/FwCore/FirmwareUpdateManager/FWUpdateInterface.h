/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FWUpdateInterface.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the FWUpdateInterface class.
 @details

 ****************************************************************************
*/

#ifndef FWUPDATEINTERFACE_H
#define FWUPDATEINTERFACE_H

#include "FWUpdateCommonInclude.h"
#include "Loggable.h"


using namespace std;

class FWUpdateInterface : public Loggable
{
	public:

        /*! *************************************************************************************************
         * @brief constructor
         * **************************************************************************************************
         */
        FWUpdateInterface();

        /*! *************************************************************************************************
         * @brief destructor
         * **************************************************************************************************
         */
        virtual ~FWUpdateInterface();

		virtual int getVersion(enumUpdFirmware /*eFwType*/, string& sVersion);
		virtual int prepareToFwUpdate(bool /*start*/=true);
		virtual int updateFirmware(enumUpdFirmware /*eFwType*/ , string& /*sSourcePath*/,long /*lLength*/, unsigned long /*ullCrc32*/);
		virtual int exeCommand(enumFWUpgradeCmd /*eCommand*/)=0;

	protected:

		void increaseOpProgress(void);
		int getOperationProgress(void);
		void setTotalOp(unsigned long ulTotalOp);
		void addToTotalOp(unsigned long ulOpNum);

    public:

        bool m_bFirmwareForce;

    protected:

        int m_liFirmwareUpdateResult;
        int m_liTotalCountElements;
        int m_liProgressElement;

};

#endif // FWUPDATEINTERFACE_H
