/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FWUpdateManager.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the FWUpdateManager class.
 @details

 ****************************************************************************
*/

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <algorithm>

#include "FWUpdateManager.h"
#include "StrX.h"
#include "XStr.h"
#include "xercesc/dom/DOMNodeList.hpp"
#include "xercesc/dom/DOMNamedNodeMap.hpp"
#include "Parser/DOMTreeErrorReporter.h"
#include "MainExecutor.h"
#include "TARUtilities.h"
#include "CRCFile.h"

#define DESCRIPTOR_FILE_NAME	"Descriptor.xml"

#define XML_TAG_DESCR_ITEM		"descriptor_item"
#define XML_TAG_DESCR_OWNER		"descriptor_owner"
#define XML_TAG_NODE_LIST		"node_list"
#define XML_TAG_NODE_ITEM		"node_item"
#define XML_TAG_NODE_CRC		"node_crc32"
#define XML_TAG_NODE_NAME		"node_name"
#define XML_TAG_NODE_TYPE		"node_type"
#define XML_TAG_NODE_DEST		"node_destination"
#define XML_TAG_NODE_FQN		"node_fqn"
#define XML_TAG_NODE_VERS		"version_str"
#define XML_TAG_CMD_LIST		"command_list"
#define XML_TAG_CMD_ITEM		"command_item"
#define XML_TAG_TYPE			"type"
#define XML_TAG_VERS			"version_str"

#define XML_TAG_TYPE_FW			"firmware"
#define XML_TAG_TYPE_EEPROM		"eeprom"
#define XML_TAG_TYPE_TAR		"tar"

#define XML_TAG_TYPE_FW_BOOT	"boot"
#define XML_TAG_TYPE_FW_APPL	"applicative"
#define XML_TAG_TYPE_FW_FPGA	"fpga"
#define XML_TAG_TYPE_FW_KERNEL	"kernel"

#define XML_TAG_DEV_MASTER		"master"
#define XML_TAG_DEV_SECT		"section"
#define XML_TAG_DEV_NSH			"nsh"
#define XML_TAG_DEV_CAMERA		"camera"

#define XML_TAG_EEPROM_PARAMS	"eepromParameters"
#define XML_TAG_EEPROM_TABLE	"tableEEPROM"
#define XML_TAG_TABLE_TYPE		"TableType"
#define XML_TAG_TABLE_SPEED		"SPEED"
#define XML_TAG_TABLE_VOLUME	"VOLUME"
#define XML_TAG_RECORDS			"records"
#define XML_TAG_NODE_FORMAT		"nodeValFormat"
#define XML_TAG_RECORD			"record"
#define XML_ATTR_IDHEX			"IdHex"


#define TAR_KEY_FILE			".tar"
#define KEY_BACK_SLASH			"/"

#define MAX_TIMEOUT_FOR_FW_UPD_MIN	5 // 5 mins : timeout for any update

// time evaluation defines
#define UPDATE_TIME_NSH          "NSH"
#define UPDATE_TIME_CAMERA       "Camera"
#define UPDATE_TIME_MASTER       "Master"
#define UPDATE_TIME_SECTION      "Section"

#define UPDATE_TIME_EXT_APPLICATION "Application"
#define UPDATE_TIME_EXT_SYSTEM      "System"
#define UPDATE_TIME_EXT_FPGA        "FPGA"

FWUpdateManager::FWUpdateManager()
{
	m_pLogger = nullptr;
	m_pDescriptor = nullptr;
	m_pEeprom = nullptr;
	m_pOp = nullptr;
	m_bConfigOk = false;
	m_bForceUpd = false;
	m_strDirPath.clear();
	m_strInstrPkt.clear();
	m_lActionQueue.clear();
	m_vComponents.clear();
}

FWUpdateManager::~FWUpdateManager()
{
	SAFE_DELETE(m_pDescriptor);
	SAFE_DELETE(m_pEeprom);
}

int FWUpdateManager::init(Log* pLogger, OpFwUpdate* pOp, vector<string>& vComponentsToBeUpd, bool bForceUpd)
{
	m_bConfigOk = false;

	if ( pLogger == nullptr )	return -1;
	if ( pOp == nullptr )		return -1;

	SAFE_DELETE(m_pDescriptor);
	m_pDescriptor = new FWUpdateDescriptor();
	if ( m_pDescriptor == nullptr )	return -1;

	SAFE_DELETE(m_pEeprom);
	m_pEeprom = new FwUpdateEEprom();
	if ( m_pEeprom == nullptr )	return -1;

	m_pLogger = pLogger;
	m_pOp = pOp;
	m_vComponents = vComponentsToBeUpd;
	m_bForceUpd = bForceUpd;
	m_strInstrPkt.clear();
	m_bConfigOk = true;

    m_timeUpdate = m_timeCurrent = 0;
    m_percentageUpdate = 0;

	return 0;
}

int FWUpdateManager::createBackup(string& strTarPath, string& strTarDest)
{
	if ( strTarPath.empty() )	return -1;
	if ( strTarDest.empty() )	return -1;

	// Create path if it does not exists
	string strDirDest(strTarDest.substr(0, strTarDest.rfind(KEY_BACK_SLASH)));

	FolderManager manager;
    vector<string> vPathSplitted = splitString(strDirDest, KEY_BACK_SLASH);
    string vCurrPath("");

	for ( size_t i = 0; i < vPathSplitted.size(); i++ )
	{
		if ( vPathSplitted[i].empty() )		continue;
		if ( vPathSplitted[i] == "." )		continue;

		vCurrPath += (KEY_BACK_SLASH + vPathSplitted[i] );
		manager.loadFileName(vCurrPath);

		if ( manager.exists() )			continue;

		int liRes = manager.makeDir();
		if ( liRes )
		{
			m_pLogger->log(LOG_ERR, "FWUpdateManager::parseUPDDescriptor: unable to create directory %s",
						  vCurrPath.c_str());
			return -1;
		}
	}

	// Update filename for the backup with the current date
	string strFileName(strTarDest.substr(strTarDest.rfind(KEY_BACK_SLASH)+1));
	string strUpdName(strDirDest + KEY_BACK_SLASH + getFormattedTime(false) + "_" + strFileName);
	// copy tar
	manager.loadFileName(strTarPath);

	bool bRes = manager.copyFile(strUpdName);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "FWUpdateManager::createBackup: unable to create backup"
								"of %s -> %s", strTarPath.c_str(), strUpdName.c_str());
		return -1;
	}

	return 0;
}

int FWUpdateManager::updateDevices(string& strTarFile, bool bOnlyDecode)
{
	if ( ! m_bConfigOk )	return -1;

    m_strInstrPkt.assign((m_strInstrPkt.empty()) ? strTarFile : m_strInstrPkt);

	m_pLogger->log(LOG_INFO, "FWUpdateManager::updateDevices: unzipping file %s", strTarFile.c_str());
	int liRes = unzipTarFile(strTarFile);
	if ( liRes )
	{
        m_pLogger->log(LOG_ERR, "FWUpdateManager::updateDevices: ERROR unzipping %s", strTarFile.c_str());
        return -1;
	}

    liRes = createUPDDescriptor();
    if ( liRes )
    {
        m_pLogger->log(LOG_ERR, "FWUpdateManager::updateDevices: ERROR updDescriptor");
        return -1;
    }

	m_pLogger->log(LOG_INFO, "FWUpdateManager::updateDevices: parsing UDP descriptor");
    liRes = parseUPDDescriptor();
    if ( liRes )
    {
        m_pLogger->log(LOG_ERR, "FWUpdateManager::updateDevices: ERROR parsing updDescriptor");
        return -1;
    }

    // Check if the device is in the update list. If empty, all devices have to be updated
    if ( ! m_vComponents.empty() && m_strInstrPkt.compare(strTarFile) )
    {
        // Owner is always a .tar file, so to be able to compare it with components list we
        // need to cut off the extension.
        size_t pos = m_pDescriptor->getOwnerName().find(".tar");
        string strCurrComp = m_pDescriptor->getOwnerName().substr(0, pos);

        m_pLogger->log(LOG_DEBUG, "FWUpdateManager::componet %s %s ", m_pDescriptor->getOwnerName().c_str(), strCurrComp.c_str());

        if ( find(m_vComponents.begin(), m_vComponents.end(), strCurrComp) == std::end(m_vComponents) )
        {
            m_pLogger->log(LOG_DEBUG, "FWUpdateManager:: remove component %s %s ", m_pDescriptor->getOwnerName().c_str(), strCurrComp.c_str());
            // delete commands from list related to this components that is not included in list
            list<string>::iterator itBegin, itEnd;
            itBegin = itEnd = m_lActionQueue.begin();
            advance(itEnd, m_pDescriptor->getCommandsNum());
            m_lActionQueue.erase(itBegin, itEnd);
            return 0;
        }
    }

    // pTmpDescr is needed to get trace of the old descriptor because of the nested loop
    FWUpdateDescriptor* pTmpDescr = new FWUpdateDescriptor();
    *pTmpDescr = *m_pDescriptor;

    for ( size_t i = 0; i < pTmpDescr->getCommandsNum(); i++ )
    {
        string strCmd(m_lActionQueue.front());
        m_lActionQueue.pop_front();
		m_pLogger->log(LOG_INFO, "FWUpdateManager::updateDevices: executing action %s", strCmd.c_str());
        liRes = executeAction(strCmd, pTmpDescr, bOnlyDecode);
        if ( liRes )
        {
            m_pLogger->log(LOG_ERR, "FWUpdateManager::updateDevices: action Failed %s", strCmd.c_str());
            // If an execute fails, then remove all the actions related to that descriptor
            auto begin = m_lActionQueue.begin();
            auto end = begin;
            advance(end, pTmpDescr->getCommandsNum() - (i+1));
            m_lActionQueue.erase(begin, end);
            break;
        }
    }

    SAFE_DELETE(pTmpDescr);
	return 0;
}

int FWUpdateManager::clearRubbish(string& strTarDir)
{
	if ( ! m_bConfigOk )		return -1;
	if ( strTarDir.empty() )	return -1;

	// delete .tar file
	FolderManager manager;
	manager.loadFileName(strTarDir);
	manager.deleteFile();

	size_t pos = strTarDir.find(TAR_KEY_FILE, 0);
	if ( pos != string::npos )
	{
		strTarDir.assign(strTarDir.substr(0, pos));
	}
	m_strDirPath.assign(strTarDir);
	createUPDDescriptor();

	// Remove IS and Image directories
	vector<string> vRootPaths;
	for ( int i = 0; i < m_pDescriptor->getNodesNum(); i++ )
	{
		checkForRootPaths(vRootPaths, m_pDescriptor->getNodeDest(i));
	}

	for ( auto s : vRootPaths )
	{
		manager.deleteDirRecursively(s.c_str());
	}

	// delete unzipped directory and all subfloders
	manager.deleteDirRecursively(strTarDir.c_str());

	clearFwUpdateManager();
	return 0;
}

int FWUpdateManager::unzipTarFile(string& strTarFile)
{
	if ( ! m_bConfigOk )	return -1;

	TARutilities utilities(m_pLogger);
	bool bRes = utilities.openArchive(strTarFile);
	if ( ! bRes )	return -1;

	size_t found = strTarFile.find(TAR_KEY_FILE);
	if ( found == string::npos )
	{
		m_strDirPath.clear();
		return -1;
	}
	m_strDirPath = strTarFile.substr(0, found);

	return 0;
}

int FWUpdateManager::createUPDDescriptor()
{
	if ( ! m_bConfigOk )	return -1;

	// Open descriptor
    string strDescriptorPath(m_strDirPath + KEY_BACK_SLASH + DESCRIPTOR_FILE_NAME);
	ifstream file(strDescriptorPath);
	if ( ! file.is_open() )
	{
		m_pLogger->log(LOG_ERR, "FWUpdateManager::createUPDDescriptor: unable to open %s", m_strDirPath.c_str());
		return -1;
	}

	ostringstream xmlStream ;
	xmlStream << file.rdbuf();

	file.close();

	int liRes = parseXmlFile(xmlStream.str());

	return liRes;
}

int FWUpdateManager::parseUPDDescriptor()
{
	if ( ! m_bConfigOk )	return -1;

	for ( int i = 0; i < m_pDescriptor->getNodesNum(); i++ )
	{
        string strCurrFilePath = m_strDirPath + KEY_BACK_SLASH + m_pDescriptor->getNodeName(i);
		CRCFile crcFile(m_pLogger);
//		unsigned long uliCrc32File;

//		crcFile.calculateCRCFileMaxAlloc(strCurrFilePath, uliCrc32File);
//		if ( uliCrc32File != stoul(m_pDescriptor->getNodeCRC32(i), 0, 16) )
//		{
//			m_pLogger->log(LOG_ERR, "FWUpdateManager::parseUPDDescriptor: wrong CRC32 of %s, calculated[%lu],"
//									"read in descriptor[%s]", m_pDescriptor->getNodeName(i).c_str());
//			return -1;
//		}

		FolderManager manager;
		vector<string> vPathSplitted = splitString(m_pDescriptor->getNodeDest(i), KEY_BACK_SLASH);
        string vCurrPath("");

		for ( size_t i = 0; i < vPathSplitted.size(); i++ )
		{
			if ( vPathSplitted[i].empty() )		continue;

			vCurrPath += (KEY_BACK_SLASH + vPathSplitted[i] );
			manager.loadFileName(vCurrPath);

			if ( manager.exists() )			continue;

			int liRes = manager.makeDir();
			if ( liRes )
			{
				m_pLogger->log(LOG_ERR, "FWUpdateManager::parseUPDDescriptor: unable to create directory %s",
							  vCurrPath.c_str());
				return -1;
			}
		}

		// Copy the file where indicated in the descriptor
        string strCopyDest(m_pDescriptor->getNodeDest(i) + m_pDescriptor->getNodeName(i));
		int liRes = rename(strCurrFilePath.c_str(), strCopyDest.c_str());
		if ( liRes )
		{
			m_pLogger->log(LOG_ERR, "FWUpdateManager::parseUPDDescriptor: unable to copy %s in %s, errno: %s",
						  strCurrFilePath.c_str(), strCopyDest.c_str(), strerror(errno));
			return -1;
		}
	}

	return 0;
}

int FWUpdateManager::executeAction(string& strAction, FWUpdateDescriptor* pDescr, bool bOnlyDecode)
{
	if ( ! m_bConfigOk )	return -1;

	// Find the Node Idx we are considering
	int liIdx;
	for ( liIdx = 0; liIdx < pDescr->getNodesNum(); liIdx++ )
	{
		if ( ! strAction.compare(pDescr->getNodeName(liIdx)) )
		{
			break;
		}
	}

	if ( strAction[0] == '#' )		return 0;
	if ( ! pDescr->getNodeType(liIdx).compare(XML_TAG_TYPE_TAR) )
	{
		string strPath(pDescr->getNodeDest(liIdx) + pDescr->getNodeName(liIdx));
        return updateDevices(strPath, bOnlyDecode);
	}
	else if ( ! pDescr->getNodeType(liIdx).compare(XML_TAG_TYPE_EEPROM) )
	{
		return handleEEpromFile(pDescr, liIdx);
	}

	// Only the case of the firmware is left, this can be: kernel, bootloader, applicative or FPGA
	enumUpdType cFwType = eFwNone;
	if ( pDescr->getNodeFqn(liIdx).find(XML_TAG_TYPE_FW_BOOT) != string::npos )
	{
		cFwType = eBootLoader;
	}
	else if ( pDescr->getNodeFqn(liIdx).find(XML_TAG_TYPE_FW_FPGA) != string::npos )
	{
		cFwType = eFPGA;
	}
	else if ( pDescr->getNodeFqn(liIdx).find(XML_TAG_TYPE_FW_KERNEL) != string::npos )
	{
		cFwType = eKernel;
	}
	else
	{
		cFwType = eApplicative;
	}

	FwUpdateInfo* pInfo = new FwUpdateInfo();
	pInfo->m_eFwType = cFwType;
	pInfo->m_ullCrc32 = stoul(pDescr->getNodeCRC32(liIdx), 0, 16);
	pInfo->m_strNewFwVers.assign(pDescr->getNodeVersion(liIdx));
	pInfo->m_strSourcePath = pDescr->getNodeDest(liIdx) + pDescr->getNodeName(liIdx);
	pInfo->m_lLength = getFileSize(pInfo->m_strSourcePath);
	if ( pInfo->m_lLength == -1 )	return -1;

	// I don't actually care to be really accurate (4min or 4.10 min is still ok)
    bool bErrorFound = false;

	// Now that I have found the fw type, I proceed with the fw update
	if  ( pDescr->getNodeFqn(liIdx).find(XML_TAG_DEV_MASTER) != string::npos )
	{
		pInfo->m_eId = eIdModule;
		if ( isVersionOk(pInfo) )
		{
            if(bOnlyDecode)
            {
                m_pLogger->log(LOG_INFO, "FWUpdateManager::executeAction: add time for Module");
                m_timeUpdate += updateTotalTime(INSTRUMENT_ID, pInfo->m_eFwType);
            }
            else
            {
                m_pOp->setParamsFromManager(*pInfo);
                // Update FwUpdateLogFile
                m_pOp->addItemToLog(pInfo->m_eId, pInfo->m_eFwType);
                masterBoardManagerSingleton()->setMasterAction(eMasterActionFwUpdate, (Operation*)m_pOp);
                if(waitForUpdate(INSTRUMENT_ID) == false)
                {
                    bErrorFound = true;
                }
                MainExecutor::getInstance()->updateApplicative();
            }
		}
	}
    else if ( pDescr->getNodeFqn(liIdx).find(XML_TAG_DEV_SECT) != string::npos )
	{
		// Update both sections
		for ( int i = SCT_A_ID; i < SCT_NONE_ID; i++ )
		{
			pInfo->m_eId = ( i == SCT_A_ID ) ? eIdSectionA : eIdSectionB;
			if ( isVersionOk(pInfo) )
			{
                if(bOnlyDecode)
                {
                    m_pLogger->log(LOG_INFO, "FWUpdateManager::executeAction: add time for Section %c (%d)", A_CHAR + i, pInfo->m_eFwType);
                    m_timeUpdate += updateTotalTime(i, pInfo->m_eFwType);
                }
                else
                {
                    m_pOp->setParamsFromManager(*pInfo);
                    // Update FwUpdateLogFile
                    m_pOp->addItemToLog(pInfo->m_eId, pInfo->m_eFwType);
                    sectionBoardManagerSingleton(i)->setSectionAction(SECTION_ACTION_FW_UPDATE, (Operation*)m_pOp);
                    // wait for the action being completed
                    if(waitForUpdate(i) == false)
                    {
                        bErrorFound = true;
                    }
                }
			}
		}
	}
	else if ( pDescr->getNodeFqn(liIdx).find(XML_TAG_DEV_NSH) != string::npos )
	{
		pInfo->m_eId = eIdNsh;
		if ( isVersionOk(pInfo) )
		{
            if(bOnlyDecode)
            {
                m_pLogger->log(LOG_INFO, "FWUpdateManager::executeAction: add time for NSH");
                m_timeUpdate += updateTotalTime(NSH_ID, pInfo->m_eFwType);
            }
            else
            {
                m_pOp->setParamsFromManager(*pInfo);
                // Update FwUpdateLogFile
                m_pOp->addItemToLog(pInfo->m_eId, pInfo->m_eFwType);
                nshBoardManagerSingleton()->setNSHAction(eNSHActionFwUpdate, (Operation*)m_pOp);
                // wait for the action being completed
                if(waitForUpdate(NSH_ID) == false)
                {
                    bErrorFound = true;
                }
            }
		}
	}
	else if ( pDescr->getNodeFqn(liIdx).find(XML_TAG_DEV_CAMERA) != string::npos )
	{
		pInfo->m_eId = eIdCamera;
		if ( isVersionOk(pInfo) )
		{
            if(bOnlyDecode)
            {
                m_pLogger->log(LOG_INFO, "FWUpdateManager::executeAction: add time for CAMERA");
                m_timeUpdate += updateTotalTime(CAMERA_ID, pInfo->m_eFwType);
            }
            else
            {
                m_pOp->setParamsFromManager(*pInfo);
                // Update FwUpdateLogFile
                m_pOp->addItemToLog(pInfo->m_eId, pInfo->m_eFwType);
                cameraBoardManagerSingleton()->setCameraAction(eCameraActionFwUpdate, (Operation*)m_pOp);
                // wait for the action being completed
                if(waitForUpdate(CAMERA_ID) == false)
                {
                    bErrorFound = true;
                }
            }
		}
	}

	SAFE_DELETE(pInfo);

    return (bErrorFound == false) ? 0 : -1;
}

bool FWUpdateManager::waitForUpdate(uint8_t device)
{
    unsigned int uliMsecElapsed = 0;
    bool bDeviceBusy = true;

    while ( true )
    {
        switch(device)
        {
            case SCT_A_ID:
            case SCT_B_ID:
                bDeviceBusy = sectionBoardManagerSingleton(device)->isActionInProgress();
            break;

            case CAMERA_ID :
                bDeviceBusy = cameraBoardManagerSingleton()->isCameraBusy();
            break;

            case NSH_ID :
                bDeviceBusy = !nshBoardManagerSingleton()->isActionInProgress();
            break;

            case INSTRUMENT_ID :
                bDeviceBusy = masterBoardManagerSingleton()->isMasterBusy();
            break;
        }

        if(bDeviceBusy == false)
        {
            break;
        }

        msleep(200);
        uliMsecElapsed += 200;
        m_timeCurrent += 200;

        if ( uliMsecElapsed >= (MAX_TIMEOUT_FOR_FW_UPD_MIN * SEC_PER_MIN * MSEC_PER_SEC) )
        {
            m_pLogger->log(LOG_ERR, "FWUpdateManager::waitForUpdate timeout for Device %d", device);
            // TODO remove operation, update UpdateLog.log and return -1; ?
            break;
        }

        // calculate the current percentage of the whole procedure
        uint8_t percentage = 100;
        if(m_timeUpdate > m_timeCurrent)
        {
            percentage = 100 - ((m_timeUpdate - m_timeCurrent) * 100 / m_timeUpdate);
        }

        if(percentage > m_percentageUpdate)
        {
            // send progress message
            m_pLogger->log(LOG_INFO, "FWUpdateManager::waitForUpdate percentage %d (%d vs %d)", percentage, m_timeUpdate, m_timeCurrent);
            if(m_percentageUpdate == 0)
                m_pOp->compileSendProgressCmd(1);
            else
                m_pOp->compileSendProgressCmd(m_percentageUpdate);
            m_percentageUpdate += 10;
        }
    }

    return (bDeviceBusy == false);
}

int FWUpdateManager::updateTotalTime(uint8_t device, uint8_t type)
{
    int timeExecution = 0;
    std::string strCode;

    switch(device)
    {
        case SCT_A_ID:
        case SCT_B_ID:
            strCode.assign(UPDATE_TIME_SECTION);
        break;

        case CAMERA_ID :
            strCode.assign(UPDATE_TIME_CAMERA);
        break;

        case NSH_ID :
            strCode.assign(UPDATE_TIME_NSH);
        break;

        case INSTRUMENT_ID :
            strCode.assign(UPDATE_TIME_MASTER);
        break;
    }

    if(strCode.empty() == true) return timeExecution;

    switch(type)
    {
        case eKernel:
            strCode.append(UPDATE_TIME_EXT_SYSTEM);
        break;

        case eApplicative:
            strCode.append(UPDATE_TIME_EXT_APPLICATION);
        break;

        case eFPGA:
            strCode.append(UPDATE_TIME_EXT_FPGA);
        break;

    }

    std::string strRead, strId, strTime;
    std::ifstream inFile;

    inFile.open(UPDATE_TIME_PATH);
    if(inFile.fail())
    {
        // File does not exist code here
        m_pLogger->log(LOG_ERR, "FWUpdateManager::updateTotalTime definition file %s not found", UPDATE_TIME_PATH);
        return timeExecution;
    }

    while(!inFile.eof())
    {
        getline(inFile, strRead);
        if(!strRead.empty())
        {
            // lines beginning with # are comments
            if(strRead.front() != '#')
            {
                size_t posStr = 0;
                if((posStr = strRead.find("=")) != std::string::npos)
                {
                    strTime = strRead.substr(posStr + 1);
                    strTime = trimString(strTime);

                    strId = strRead.substr(0, posStr);
                    strId = trimString(strId);

                    if(strId.compare(strCode) == 0)
                    {
                        timeExecution = atoi(strTime.c_str());
                        m_pLogger->log(LOG_INFO, "FWUpdateManager::updateTotalTime time for %s = %d", strCode.c_str(), timeExecution);
                        break;
                    }
                }
            }
        }
    }
    inFile.close();

    // converted to msecs
    return (timeExecution * 1000);
}

int FWUpdateManager::parseXmlFile(string strXmlBuffer)
{
	if ( strXmlBuffer.empty() )	return -1;

	// Init xerces buffer
	XercesDOMParser* pXercesDOMParser = new XercesDOMParser();
//    XercesDOMParser pXercesDOMParser;

	pXercesDOMParser->setValidationScheme(XercesDOMParser::Val_Never);
	pXercesDOMParser->setDoNamespaces(false);
	pXercesDOMParser->setDoSchema(false);
	pXercesDOMParser->setHandleMultipleImports(false);
	pXercesDOMParser->setValidationSchemaFullChecking(false);
	pXercesDOMParser->setCreateEntityReferenceNodes(false);
	pXercesDOMParser->setLoadExternalDTD(false);
	pXercesDOMParser->setDoXInclude(false);

	// Add the error handler, now is just a logger
    DOMTreeErrorReporter pDomErrorReporter;
    pDomErrorReporter.setLogger(m_pLogger);
	pXercesDOMParser->setErrorHandler(&pDomErrorReporter);

	pXercesDOMParser->resetDocumentPool();
	pXercesDOMParser->resetCachedGrammarPool();
	pXercesDOMParser->reset();

	// Get and parse the buffer
	MemBufInputSource xmlBuffer((const XMLByte*)strXmlBuffer.c_str(), strXmlBuffer.length(), "sCmdBody", false);
	pXercesDOMParser->parse(xmlBuffer);

	// Get the entire DOM representation of the buffer
	// There is no need to free the following  pointer as it is owned by the parent parser object
	DOMDocument* pXmlDoc = pXercesDOMParser->getDocument();
	if ( ! pXmlDoc )
	{
		m_pLogger->log(LOG_ERR, "FWUpdateManager::parseXmlFile: corrupted XML buffer");
		return-1;
	}
	// Get the top-level element: name is "vminst:COMMAND_NAME""
	DOMElement* pElementRoot = pXmlDoc->getDocumentElement();
	if ( ! pElementRoot )
	{
		m_pLogger->log(LOG_ERR, "FWUpdateManager::parseXmlFile: XML root tag not found");
		return -1;
	}

	int liRes = -1;
	StrX strTagCommandName(pElementRoot->getTagName());
	if ( strcmp(strTagCommandName.getString(), XML_TAG_DESCR_ITEM) == 0 )
	{
		liRes = buildDescriptorFromRootNode(pElementRoot);
	}
	else if ( strcmp(strTagCommandName.getString(), XML_TAG_EEPROM_PARAMS) == 0 )
	{
		liRes = buildEepromParamFromRootNode(pElementRoot);
	}

//	SAFE_DELETE(pDomErrorReporter);
//	SAFE_DELETE(pXercesDOMParser);

	return liRes;
}

int FWUpdateManager::buildDescriptorFromRootNode(DOMElement* pXmlRootNode)
{
	if ( pXmlRootNode == nullptr )  return -1;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	if ( l == nullptr )				return -1;

	// Clear descriptor
	m_pDescriptor->clearAll();

	int liNumRootChildren = l->getLength();
	int liNodeIdx = 0;

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if ( ! strNodeName.compare(XML_TAG_VERS) )
			{
				m_pDescriptor->setOwnerVersion(strNodeVal);
			}
			else if ( ! strNodeName.compare(XML_TAG_DESCR_OWNER) )
			{
				m_pDescriptor->setOwnerName(strNodeVal);
			}
			else if ( ! strNodeName.compare(XML_TAG_NODE_LIST) )
			{
				DOMNodeList* pIntraNodeList = (l->item(i)->getChildNodes());
				int liNumChilds = pIntraNodeList->getLength();
				for ( int i = 0; i != liNumChilds; ++i )
				{
					StrX sIntraNodeName(pIntraNodeList->item(i)->getNodeName());
					string strIntraNodeName(sIntraNodeName.getString());
					if ( strIntraNodeName.length() > 0 )
					{
						if ( ! strIntraNodeName.compare(XML_TAG_NODE_ITEM) )
						{
							DOMNodeList* pInfoNodeList = (pIntraNodeList->item(i)->getChildNodes());
							int liNumInfo = pInfoNodeList->getLength();
							for ( int i = 0; i != liNumInfo; ++i )
							{
								StrX sInfoNodeName(pInfoNodeList->item(i)->getNodeName());
								string strInfoNodeName(sInfoNodeName.getString());
								if ( strInfoNodeName.length() > 0 )
								{
									StrX sVal(pInfoNodeList->item(i)->getTextContent());
									string strNodeVal(sVal.getString());
									if ( ! strInfoNodeName.compare(XML_TAG_NODE_CRC) )
									{
										m_pDescriptor->setNodeCRC32(strNodeVal, liNodeIdx);
									}
									else if ( ! strInfoNodeName.compare(XML_TAG_NODE_NAME) )
									{
										m_pDescriptor->setNodeName(strNodeVal, liNodeIdx);
									}
									else if ( ! strInfoNodeName.compare(XML_TAG_NODE_TYPE) )
									{
										m_pDescriptor->setNodeType(strNodeVal, liNodeIdx);
									}
									else if ( ! strInfoNodeName.compare(XML_TAG_NODE_DEST) )
									{
										m_pDescriptor->setNodeDest(strNodeVal, liNodeIdx);
									}
									else if ( ! strInfoNodeName.compare(XML_TAG_NODE_FQN) )
									{
										m_pDescriptor->setNodeFqn(strNodeVal, liNodeIdx);
									}
									else if ( ! strInfoNodeName.compare(XML_TAG_NODE_VERS) )
									{
										m_pDescriptor->setNodeVersion(strNodeVal, liNodeIdx);
									}
								}
							}
							liNodeIdx++;
						}
					}
				}
			}
			else if ( ! strNodeName.compare(XML_TAG_CMD_LIST) )
			{
				DOMNodeList* pIntraNodeList = (l->item(i)->getChildNodes());
				int liNumInfo = pIntraNodeList->getLength();
				for ( int i = 0; i != liNumInfo; ++i )
				{
					StrX sInfoNodeName(pIntraNodeList->item(i)->getNodeName());
					string strInfoNodeName(sInfoNodeName.getString());
					if ( strInfoNodeName.length() > 0 )
					{
						StrX sVal(pIntraNodeList->item(i)->getTextContent());
						string strNodeVal(sVal.getString());
						if ( ! strInfoNodeName.compare(XML_TAG_CMD_ITEM) )
						{
							m_pDescriptor->addCommandItem(strNodeVal);
						}
					}
				}
			}
		}
	}

	// copy commands at the beginning
	for ( int i = m_pDescriptor->getCommandItems().size()-1; i >= 0; i--)
	{
		m_lActionQueue.push_front(m_pDescriptor->getCommandItems()[i]);
	}

	return 0;
}

int FWUpdateManager::buildEepromParamFromRootNode(DOMElement* pXmlRootNode)
{
	if ( pXmlRootNode == nullptr )  return -1;

	DOMNodeList* l = pXmlRootNode->getChildNodes();
	if ( l == nullptr )				return -1;

	int liNumRootChildren = l->getLength();
	int liSpeedTableIdx = 0, liVolumeTableIdx = 0;

	for ( int i = 0; i < liNumRootChildren; i++ )
	{
		StrX sNodeName(l->item(i)->getNodeName());
		string strNodeName(sNodeName.getString());
		if (strNodeName.length() > 0)
		{
			enum {eSpeed, eVolume, eNone};
			int liTableType = eNone;

			StrX sVal(l->item(i)->getTextContent());
			string strNodeVal(sVal.getString());
			if ( ! strNodeName.compare(XML_TAG_TYPE) )
			{
				m_pEeprom->setBoardType(strNodeVal);
			}
			else if ( ! strNodeName.compare(XML_TAG_VERS) )
			{
				m_pEeprom->setVersion(strNodeVal);
			}
			else if ( ! strNodeName.compare(XML_TAG_EEPROM_TABLE) )
			{
				DOMNodeList* pIntraNodeList = (l->item(i)->getChildNodes());
				int liNumChilds = pIntraNodeList->getLength();
				for ( int i = 0; i != liNumChilds; ++i )
				{
					StrX sIntraNodeName(pIntraNodeList->item(i)->getNodeName());
					string strIntraNodeName(sIntraNodeName.getString());
					if ( strIntraNodeName.length() > 0 )
					{
						if (  ! strIntraNodeName.compare(XML_TAG_TABLE_TYPE) )
						{
							StrX sTableType(pIntraNodeList->item(i)->getTextContent());
							string strNodeType(sTableType.getString());
							liTableType = ! strcmp(strNodeType.c_str(), XML_TAG_TABLE_SPEED) ? eSpeed : eVolume;
						}
						else if ( ! strIntraNodeName.compare(XML_TAG_RECORDS) )
						{
							DOMNodeList* pInfoNodeList = (pIntraNodeList->item(i)->getChildNodes());
							int liNumInfo = pInfoNodeList->getLength();
							for ( int i = 0; i != liNumInfo; ++i )
							{
								StrX sInfoNodeName(pInfoNodeList->item(i)->getNodeName());
								string strInfoNodeName(sInfoNodeName.getString());
								if ( strInfoNodeName.length() > 0 )
								{
									StrX sVal(pInfoNodeList->item(i)->getTextContent());
									string strNodeVal(sVal.getString());
									if ( ! strInfoNodeName.compare(XML_TAG_NODE_FORMAT) )
									{
										if ( liTableType == eSpeed )
										{
											m_pEeprom->setSpeedFormat(strNodeVal);
										}
										else if ( liTableType == eVolume )
										{
											m_pEeprom->setVolumeFormat(strNodeVal);
										}
									}
									else if ( ! strInfoNodeName.compare(XML_TAG_RECORD) )
									{
										DOMNamedNodeMap* pEepromNodeMap = (pInfoNodeList->item(i)->getAttributes());
										if ( pEepromNodeMap )
										{
											DOMNode* pEepromDomNode;
											XStr sAttr1(XML_ATTR_IDHEX);
											pEepromDomNode = pEepromNodeMap->getNamedItem(sAttr1.unicodeForm());
											if ( pEepromDomNode )
											{
												StrX sValAttr(pEepromDomNode->getNodeValue());
												string strValAttr(sValAttr.getString());

												if ( liTableType == eSpeed )
												{
													m_pEeprom->setSpeedIdx(strValAttr, liSpeedTableIdx);
													m_pEeprom->setSpeedVal(strNodeVal, liSpeedTableIdx);
													liSpeedTableIdx++;
												}
												else if ( liTableType == eVolume )
												{
													m_pEeprom->setVolumeIdx(strValAttr, liVolumeTableIdx);
													m_pEeprom->setVolumeVal(strNodeVal, liVolumeTableIdx);
													liVolumeTableIdx++;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return 0;
}

void FWUpdateManager::checkForRootPaths(vector<string>& vRootPaths, const string& strTmpPath)
{
	if ( strTmpPath.empty() )	return;

	vector<string> vPathSplitted = splitString(strTmpPath, "/");
	string strRoot;
	for ( auto s : vPathSplitted )
	{
		if ( s.empty() || ! s.compare(".") )	continue;
		if ( ! s.compare("home") )				continue;
		if ( ! s.compare("root") )				continue;
		strRoot.assign(s);						break;		// first element is the highest level dir
	}

	if ( vRootPaths.empty() )
	{
		vRootPaths.push_back(strRoot);
	}
	else
	{
		for ( auto s : vRootPaths )
		{
			if ( strRoot.compare(s)	)
			{
				// add item only if missing
				vRootPaths.push_back(strRoot);
				return;
			}
		}
	}

	return;
}

void FWUpdateManager::clearFwUpdateManager()
{
	m_strDirPath.clear();
	m_strInstrPkt.clear();
	m_lActionQueue.clear();
}

int FWUpdateManager::getFileSize(string& strFileName)
{
	struct stat statbuf;

	if ( stat(strFileName.c_str(), &statbuf) == -1)
	{
		switch ( errno )
		{
			case EACCES:
				m_pLogger->log(LOG_ERR, "CR8062Link::getFileSize: permission denied in accessing %s.", strFileName.c_str());
			break;

			case EFAULT:
				m_pLogger->log(LOG_ERR, "CR8062Link::getFileSize: bad address for %s.", strFileName.c_str());
			break;

			case EBADF:
				m_pLogger->log(LOG_ERR, "CR8062Link::getFileSize: not a valid open file descriptor for %s.", strFileName.c_str());
			break;

			case ENOENT:
				m_pLogger->log(LOG_ERR, "CR8062Link::getFileSize: file %s does not exists.", strFileName.c_str());
			break;

			default:
				m_pLogger->log(LOG_ERR, "CR8062Link::getFileSize: not handled error accessing %s.", strFileName.c_str());
			break;
		}
		return -1;
	}

	return statbuf.st_size;
}

bool FWUpdateManager::isVersionOk(FwUpdateInfo* pInfo)
{
	if ( pInfo == nullptr )		return false;
    if ( m_bForceUpd )			return true;

	string strCurrentVers("");

	switch ( pInfo->m_eId )
	{
		case eIdModule:
			if ( pInfo->m_eFwType == eKernel )
			{
				strCurrentVers = infoSingleton()->getMasterKernelRelease();
			}
			else if ( pInfo->m_eFwType == eMiniKernel )
			{
				strCurrentVers = infoSingleton()->getMasterMiniKernelRelease();
			}
			else if ( pInfo->m_eFwType == eBootLoader )
			{
				strCurrentVers = infoSingleton()->getMasterBootRelease();
			}
			else if ( pInfo->m_eFwType == eApplicative )
			{
				strCurrentVers = infoSingleton()->getMasterApplicationRelease();
			}
		break;


		case eIdSectionA:
			if ( pInfo->m_eFwType == eBootLoader )
			{
				strCurrentVers = infoSingleton()->getSectionABootRelease();
			}
			else if ( pInfo->m_eFwType == eApplicative )
			{
				strCurrentVers = infoSingleton()->getSectionAApplicationRelease();
			}
			else if ( pInfo->m_eFwType == eFPGA )
			{
				strCurrentVers = infoSingleton()->getSectionAFpgaRelease();
			}
		break;

		case eIdSectionB:
			if ( pInfo->m_eFwType == eBootLoader )
			{
				strCurrentVers = infoSingleton()->getSectionBBootRelease();
			}
			else if ( pInfo->m_eFwType == eApplicative )
			{
				strCurrentVers = infoSingleton()->getSectionBApplicationRelease();
			}
			else if ( pInfo->m_eFwType == eFPGA )
			{
				strCurrentVers = infoSingleton()->getSectionBFpgaRelease();
			}
		break;

		case eIdNsh:
			if ( pInfo->m_eFwType == eBootLoader )
			{
				strCurrentVers = infoSingleton()->getNshBootRelease();
			}
			else if ( pInfo->m_eFwType == eApplicative )
			{
				strCurrentVers = infoSingleton()->getNshApplicationRelease();
			}
		break;

		case eIdCamera:
			if ( pInfo->m_eFwType == eApplicative )
			{
				strCurrentVers = infoSingleton()->getCameraApplicationRelease();
			}
		break;

		default:
		break;
	}

	if ( strCurrentVers.empty() )
	{
		m_pLogger->log(LOG_ERR, "FWUpdateManager::isVersionOk: unable to match device with fwType");
		return false;
	}

	// remove dots and convert into num to be able to check if the version is the same, the previous or the next
	string strNewVers = pInfo->m_strNewFwVers;
	int liRes = compareVersions(strNewVers, strCurrentVers);
	if ( liRes <= 0 )
	{
		m_pLogger->log(LOG_DEBUG, "FWUpdateManager::isVersionOk: dealing with an old version, Id: %d, FwType: %d,"
                                  "Curr Vers: %s, New Vers: %s", pInfo->m_eId, pInfo->m_eFwType, strCurrentVers.c_str(),
                                  strNewVers.c_str());
		return false;
	}

	return true;
}

int FWUpdateManager::compareVersions(string& a, string& b)
{
	//  vnum stores each numeric part of version
	int iNumA = 0, iNumB = 0;

	//  loop untill both string are processed
	for ( size_t i=0,j=0; (i<a.length() || j<b.length()); )
	{
		//  storing numeric part of version 1 in iNumA
		while (i < a.length() && a[i] != '.')
		{
			iNumA = iNumA * 10 + (a[i] - '0');
			i++;
		}

		//  storing numeric part of version 2 in iNumB
		while (j < b.length() && b[j] != '.')
		{
			iNumB = iNumB * 10 + (b[j] - '0');
			j++;
		}

		if (iNumA > iNumB)
			return 1;
		if (iNumB > iNumA)
			return -1;

		//  if equal, reset variables and go for next numeric
		// part
		iNumA = iNumB = 0;
		i++;
		j++;
	}
	return 0;
}

int FWUpdateManager::handleEEpromFile(FWUpdateDescriptor* pDescr, int liIdx)
{
	if ( pDescr == nullptr )	return -1;

	string strPath(pDescr->getNodeDest(liIdx) + pDescr->getNodeName(liIdx));

	// Section is the only device which has the eeprom
	if ( pDescr->getNodeFqn(liIdx).find(XML_TAG_DEV_SECT) == string::npos )
	{
		// TODO Set outcome writing file
		return -1;
	}

	ifstream file(strPath);
	if ( ! file.is_open() )
	{
		m_pLogger->log(LOG_ERR, "FWUpdateManager::v: unable to open %s", strPath.c_str());
		return -1;
	}

	ostringstream xmlStream ;
	xmlStream << file.rdbuf();

	file.close();

	int liRes = parseXmlFile(xmlStream.str());
	if ( liRes )
	{
		// TODO Set outcome wrinting file
		return -1;
	}

	// update eeprom
	FwUpdateInfo* pInfo = new FwUpdateInfo();
	pInfo->m_eFwType = eEeprom;
	pInfo->m_strNewFwVers = m_pEeprom->getVersion();

	unsigned int uliMsecElapsed = 0;

	for ( int i = SCT_A_ID; i < SCT_NONE_ID; i++ )
	{
		pInfo->m_eId = ( i == SCT_A_ID ) ? eIdSectionA : eIdSectionB;
		m_pOp->setParamsFromManager(*pInfo);
		m_pOp->setEepromParamsFromManager(*m_pEeprom);
		// Update FwUpdateLogFile
		m_pOp->addItemToLog(pInfo->m_eId, pInfo->m_eFwType);

		sectionBoardManagerSingleton(i)->setSectionAction(SECTION_ACTION_FW_UPDATE, (Operation*)m_pOp);
		// wait for the action being completed
		while ( sectionBoardManagerSingleton(i)->checkIsPossibleSectionAction() == eOperationError )
		{
			msleep(200);
			uliMsecElapsed += 200;
			// Timeout max 4min
            if ( uliMsecElapsed >= (MAX_TIMEOUT_FOR_FW_UPD_MIN * SEC_PER_MIN * MSEC_PER_SEC) )
			{
				// TODO remove operation, update UpdateLog.log and return -1; ?
				break;
			}
		}
	}

	SAFE_DELETE(pInfo);

	// return result

	return 0;
}
