/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FWUpdateDescriptor.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the FWUpdateDescriptor class.
 @details

 ****************************************************************************
*/

#include "FWUpdateDescriptor.h"

FWUpdateDescriptor::FWUpdateDescriptor()
{
	m_Owner.strFwVersion.clear();
	m_Owner.strName.clear();
	m_vNode.clear();
	m_vComItems.clear();
	m_nNodesNum = 0;
}

FWUpdateDescriptor::~FWUpdateDescriptor()
{

}

void FWUpdateDescriptor::addNode()
{
	m_nNodesNum++;
	m_vNode.resize(m_nNodesNum);
}

void FWUpdateDescriptor::setNodeCRC32(string& strCRC32, int liNodeIdx)
{
	if ( liNodeIdx > m_nNodesNum )	return;

	// Only one step at a time is accepted
	if ( liNodeIdx == m_nNodesNum )
	{
		addNode();
	}

	m_vNode[liNodeIdx].strCRC32.assign(strCRC32);
}

void FWUpdateDescriptor::setNodeName(string& strName, int liNodeIdx)
{
	if ( liNodeIdx > m_nNodesNum )	return;

	// Only one step at a time is accepted
	if ( liNodeIdx == m_nNodesNum )
	{
		addNode();
	}

	m_vNode[liNodeIdx].strName.assign(strName);
}

void FWUpdateDescriptor::setNodeType(string& strType, int liNodeIdx)
{
	if ( liNodeIdx > m_nNodesNum )	return;

	// Only one step at a time is accepted
	if ( liNodeIdx == m_nNodesNum )
	{
		addNode();
	}

	m_vNode[liNodeIdx].strType.assign(strType);
}

void FWUpdateDescriptor::setNodeDest(string& strDest, int liNodeIdx)
{
	if ( liNodeIdx > m_nNodesNum )	return;

	// Only one step at a time is accepted
	if ( liNodeIdx == m_nNodesNum )
	{
		addNode();
	}

	m_vNode[liNodeIdx].strDest.assign(strDest);
}

void FWUpdateDescriptor::setNodeFqn(string& strFqn, int liNodeIdx)
{
	if ( liNodeIdx > m_nNodesNum )	return;

	// Only one step at a time is accepted
	if ( liNodeIdx == m_nNodesNum )
	{
		addNode();
	}

	m_vNode[liNodeIdx].strFqn.assign(strFqn);
}

void FWUpdateDescriptor::setNodeVersion(string& strVers, int liNodeIdx)
{
	if ( liNodeIdx > m_nNodesNum )	return;

	// Only one step at a time is accepted
	if ( liNodeIdx == m_nNodesNum )
	{
		addNode();
	}

	m_vNode[liNodeIdx].strFwVersion.assign(strVers);
}

void FWUpdateDescriptor::setOwnerName(string& strName)
{
	m_Owner.strName.assign(strName);
}

void FWUpdateDescriptor::setOwnerVersion(string& strVers)
{
	m_Owner.strFwVersion.assign(strVers);
}

void FWUpdateDescriptor::addCommandItem(string& strAction)
{
	if ( strAction.empty() )	return;

	m_vComItems.push_back(strAction);
}

int FWUpdateDescriptor::getNodesNum()
{
	return m_nNodesNum;
}

string FWUpdateDescriptor::getNodeCRC32(int liNodeIdx)
{
	if ( liNodeIdx >= m_nNodesNum )		return ("");

	return m_vNode[liNodeIdx].strCRC32;
}

string FWUpdateDescriptor::getNodeName(int liNodeIdx)
{
	if ( liNodeIdx >= m_nNodesNum )		return ("");

	return m_vNode[liNodeIdx].strName;
}

string FWUpdateDescriptor::getNodeType(int liNodeIdx)
{
	if ( liNodeIdx >= m_nNodesNum )		return ("");

	return m_vNode[liNodeIdx].strType;
}

string FWUpdateDescriptor::getNodeDest(int liNodeIdx)
{
	if ( liNodeIdx >= m_nNodesNum )		return ("");

	return m_vNode[liNodeIdx].strDest;
}

string FWUpdateDescriptor::getNodeFqn(int liNodeIdx)
{
	if ( liNodeIdx >= m_nNodesNum )		return ("");

	return m_vNode[liNodeIdx].strFqn;
}

string FWUpdateDescriptor::getNodeVersion(int liNodeIdx)
{
	if ( liNodeIdx >= m_nNodesNum )		return ("");

	return m_vNode[liNodeIdx].strFwVersion;
}

string FWUpdateDescriptor::getOwnerName()
{
	return m_Owner.strName;
}

string FWUpdateDescriptor::getOwnerVersion()
{
	return m_Owner.strFwVersion;
}

vector<string>& FWUpdateDescriptor::getCommandItems()
{
	return m_vComItems;
}

size_t FWUpdateDescriptor::getCommandsNum()
{
	return m_vComItems.size();
}

void FWUpdateDescriptor::clearAll()
{
	m_Owner = {};
	m_vNode.clear();
	m_vComItems.clear();
	m_nNodesNum = 0;
}
