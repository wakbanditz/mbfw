/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FWUpdateCommonInclude.h
 @author  BmxIta FW dept
 @brief   Contains the definition for the FWUpdateManager classes.
 @details

 ****************************************************************************
*/

#ifndef FWUPDATECOMMONINCLUDE_H
#define FWUPDATECOMMONINCLUDE_H

#include "StatusInclude.h"

typedef enum
{
    eFirmwareUpdateWait= 2,
	eFirmwareUpdateOK  = 0,
	eFirmwareUpdateErr = -1
}
eTypeFirmwareUpdateResult;

typedef enum
{
	eUpdFwBootLoader	= '0',
	eUpdFwApplicative	= '1',
	eUpdFwNone			= -1
} enumUpdFirmware;

typedef enum
{
	eKernel = 1,
    eMiniKernel = 2,
    eBootLoader = 3,
    eApplicative = 4,
    eFPGA = 5,
    eEeprom = 6,
    eFwNone = 7
} enumUpdType;

typedef enum
{
	eReset = 1,
    eRemove = 2,
    eForceOn = 3,
    eLockParam = 4,
    eUnlockParam = 5,
    eTransparentModeParam = 6,
    eChangeModalityNominal = 7,
    eChangeModalityUpgrade = 8,
    eResetNominal = 9,
    eResetUpgrade = 10,
    eRemoveCalibration = 11,
    eNumCmdTy = 12
} enumFWUpgradeCmd ;

typedef enum {
    eNOCheckCRCFile=0,
    eCheckCRCFileSrc=1<<0,
    eCheckCRCFileDst=1<<1,
    eCheckCRCFileSrcDst=(eCheckCRCFileSrc|eCheckCRCFileDst)
} eCheckCRCFileType;


class FwUpdateContainer
{
	public:

		FwUpdateContainer()
		{
			m_strUrl.clear();
			m_strForce.clear();
			m_strCRC32.clear();
			m_vComponents.clear();
		}

		virtual ~FwUpdateContainer(){}

    public:

        std::string m_strUrl;
        std::string m_strForce;
        std::string m_strCRC32;
        std::vector<std::string> m_vComponents;

};


class FwUpdateInfo
{
	public:

		FwUpdateInfo()
		{
			m_eId = eIdMax;
			m_eFwType = eFwNone;
			m_strSourcePath.clear();
			m_strNewFwVers.clear();
			m_lLength = 0;
			m_ullCrc32 = 0;
		}

		virtual ~FwUpdateInfo(){}

    public:

        eDeviceId m_eId;
        enumUpdType m_eFwType;
        std::string m_strSourcePath;
        std::string m_strNewFwVers;
        long m_lLength;
        unsigned long m_ullCrc32;

};


#endif // FWUPDATECOMMONINCLUDE_H
