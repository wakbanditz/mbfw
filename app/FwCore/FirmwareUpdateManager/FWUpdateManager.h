/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FWUpdateManager.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the FWUpdateManager class.
 @details

 ****************************************************************************
*/

#ifndef FWUPDATEMANAGER_H
#define FWUPDATEMANAGER_H

#include <xercesc/dom/DOMElement.hpp>
#include <list>

#include "FWUpdateDescriptor.h"
#include "FWUpdateEEprom.h"
#include "OpFwUpdate.h"
#include "Log.h"

/*! **********************************************************************************************************************
 * Fw update procedure: a .tar file will be sent to the Master board. It is composed by many parts:
 * - descriptor, that is an xml file where are indicated which fw files are contained in the folder and the fw version.
 * - .tar files of each fw update, composed as described below:
 *		- descriptor.
 *		- .bin files.
 *		- .xml files for tables update.
 *
 * The result of all the fw updates is stored in ./UPDATE/UpdateLog.log (cleaned once the OpFwDescriptor parse it).
 * ***********************************************************************************************************************
 */

using namespace xercesc_3_2;


class FWUpdateManager
{
	public:

		FWUpdateManager();

		virtual ~FWUpdateManager();

        /*! *************************************************************************************************
         * @brief  init initialize the variables for the FWUpdate procedure
         * @param  pLogger pointer to Logger object
         * @param pOp pointer to the Operation object
         * @param vComponentsToBeUpd vector containing the components to be updated
         * @param bForceUpd true if force (no version check) is enabled
         * @return 0 in case of success, false otherwise
         * **************************************************************************************************
         */
        int init(Log* pLogger, OpFwUpdate* pOp, vector<string>& vComponentsToBeUpd, bool bForceUpd);

        /*! *************************************************************************************************
         * @brief  createBackup creates a backup copy
         * @param  strTarPath source file
         * @param strTarDest destination file
         * @return 0 in case of success, false otherwise
         * **************************************************************************************************
         */
        int createBackup(string& strTarPath, string& strTarDest);

        /*! *************************************************************************************************
         * @brief updateDevices reentrant function to unzip tar file, decode and call related actions
         * @param strTarFile tar file to open
         * @param bOnlyDecode true if no real action has to be pefromed but only time evaluation
         * @return 0 in case of success, false otherwise
         * **************************************************************************************************
         */
        int updateDevices(string& strTarFile, bool bOnlyDecode = false);

        /*! *************************************************************************************************
         * @brief clearRubbish remove temporary files
         * @param strTarDir folder to be cleaned
         * @return 0 in case of success, false otherwise
         * **************************************************************************************************
         */
        int clearRubbish(string& strTarDir);

        /*! *************************************************************************************************
         * @brief clearFwUpdateManager cleanup of temporary variabless
         * **************************************************************************************************
         */
        void clearFwUpdateManager(void);

	private:

		/*! *************************************************************************************************
		 * @brief  unzipTarFile create a directory named strTarFile (without .tar extension) file in the same
		 *		   location as strTarFile
		 * @param  strTarFile name of the .tar file
		 * @return 0 in case of success, false otherwise
		 * **************************************************************************************************
		 */
		int unzipTarFile(string& strTarFile);

		int createUPDDescriptor(void);

		int parseUPDDescriptor(void);

        int executeAction(string& strAction, FWUpdateDescriptor* pDescr, bool bOnlyDecode);

		int parseXmlFile(string strXmlBuffer);

		int buildDescriptorFromRootNode(DOMElement* pXmlRootNode);

		int buildEepromParamFromRootNode(DOMElement* pXmlRootNode);

		void checkForRootPaths(vector<string>& vRootPaths, const string& strTmpPath);

		int getFileSize(string& strFileName);

		bool isVersionOk(FwUpdateInfo* pInfo);

		int compareVersions(string& a, string& b);

		int handleEEpromFile(FWUpdateDescriptor* pDescr, int liIdx);

        bool waitForUpdate(uint8_t device);

        int updateTotalTime(uint8_t device, uint8_t type);


    private:

        Log*	m_pLogger;
        string	m_strDirPath;
        string  m_strInstrPkt;
        bool	m_bConfigOk;
        bool	m_bForceUpd;

        OpFwUpdate*			m_pOp;
        FWUpdateDescriptor* m_pDescriptor;
        FwUpdateEEprom*		m_pEeprom;
        list<string>		m_lActionQueue;
        vector<string>		m_vComponents;
        uint32_t            m_timeUpdate, m_timeCurrent;
        uint8_t             m_percentageUpdate;

};

#endif // FWUPDATEMANAGER_H
