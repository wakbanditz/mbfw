#include "File.h"

#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#define READ_ONLY	"r"

const string File::m_fs = "/";
const string File::m_crlf = "\n";
const char File::m_cfs = '/';

const string File::m_KeyTar = ".tar";
const string File::m_KeyTxt = ".txt";
const string File::n_KeyXml = ".xml";

File::File()
{
	m_sFilename.clear();
	m_sAbsoluteFilename.clear();
	m_sFilePath.clear();
	m_isaFile = false;
}

File::File(string &sAbsFileNamePath)
{
	m_sFilename.clear();
	m_sAbsoluteFilename.clear();
	m_sFilePath.clear();
	m_isaFile = false;

	File::loadFileName(sAbsFileNamePath);
}

File::~File()
{

}

bool File::setLogger(Log* pLogger)
{
	if ( pLogger == 0 )	return false;

	m_pLogger = pLogger;
	return true;
}

string File::getFileName()
{
	return m_sFilename;
}

int File::loadFileName(string sAbsFileNamePath)
{
	if ( sAbsFileNamePath.empty() )		return -1;

	m_sAbsoluteFilename = sAbsFileNamePath;
	if ( isDir() )
	{
		m_sFilePath = sAbsFileNamePath;
		m_isaFile = false;
		return 2;
	}
	else
	{
		size_t usPos = sAbsFileNamePath.rfind(m_fs, sAbsFileNamePath.length());
		if ( usPos != string::npos )
		{
			m_sFilename = sAbsFileNamePath.substr(usPos + 1, sAbsFileNamePath.length() - usPos);
			m_sFilePath = sAbsFileNamePath.substr(0, usPos);
			m_isaFile = true;
			return 1;
		}
		// assign value of absolutefilenamepath to filename if it is only fileName
		if ( sAbsFileNamePath.length() > 0 )
		{
			m_sFilename = sAbsFileNamePath;
			m_isaFile = true;
			return 1;
		}
	}
	return -1;
}

bool File::isDir(void)
{
	struct stat statBuf;
	if ( stat(m_sAbsoluteFilename.c_str(), &statBuf) == -1)
	{
		m_pLogger->log(LOG_ERR, "File::isDirectory: wrong path or sys error");
		return false;
	}

	if ( S_ISDIR(statBuf.st_mode) )
	{
		return true;
	}
	return false;
}

bool File::isFile(void)
{
	struct stat statBuf;
	stat(m_sAbsoluteFilename.c_str(), &statBuf);

	if (S_ISREG(statBuf.st_mode))
	{
		return true;
	}
	return false;
}

int File::makeDir(void)
{
	int liRes = mkdir(m_sAbsoluteFilename.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
	return liRes;
}

int File::makeDir(string sPath)
{
	int liRes = mkdir(sPath.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
	return liRes;
}

bool File::exists()
{
	if ( m_isaFile )
	{
		return fileExists();
	}
	return dirExists();
}

int File::deleteEmptyDir()
{
	int liRes = rmdir(m_sAbsoluteFilename.c_str());
	return liRes;
}

int File::deleteDirRecursively(const char* dirName)
{
	DIR* dir;
	dir = opendir(dirName);
	if (dir == NULL)
	{
		m_pLogger->log(LOG_ERR, "File::deleteDirRecursively: error opening directory");
		return -1;
	}

	int liRes = 0;

	struct dirent* entry;
	char path[256];

	while ( (entry = readdir(dir)) != NULL )
	{
		/* Skip the names "." and ".." as we don't want to recurse on them. */
		if ( !strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
		{
		   continue;
		}

		snprintf(path, (size_t) PATH_MAX, "%s/%s", dirName, entry->d_name);
		struct stat statBuf;
		if ( ! stat(path, &statBuf) )
		{
			// If it is a directory then go inside it, otherwise delete the item.
			if ( S_ISDIR(statBuf.st_mode) )
			{
				liRes = deleteDirRecursively(path);
			}
			else
			{
				liRes = unlink(path);
			}
		}
	}
	closedir(dir);
	if ( isDirEmpty(dirName) )
	{
		liRes = rmdir(dirName);
		return liRes;
	}
	m_pLogger->log(LOG_INFO, "File::deleteDirRecursively: directory completely deleted.");
	return 0;
}

int File::deleteFile()
{
	int liRes = remove(m_sAbsoluteFilename.c_str());
	return liRes;
}

int File::listFiles(vector<File*>& listFiles, string sFilePath)
{
	vector<string> listFile;
	getFilesInDirectory(listFile, sFilePath);

	for (auto i = listFile.begin(); i != listFile.end(); ++i)
	{
		string sCurrFile = *i;
		File *pFile = new File(sCurrFile);
		if ( pFile != NULL )
		{
			listFiles.push_back(pFile);
		}
	}

	return ( listFiles.size() );
}

int File::isDirEmpty(const string& sDirectory)
{
	DIR* pDir;
	struct dirent* pEntry;
	struct stat statBuf;
	string sFileName;
	string sFullFileName;

	pDir = opendir(sDirectory.c_str());
	if ( pDir == 0 )		return -1;
	while ( (pEntry = readdir(pDir)) != NULL )
	{
		sFileName.clear();
		sFullFileName.clear();
		sFileName = pEntry->d_name;
		sFullFileName = sDirectory;
		sFullFileName += m_fs;
		sFullFileName += sFileName;

		if ( sFileName[0] == '.' )
			continue;

		if ( stat(sFullFileName.c_str(), &statBuf) == -1 )
			continue;

		closedir(pDir);
		return 1;
	}
	closedir(pDir);
	return 0;
}

int File::getElementsInDirectory(vector<string>& vsOut, const string& sDirectory)
{
	vsOut.clear();

	DIR* pDir;
	struct dirent* pEntry;
	struct stat statBuf;
	string sFileName;
	string sFullFileName;

	pDir = opendir(sDirectory.c_str());
	if ( pDir == 0 )		return -1;
	while ( (pEntry = readdir(pDir)) != NULL )
	{
		sFileName.clear();
		sFullFileName.clear();
		sFileName = pEntry->d_name;
		sFullFileName = sDirectory;
		sFullFileName += m_fs;
		sFullFileName += sFileName;

		if ( sFileName[0] == '.' )
			continue;

		if ( stat(sFullFileName.c_str(), &statBuf) == -1 )
			continue;

		vsOut.push_back(sFullFileName);
	}
	closedir(pDir);
	return 0;
}

int File::getFilesInDirectory(vector<string>& vsOut, const string& sDirectory)
{
	vsOut.clear();

	DIR* pDir;
	struct dirent* pEntry;
	struct stat statBuf;
	string sFileName;
	string sFullFileName;

	pDir = opendir(sDirectory.c_str());
	if ( pDir == 0 )		return -1;
	while ( (pEntry = readdir(pDir)) != NULL )
	{
		sFileName.clear();
		sFullFileName.clear();
		sFileName = pEntry->d_name;
		sFullFileName = sDirectory;
		sFullFileName += m_fs;
		sFullFileName += sFileName;

		if ( sFileName[0] == '.' )
			continue;

		if ( stat(sFullFileName.c_str(), &statBuf) == -1 )
			continue;

		const bool bIsDirectory = ( statBuf.st_mode & S_IFDIR ) != 0;

		if ( bIsDirectory )
			continue;

		vsOut.push_back(sFullFileName);
	}
	closedir(pDir);
	return 0;
}

int File::getFilesInDirectoryFilter(vector<string>& vsOut, const string& sDirectory, const string& sType)
{
	DIR *dir;
	struct dirent* entry;
	struct stat statBuf;
	string sFileName("");
	string sFullFileName("");
	string sFileNameExt("");

	dir = opendir(sDirectory.c_str());
	if ( dir == 0 )		return -1;
	while ( (entry = readdir(dir) ) != NULL )
	{
		sFileName.clear();
		sFullFileName.clear();
		sFileName = entry->d_name;
		sFullFileName = sDirectory;
		sFullFileName += m_fs;
		sFullFileName += sFileName;

		if ( sFileName[0] == '.' )
			continue;

		if ( stat(sFullFileName.c_str(), &statBuf) == -1 )
			continue;

		const bool bIsDir = ( statBuf.st_mode & S_IFDIR ) != 0;

		if ( bIsDir )
			continue;

		sFileNameExt = getFileExt(sFileName);

		if ( sFileNameExt.compare(sType) == 0 )
		{
			vsOut.push_back(sFullFileName);
		}
	}
	closedir(dir);
	return 0;
}

string File::getFileNameWidthOutExt(const string& sFileName)
{
	size_t usPos = sFileName.rfind('.', sFileName.length());

	if ( usPos != string::npos )
	{
		return (sFileName.substr(0, usPos));
	}
	return ("");
}

void File::getFileLen(unsigned long& ullFileLen)
{
	struct stat statbuf;
	ullFileLen = 0;

	if ( stat(m_sAbsoluteFilename.c_str(), &statbuf) != -1)
	{
		ullFileLen = statbuf.st_size;
	}
}

void File::reset()
{
	m_sFilename.clear();
	m_sAbsoluteFilename.clear();
	m_sFilePath.clear();
	m_isaFile = false;
}

bool File::readFile(string sFileName, uint8_t* rgcFile, unsigned long lliFileSize)
{
	if ( rgcFile == 0 )		return false;
	if ( lliFileSize == 0 )	return false;

	ifstream file(sFileName);

	if ( file )
	{
		while ( file.read((char*)rgcFile, lliFileSize) ) {};

		file.close();
		return true;
	}

	m_pLogger->log(LOG_ERR, "File::readFile: unable to open %s", sFileName.c_str());
	return false;
}

string File::getFileExt(const string& sFileName)
{
	string sFileExt("");
	size_t usPos = sFileName.rfind('.', sFileName.length());
	if ( usPos != string::npos)
	{
		sFileExt = sFileName.substr(usPos + 1, sFileName.length() - usPos);
	}
	return sFileExt;
}

bool File::fileExists(void)
{
	FILE *fb = fopen(m_sAbsoluteFilename.c_str(), READ_ONLY);

	if ( fb != NULL )
	{
		fclose(fb);
		return true;
	}
	else
	{
		return false;
	}
}

bool File::dirExists(void)
{
	struct stat statBuf;

	if ( stat(m_sAbsoluteFilename.c_str(), &statBuf) == 0 && S_ISDIR(statBuf.st_mode) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

