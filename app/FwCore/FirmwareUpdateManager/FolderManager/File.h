#ifndef FILE_H
#define FILE_H

#include <string>
#include <string.h>
#include <vector>

#include "CommonInclude.h"
#include "Log.h"

using namespace std;

class File
{
	private:

		string m_sFilename;
		string m_sAbsoluteFilename;
		string m_sFilePath;
		bool m_isaFile;

		Log* m_pLogger;

	public:

		static const string m_fs;
		static const string m_crlf;
		static const char m_cfs;
		static const string  m_KeyTar;
		static const string  m_KeyTxt;
		static const string  n_KeyXml;

	public:

		File();
		File(string& sAbsFileNamePath);
		virtual ~File();

		bool setLogger(Log* pLogger);
		string getFileName(void);
		int loadFileName(string sAbsFileNamePath);
		bool isDir(void);
		bool isFile(void);
		int makeDir(void);
		int makeDir(string sPath);
		bool exists(void);
		int deleteFile(void);
		int deleteEmptyDir(void);
		int deleteDirRecursively(const char* dirName);
		int isDirEmpty(const string& sDirectory);
		int getElementsInDirectory(vector<string>& vsOut, const string& sDirectory);
		int listFiles(vector<File *> &listFiles, string sFilePath);
		int getFilesInDirectory(vector<string> &vsOut, const string &sDirectory);
		int getFilesInDirectoryFilter(vector<string> &vsOut, const string &sDirectory, const string &sType);
		string getFileNameWidthOutExt(const string& sFileName);
		void getFileLen(unsigned long &ullFileLen);
		void reset(void);

		bool readFile(string sFileName, uint8_t* rgcFile, unsigned long lliFileSize);



	private:

		string getFileExt(const string& sFileName);
		bool fileExists(void);
		bool dirExists(void);
};

#endif // FILE_H
