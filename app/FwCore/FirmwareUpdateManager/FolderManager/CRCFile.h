#ifndef CRCFILE_H
#define CRCFILE_H

#include <string>

#include "Log.h"

using namespace std;

#define SUCCESS				1

class CRCFile
{		
	public:

		typedef enum
		{
			eMaxAllocMemoryFile = (4L*1024L)
		} eMaxAllocMemory;

		Log* m_pLogger;
		unsigned long crcvalue;

	public:

		CRCFile(Log* pLogger);
		virtual ~CRCFile(void);

		int calculateCRCFile(const string &sFileName, unsigned long &ullCRCvalue);
		int calculateCRCFileMaxAlloc(const string &sFileName, unsigned long &ulCRCvalue, unsigned long ulMaxAlloc = eMaxAllocMemoryFile);

		int CRC16FileMaxAlloc(const string &filename,unsigned short &_crc16value,unsigned long maxAlloc);

	private:

		unsigned long get32crc(const unsigned char* pBuf, int liLen);
		void calc32crc(unsigned long* pCrc32, unsigned char cVal);

//		void crc16_seq(short *crc, unsigned char m);

};

#endif // CRCFILE_H
