/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FWUpdateEEprom.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the FWUpdateEEprom class.
 @details

 ****************************************************************************
*/

#include "FWUpdateEEprom.h"

FwUpdateEEprom::FwUpdateEEprom()
{
	m_nSpeedNodesNum = 0;
	m_nVolumeNodesNum = 0;
	m_strBoardType.clear();
	m_strVersion.clear();
	tSpeed.eFormat = eNoBase;
	tSpeed.vRecord.clear();
	tVolume.eFormat = eNoBase;
	tVolume.vRecord.clear();
}

FwUpdateEEprom::~FwUpdateEEprom()
{

}

void FwUpdateEEprom::addSpeedNode()
{
	m_nSpeedNodesNum++;
	tSpeed.vRecord.resize(m_nSpeedNodesNum);
}

void FwUpdateEEprom::addVolumeNode()
{
	m_nVolumeNodesNum++;
	tVolume.vRecord.resize(m_nVolumeNodesNum);
}

void FwUpdateEEprom::setBoardType(string& strType)
{
	m_strBoardType.assign(strType);
}

void FwUpdateEEprom::setVersion(string& strVersion)
{
	m_strVersion.assign(strVersion);
}

void FwUpdateEEprom::setSpeedFormat(string& strFormat)
{
	tSpeed.eFormat = strFormat.compare("HEX") ? eDec : eHex;
}

void FwUpdateEEprom::setSpeedIdx(string& strSpeedIdx, int liIdx)
{
	if ( liIdx > m_nSpeedNodesNum )	return;

	// Only one step at a time is accepted
	if ( liIdx == m_nSpeedNodesNum )
	{
		addSpeedNode();
	}

	tSpeed.vRecord[liIdx].usIdx = stoi(strSpeedIdx, nullptr, 16);
}

void FwUpdateEEprom::setSpeedVal(string& strSpeedVal, int liIdx)
{
	if ( liIdx > m_nSpeedNodesNum )	return;

	// Only one step at a time is accepted
	if ( liIdx == m_nSpeedNodesNum )
	{
		addSpeedNode();
	}

	switch ( tSpeed.eFormat )
	{
		case eHex:
			tSpeed.vRecord[liIdx].llValue = stoi(strSpeedVal, nullptr, 16);
		break;

		default:
			tSpeed.vRecord[liIdx].llValue = stoi(strSpeedVal);
		break;
	}
}

void FwUpdateEEprom::setVolumeFormat(string& strFormat)
{
	tVolume.eFormat = strFormat.compare("HEX") ? eDec : eHex;
}

void FwUpdateEEprom::setVolumeIdx(string& strVolIdx, int liIdx)
{
	if ( liIdx > m_nVolumeNodesNum )	return;

	// Only one step at a time is accepted
	if ( liIdx == m_nVolumeNodesNum )
	{
		addVolumeNode();
	}

	tVolume.vRecord[liIdx].usIdx = stoi(strVolIdx, nullptr, 16);
}

void FwUpdateEEprom::setVolumeVal(string& strVolVal, int liIdx)
{
	if ( liIdx > m_nVolumeNodesNum )	return;

	// Only one step at a time is accepted
	if ( liIdx == m_nVolumeNodesNum )
	{
		addVolumeNode();
	}

	switch ( tVolume.eFormat )
	{
		case eHex:
			tVolume.vRecord[liIdx].llValue = stoi(strVolVal, nullptr, 16);
		break;

		default:
			tVolume.vRecord[liIdx].llValue = stoi(strVolVal);
		break;
	}
}

string FwUpdateEEprom::getBoardType()
{
	return m_strBoardType;
}

string FwUpdateEEprom::getVersion()
{
	return m_strVersion;
}

uint16_t FwUpdateEEprom::getSpeedIdx(int liIdx)
{
	if ( liIdx >= m_nSpeedNodesNum )	return 0;

	return tSpeed.vRecord[liIdx].usIdx;
}

int64_t FwUpdateEEprom::getSpeedValue(int liIdx)
{
	if ( liIdx >= m_nSpeedNodesNum )	return 0;

	return tSpeed.vRecord[liIdx].llValue;
}

uint16_t FwUpdateEEprom::getVolumeIdx(int liIdx)
{
	if ( liIdx >= m_nVolumeNodesNum )	return 0;

	return tVolume.vRecord[liIdx].usIdx;
}

int64_t FwUpdateEEprom::getVolumeValue(int liIdx)
{
	if ( liIdx >= m_nVolumeNodesNum )	return 0;

	return tVolume.vRecord[liIdx].llValue;
}

int FwUpdateEEprom::getSpeedTableSize()
{
	return tSpeed.vRecord.size();
}

int FwUpdateEEprom::getVolumeTableSize()
{
	return tVolume.vRecord.size();
}

void FwUpdateEEprom::clearAll()
{
	m_nSpeedNodesNum = 0;
	m_nVolumeNodesNum = 0;
	m_strBoardType.clear();
	m_strVersion.clear();
	tSpeed.eFormat = eNoBase;
	tSpeed.vRecord.clear();
	tVolume.eFormat = eNoBase;
	tVolume.vRecord.clear();
}
