/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FWUpdateEEprom.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the FWUpdateEEprom class.
 @details

 ****************************************************************************
*/

#ifndef FWUPDATEEEPROM_H
#define FWUPDATEEEPROM_H

#include "FWUpdateCommonInclude.h"

using namespace std;

struct record
{
	uint16_t usIdx;
	int64_t  llValue;
};


typedef enum { eHex, eDec, eNoBase } eBase;

struct table
{
	eBase eFormat; // note this is only what is written in the xml file. All the values are dec.
	vector<record> vRecord;
};

class FwUpdateEEprom
{
	public:

        /*! *************************************************************************************************
         * @brief constructor
         * **************************************************************************************************
         */
        FwUpdateEEprom();

        /*! *************************************************************************************************
         * @brief destructor
         * **************************************************************************************************
         */
        virtual ~FwUpdateEEprom();

        /*! *************************************************************************************************
         * @brief SETTER functions
         * **************************************************************************************************
         */
        void addSpeedNode();
        void addVolumeNode();

        void setBoardType(string& strType);
		void setVersion(string& strVersion);
		void setSpeedFormat(string& strFormat); // dec or hex values
		void setSpeedIdx(string& strSpeedIdx, int liIdx);
		void setSpeedVal(string& strSpeedVal, int liIdx);
		void setVolumeFormat(string& strFormat); // dec or hex values
		void setVolumeIdx(string& strVolIdx, int liIdx);
		void setVolumeVal(string& strVolVal, int liIdx);

        void clearAll(void);

        /*! *************************************************************************************************
         * @brief GETTER functions
         * **************************************************************************************************
         */
        string getBoardType(void);
		string getVersion(void);
		uint16_t getSpeedIdx(int liIdx);
		int64_t getSpeedValue(int liIdx);
		uint16_t getVolumeIdx(int liIdx);
		int64_t getVolumeValue(int liIdx);
		int getSpeedTableSize(void);
		int getVolumeTableSize(void);

    private:

        string m_strBoardType;
        string m_strVersion;

        int	m_nSpeedNodesNum;
        int	m_nVolumeNodesNum;

        table tSpeed;
        table tVolume;

};

#endif // FWUPDATEEEPROM_H
