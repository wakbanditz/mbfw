#ifndef FWUPDATEINTERFACE_H
#define FWUPDATEINTERFACE_H

#include <vector>
#include <string>

#include "FWUpdateCommonInclude.h"
#include "Loggable.h"


/*! **********************************************************************************************************************
 * Fw update procedure: a .tar file will be sent to the Master board. It is composed by many parts:
 * - descriptor, that is an xml file where are indicated which fw files are contained in the folder and the fw version.
 * - .tar files of each fw update, composed as described below:
 *		- descriptor.
 *		- .bin files.
 *		- .xml files for tables update.
 * ***********************************************************************************************************************
 */

using namespace std;

class FWUpdateInterface : public Loggable
{
	public:

		bool m_bFirmwareForce;

	protected:

		int m_liFirmwareUpdateResult;
		int m_liTotalCountElements;
		int m_liProgressElement;

	public:

		FWUpdateInterface();
		virtual ~FWUpdateInterface();

		virtual int getVersion(enumFWType /*eFwType*/, string& sVersion);
		virtual int prepareToFwUpdate(bool /*start*/=true);
		virtual int updateFirmware(enumFWType /*eFwType*/ , string& /*sSourcePath*/,long /*lLength*/, unsigned long /*ullCrc32*/);
		virtual int exeCommand(enumFWUpgradeCmd /*eCommand*/)=0;

	protected:

		void increaseOpProgress(void);
		int getOperationProgress(void);
		void setTotalOp(unsigned long ulTotalOp);
		void addToTotalOp(unsigned long ulOpNum);
};

#endif // FWUPDATEINTERFACE_H
