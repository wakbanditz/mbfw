#ifndef FWUPDATECOMMONINCLUDE_H
#define FWUPDATECOMMONINCLUDE_H


typedef enum
{
	eFirmwareUpdateWait= 2,
	eFirmwareUpdateOK  = 0,
	eFirmwareUpdateErr = -1
}
eTypeFirmwareUpdateResult;

typedef enum
{
	eKernel = 1,
	eBootLoader,
	eApplicative,
	eFPGA,
	eNumFwTy
} enumFWType;


typedef enum
{
	eReset = 1,
	eRemove,
	eForceOn,
	eLockParam,
	eUnlockParam,
	eTransparentModeParam,
	eChangeModalityNominal,
	eChangeModalityUpgrade,
	eResetNominal,				// 20150721 to reset COP with command F0000....
	eResetUpgrade,				// 20150721 to reset COP with command :S050....
	eRemoveCalibration,			// 20150908 command for clear board calibration
	eNumCmdTy
} enumFWUpgradeCmd ;




#endif // FWUPDATECOMMONINCLUDE_H
