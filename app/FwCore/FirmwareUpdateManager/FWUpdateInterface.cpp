/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FWUpdateInterface.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the FWUpdateInterface class.
 @details

 ****************************************************************************
*/

#include "FWUpdateInterface.h"


FWUpdateInterface::FWUpdateInterface()
{
	m_bFirmwareForce = false;
	m_liFirmwareUpdateResult = eFirmwareUpdateErr;
	m_liTotalCountElements = -1;
	m_liProgressElement = -1;
}

FWUpdateInterface::~FWUpdateInterface()
{

}

int FWUpdateInterface::getVersion(enumUpdFirmware, string& sVersion)
{
	sVersion.clear();
	return eFirmwareUpdateOK;
}

int FWUpdateInterface::prepareToFwUpdate(bool)
{
	return eFirmwareUpdateOK;
}

int FWUpdateInterface::updateFirmware(enumUpdFirmware, string&, long, unsigned long)
{
	return eFirmwareUpdateOK;
}


int FWUpdateInterface::exeCommand(enumFWUpgradeCmd)
{
	return eFirmwareUpdateOK;
}

void FWUpdateInterface::increaseOpProgress()
{
	m_liProgressElement++;
}

void FWUpdateInterface::setTotalOp(unsigned long ulTotalOp)
{
	m_liTotalCountElements = ulTotalOp;
}

void FWUpdateInterface::addToTotalOp(unsigned long ulOpNum)
{
	m_liTotalCountElements += ulOpNum;
}

int FWUpdateInterface::getOperationProgress()
{
    double fPercProgress;
	int liResProgress = 0;

	if ( m_liTotalCountElements > 0 )
	{
        fPercProgress = (double)m_liProgressElement * (double)100.0;
        fPercProgress /= (double)m_liTotalCountElements;
		liResProgress = (int)fPercProgress;
	}
	return liResProgress;
}
