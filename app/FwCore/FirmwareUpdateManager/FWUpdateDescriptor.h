/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FWUpdateDescriptor.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the FWUpdateDescriptor class.
 @details

 ****************************************************************************
*/

#ifndef FWUPDATECONTAINER_H
#define FWUPDATEDESCRIPTOR_H

#include "FWUpdateCommonInclude.h"

using namespace std;

typedef struct
{
	string strFwVersion;
	string strName;
} structOwnerInfo;

typedef struct
{
	string strCRC32;
	string strName;
	string strType;
	string strDest;
	string strFqn; // fully qualified name
	string strFwVersion;
} structNodeInfo;

class FWUpdateDescriptor
{
	public:

        /*! *************************************************************************************************
         * @brief default constructor
         * **************************************************************************************************
         */
        FWUpdateDescriptor();

        /*! *************************************************************************************************
         * @brief destructor
         * **************************************************************************************************
         */
        virtual ~FWUpdateDescriptor();

        /*! *************************************************************************************************
         * @brief to add a node to the structure
         * **************************************************************************************************
         */
        void addNode(void);

        /*! *************************************************************************************************
         * @brief SETTER functions
         * **************************************************************************************************
         */

		void setNodeCRC32(string& strCRC32, int liNodeIdx);
		void setNodeName(string& strName, int liNodeIdx);
		void setNodeType(string& strType, int liNodeIdx);
		void setNodeDest(string& strDest, int liNodeIdx);
		void setNodeFqn(string& strFqn, int liNodeIdx);
		void setNodeVersion(string& strVers, int liNodeIdx);
		void setOwnerName(string& strName);
		void setOwnerVersion(string& strVers);
		void addCommandItem(string& strAction);

        void clearAll(void);

        /*! *************************************************************************************************
         * @brief GETTER functions
         * **************************************************************************************************
         */

        int getNodesNum(void);
		string getNodeCRC32(int liNodeIdx);
		string getNodeName(int liNodeIdx);
		string getNodeType(int liNodeIdx);
		string getNodeDest(int liNodeIdx);
		string getNodeFqn(int liNodeIdx);
		string getNodeVersion(int liNodeIdx);
		string getOwnerName(void);
		string getOwnerVersion(void);
		vector<string>& getCommandItems(void);
		size_t getCommandsNum(void);

private:

    vector<string>			m_vComItems;
    structOwnerInfo			m_Owner;
    vector<structNodeInfo>	m_vNode;
    int						m_nNodesNum;

};

#endif // FWUPDATECONTAINER_H
