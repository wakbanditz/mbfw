INCLUDEPATH += $$PWD/../FirmwareUpdateManager/

HEADERS += \
    $$PWD/FWUpdateInterface.h \
    $$PWD/FWUpdateCommonInclude.h \
    $$PWD/FWUpdateManager.h \
    $$PWD/FWUpdateDescriptor.h \
    $$PWD/FWUpdateEEprom.h

SOURCES += \
    $$PWD/FWUpdateInterface.cpp \
    $$PWD/FWUpdateManager.cpp \
    $$PWD/FWUpdateDescriptor.cpp \
    $$PWD/FWUpdateEEprom.cpp
    

