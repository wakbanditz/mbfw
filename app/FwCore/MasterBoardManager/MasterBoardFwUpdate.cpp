#include <iostream>
#include <fstream>

#include "MasterBoardFwUpdate.h"
#include "CRCFile.h"
#include "FolderManager.h"



MasterBoardFwUpdate::MasterBoardFwUpdate()
{

}

MasterBoardFwUpdate::~MasterBoardFwUpdate()
{

}

int MasterBoardFwUpdate::CopyFiles(string &sourcepath, string &dstpath, unsigned long len, unsigned long crc32, int checkFile)
{
//    int ierr;
//    int progress;
    unsigned long lenFileCopied;
    int errCheck=0;
    unsigned long crc32Check;
    CRCFile crcFile(m_pLogger);
//	ierr=FwUpdateInterface::eFirmwareUpdateErr;
    lenFileCopied=0;
//	totalCountElements=3;
//	progressElement=0;
//	lastCountElemet=0;
//    progress=get_progressTableOP();
    //check dst file if it is requested
    if ((checkFile&eCheckCRCFileSrc)==eCheckCRCFileSrc)
    {
        errCheck = crcFile.calculateCRCFileMaxAlloc(sourcepath, crc32Check, crcFile.eMaxAllocMemoryFile);
//        errCheck=crcFile.CRCFileMaxAlloc(sourcepath, crc32Check, crcFile::eMaxAllocMemory);
        if ((errCheck != 0) || (crc32Check!=crc32))
        {
//            errCheck=Failure;
            log(LOG_ERR,"## CMainBoardFWUpd::copyfile ErrCRCSrcfile :%s exp[%x] calc[%x]##\n",sourcepath.c_str(),crc32,crc32Check);
        }
        else
        {
//            progressElement=1;
//            progress=get_progressTableOP();
        }
    }
    //copy file on the destination
    if (errCheck == 0)
    {
        ifstream sourceFile(sourcepath.c_str(), ios::binary);
        ofstream destFile(dstpath.c_str(), ios::binary);

        if ((sourceFile.is_open()) &&
            (destFile.is_open()))
        {
            istreambuf_iterator<char> begin_src(sourceFile);
            istreambuf_iterator<char> end_src;
            ostreambuf_iterator<char> begin_dst(destFile);
            std::copy(begin_src, end_src, begin_dst);
            destFile.flush();
            lenFileCopied=destFile.tellp();
            log(LOG_INFO,"## CMainBoardFirmwareUpdate::copyfile Len[%d] ##\n",lenFileCopied);
            log(LOG_INFO,"## [%s]->[%s] ##\n",sourcepath.c_str(),dstpath.c_str());
            if (lenFileCopied!=len)
            {
                log(LOG_ERR,"## LenError Copied[%d]- Expected[%d] ##\n",lenFileCopied,len);
                return -1;
            }
            else
            {
//                progressElement=2;
//                progress=get_progressTableOP();
            }
        }
        else
        {
            if (!(sourceFile.is_open()))
            {
                log(LOG_ERR,"## CMainBoardFirmwareUpdate::copyfile ErrOpenSrcfile :%s ##\n",sourcepath.c_str());
            }
            if (!(destFile.is_open()))
            {
                log(LOG_ERR,"## CMainBoardFirmwareUpdate::copyfile ErrOpenDstfile :%s ##\n",dstpath.c_str());
            }
        }

        if (sourceFile.is_open())
        {
            sourceFile.close();
        }
        if (destFile.is_open())
        {
            destFile.close();
        }
    }
    //check dst file if it is requested
    if ((errCheck == 0) && ((checkFile&eCheckCRCFileDst) == eCheckCRCFileDst))
    {
        errCheck = crcFile.calculateCRCFileMaxAlloc(dstpath,crc32Check,crcFile.eMaxAllocMemoryFile);
        if ((errCheck!= 0)||
            (crc32Check!=crc32))
        {
//            errCheck=Failure;
            log(LOG_ERR,"## CMainBoardFWUpd::copyfile ErrorCRCDstfile :%s exp[%x] calc[%x]##\n",dstpath.c_str(),crc32,crc32Check);
        }
        else
        {
//            progressElement=3;
//            progress=get_progressTableOP();
        }
    }

//    if (errCheck==0)
//    {
//        ierr=FwUpdateInterface::eFirmwareUpdateOK;
//    }

    return 0;
}

int MasterBoardFwUpdate::updatefirmware(enumUpdType fwtype, string &source_path, long length, unsigned long crc32)
{
    int resUpdateFirmware;
    int errCopyFiles;
//    int progress;
    int errCheck;
    unsigned long crc32tmp;
    unsigned long crc32additional;
    unsigned long lengthadditional;
    CRCFile crcFile(m_pLogger);
    FolderManager fileImage;
    //////////////////////////

    string tmpDstImage;
    string tmpRenImage;

    tmpDstImage.assign(FWCORE_DEFAULT_APPLICATION);
    tmpRenImage.assign(FWCORE_OLD_DEFAULT_APPLICATION);

//    totalCountElements=4;
//    progressElement=0;
//    lastCountElemet=0;
//    progress=get_progressTableOP();
    log(LOG_INFO,"## Firmware Update Applicative [%s] ##\n",source_path.c_str());
    ////////////////////////////////////////////////////////
    //// calculate and check crc of the source file
    errCopyFiles=crcFile.calculateCRCFileMaxAlloc(source_path,crc32tmp,CRCFile::eMaxAllocMemoryFile);
//    progressElement++;
//    progress=get_progressTableOP();
    resUpdateFirmware = eFirmwareUpdateErr;
//    errCheck=Failure;
    if (errCopyFiles == 0)
    {
        if (crc32tmp==crc32)
        {
            //save old Application
            errCheck = crcFile.calculateCRCFileMaxAlloc(tmpDstImage, crc32additional, CRCFile::eMaxAllocMemoryFile);
            if (errCheck == 0)
            {
//                progressElement++;
//                progress=get_progressTableOP();

                fileImage.loadFileName(tmpDstImage);
                fileImage.getFileLen(lengthadditional);

                log(LOG_INFO,"## Copy [%s] -> [%s] ##\n", tmpDstImage.c_str(), tmpRenImage.c_str());
                log(LOG_INFO,"--> len[%d] crc[%08X]\n", lengthadditional, crc32additional);
                errCopyFiles = CopyFiles(tmpDstImage, tmpRenImage, lengthadditional, crc32additional, eCheckCRCFileDst);
                log(LOG_INFO,"--> result[%d]\n",errCopyFiles);
//                progressElement++;
//                progress=get_progressTableOP();
                //copy it is not mandatory
            }
            /////////////////////////
            // save applicative and check crc of the source file
            errCopyFiles=CopyFiles(source_path, tmpDstImage, length, crc32tmp, eCheckCRCFileDst);
            if (errCopyFiles == eFirmwareUpdateOK)
            {
                resUpdateFirmware = eFirmwareUpdateOK;
//                progressElement=totalCountElements;
//                progress=get_progressTableOP();
            }
        }//(crc32tmp==crc32)
    ////////////////////////////////////////////////////////
    }

    if (resUpdateFirmware==eFirmwareUpdateErr)
    {
        return -1;
//        MainError.ErrorSrcReport=1;
//        MainError.srcErr="APPLICATIVE";
//        errConvError=objConvError2Alarm.ConvertErrorDevice2Alarm(MAINBOARDUNIT_SHARED,MainError,lAlarms);
    }
    return 0;
}
