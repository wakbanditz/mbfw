/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    MasterBoardManager.cpp
 @author  BmxIta FW dept
 @brief   Contains the declaration for the MasterBoardManager class.
 @details

 ****************************************************************************
*/

#include "FanManager.h"

#include "CommonInclude.h"
#include "ModuleInclude.h"
#include "MainExecutor.h"

#include "ADT7470.h"

#define RPM_COMPARISON_TOLERANCE 30

FanManager::FanManager(std::string name, uint8_t index, uint16_t errorCode)
{
    m_strName.assign(name);
    m_index = index;
    m_errorCode = errorCode;

    resetValues();
    m_tablePwmRpm.clear();
    m_tablePwmNtc.clear();

    if(m_strName.empty() == false)
    {
        readRpmFile();
        readTemperatureFile();
    }
}

void FanManager::resetValues()
{
    m_power = 0;
    m_setting = 0;
    m_rpm = 0;
}

void FanManager::setPower(uint8_t power)
{
    if(power > 100) power = 100;

    m_power = power;
    if(MainExecutor::getInstance()->m_i2cBoard.m_pFanController)
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(m_index, power);
    }
}

void FanManager::setSetting(uint8_t setting)
{
    if(setting > 100) setting = 100;

    m_setting = setting;
}

uint8_t FanManager::getIndex()
{
    return m_index;
}

uint16_t FanManager::getErrorCode()
{
    return m_errorCode;
}

uint16_t FanManager::getRpm()
{
    return m_rpm;
}

uint8_t FanManager::getSetting()
{
    return m_setting;
}

uint8_t FanManager::getPower()
{
    return m_power;
}

bool FanManager::readRpm()
{
    if(MainExecutor::getInstance()->m_i2cBoard.m_pFanController)
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pFanController->readFan(m_index, m_rpm);

        uint16_t expectedRpm = 0;

        // detect the expected value from the table
        for(uint8_t i = 0; i < m_tablePwmRpm.size(); i++)
        {
            if(m_power >= m_tablePwmRpm.at(i).first)
            {
                expectedRpm = m_tablePwmRpm.at(i).second;
                break;
            }
        }

        // error is detected if the measured is not over the expected - percentage
        if(expectedRpm > 0)
        {
            double tolerance = expectedRpm * ( 100 - RPM_COMPARISON_TOLERANCE) / 100;
            if( m_rpm < (uint16_t)tolerance)
            {
                return false;
            }
        }
    }
    return true;
}

void FanManager::setTemperaturePower(uint16_t temperature)
{
    for(uint8_t i = 0; i < m_tablePwmNtc.size(); i++)
    {
        if(temperature >= m_tablePwmNtc.at(i).first)
        {
            m_power = m_tablePwmNtc.at(i).second;
            break;
        }
    }
}

void FanManager::transferPower()
{
    if(MainExecutor::getInstance()->m_i2cBoard.m_pFanController)
    {
        MainExecutor::getInstance()->m_i2cBoard.m_pFanController->writeCurrentDutyCycle(m_index, m_power);
    }
}


uint8_t FanManager::readRpmFile()
{
    std::string fileName, strRead;
    std::ifstream inFile;

    fileName.assign(FANS_FOLDER);
    fileName.append("fan");
    fileName.append(m_strName);
    fileName.append(".cfg");

    inFile.open(fileName);
    if(inFile.fail())
    {
        return 0;
    }

    while(!inFile.eof())
    {
        getline(inFile, strRead);
        if(!strRead.empty())
        {
            // lines beginning with # are comments
            if(strRead.front() != '#')
            {
                // split the line separating the fields
                std::vector<std::string> listStr;

                listStr = splitString(strRead, HASHTAG_SEPARATOR);
                if(listStr.size() == 2)
                {
                    // split is valid: assign values
                    m_tablePwmRpm.emplace_back((uint8_t)atoi(listStr.at(0).c_str()),
                                               (uint16_t)atoi(listStr.at(1).c_str()));

                }
            }
        }
    }
    inFile.close();
    return m_tablePwmRpm.size();
}

uint8_t FanManager::readTemperatureFile()
{
    std::string fileName, strRead;
    std::ifstream inFile;

    fileName.assign(FANS_FOLDER);
    fileName.append("fanTemperature");
    fileName.append(m_strName);
    fileName.append(".cfg");

    inFile.open(fileName);
    if(inFile.fail())
    {
        return 0;
    }

    while(!inFile.eof())
    {
        getline(inFile, strRead);
        if(!strRead.empty())
        {
            // lines beginning with # are comments
            if(strRead.front() != '#')
            {
                // split the line separating the fileds
                std::vector<std::string> listStr;

                listStr = splitString(strRead, HASHTAG_SEPARATOR);
                if(listStr.size() == 2)
                {
                    // split is valid: assign values
                    m_tablePwmNtc.emplace_back((uint16_t)atoi(listStr.at(0).c_str()),
                                               (uint8_t)atoi(listStr.at(1).c_str()));

                }
            }
        }
    }
    inFile.close();
    return m_tablePwmNtc.size();
}
