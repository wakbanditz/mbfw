/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    MasterBoardManager.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the MasterBoardManager class.
 @details

 ****************************************************************************
*/

#ifndef MASTERBOARDMANAGER_H
#define MASTERBOARDMANAGER_H

#include <vector>

#include "Thread.h"
#include "StaticSingleton.h"
#include "Loggable.h"
#include "Semaphore.h"

#include "CommonInclude.h"
#include "ModuleInclude.h"
#include "FWUpdateCommonInclude.h"
#include "Operation.h"


class MasterBoardError;
class Operation;
class FanManager;

class MasterBoardManager : public StaticSingleton<MasterBoardManager>,
							public Thread,
							public Loggable
{
	public:

		/*! ***********************************************************************************************************
		 * @brief MasterBoardManager constructor
		 * ************************************************************************************************************
		 */
		MasterBoardManager();

		/*! ***********************************************************************************************************
		 * @brief MasterBoardManager destructor
		 * ************************************************************************************************************
		 */
		virtual ~MasterBoardManager();

		/*! ***********************************************************************************************************
		 * @brief to set the action the thread will execute
		 * @param action the action code
		 * @param pOp pointer to a operation (if any) linked to the action
		 * @return true if action is accepted, false otherwise
		 * ************************************************************************************************************
		 */
		int setMasterAction(int action, Operation* pOp = 0);

		/*! ***********************************************************************************************************
		 * @brief to initialize the object
		 * ************************************************************************************************************
		 */
		void initMasterBoardManager();

        /*!
         * \brief stopMasterBoardManager
         */
        void stopMasterBoardManager();

		/*!
		 * @brief clearAllErrors	to reset the active flag of all errors (at board initializaztion)
		 */
		void clearAllErrors();

		/*! ***********************************************************************************************************
         * @brief setErrorFromCode to search in the error list the one with the requested code and return
		 *				the corresponding info (don't care about active status)
		 * @param code the error code
         * @param bKeepSet if true the error is mantained, if false is immediately reset (command rejection failure)
		 * @return the string formatted as timestamp#code#category#description#component
		 * ************************************************************************************************************
		 */
		std::string setErrorFromCode(uint16_t code, bool bKeepSet = false);

        /*! ***********************************************************************************************************
         * @brief isErrorFromCodeActive to search in the error list the one with the requested code and return
         *				the corresponding active status
         * @param code the error code
         * @return true if error is set, false otherwise
         * ************************************************************************************************************
         */
        bool isErrorFromCodeActive(uint16_t code);

		/*! ***********************************************************************************************************
		 * @brief getErrorList retrieve the errors active on the section: each error corresponds to an element
		 *			in the vector: the string is formatted as timestamp#code#category#description
		 * @param pVectorList pointer to the list
         * @param errorLevel the level error to filter
		 * @return the number of elements in the vector
		 * ************************************************************************************************************
		 */
        int getErrorList(std::vector<std::string> * pVectorList, uint8_t errorLevel = GENERAL_ERROR_LEVEL_ERROR);

		/*! ***********************************************************************************************************
		 * @brief setLed set the Module Led status
		 * @param led led identifier
		 * @param mode mode setting
		 * @return true if ok, false otherwise
		 * ************************************************************************************************************
		 */
		bool setLed(eModuleLedId led, eModuleLedMode mode);

		/*! *************************************************************************************************
		 * @brief DATA SETTERS
		 * **************************************************************************************************
		 */
		void setInternalMinTemperature(uint16_t value);
		void setInternalMaxTemperature(uint16_t value);

		void setFanDuration(uint16_t duration);
		void setFanSettings(eModuleFanId index, uint8_t value);

		/*! *************************************************************************************************
		 * @brief DATA GETTERS
		 * **************************************************************************************************
		 */
		uint16_t getInternalMinTemperature();
		uint16_t getInternalMaxTemperature();

		uint16_t getInternalTemperature();
		uint16_t getNtcValue(eModuleNtcId index);

		uint8_t getFanPower(eModuleFanId index);
		uint16_t getFanRpm(eModuleFanId index);

		float getVoltage();

        /*! ***********************************************************************************************************
         * @brief return the MasterBoard manager status
         * @return true if MasterBoardManager is doing an action
         * ***********************************************************************************************************
         */
        bool isMasterBusy();

	protected:

		/*! ***********************************************************************************************************
		 * @brief workerThread neverending thread normally waiting on the semaphore : when the semaphore it's released
		 *		the thread executes the procedure
		 * @return 0 when thread ends
		 * ***********************************************************************************************************
		 */
		int workerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions before the start of the thread
		 * ***********************************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions after the end of the thread
		 * ***********************************************************************************************************
		 */
		void afterWorkerThread(void);


	private:

		/*! ***********************************************************************************************************
		 * @brief saveCounters to periodically save the instrument counter files
		 * ************************************************************************************************************
		 */
		void saveCounters();

		/*! ***********************************************************************************************************
		 * @brief manageVoltages to read Voltages from PowerManager
		 * ************************************************************************************************************
		 */
		void manageVoltages();

		/*! ***********************************************************************************************************
		 * @brief manageTemperature to read NTCs and set the fan accordingly OR setting the fan
		 * ************************************************************************************************************
		 */
		void manageTemperature();

		/*! ***********************************************************************************************************
		 * @brief clearTemperatureError to clear the temperature error
		 * ************************************************************************************************************
		 */
		bool clearTemperatureError();

		/*! ***********************************************************************************************************
		 * @brief setTemperature acquire NTCs and calculate the "ufficial" value
		 * ************************************************************************************************************
		 */
		void setTemperature();

		/*! ***********************************************************************************************************
		 * @brief setFans set the fan power according to the NTCs or Settings
		 * ************************************************************************************************************
		 */
		void setFans();

		/*! ***********************************************************************************************************
		 * @brief setFansOff switch off all fans
		 * ************************************************************************************************************
		 */
		void setFansOff();

		/*! ***********************************************************************************************************
		 * @brief clear alla variables used for fan, voltage and temperature
		 * ************************************************************************************************************
		 */
		void resetVariables();

		/*! ***********************************************************************************************************
		 * @brief getUpdateInfo return pointer to the last device updated
         * @param pOp pointer to the Operation object
		 * ************************************************************************************************************
		 */
		FwUpdateInfo* getUpdateInfo(Operation* pOp);

		/*! ***********************************************************************************************************
		 * @brief setUpdateOutcome to update the status of the fw update operation
         * @param pInfo
         * @param bRes
         * @return
		 * ************************************************************************************************************
		 */
		int setUpdateOutcome(FwUpdateInfo* pInfo, bool bRes);

        /*! ***********************************************************************************************************
         * @brief setLedsOff to switch off all leds
         * @return true if success, false otherwise
         * ************************************************************************************************************
         */
        bool setLedsOff();

        /*! ***********************************************************************************************************
         * @brief convertNtcToTemperature convert the value read from PowerManager device to temperature
         *      using the Steinhart formula
         * @param fTemp the value acquired from the device
         * @return the equivalent degrees value
         * ************************************************************************************************************
         */
        uint16_t convertNtcToTemperature(float fTemp);

    private:

        int					m_nMasterAction;
        Operation*			m_pOperation;
        Semaphore			m_synchSem;

        std::vector< MasterBoardError * > m_vectorErrors;

        time_t	m_counterSeconds;

        time_t	m_voltageSeconds;
        float m_fVoltageValue;

        uint16_t	m_temperatureFanRecord;

        // temperature regolation parameters
        time_t		m_temperatureSeconds;
        uint16_t m_InternalMin, m_InternalMax;
        uint16_t m_InternalTemperature;
        uint16_t m_ntcValues[eNtcNone];

        // fan management
        std::vector< FanManager * > m_fanVector;
        uint16_t	m_fanDuration;
        bool		m_fanAutoEnable;

};

#endif // MASTERBOARDMANAGER_H
