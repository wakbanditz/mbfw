/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    FanManager.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the MasterBoardManager class.
 @details

 ****************************************************************************
*/

#ifndef FANMANAGER_H
#define FANMANAGER_H

#include <iostream>
#include <stdint.h>
#include <string.h>
#include <vector>

class FanManager
{
    public:

        /*! ***********************************************************************************************************
         * @brief constructor
         * @param name the name of the fan (to be lined to rpm file)
         * @param index the index of the fan (used by i2c link)
         * @param errorCode the error code linked to the fan
         * ************************************************************************************************************
         */
        FanManager(std::string name, uint8_t index, uint16_t errorCode);

        /*! ***********************************************************************************************************
         * @brief reset the power, rpm and setting values
         * ************************************************************************************************************
         */
        void resetValues();

        /*! ***********************************************************************************************************
         * @brief set the power (pwm) value
         * @param power the pwm value (0-100)
         * ************************************************************************************************************
         */
        void setPower(uint8_t power);

        /*! ***********************************************************************************************************
         * @brief set the setting (pwm) value
         * @param setting the pwm value (0-100)
         * ************************************************************************************************************
         */
        void setSetting(uint8_t setting);

        /*! ***********************************************************************************************************
         * @brief retrieve the fan index
         * @return the fan index
         * ************************************************************************************************************
         */
        uint8_t getIndex();

        /*! ***********************************************************************************************************
         * @brief retrieve the fan error code
         * @return the error code
         * ************************************************************************************************************
         */
        uint16_t getErrorCode();

        /*! ***********************************************************************************************************
         * @brief retrieve the fan rpm
         * @return the fan rpm
         * ************************************************************************************************************
         */
        uint16_t getRpm();

        /*! ***********************************************************************************************************
         * @brief retrieve the fan setting
         * @return the fan setting
         * ************************************************************************************************************
         */
        uint8_t getSetting();

        /*! ***********************************************************************************************************
         * @brief retrieve the fan power
         * @return the fan power (pwm)
         * ************************************************************************************************************
         */
        uint8_t getPower();

        /*! ***********************************************************************************************************
         * @brief read from fan controller the fan rpm and compares it to the expected values (according to current power)
         * @return true if fan is correctly working, false otherwise
         * ************************************************************************************************************
         */
        bool readRpm();

        /*! ***********************************************************************************************************
         * @brief find the pwm associated to the temperature according to the table read from configuration file
         * @param temperature the temperature value
         * ************************************************************************************************************
         */
        void setTemperaturePower(uint16_t temperature);

        /*! ***********************************************************************************************************
         * @brief set the current power value to the fan controller device
         * ************************************************************************************************************
         */
        void transferPower();

    private:

        /*! ***********************************************************************************************************
         * @brief read the rpm vs pwm file of the fan
         * @return the number of elements found
         * ************************************************************************************************************
         */
        uint8_t readRpmFile();

        /*! ***********************************************************************************************************
         * @brief readTemperatureFile to read from fanTemperature.cfg the couples temperature vs fan
         * @return the number of elements found
         * ************************************************************************************************************
         */
        uint8_t readTemperatureFile();

    private:

        std::string m_strName;
        uint8_t m_index;

        uint8_t		m_power;
        uint8_t		m_setting;
        uint16_t	m_rpm;
        uint16_t    m_errorCode;

        std::vector<std::pair<uint8_t, uint16_t>> m_tablePwmRpm;
        std::vector<std::pair<uint16_t, uint8_t>> m_tablePwmNtc;
};

#endif // FANMANAGER_H
