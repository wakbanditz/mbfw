#ifndef MASTERBOARDFWUPDATE_H
#define MASTERBOARDFWUPDATE_H

#include "FWUpdateInterface.h"

using namespace  std;


#define FWCORE_DEFAULT_APPLICATION        "FwCore"
#define FWCORE_OLD_DEFAULT_APPLICATION    "OLD_FwCore"

class MasterBoardFwUpdate : public FWUpdateInterface
{
    public:
        /*! *************************************************************************************************
         * @brief constructor
         * **************************************************************************************************
         */
        MasterBoardFwUpdate();

        /*! *************************************************************************************************
         * @brief destructor
         * **************************************************************************************************
         */
        virtual ~MasterBoardFwUpdate();

        /*! *************************************************************************************************
         * @brief CopyFiles
         * **************************************************************************************************
         */
        int CopyFiles(string &sourcepath,string &dstpath,unsigned long len,unsigned long crc32,int checkFile=eNOCheckCRCFile);

        /*! *************************************************************************************************
         * @brief updatefirmware
         * **************************************************************************************************
         */
        int updatefirmware(enumUpdType fwtype, string &source_path, long length, unsigned long crc32);
};

#endif // MASTERBOARDFWUPDATE_H
