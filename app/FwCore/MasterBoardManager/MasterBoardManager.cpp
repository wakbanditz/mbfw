/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    MasterBoardManager.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the MasterBoardManager class.
 @details

 ****************************************************************************
*/
#include <sys/types.h>
#include <sys/socket.h>

#include <math.h>

#include "MasterBoardManager.h"
#include "ErrorUtilities.h"
#include "MainExecutor.h"
#include "Operation.h"
#include "FanManager.h"
#include "NetworkIPC.h"
#include "SectionBoardGeneral.h"

#include "ADT7470.h"
#include "UCD9090a.h"
#include "PCA9955B.h"


#define SEM_MASTERBOARD_ACTION_SYNCH_NAME		"/sem-MasterBoard-action-synch"
#define SEM_MASTERBOARD_ACTION_INIT_VALUE		0

#define MASTER_DEVICE_NAME		"Instrument"

#define COUNTERS_SAVING_INTERVAL	60	// every 60 secs save the counter file
#define TEMPERATURE_INTERVAL		10	// every 10 secs check temperature
#define TEMPERATURE_RECORD_COUNTER	((60 * 60) / TEMPERATURE_INTERVAL) // every 1 hour record the fan+temperature
#define VOLTAGES_INTERVAL			(3 * 60 * 60)	// every 3 hour check voltages
#define FIRST_VOLTAGE_INTERVAL		60		// first voltage registration after 1 min


// NOTE : this thread is always running (it manages the instrument temperature)
// so it is not linked to a semaphore (defined only for homogeneity)

MasterBoardManager::MasterBoardManager()
{
	m_nMasterAction = eMasterActionNone;
	m_pOperation = 0;
	m_vectorErrors.clear();

	resetVariables();

	m_InternalMax = m_InternalMin = 0;

}

MasterBoardManager::~MasterBoardManager()
{
	if ( isRunning() )
	{
		stopThread();
	}

	msleep(500);

	if ( isStopped() )
	{
		log(LOG_INFO, "MasterBoardManager thread closed");
	}

	int i;

    MasterBoardError * pError;
	for(i = m_vectorErrors.size() -1; i >= 0; i--)
	{
		pError = m_vectorErrors.at(i);
        SAFE_DELETE(pError);
	}

    FanManager * pFanManager;
    for(i = m_fanVector.size() -1; i >= 0; i--)
    {
        pFanManager = m_fanVector.at(i);
        SAFE_DELETE(pFanManager);
    }
}

void MasterBoardManager::resetVariables()
{
	m_counterSeconds = getSecondsTime();
	m_voltageSeconds = getSecondsTime();
	m_temperatureSeconds = getSecondsTime();

	m_fVoltageValue = 0;

	m_temperatureFanRecord = 0;
	m_InternalTemperature = 0;
	for(uint8_t i = 0; i < eNtcNone; i++)	m_ntcValues[i] = 0;

    for(uint8_t i = 0; i < m_fanVector.size(); i++)
    {
        m_fanVector.at(i)->resetValues();
    }

	m_fanDuration = 0;
	m_fanAutoEnable = true;
}

void MasterBoardManager::initMasterBoardManager()
{

//	m_synchSem.create(SEM_MASTERBOARD_ACTION_SYNCH_NAME, SEM_MASTERBOARD_ACTION_INIT_VALUE);
//	m_synchSem.enableErrorPrint(true);

    // create a FanManager per each fan present in the instrument
    FanManager * pFanManager;

    pFanManager = new FanManager(FAN_NAME_SUPPLY, eFanSupply, ERR_SUPPLY_FAN_FAIL);
    m_fanVector.push_back(pFanManager);
    pFanManager = new FanManager(FAN_NAME_SECTIONA, eFanSectionA, ERR_SECTIONA_FAN_FAIL);
    m_fanVector.push_back(pFanManager);
    pFanManager = new FanManager(FAN_NAME_SECTIONB, eFanSectionB, ERR_SECTIONB_FAN_FAIL);
    m_fanVector.push_back(pFanManager);

    setFansOff();

    setLed(eModuleLedGreen, eModuleLedSteady);

	// load the error vectors
	m_vectorErrors.clear();

	// from ErrorUtilities
	parseErrorFile(MASTER_DEVICE_NAME, (std::vector< GeneralError * > *)&m_vectorErrors, eErrorMaster);

	// init action
	setMasterAction(eMasterActionInit);

	if ( !isRunning() )
	{
		startThread();
	}

}

void MasterBoardManager::stopMasterBoardManager()
{
    setLed(eModuleLedGreen, eModuleLedBlink);
    stopThread();
}


int MasterBoardManager::setMasterAction(int action, Operation* pOp)
{
	log(LOG_INFO, "MasterBoardManager::setAction: %d", action);

	if ( action == eMasterActionNone )
	{
		// no action to do
		log(LOG_INFO, "MasterBoardManager::setAction: no action");
		return eOperationWaitForDeath;
	}

	m_pOperation = pOp;
	m_nMasterAction = action;

/*
	int liRetVal = m_synchSem.release();
	if ( liRetVal == SEM_FAILURE )
	{
		log(LOG_ERR, "MasterBoardManager::setNSHAction: unable to unlock thread synch semaphore");
		return eOperationError;
	}
	else
	{
		log(LOG_INFO, "MasterBoardManager::setNSHAction: operation <%d>", m_nNSHAction);
	}
*/
	return eOperationRunning;
}

void MasterBoardManager::beforeWorkerThread()
{
    log(LOG_DEBUG, "MasterBoardManager::started");
}

void MasterBoardManager::afterWorkerThread()
{
	m_synchSem.closeAndUnlink();
    log(LOG_DEBUG, "MasterBoardManager::stopped");
}

int MasterBoardManager::workerThread()
{
	int8_t cResult;

	while ( isRunning() )
	{
		/*
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "NSHBoardManager::th: unlocked but wait returned %d", liSynchRet);
			continue;
		}

		if ( m_nMasterAction == eNSHActionNone )
		{
			log(LOG_ERR, "NSHBoardManager::th: BAD unlocked without action");
			continue;
		}
		*/

		if  ( m_nMasterAction == eMasterActionInit )
		{
			log(LOG_INFO, "MasterBoardManager::thread Init");
			clearAllErrors();
			resetVariables();
			// set Module Led Blinking Green
            setLed(eModuleLedGreen, eModuleLedBlink);
			// change the action to wait for completion
			m_nMasterAction = eMasterActionWaitInit;
		}
		else if  ( m_nMasterAction == eMasterActionWaitInit )
		{
			// when state differs from Init -> completed
			if(stateMachineSingleton()->getDeviceStatus(eIdModule) != eStatusInit)
			{
                // set Module Led Steady Green
                setLed(eModuleLedGreen, eModuleLedSteady);
                log(LOG_INFO, "MasterBoardManager::INIT completed");
				registerGenericEvent("Initialization completed");
                m_nMasterAction = eMasterActionNone;
                m_pOperation = nullptr;
            }
		}
		else if  ( m_nMasterAction == eMasterActionLed )
		{
			log(LOG_INFO, "MasterBoardManager::thread LED");

			// set the Module led according to the devices status
            if(stateMachineSingleton()->getDeviceStatus(eIdModule) == eStatusInit)
            {
                // instrument is initializing -> led green blinking
                setLed(eModuleLedGreen, eModuleLedBlink);
            }
            else if(stateMachineSingleton()->getDeviceStatus(eIdNsh) == eStatusError)
			{
				// module not usable: set Module Led Blinking Red
				setLed(eModuleLedRed, eModuleLedBlink);
                // ... and also the 2 sections (if not disabled)
                if (stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionA))
                {
                    sectionBoardGeneralSingleton(SCT_A_ID)->setLed(eLedRed, eLedOn);
                }
                if (stateMachineSingleton()->isDeviceAbleToAcceptCommands(eIdSectionB))
                {
                    sectionBoardGeneralSingleton(SCT_B_ID)->setLed(eLedRed, eLedOn);
                }
            }
			else if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusError)
			{
				// if one section is in error and the other one is in error too or disabled -> the module is not usable
				if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusError ||
					stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusDisable)
				{
					// module not usable: set Module Led Blinking Red
					setLed(eModuleLedRed, eModuleLedBlink);
				}
				else
				{
					// module usable: set Module Led Blinking Orange
					setLed(eModuleLedOrange, eModuleLedBlink);
				}
			}
            else if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusUnload ||
                    stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusUnload)
            {

                if(protocolThreadPointer()->isSectionUnloadInError(SCT_A_ID) &&
                    protocolThreadPointer()->isSectionUnloadInError(SCT_B_ID) )
                {
                    // module not usable (both section are out of order): set Module Led Blinking Red
                    setLed(eModuleLedRed, eModuleLedBlink);
                }
                // the section has completed a protocol: if it is in error -> Orange led blinking
                else if(protocolThreadPointer()->isSectionUnloadInError(SCT_A_ID) ||
                        protocolThreadPointer()->isSectionUnloadInError(SCT_B_ID) )
                {
                    // module usable (one section is out of order): set Module Led Blinking Orange
                    setLed(eModuleLedOrange, eModuleLedBlink);
                }
                else
                {
                    // module usable ok: set Module Led Steady Green
                    setLed(eModuleLedGreen, eModuleLedSteady);
                }
            }
            else if(stateMachineSingleton()->getDeviceStatus(eIdSectionB) == eStatusError)
			{
				// if one section is in error and the other one is in error too or disabled -> the module is not usable
				if(stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusError ||
					stateMachineSingleton()->getDeviceStatus(eIdSectionA) == eStatusDisable)
				{
					// module not usable: set Module Led Blinking Red
					setLed(eModuleLedRed, eModuleLedBlink);
				}
				else
				{
					// module usable: set Module Led Blinking Orange
					setLed(eModuleLedOrange, eModuleLedBlink);
				}
			}
			else if(stateMachineSingleton()->getDeviceStatus(eIdCamera) == eStatusError)
			{
				// module is usable (without camera): set Module Led Blinking Orange
				setLed(eModuleLedOrange, eModuleLedBlink);
			}
            else
            {
                // default status: green led steady
                setLed(eModuleLedGreen, eModuleLedSteady);
            }

			m_nMasterAction = eMasterActionNone;
            m_pOperation = nullptr;
		}
		else if ( m_nMasterAction == eMasterActionExecute )
		{
			log(LOG_INFO, "MasterBoardManager::thread Execute");

			//executes the actions related to the incoming command
			 while(m_pOperation->executeCmd(INSTRUMENT_ID) == 0) {};

			m_nMasterAction = eMasterActionNone;
            m_pOperation = nullptr;
		}
		else if ( m_nMasterAction == eMasterActionCmd )
		{
			//executes the actions related to the incoming command
			m_pOperation->executeCmd(INSTRUMENT_ID);

			//compile and send the answer
			cResult = m_pOperation->compileSendOutCmd();

			/* ********************************************************************************************
			* Remove the operation from the OperationManager list
			* ********************************************************************************************
			*/
			if ( cResult == eOperationEnabledForDeath )
			{
				/* ********************************************************************************************
				 * Access the state machine
				 * ********************************************************************************************
				 */
				m_pOperation->restoreStateMachine();

				/* ********************************************************************************************
				 * Reset variables and remove the common operation
				 * ********************************************************************************************
				 */
				m_nMasterAction = eMasterActionNone;
				operationSingleton()->removeOperation(m_pOperation);
				m_pOperation = 0;

				log(LOG_INFO, "MasterBoardManager::workerThread-->eOperationEnabledForDeath");
			}
			else
			{
				m_nMasterAction = eMasterActionNone;
                m_pOperation = nullptr;

				log(LOG_INFO, "MasterBoardManager::thread-->without operation removal");
			}
		}
		else if ( m_nMasterAction  == eMasterActionFwUpdate )
		{
			log(LOG_INFO, "MasterBoardManager::workerThread-->wait FW UPD on master");

			FwUpdateInfo* pInfo;
			pInfo = getUpdateInfo(m_pOperation);
			if ( pInfo != nullptr )
			{
                // Implemented only applicative upd for the moment
				if ( pInfo->m_eFwType == eApplicative )
				{
					m_pLogger->log(LOG_INFO, "MasterBoardManager::workerThread: trying to launch bash for upd");
					string strFunc("chmod +x /home/root/updateAppl.sh &");
					int liRes = system(strFunc.c_str());
					if ( liRes != -1 )
					{
                        m_pLogger->log(LOG_INFO, "MasterBoardManager::workerThread: launch update script");
                        strFunc = "/home/root/updateAppl.sh &";
						liRes = system(strFunc.c_str());
					}
					setUpdateOutcome(pInfo, (liRes != -1));
				}
                else if ( pInfo->m_eFwType == eKernel )
                {
                    m_pLogger->log(LOG_INFO, "MasterBoardManager::workerThread: trying to launch bash for upd");
                    string strFunc("update.sh update-image-biomerieux.swu &");
                    int liRes = system(strFunc.c_str());
                    if ( liRes != -1 )
                    {

                        NetworkIPC  netIPC;
                        progress_msg    progMsg;

                        int fd = netIPC.connectProgressIPCWrap(true);

                        while (progMsg.status < SUCCESS)
                        {
                            msleep(1000);
                            netIPC.receiveProgressIPC(&fd, &progMsg);

                            m_pLogger->log(LOG_INFO, "MasterBoardManager::workerThread: SOupdate status %i",  progMsg.status);
                        }

                        m_pLogger->log(LOG_INFO, "MasterBoardManager::workerThread: SOupdate performed");
                    }
                    setUpdateOutcome(pInfo, (liRes != -1));
                }
			}

			m_nMasterAction = eMasterActionNone;
			m_pOperation = nullptr;
		}

		// standard default actions of the thread

		// if the status is sleep or disable -> switch off Module Led and Fan
		if(stateMachineSingleton()->getDeviceStatus(eIdModule) == eStatusDisable ||
			stateMachineSingleton()->getDeviceStatus(eIdModule) == eStatusSleep)
		{
            setLedsOff();
			setFansOff();
		}
		else
		{
			// Instrument NTC acquisition and FAN management
			manageTemperature();
			manageVoltages();
			saveCounters();
		}

		sleep(1);
	}

	return 0;
}

void MasterBoardManager::clearAllErrors()
{
	size_t i;
	MasterBoardError * pError;

	for(i = 0; i < m_vectorErrors.size(); i++)
	{
		pError = m_vectorErrors.at(i);
		pError->setActive(false);
	}
}

std::string MasterBoardManager::setErrorFromCode(uint16_t code, bool bKeepSet)
{
	std::string strError;
	size_t i;

	strError.clear();

	for(i = 0; i < m_vectorErrors.size(); i++)
	{
		// check if the error is activated
		MasterBoardError * pError = m_vectorErrors.at(i);

		if(pError->getFlag() == std::to_string(code))
		{
			// set the error as active so that the time is set
			pError->setActive(true, true);

			// the string is formatted as timestamp#code#category#description
			strError.assign(pError->getStrTime());
			strError.append(HASHTAG_SEPARATOR);
			strError.append(pError->getCode());
			strError.append(HASHTAG_SEPARATOR);
			strError.append(pError->getCategory());
			strError.append(HASHTAG_SEPARATOR);
			strError.append(pError->getDescription());
			strError.append(HASHTAG_SEPARATOR);
			strError.append(MASTER_DEVICE_NAME);

			// always register the error event
			registerErrorEvent(INSTRUMENT_ID, MASTER_DEVICE_NAME, pError->getCode(), pError->getLevel(),
							   pError->getDescription(), pError->getEnabled(), pError->getActive());

			// and then reset it if just temporary
			if(bKeepSet == false)
			{
				pError->setActive(false);
			}

			break;
		}
	}
	return strError;
}

bool MasterBoardManager::isErrorFromCodeActive(uint16_t code)
{
    bool bActive = false;
    size_t i;

    for(i = 0; i < m_vectorErrors.size(); i++)
    {
        // check if the error is activated
        MasterBoardError * pError = m_vectorErrors.at(i);

        if(pError->getFlag() == std::to_string(code))
        {
            bActive = pError->getActive();
            break;
        }
    }
    return bActive;
}

int MasterBoardManager::getErrorList(std::vector<std::string> * pVectorList, uint8_t errorLevel)
{
	// NOTE don't clear the vector but add elements !
	std::string strError;
	size_t i;

	for(i = 0; i < m_vectorErrors.size(); i++)
	{
		// check if the error is activated
		MasterBoardError * pMasterError = m_vectorErrors.at(i);

		if(pMasterError->getActive() == true &&
			pMasterError->getStrTime().empty() == false)
		{
            if(errorLevel == GENERAL_ERROR_LEVEL_NONE ||
                    pMasterError->getLevel() == errorLevel)
            {
                // the string is formatted as timestamp#code#category#description
                strError.assign(pMasterError->getStrTime());
                strError.append(HASHTAG_SEPARATOR);
                strError.append(pMasterError->getCode());
                strError.append(HASHTAG_SEPARATOR);
                strError.append(pMasterError->getCategory());
                strError.append(HASHTAG_SEPARATOR);
                strError.append(pMasterError->getDescription());

                pVectorList->push_back(strError);
            }
		}
	}
	return pVectorList->size();
}

bool MasterBoardManager::clearTemperatureError()
{
	bool bErrorCleared = false;

	for(size_t i = 0; i < m_vectorErrors.size(); i++)
	{
		// check if the message sent from the board correspond to one stored in the list
		MasterBoardError * pMasterError = m_vectorErrors.at(i);

		if(pMasterError->getActive() == true &&
			pMasterError->getRestoreEnabled() )
		{
			// the error is related to temperature and it's active -> clear it
			m_pLogger->log(LOG_INFO, "MasterBoard clear %s", pMasterError->getDescription().c_str());

			pMasterError->setActive(false);
			bErrorCleared = true;
		}
	}
	return bErrorCleared;
}

void MasterBoardManager::saveCounters()
{
	time_t tmpSeconds = getSecondsTime();

	if((tmpSeconds - m_counterSeconds) > COUNTERS_SAVING_INTERVAL)
	{
		infoSingleton()->saveCountersToFile();

		log(LOG_INFO, "MasterBoardManager::saveCounters");

		m_counterSeconds = tmpSeconds;
	}
}

void MasterBoardManager::manageVoltages()
{
	// check voltages periodically
	time_t tmpSeconds = getSecondsTime();

	if( (m_voltageSeconds == 0 && (tmpSeconds > FIRST_VOLTAGE_INTERVAL)) ||
		((tmpSeconds - m_voltageSeconds) > VOLTAGES_INTERVAL) )
	{
		if(MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager)
		{
			MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readVout(m_fVoltageValue);
		}

		// record the current voltages
		registerVoltages();

		m_voltageSeconds = tmpSeconds;
	}
}

void MasterBoardManager::manageTemperature()
{
#ifdef __DEBUG_PROTOCOL
         return;
#endif

	// check periodically
	time_t tmpSeconds = getSecondsTime();

	if((tmpSeconds - m_temperatureSeconds) > TEMPERATURE_INTERVAL)
	{
		setTemperature();

		setFans();

		// record the current temperatures + fans
		if((m_temperatureFanRecord == 0) ||
			(m_temperatureFanRecord > TEMPERATURE_RECORD_COUNTER) )
		{
			m_temperatureFanRecord = 0;
			registerTemperatures();
			registerFans();
		}
		m_temperatureFanRecord++;

		m_temperatureSeconds = tmpSeconds;
	}
}

void MasterBoardManager::setTemperature()
{
#ifdef __DEBUG_PROTOCOL
         return;
#endif

	if(MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager)
	{
        float fTemp;

        // temperature of the PowerManager device
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readTemperature(0, 1, fTemp);
        m_ntcValues[eNtcPowerManager] = (uint16_t)(fTemp * 100);
		m_pLogger->log(LOG_DEBUG_PARANOIC, "MasterBoard temperature Device %d", m_ntcValues[eNtcPowerManager]);

        // temperature of the NTC located in the MasterBoard box
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readTemperature(1, 2, fTemp);
        m_ntcValues[eNtcSupply] = convertNtcToTemperature(fTemp);
		m_pLogger->log(LOG_DEBUG_PARANOIC, "MasterBoard temperature Box %d", m_ntcValues[eNtcSupply]);

        // temperature of the NTC located inside the instrument (over the SS)
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readTemperature(2, 2, fTemp);
        m_ntcValues[eNtcInstrument] = convertNtcToTemperature(fTemp);
		m_pLogger->log(LOG_DEBUG_PARANOIC, "MasterBoard temperature Instrument %d", m_ntcValues[eNtcInstrument]);

        // temperature of the NTC not connected
        MainExecutor::getInstance()->m_i2cBoard.m_pPowerManager->readTemperature(3, 2, fTemp);
        m_ntcValues[eNtcSpare] = convertNtcToTemperature(fTemp);
		m_pLogger->log(LOG_DEBUG_PARANOIC, "MasterBoard temperature Spare %d", m_ntcValues[eNtcSpare]);
    }

    // calculate the "internal" temperature : by now it's the instrument ntc
    m_InternalTemperature = m_ntcValues[eNtcInstrument];

    if(m_InternalTemperature == 0)
    {
        setErrorFromCode(ERR_TEMPERATURE_NOT_AVAILABLE, true);
        log(LOG_ERR, "MasterBoardManager Instrument Temperature FAIL %d", m_InternalTemperature);
        // send VidasEp to SW
        requestVidasEp();
    }
    else if((m_InternalMin > 0) && (m_InternalTemperature < m_InternalMin))
	{
		setErrorFromCode(ERR_TEMPERATURE_LOW, true);
        log(LOG_ERR, "MasterBoardManager Instrument Temperature LOW %d", m_InternalTemperature);
        // send VidasEp to SW
		requestVidasEp();
	}
	else if((m_InternalMax > 0) && (m_InternalTemperature > m_InternalMax))
	{
		setErrorFromCode(ERR_TEMPERATURE_HIGH, true);
        log(LOG_ERR, "MasterBoardManager Instrument Temperature HIGH %d", m_InternalTemperature);
        // send VidasEp to SW
		requestVidasEp();
	}
	else
	{
		// temperature in range -> reset error if present
		if(clearTemperatureError())
		{
			// send VidasEp to SW
			requestVidasEp();
		}
	}
}

uint16_t MasterBoardManager::convertNtcToTemperature(float fTemp)
{
    const float maxVoltage = 24000.0;
    const float ntcParamA = 0.001129241;
    const float ntcParamB = 0.0002341077;
    static float ntcParamC = 0.00000008775468;


    // convert voltage to resistor
    float fNtcValue = (((maxVoltage * 2.048) / (fTemp / 100)) - maxVoltage);

    m_pLogger->log(LOG_DEBUG_PARANOIC, "convertNtcToTemperature %f -> %f", fTemp, fNtcValue);

    float fLogNtcValue = logf(fNtcValue);

    //temp = ParamA  + ParamB * fLogNtcValue + ParamC * pow(fNtcValue, 3);
    fNtcValue = 1.0;
    fNtcValue *= ntcParamC;
    fNtcValue *= fLogNtcValue;
    fNtcValue *= fLogNtcValue;
    fNtcValue *= fLogNtcValue;
    fNtcValue += (ntcParamB * fLogNtcValue);
    fNtcValue += ntcParamA;

    if (fNtcValue != 0.0)
    {
        fNtcValue = (1.0 / fNtcValue);
        fNtcValue -= 273.15; 																	// convert tfrom °K to °C
    }

    // return value is expressed in 0.01 degrees
    return (uint16_t)(fNtcValue * 100);


}

void MasterBoardManager::setFans()
{
#ifdef __DEBUG_PROTOCOL
         return;
#endif

    // acquire fan RPM and check the values
     for(uint8_t i = 0; i < m_fanVector.size(); i++)
     {
         if(m_fanVector.at(i)->readRpm() == false)
         {
             //  set the fan error
             setErrorFromCode(m_fanVector.at(i)->getErrorCode(), true);
             log(LOG_ERR, "MasterBoardManager Fan %d Error", i);
         }
		 m_pLogger->log(LOG_DEBUG_PARANOIC, "MasterBoard fan %d rpm %d", i, m_fanVector.at(i)->getRpm());
     }

	if(m_fanAutoEnable == true)
	{
        // set Fans according to regolation
        for(uint8_t i = 0; i < m_fanVector.size(); i++)
        {
            if(m_fanVector.at(i)->getIndex() == eFanSupply)
            {
                m_fanVector.at(i)->setTemperaturePower(m_ntcValues[eNtcSupply]);
            }
            else
            {
                m_fanVector.at(i)->setTemperaturePower(m_ntcValues[eNtcInstrument]);
            }
        }
	}
	else
	{
		// set Fans according to the Settings
        for(uint8_t i = 0; i < m_fanVector.size(); i++)
        {
            m_fanVector.at(i)->setPower(m_fanVector.at(i)->getSetting());
        }

		// is setting time expired ?
		if(m_fanDuration < TEMPERATURE_INTERVAL)
        {
			m_fanDuration = 0;
        }
		else
		{
			m_fanDuration -= TEMPERATURE_INTERVAL;
		}

		if(m_fanDuration == 0)
		{
			// re-enable the automatic loop
			m_fanAutoEnable = true;
		}
	}

    // transfer values to Fan controller
    for(uint8_t i = 0; i < m_fanVector.size(); i++)
    {
        m_fanVector.at(i)->transferPower();
		m_pLogger->log(LOG_DEBUG_PARANOIC, "MasterBoard fan %d pwm %d", i, m_fanVector.at(i)->getPower());
    }
}

void MasterBoardManager::setFansOff()
{
    for(uint8_t i = 0; i < m_fanVector.size(); i++)
    {
        m_fanVector.at(i)->setPower(0);
    }
}

bool MasterBoardManager::setLedsOff()
{
    if(MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver == 0)
    {
        log(LOG_INFO, "MasterBoardManager::setLed--> LED DRIVER not enabled");
        return false;
    }

    MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_GREEN, 0);
    MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_ORANGE, 0);
    MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_RED, 0);

    return true;
}

bool MasterBoardManager::setLed(eModuleLedId led, eModuleLedMode mode)
{
 #ifdef __DEBUG_PROTOCOL
		  return false;
 #endif

    if(MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver == 0)
    {
        log(LOG_INFO, "MasterBoardManager::setLed--> LED DRIVER not enabled");
		return false;
	}

    if(isErrorFromCodeActive(ERR_MODULE_LED))
    {
        // a LED error is set -> all leds are off
        log(LOG_INFO, "MasterBoardManager::setLed--> LED in Error");
        return true;
    }


    // first switch all leds off then execute the command
    int iError = 0;

    if(setLedsOff() == false)
    {
        log(LOG_DEBUG, "MasterBoardManager::setLedOff--> failed");
        return false;
    }

	switch (led)
	{
		case eModuleLedGreen:
		{
			if (mode == eModuleLedBlink)
			{
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_BLNK_CTL);
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_GREEN, 3);
			}
			else if (mode == eModuleLedSteady)
			{
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_DIM_CTL);
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_GREEN, 3);
			}
			else
			{
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_GREEN, 0);
			}
		}
		break;

        case eModuleLedRed:
        {
            if (mode == eModuleLedBlink)
            {
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_BLNK_CTL);
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_RED, 3);
            }
            else if (mode == eModuleLedSteady)
            {
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_DIM_CTL);
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_RED, 3);
            }
            else
            {
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_RED, 0);
            }
        }
        break;

        case eModuleLedOrange:
        {
            if (mode == eModuleLedBlink)
            {
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_BLNK_CTL);
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_ORANGE, 3);
            }
            else if (mode == eModuleLedSteady)
            {
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->config(PCALED_MODE2_DIM_CTL);
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_ORANGE, 3);
            }
            else
            {
                iError += MainExecutor::getInstance()->m_i2cBoard.m_pLedDriver->writeLedOut(OUTPUT_LED_ORANGE, 0);
            }
        }
        break;

        default:
		break;
	}

    if(iError < 0)
    {
        // an error has occurred while setting the leds
        setErrorFromCode(ERR_MODULE_LED, true);
        // -> switch off all leds
        setLedsOff();
    }

    return true;
}

void MasterBoardManager::setInternalMinTemperature(uint16_t value)
{
	m_InternalMin = value;
}

void MasterBoardManager::setInternalMaxTemperature(uint16_t value)
{
	m_InternalMax = value;
}

void MasterBoardManager::setFanDuration(uint16_t duration)
{
	m_fanDuration = duration;
	// disable or enable the regolation loop according to this value
	if(m_fanDuration > 0)
	{
		m_fanAutoEnable = false;
	}
	else
	{
        m_fanAutoEnable = true;
	}
	m_pLogger->log(LOG_DEBUG_PARANOIC, "MasterBoard fan Duration %d", m_fanDuration);
}

void MasterBoardManager::setFanSettings(eModuleFanId index, uint8_t value)
{
    for(uint8_t i = 0; i < m_fanVector.size(); i++)
    {
        if(m_fanVector.at(i)->getIndex() == index)
        {
            m_fanVector.at(i)->setSetting(value);
			m_pLogger->log(LOG_DEBUG_PARANOIC, "MasterBoard fan Setting %d %d", index, value);
        }
    }
}

uint16_t MasterBoardManager::getInternalMinTemperature()
{
	return m_InternalMin;
}

uint16_t MasterBoardManager::getInternalMaxTemperature()
{
	return m_InternalMax;
}

uint16_t MasterBoardManager::getInternalTemperature()
{
	return m_InternalTemperature;
}

uint16_t MasterBoardManager::getNtcValue(eModuleNtcId index)
{
	if(index >= eNtcNone) return 0;

	return m_ntcValues[index];
}

uint8_t MasterBoardManager::getFanPower(eModuleFanId index)
{
    uint8_t pwm = 0;
    for(uint8_t i = 0; i < m_fanVector.size(); i++)
    {
        if(m_fanVector.at(i)->getIndex() == index)
        {
            pwm = m_fanVector.at(i)->getPower();
            break;
        }
    }
    return	pwm;
}

uint16_t MasterBoardManager::getFanRpm(eModuleFanId index)
{
    uint16_t rpm = 0;
    for(uint8_t i = 0; i < m_fanVector.size(); i++)
    {
        if(m_fanVector.at(i)->getIndex() == index)
        {
            rpm = m_fanVector.at(i)->getRpm();
            break;
        }
    }
    return	rpm;
}

float MasterBoardManager::getVoltage()
{
	return m_fVoltageValue;
}

bool MasterBoardManager::isMasterBusy()
{
	if ( m_pOperation == 0 )
	{
		return false;
	}
	return true;
}

FwUpdateInfo* MasterBoardManager::getUpdateInfo(Operation* pOp)
{
	if ( pOp == nullptr )	return nullptr;

	OpFwUpdate* pUpdOp = (OpFwUpdate*)pOp;
	return pUpdOp->getLastDeviceInfoForUpd();
}

int MasterBoardManager::setUpdateOutcome(FwUpdateInfo* pInfo, bool bRes)
{
	if ( pInfo == nullptr )	return -1;

	OpFwUpdate* pUpdOp = (OpFwUpdate*)m_pOperation;
	return pUpdOp->writeUpdOutcomeLog(pInfo->m_eId, pInfo->m_eFwType, bRes);
}

