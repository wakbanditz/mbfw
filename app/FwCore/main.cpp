/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    main.cpp
 @author  BmxIta FW dept
 @brief   Contains the main function.
 @details

 ****************************************************************************
*/

#include <signal.h>
#include "Log.h"
#include "MainExecutor.h"
#include "CommonInclude.h"

void error(char* sMsg)
{
	perror(sMsg);
	exit(1);
}

static bool bKeepRunning = true;

void signalHandler(int nSignal)
{
	static int sig_counter = 1;
	printf("Received signal %d\n", nSignal);
	switch ( nSignal )
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
			bKeepRunning = false;
			if ( ++sig_counter > 5 )
			{
				// Fifth time that the soft mode has failed brute exits
				printf("Brute force exit ...");
				exit(1);
			}
		break;
		case SIGTSTP:
			bKeepRunning = false;
		break;
	}
}

int main(void)
{
	signal(SIGINT, signalHandler);
	signal(SIGKILL, signalHandler);
	signal(SIGTERM, signalHandler);
	signal(SIGTSTP, signalHandler);

	/* ***********************************************************************************************************
	 * INITIALIZATION STAGE
	 * ***********************************************************************************************************
	 */
	bool bRes;
	int startError = 0;

	bRes = MainExecutor::getInstance()->initConfigurator();
	if ( bRes == false )
	{
		printf("Main unable to init Configurator\n");
		exit(1);
	}

	bRes = MainExecutor::getInstance()->initLogger();
	if ( bRes == false )
	{
		printf("Main unable to init Logger\n");
		exit(1);
	}
	Log* plog = MainExecutor::getInstance()->getLogger();
	plog->log(LOG_INFO, "Starting << %s >>", PROGRAM_NAME);

	int liRes = MainExecutor::getInstance()->setRootPolicy();
	if ( liRes == -1 )
	{
		plog->log(LOG_ERR, "Main unable to set root policy to 755");
	}

	bRes = MainExecutor::getInstance()->initEventRecorder();
	if ( bRes == false )
	{
		printf("Main unable to init EventRecorder\n");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initInstrumentInfo();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to init InstrumentInfo");
		startError++;
	}

//#ifndef __DEBUG_PROTOCOL
    bRes = MainExecutor::getInstance()->initGpioInterface();
    if ( bRes == false )
    {
        plog->log(LOG_ERR, "Main unable to initialize Gpio interface");
        startError++;
    }

    bRes = MainExecutor::getInstance()->initUSBPeripherals();
    if ( bRes == false )
    {
        plog->log(LOG_ERR, "Main unable to USB peripherals");
        startError++;
    }

    bRes = MainExecutor::getInstance()->initI2CPeripherals();
    if ( bRes == false )
    {
        plog->log(LOG_ERR, "Main unable to initialize I2C peripherals");
        startError++;
    }

    bRes = MainExecutor::getInstance()->initSPIPeripherals();
    if ( bRes == false )
    {
        plog->log(LOG_ERR, "Main unable to initialize SPI peripherals");
        startError++;
    }
//#endif

	bRes = MainExecutor::getInstance()->initTimeValues();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to init Time values");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initStateMachine();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to init StateMachine");
		startError++;
	}

	bRes = MainExecutor::getInstance()->restoreDisableState();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to restore Disabled state");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initIPCConfig();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to configure IPC settings");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initLoginManager();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to init Login");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initScheduler();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to init Scheduler");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initWebServer();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to init Web Server");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initSectionBoards();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to init Sections");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initSectionBoardManagers();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to init Sections Managers");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initNSHBoardManager();
	if ( bRes == false )
	{
        plog->log(LOG_ERR, "Main unable to init NSH Manager");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initCameraBoardManager();
	if ( bRes == false )
	{
        plog->log(LOG_ERR, "Main unable to init Camera Manager");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initMasterBoardManager();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to init Master Manager");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initOpManagerSrvCollector();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to start OperationManager");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initProtocolThread();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to start ProtocolThread");
		startError++;
	}

	bRes = MainExecutor::getInstance()->initTestManager();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to start TestManager");
	}

	bRes = MainExecutor::getInstance()->setTimeValues();
	if ( bRes == false )
	{
		plog->log(LOG_ERR, "Main unable to set Time Factor");
	}

#ifndef __DEBUG_PROTOCOL
    bRes = MainExecutor::getInstance()->initPowerOffManager();
    if ( bRes == false )
    {
        plog->log(LOG_ERR, "Main unable to startup PowerOff Manager");
    }
#endif

	// check last shutdown (after MasterBoardManager is initialized)
	MainExecutor::getInstance()->readLastShutdownInstrument();

	// start main loop if no errors are detected at startup
	if(startError == 0)
	{

#ifndef __DEBUG_PROTOCOL
        // Set startup instrument
        MainExecutor::getInstance()->firstStartupInstrument();
#endif

		// wait for all tasks and threads to be started ...
		sleep(5);

		/*
		 * ***************************************************************************************************************
		 * MAIN CYCLE
		 * ****************************************************************************************************************
		 */
		plog->log(LOG_INFO, "Main: start main cycle");

		while ( bKeepRunning )
		{
            usleep(100);
			if ( !MainExecutor::getInstance()->checkTestManager() )
			{
                plog->log(LOG_INFO, "Main: TestManager -> end main cycle");
                bKeepRunning = false;
			}
			else if(MainExecutor::getInstance()->getShutdown() == true)
			{
                plog->log(LOG_INFO, "Main: Shutdown -> end main cycle");
                bKeepRunning = false;
			}
            else
            {

            }
		}
	}

	/* ****************************************************************************************************************
	 * CLOSING STAGE
	 * ****************************************************************************************************************
	 */
	MainExecutor::getInstance()->stopTestManager();
	MainExecutor::getInstance()->stopProtocolThread();
	MainExecutor::getInstance()->closeScheduler();
	MainExecutor::getInstance()->stopOpManagerSrvCollector();
	MainExecutor::getInstance()->stopMasterBoardManager();
	MainExecutor::getInstance()->stopSPIPeripherals();
	MainExecutor::getInstance()->stopI2CPeripherals();
	MainExecutor::getInstance()->stopUSBPeripherals();
	MainExecutor::getInstance()->stopSectionBoardManagers();
	MainExecutor::getInstance()->stopNSHBoardManager();
	MainExecutor::getInstance()->closeGpioInterface();
	MainExecutor::getInstance()->stopPowerOffManager();
	MainExecutor::getInstance()->stopSectionBoards();
	MainExecutor::getInstance()->stopWebServer();
	MainExecutor::getInstance()->stopEventRecorder();
	MainExecutor::getInstance()->stopInstrumentInfo();

	plog->log(LOG_INFO, "Main: stop main cycle. Exit program");

	if(MainExecutor::getInstance()->getShutdown() == true)
	{
        MainExecutor::getInstance()->m_powerOffManager.shutdownBoard();
	}
	return 0;
}
