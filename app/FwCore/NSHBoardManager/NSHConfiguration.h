/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHConfiguration.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the NSHConfiguration class.
 @details

 ****************************************************************************
*/

#ifndef NSHCONFIGURATION_H
#define NSHCONFIGURATION_H

#include <vector>
#include <string>
#include "CommonInclude.h"

using namespace std;

class NSHConfiguration
{
	public:

		/*! *************************************************************************************************
		 * @brief NSHConfiguration constructor
		 * **************************************************************************************************
		 */
        NSHConfiguration(string strFileName = CONF_NSH_FILE,
                         string strFileNameDefault = CONF_NSH_DEFAULT_FILE);

		/*! *************************************************************************************************
		 * @brief NSHConfiguration default destructor
		 * **************************************************************************************************
		 */
		virtual ~NSHConfiguration();

		/*! *************************************************************************************************
		 * @brief  initNSHConfiguration reads CONF_NSH_FILE and save the configuration of the NSH
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool initNSHConfiguration(void);

		/*! *************************************************************************************************
		 * @brief  updateNSHConfiguration update an item if it is already present
		 * @param  strItemName item updated
         * @param itemValue the new value to set
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
        bool updateNSHConfiguration(string& strItemName, int itemValue);

		/*! *************************************************************************************************
		 * @brief  updateNSHConfigurations update items if it is already present
		 * @param  vItems items updated
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool updateNSHConfigurations(vector<pair<string, string>>& vItems);

		/*! *************************************************************************************************
		 * @brief  getNSHConfiguration get all the configuration of the NSH
		 * @param  vCalibrationItem vector where the config is stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getNSHConfiguration(vector<pair<string, string>>& vCalibrationItem);

		/*! *************************************************************************************************
		 * @brief  getItemValue get the value of the item
		 * @param  strItemName name of the item
         * @param  strItemValue struct where the item values will be stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getItemValue(const string& strItemName, string& strItemValue);

		/*! *************************************************************************************************
		 * @brief  getLabelList get the list of all the items' name
		 * @param  vList vector where the names will be stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getLabelList(vector<string>& vList);

		/*! *************************************************************************************************
		 * @brief  isLabelValid find out if the label is present or not
		 * @param  strLabel label name
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool isLabelValid(string strLabel);

    private:

        bool							m_bConfigOk;
        vector<pair<string, string>>	m_vCalibrationItem;
        string							m_strFileName, m_strFileNameDefault;

};

#endif // NSHCONFIGURATION_H
