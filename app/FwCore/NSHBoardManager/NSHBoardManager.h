/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHBoardManager.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the NSHBoardManager class.
 @details

 ****************************************************************************
*/

#ifndef NSHBOARDMANAGER_H
#define NSHBOARDMANAGER_H

#include "Thread.h"
#include "StaticSingleton.h"
#include "Loggable.h"
#include "Semaphore.h"

#include "CommonInclude.h"
#include "NSHConfiguration.h"
#include "SPILinkBoard.h"
#include "Operation.h"
#include "OpReadSec.h"

// Config and strips defines
#define NSH_SOLID_STANDARD_ID			0	// SolidStandard identifier in the SH positions
#define NSH_STRIP_1_ID					1
#define NSH_STRIPS_PER_SECTION			6

#define NSH_SOLID_STANDARD_CONFIG_NAME	"SS"
#define NSH_SLOT_1_CENTER_CONFIG_NAME	"Slot_1C"
#define NSH_SLOT_7_CENTER_CONFIG_NAME	"Slot_7C"

enum
{
	eNSHActionInit = 0,
    eNSHActionReInit,
    eNSHActionCmd,
    eNSHActionExecute,
    eNSHActionMoveToSection,
    eNSHActionMove,
    eNSHActionReadSection,
    eNSHActionReadAirSection,
    eNSHActionReadPartialSection,
    eNSHActionFwUpdate,
    eNSHActionEnd,
    eNSHActionNone
};

typedef enum
{
    eRead = 1,
    eAirRead = 2
} eActionType;

typedef enum
{
    eMultiRead = 1,
    eMultiAirRead = 2
} eMultiActionType;

// single coordinate for reading
struct structReading
{
	int liTeorVal;       // teoretical value
	int liActVal;        // actual value
};

// DO NOT MODIFY THE ORDER
enum
{
	eStripLeft = 0,
	eStripCenter,
	eStripRight,
	eStripNone
};


class SPILinkBoard;

class NSHBoardManager : public StaticSingleton<NSHBoardManager>,
						public Thread,
						public Loggable
{
	public:

		/*! *************************************************************************************************
		 * @brief NSHBoardManager constructor
		 * **************************************************************************************************
		 */
		NSHBoardManager();

		/*! *************************************************************************************************
		 * @brief NSHBoardManager destructor
		 * **************************************************************************************************
		 */
		virtual ~NSHBoardManager();

		/*! *************************************************************************************************
		 * @brief  setNSHAction set action and release semaphore
		 * @param  action type of action to be performed: can be init, command or execute
		 * @param  pOp pointer to the operation to be done
		 * @return one of the eOperationStatus enum
		 * **************************************************************************************************
		 */
		int setNSHAction(int action, Operation* pOp);

		/*! *************************************************************************************************
		 * @brief initNSHBoardManager
		 * **************************************************************************************************
		 */
		void initNSHBoardManager(void);

		/*! ***********************************************************************************************************
		 * @brief to check if an action is already ongoing for the NSH
		 * @return eOperationWaitForBirth if no operation is active, eOperationError otherwise
		 * ************************************************************************************************************
		 */
		int checkIsPossibleNSHAction(void);

		/*! *************************************************************************************************
		 * @brief  moveNSHandRead move the NSH to he desired position and get the RFU value
		 * @param  liPosition absolute position where the NSH has to be moved
		 * @param  liValueRead value of RFU read by the NSH
		 * @param  eAction action to be performed (read of air or sample)
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
		int moveNSHandRead(int liPosition, int& liValueRead, eActionType eAction = eRead);

		/*! *************************************************************************************************
		 * @brief  moveNSHandMultiRead move the NSH to he desired position and get the RFU value
		 * @param  liPosition absolute position where the NSH has to be moved
		 * @param  liValueRead value of RFU read by the NSH
		 * @param  eAction action to be performed (multi read of air or sample)
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
		int moveNSHandMultiRead(int liPosition, int& liValueRead, eMultiActionType eAction = eMultiRead);

		/*! *************************************************************************************************
		 * @brief  findXMaxFluoVal - search the position for the maximum fluorescence value along the X axis,
		 *		   for the position which the ID number is received as parameter. A zero ID means the solid
		 *		   standard
		 * @param  ucStripID can be 0 to 12, where 0 is the SS
         * @param liNewCentralPos the maximum position
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
		int findXMaxFluoVal(uint8_t ucStripID, int& liNewCentralPos);

		/*! *************************************************************************************************
		 * @brief  autoCalibrateSS function used to auto calibrate the solid standard
		 * @param  liRfuValSS rfu value of the solid standard
		 * @return 0 in case of success, -1 in case of errors
		 * **************************************************************************************************
		 */
		int autoCalibrateSS(int& liRfuValSS);

		/*! *************************************************************************************************
		 * @brief getErrorList get the listo of active errors formatted as timestamp#code#category#description
         * @param errorLevel the level error by which the errors are filtered
         * @param vErrors vector where the errors will be stored
		 * **************************************************************************************************
		 */
        void getErrorList(vector<string>* vErrors, uint8_t errorLevel = GENERAL_ERROR_LEVEL_NONE);

		/*! *************************************************************************************************
		 * @brief updateErrorsTime to set the correct time of the errors occurred when time was not yet set
		 * @param currentMsec the startup time in msecs
		 * @param currentTime pointer to current time structure
		 * @return the number of changed items
		 * **************************************************************************************************
		 */
		int updateErrorsTime(uint64_t currentMsec, struct tm * currentTime);

		/*! ***********************************************************************************************************
		 * @brief clearErrorList set errors as non active
		 * ************************************************************************************************************
		 */
		void clearErrorList(void);

		/*! ************************************************************************************************************
		 * @brief  getSectionStripSteps get the calibrated position for the NSH on the requested section-strip-position
		 * @param  section the section index (0 - 1)
		 * @param  strip the strip index (0 - 5)
		 * @param  position the position index (right-center-left) as defined in the enum
		 * @return the steps in case of success, -1 in case of errors
		 * *************************************************************************************************************
		 */
		int getSectionStripSteps(uint8_t section, uint8_t strip, uint8_t position);

		/*! ************************************************************************************************************
		 * @brief  readSectionProtocol read all the strips of a section (left-center-right)
		 * @param  section the section index (0 - 1)
		 * @param pRfuConv pointer to the array enabling the model on reader per each strip
		 * @return true in case of success, false otherwise
		 * *************************************************************************************************************
		 */
		bool readSectionProtocol(uint8_t section, uint8_t* pRfuConv);

		/*! ************************************************************************************************************
		 * @brief  readSectionAir read all the strips of a section ( only center)
		 * @param  section the section index (0 - 1)
		 * @return true in case of success, false otherwise
		 * *************************************************************************************************************
		 */
		bool readSectionAir(uint8_t section);

		/*! ************************************************************************************************************
		 * @brief  requestMoveSection called by protocol thread to position the reader on a specific section
		 * @param  section the section index (0 - 1)
		 * @return true in case of success, false otherwise
		 * *************************************************************************************************************
		 */
		bool requestMoveSection(uint8_t section);

		/*! ************************************************************************************************************
		 * @brief  readSection called by protocol thread to read the whole section
		 * @param  section the section index (0 - 1)
		 * @param bComplete true if the reading has to be performed on left-center-right, false if only on center
		 * @return true in case of success, false otherwise
		 * *************************************************************************************************************
		 */
		bool readSection(uint8_t section, bool bComplete);

		/*! ************************************************************************************************************
		 * @brief  getReadSection called by protocol thread to get the read values of the whole section
		 * @param  section the section index (0 - 1)
		 * @param pReadings output pointer to the destination buffer
		 * @return true in case of success, false otherwise
		 * *************************************************************************************************************
		 */
		bool getReadSection(uint8_t section, int32_t * pReadings);

		/*! ************************************************************************************************************
		 * @brief  isActionInProgress check if an action is set for the thread
		 * @return true if an action is running, false otherwise
		 * *************************************************************************************************************
		 */
		bool isActionInProgress();

		/*! ************************************************************************************************************
		 * @brief  requestcheckStripPresence called by protocol thread to read the substrate of the slot requested
         * @param  ucSection the section index (0 - 1)
		 * @param  vSlots vector representing the slots to be checked (1 - 6)
		 * @return true in case of success, false otherwise
		 * *************************************************************************************************************
		 */
		bool requestCheckStripPresence(uint8_t ucSection, const vector<int>& vSlots);

		/*! ************************************************************************************************************
		 * @brief  requestMoveToSlot called by protocol thread to position the reader on a specific slot
		 * @param  ucSection the section index (0 - 1)
		 * @param  strSlot the slot index ("1" - "6", "0" left for SS + "home")
		 * @param  target target of interest
		 * @return true in case of success, false otherwise
		 * *************************************************************************************************************
		 */
		bool requestMoveNshMotor(uint8_t ucSection, string strSlot, eTarget target);


	protected:

		/*! *************************************************************************************************
		 * @brief beforeWorkerThread
		 * **************************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! *************************************************************************************************
		 * @brief  workerThread
		 * @return always 0
		 * **************************************************************************************************
		 */
		int workerThread(void);

		/*! *************************************************************************************************
		 * @brief afterWorkerThread
		 * **************************************************************************************************
		 */
		void afterWorkerThread(void);

	private:

		/*! *************************************************************************************************
		 * @brief  getStripCenter reads the calibr file and gets the central position of the strip
		 * @param  ucStripID id of the strip to be checked, 0 is the SS, 1 to 12 for the slots
		 * @param  pRead pointing to struct where the reading will be stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getStripCenter(uint8_t ucStripID, structReading *pRead = nullptr);

		/*! *************************************************************************************************
		 * @brief  updateNSHStripCenter update calibr file with the new value of the center of the strip
		 * @param  ucStripID id of the strip to be checked, 0 is the SS, 1 to 12 for the slots
		 * @param  liVal new position val
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool updateNSHStripCenter(uint8_t ucStripID, int liVal);		

		/*! ************************************************************************************************************
		 * @brief  checkStripPresence called by protocol thread to read the substrate of the slot requested
		 * @param  section the section index (0 - 1)
		 * @param  vSlots vector representing the slots to be checked  (1 - 6, 0 left for SS)
		 * @return true in case of success, false otherwise
		 * *************************************************************************************************************
		 */
		bool readPartialSection(uint8_t ucSection, const vector<string>& vSlots);

		int setNSHProtocolAction(int action);

		int moveNSH(uint8_t section);
		int moveNSH(uint8_t ucSect, string strSlot, eTarget target);

		FwUpdateInfo* getUpdateInfo(Operation* pOp);

		int setUpdateOutcome(FwUpdateInfo* pInfo, bool bRes);

    private:

        int					m_nNSHAction;
        Operation*			m_pOperation;
        Semaphore			m_synchSem;

        uint8_t				m_section;
        uint8_t				m_rfuStrip[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];
        int32_t				m_readingValues[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS * 3];
		// First member is the target (eTarget), second member is related to the slots to be checked
		pair<eTarget, vector<string>> m_movementList;

    public:

        NSHConfiguration*	m_pConfig;
};

#endif // NSHBOARDMANAGER_H
