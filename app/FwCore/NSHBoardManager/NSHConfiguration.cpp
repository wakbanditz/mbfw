/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHConfiguration.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the NSHConfiguration class.
 @details

 ****************************************************************************
*/

#include "NSHConfiguration.h"

#include <iostream>
#include <fstream>
#include <algorithm>

#define FIELDS_NUM		2

NSHConfiguration::NSHConfiguration(string strFileName, string strFileNameDefault)
{
	m_strFileName = strFileName;
    m_strFileNameDefault.assign(strFileNameDefault);
	m_vCalibrationItem.clear();
	m_bConfigOk = false;
}

NSHConfiguration::~NSHConfiguration()
{

}

bool NSHConfiguration::initNSHConfiguration(void)
{
    if ( m_strFileName.empty() ||
         m_strFileNameDefault.empty() )	return false;

    m_vCalibrationItem.clear();

    ifstream file;
    bool bDefaultFile = false;

    // try to read the cal file: if it's not good or it does not exist (new installation) -> open the default one
    file.open(m_strFileName);
    if ( ! file.good() )
    {
        file.close();
        file.open(m_strFileNameDefault);
        if ( ! file.good() )
        {
            file.close();
            return false;
        }
        bDefaultFile = true;
    }

    int liLinesNum = count(istreambuf_iterator<char>(file), istreambuf_iterator<char>(), '\n');

	if ( liLinesNum == 0 )
    {
		if ( bDefaultFile )
        {
            // the default file is empty -> fail
            file.close();
            return false;
        }
        else
        {
            // try the default one ...
            file.close();
            file.open(m_strFileNameDefault);
            if ( ! file.good() )
            {
                return false;
            }
            bDefaultFile = true;
            // repeat the count as case the target file has changed
            liLinesNum = count(istreambuf_iterator<char>(file), istreambuf_iterator<char>(), '\n');
        }
    }

	file.clear();
	file.seekg(0, ios::beg);

	vector<string> strConf;
	strConf.resize(liLinesNum+1);
	string strTmp("");

	// Save all the lines in a different string
	int liItemsNum = 0;
	while ( getline(file, strTmp) )
	{
		// Remove white spaces
		strTmp.erase(remove(strTmp.begin(), strTmp.end(), ' '), strTmp.end());

		// Do not consider if empty
		if ( strTmp.empty() )		continue;
		// Remove comments
		if ( strTmp[0] == '#' )		continue;

		strConf[liItemsNum] = strTmp;
		liItemsNum++;
	}

	file.close();

	strConf.resize(liItemsNum);
	m_vCalibrationItem.resize(liItemsNum);//, vector<string>(FIELDS_NUM));

	for ( int i = 0; i != liItemsNum; i++ )
	{
		bool bAssignmentFound = false;

		for ( auto c : strConf[i] )
		{
			if ( ! bAssignmentFound )
			{
				if ( c != '=' )
				{
					m_vCalibrationItem[i].first.push_back(c);
				}
				else
				{
					bAssignmentFound = true;
				}
			}
			else
			{
				m_vCalibrationItem[i].second.push_back(c);
			}
		}
	}

	m_bConfigOk = true;

	return true;
}

bool NSHConfiguration::updateNSHConfigurations(vector<pair<string, string>>& vItems)
{
	// Update list of calibration items
	if ( ! m_bConfigOk )
	{
		initNSHConfiguration();
	}

	// Update class members
	uint liSize = m_vCalibrationItem.size();
	bool bItemFound = false;

	for ( uint j = 0; j != vItems.size(); j++ )
	{
		for ( uint i = 0; i != liSize; i++ )
		{
			if ( ! m_vCalibrationItem[i].first.compare(vItems[j].first) )
			{
				m_vCalibrationItem[i].second = vItems[j].second;
				bItemFound = true;
				break;
			}
		}
		if ( ! bItemFound )
		{
			m_vCalibrationItem.push_back(vItems[j]);
		}
	}

	// Update config file
	ofstream file;
	file.open(m_strFileName, std::ofstream::out | std::ofstream::trunc); // trunc to delete the content of the file when opening
	if ( ! file.good() )
	{
		return false;
	}

	liSize = m_vCalibrationItem.size();
	for ( uint i = 0; i != liSize; i++ )
	{
		file << m_vCalibrationItem[i].first << " = " << m_vCalibrationItem[i].second << endl;
	}
	file.close();

	return true;
}

bool NSHConfiguration::updateNSHConfiguration(string& strItemName, int itemValue)
{
	// Update list of calibration items
	if ( ! m_bConfigOk )
	{
		initNSHConfiguration();
	}

	// Update class member
	int liSize = m_vCalibrationItem.size();
	bool bItemFound = false;
	for ( int i = 0; i != liSize; i++ )
	{
		if ( ! m_vCalibrationItem[i].first.compare(strItemName) )
		{
            m_vCalibrationItem[i].second = to_string(itemValue);
			bItemFound = true;
			break;
		}
	}

	if ( ! bItemFound )		return false;

	ofstream file;
	file.open(m_strFileName, std::ofstream::out | std::ofstream::trunc); // trunc to delete the content of the file when opening
	if ( ! file.good() )
	{
		return false;
	}

	for ( int i = 0; i != liSize; i++ )
	{
		file << m_vCalibrationItem[i].first << " = " << m_vCalibrationItem[i].second << endl;
	}
	file.close();

	return true;
}

bool NSHConfiguration::getNSHConfiguration(vector<pair<string, string> >& vCalibrationItem)
{
	if ( ! m_bConfigOk )	return false;

	vCalibrationItem = m_vCalibrationItem;

	return true;
}

bool NSHConfiguration::getItemValue(const string& strItemName, string& strItemValue)
{
	if ( ! m_bConfigOk )	return false;

	strItemValue.clear();
	int liSize = m_vCalibrationItem.size();
	bool bFound = false;

	for ( int i = 0; i != liSize; i++ )
	{
		if ( ! m_vCalibrationItem[i].first.compare(strItemName) )
		{
			strItemValue = m_vCalibrationItem[i].second;
			bFound = true;
			break;
		}
	}

	return bFound;
}

bool NSHConfiguration::getLabelList(vector<string>& vList)
{
	if ( ! m_bConfigOk )	return false;

	vList.clear();

	int liSize = m_vCalibrationItem.size();
	for ( int i = 0; i != liSize; i++ )
	{
		vList.push_back(m_vCalibrationItem[i].first);
	}

	return true;
}

bool NSHConfiguration::isLabelValid(string strLabel)
{
	if ( ! m_bConfigOk )	return false;

	int liSize = m_vCalibrationItem.size();
	for ( int i = 0; liSize; i++ )
	{
		if ( m_vCalibrationItem[i].first == strLabel )
		{
			return true;
		}
	}

	return false;
}

