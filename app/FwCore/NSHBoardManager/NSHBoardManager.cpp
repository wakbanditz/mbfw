/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHBoardManager.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the NSHBoardManager class.
 @details

 ****************************************************************************
*/

#include "NSHBoardManager.h"
#include "NSHReaderGeneral.h"
#include "NSHReaderCommonInclude.h"
#include "MainExecutor.h"

#define SEM_NSH_ACTION_SYNCH_NAME		"/sem-NSH-action-synch"
#define SEM_NSH_ACTION_INIT_VALUE		0

#define NSH_DEF_STATE					'4'

// State Machine Defines
#define NSH_MACHINE_SEARCH_INIT			0
#define NSH_MACHINE_SEARCH_TOOL			1
#define NSH_MACHINE_SEARCH_LWINDOW		2
#define NSH_MACHINE_SEARCH_RWINDOW		3
#define NSH_MACHINE_SEARCH_EXIT			4

// Fluorescence readings
#define NSH_STSRCH_HWIDTH				90     //half width for strip center position
#define NSH_STSRCH_FWIDTH				(2 * NSH_STSRCH_HWIDTH)   // full width for strip center position
#define NSH_DEFAULT_MAX_SH_VAL_AIR		1000
#define NSH_CHECK_SS_PHASE1				0.6f
#define NSH_CHECK_SS_PHASE2				3.0f
#define NSH_CHECK_SS_PHASE3				0.4f
#define NSH_CHECK_SS_MAX_NUM			3
#define NSH_STEP_RESOLUTION				5

#define NSH_MULTI_READ_NUM				3
#define NSH_MULTI_READ_DELTA			19

// ERRORS
#define ERR_MAX_READ_OUT_OF_BOUNDS		18   // maximum reading outside prefixed boundaries

#define SH_STRIP_COFFS		19
#define SH_STRIP_DELTA		360

#define NSH_DEFAULT_SERIAL  "NSH_11111"     // NSH serial number default value

static Mutex	m_mutexNSH;

NSHBoardManager::NSHBoardManager()
{
    m_pOperation = nullptr;
    m_nNSHAction = eNSHActionNone;
    m_pConfig = nullptr;
    m_section = NSH_ID;
    for(uint8_t section = 0; section < SCT_NUM_TOT_SECTIONS; section++)
    {
        for(uint8_t i = 0; i < SCT_NUM_TOT_SLOTS; i++)
            m_rfuStrip[section][i] = 0;
    }
}

NSHBoardManager::~NSHBoardManager()
{
    SAFE_DELETE(m_pConfig);

    if ( isRunning() )
    {
        stopThread();
    }

    msleep(500);

    if ( isStopped() )
    {
        log(LOG_INFO, "NSHBoardManager thread closed");
    }
}

bool NSHBoardManager::isActionInProgress()
{
	if ( m_nNSHAction == eNSHActionNone )	return false;

	return true;
}

void NSHBoardManager::initNSHBoardManager()
{
    SAFE_DELETE(m_pConfig);

    m_pConfig = new NSHConfiguration();
    if ( m_pConfig != nullptr )
    {
        m_pConfig->initNSHConfiguration();
    }

    m_synchSem.create(SEM_NSH_ACTION_SYNCH_NAME, SEM_NSH_ACTION_INIT_VALUE);
    m_synchSem.enableErrorPrint(true);

    if ( !isRunning() )
    {
        startThread();
    }
}

int NSHBoardManager::checkIsPossibleNSHAction()
{
    if ( m_pOperation != 0 )
    {
        log(LOG_ERR, "NSHBoardManager::checkIsPossibleNSHAction: BAD! trying to start with an already running operation");
        return eOperationError;
    }

    return eOperationWaitForBirth;
}

int NSHBoardManager::setNSHAction(int action, Operation* pOp)
{
    log(LOG_INFO, "NSHBoardManager::setNSHAction: ACTION = %d", action);

    if ( m_nNSHAction != eNSHActionNone || m_pOperation != nullptr )
    {
        log(LOG_ERR, "NSHBoardManager::setNSHAction: trying to start when operation in progress");
        return eOperationError;
    }

    if ( action == eNSHActionNone )
    {
        // no action to do
        log(LOG_INFO, "NSHBoardManager::setNSHAction: no action");
        return eOperationWaitForDeath;
    }

    m_pOperation = pOp;
    m_nNSHAction = action;

    int liRetVal = m_synchSem.release();
    if ( liRetVal == SEM_FAILURE )
    {
        log(LOG_ERR, "NSHBoardManager::setNSHAction: unable to unlock thread synch semaphore");
        return eOperationError;
    }
    else
    {
        log(LOG_INFO, "NSHBoardManager::setNSHAction: operation <%d>", m_nNSHAction);
    }

    return eOperationRunning;
}

int NSHBoardManager::setNSHProtocolAction(int action)
{
    log(LOG_INFO, "NSHBoardManager::setNSHProtocolAction: ACTION = %d", action);

    if ( m_nNSHAction != eNSHActionNone)
    {
        log(LOG_ERR, "NSHBoardManager::setNSHProtocolAction: trying to start when operation in progress");
        return eOperationError;
    }

    if ( action == eNSHActionNone )
    {
        // no action to do
        log(LOG_INFO, "NSHBoardManager::setNSHProtoclAction: no action");
        return eOperationError;
    }

    m_pOperation = 0;
    m_nNSHAction = action;

    int liRetVal = m_synchSem.release();
    if ( liRetVal == SEM_FAILURE )
    {
        log(LOG_ERR, "NSHBoardManager::setNSHProtocolAction: unable to unlock thread synch semaphore");
        return eOperationError;
    }
    else
    {
        log(LOG_INFO, "NSHBoardManager::setNSHProtocolAction: operation <%d>", m_nNSHAction);
    }

    return eOperationRunning;
}

int NSHBoardManager::moveNSHandRead(int liPosition, int& liValueRead, eActionType eAction)
{
    if ( ! spiBoardLinkSingleton()->isNSHMotorEnabled() )	return -1;
    if ( ! spiBoardLinkSingleton()->isNSHReaderEnabled() )	return -1;

    // move in the desired position + read
    bool bRes = spiBoardLinkSingleton()->move(eAbsPos, liPosition);
    if ( ! bRes )
    {
        m_pLogger->log(LOG_ERR, "NSHBoardManager::moveNSHandRead: Impossible to move NSH in position %d", liPosition);
        return -1;
    }

    switch ( eAction )
    {
        case eRead:
        {
            structNSHFluoRead sFluoRead;
            int8_t cRes = nshReaderGeneralSingleton()->getNSHFluoRead(NSH_ENABLE_CONVERSION_FACTOR, &sFluoRead);
            if ( cRes )
            {
                m_pLogger->log(LOG_ERR, "NSHBoardManager::moveNSHandRead: Impossible to read RFU value");
                return -1;
            }

            liValueRead = sFluoRead.liRFUValue;
        }
        break;

        case eAirRead:
        {
            int8_t cRes = nshReaderGeneralSingleton()->readAir(liValueRead);
            if ( cRes )
            {
                m_pLogger->log(LOG_ERR, "NSHBoardManager::moveNSHandRead: Impossible to read RFU value of air");
                return -1;
            }
        }
        break;
    }
    return 0;
}

int NSHBoardManager::moveNSHandMultiRead(int liPosition, int& liValueRead, eMultiActionType eAction)
{
    if ( ! spiBoardLinkSingleton()->isNSHMotorEnabled() )	return -1;
    if ( ! spiBoardLinkSingleton()->isNSHReaderEnabled() )	return -1;

    eActionType eSingleAction = ( eAction == eMultiRead ) ? eRead : eAirRead;
    int rgValues[NSH_MULTI_READ_NUM] = {0};
    int rgPositions[NSH_MULTI_READ_NUM] = {liPosition - NSH_MULTI_READ_DELTA, liPosition, liPosition + NSH_MULTI_READ_DELTA};

    for ( int i = 0; i != NSH_MULTI_READ_NUM; i++ )
    {
        int liRes = moveNSHandRead(rgPositions[i], rgValues[i], eSingleAction);
        if ( liRes )
        {
            m_pLogger->log(LOG_ERR, "NSHBoardManager::moveNSHandMultiRead: unable to perform single read");
            return -1;
        }
    }

    liValueRead = 0;
    int liMinValue = INT_MAX;

    for ( int i = 0; i < NSH_MULTI_READ_NUM; i++ )
    {
        liValueRead += rgValues[i];
        if ( rgValues[i] < liMinValue )
        {
            liMinValue = rgValues[i];
        }
    }

    liValueRead -= liMinValue;
    liValueRead /= ( NSH_MULTI_READ_NUM - 1 );

    return 0;
}

int NSHBoardManager::findXMaxFluoVal(uint8_t ucStripID, int& liNewCentralPos)
{
	// Only SS, strip 1 or 7 admitted - not anymore.
	if ( ucStripID > 2*NSH_STRIPS_PER_SECTION )
	{
		m_pLogger->log(LOG_ERR, "NSHBoardManager::findXMaxFluoVal: specified bad calibration position (%d)\n", ucStripID);
		return -1;
	}

	liNewCentralPos = -1;

	structReading read;
	// NOTE : in Modular the ScanHead motor direction is opposite !
	getStripCenter(ucStripID, &read);

	int liLeftIdx = read.liTeorVal - NSH_STSRCH_HWIDTH;
	int liRightIdx = read.liTeorVal + NSH_STSRCH_HWIDTH;
	int liNextIdx = read.liTeorVal;
	int liRFUVal = -1;

	int liFoundLeft = liLeftIdx;
	int liFoundRight = liRightIdx;

	int liStepNum = 0, liSensitivity = 1;
	int liTmpStep = 0, liFoundTool = 0;
	int liMachineStep = NSH_MACHINE_SEARCH_TOOL;

	/*!**************************************************************************/
	/* search window of the tool */
	/*!**************************************************************************/
	while (true)
	{
		liNextIdx += ( liSensitivity * liStepNum );

		if ( (liNextIdx < liLeftIdx) || (liNextIdx > liRightIdx) )
		{
			break;
		}

		int liRes = moveNSHandRead(liNextIdx, liRFUVal);
		if ( liRes )	return -1;

		m_pLogger->log(LOG_DEBUG, "NSHBoardManager::findXMaxFluoVal: [Pos: %d] [reading:%d]", liNextIdx, liRFUVal);

		switch (liMachineStep)
		{
			case NSH_MACHINE_SEARCH_TOOL:
			{
				// move in bisearch to found tool
				if ( liRFUVal > NSH_DEFAULT_MAX_SH_VAL_AIR )
				{
					liFoundTool = liNextIdx;
					// now search window out
					liMachineStep = NSH_MACHINE_SEARCH_LWINDOW;
					liStepNum = -NSH_STEP_RESOLUTION;
					liSensitivity = 1;
				}
				else
				{
					liNextIdx = read.liTeorVal;
					if ( liTmpStep >= 2 )
					{
						liSensitivity++;
						liTmpStep = 0;
					}
					if ( liStepNum == 0 )
					{
						liStepNum = +NSH_STEP_RESOLUTION;
						liSensitivity = 1;
					}
					else
					{
						liStepNum = ( liStepNum > 1 ) ? -NSH_STEP_RESOLUTION : NSH_STEP_RESOLUTION;
						liTmpStep++;
					}
				}
			}
			break;

			case NSH_MACHINE_SEARCH_LWINDOW:
			{
				if ( liRFUVal < NSH_DEFAULT_MAX_SH_VAL_AIR )
				{
					// no found windows
					liFoundLeft = liNextIdx;
					liMachineStep = NSH_MACHINE_SEARCH_RWINDOW;
					// skip first 20 steps with false reading
					liStepNum = 20;
					liSensitivity = 1;
				}
				else
				{
					liNextIdx = liFoundTool;
					liStepNum = -NSH_STEP_RESOLUTION;
					liSensitivity++;
				}
			}
			break;

			case NSH_MACHINE_SEARCH_RWINDOW:
			{
				if ( liRFUVal < NSH_DEFAULT_MAX_SH_VAL_AIR )
				{
					liFoundRight = liNextIdx;
					liMachineStep = NSH_MACHINE_SEARCH_EXIT;
				}
				else
				{
					liNextIdx = liFoundTool;
					liStepNum = NSH_STEP_RESOLUTION;
					liSensitivity++;
				}
			}
			break;

			case NSH_MACHINE_SEARCH_EXIT:
			{
				m_pLogger->log(LOG_DEBUG, "NSHBoardManager::findXMaxFluoVal: Window found [LPos: %d] [RPos:%d]", liFoundLeft, liFoundRight);
			}
			break;
		}
	}

	if ( liFoundTool == 0 )
	{
		m_pLogger->log(LOG_ERR, "NSHBoardManager::findXMaxFluoVal: tool not found");
		return ERR_MAX_READ_OUT_OF_BOUNDS;
	}

	/*..........................................................................
	 . Reads the fluo values starting from the current position and store it    .
	 ...........................................................................*/

	int rgValues[NSH_STSRCH_FWIDTH];
	// preinit value;
	fill_n(rgValues, NSH_STSRCH_FWIDTH, NSH_DEFAULT_MAX_SH_VAL_AIR);

	int liMaxVal = NSH_DEFAULT_MAX_SH_VAL_AIR;
	int liMaxIdx = 0, liValueCnt = 0;
	int liDecreasedCnt = 0, liCentralVal = 0;
	int liStepCnt = liFoundLeft;
	int liDeltaStep = liFoundRight - liFoundLeft;
	bool bContinueToSearch = true;

	for ( int i = 0; i < liDeltaStep; i++ )
	{
		int liRes = moveNSHandRead((liStepCnt + i), liRFUVal);
		if ( liRes )	return -1;

		rgValues[i] = liRFUVal;

		// no air reading
		if ( ( bContinueToSearch ) && ( rgValues[i] > NSH_DEFAULT_MAX_SH_VAL_AIR ) )
		{
			if ( rgValues[i] > liMaxVal )
			{
				liMaxVal = liRFUVal;
				liMaxIdx = i;
				liValueCnt = 1;
				liDecreasedCnt = 0;
				m_pLogger->log(LOG_DEBUG, "NSHBoardManager::findXMaxFluoVal: [Pos: %d] [reading:%d]", (liStepCnt + i), rgValues[i]);
			}
			else
			{
                if ( rgValues[i] == liMaxVal )
				{
					liValueCnt++;
					liDecreasedCnt = 0;
				}
				else
				{
					// value decreasing
					if ( liValueCnt > 0 )//max value is stored
					{
						liDecreasedCnt++;
						if ( liDecreasedCnt >= NSH_STEP_RESOLUTION )
						{
							// five decreasing
							if( ( liMaxIdx + liValueCnt ) >= NSH_STSRCH_FWIDTH )
							{
								/*......................................................................
							   . Maximum value found on invalid position (edge). Returns the error.   .
							   .......................................................................*/
								m_pLogger->log(LOG_ERR, "NSHBoardManager::findXMaxFluoVal: max value not found");
								return ERR_MAX_READ_OUT_OF_BOUNDS;
							}
							else
							{
								/*......................................................................
							   . Acceptable position found for maximum value. Computes it and return. .
							   .......................................................................*/
								liCentralVal = liMaxIdx + (liValueCnt / 2);
								m_pLogger->log(LOG_INFO, "NSHBoardManager::findXMaxFluoVal: max value found [Pos: %d] [Value:%d] Step:%d "
														 "_ MaxId[%d]", (liStepCnt + liCentralVal), rgValues[liCentralVal], i, liMaxIdx);
							}
							bContinueToSearch = false;
							break;
						}
					}
				}
			}
		}
	}

	// This is when the first value is the max value -> the max is not in the range found
	if ( liMaxIdx == 0 )
	{
		m_pLogger->log(LOG_ERR, "NSHBoardManager::findXMaxFluoVal: max value not found");
		return ERR_MAX_READ_OUT_OF_BOUNDS;
	}

	/*......................................................................
	. The maximum reading position has been successfully found. Save it.   .
	.......................................................................*/
	int liBestReadPos = liStepCnt + liCentralVal;    // actual best reading position

	bool bRes = updateNSHStripCenter(ucStripID, liBestReadPos);
	if ( ! bRes )	return -1;

	liNewCentralPos = liBestReadPos;
	return 0;
}

int NSHBoardManager::autoCalibrateSS(int& liRfuValSS)
{
	if ( ! spiBoardLinkSingleton()->isNSHMotorEnabled() )	return -1;
	if ( ! spiBoardLinkSingleton()->isNSHReaderEnabled() )	return -1;

	m_pLogger->log(LOG_INFO, "NSHBoardManager::autoCalibrationSS: Started");
	liRfuValSS = -1;

	/*!*******************************************************************************************
	 * Time for reading. Standard solid.
	 * *******************************************************************************************/

	char cMotorState = spiBoardLinkSingleton()->getMotorState();
	if ( cMotorState != eErrNone )
	{
		m_pLogger->log(LOG_ERR, "NSHBoardManager::autoCalibrationSS: error motor step");
		return -1;
	}

	// Ready to start
	string strVal("");
	int liPos = 0, liFluoVal = 0;
	if ( ! m_pConfig->getItemValue("SS", strVal) ) return false;

    // update the counter of AutoCalibrations
    infoSingleton()->addInstrumentCounter(eCounterAutoCalibration, 1);

    liPos = stoi(strVal);

    int liRes = moveNSHandRead(liPos, liFluoVal);
	if ( liRes )	return -1;

	if ( liFluoVal < NSH_DEFAULT_MAX_SH_VAL_AIR )
	{
		// bracket error, no solid standard present
        spiBoardLinkSingleton()->m_pSPIErr->decodeNotifyEvent(ERR_NSH_SS_NOT_FOUND);
		return -1;
	}

    int liGoldenSS = 0;

	liRes = nshReaderGeneralSingleton()->getSolidStdRFUStored(liGoldenSS);
	if ( liRes )	return -1;

    int liDiffSS = 0;
    float fMeasureSS = 0.;

    if(liGoldenSS > 0)
    {
        liDiffSS = abs(liGoldenSS - liFluoVal);
        fMeasureSS = ((float)liDiffSS / (float)liGoldenSS) * ((float)100.0);
    }

    m_pLogger->log(LOG_INFO, "NSHBoardManager::autoCalibrationSS: [StandardSolid]=%d [Golden]=%d"
							 "[Percentage]=%.2f", liFluoVal, liGoldenSS, fMeasureSS);

	if ( fMeasureSS < NSH_CHECK_SS_PHASE1 )
	{
        registerSolidStandardReadings("CalibrationOK", liFluoVal, liGoldenSS);
        liRfuValSS = liGoldenSS;
		m_pLogger->log(LOG_INFO, "NSHBoardManager::autoCalibrationSS: SS already calibrated");

        // update data on file anyway
        std::ofstream outFile(NSH_SS_FILE);

        strucCalibrParam sCalibr;
        liRes = nshReaderGeneralSingleton()->getCalibrParams(&sCalibr);
        if ( liRes )
        {
            m_pLogger->log(LOG_ERR, "NSHBoardManager::autoCalibrationSS: not able to get gain");
            return -1;
        }
        std::string strGain;
        strGain.assign(to_string(int(sCalibr.fSSCalibPar * 100)));

        // store all values to the cal file
        outFile << to_string(liGoldenSS) << std::endl;
        outFile << to_string(1) << std::endl;
        outFile << to_string(liFluoVal) << std::endl;
        outFile << strGain << std::endl;

        // store value to local variables so that are available without asking the NSH
        infoSingleton()->setSSvalue(to_string(liGoldenSS));
        infoSingleton()->setSSgain(strGain);

        return 0;
	}

	if ( fMeasureSS > NSH_CHECK_SS_PHASE2 )
	{
        registerSolidStandardReadings("CalibrationDrift", liFluoVal, liGoldenSS);
        spiBoardLinkSingleton()->m_pSPIErr->decodeNotifyEvent(WARN_NSH_EXCESSIVE_DRIFT);
		m_pLogger->log(LOG_ERR, "NSHBoardManager::autoCalibrationSS: excessive drift[%f > 3.0]", fMeasureSS);
		return -1;
	}

	/*!*******************************************************************************************
	 * Init auto calibration procedure
	 * *******************************************************************************************/

    std::ofstream outFile(NSH_SS_FILE);

	for ( int liAutoCalibNum = 0; liAutoCalibNum < NSH_CHECK_SS_MAX_NUM; liAutoCalibNum++ )
	{
		m_pLogger->log(LOG_INFO, "NSHBoardManager::autoCalibrationSS: autocalibrate attempt num %d", liAutoCalibNum);

		int liErrCode = -1;
		char cRes = nshReaderGeneralSingleton()->autoCalibrateSolidStd(liErrCode);

		if ( cRes )
		{
			// Error reported automatically
			if ( liErrCode != eNSHErrSolidStdWarning )
			{
				return -1;
			}
		}

		structNSHFluoRead sFluoRead;
		cRes = nshReaderGeneralSingleton()->getNSHFluoRead(NSH_ENABLE_CONVERSION_FACTOR, &sFluoRead);

		if ( cRes )	return -1;

        strucCalibrParam sCalibr;
        liRes = nshReaderGeneralSingleton()->getCalibrParams(&sCalibr);
        if ( liRes )
        {
            m_pLogger->log(LOG_ERR, "NSHBoardManager::autoCalibrationSS: not able to get gain");
            return -1;
        }
        std::string strGain;
        strGain.assign(to_string(int(sCalibr.fSSCalibPar * 100)));

        // store all values to the cal file
        if(liAutoCalibNum == 0)
        {
            // first cycle -> save to file also the GoldenValue
            outFile << to_string(liGoldenSS) << std::endl;
        }
        outFile << to_string(liAutoCalibNum + 1) << std::endl;
        outFile << to_string(sFluoRead.liRFUValue) << std::endl;
        outFile << strGain << std::endl;

		liFluoVal = sFluoRead.liRFUValue;
        liDiffSS = abs(liGoldenSS - liFluoVal);
		fMeasureSS = ((float)liDiffSS / (float)liGoldenSS) * ((float)100.0);
		if ( fMeasureSS <= NSH_CHECK_SS_PHASE3 )
		{
            liRfuValSS = liGoldenSS;
			m_pLogger->log(LOG_INFO, "NSHBoardManager::autoCalibrationSS: autocalibration ended successully, "
									 "[StandardSolid]=%d [Golden]=%d", liFluoVal, liGoldenSS);

            registerSolidStandardReadings("CalibrationExecuted", liFluoVal, liGoldenSS);

            // store value to local variables so that are available without asking the NSH
            infoSingleton()->setSSvalue(to_string(liGoldenSS));
            infoSingleton()->setSSgain(strGain);

            return 0;
		}
		else
		{
			m_pLogger->log(LOG_INFO, "NSHBoardManager::autoCalibrationSS: repeat, cycle %d", liAutoCalibNum);
		}
	}

    spiBoardLinkSingleton()->m_pSPIErr->decodeNotifyEvent(ERR_NSH_EXCESSIVE_DRIFT);

	m_pLogger->log(LOG_ERR, "NSHBoardManager::autoCalibrationSS: unable to calibrate NSH reader");
    registerSolidStandardReadings("CalibrationKO", liFluoVal, liGoldenSS);
    return -1;
}

int NSHBoardManager::moveNSH(uint8_t section)
{
	if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHMotorEnabled() )	return -1;
	if ( ! MainExecutor::getInstance()->m_SPIBoard.isNSHReaderEnabled() )	return -1;

	int steps;
	if ( section == SCT_NUM_TOT_SECTIONS )
	{
		// solid standard position
		structReading read;

		getStripCenter(NSH_SOLID_STANDARD_ID, &read);
		steps = read.liTeorVal;
	}
	else
	{
		// move in the desired position
		steps = getSectionStripSteps(section, 0, eStripLeft);
	}

	if ( steps < 0 )	return -1;

	bool bRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, steps);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "NSHBoardManager::moveNSH: Impossible to move NSH to section %d", section);
		return -1;
	}
	return 0;
}

int NSHBoardManager::moveNSH(uint8_t ucSect, string strSlot, eTarget target)
{
	string strNshPos("");
	// To avoid "SS" case

	if ( ! strSlot.compare("Home") )
	{
		return spiBoardLinkSingleton()->move(eHome);
	}

	int liSlot = 0;
	try	{ liSlot = std::stoi(strSlot); }
	catch (const std::invalid_argument& ia)
	{
		// Only exception acepted is "home"
		log(LOG_ERR, "NSHBoardManager::readPartialSection: Invalid argument %s", strSlot.c_str());
		return -1;
	}

	if ( liSlot != NSH_SOLID_STANDARD_ID )
	{
		liSlot = ( ucSect == SCT_A_ID ) ? liSlot : liSlot + SCT_NUM_TOT_SLOTS;
	}

	switch ( target )
	{
		case eTarget::eNone:
		case eTarget::eSubstrate:
			if ( liSlot == NSH_SOLID_STANDARD_ID )	strNshPos = "SS";
			else									strNshPos = "Slot_" + to_string(liSlot) + "C";
		break;

		case eTarget::eStrip:
			strNshPos = "CameraBC_" + to_string(liSlot);
		break;

		case eTarget::eConeAbsence:
		case eTarget::eSpr:
			strNshPos = "CameraDataMatrix_" + to_string(liSlot);
		break;

		case eTarget::eSample0:
			strNshPos = "CameraSample0_" + to_string(liSlot);
		break;

		case eTarget::eSample3:
			strNshPos = "CameraSample3_" + to_string(liSlot);
		break;
	}

	string strPos("");
	nshBoardManagerSingleton()->m_pConfig->getItemValue(strNshPos, strPos);
	int liPos = stoi(strPos);
	return MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
}

void NSHBoardManager::getErrorList(vector<string>* vErrors, uint8_t errorLevel)
{
	if ( ! spiBoardLinkSingleton()->isSPIErrEnabled() )	return;

    spiBoardLinkSingleton()->m_pSPIErr->getErrorList(vErrors, errorLevel);
}

int NSHBoardManager::updateErrorsTime(uint64_t currentMsec, tm* currentTime)
{
	if ( ! spiBoardLinkSingleton()->isSPIErrEnabled() )	return -1;

	return spiBoardLinkSingleton()->m_pSPIErr->updateErrorsTime(currentMsec, currentTime);
}

void NSHBoardManager::clearErrorList()
{
	if ( ! spiBoardLinkSingleton()->isSPIErrEnabled() )	return;

	spiBoardLinkSingleton()->m_pSPIErr->clearAllErrors();
}

void NSHBoardManager::beforeWorkerThread()
{
    log(LOG_DEBUG, "NSHBoardManager:: thread started");
}

int NSHBoardManager::workerThread()
{
	int8_t cResult;

	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "NSHBoardManager::th: unlocked but wait returned %d", liSynchRet);
			continue;
		}

		if ( m_nNSHAction == eNSHActionNone )
		{
			log(LOG_ERR, "NSHBoardManager::th: BAD unlocked without action");
			continue;
		}
		log(LOG_INFO, "NSHBoardManager::th: operation <%d>", m_nNSHAction);

		if  ( m_nNSHAction == eNSHActionReInit )
		{
			changeDeviceStatus(eIdNsh, eStatusInit);
			m_mutexNSH.lock();
			m_mutexNSH.unlock();
			m_nNSHAction = eNSHActionInit;
		}

		if  ( m_nNSHAction == eNSHActionInit )
		{
			changeDeviceStatus(eIdNsh, eStatusInit);
			m_mutexNSH.lock();

			// clear errors
			clearErrorList();

            bool bRes = true;

            // reset the device via hardware
            log(LOG_INFO, "NSHBoardManager::hwReset");
            if(spiBoardLinkSingleton()->m_pNSHReader->m_NSH.hwReset())
            {
                // error during reset procedure
                log(LOG_ERR, "NSHBoardManager:: error in hwReset !");
                bRes = false;
            }

			structNSHStatus NSHStatus;

			// Wait for NSH in correct state
            uint8_t timeoutCnt = 0;
            while ( bRes == true && NSHStatus.ucStep != NSH_DEF_STATE )
			{
                if(timeoutCnt++ > 10)
                {
                    // 10 secs timeout to be in idle
                    bRes = false;
                }
                else
                {
                    sleep(1);
                    spiBoardLinkSingleton()->m_pNSHReader->m_NSH.getNSHStatus(&NSHStatus);
                }
			}

			if ( bRes == true )
			{
                // read and save application and boot version
                string strFirmware("");
                spiBoardLinkSingleton()->m_pNSHReader->m_NSH.getFwVersion(eAppVersion, strFirmware);
                MainExecutor::getInstance()->m_instrumentInfo.setNshApplicationRelease(strFirmware);
                spiBoardLinkSingleton()->m_pNSHReader->m_NSH.getFwVersion(eBootVersion, strFirmware);
                MainExecutor::getInstance()->m_instrumentInfo.setNshBootRelease(strFirmware);

                // read serial number
                structSHIdentification SHId;
                spiBoardLinkSingleton()->m_pNSHReader->m_NSH.getSHIdentification(&SHId);
                // compose the string
                strFirmware.assign(to_string(SHId.uliLeastSignifSHId));
                strFirmware.append("-");
                strFirmware.append(to_string(SHId.uliMediumSHId));
                strFirmware.append("-");
                strFirmware.append(to_string(SHId.uliMostSignifSHId));
                log(LOG_INFO, "NSHBoardManager::serial number = %s ", strFirmware.c_str());

                // Bring NSH block at home
                if ( spiBoardLinkSingleton()->isNSHEncoderEnabled() )
                {
                    bRes = spiBoardLinkSingleton()->move(eHome);
                }
                else
                {
                    bRes = ( spiBoardLinkSingleton()->m_pNSHMotor->searchForHome() == ERR_NONE );
                }

                int liRfuValSS;
                if(nshBoardManagerSingleton()->autoCalibrateSS(liRfuValSS) == -1)
                {
                    // error in AutoCalibration
                    bRes = false;
                }

                // move the nsh to the SS (to short the movement towards sections)
                if ( bRes == true )
                {
                    moveNSH(SCT_NUM_TOT_SECTIONS);
                }

                // at startup let's read from the file if OPTCHECK is valid or not
                if(readOPTCheckFile() == true)
                {
                    // set OPTCHECK error
                    spiBoardLinkSingleton()->m_pSPIErr->decodeNotifyEvent(ERR_NSH_OPT_CHECK);
                    bRes = false;
                }

                // compare the serial number stored locally on MasterBoard vs the one stored on NSH board
                std::string strStoredSerial;
                strStoredSerial.assign(MainExecutor::getInstance()->m_instrumentInfo.getNshSerialNumber());
                if(strStoredSerial.compare(NSH_DEFAULT_SERIAL) != 0 && strFirmware.compare(strStoredSerial) != 0)
                {
                    // set corresponding error
                    spiBoardLinkSingleton()->m_pSPIErr->decodeNotifyEvent(ERR_NSH_SERIAL_NUMBER);
                    bRes = false;
                }
            }
            // end of initialization procedure
            log(LOG_INFO, "NSHBoardManager:: initialization completed %d !", bRes);

			m_mutexNSH.unlock();

			// the init can be "automatic" at startup or due to a INIT command so linked to a operation
			if(m_pOperation)
			{
                m_pOperation->notifyCompleted(NSH_ID, bRes);
			}

            m_nNSHAction = eNSHActionNone;
            m_pOperation = nullptr;

            if(bRes == true)
            {
                changeDeviceStatus(eIdNsh, eStatusIdle);
            }
            else
            {
                changeDeviceStatus(eIdNsh, eStatusError);
            }
        }
		else if ( m_nNSHAction == eNSHActionMoveToSection )
		{
			m_mutexNSH.lock();
			// movement requested by Protocol Thread
            if(moveNSH(m_section) == -1)
            {
                changeDeviceStatus(eIdNsh, eStatusError);
            }
			m_mutexNSH.unlock();
			m_nNSHAction = eNSHActionNone;
		}
		else if (  m_nNSHAction == eNSHActionMove )
		{
			m_mutexNSH.lock();
			// movement requested by Protocol Thread

			if ( m_movementList.second.size() )
			{
				moveNSH(m_section, m_movementList.second.front(), m_movementList.first);
			}
			m_mutexNSH.unlock();
			m_nNSHAction = eNSHActionNone;
		}
		else if ( m_nNSHAction == eNSHActionReadSection )
		{
			m_mutexNSH.lock();
			// section reading requested by Protocol Thread
            if(readSection(m_section, true) == false)
            {
                changeDeviceStatus(eIdNsh, eStatusError);
            }
            m_mutexNSH.unlock();
			m_nNSHAction = eNSHActionNone;
		}
		else if ( m_nNSHAction == eNSHActionReadAirSection )
		{
			m_mutexNSH.lock();
			// section reading requested by Protocol Thread
            if(readSection(m_section, false) == false)
            {
                changeDeviceStatus(eIdNsh, eStatusError);
            }
            m_mutexNSH.unlock();
			m_nNSHAction = eNSHActionNone;
		}
		else if ( m_nNSHAction ==  eNSHActionReadPartialSection )
		{
			m_mutexNSH.lock();
			// section reading requested by Protocol Thread
			if ( m_movementList.first == eTarget::eSubstrate )
			{
				readPartialSection(m_section, m_movementList.second);
			}
			m_mutexNSH.unlock();
			m_nNSHAction = eNSHActionNone;
		}
		else if ( m_nNSHAction == eNSHActionExecute )
		{
			m_mutexNSH.lock();

			log(LOG_INFO, "NSHBoardManager::workerThread-->execute on NSH");

			//executes the actions related to the incoming command
			if(m_pOperation)
			{
				while(m_pOperation->executeCmd(NSH_ID) == 0) {};
			}

			m_mutexNSH.unlock();

			m_nNSHAction = eNSHActionNone;
			m_pOperation = nullptr;
		}
		else if ( m_nNSHAction == eNSHActionEnd )
		{
			// complete for shutdown purpose
		}
		else if ( m_nNSHAction == eNSHActionCmd )
		{
			m_mutexNSH.lock();

			if ( m_pOperation )
			{
				//executes the actions related to the incoming command
				m_pOperation->executeCmd(NSH_ID);
				m_mutexNSH.unlock();

				/* ********************************************************************************************
				 * Access the state machine
				 * ********************************************************************************************
				 */
				m_pOperation->restoreStateMachine();

				//compile and send the answer
				cResult = m_pOperation->compileSendOutCmd();

				/* ********************************************************************************************
				* Remove the operation from the OperationManager list
				* ********************************************************************************************
				*/
				if ( cResult == eOperationEnabledForDeath )
				{
					/* ********************************************************************************************
					 * Reset variables and remove the common operation
					 * ********************************************************************************************
					 */
					operationSingleton()->removeOperation(m_pOperation);

					log(LOG_INFO, "NSHBoardManager::workerThread-->operation removed");
				}
				else
				{
					log(LOG_ERR, "NSHBoardManager::workerThread-->NO operation removal");
				}
			}
			m_nNSHAction = eNSHActionNone;
			m_pOperation = nullptr;
		}
		else if ( m_nNSHAction  == eNSHActionFwUpdate )
		{
			log(LOG_INFO, "NSHBoardManager::workerThread-->wait FW UPD on nsh");

			FwUpdateInfo* pInfo;
			pInfo = getUpdateInfo(m_pOperation);
			if ( pInfo != nullptr )
			{
				int liRes;
				liRes = spiBoardLinkSingleton()->m_pNSHReader->m_NSHFwUpdate.updateFirmware(pInfo->m_eFwType, pInfo->m_strSourcePath,
																							pInfo->m_lLength, pInfo->m_ullCrc32);
				setUpdateOutcome(pInfo, (liRes==ERR_NONE));
				if ( ! liRes )
				{
					if ( pInfo->m_eFwType == eApplicative )
					{
						infoSingleton()->setNshApplicationRelease(pInfo->m_strNewFwVers);
					}
					else
					{
						infoSingleton()->setNshBootRelease(pInfo->m_strNewFwVers);
					}
				}
			}
			m_nNSHAction = eNSHActionNone;
			m_pOperation = nullptr;
		}
		log(LOG_INFO, "NSHBoardManager::workerThread-->done");

	}
	return 0;
}

void NSHBoardManager::afterWorkerThread()
{
	m_synchSem.closeAndUnlink();
	log(LOG_INFO, "NSHBoardManager::th: stopped");
}

bool NSHBoardManager::getStripCenter(uint8_t ucStripID, structReading* pRead)
{
	string strItem("");

	// Assign the name to the item to be found in the config file
	if ( ucStripID == NSH_SOLID_STANDARD_ID )
	{
		strItem = "SS";
	}
	else
	{
		strItem = "Slot_" + to_string(ucStripID) + "C";
	}

	string strVal("");
	if ( ! m_pConfig->getItemValue(strItem, strVal) ) return false;
	if ( pRead )	pRead->liTeorVal = stoi(strVal);

	return true;
}

int NSHBoardManager::getSectionStripSteps(uint8_t section, uint8_t strip, uint8_t position)
{
	if(section >= SCT_NUM_TOT_SECTIONS || strip >= SCT_NUM_TOT_SLOTS)
	{
		return -1;
	}

	std::string strSuffix("");

	switch(position)
	{
		case eStripCenter:
			strSuffix.assign("C");
		break;
		case eStripRight:
			strSuffix.assign("R");
		break;
		case eStripLeft:
			strSuffix.assign("L");
		break;
		case eStripNone :
		default:
		break;
	};

	if(strSuffix.empty())
	{
		return -1;
	}

	// Note strip is 0-5 while in file is 1-6
	uint8_t stripIndex = section * SCT_NUM_TOT_SLOTS + strip + 1;

	string strItem("");

	// Assign the name to the item to be found in the config file
	strItem = "Slot_" + to_string(stripIndex) + strSuffix;

	string strStepsVal("");

	if(m_pConfig->getItemValue(strItem, strStepsVal) == false)
	{
		return -1;
	}

	int liSteps = stoi(strStepsVal);

	return liSteps;
}

bool NSHBoardManager::updateNSHStripCenter(uint8_t ucStripID, int liVal)
{
	vector<pair<string, string>> vItems;

	if ( ucStripID == NSH_SOLID_STANDARD_ID )
	{
		string strName = NSH_SOLID_STANDARD_CONFIG_NAME;
		bool bRes = m_pConfig->updateNSHConfiguration(strName, liVal);
		return bRes;
	}
	else if ( ucStripID <= 2*NSH_STRIPS_PER_SECTION )
	{
		char cStart = NSH_STRIP_1_ID;
		if ( ucStripID > NSH_STRIPS_PER_SECTION )	cStart += NSH_STRIPS_PER_SECTION;

		for ( int i = cStart; i < cStart+NSH_STRIPS_PER_SECTION; i++ )
		{
			int liCentralVal = liVal - ( (i-ucStripID) * SH_STRIP_DELTA );

			pair<string, string> vLeft("Slot_" + to_string(i) + "L", to_string(liCentralVal+SH_STRIP_COFFS));
			pair<string, string> vCenter("Slot_" + to_string(i) + "C", to_string(liCentralVal));
			pair<string, string> vRight("Slot_" + to_string(i) + "R", to_string(liCentralVal-SH_STRIP_COFFS));

			vItems.push_back(vLeft);
			vItems.push_back(vCenter);
			vItems.push_back(vRight);

			// Finding the slot positions also the position related to X0, X3, BC and DM have to be calculated
			// TODO fixed shift to be found!
			int shift = 0;
			pair<string, string> vCamera0("CameraSample0_" + to_string(i), to_string(liCentralVal+shift));
			pair<string, string> vCamera3("CameraSample3_" + to_string(i), to_string(liCentralVal+shift));
			pair<string, string> vCameraBC("CameraBC_" + to_string(i), to_string(liCentralVal+shift));
			pair<string, string> vCameraDM("CameraDataMatrix_" + to_string(i), to_string(liCentralVal+shift));
			vItems.push_back(vCamera0);
			vItems.push_back(vCamera3);
			vItems.push_back(vCameraBC);
			vItems.push_back(vCameraDM);
		}
	}
	else
	{
		return false;
	}

	bool bRes = m_pConfig->updateNSHConfigurations(vItems);

	return bRes;
}


bool NSHBoardManager::readSectionProtocol(uint8_t section, uint8_t * pRfuConv)
{
    if ((section != SCT_A_ID) && (section != SCT_B_ID ))		return false;

    m_section = section;

    for(uint8_t strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
    {
        // first set the rfu conversion
        if(*pRfuConv == 1)
        {
            m_rfuStrip[section][strip] = eNSHModelEnabled;
        }
        else
        {
            m_rfuStrip[section][strip] = eNSHModelDisabled;
        }

        ++pRfuConv;
    }

    if(setNSHProtocolAction(eNSHActionReadSection) != eOperationRunning)
    {
        return false;
    }

    return true;
}

bool NSHBoardManager::readSectionAir(uint8_t section)
{
	if ((section != SCT_A_ID) && (section != SCT_B_ID ))		return false;

	m_section = section;

	if(setNSHProtocolAction(eNSHActionReadAirSection) != eOperationRunning)
	{
		return false;
	}

	return true;
}

bool NSHBoardManager::requestMoveSection(uint8_t section)
{
    if(section == SCT_NUM_TOT_SECTIONS)
    {
        // movement to the SolidStandard position
        if(getStripCenter(NSH_SOLID_STANDARD_ID) == false)
        {
            return false;
        }
    }
    else if(getSectionStripSteps(section, 0, eStripLeft) < 0)
    {
        return false;
    }

    m_section = section;

    if(setNSHProtocolAction(eNSHActionMoveToSection) != eOperationRunning)
    {
        return false;
    }

    return true;
}

bool NSHBoardManager::readSection(uint8_t section, bool bComplete)
{
    uint8_t readIndex = 0;

    uint8_t startPosition, endPosition;

    if(bComplete == true)
    {
        // standard strip reading in protocol
        startPosition = eStripLeft;
        endPosition = eStripNone;
    }
    else
    {
        // air reading in protocol
        startPosition = eStripCenter;
        endPosition = eStripCenter + 1;
    }

    // preliminary clear of read values
    for(uint8_t strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
    {
        for(uint8_t position = startPosition; position < endPosition; position++)
        {
            m_readingValues[section][readIndex] = 0;
            ++readIndex;
        }
    }

    readIndex = 0;
    // loop on all strips
    for(uint8_t strip = 0; strip < SCT_NUM_TOT_SLOTS; strip++)
    {
        int steps;
        structNSHFluoRead localNSHFluoRead;

        for(uint8_t position = startPosition; position < endPosition; position++)
        {
            // move the reader to the strip position
            steps = getSectionStripSteps(section, strip, position);
            if(MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, steps) == false)
            {
                log(LOG_INFO, "NSHBoardManager::readSectionProtocol strip %d move %d !", strip, position);
                return false;
            }
            // perform reading and store reading value
            if(nshReaderGeneralSingleton()->getNSHFluoRead(m_rfuStrip[section][strip], &localNSHFluoRead) != 0)
            {
                log(LOG_INFO, "NSHBoardManager::readSectionProtocol strip %d reading !", strip);
                return false;
            }
            m_readingValues[section][readIndex] = localNSHFluoRead.liRFUValue;
            ++readIndex;
        }
    }

    // last movement to the Solid Standard (in between the sections)
    structReading read;

    getStripCenter(NSH_SOLID_STANDARD_ID, &read);
    if(MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, read.liTeorVal) == false)
    {
        log(LOG_ERR, "NSHBoardManager::readSectionProtocol SS move %d !", read.liTeorVal);
        return false;
    }

    // update the counter of Readings
    infoSingleton()->addInstrumentCounter(eCounterReading, 1);

    return true;
}

bool NSHBoardManager::getReadSection(uint8_t section, int32_t * pReadings)
{
    if ((section != SCT_A_ID) && (section != SCT_B_ID ))		return false;

    // loop on all values
    for(uint8_t counter = 0; counter < SCT_NUM_TOT_SLOTS * 3; counter++)
    {
		*pReadings = m_readingValues[section][counter];
		++pReadings;
    }
    return true;
}

bool NSHBoardManager::requestCheckStripPresence(uint8_t ucSection, const vector<int>& vSlots)
{
	m_section = ucSection;
	m_movementList.first = eTarget::eSubstrate;
	m_movementList.second.clear();
	for ( size_t i = 0; i != vSlots.size(); ++i )
	{
		m_movementList.second.push_back(to_string(vSlots[i]));
	}

	if ( setNSHProtocolAction(eNSHActionReadPartialSection) != eOperationRunning )
	{
		return false;
	}

	return true;
}

bool NSHBoardManager::requestMoveNshMotor(uint8_t ucSection, string strSlot, eTarget target)
{
	if ( ! spiBoardLinkSingleton()->isNSHMotorEnabled() )	return -1;

	m_section = ucSection;
	m_movementList.first = target;
	m_movementList.second.assign(1, strSlot);

	if ( setNSHProtocolAction(eNSHActionMove) != eOperationRunning )
	{
		return false;
	}

	return true;
}

bool NSHBoardManager::readPartialSection(uint8_t ucSection, const vector<string>& vSlots)
{
	if ( ucSection >= SCT_NUM_TOT_SECTIONS )		return false;
	if ( vSlots.empty() )							return true;

	bool bRes = true;
	for ( uint8_t i = 0; i < vSlots.size(); ++i )
	{
		int liSlot = 0;
		try
		{
			liSlot = std::stoi(vSlots[i]);
		}
		catch (const std::invalid_argument& ia)
		{
			log(LOG_ERR, "NSHBoardManager::readPartialSection: Invalid argument %s", vSlots[i].c_str());
			continue;
		}

		structNSHFluoRead fluoRead;
		int liPos = getSectionStripSteps(ucSection, liSlot, eStripCenter);

		bRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, liPos);
		if ( ! bRes )
		{
			log(LOG_ERR, "NSHBoardManager::readPartialSection: unable to move NSH to pos [%d] to strip %d", liPos, i);
			return false;
		}

		if ( nshReaderGeneralSingleton()->getNSHFluoRead(m_rfuStrip[ucSection][liSlot], &fluoRead) != 0 )
		{
			log(LOG_ERR, "NSHBoardManager::readPartialSection: unable to read RFU value of [strip %d] !", i);
			return false;
		}
		m_readingValues[ucSection][liSlot] = fluoRead.liRFUValue;
	}

	// Move the SH to default position (SS) only if needed
	structReading read;
	getStripCenter(NSH_SOLID_STANDARD_ID, &read);
	bRes = MainExecutor::getInstance()->m_SPIBoard.move(eAbsPos, read.liTeorVal);
	if ( ! bRes )
	{
		log(LOG_ERR, "NSHBoardManager::readPartialSection: unable to move NSH to SS pos [%d]", read.liTeorVal);
		return false;
	}

	// update the counter of Readings
	infoSingleton()->addInstrumentCounter(eCounterReading, 1);

	return true;
}

FwUpdateInfo* NSHBoardManager::getUpdateInfo(Operation* pOp)
{
    if ( pOp == nullptr )	return nullptr;

    OpFwUpdate* pUpdOp = (OpFwUpdate*)pOp;
    return pUpdOp->getLastDeviceInfoForUpd();
}

int NSHBoardManager::setUpdateOutcome(FwUpdateInfo* pInfo, bool bRes)
{
	if ( pInfo == nullptr )	return -1;

    OpFwUpdate* pUpdOp = (OpFwUpdate*)m_pOperation;
    return pUpdOp->writeUpdOutcomeLog(pInfo->m_eId, pInfo->m_eFwType, bRes);
}
