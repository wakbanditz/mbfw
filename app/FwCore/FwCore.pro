TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

# path inclusions
INCLUDEPATH += $$PWD/../../

# internal libraries
unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/XmlParser/ -lXmlParser
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/XmlParser/XercesSrc
DEPENDPATH += $$PWD/../../bmx_libs/Libs/XmlParser/XercesSrc
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/XmlParser/libXmlParser.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/HttpClient/ -lHttpClient
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/HttpClient
DEPENDPATH += $$PWD/../../bmx_libs/Libs/HttpClient
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/HttpClient/libHttpClient.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/WebServer/ -lWebServer
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/WebServer
DEPENDPATH += $$PWD/../../bmx_libs/Libs/WebServer
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/WebServer/libWebServer.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/SerialInterface/ -lSerialInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/SerialInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/SerialInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/SerialInterface/libSerialInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/USBInterface/ -lUSBInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/USBInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/USBInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/USBInterface/libUSBInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Thread
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/CanInterface/ -lCanInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/CanInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/CanInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/CanInterface/libCanInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/FolderManager/ -lFolderManager
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/FolderManager
DEPENDPATH += $$PWD/../../bmx_libs/Libs/FolderManager
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/FolderManager/libFolderManager.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Log
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/IPC/ -lIPC
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/IPC
DEPENDPATH += $$PWD/../../bmx_libs/Libs/IPC
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/IPC/libIPC.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/DataCollector/ -lDataCollector
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/DataCollector
DEPENDPATH += $$PWD/../../bmx_libs/Libs/DataCollector
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/DataCollector/libDataCollector.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/SPIInterface/ -lSPIInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/SPIInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/SPIInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/SPIInterface/libSPIInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/TimeStamp
DEPENDPATH += $$PWD/../../bmx_libs/Libs/TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/TimeStamp/libTimeStamp.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/GpioInterface/ -lGpioInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/GpioInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/GpioInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/GpioInterface/libGpioInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Config/ -lConfig
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Config
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Config/libConfig.a

INCLUDEPATH += $$PWD/../../bmx_libs/Libs/StaticSingleton
DEPENDPATH += $$PWD/../../bmx_libs/Libs/StaticSingleton

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/I2CInterface/ -lI2CInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/I2CInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/I2CInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/I2CInterface/libI2CInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Scheduler/ -lScheduler
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Scheduler
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Scheduler
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Scheduler/libScheduler.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/UdpClient/ -lUdpClient
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/UdpClient
DEPENDPATH += $$PWD/../../bmx_libs/Libs/UdpClient
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/UdpClient/libUdpClient.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/SectionTimeCalc/ -lSectionTimeCalc
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/SectionTimeCalc
DEPENDPATH += $$PWD/../../bmx_libs/Libs/SectionTimeCalc
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/SectionTimeCalc/libSectionTimeCalc.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/EventRecorder/ -lEventRecorder
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/EventRecorder
DEPENDPATH += $$PWD/../../bmx_libs/Libs/EventRecorder
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/EventRecorder/libEventRecorder.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/TARUtilities/ -lTARUtilities
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/TARUtilities
DEPENDPATH += $$PWD/../../bmx_libs/Libs/TARUtilities
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/TARUtilities/libTARUtilities.a


# system libraries
LIBS += -pthread -lrt -lpthread  -lcurl -lasound
LIBS += -lusb-1.0
LIBS += -ludev

#headers and sources inside project subfolders
include(../../mbfwInclude.pri)
include(WebServer/WebServer.pri)
include(WebClient/WebClient.pri)
include(PeripheralLinks/PeripheralLinks.pri)
include(WorkflowManager/WorkflowManager.pri)
include(MainExecutor/MainExecutor.pri)
include(FirmwareUpdateManager/FirmwareUpdateManager.pri)
include(InstrumentInfo/InstrumentInfo.pri)
include(SectionBoardManager/SectionBoardManager.pri)
include(PowerOffManager/PowerOffManager.pri)
include(NSHBoardManager/NSHBoardManager.pri)
include(ProtocolManager/ProtocolManager.pri)
include(CameraBoardManager/CameraBoardManager.pri)
include(ErrorManager/ErrorManager.pri)
include(MasterBoardManager/MasterBoardManager.pri)

SOURCES += main.cpp
TARGET = FwCore

# deployment directives
target.path = /home/root
INSTALLS += target

# project defines
# DEFINES += CONF_FILE=\\\"$${CONFIG_FILE}\\\"
DEFINES += PROGRAM_NAME=\\\"$${PROCESS_NAME_FW_CORE}\\\"

DISTFILES +=

HEADERS +=
