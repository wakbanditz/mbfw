/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PowerOffCommonInclude.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the PowerOffManager class.
 @details

 ****************************************************************************
*/

#ifndef POWEROFFCOMMONINCLUDE_H
#define POWEROFFCOMMONINCLUDE_H

#define CFG_POFF_GPIO_ITR		1
#define CFG_POFF_GPIO_MOFF		2


#define POFF_DEF_GPIO_IDX_ITR	"5"
#define POFF_DEF_GPIO_IDX_OUT	"6"

#endif // POWEROFFCOMMONINCLUDE_H
