/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PowerOffManager.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the PowerOffManager class.
 @details

 ****************************************************************************
*/

#ifndef POWEROFFMANAGER_H
#define POWEROFFMANAGER_H

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#include "stdint.h"
#include "Thread.h"
#include "StaticSingleton.h"
#include "Loggable.h"
#include "CommonInclude.h"
#include "GpioInterface.h"
#include "PowerOffCommonInclude.h"

typedef enum
{
	ePOFFWaitIrq			= 0,
	ePOFFResetFlag			= 1,
	ePOFFSendMsgToGW		= 2,
	ePOFFWaitMsgFromGW		= 3,
    ePOFFCtrlGWOff			= 4,
    ePOFFShutdown			= 5,
    ePOFFWaitShutdown       = 6
}ePowerOffManagerStatus;

#define POFF_TIMEOUT_WAIT_ITR	10000
/*!
 * @brief The PowerOffManager class is dedicated for the management of the power off procedure.
 */
class PowerOffManager : public StaticSingleton<PowerOffManager>,
						public Thread,
						public Loggable
{
	public:

		/*! ***********************************************************************************************************
		 * @brief PowerOffManager constructor
		 * ************************************************************************************************************
		 */
		PowerOffManager();

		/*! ***********************************************************************************************************
		 * @brief PowerOffManager destructor
		 * ************************************************************************************************************
		 */
		virtual ~PowerOffManager();

        /*!
         * \brief initPMManager
         * \param sConfigFileName
         * \param gpioInterface
         * \return
         */
        int initPMManager(const char* sConfigFileName, GpioInterface *gpioInterface);

		/*!
		 * @brief sendMasterOffSignal
		 * @return 0 in case of success, -1 in case of error.
		 */
		int sendMasterOffSignal(void);

        /**
         * @brief shutdownBoard It calls the library function to invoke the shutdown of the system
         */
        void shutdownBoard(void);


	protected:

		/*! ***********************************************************************************************************
		 * @brief workerThread neverending thread normally waiting on the semaphore : when the semaphore it's released
		 *		the thread executes the procedure
		 * @return 0 when thread ends
		 * ***********************************************************************************************************
		 */
		int workerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions before the start of the thread
		 * ***********************************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! ***********************************************************************************************************
		 * @brief beforeWorkerThread to perform actions after the end of the thread
		 * ***********************************************************************************************************
		 */
		void afterWorkerThread(void);

    private:
        /*!
         * @brief checkItrPwrManager Checks if the relates interrupt is triggered on the requested pin.
         * @param uliTimeOutMsec
         * @return
         */
        int checkItrPwrManager(uint32_t uliTimeOutMsec = POFF_TIMEOUT_WAIT_ITR);


    private:

        GpioInterface*	m_pGpio;
        int8_t			m_nMasterOffIdx;
        int8_t			m_nItrPwrManIdx;
        ePowerOffManagerStatus	m_eState;
        Config			m_Config;
        bool            m_bshutdownState;

};

#endif // POWEROFFMANAGER_H
