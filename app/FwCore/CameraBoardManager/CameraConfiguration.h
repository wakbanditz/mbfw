/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CameraConfiguration.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CameraConfiguration class..
 @details

 ****************************************************************************
*/
#ifndef CAMERACONFIGURATION_H
#define CAMERACONFIGURATION_H
#include <unordered_map>

#include "CR8062Include.h"

using namespace std;

typedef struct
{
	string strItem;
	string strX0;
	string strY0;
	string strWidth;
	string strHeight;
} structCameraConf;

enum class eConfigType { eROI, eCW };

typedef struct
{
	eBatchTarget eTarget;
	int			 x;
	int			 y;
	int			 width;
	int			 height;
} structRect;

class CameraConfiguration
{
	public:

		/*! *************************************************************************************************
         * @brief CameraConfiguration constructor
		 * **************************************************************************************************
		 */
        CameraConfiguration(string strFileName = CONF_CAMERA_FILE,
                            string strFileNameDefault = CONF_CAMERA_DEFAULT_FILE);

		/*! *************************************************************************************************
         * @brief CameraConfiguration destructor
		 * **************************************************************************************************
		 */
		virtual ~CameraConfiguration();

		/*! *************************************************************************************************
		 * @brief  initCameraConfiguration reads CONF_CAMERA_FILE and save the configuration of the camera
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool initCameraConfiguration(void);

		/*! *************************************************************************************************
		 * @brief  updateCameraConfig update all the configuration of the camera or a subset of it
		 * @param  mSubSet map of items to be updated
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool updateCameraConfig(unordered_map<string, structRect>& mSubSet);

		/*! *************************************************************************************************
		 * @brief  updateCameraConfigFile update the config file with the member values.
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool updateCameraConfigFile(void);

		/*! *************************************************************************************************
		 * @brief  getCameraConfig get all the configuration of the camera
         * @param  mConfig vector where the config is stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getCameraConfig(unordered_map<string, structRect>& mConfig);

		/*! *************************************************************************************************
		 * @brief  getItemValue get the value of the item
		 * @param  strItemName name of the item
         * @param  rect struct where the item values will be stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getItemValue(const string& strItemName, structRect& rect);

		/** *************************************************************************************************
		 * @brief  setItemValue get the value of the item
		 * @param  strItemName name of the item
         * @param  rect struct where the item values will be stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool setItemValue(const string& strItemName, const structRect& rect);

		/*! *************************************************************************************************
		 * @brief  getLabelList get the list of all the items' name
		 * @param  vList vector where the names will be stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getLabelList(vector<string>& vList);

		/*! *************************************************************************************************
		 * @brief  isLabelValid find out if the label is present or not
		 * @param  strLabel label name
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool isLabelValid(string& strLabel);

    private:

        bool	m_bConfigOk;
        string	m_strFileName, m_strFileNameDefault;

        // Here are only stored ROI and CW
        unordered_map<string, structRect> m_mapItems;

};

#endif // CAMERACONFIGURATION_H
