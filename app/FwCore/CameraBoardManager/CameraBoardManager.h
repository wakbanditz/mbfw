/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CameraBoardManager.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CameraBoardManager class..
 @details

 ****************************************************************************
*/

#ifndef CAMERABOARDMANAGER_H
#define CAMERABOARDMANAGER_H

#include "Thread.h"
#include "StaticSingleton.h"
#include "Loggable.h"
#include "Semaphore.h"

#include "CommonInclude.h"
#include "FWUpdateCommonInclude.h"
#include "CameraConfiguration.h"
#include "USBErrors.h"
#include "Operation.h"

#define SEM_CAMERA_ACTION_SYNCH_NAME		"/sem-Camera-action-synch"
#define SEM_CAMERA_ACTION_INIT_VALUE		0

enum
{
	eCameraActionInit = 0,
	eCameraActionReInit,
	eCameraActionCmd,
	eCameraActionDecodeBC,
	eCameraActionDecodeDM,
	eCameraActionCheckW0,
	eCameraActionCheckW3,
	eCameraActionCheckSpr,
	eCameraActionExecute,
	eCameraActionFwUpdate,
	eCameraActionNone
};

class CameraBoardManager :	public StaticSingleton<CameraBoardManager>,
							public Thread,
							public Loggable
{
	public:

		/*! *************************************************************************************************
		 * @brief CameraBoardManager default constructor
		 * **************************************************************************************************
		 */
		CameraBoardManager();

		/*! *************************************************************************************************
		 * @brief CameraBoardManager default destructor
		 * **************************************************************************************************
		 */
		virtual ~CameraBoardManager();

		/*! *************************************************************************************************
		 * @brief initCameraBoardManager
		 * **************************************************************************************************
		 */
		void initCameraBoardManager(void);

		/*! ***********************************************************************************************************
		 * @brief to check if an action is already ongoing for the camera
		 * @return eOperationWaitForBirth if no operation is active, eOperationError otherwise
		 * ************************************************************************************************************
		 */
		int checkIsPossibleCameraAction(void);
		int isCameraBusy(void);
		int isActionInProgress(void);

		/*! *************************************************************************************************
		 * @brief  setCameraAction set action and release semaphore
		 * @param  action type of action to be performed: can be init, command or execute
		 * @param  pOp pointer to the operation to be done
		 * @return one of the eOperationStatus enum
		 * **************************************************************************************************
		 */
		int setCameraAction(int action, Operation* pOp);

		/*! *************************************************************************************************
		 * @brief  setCameraAction set action and release semaphore
		 * @param  action type of action to be performed: can be init, command or execute
		 * @return one of the eOperationStatus enum
		 * **************************************************************************************************
		 */
		int setCameraReadSecAction(int action);

		/*! *************************************************************************************************
		 * @brief getErrorList get the list of the active errors
		 * @param vErrors vector where the errors will be stored
         * @param errorLevel the level of errors to retrieve
		 * **************************************************************************************************
		 */
        void getErrorList(vector<string>* vErrors, uint8_t errorLevel);

		/*! ***********************************************************************************************************
		 * @brief clearErrorList set errors as non active
		 * ************************************************************************************************************
		 */
		void clearErrorList(void);

		/*! *************************************************************************************************
		 * @brief updateErrorsTime to set the correct time of the errors occurred when time was not yet set
		 * @param currentMsec the startup time in msecs
		 * @param currentTime pointer to current time structure
		 * @return the number of changed items
		 * **************************************************************************************************
		 */
		int updateErrorsTime(uint64_t currentMsec, tm* currentTime);

		int requestDecodeBarcode(int liSectionId, int liSlotId, bool bTakePicture);
		int requestDecodeDataMatrix(int liSectionId, int liSlotId, bool bTakePicture);
		int requestCheckSamplePresence(int liWellNum, int liSectionId, int liSlotId, bool bTakePicture);
		int requestCheckSprPresence(int liSectionId, int liSlotId, bool bTakePicture);
		void getDecodedCode(string& strBarcode, vector<unsigned char>&vImage, int liSectionId, int liSlotId);
		void getSamplePresenceResult(float&fResult, vector<unsigned char>& vImage, int liSectionId, int liSlotId);


	protected:

		/*! *************************************************************************************************
		 * @brief beforeWorkerThread
		 * **************************************************************************************************
		 */
		void beforeWorkerThread(void);

		/*! *************************************************************************************************
		 * @brief  workerThread
		 * @return always 0
		 * **************************************************************************************************
		 */
		int workerThread(void);

		/*! *************************************************************************************************
		 * @brief afterWorkerThread
		 * **************************************************************************************************
		 */
		void afterWorkerThread(void);

	private:

		FwUpdateInfo* getUpdateInfo(Operation* pOp);

		int setUpdateOutcome(FwUpdateInfo* pInfo, bool bRes);

		int decodeBarcode(int liSlot, int liSection, bool bGetPicture);

		int decodeDataMatrix(int liSlot, int liSection, bool bGetPicture);

		int checkSamplePresence(int liSlot, int liSection, int liWell, bool bStorePic);

		int checkSprPresence(int liSlot, int liSection, bool bGetPicture);

		void recoverCamera(void);

    private:

		int			m_nCameraAction;
		Operation*	m_pOperation;
		Semaphore	m_synchSem;

		string		m_vDecodedCodes[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS]; 
		float		m_fAlgorithmOutput[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];
		vector<unsigned char> m_vPictures[SCT_NUM_TOT_SECTIONS][SCT_NUM_TOT_SLOTS];

    public:

		CameraConfiguration*	m_pConfig;
};

#endif // CAMERABOARDMANAGER_H
