/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CameraBoardManager.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CaneraBoardManager class.
 @details

 ****************************************************************************
*/

#include "CameraBoardManager.h"
#include "MainExecutor.h"
#include "SprDetection/SprDetectionLink.h"

#define SELECTED_CODE			"_"
#define SELECTED_PICTURE		'_'
#define DECODE_BUT_NOT_GET_PIC	'^'
#define WELL_0					0
#define WELL_3					3
#define CODE_NOT_DECODED_MSG	"______" // when dtmx not decoded, but we don't know if the spr is present or not
#define CODE_NOT_PRESENT_MSG	"!"		 // when the spr is not present
#define CODE_PRESENT_MSG		"?"		 // when the spr is present, but dtmx not decoded

static Mutex m_mutexCamera;

CameraBoardManager::CameraBoardManager()
{
	m_pOperation = nullptr;
	m_nCameraAction = eCameraActionNone;
	m_pConfig = nullptr;
}

CameraBoardManager::~CameraBoardManager()
{
	SAFE_DELETE(m_pConfig);

	if ( isRunning() )
	{
		stopThread();
	}

	msleep(500);

	if ( isStopped() )
	{
		log(LOG_INFO, "CameraBoardManager thread closed");
	}
}

void CameraBoardManager::initCameraBoardManager()
{
	SAFE_DELETE(m_pConfig);

	m_pConfig = new CameraConfiguration();
	if ( m_pConfig != nullptr )
	{
		m_pConfig->initCameraConfiguration();
	}

	m_synchSem.create(SEM_CAMERA_ACTION_SYNCH_NAME, SEM_CAMERA_ACTION_INIT_VALUE);
	m_synchSem.enableErrorPrint(true);

	if ( !isRunning() )
	{
		startThread();
	}
}

int CameraBoardManager::checkIsPossibleCameraAction(void)
{
	if ( m_pOperation != 0 )
	{
		log(LOG_ERR, "CameraBoardManager::checkIsPossibleCameraAction: BAD! trying to start with an already running operation");
		return eOperationError;
	}

	return eOperationWaitForBirth;
}

int CameraBoardManager::isCameraBusy(void)
{
	if ( m_pOperation != 0 )
	{
        return true;
	}
    return false;
}

int CameraBoardManager::isActionInProgress()
{
	if( m_nCameraAction == eCameraActionNone )	return false;

	return true;
}

int CameraBoardManager::setCameraAction(int action, Operation* pOp)
{
	log(LOG_INFO, "CameraBoardManager::setCameraAction: ACTION = %d", action);

	if ( m_nCameraAction != eCameraActionNone || m_pOperation != nullptr )
	{
		log(LOG_ERR, "CameraBoardManager::setCameraAction: trying to start when operation in progress");
		return eOperationError;
	}

	if ( action == eCameraActionNone )
	{
		// no action to do
		log(LOG_INFO, "CameraBoardManager::setCameraAction: no action");
		return eOperationWaitForDeath;
	}

	m_pOperation = pOp;
	m_nCameraAction = action;

	int liRetVal = m_synchSem.release();
	if ( liRetVal == SEM_FAILURE )
	{
		log(LOG_ERR, "CameraBoardManager::setCameraAction: unable to unlock thread synch semaphore");
		return eOperationError;
	}
	else
	{
		log(LOG_INFO, "CameraBoardManager::setCameraAction: operation <%d>", m_nCameraAction);
	}

	return eOperationRunning;
}

int CameraBoardManager::setCameraReadSecAction(int action)
{
	log(LOG_INFO, "CameraBoardManager::setCameraReadSecAction: ACTION = %d", action);

	if ( m_nCameraAction != eCameraActionNone)
	{
		log(LOG_ERR, "CameraBoardManager::setCameraReadSecAction: trying to start when operation in progress");
		return eOperationError;
	}

	if ( action == eCameraActionNone )
	{
		// no action to do
		log(LOG_INFO, "CameraBoardManager::setCameraReadSecAction: no action");
		return eOperationError;
	}

	m_pOperation = 0;
	m_nCameraAction = action;

	int liRetVal = m_synchSem.release();
	if ( liRetVal == SEM_FAILURE )
	{
		log(LOG_ERR, "CameraBoardManager::setCameraReadSecAction: unable to unlock thread synch semaphore");
		return eOperationError;
	}
	else
	{
		log(LOG_INFO, "CameraBoardManager::setCameraReadSecAction: operation <%d>", m_nCameraAction);
	}

	return eOperationRunning;
}

void CameraBoardManager::getErrorList(vector<string>* vErrors, uint8_t errorLevel)
{
	if ( ! usbBoardLinkSingleton()->isUSBErrEnabled() )	return;

    usbBoardLinkSingleton()->m_pUSBErr->getErrorList(vErrors, errorLevel);
}

void CameraBoardManager::clearErrorList()
{
	if ( ! usbBoardLinkSingleton()->isUSBErrEnabled() )	return;

	usbBoardLinkSingleton()->m_pUSBErr->clearAllErrors();
}

int CameraBoardManager::updateErrorsTime(uint64_t currentMsec, tm* currentTime)
{
	if ( ! usbBoardLinkSingleton()->isUSBErrEnabled() )		return -1;

	return usbBoardLinkSingleton()->m_pUSBErr->updateErrorsTime(currentMsec, currentTime);
}

int CameraBoardManager::requestDecodeBarcode(int liSectionId, int liSlotId, bool bTakePicture)
{
	if ( liSectionId >= SCT_NUM_TOT_SECTIONS )				return -1;
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )					return -1;

	m_vDecodedCodes[liSectionId][liSlotId] = SELECTED_CODE;
	if ( bTakePicture )		m_vPictures[liSectionId][liSlotId].assign(1, SELECTED_PICTURE);

	if ( setCameraReadSecAction(eCameraActionDecodeBC) != eOperationRunning )
	{
		return -1;
	}

	return 0;
}

int CameraBoardManager::requestDecodeDataMatrix(int liSectionId, int liSlotId, bool bTakePicture)
{
	if ( liSectionId >= SCT_NUM_TOT_SECTIONS )				return -1;
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )					return -1;

	m_vDecodedCodes[liSectionId][liSlotId] = SELECTED_CODE;
	if ( bTakePicture )		m_vPictures[liSectionId][liSlotId].assign(1, SELECTED_PICTURE);

	if ( setCameraReadSecAction(eCameraActionDecodeDM) != eOperationRunning )
	{
		return -1;
	}

	return 0;
}

int CameraBoardManager::requestCheckSamplePresence(int liWellNum, int liSectionId, int liSlotId, bool bTakePicture)
{
	if ( liSectionId >= SCT_NUM_TOT_SECTIONS )				return -1;
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )					return -1;
	if ( liWellNum != WELL_0 && liWellNum != WELL_3 )		return -1;

	if ( bTakePicture )		m_vPictures[liSectionId][liSlotId].assign(1, SELECTED_PICTURE);
	else					m_vPictures[liSectionId][liSlotId].assign(1, DECODE_BUT_NOT_GET_PIC);

	int liRes = 0;
	if ( ! liWellNum )	liRes = setCameraReadSecAction(eCameraActionCheckW0);
	else				liRes = setCameraReadSecAction(eCameraActionCheckW3);

	if ( liRes != eOperationRunning )
	{
		return -1;
	}

	return 0;
}

int CameraBoardManager::requestCheckSprPresence(int liSectionId, int liSlotId, bool bTakePicture)
{
	if ( liSectionId >= SCT_NUM_TOT_SECTIONS )				return -1;
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )					return -1;

	m_vDecodedCodes[liSectionId][liSlotId] = SELECTED_CODE;
	if ( bTakePicture )		m_vPictures[liSectionId][liSlotId].assign(1, SELECTED_PICTURE);

	int liRes = setCameraReadSecAction(eCameraActionCheckSpr);
	if ( liRes != eOperationRunning )
	{
		return -1;
	}

	return 0;
}

void CameraBoardManager::getDecodedCode(string& strBarcode, vector<unsigned char>& vImage, int liSectionId, int liSlotId)
{
	if ( liSectionId >= SCT_NUM_TOT_SECTIONS )				return;
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )					return;

	strBarcode.assign(m_vDecodedCodes[liSectionId][liSlotId]);
	m_vDecodedCodes[liSectionId][liSlotId].clear();
	// If the image is not requested, the vector is empty
	vImage = m_vPictures[liSectionId][liSlotId];
	m_vPictures[liSectionId][liSlotId].clear();
}

void CameraBoardManager::getSamplePresenceResult(float& fResult, vector<unsigned char>& vImage, int liSectionId, int liSlotId)
{
	if ( liSectionId >= SCT_NUM_TOT_SECTIONS )				return;
	if ( liSlotId >= SCT_NUM_TOT_SLOTS )					return;

	vImage = m_vPictures[liSectionId][liSlotId];
	fResult = m_fAlgorithmOutput[liSectionId][liSlotId];
	// clear variables used
	m_fAlgorithmOutput[liSectionId][liSlotId] = -1.0f;
	m_vPictures[liSectionId][liSlotId].clear();

	return;
}

void CameraBoardManager::beforeWorkerThread()
{
    log(LOG_DEBUG, "CameraBoardManager:: thread started");
}

int CameraBoardManager::workerThread()
{
	int8_t cResult;

	while ( isRunning() )
	{
		int liSynchRet = m_synchSem.wait();
		if( liSynchRet != SEM_SUCCESS )
		{
			log(LOG_WARNING, "CameraBoardManager::th: unlocked but wait returned %d", liSynchRet);
			continue;
		}

		if ( m_nCameraAction == eCameraActionNone )
		{
			log(LOG_ERR, "CameraBoardManager::th: BAD unlocked without action");
			continue;
		}
		log(LOG_INFO, "CameraBoardManager::th: operation <%d>", m_nCameraAction);

		if  ( m_nCameraAction == eCameraActionReInit )
		{
			changeDeviceStatus(eIdCamera, eStatusInit);
			m_mutexCamera.lock();
			// !!! TODO reset CAMERA if possible do something and change action to Init
			m_mutexCamera.unlock();
			m_nCameraAction = eCameraActionInit;
		}

		if  ( m_nCameraAction == eCameraActionInit )
		{
			changeDeviceStatus(eIdCamera, eStatusInit);

			// clean Camera Errors
			clearErrorList();

			string strFirmware("");
			usbBoardLinkSingleton()->m_pCR8062->m_Camera.getFWversion(strFirmware);
			MainExecutor::getInstance()->m_instrumentInfo.setCameraApplicationRelease(strFirmware);

			// end of initialization procedure
			log(LOG_INFO, "CameraBoardManager:: initialization completed !");

			if(m_pOperation)
			{
				m_pOperation->notifyCompleted(CAMERA_ID, true);
			}
            m_pOperation = nullptr;
            m_nCameraAction = eCameraActionNone;
			changeDeviceStatus(eIdCamera, eStatusIdle);
		}
		else if ( m_nCameraAction == eCameraActionExecute )
		{
			if(m_pOperation)
			{
				m_mutexCamera.lock();

				log(LOG_INFO, "CameraBoardManager::workerThread-->execute on Camera");

				//executes the actions related to the incoming command
				while ( m_pOperation->executeCmd(CAMERA_ID) == 0 ) {};

				m_mutexCamera.unlock();
			}
			m_nCameraAction = eCameraActionNone;
			m_pOperation = nullptr;
		}
		else if ( m_nCameraAction == eCameraActionDecodeBC )
		{
			m_mutexCamera.lock();
			// section reading requested by Protocol Thread
			int liSection = -1;
			int liSlot = -1;
			bool bGetPicture = false;
			for ( int i = 0; i != SCT_NUM_TOT_SECTIONS; ++i )
			{
				for ( int j = 0; j != SCT_NUM_TOT_SLOTS; ++j )
				{
					if ( ! m_vDecodedCodes[i][j].compare(SELECTED_CODE) )
					{
						liSection = i;
						liSlot = j;
						if ( m_vPictures[i][j].size() == 1 && m_vPictures[i][j].front() == SELECTED_PICTURE )
						{
							bGetPicture = true;
						}
					}
				}
			}
			if ( liSlot != -1 && liSection != -1 )
			{
				decodeBarcode(liSlot, liSection, bGetPicture);
			}
			m_mutexCamera.unlock();
			m_nCameraAction = eCameraActionNone;
		}
		else if ( m_nCameraAction == eCameraActionDecodeDM )
		{
			m_mutexCamera.lock();
			// section reading requested by Protocol Thread
			int liSection = -1;
			int liSlot = -1;
			bool bGetPicture = false;
			for ( int i = 0; i != SCT_NUM_TOT_SECTIONS; ++i )
			{
				for ( int j = 0; j != SCT_NUM_TOT_SLOTS; ++j )
				{
					if ( ! m_vDecodedCodes[i][j].compare(SELECTED_CODE) )
					{
						liSection = i;
						liSlot = j;
						if ( m_vPictures[i][j].size() == 1 && m_vPictures[i][j].front() == SELECTED_PICTURE )
						{
							bGetPicture = true;
						}
					}
				}
			}
			if ( liSlot != -1 && liSection != -1 )
			{
				decodeDataMatrix(liSlot, liSection, bGetPicture);
			}
			m_mutexCamera.unlock();
			m_nCameraAction = eCameraActionNone;
		}
		else if ( m_nCameraAction == eCameraActionCheckW0 )
		{
			m_mutexCamera.lock();
			// section reading requested by Protocol Thread
			int liSection = -1;
			int liSlot = -1;
			bool bStorePicture = false;
			for ( int i = 0; i != SCT_NUM_TOT_SECTIONS; ++i )
			{
				for ( int j = 0; j != SCT_NUM_TOT_SLOTS; ++j )
				{
					// If is the correct one the vector is only --> '_'
					if ( (m_vPictures[i][j].size() == 1 && m_vPictures[i][j].front() == SELECTED_PICTURE)  )
					{
						liSection = i;
						liSlot = j;
						bStorePicture = true;
					}
					else if ( m_vPictures[i][j].size() == 1 && m_vPictures[i][j].front() == DECODE_BUT_NOT_GET_PIC )
					{
						liSection = i;
						liSlot = j;
					}
				}
			}
			if ( liSlot != -1 && liSection != -1 )
			{
				checkSamplePresence(liSlot, liSection, WELL_0, bStorePicture);
			}
			m_mutexCamera.unlock();
			m_nCameraAction = eCameraActionNone;
		}
		else if ( m_nCameraAction == eCameraActionCheckW3 )
		{
			m_mutexCamera.lock();
			// section reading requested by Protocol Thread
			int liSection = -1;
			int liSlot = -1;
			bool bStorePicture = false;
			for ( int i = 0; i != SCT_NUM_TOT_SECTIONS; ++i )
			{
				for ( int j = 0; j != SCT_NUM_TOT_SLOTS; ++j )
				{
					// If is the correct one the vector is only --> '_'
					if ( (m_vPictures[i][j].size() == 1 && m_vPictures[i][j].front() == SELECTED_PICTURE)  )
					{
						liSection = i;
						liSlot = j;
						bStorePicture = true;
					}
					else if ( m_vPictures[i][j].size() == 1 && m_vPictures[i][j].front() == DECODE_BUT_NOT_GET_PIC )
					{
						liSection = i;
						liSlot = j;
					}
				}
			}
			if ( liSlot != -1 && liSection != -1 )
			{
				checkSamplePresence(liSlot, liSection, WELL_3, bStorePicture);
			}
			m_mutexCamera.unlock();
			m_nCameraAction = eCameraActionNone;
		}
		else if ( m_nCameraAction == eCameraActionCheckSpr )
		{
			m_mutexCamera.lock();
			// section reading requested by Protocol Thread
			int liSection = -1;
			int liSlot = -1;
			bool bGetPicture = false;
			for ( int i = 0; i != SCT_NUM_TOT_SECTIONS; ++i )
			{
				for ( int j = 0; j != SCT_NUM_TOT_SLOTS; ++j )
				{
					if ( ! m_vDecodedCodes[i][j].compare(SELECTED_CODE) )
					{
						liSection = i;
						liSlot = j;
						if ( m_vPictures[i][j].size() == 1 && m_vPictures[i][j].front() == SELECTED_PICTURE )
						{
							bGetPicture = true;
						}
					}
				}
			}
			if ( liSlot != -1 && liSection != -1 )
			{
				checkSprPresence(liSlot, liSection, bGetPicture);
			}
			m_mutexCamera.unlock();
			m_nCameraAction = eCameraActionNone;
		}
		else if ( m_nCameraAction == eCameraActionCmd )
		{
			if(m_pOperation)
			{
				m_mutexCamera.lock();

				//executes the actions related to the incoming command
				m_pOperation->executeCmd(CAMERA_ID);
				m_mutexCamera.unlock();

				//compile and send the answer
				cResult = m_pOperation->compileSendOutCmd();

				/* ********************************************************************************************
				* Remove the operation from the OperationManager list
				* ********************************************************************************************
				*/
				if ( cResult == eOperationEnabledForDeath )
				{
					/* ********************************************************************************************
					 * Access the state machine
					 * ********************************************************************************************
					 */
					m_pOperation->restoreStateMachine();

					/* ********************************************************************************************
					 * Reset variables and remove the common operation
					 * ********************************************************************************************
					 */
					m_nCameraAction = eCameraActionNone;
					operationSingleton()->removeOperation(m_pOperation);
					m_pOperation = nullptr;

					log(LOG_INFO, "CameraBoardManager::workerThread-->eOperationEnabledForDeath");
				}
				else
				{
					log(LOG_INFO, "CameraBoardManager::workerThread-->without operation removal");
				}
			}
			m_nCameraAction = eCameraActionNone;
			m_pOperation = nullptr;
		}		
		else if ( m_nCameraAction  == eCameraActionFwUpdate )
		{
			log(LOG_INFO, "SectionBoardManager::workerThread-->wait FW UPD on camera");

			FwUpdateInfo* pInfo;
			pInfo = getUpdateInfo(m_pOperation);
			if ( pInfo != nullptr )
			{
				// Camera uses crc16 and from SSW we got crc32. For this reason we can't pass it
				// to the function. Anyway, if the update action is set, this means that the UPG
				// file has passed the crc32 check, so it is not corrupted.
				bool bRes = usbBoardLinkSingleton()->m_pCR8062->updateCR8062FW(pInfo->m_strSourcePath, pInfo->m_lLength, pInfo->m_strNewFwVers);
				setUpdateOutcome(pInfo, bRes);

				if ( bRes && (pInfo->m_eFwType == eApplicative) )
				{
					infoSingleton()->setCameraApplicationRelease(pInfo->m_strNewFwVers);
				}
			}

			m_nCameraAction = eCameraActionNone;
			m_pOperation = nullptr;
		}

		log(LOG_INFO, "CameraBoardManager::workerThread-->done");
	}
	return 0;
}

void CameraBoardManager::afterWorkerThread()
{
	m_synchSem.closeAndUnlink();
    log(LOG_DEBUG, "CameraBoardManager::th: stopped");
}

FwUpdateInfo* CameraBoardManager::getUpdateInfo(Operation* pOp)
{
	if ( pOp == nullptr )	return nullptr;

	OpFwUpdate* pUpdOp = (OpFwUpdate*)pOp;
	return pUpdOp->getLastDeviceInfoForUpd();
}

int CameraBoardManager::setUpdateOutcome(FwUpdateInfo* pInfo, bool bRes)
{
	if ( pInfo == nullptr )	return -1;

	OpFwUpdate* pUpdOp = (OpFwUpdate*)m_pOperation;
	return pUpdOp->writeUpdOutcomeLog(pInfo->m_eId, pInfo->m_eFwType, bRes);
}

int CameraBoardManager::decodeBarcode(int liSlot, int liSection, bool bGetPicture)
{
	uint8_t ucSlot = liSlot+1; // slot1 has to be 1 here, not 0
	ucSlot = ( liSection == SCT_A_ID ) ? ucSlot : ucSlot + SCT_NUM_TOT_SLOTS;
	int liRes = usbBoardLinkSingleton()->m_pCR8062->uploadBatchFile(ucSlot, eBatchTarget::eLinearBarcode);
	if ( liRes )	recoverCamera();

	liRes = usbReaderSingleton()->getLinearBarcode(m_vDecodedCodes[liSection][liSlot]);
	if ( liRes != ERR_NONE )
	{
		// Failed to decode
		m_vDecodedCodes[liSection][liSlot] = "?";
		if ( liRes == -1 )
		{
			recoverCamera();
		}
	}

	if ( bGetPicture )
	{
		structInfoImage info;
		m_vPictures[liSection][liSlot].clear();
		liRes = usbCameraSingleton()->getPicture(m_vPictures[liSection][liSlot], &info);
		usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&info, m_vPictures[liSection][liSlot]);
	}

	return liRes;
}

int CameraBoardManager::decodeDataMatrix(int liSlot, int liSection, bool bGetPicture)
{
	uint8_t ucSlot = liSlot+1; // slot1 has to be 1 here, not 0
	ucSlot = ( liSection == SCT_A_ID ) ? ucSlot : ucSlot + SCT_NUM_TOT_SLOTS;
	int liRes = usbBoardLinkSingleton()->m_pCR8062->uploadBatchFile(ucSlot, eBatchTarget::eDataMatrix);
	if ( liRes )	recoverCamera();

	int liOldDecodingTimeOut = usbReaderSingleton()->getDecodingTimeoutMs();
	liRes = usbReaderSingleton()->_debug_DM_read(m_vDecodedCodes[liSection][liSlot], liSection);
	if ( liRes != ERR_NONE )
	{
		// Failed to decode
		m_vDecodedCodes[liSection][liSlot] = "______"; // code not decoded, but we don't know yet if is
													   // because the spr absence
		if ( liRes == -1 )
		{
			recoverCamera();
		}
	}
	int liNewDecodingTimeOut = usbReaderSingleton()->getDecodingTimeoutMs();
	if ( liNewDecodingTimeOut != liOldDecodingTimeOut )
	{
		usbReaderSingleton()->setDecodingTimeMsec(liOldDecodingTimeOut);
	}

	if ( bGetPicture )
	{
		structInfoImage info;
		m_vPictures[liSection][liSlot].clear();
		liRes = usbCameraSingleton()->getPicture(m_vPictures[liSection][liSlot], &info);
		usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&info, m_vPictures[liSection][liSlot]);
	}

	return liRes;
}

int CameraBoardManager::checkSprPresence(int liSlot, int liSection, bool bGetPicture)
{
	uint8_t ucSlot = liSlot+1; // slot1 has to be 1 here, not 0
	ucSlot = ( liSection == SCT_A_ID ) ? ucSlot : ucSlot + SCT_NUM_TOT_SLOTS;
	int liRes = usbBoardLinkSingleton()->m_pCR8062->uploadBatchFile(ucSlot, eBatchTarget::eConeAbsence);
	if ( liRes )	recoverCamera();

	structInfoImage info;
	m_vPictures[liSection][liSlot].clear();
	liRes = usbCameraSingleton()->getPicture(m_vPictures[liSection][liSlot], &info);
	if ( liRes != ERR_NONE )
	{
		m_pLogger->log(LOG_ERR, "OpReadSec::executeSprRead: unable to get picture");
		recoverCamera();
		if ( bGetPicture )
		{
			// Adding this fake image the user understands something went wrong
			m_vPictures[liSection][liSlot].resize(40000);
			for ( int i = 0; i != 40000; i+=2 )
			{
				m_vPictures[liSection][liSlot].at(i) = 0;
				m_vPictures[liSection][liSlot].at(i+1) = 255;
			}
			structInfoImage info;
			info.liSize = 40000;
			info.eFormat = eRAW;
			info.liWidth = 200;
			info.liHeight = 200;
			usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&info, m_vPictures[liSection][liSlot]);
		}
		m_vDecodedCodes[liSection][liSlot].assign(CODE_PRESENT_MSG);
		return -1;
	}

	vector<unsigned char> vImage(m_vPictures[liSection][liSlot]);
	if ( int(vImage.size()) == (info.liWidth*info.liHeight) )
	{
		usbBoardLinkSingleton()->m_pCR8062->ruotate180degree<unsigned char>(vImage.data(), info.liWidth, info.liHeight);
		SprDetectionLink detector;
		detector.init(vImage.data(), m_pLogger, info.liWidth, info.liHeight);
		bool bIsPresent = false;
		detector.perform(bIsPresent);
		if ( ! bIsPresent )		m_vDecodedCodes[liSection][liSlot].assign(CODE_NOT_PRESENT_MSG);
		else					m_vDecodedCodes[liSection][liSlot].assign(CODE_PRESENT_MSG);
	}
	else
	{
		m_vDecodedCodes[liSection][liSlot].assign(CODE_NOT_DECODED_MSG);
	}

	// remove picture if not requested
	if ( ! bGetPicture )	m_vPictures[liSection][liSlot].clear();
	else					usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&info, m_vPictures[liSection][liSlot]);

	return liRes;
}

int CameraBoardManager::checkSamplePresence(int liSlot, int liSection, int liWell, bool bStorePic)
{
	if ( liSection >= SCT_NUM_TOT_SECTIONS )		return -1;
	if ( liSlot >= SCT_NUM_TOT_SLOTS )				return -1;
	if ( liWell != WELL_0 && liWell != WELL_3 )		return -1;

	uint8_t ucSlot = liSlot+1; // slot1 has to be 1 here, not 0
	ucSlot = ( liSection == SCT_A_ID ) ? ucSlot : ucSlot + SCT_NUM_TOT_SLOTS;
	eBatchTarget eTarget = ( liWell == WELL_0 )	? eBatchTarget::eSampleX0 : eBatchTarget::eSampleX3;
	int liRes = usbBoardLinkSingleton()->m_pCR8062->uploadBatchFile(ucSlot, eTarget);
	if ( liRes )	recoverCamera();

	structInfoImage info;
	m_vPictures[liSection][liSlot].clear();
	liRes = usbCameraSingleton()->getPicture(m_vPictures[liSection][liSlot], &info);
	if ( liRes != ERR_NONE )
	{
		m_pLogger->log(LOG_ERR, "CameraBoardManager::checkSamplePresence: unable to get picture");
		recoverCamera();
		if ( bStorePic )
		{
			// Adding this fake image the user understands something went wrong
			m_vPictures[liSection][liSlot].resize(40000);
			for ( int i = 0; i != 40000; i+=2 )
			{
				m_vPictures[liSection][liSlot].at(i) = 0;
				m_vPictures[liSection][liSlot].at(i+1) = 255;
			}
			structInfoImage info;
			info.liSize = 40000;
			info.eFormat = eRAW;
			info.liWidth = 200;
			info.liHeight = 200;
			usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&info, m_vPictures[liSection][liSlot]);
		}
		return -1;
	}

	vector<uint32_t> vImage(m_vPictures[liSection][liSlot].begin(), m_vPictures[liSection][liSlot].end());
	usbBoardLinkSingleton()->m_pCR8062->ruotate180degree<uint32_t>(vImage.data(), info.liWidth, info.liHeight);
	for ( auto it = vImage.begin(); it != vImage.end(); ++it )
	{
		(*it) *= DECIMAL_DIGITS_FACTOR;
	}

	SampleDetectionLink detector;
	int liSlotId = liSlot + 1; // slot1 has to be 1 here, not 0
	liSlotId = ( liSection == SCT_A_ID ) ? liSlotId : liSlotId + SCT_NUM_TOT_SLOTS;
	bool bRes = detector.initAlgo(m_pLogger, liSlotId, liWell);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "CameraBoardManager::checkSamplePresence: unable to init sample detection algorithm");
		if ( ! bStorePic )	m_vPictures[liSection][liSlot].clear();
		else				usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&info, m_vPictures[liSection][liSlot]);
		return -1;
	}

	float fResult = 0.0f;
	// TODO handle m_vSettings Hor/ver for the algorithm
	bRes = detector.sampleDetectionAlgorithm(vImage.data(), info.liWidth, fResult);
	if ( ! bRes )
	{
		if ( ! bStorePic )	m_vPictures[liSection][liSlot].clear();
		else				usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&info, m_vPictures[liSection][liSlot]);
		m_pLogger->log(LOG_ERR, "OpReadSec::executeStripRead: unable to apply sample detection algorithm");
		return -1;
	}

	if ( fResult < 0.0f )	fResult = 0.0f; // it has to be non negative
	m_fAlgorithmOutput[liSection][liSlot] = fResult;

	// add only now so that the algorithm is applied on a clean image
	if ( bStorePic )		usbBoardLinkSingleton()->m_pCR8062->addBMPHeader(&info, m_vPictures[liSection][liSlot]);
	return 0;
}

void CameraBoardManager::recoverCamera()
{
	msleep(4000);
	bool bRes = usbBoardLinkSingleton()->m_pCR8062->bringToLife();
	if ( bRes )
	{
		m_pLogger->log(LOG_WARNING, "OpReadSec::recoverCamera: recover action performed");
	}
	else
	{
		m_pLogger->log(LOG_WARNING, "OpReadSec::recoverCamera: unable to recover camera");
	}
}
