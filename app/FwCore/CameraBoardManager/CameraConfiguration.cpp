/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CameraConfiguration.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CaneraConfiguration class.
 @details

 ****************************************************************************
*/

#include "CameraConfiguration.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string.h>

CameraConfiguration::CameraConfiguration(string strFileName, string strFileNameDefault)
{
	m_strFileName.assign(strFileName);
    m_strFileNameDefault.assign(strFileNameDefault);
	m_mapItems.clear();
	m_bConfigOk = false;
}

CameraConfiguration::~CameraConfiguration()
{

}

bool CameraConfiguration::initCameraConfiguration()
{
    if ( m_strFileName.empty() ||
         m_strFileNameDefault.empty() )	return false;

	m_mapItems.clear();

    bool bDefaultFile = false;

    // try to read the cal file: if it's not good or it does not exist (new installation) -> open the default one
	ifstream file;
	file.open(m_strFileName);
	if ( ! file.good() )
	{
        file.close();
        file.open(m_strFileNameDefault);
        if ( ! file.good() )
        {
            file.close();
            return false;
        }
        bDefaultFile = true;
	}

	int liLinesNum = count(istreambuf_iterator<char>(file), istreambuf_iterator<char>(), '\n');
	if ( liLinesNum == 0 )
    {
		if ( bDefaultFile )
        {
            // the default file is empty -> fail
            file.close();
            return false;
        }
        else
        {
            // try the default one ...
            file.close();
            file.open(m_strFileNameDefault);
			if ( ! file.good() )	return false;

            bDefaultFile = true;
            // repeat the count as the target file has changed
            liLinesNum = count(istreambuf_iterator<char>(file), istreambuf_iterator<char>(), '\n');
        }
    }

	file.clear();
	file.seekg(0, ios::beg);

	vector<string> strConf;
	strConf.resize(liLinesNum+1);
	string strTmp("");

	// Save all the lines in a different string
	int liItemsNum = 0;
	while ( getline(file, strTmp) )
	{
		// Remove white spaces
		strTmp.erase(remove(strTmp.begin(), strTmp.end(), ' '), strTmp.end());

		// Do not consider if empty
		if ( strTmp.empty() )		continue;
		// Remove comments
		if ( strTmp[0] == '#' )		continue;

		strConf[liItemsNum] = strTmp;
		liItemsNum++;
	}

	file.close();
	strConf.resize(liItemsNum);

	for ( int i = 0; i != liItemsNum; i++ )
	{
		bool bAssignmentFound = false;
		int liIdx = 0;
		string strItem("");
		string strTmpX("");
		string strTmpy("");
		string strTmpWidth("");
		string strTmpHeight("");
		eBatchTarget eTarget;

		for ( char c : strConf[i] )
		{
			if ( ! bAssignmentFound )
			{
				if ( c != '=' )
				{
					strItem.push_back(c);
				}
				else
				{
					// If the element is already in the map, skip it
					if ( m_mapItems.find(strItem) != m_mapItems.end() )		break;

					if		( strItem.find(TARGET_NAME_SAMPLE0) != string::npos )		eTarget = eBatchTarget::eSampleX0;
					else if ( strItem.find(TARGET_NAME_SAMPLE3) != string::npos )		eTarget = eBatchTarget::eSampleX3;
					else if ( strItem.find(TARGET_NAME_BC)		!= string::npos )		eTarget = eBatchTarget::eLinearBarcode;
					else if ( strItem.find(TARGET_NAME_BC_ABS)	!= string::npos )		eTarget = eBatchTarget::eConeAbsence;
					else if ( strItem.find(TARGET_NAME_DATAMATRIX) != string::npos )	eTarget = eBatchTarget::eDataMatrix;
					else continue; // name not recognised

					bAssignmentFound = true;
				}
			}
			else
			{
				if ( c == '#' )
				{
					liIdx++;
					continue;
				}

				switch ( liIdx )
				{
					case 0:	strTmpX.push_back(c);	break;

					case 1:	strTmpy.push_back(c);	break;

					case 2:	strTmpWidth.push_back(c);	break;

					case 3: strTmpHeight.push_back(c);	break;

					default:
						continue;
					break;
				}
			}
		}
		// Add element to map only if it is a new one
		if ( strTmpX.empty() )	continue;
		structRect rect{eTarget, stoi(strTmpX), stoi(strTmpy), stoi(strTmpWidth), stoi(strTmpHeight)};
		m_mapItems[strItem] = rect;
	}

	m_bConfigOk = true;
	return true;
}

//bool CameraConfiguration::updateCameraConfig(const vector<structCameraConf>& vNewConfItem, eConfigType eType)
//{
//	// Update list of calibration items
//	if ( ! m_bConfigOk )
//	{
//		initCameraConfiguration();
//	}

//	for ( size_t i = 0; i != vNewConfItem.size(); i++ )
//	{
//		if ( m_mapItems.find(vNewConfItem[i].strItem) == m_mapItems.end() )
//		{
//			return false; // Missing item
//		}

//		structRect rect{ eType, stoi(vNewConfItem[i].strX0), stoi(vNewConfItem[i].strY0),
//						 stoi(vNewConfItem[i].strWidth), stoi(vNewConfItem[i].strHeight)};
//		m_mapItems.at(vNewConfItem[i].strItem) = rect;
//	}


//	// If the item exists Update calibration file otherwise add it to both
//	ofstream ofile;
//	ofile.open(m_strFileName, std::ofstream::out | std::ofstream::trunc); // trunc to delete the content of the file when opening
//	if ( ! ofile.good() )
//	{
//		return false;
//	}

//	for ( const auto &it : m_mapItems )
//	{
//		ofile << it.first << " = " << it.second.x << "#" << it.second.y << "#"
//			  << it.second.width << "#" << it.second.height << endl;
//	}
//	ofile.close();

//	return true;
//}

bool CameraConfiguration::updateCameraConfig(unordered_map<string, structRect>& mSubSet)
{
	// Update list of calibration items
	if ( ! m_bConfigOk )
	{
		initCameraConfiguration();
	}

	for ( const auto &it : mSubSet )
	{
		if ( m_mapItems.find(it.first) == m_mapItems.end() )
		{
			return false; // Missing item
		}
		m_mapItems.at(it.first) = it.second;
	}

	// If the item exists Update calibration file otherwise add it to both
	ofstream ofile;
	ofile.open(m_strFileName, std::ofstream::out | std::ofstream::trunc); // trunc to delete the content of the file when opening
	if ( ! ofile.good() )
	{
		return false;
	}

	for ( const auto &it : m_mapItems )
	{
		ofile << it.first << " = " << it.second.x << "#" << it.second.y << "#"
			  << it.second.width << "#" << it.second.height << endl;
	}
	ofile.close();

	return true;
}

bool CameraConfiguration::updateCameraConfigFile()
{
	ofstream ofile;
	ofile.open(m_strFileName, std::ofstream::out | std::ofstream::trunc); // trunc to delete the content of the file when opening
	if ( ! ofile.good() )
	{
		return false;
	}

	for ( const auto &it : m_mapItems )
	{
		ofile << it.first << " = " << it.second.x << "#" << it.second.y << "#"
			  << it.second.width << "#" << it.second.height << endl;
	}
	ofile.close();

	return true;
}

bool CameraConfiguration::getCameraConfig(unordered_map<string, structRect>& mConfig)
{
	if ( ! m_bConfigOk )	return false;

	mConfig = m_mapItems;

	return true;
}

bool CameraConfiguration::getItemValue(const string& strItemName, structRect& rect)
{
	if ( ! m_bConfigOk )	return false;

	if ( m_mapItems.find(strItemName) == m_mapItems.end() )
	{
		return false;
	}

	rect = m_mapItems.at(strItemName);
	return true;
}

bool CameraConfiguration::setItemValue(const string& strItemName, const structRect& rect)
{
	if ( ! m_bConfigOk )	return false;

	if ( m_mapItems.find(strItemName) == m_mapItems.end() )
	{
		return false;
	}

	m_mapItems.at(strItemName) = rect;
	return true;
}

bool CameraConfiguration::getLabelList(vector<string>& vList)
{
	if ( ! m_bConfigOk )	return false;

	vList.clear();

	for ( const auto &it : m_mapItems )
	{
		vList.push_back(it.first);
	}

	return true;
}

bool CameraConfiguration::isLabelValid(string& strLabel)
{
	if ( ! m_bConfigOk )	return false;
	if ( strLabel.empty() )	return false;

	if ( m_mapItems.find(strLabel) == m_mapItems.end() )
	{
		return false;
	}

	return true;
}
