/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionNTCParameters.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionNTCParameters class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONNTCPARAMETERS_H
#define SECTIONNTCPARAMETERS_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <Mutex.h>

typedef enum
{
	eNoExpFormula		='X',
	eExpFormula			='0',
	eSteinhartFormula	='1'

} enumTypeFormulaNTCParams;

/*!
 * @class SectionNTCParameters
 * @brief The SectionNTCParameters class
 */
class SectionNTCParameters
{
	public:
		SectionNTCParameters();
		virtual ~SectionNTCParameters();

		int			setNTCParameters( uint8_t * pubParamBuff );
		void		printNTCParameters( void );

		/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		+ ConvertNTCValue - method to convert NTC ADC value to temperature					+
		++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
		/*!************************************************************************/
		/*in case of Exponential formula [F = ‘0’]:
		P1 = reference temperature of NTC given in hundredths of °K (°K *100)
		P2 = B value of NTC
		P3 = resistance of NTC at temperature specified by P1 (in ohm)
		In order to get the desired temperature (in °K degrees), given the A/D converter value AD_val,
		the following formula should be applied:
		curr_R = (R * (AD_val / D) ) / ((Vr / 1000) – (AD_val / D))
		temp = P2 / ((P2 / (P1 / 100)) + log(curr_R / P3))

		NOTE2: in case of Steinhart formula [F = ‘1’]:
		P1 = A coefficient of NTC multiplied by 10^9
		P2 = B coefficient of NTC multiplied by 10^10
		P3 = C coefficient of NTC multiplied by 10^13
		In order to get the desired temperature (in °K degrees), given the A/D converter value AD_val,
		the following formula should be applied:
		curr_R = (R * (AD_val / D) ) / (Vr / 1000 - (AD_val / D))
		Inv_temp = A / 10^9 + (B / 10^10)*log(curr_R) + (C / 10^13)*(log(curr_R)^3)
		temp = 1 / Inv_temp;*/
		/*!************************************************************************/
		double convertNTCValue(uint16_t usADCValue);

    private:

        enumTypeFormulaNTCParams	m_eFormulaType;
        uint16_t				m_usVrXRef;     // reference voltage of A/D converter given  in mV, 1 < X < 4
        uint16_t				m_usDivisor;    // divisor to be applied for the converting formula, 1 < x < 4
        uint32_t				m_uiResistance; // series resistance of NTC circuit given in ohm, 1 < x < 8
        uint32_t				m_uiParam1NTC;  // 1st  parameter of NTC, 1 < x < 8
        uint32_t				m_uiParam2NTC;  // 2nd  parameter of NTC, 1 < x < 8
        uint32_t				m_uiParam3NTC;  // 3rd  parameter of NTC, 1 < x < 8

        float					m_fVrXRef;      // reference voltage of A/D converter given  in mV, 1 < X < 4
        float					m_fDivisor;     // divisor to be applied for the converting formula, 1 < x < 4
        float					m_fResistance;  // series resistance of NTC circuit given in ohm, 1 < x < 8

        // ////////////////////////////////
        // exponential formula
        double					m_fReftemp;
        double					m_fCoeff;
        double					m_fResRef;

        // /////////////////////
        // Steinhart formula
        double					m_fParamA;
        double					m_fParamB;
        double					m_fParamC;

        Mutex					m_Mutex;	//to prevent operation to the same variables

};

#endif // SECTIONNTCPARAMETERS_H
