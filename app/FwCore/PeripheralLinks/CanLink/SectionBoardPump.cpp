/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardPump.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionBoardPump class.
 @details

 ****************************************************************************
*/

#include "SectionBoardPump.h"

SectionBoardPump::SectionBoardPump(int section) : CanBoardBase(section)
{
	m_strDevice.assign(SECTION_PUMP_STRING);
}

SectionBoardPump::~SectionBoardPump()
{

}

int SectionBoardPump::addCommonMessage( void )
{
	if ( m_pLogger == NULL )
	{
		return -1;
	}
	else
	{
		m_CommonMessage.init( m_pLogger, this, SECTION_PUMP_CHAR );
	}

	return 0;
}

/*!***************************************************************************
 * COMMAND PUMP
 * ***************************************************************************/
int SectionBoardPump::getSensor( bool &bHomePump, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘S’ | ETX */
	pPayLoad[0] = SECTION_PUMP_CHAR;
	pPayLoad[1] = 'S';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | 'S' | f | state | ETX */
		uint8_t ubSensorState = pucAnsMsg[IDX_CAN_DATA_MSG] - 0x30;
		ubSensorState = ubSensorState & CANSTD_SENSOR_MASK_HOME_PUMP;
		if ( ubSensorState == 0x00 )
		{
			bHomePump = false;
		}
		else
		{
			bHomePump = true;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPump::getSensor-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPump::getSensor-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPump::setSerialNumber( string sSerialNumber, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	const char		*ptrStr = NULL;

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	uint32_t ulLenStr	= sSerialNumber.size();
	ptrStr		= sSerialNumber.c_str();

	/* STX | d | ‘B’ | S1 | S2 | S3 | S4 | S5 | S6 | ETX */
	pPayLoad[0] = SECTION_PUMP_CHAR;
	pPayLoad[1] = 'B';

	for ( uint32_t ulidx = 0; ulidx < ulLenStr; ulidx++ )
	{
		pPayLoad[ ulidx + 2 ] = ptrStr[ ulidx ];
	}

	int res = sendCanMsg( pPayLoad, 8, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘B’ | f |   ETX */
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPump::setSerialNumber-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPump::getSensor-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPump::getSerialNumber( string &sSerialNumber, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘b’ | ETX */
	pPayLoad[0] = SECTION_PUMP_CHAR;
	pPayLoad[1] = 'b';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘b’ | f | S1 | S2 | S3 | S4 | S5 | S6 | ETX */
		sSerialNumber = (char*)&pucAnsMsg[3];

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPump::getSerialNumber-->Performed %s", pucAnsMsg);
//		m_pLogger->log(LOG_INFO,"SectionBoardPump::getSerialNumber-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPump::getSerialNumber-->ERROR");
		return -1;
	}
	return 0;
}

int SectionBoardPump::aspirateVolume( uint8_t ubVolTable, uint8_t ubSpeedTable, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	//STX | d | ‘A’ | vol_table | speed_table | ETX
	pPayLoad[0] = SECTION_PUMP_CHAR;
	pPayLoad[1] = 'A';
	pPayLoad[2] = ubVolTable;
	pPayLoad[3] = ubSpeedTable;

	int res = sendCanMsg( pPayLoad, 4, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘A’ | f | sign | A1 | A2 | A3 | A4 | ETX
		uint32_t ulValue = (uint32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 1]);

		// Control the sign
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == '-')
		{
			ulValue *= -1LL;
		}
		else
		{
			ulValue *= 1LL;
		}
        m_pLogger->log(LOG_DEBUG,"SectionBoardPump::aspirateVolume-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPump::aspirateVolume-->ERROR");
		return -1;
	}
	return 0;


}

int SectionBoardPump::dispenseVolume( uint8_t ubVolTable, uint8_t ubSpeedTable, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	//STX | d | ‘D’ | vol_table | speed_table | ETX
	pPayLoad[0] = SECTION_PUMP_CHAR;
	pPayLoad[1] = 'D';
	pPayLoad[2] = ubVolTable;
	pPayLoad[3] = ubSpeedTable;

	int res = sendCanMsg( pPayLoad, 4, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘D’ | f | sign | A1 | A2 | A3 | A4 | ETX
		uint32_t ulValue = (uint32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 1]);

		// Control the sign
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == '-')
		{
			ulValue *= -1LL;
		}
		else
		{
			ulValue *= 1LL;
		}
        m_pLogger->log(LOG_DEBUG,"SectionBoardPump::dispenseVolume-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPump::dispenseVolume-->ERROR");
		return -1;
	}

	return 0;
}

/*!***************************************************************************
 * PACKETS MANAGER                                                           *
 * ***************************************************************************/
int SectionBoardPump::spreadMsg( uint8_t * pMsg )
{
	if ( pMsg == NULL )
	{
		return -1;
	}
	else
	{
        switch ( pMsg[IDX_CAN_TYPE_MSG] )
        {
            case 'Z':
            {
                char      pcDescription[SCTB_MAX_DESCRIPTION];
                decodeNotifyEvent( (char *)pMsg, pcDescription );

                m_pLogger->log(LOG_INFO, "SectionBoardPump::spreadMsg-->%s", pcDescription);
            }
            break;

            default:
            {
                sendToMsgQueueAnsw( (char *)pMsg, strlen((char*) pMsg) );
                m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardPump::spreadMsg-->sendToMsgQueueAnsw %s", (char *)pMsg);
            }
            break;
        }
	}

	return 0;
}

int SectionBoardPump::decodeAsynchronousMsg( uint8_t *pMsg )
{
	m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardPump::decodeAsynchronousMsg-->%s", pMsg);
	return 0;
}

bool SectionBoardPump::decodeNotifyEvent(char *pubMsg, char *pcDescription)
{
	uint8_t	ubFlag1 = pubMsg[2];
//	uint8_t	ubFlag2 = pubMsg[3];
	char strDescription[64];

	strDescription[0] = 0;
	if(pcDescription) *pcDescription = 0;

	// first test the default function that checks the errors defined in SECTION.err file
	if(CanBoardBase::decodeNotifyEvent(pubMsg, pcDescription))
	{
		return true;
	}

	// check other events from section
	switch ( ubFlag1 )
	{
		//.not applicable
		default:
		break;
	}

	bool bRet = true;

	if (strDescription[0] == 0  )
	{
		sprintf(strDescription, "%s -> (TBD)", pubMsg);
		bRet = false;
	}

	if(pcDescription) strcpy(pcDescription, strDescription);

	m_pLogger->log(LOG_INFO, "SectionBoardPIB::decodeNotifyEvent: %s", strDescription);

	return bRet;
}

