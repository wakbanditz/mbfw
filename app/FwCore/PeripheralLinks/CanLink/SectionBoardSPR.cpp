/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardSPR.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionBoardSPR class.
 @details

 ****************************************************************************
*/

#include "SectionBoardSPR.h"
#include "MainExecutor.h"

SectionBoardSPR::SectionBoardSPR(int section) : CanBoardBase(section)
{
	m_strDevice.assign(SECTION_SPR_STRING);
}

SectionBoardSPR::~SectionBoardSPR()
{

}

int SectionBoardSPR::addCommonMessage()
{
	if ( m_pLogger == NULL )
	{
		return -1;
	}
	else
	{
		m_CommonMessage.init( m_pLogger, this, SECTION_SPR_CHAR );
	}

	return 0;
}

/*!***************************************************************************
 * COMMAND SPR
 * ***************************************************************************/
int SectionBoardSPR::getSensor( bool &bHomeSPR, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘S’ | ETX */
	pPayLoad[0] = SECTION_SPR_CHAR;
	pPayLoad[1] = 'S';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | 'S' | f | state | ETX */
		uint8_t ubSensorState	= pucAnsMsg[IDX_CAN_DATA_MSG] - 0x30;
		uint8_t ubHome			= ubSensorState & CANSTD_SENSOR_MASK_HOME_SPR;
		if ( ubHome == 0x00 )
		{
			bHomeSPR = false;
		}
		else
		{
			bHomeSPR = true;
		}
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardSPR::getSensor-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardSPR::getSensor-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardSPR::getTemperature( float& fTemp, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 't' | ETX */
	pPayLoad[0] = SECTION_SPR_CHAR;
	pPayLoad[1] = 't';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘t’ | f | T1.1 | T1.2 | T1.3 | T1.4 | T2.1 | T2.2 | T2.3 | T2.4 | ETX */
		//When o = ‘2’ or o=’3’ the T2.1 T2.2 T2.3 T2.4  values are  invalid,
		uint8_t		ucidx = IDX_CAN_DATA_MSG;
		uint16_t	usT1 = getVal4x( (char *)&pucAnsMsg[ucidx] );

		fTemp = (float)usT1 / 100;

		m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardSPR::getTemperature--> %s, t1=%d, temp=%f", pucAnsMsg, usT1, fTemp);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardSPR::getTemperature-->ERROR");
		return -1;
	}
	return 0;
}

int SectionBoardSPR::setTemperature(uint16_t intTemp, uint16_t intLowTemp, uint16_t intHighTemp, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘T’ | T1| T2| T3 | T4 | Rl1 | Rl2 | Rl3 | Rh1 | Rh2 |Rh3 | ETX */
	pPayLoad[0] = SECTION_SPR_CHAR;
	pPayLoad[1] = 'T';

	// NOTE : the temperature values are already expressed as 0.01 degrees,
	// the low/high range are to be set as relative to target (while as parameters they are absolute values)
	uint16_t	usLowRange	= (intTemp - intLowTemp);
	uint16_t	usHighRange = (intHighTemp - intTemp);
	sprintf((char*)&pPayLoad[2], "%04X%03X%03X", intTemp, usLowRange, usHighRange);

	int res = sendCanMsg( pPayLoad, 12, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘T’ | f | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardSPR::setTemperature-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardSPR::setTemperature-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardSPR::resetSPRDriver( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	//STX | d | ‘D’  | ETX
	pPayLoad[0] = SECTION_SPR_CHAR;
	pPayLoad[1] = 'D';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘D’ |  f  | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardSPR::resetSPRDriver-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardSPR::resetSPRDriver-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardSPR::setRegister( uint8_t ubReg, uint8_t ubVal1, uint8_t ubVal2, uint8_t ubVal3, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	//STX | d | ‘B’ | register | V1.1 | V1.2 | V2.1 | V2.2 | V3.1 | V3.2 | ETX
	pPayLoad[0] = SECTION_SPR_CHAR;
	pPayLoad[1] = 'B';
	pPayLoad[2] = ubReg;

	sprintf((char*)&pPayLoad[3], "%02X%02X%02X", ubVal1, ubVal2, ubVal3);

	int res = sendCanMsg( pPayLoad, 9, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘D’ |  f  | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardSPR::setRegister-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardSPR::setRegister-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardSPR::getRegister( uint8_t ubReg, uint8_t &ubVal1, uint8_t &ubVal2, uint8_t &ubVal3, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	//STX | d | ‘b’ | register | ETX
	pPayLoad[0] = SECTION_SPR_CHAR;
	pPayLoad[1] = 'b';
	pPayLoad[2] = ubReg;

	int res = sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘b’  | V1.1 | V1.2 | V2.1 | V2.2 | V3.1 | V3.2 | ETX
		uint8_t	ucidx = 2;
		ubVal1 = (uint8_t)getVal2x((char *)&pucAnsMsg[ucidx]);
		ucidx = ucidx + 2;
		ubVal2 = (uint8_t)getVal2x((char *)&pucAnsMsg[ucidx]);
		ucidx = ucidx + 2;
		ubVal3 = (uint8_t)getVal2x((char *)&pucAnsMsg[ucidx]);

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardSPR::setRegister-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardSPR::setRegister-->ERROR");
		return -1;
	}

	return 0;
}


/*!***************************************************************************
 * PACKETS MANAGER                                                           *
 * ***************************************************************************/
int SectionBoardSPR::spreadMsg( uint8_t * pMsg )
{
	if ( pMsg == NULL )
	{
		return -1;
	}
	else
	{
        switch ( pMsg[IDX_CAN_TYPE_MSG] )
        {
            case 'Z':
            {
                char      pcDescription[SCTB_MAX_DESCRIPTION];
                decodeNotifyEvent( (char *)pMsg, pcDescription );

                m_pLogger->log(LOG_INFO, "SectionBoardSPR::spreadMsg-->%s", pcDescription);
            }
            break;

            default:
            {
                sendToMsgQueueAnsw( (char *)pMsg, strlen((char*) pMsg) );
                m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardSPR::spreadMsg-->sendToMsgQueueAnsw %s", (char *)pMsg);
            }
            break;
        }
	}

	return 0;
}

int SectionBoardSPR::decodeAsynchronousMsg( uint8_t *pMsg )
{
	m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardSPR::decodeAsynchronousMsg-->%s", pMsg);
	return 0;
}

bool SectionBoardSPR::decodeNotifyEvent(char *pubMsg, char *pcDescription)
{
	uint8_t	ubFlag1 = pubMsg[2];
	uint8_t	ubFlag2 = pubMsg[3];
	char strDescription[64];

	strDescription[0] = 0;
	if(pcDescription) *pcDescription = 0;

	// first test the default function that checks the errors defined in SECTION.err file
	if(CanBoardBase::decodeNotifyEvent(pubMsg, pcDescription))
	{
		return true;
	}

	// check other events from section
	switch ( ubFlag1 )
	{

		case 'I':	// Units: '5', '3'
		{
			switch ( ubFlag2 )
			{
				case '5':
				{
                    sprintf(strDescription, "%s -> Target temperature reached", pubMsg );
                    if(clearTemperatureError())
                    {
                        // a temperature error has been cleared -> invoke SectionBoardManager
                        // to check if the status and leds are to be updated
                        checkErrorsOnSection(m_section);
                    }
				}
				break;
			}
		}
		break;

		//.not applicable
		default:
		break;
	}

	bool bRet = true;

	if (strDescription[0] == 0  )
	{
		sprintf(strDescription, "%s -> (TBD)", pubMsg);
		bRet = false;
	}

	if(pcDescription) strcpy(pcDescription, strDescription);

    m_pLogger->log(LOG_INFO, "SectionBoardSpr::decodeNotifyEvent %c: %s", m_section + A_CHAR, strDescription);

	return bRet;
}

