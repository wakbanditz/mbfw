/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardPIB.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionBoardPIB class.
 @details

 ****************************************************************************
*/

#include "SectionBoardPIB.h"

SectionBoardPIB::SectionBoardPIB(int section) : CanBoardBase(section)
{
	m_strDevice.assign(SECTION_PIB_STRING);

	for (int i = 0; i < PIB_MAX_NUM_CHANNEL; i++)
	{
		m_stPressure[i] = {};
	}
}

SectionBoardPIB::~SectionBoardPIB( )
{

}

/*!***************************************************************************
 * COMMAND PIB                                                               *
 * ***************************************************************************/
int SectionBoardPIB::enablePressureAcquisition( enumSectionPIBStartStateAcq eState, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘s’ | ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 's';
	pPayLoad[2] = eState;

	int res = sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | 4 | 's' | f | state | ETX
		m_pLogger->log(LOG_INFO,"SectionBoardPIB::enablePressureAcquisition-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::enablePressureAcquisition-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::enableProtocolPressure(uint8_t strip1, uint8_t strip2, uint8_t strip3,
											uint8_t strip4, uint8_t strip5, uint8_t strip6,
											uint16_t usTimeoutMs )
{

	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘J’ | strip1 | strip2| strip3| strip4| strip5| strip6 | ETX*/
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'J';
	pPayLoad[2] = strip1 + ZERO_CHAR;
	pPayLoad[3] = strip2 + ZERO_CHAR;
	pPayLoad[4] = strip3 + ZERO_CHAR;
	pPayLoad[5] = strip4 + ZERO_CHAR;
	pPayLoad[6] = strip5 + ZERO_CHAR;
	pPayLoad[7] = strip6 + ZERO_CHAR;

	int res = sendCanMsg( pPayLoad, 8, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		pPayLoad[8] = 0;
		/* STX | o | ‘J’ | f | ETX */
		m_pLogger->log(LOG_INFO,"SectionBoardPIB::enableProtocolPressure-->Performed %s", pPayLoad);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::enableProtocolPressure-->ERROR");
		return -1;
	}
	return 0;
}


int SectionBoardPIB::setPressure( int32_t * slPressure, uint16_t usTimeoutMs )
{
	uint8_t		pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t		pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	uint16_t	usIdx = 0;
	uint16_t	usLenPacket = 0;

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘P’ | sign1| P1,1 | P1,2 | P1,3 | P1,4 | P1,5 | P1,6 | P1,7 | P1,8 |  …..
	// sign6 | P6,1 | P6,2 | P6,3 | P6,4| P6,5 | P6,6 | ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'P';

	//Fill the buffer with ASCII characters
	usIdx = 2;
	for ( int i=0; i < PIB_MAX_NUM_CHANNEL; i++ )
	{
		if ( slPressure[i] < 0 )
		{
			pPayLoad[ usIdx ] = '-';
			uint32_t	ulVal = -1*slPressure[i];
			sprintf( (char*)&pPayLoad[ usIdx + 1 ], "%08X",  ulVal );
		}
		else
		{
			pPayLoad[ usIdx ] = '+';
			uint32_t	ulVal = slPressure[i];
			sprintf( (char*)&pPayLoad[ usIdx + 1 ], "%08X",  ulVal );
		}
		usIdx = usIdx + 9;
	}
	usLenPacket = usIdx;

	int res = sendCanMsg( pPayLoad, usLenPacket, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘P’ | f | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::setPressure-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::setPressure-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::getPressure( int32_t &slValPress1,
								  int32_t &slValPress2,
								  int32_t &slValPress3,
								  int32_t &slValPress4,
								  int32_t &slValPress5,
								  int32_t &slValPress6,
								  uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘G’ |ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'G';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘G’ | f | sign1 | P1,1 | P1,2 | P1,3 | P1,4 | P1,5 | P1,6 | P1,7 | P1,8 | … |
		//sign6 | P6,1 | P6,2 | P6,3 | P6,4 | P6,5 | P6,6 | P6,7 | P6,8 | ETX
		uint16_t	idx = 3;
		for ( int i = 0; i < PIB_MAX_NUM_CHANNEL; i++ )
		{
			if ( pucAnsMsg[idx] == '-' )
			{
				int32_t slVal = getVal8x((char*)&pucAnsMsg[idx+1]);
				m_stPressure[i].slPressure = -1*slVal;
			}
			else
			{
				int32_t slVal = getVal8x((char*)&pucAnsMsg[idx+1]);
				m_stPressure[i].slPressure = slVal;
			}

			idx = idx + 9;
		}

		slValPress1 = m_stPressure[0].slPressure;
		slValPress2 = m_stPressure[1].slPressure;
		slValPress3 = m_stPressure[2].slPressure;
		slValPress4 = m_stPressure[3].slPressure;
		slValPress5 = m_stPressure[4].slPressure;
		slValPress6 = m_stPressure[5].slPressure;

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::getPressure-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::getPressure-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::getPressureStorage(uint16_t usTimeoutMs)
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘N’ |ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'N';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘N’ | f | S,1 | S,2 | S,3 | S,4 | S,5 | S,6 | S,7 | S,8 | ETX
		m_ulStorageFactor = (uint32_t)getVal8x( (char*)&pucAnsMsg[3] );
		m_pLogger->log(LOG_DEBUG,"SectionBoardPIB::getPressureStorage-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::getPressureStorage-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::getPressureConfiguration( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘t’ |ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 't';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘t’ | f | O1,1 | O1,2 | O1,3 | O1,4 | O1,5 | O1,6 |
		//						K1,1 | K1,2 | K1,3 | K1,4 | K1,5 | K1,6 | …. |
		//						O6,1 | O6,2 | O6,3 | O6,4 | O6,5 | O6,6 |
		//						K6,1 | K6,2 | K6,3 | K6,4 | K6,6 | K6,6 | ETX
		uint16_t ulIdxData = 3;
		for ( int i = 0; i < PIB_MAX_NUM_CHANNEL; i++ )
		{
			m_stPressure[i].slOffset	 = (int32_t)getVal6x((char*)&pucAnsMsg[ ulIdxData ]);
			m_stPressure[i].slConvFactor = (int32_t)getVal6x((char*)&pucAnsMsg[ ulIdxData + 6 ]);

			ulIdxData = ulIdxData + 12;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::getPressureConfiguration-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::getPressureConfiguration-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::getPressureCalibration( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘S’ |ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'S';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘S’ | f | sign1 | P1,1 | P1,2 | P1,3 | P1,4 | P1,5 | P1,6 | P1,7 | P1,8 | … |
		//sign6 | P6,1 | P6,2 | P6,3 | P6,4 | P6,5 | P6,6 | P6,7 | P6,8 | ETX
		uint16_t ulIdxData = 3;
		for ( int i = 0; i < PIB_MAX_NUM_CHANNEL; i++ )
		{
			int32_t sign = 1;
			if ( pucAnsMsg[ulIdxData] == '-' )
			{
				sign = -1;
			}
			int32_t slVal = getVal8x((char*)&pucAnsMsg[ulIdxData + 1]);
			m_stPressure[i].slCalibration = slVal * sign;

			ulIdxData = ulIdxData + 9;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::getPressureCalibration-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::getPressureCalibration-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::getPressureSendingRate(uint8_t channel, uint8_t* value, uint16_t usTimeoutMs )
{
    uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
    uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

    memset(pPayLoad, 0, sizeof(pPayLoad));
    memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

    // STX | d | ‘g’ | ETX
    pPayLoad[0] = SECTION_PIB_CHAR;
    pPayLoad[1] = 'g';
    pPayLoad[2] = channel + ZERO_CHAR;

    int res = sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
    if ( res == 0 )
    {
        // STX | o | ‘g’ | f  | value | ETX
        if(value)
        {
            *value = pucAnsMsg[ IDX_CAN_DATA_MSG ];
        }
        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::getPressureSendingRate-->Performed %s", pucAnsMsg);
    }
    else
    {
        m_pLogger->log(LOG_ERR,"SectionBoardPIB::getPressureSendingRate-->ERROR");
        return -1;
    }

    return 0;
}

int SectionBoardPIB::runPressureOffset( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘d’ |ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'd';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if (res == 0)
	{
		//STX | o | ‘d’ | f  | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::runPressureOffset-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::runPressureOffset-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::getADCIdentifier( uint16_t &usIdentifier, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	//STX | d | ‘C’ | ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'C';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if (res == 0)
	{
		//STX | o | ‘C’ | f | id.1 |id.2 | ETX
		usIdentifier = (uint16_t)getVal2x((char *)&pucAnsMsg[3]);

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::getADCIdentifier-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::getADCIdentifier-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::setADCConfiguration( uint8_t ubRegister, uint8_t ubRegByte1, uint8_t ubRegByte2, uint8_t ubRegByte3, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	//STX | d | ‘E’ | ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'E';
	pPayLoad[2] = ubRegister;
	pPayLoad[3] = ubRegByte1;
	pPayLoad[4] = ubRegByte2;
	pPayLoad[5] = ubRegByte3;

	int res = sendCanMsg( pPayLoad, 6, pucAnsMsg, usTimeoutMs );
	if (res == 0)
	{
		//STX | o | ‘E’ | f |registerByte.1 | registerByte.2 | registerByte.3 | ETX
		uint8_t ubResRegByte1 = pucAnsMsg[3];
		uint8_t ubResRegByte2 = pucAnsMsg[4];
		uint8_t ubResRegByte3 = pucAnsMsg[5];

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::setADCConfiguration-->R1= <%d>, R2= <%d>, R3= <%d>", ubResRegByte1,
					   ubResRegByte2,
					   ubResRegByte3);
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::setADCConfiguration-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::setADCConfiguration-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::getADCData( int32_t &slValADC1,
								 int32_t &slValADC2,
								 int32_t &slValADC3,
								 int32_t &slValADC4,
								 int32_t &slValADC5,
								 int32_t &slValADC6,
								 uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	//STX | d | ‘A’ | ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'A';

	int32_t slADSVal[PIB_MAX_NUM_CHANNEL];
	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if (res == 0)
	{
		//STX | o | ‘A’ | f |  Value1.1 | Value1.2 | Value1.3 | Value1.4 | Value1.5 | Value1.6 | ….
		//| Value1.1 | Value1.2 | Value1.3 | Value1.4 | Value1.5 | Value1.6 | ETX

		uint16_t ulIdxData = 3;
		for ( int i = 0; i < PIB_MAX_NUM_CHANNEL; i++ )
		{
			slADSVal[i] = getVal6x((char*)&pucAnsMsg[ulIdxData]);

			ulIdxData = ulIdxData + 6;
		}

		slValADC1 = slADSVal[0];
		slValADC2 = slADSVal[1];
		slValADC3 = slADSVal[2];
		slValADC4 = slADSVal[3];
		slValADC5 = slADSVal[4];
		slValADC6 = slADSVal[5];

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::getADCData-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::getADCData-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::writeEepromParameter(uint16_t usParameter, int64_t llValue, uint16_t usTimeoutMs )
{
	int8_t		sign;
	uint32_t	ulValue;
	uint8_t		pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t		pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | 'W' | pN1 | pN2 | pN3 | pN4 | sign | pV1 | pV2 | pV3 | pV4 | pV5 | pV6 | pV7 | pV8 | ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'W';

	if ( llValue >= 0 )
	{
		sign	= '+';
		ulValue = (uint32_t)llValue;
	} else
	{
		sign	= '-';
		ulValue	= (uint32_t)(-1*llValue);
	}

	sprintf((char*)&pPayLoad[2],"%04X%1c%08X", usParameter, sign, ulValue);

	int iRes = sendCanMsg( pPayLoad, 15, pucAnsMsg, usTimeoutMs );
	if ( iRes == 0 )
	{
		//STX | o | ‘W’ | f | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardPIB::writeEepromParameter-->Performed");
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::sectionMechanicReset-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::readEepromParameter(uint16_t usParameter, int64_t& sllValue, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | 'R' | pN1 | pN2 | pN3 | pN4 | ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'R';

	sprintf((char*)&pPayLoad[2],"%04X", usParameter);

	int iRes = sendCanMsg( pPayLoad, 6, pucAnsMsg, usTimeoutMs );
	if ( iRes == 0 )
	{
		//STX | o | ‘R’ | f | sign | pV1 | pV2 | pV3 | pV4 | pV5 | pV6 | pV7 | pV8 | ETX
		sllValue = (int64_t)getVal8x((char *)&pucAnsMsg[4]);

		/* Control the sign */
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == '-')
		{
			sllValue *= -1LL;
		}
		else
		{
			sllValue *= 1LL;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardPIB::readEepromParameter-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::sectionMechanicReset-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardPIB::restoreDefaultParameterConfiguration( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘D’ | ETX
	pPayLoad[0] = SECTION_PIB_CHAR;
	pPayLoad[1] = 'D';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | d | ‘D’ | f | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardPIB::restoreDefaultParameterConfiguration-->Performed");
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardPIB::restoreDefaultParameterConfiguration-->ERROR");
		return -1;
	}
	return 0;
}

/*!***************************************************************************
 * DATA GETTERS                                                           *
 * ***************************************************************************/

int32_t SectionBoardPIB::getOffsetChannel(uint8_t channel)
{
	if(channel >= PIB_MAX_NUM_CHANNEL) return 0;

	return m_stPressure[channel].slOffset;
}

uint32_t SectionBoardPIB::readStorageFactor()
{
    return m_ulStorageFactor;
}

int32_t SectionBoardPIB::readPressureCalibrationChannel(uint8_t channel)
{
	if(channel >= PIB_MAX_NUM_CHANNEL) return 0;

	return m_stPressure[channel].slCalibration;
}

int32_t SectionBoardPIB::readPressureConversionChannel(uint8_t channel)
{
	if(channel >= PIB_MAX_NUM_CHANNEL) return 0;

	return m_stPressure[channel].slConvFactor;
}

/*!***************************************************************************
 * PACKETS MANAGER                                                           *
 * ***************************************************************************/

int SectionBoardPIB::spreadMsg( uint8_t * pMsg )
{
	if ( pMsg == NULL )
	{
		return -1;
	}
	else
	{
        switch ( pMsg[IDX_CAN_TYPE_MSG] )
		{
			case 'Z':
			{
				char      pcDescription[SCTB_MAX_DESCRIPTION];
                decodeNotifyEvent( (char *)pMsg, pcDescription );

				m_pLogger->log(LOG_INFO, "SectionBoardPIB::spreadMsg-->%s", pcDescription);
			}
			break;

			default:
			{
                sendToMsgQueueAnsw( (char *)pMsg, strlen((char*) pMsg) );
                m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardPIB::spreadMsg-->sendToMsgQueueAnsw %s", (char *)pMsg);
            }
			break;
		}

	}

	return 0;
}

int SectionBoardPIB::decodeAsynchronousMsg( uint8_t *pMsg )
{
	m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardPIB::decodeAsynchronousMsg-->%s", pMsg);
	return 0;
}

bool SectionBoardPIB::decodeNotifyEvent(char *pubMsg, char *pcDescription)
{
	uint8_t	ubFlag1 = pubMsg[2];
//	uint8_t	ubFlag2 = pubMsg[3];
	char strDescription[64];

	strDescription[0] = 0;
	if(pcDescription) *pcDescription = 0;

	// first test the default function that checks the errors defined in SECTION.err file
	if(CanBoardBase::decodeNotifyEvent(pubMsg, pcDescription))
	{
		return true;
	}

	// check other events from section
	switch ( ubFlag1 )
	{
		//.not applicable
		default:
		break;
	}

	bool bRet = true;

	if (strDescription[0] == 0  )
	{
		sprintf(strDescription, "%s -> (TBD)", pubMsg);
		bRet = false;
	}

	if(pcDescription) strcpy(pcDescription, strDescription);

	m_pLogger->log(LOG_INFO, "SectionBoardPIB::decodeNotifyEvent: %s", strDescription);

	return bRet;
}

