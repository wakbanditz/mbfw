/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardPIB.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionBoardPIB class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONBOARDPIB_H
#define SECTIONBOARDPIB_H

#include "CanBoardBase.h"
#include "CanLinkCommonInclude.h"

typedef enum
{
	PIB_CHANNEL_1			= 0,
	PIB_CHANNEL_2			= 1,
	PIB_CHANNEL_3			= 2,
	PIB_CHANNEL_4			= 3,
	PIB_CHANNEL_5			= 4,
	PIB_CHANNEL_6			= 5,

	PIB_MAX_NUM_CHANNEL		= 6

} enumSectionPIBChannelsPressure;

typedef struct
{
	int32_t	slOffset;
	int32_t slPressure;
	int32_t slConvFactor;
	int32_t slCalibration;

} structSectionPIBPressure;

/*!
 * @class SectionBoardPIB
 * @brief The SectionBoardPIB class
 */

class SectionBoardPIB : public CanBoardBase
{

	public:
		SectionBoardPIB(int section);
		virtual ~SectionBoardPIB();

		/*!***************************************************************************
		 * COMMAND PIB                                                               *
		 * ***************************************************************************/
		int enablePressureAcquisition( enumSectionPIBStartStateAcq eState, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int enableProtocolPressure(uint8_t strip1, uint8_t strip2, uint8_t strip3,
									uint8_t strip4, uint8_t strip5, uint8_t strip6,
									uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int setPressure( int32_t * slPressure, uint16_t usTimeoutMs );

		int getPressure(int32_t &slValPress1,
						int32_t &slValPress2,
						int32_t &slValPress3,
						int32_t &slValPress4,
						int32_t &slValPress5,
						int32_t &slValPress6, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int getPressureStorage( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int getPressureConfiguration( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int getPressureCalibration( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

        int getPressureSendingRate(uint8_t channel, uint8_t * value = 0, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int runPressureOffset( uint16_t usTimeoutMs );

		int getADCIdentifier( uint16_t &usIdentifier, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );
		int setADCConfiguration( uint8_t ubRegister, uint8_t ubRegByte1, uint8_t ubRegByte2, uint8_t ubRegByte3,
								 uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int getADCData(int32_t &slValADC1, int32_t &slValADC2, int32_t &slValADC3,
					   int32_t &slValADC4, int32_t &slValADC5, int32_t &slValADC6,
					   uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int writeEepromParameter(uint16_t usParameter, int64_t llValue, uint16_t usTimeoutMs );
		int readEepromParameter(uint16_t usParameter, int64_t& sllValue, uint16_t usTimeoutMs );
		int restoreDefaultParameterConfiguration( uint16_t usTimeoutMs );

		/*!***************************************************************************
		 * DATA GETTERS                                                           *
		 * ***************************************************************************/

		/*!
		 * @brief getOffsetChannel to read the channel pressure offset
		 * @param channel the channel index
		 * @return the offset value
		 */
		int32_t	getOffsetChannel(uint8_t channel);

		/*!
		 * @brief readStorageFactor to read the pressure factor
		 * @return the storage value
		 */
		uint32_t readStorageFactor();

        /*!
		 * @brief readPressureCalibrationChannel to read the pressure calibration
		 * @param channel the channel index
		 * @return the pressure calibration value
		 */
		int32_t readPressureCalibrationChannel(uint8_t channel);

		/*!
		 * @brief readPressureConversionChannel to read the pressure conversion factor
		 * @param channel the channel index
		 * @return the pressure conversion value
		 */
		int32_t readPressureConversionChannel(uint8_t channel);

		/*!***************************************************************************
		 * PACKETS MANAGER                                                           *
		 * ***************************************************************************/

		/*!
		 * @brief spreadMsg
		 *		  The payload message is sent to the related message queue according to the received type message
		 * @param pMsg
		 * @return 0 if success, -1 otherwise
		 */
		int spreadMsg( uint8_t *pMsg );

	protected:
		int decodeAsynchronousMsg( uint8_t * pMsg );
		bool decodeNotifyEvent( char *pubMsg , char *pcDescription );

    private:
        structSectionPIBPressure	m_stPressure[PIB_MAX_NUM_CHANNEL];
        uint32_t m_ulStorageFactor;

};

#endif // SECTIONBOARDPIB_H
