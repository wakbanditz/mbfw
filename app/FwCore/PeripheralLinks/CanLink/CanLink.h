/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanLink.h
 @author  BmxIta FW dept
 @brief   Contains the include for the CAN classes.
 @details

 ****************************************************************************
*/

#ifndef CANLINK_H
#define CANLINK_H

#include "SectionBoardLink.h"
#include "CanLinkCommonInclude.h"
#include "SectionBoardGeneral.h"
#include "SectionBoardTray.h"
#include "SectionBoardPump.h"
#include "SectionBoardTower.h"
#include "SectionBoardSPR.h"
#include "SectionBoardPIB.h"

#endif // CANLINK_H
