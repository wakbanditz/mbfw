/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardGeneral.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionBoardGeneral class.
 @details

 ****************************************************************************
*/

#include "SectionBoardGeneral.h"
#include "MainExecutor.h"

#define PROTLENCHECKSUM   2
//#define MAXLENPKTPROT   253
#define MAXLENPKTPROT   64
#define MAXLENPKTBODY (MAXLENPKTPROT+PROTLENCHECKSUM)

//#if defined(SECTION_PROTOCOL_001)
#define MAXCOUNTERPKT 18
static char counterPacketProt[MAXCOUNTERPKT]={
	'0','1','2','3','4','5','6','7','8','9',
	'A','B','C','D','E','F','G','H'};
//#endif

typedef enum
{
	ePacketTypeProtBody = '0',
	ePacketTypeProtLast = '1'
} ePacketTypeProt;



SectionBoardGeneral::SectionBoardGeneral(int section) : CanBoardBase(section)
{
	m_usVoltageP24V = 0;
	m_usVoltageP3V3 = 0;
	m_usVoltageP5V	= 0;
	m_usVoltageP1V2 = 0;

	m_fTemperatureSPR = 0.;
	m_fTemperatureTray = 0.;

	m_sensorPump = false;
	m_sensorSpr = false;
	m_sensorTray = false;
	m_sensorTower = false;

	m_doorStatus = eDoorUnknown;
	m_doorStatus1 = eDoorUnknown;
	m_doorStatus2 = eDoorUnknown;
	m_sectionReady = false;

	m_protocolWindowJ = false;
	m_protocolEnd = false;
	m_protocolAborted = false;

	m_strDevice.assign(SECTION_GENERAL_STRING);
}

SectionBoardGeneral::~SectionBoardGeneral()
{
	/* Nothing to do yet */
}

int SectionBoardGeneral::getVoltages( uint16_t usTimeoutMs ,uint16_t * us24V0Val,
									  uint16_t * us5V0Val, uint16_t  * us3V3Val, uint16_t * us1V2Val )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[8];
	memset(pPayLoad, 0, sizeof(pPayLoad));

	/* STX | d | 'V' | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'V';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		uint8_t ucIdxMsg = IDX_CAN_DATA_MSG;
		// convertion from ASCII to int
		m_usVoltageP3V3 = getVal4x((char*)&pucAnsMsg[ucIdxMsg]);
		if(us3V3Val) *us3V3Val = m_usVoltageP3V3;
		ucIdxMsg = ucIdxMsg + 4;
		m_usVoltageP24V = getVal4x((char*)&pucAnsMsg[ucIdxMsg]);
		if(us24V0Val) *us24V0Val = m_usVoltageP24V;
		ucIdxMsg = ucIdxMsg + 4;
		m_usVoltageP5V = getVal4x((char*)&pucAnsMsg[ucIdxMsg]);
		if(us5V0Val) *us5V0Val = m_usVoltageP5V;
		ucIdxMsg = ucIdxMsg + 4;
		m_usVoltageP1V2 = getVal4x((char*)&pucAnsMsg[ucIdxMsg]);
		if(us1V2Val) *us1V2Val = m_usVoltageP1V2;

        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::getVoltages %c--> 3V3=%d, 24V=%d, 5V=%d, 1V2=%d",
                       m_section + SECTION_CHAR_A, m_usVoltageP3V3, m_usVoltageP24V, m_usVoltageP5V, m_usVoltageP1V2);
	}
	else
	{
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::getTemperature( uint16_t& usSPRTemp, uint16_t& usTrayTemp, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 't' | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 't';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		uint8_t ucidx = IDX_CAN_DATA_MSG;

		uint16_t	usT1 = getVal4x( (char *)&pucAnsMsg[ucidx] );
		ucidx = ucidx + 4;
		uint16_t	usT2 = getVal4x( (char *)&pucAnsMsg[ucidx] );

		usSPRTemp = usT1;
		usTrayTemp = usT2;

        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::getTemperature %c--> t1=%d, t2=%d", m_section + SECTION_CHAR_A, usT1, usT2);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::getTemperature %c -->ERROR", m_section + SECTION_CHAR_A );
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::getSensors(uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 'S' | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'S';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | 'S' | f | state | ETX */
		uint8_t ubSensorState = pucAnsMsg[IDX_CAN_DATA_MSG];
		ubSensorState = ubSensorState - 0x30;
		// decode sensor status:
		if(ubSensorState & 0x01)
		{
			m_sensorPump = true;
		}
		else
		{
			m_sensorPump = false;
		}
		if(ubSensorState & 0x02)
		{
			m_sensorTower = true;
		}
		else
		{
			m_sensorTower = false;
		}
		if(ubSensorState & 0x04)
		{
			m_sensorTray = true;
		}
		else
		{
			m_sensorTray = false;
		}
		if(ubSensorState & 0x08)
		{
			m_pushbuttonStatus = true;
		}
		else
		{
			m_pushbuttonStatus = false;
		}
		if(ubSensorState & 0x10)
		{
			m_doorStatus1 = eDoorClosed;
		}
		else
		{
			m_doorStatus1 = eDoorOpen;
		}
		if(ubSensorState & 0x20)
		{
			m_doorStatus2 = eDoorClosed;
		}
		else
		{
			m_doorStatus2 = eDoorOpen;
		}

		if(m_doorStatus1 == m_doorStatus2)
		{
			// update general door status
			m_doorStatus = m_doorStatus1;
		}

        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::getSensor--> %c Performed %s", m_section + SECTION_CHAR_A, pucAnsMsg);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::getSensor %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}
	return 0;
}

int SectionBoardGeneral::getVolumeKFactor( uint32_t& ulKFactor, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘g’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'g';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘g’ | f  | A1 | A2 | A3 | A4 | A5 | A6 | A7 | A8 | ETX */
		ulKFactor = getVal8x((char*)&pucAnsMsg[3]);

        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::getVolumeKFactor--> %c Performed, ulKFactor is %d", m_section + SECTION_CHAR_A, ulKFactor);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::getVolumeKFactor %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::setVolumeKFactor( uint32_t ulVolumeKFactor, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	if ( ulVolumeKFactor > 100 )
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::setVolumeKFactor %c -->ERROR",m_section + SECTION_CHAR_A);
		return -1;
	}

	/* STX | d | ‘d’ | F1 | F2 | F3 | F4 | F5 | F6 | F7 | F8 | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'd';
	pPayLoad[2] = ZERO_CHAR;
	pPayLoad[3] = ZERO_CHAR;
	pPayLoad[4] = ZERO_CHAR;
	pPayLoad[5] = ZERO_CHAR;
	pPayLoad[6] = ZERO_CHAR;
	pPayLoad[7] = ZERO_CHAR;

	sprintf((char*)&pPayLoad[8], "%02x", ulVolumeKFactor);

	int res = sendCanMsg( pPayLoad, 10, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘d’ |  f  | ETX */
        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::setVolumeKFactor %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::setVolumeKFactor %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::getPressureAlgorithm( uint8_t& ubPressureAlgorithm, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘G’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'G';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘G’ | f | S1 | ETX*/
		ubPressureAlgorithm = pucAnsMsg[3];

        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::getPressureAlgorithm %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::getPressureAlgorithm %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::setPressureAlgorithm( uint8_t strip, enumSectionPressureAlgorithm ePressureAlgorithm, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘F’ | strip | algo | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'F';
	pPayLoad[2] = '0' + strip;
	pPayLoad[3] = ePressureAlgorithm;

	int res = sendCanMsg( pPayLoad, 4, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘F’ | f |   ETX */
        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::setPressureAlgorithm %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::setPressureAlgorithm %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::resetMechanic( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘M’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'M';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | d | ‘M’ | f | ETX */
        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::sectionMechanicReset %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::sectionMechanicReset %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}
	return 0;
}

int SectionBoardGeneral::writeEepromParameter(uint16_t usParameter, int64_t llValue, uint16_t usTimeoutMs )
{
	int8_t		sign;
	uint32_t	ulValue;
	uint8_t		pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t		pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | 'W' | pN1 | pN2 | pN3 | pN4 | sign | pV1 | pV2 | pV3 | pV4 | pV5 | pV6 | pV7 | pV8 | ETX
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'W';

	if ( llValue >= 0 )
	{
		sign	= '+';
		ulValue = (uint32_t)llValue;
	} else
	{
		sign	= '-';
		ulValue	= (uint32_t)(-1*llValue);
	}

	sprintf((char*)&pPayLoad[2],"%04X%1c%08X", usParameter, sign, ulValue);

	int iRes = sendCanMsg( pPayLoad, 15, pucAnsMsg, usTimeoutMs );
	if ( iRes == 0 )
	{
		//STX | o | ‘W’ | f | ETX
        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::writeEepromParameter %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::sectionMechanicReset %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::readEepromParameter(uint16_t usParameter, int64_t& sllValue, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 'R' | pN1 | pN2 | pN3 | pN4 | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'R';

	sprintf((char*)&pPayLoad[2],"%04X", usParameter);

	int iRes = sendCanMsg( pPayLoad, 6, pucAnsMsg, usTimeoutMs );
	if ( iRes == 0 )
	{

		sllValue= (int64_t)getVal8x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 1]);

		/* Control the sign */
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == 0x2D)
		{
			sllValue *= -1LL;
		}
		else
		{
			sllValue *= 1LL;
		}

        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::readEepromParameter %c -->Performed %s", m_section + SECTION_CHAR_A, pucAnsMsg);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::sectionMechanicReset %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::restoreDefaultEepromState( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘D’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'D';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | d | ‘D’ | f | ETX */
        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::restoreDefaultParameterConfiguration %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::restoreDefaultParameterConfiguration %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}
	return 0;
}

int SectionBoardGeneral::setInfoState( enumSectionInfoState eInfoState, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘J’ | info_state   | ETX*/
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'J';
	pPayLoad[2] = eInfoState;

	int res = sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘J’ | f | ETX */
        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::setInfoState %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::setInfoState %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}
	return 0;
}

int SectionBoardGeneral::setLed( enumSectionLedType eLedType, enumSectionLedFreq eLedFreq, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 'L' | LED type | Freq | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'L';
	pPayLoad[2] = eLedType;
	pPayLoad[3] = eLedFreq;

	int res = sendCanMsg( pPayLoad, 4, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘L’ | f | ETX */
        m_pLogger->log(LOG_DEBUG, "SectionBoardGeneral::setLed %c --> %s Performed", m_section + SECTION_CHAR_A, pPayLoad);
	}
	else
	{
        m_pLogger->log(LOG_ERR, "SectionBoardGeneral::setLed %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::enterMaintenanceState( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘Q’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'Q';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘Q’ | f | ETX */
        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::enterMaintenanceState %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::enterMaintenanceState %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::exitMaintenanceState( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘q’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'q';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘q’ | f | ETX */
        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::exitMaintenanceState %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::exitMaintenanceState %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::runInitializationProcedure( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘K’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'K';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘K’ | f | ETX */
        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::runInitializationProcedure %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::runInitializationProcedure %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::getInternalStatus( uint8_t &ubStatus, uint16_t usTimeoutMs, bool bErrorSet )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | 'i' | ETX
	memset(pPayLoad, 0, sizeof(pPayLoad));

	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'i';

    int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs, bErrorSet );
	if ( res == 0 )
	{
		//STX | o | ‘i’ | f  | s0 | ETX
		ubStatus = pucAnsMsg[3];
        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::getInternalStatus %c --> %c", m_section + SECTION_CHAR_A, ubStatus);
	}
	else
	{
        ubStatus = 0;
        m_pLogger->log(LOG_ERR, "SectionBoardGeneral::getInternalStatus %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::sendInitializationEvents(uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | 'k' | ETX
	memset(pPayLoad, 0, sizeof(pPayLoad));

	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'k';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘k’ | f | ETX
        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::sendInitializationEvents %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR, "SectionBoardGeneral::sendInitializationEvents %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::uploadFirmware(uint8_t eTypeMemoryUpFw, uint8_t ubSeqTypePkt, uint32_t ulNPkt,
										uint8_t &ubStatusUpg, uint16_t usTimeoutMs)
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | 'U' | Type Memory | Seq./Type Pkt | Npckt1 | Npckt2 | Npckt3 | Npckt4 | ETX
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'U';
	pPayLoad[2] = eTypeMemoryUpFw;
	pPayLoad[3] = ubSeqTypePkt;

	sprintf((char*)&pPayLoad[4],"%04X", ulNPkt);
    m_pLogger->log(LOG_INFO, "SectionBoardGeneral::uploadFirmware %c -->Performed", m_section + SECTION_CHAR_A);
	int res = sendCanMsg( pPayLoad, 8, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘U’ | f1 | f2 | ETX
		ubStatusUpg = pucAnsMsg[3];
        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::uploadFirmware %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR, "SectionBoardGeneral::uploadFirmware %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}
/*!***************************************************************************
 * SECTION BOARD GENERAL STATUS                                              *
 * ***************************************************************************/

std::string SectionBoardGeneral::getFirmwareVersion(enumSectionFirmwareVersion eType, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	std::string strFirmware;

	strFirmware.clear();
	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | 'f' | flag | fw_type | fw_string | ETX
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'f';
	pPayLoad[2] = eType;

	int res = sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::getFirmwareVersion %c --> Sent to 0x%x", m_section + SECTION_CHAR_A, m_pSectionBoardLink->getID());
        m_pLogger->log(LOG_INFO, "SectionBoardGeneral::getFirmwareVersion %c --> Type: %d, Rel: %s", m_section + SECTION_CHAR_A, eType, pucAnsMsg);

		strFirmware.append((const char *)&pucAnsMsg[3]);
	}
	else
	{
        m_pLogger->log(LOG_ERR, "SectionBoardGeneral::getFirmwareVersion %c -->ERROR", m_section + SECTION_CHAR_A);
	}

	return strFirmware;
}

int SectionBoardGeneral::getTableVersion( enumSectionEepromTableType eTableType, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t ubV[6];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 'v' | table_type | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'v';
	pPayLoad[2] = eTableType;

	int res = sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘v’ | f | table_type | V1 | V2 | V3 | V4| V5 | V6 | ETX */
		ubV[0] = getValD((char*)&pucAnsMsg[4]);
		ubV[1] = getValD((char*)&pucAnsMsg[5]);
		ubV[2] = getValD((char*)&pucAnsMsg[6]);
		ubV[3] = getValD((char*)&pucAnsMsg[7]);
		ubV[4] = getValD((char*)&pucAnsMsg[8]);
		ubV[5] = getValD((char*)&pucAnsMsg[9]);

		m_Table.setVersion( eTableType, ubV );
		m_Table.printVersion( eTableType );

        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::getTableVersion %c --> TableVersion:%d.%d.%d.%d.%d.%d",
                       m_section + SECTION_CHAR_A, ubV[0], ubV[1], ubV[2], ubV[3], ubV[4], ubV[5]);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::getTableVersion %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::sendTable( uint8_t strip, enumSectionEepromTableType eTableType, enumSectionEepromPacketType ePacketType,
									uint8_t ubRowIndex, SectionEepromTable &sectionEepromTable, uint16_t usTimeoutMs )
{
	uint8_t		pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t		pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	uint16_t	usLenPacket = 0;
	uint8_t		ubVersion[6];
	uint16_t	usIdx;
	uint32_t	ulCheckSumTmp;
	uint32_t	ulBuffTmp[SECTION_EEPROM_TABLE_SIZE];
	int32_t		slBuffTmp[SECTION_EEPROM_TABLE_SIZE];

	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));
	memset(pPayLoad, 0, sizeof(pPayLoad));

	/* STX | d | ‘H’ | strip | table_type |  packet_type |  row_index | packet_str |ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'H';
	pPayLoad[2] = '0' + strip;
	pPayLoad[3] = eTableType;
	pPayLoad[4] = ePacketType;

	switch ( ePacketType )
	{
		case '0':
		{
			// Packet is the table version, row_index is not present here
			uint8_t	ubVersionTmp[6];
			sectionEepromTable.getVersion(eTableType, ubVersionTmp);

			ubVersion[0] = getAsciiValue(ubVersionTmp[0]);
			ubVersion[1] = getAsciiValue(ubVersionTmp[1]);
			ubVersion[2] = getAsciiValue(ubVersionTmp[2]);
			ubVersion[3] = getAsciiValue(ubVersionTmp[3]);
			ubVersion[4] = getAsciiValue(ubVersionTmp[4]);
			ubVersion[5] = getAsciiValue(ubVersionTmp[5]);

			sprintf((char*)&pPayLoad[5], "%s", ubVersion);
			usLenPacket = 10;
		}
		break;


		case '1':
		{
			// Packet is a row
			int uRow = getAsciiValue( ubRowIndex );
			pPayLoad[4] = uRow;

			//Convert PacketString
			sprintf( (char*)&pPayLoad[5], "%1c", uRow );

			usIdx = 5;

			switch (eTableType)
			{
				case eAspirationLowThresholdTable:
				case eAspirationHighThresholdTable:
				case eAspirationIndustryThresholdTable:
				{
					sectionEepromTable.getRowAspirationTsTable(eTableType, ubRowIndex, ulBuffTmp, ulCheckSumTmp);

					for (int i = 0; i< SECTION_EEPROM_TABLE_SIZE; i++)
					{
						sprintf((char*)&pPayLoad[usIdx], "%08X",  ulBuffTmp[i]);
						usIdx = usIdx + 8;
					}

					sprintf((char*)&pPayLoad[usIdx], "%08X",  ulCheckSumTmp);

					m_pLogger->log(LOG_INFO, "checkSum: %d", ulCheckSumTmp);

					usLenPacket = usIdx + 8;
				}
				break;

				case eVolumeTable:
				{
					sectionEepromTable.getTableVolumes(ubRowIndex, slBuffTmp, ulCheckSumTmp);

					for (int i = 0; i<SECTION_EEPROM_TABLE_SIZE; i++)
					{
						sprintf((char*)&pPayLoad[usIdx], "%08X",  slBuffTmp[i]);
						usIdx = usIdx + 8;
					}

					sprintf((char*)&pPayLoad[usIdx], "%08X",  ulCheckSumTmp);

					m_pLogger->log(LOG_INFO, "checkSum: %d", ulCheckSumTmp);

					usLenPacket = usIdx + 8;
				}
				break;

				case eSpeedTable:
				{
					sectionEepromTable.getRowSpeed( ubRowIndex, slBuffTmp, ulCheckSumTmp );

					for (int i = 0; i<SECTION_EEPROM_TABLE_SIZE; i++)
					{
						sprintf((char*)&pPayLoad[usIdx], "%08X",  ulBuffTmp[i]);
						usIdx = usIdx + 8;
					}

					sprintf((char*)&pPayLoad[usIdx], "%08X",  ulCheckSumTmp);

					usLenPacket = usIdx + 8;
				}
				break;

				default:
				{
                    m_pLogger->log(LOG_ERR, "SectionBoardGeneral::sendTable %c -->ERROR default1", m_section + SECTION_CHAR_A);
					return -1;
				}
				break;
			}
		}
		break;

		default:
		{
             m_pLogger->log(LOG_ERR, "SectionBoardGeneral::sendTable %c -->ERROR default2", m_section + SECTION_CHAR_A);
			 return -1;
		}
		break;

	}

	int res = sendCanMsg( pPayLoad, usLenPacket, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::sendTable %c -->%s", m_section + SECTION_CHAR_A, pucAnsMsg);
	}
	else
	{
        m_pLogger->log(LOG_ERR, "SectionBoardGeneral::sendTable %c -->ERROR default3", m_section + SECTION_CHAR_A);
		return -1;
	}
	return 0;
}

int SectionBoardGeneral::getTable( enumSectionEepromTableType eTableType, uint8_t ubRowIndex, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));
	memset(pPayLoad, 0, sizeof(pPayLoad));

	/* STX | d | ‘h’ | table_type| row_index | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'h';
	pPayLoad[2] = eTableType;
	int uRow = getAsciiValue(ubRowIndex);
	sprintf((char*)&pPayLoad[3], "%c", uRow);

	int res = sendCanMsg( pPayLoad, 4, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘h’ | f | table_type |  row_index | packet_str |  ETX
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::getTable %c -->%s", m_section + SECTION_CHAR_A, pucAnsMsg);

		uint16_t usIdxData = IDX_CAN_DATA_MSG+2;
		switch ( eTableType )
		{
			case eAspirationLowThresholdTable:
			case eAspirationHighThresholdTable:
			case eAspirationIndustryThresholdTable:
			{
				uint32_t ulCheckSumCalc = 0;
				for ( int i=0; i<SECTION_EEPROM_TABLE_SIZE; i++ )
				{
					uint32_t	ulTmp = getVal8x((char*)&pucAnsMsg[usIdxData]);
					m_Table.setValAspirationTsTable( eTableType, ulTmp, ubRowIndex, i);
					usIdxData = usIdxData + 8;

					ulCheckSumCalc = ulCheckSumCalc + ulTmp;
				}

				uint32_t	ulCheckSumTmp = getVal8x((char*)&pucAnsMsg[usIdxData]);
                m_pLogger->log(LOG_INFO,"SectionBoardGeneral::getTable %c -->RxChecksum= %d, CalcChecksum= %d ",
                               m_section + SECTION_CHAR_A, ulCheckSumTmp, ulCheckSumCalc);

				//Print Row of selected table
				m_Table.printRowAspirationTsTable( eTableType, ubRowIndex);
			}
			break;

			case eVolumeTable:
			{
				int32_t slCheckSumCalc = 0;
				for ( int i=0; i<SECTION_EEPROM_TABLE_SIZE; i++ )
				{
					int32_t	slTmp = getVal8x((char*)&pucAnsMsg[usIdxData]);
					m_Table.setValVolumes(slTmp, i);
					usIdxData = usIdxData + 8;

					slCheckSumCalc = slCheckSumCalc + slTmp;
				}

				int32_t	slCheckSumTmp = getVal8x((char*)&pucAnsMsg[usIdxData]);
                m_pLogger->log(LOG_INFO,"SectionBoardGeneral::getTable %c -->RxChecksum= %d, CalcChecksum= %d ",
                               m_section + SECTION_CHAR_A, slCheckSumTmp, slCheckSumCalc);

				//Print volume table
				m_Table.printValVolumesTable();
			}
			break;

			case eSpeedTable:
			{
				int32_t slCheckSumCalc = 0;
				for ( int i=0; i<SECTION_EEPROM_TABLE_SIZE; i++ )
				{
					int32_t	slTmp = getVal8x((char*)&pucAnsMsg[usIdxData]);
					m_Table.setValSpeed(slTmp, ubRowIndex, i);
					usIdxData = usIdxData + 8;

					slCheckSumCalc = slCheckSumCalc + slTmp;
				}

				int32_t	slCheckSumTmp = getVal8x((char*)&pucAnsMsg[usIdxData]);
                m_pLogger->log(LOG_INFO,"SectionBoardGeneral::getTable %c -->RxChecksum= %d, CalcChecksum= %d ",
                               m_section + SECTION_CHAR_A, slCheckSumTmp, slCheckSumCalc);

				//Print row of selected table
				m_Table.printRowSpeedTable( ubRowIndex );
			}
			break;

			default:
                m_pLogger->log(LOG_ERR,"SectionBoardGeneral::getTable %c -->ERROR default", m_section + SECTION_CHAR_A);
			break;
		}
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::getTable %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::getNTCParameters( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));
	memset(pPayLoad, 0, sizeof(pPayLoad));

	/* STX | d | 'N' | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'N';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘N’ | f  | Vr1 | Vr2 | Vr3 | Vr4 | D1 | D2 | D3 | D4 | R1 | R2 | R3 | R4 | R5 | R6 | R7 | R8 |
		//P1,1 | P1,2 | P1,3 | P1,4 | P1,5 | P1,6 | P1,7 | P1,8 | P2,1 | P2,2 | P2,3 | P2,4 | P2,5 | P2,6 | P2,7 |
		//P2,8 | P3,1 | P3,2 | P3,3 | P3,4 | P3,5 | P3,6 | P3,7 | P3,8 | F | ETX
		res = m_NTCParam.setNTCParameters( pucAnsMsg );
		if ( res == 0 )
		{			
			m_NTCParam.printNTCParameters();
		}
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::getNTCParameters %c -->Performed %s", m_section + SECTION_CHAR_A, pucAnsMsg);
	}
	else
	{
		return -1;
	}
	return 0;
}

int SectionBoardGeneral::getDefaultMotorConfiguration( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘C’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'C';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘C’ |  f  | dir_1 | steptype_1 | stepmode_1 | slopescan_1 | decslope_1 | freq_source_1 |
		 * freq_table_1 | F1_1 | F2_1 | F3_1 | F4_1 | curr_1|  dir_2 | steptype_2 | stepmode_2 | slopescan_2 |
		 * decslope_2 | freq_source_2 | freq_table_2 | F1_2 | F2_2 | F3_2 | F4_2 | curr_2| dir_3 | steptype_3 |
		 * stepmode_3 | slopescan_3 | decslope_3 | freq_source_3 | freq_table_3 | F1_3 | F2_3 | F3_3 | F4_3 | curr_3| ETX
		 * */
		m_stMotorConfig_1.ubDir			= pucAnsMsg[3];
		m_stMotorConfig_1.ubStepType	= pucAnsMsg[4];
		m_stMotorConfig_1.ubStepMode	= pucAnsMsg[5];
		m_stMotorConfig_1.ubSlopeScan	= pucAnsMsg[6];
		m_stMotorConfig_1.ubDecSlope	= pucAnsMsg[7];
		m_stMotorConfig_1.ubFreqSource	= pucAnsMsg[8];
		m_stMotorConfig_1.ubFreqTable	= pucAnsMsg[9];
		m_stMotorConfig_1.usFreqMotor	= getVal4x((char*)&pucAnsMsg[10]);
		m_stMotorConfig_1.ubCurr		= pucAnsMsg[14];

		m_stMotorConfig_2.ubDir			= pucAnsMsg[15];
		m_stMotorConfig_2.ubStepType	= pucAnsMsg[16];
		m_stMotorConfig_2.ubStepMode	= pucAnsMsg[17];
		m_stMotorConfig_2.ubSlopeScan	= pucAnsMsg[18];
		m_stMotorConfig_2.ubDecSlope	= pucAnsMsg[19];
		m_stMotorConfig_2.ubFreqSource	= pucAnsMsg[20];
		m_stMotorConfig_2.ubFreqTable	= pucAnsMsg[21];
		m_stMotorConfig_2.usFreqMotor	= getVal4x((char*)&pucAnsMsg[22]);
		m_stMotorConfig_2.ubCurr		= pucAnsMsg[26];

		m_stMotorConfig_3.ubDir			= pucAnsMsg[27];
		m_stMotorConfig_3.ubStepType	= pucAnsMsg[28];
		m_stMotorConfig_3.ubStepMode	= pucAnsMsg[29];
		m_stMotorConfig_3.ubSlopeScan	= pucAnsMsg[30];
		m_stMotorConfig_3.ubDecSlope	= pucAnsMsg[31];
		m_stMotorConfig_3.ubFreqSource	= pucAnsMsg[32];
		m_stMotorConfig_3.ubFreqTable	= pucAnsMsg[33];
		m_stMotorConfig_3.usFreqMotor	= getVal4x((char*)&pucAnsMsg[34]);
		m_stMotorConfig_3.ubCurr		= pucAnsMsg[38];


        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::getDefaultMotorConfiguration %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::getDefaultMotorConfiguration %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}


/*!***************************************************************************
 * PROTOCOL                                                                  *
 * ***************************************************************************/
int SectionBoardGeneral::enablePressureAcquisition( enumSectionPIBStartStateAcq eState, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | 5 | J | ETX
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'J';
	pPayLoad[2] = eState;

	int res = sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | 5 | 'J' | f | state | ETX
        m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardGeneral::enablePressureAcquisition %c -->Performed %s", m_section + SECTION_CHAR_A, pucAnsMsg);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::enablePressureAcquisition %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::sendStripLayout(uint8_t * str1, uint8_t * str2,
										 uint8_t * str3, uint8_t * str4,
										 uint8_t * str5, uint8_t * str6,
										 uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t counter, i, strip;
	uint8_t * pStr = 0;

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 's' | Y0,0 | Y0,1 | Y0,2 | Y0,3 | Y0,4 | Y0,5 | Y0,6 | Y0,7 | Y0,8 | Y0,A
	 *					......
	 *				 | Y5,0 | Y5,1 | Y5,2 | Y5,3 | Y5,4 | Y5,5 | Y5,6 | Y5,7 | Y5,8 | Y5,A	| ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 's';

	for(strip = 0, counter = 2; strip < SCT_NUM_TOT_SLOTS; strip++)
	{
		if(strip == 0)
			pStr = str1;
		else if(strip == 1)
			pStr = str2;
		else if(strip == 2)
			pStr = str3;
		else if(strip == 3)
			pStr = str4;
		else if(strip == 4)
			pStr = str5;
		else if(strip == 5)
			pStr = str6;

		for(i = 0; i < SCT_SLOT_NUM_WELLS; i++)
		{
			pPayLoad[counter] = *(pStr + i);
			counter++;
		}
	}

	int res = sendCanMsg( pPayLoad, counter, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		pPayLoad[counter] = 0;
		/* STX | o | ‘s’ | f  | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::sendStripLayout %c -->Performed %s", m_section + SECTION_CHAR_A, pPayLoad);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::sendStripLayout %c -->ERROR", m_section + SECTION_CHAR_A);
		res = -1;
	}

   return res;
}

int SectionBoardGeneral::sendStripCheck(uint8_t * str1, uint8_t * str2,
										 uint8_t * str3, uint8_t * str4,
										 uint8_t * str5, uint8_t * str6,
										 uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t counter, i, strip;
	uint8_t * pStr = 0;

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 'r' | Y0,0 | Y0,1 | Y0,2 | Y0,3 | Y0,4 | Y0,5 | Y0,6 | Y0,7 | Y0,8 | Y0,A
	 *					......
	 *				 | Y5,0 | Y5,1 | Y5,2 | Y5,3 | Y5,4 | Y5,5 | Y5,6 | Y5,7 | Y5,8 | Y5,A	| ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'r';

	for(strip = 0, counter = 2; strip < SCT_NUM_TOT_SLOTS; strip++)
	{
		if(strip == 0)
			pStr = str1;
		else if(strip == 1)
			pStr = str2;
		else if(strip == 2)
			pStr = str3;
		else if(strip == 3)
			pStr = str4;
		else if(strip == 4)
			pStr = str5;
		else if(strip == 5)
			pStr = str6;

		for(i = 0; i < SCT_SLOT_NUM_WELLS; i++)
		{
			pPayLoad[counter] = *(pStr + i);
			counter++;
		}
	}

	int res = sendCanMsg( pPayLoad, counter, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		pPayLoad[counter] = 0;
		/* STX | o | ‘r’ | f  | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::sendStripCheck %c -->Performed %s", m_section + SECTION_CHAR_A, pPayLoad);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::sendStripCheck %c -->ERROR", m_section + SECTION_CHAR_A);
		res = -1;
	}

    return res;
}

int SectionBoardGeneral::setPressureSendingRate(uint8_t rate1, uint8_t rate2, uint8_t rate3, uint8_t rate4, uint8_t rate5, uint8_t rate6, uint16_t usTimeoutMs)
{

    uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
    uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

    memset(pPayLoad, 0, sizeof(pPayLoad));
    memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

    /* STX | d | ‘C’ | rate1 | rate2| rate3| rate4| rate5| rate6 | ETX*/
    pPayLoad[0] = SECTION_GENERAL_CHAR;
    pPayLoad[1] = 'C';
    pPayLoad[2] = rate1 + ZERO_CHAR;
    pPayLoad[3] = rate2 + ZERO_CHAR;
    pPayLoad[4] = rate3 + ZERO_CHAR;
    pPayLoad[5] = rate4 + ZERO_CHAR;
    pPayLoad[6] = rate5 + ZERO_CHAR;
    pPayLoad[7] = rate6 + ZERO_CHAR;

    int res = sendCanMsg( pPayLoad, 8, pucAnsMsg, usTimeoutMs );
    if ( res == 0 )
    {
        pPayLoad[8] = 0;
        /* STX | o | ‘C’ | f | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::setPressureSendingRate-->Performed %s", pPayLoad);
    }
    else
    {
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::setPressureSendingRate-->ERROR");
        return -1;
    }
    return 0;
}

int SectionBoardGeneral::sendStripPressureSettings(uint8_t strip, uint8_t parameter, int16_t value,
												   uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	uint16_t usModValue;

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 'F' | strip | parameter | sign | Y0 | Y1 | Y2 | Y3 | Y4 | Y5 | Y6 | Y7 | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'F';
	pPayLoad[2] = strip + ZERO_CHAR;
	pPayLoad[3] = parameter;

	if ( value < 0)
	{
		pPayLoad[4] = '-';
		usModValue = -value;

	}
	else
	{
		pPayLoad[4] = '+';
		usModValue = value;
	}
	sprintf((char*)&pPayLoad[5], "%08X", usModValue);

	int res = sendCanMsg( pPayLoad, 13, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		pPayLoad[13] = 0;
		/* STX | o | ‘F’ | f  | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::sendStripPressureSettings %c -->Performed %s", m_section + SECTION_CHAR_A, pPayLoad);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::sendStripPressureSettings %c -->ERROR", m_section + SECTION_CHAR_A);
		res = -1;
	}

	return res;
}

int SectionBoardGeneral::sendStripPressureThresholds(uint8_t strip, uint8_t volume, uint8_t speed,
													 uint64_t low, uint64_t high, uint16_t usTimeoutMs)
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 'G' | strip | volume | speed | Y0 | Y1 | Y2 | Y3 | Y4 | Y5 | Y6 | Y7 |
	 *											 | Z0 | Z1 | Z2 | Z3 | Z4 | Z5 | Z6 | Z7 | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'G';
	pPayLoad[2] = strip + ZERO_CHAR;
	pPayLoad[3] = volume;
	pPayLoad[4] = speed;

	// !!!! TODO verificare se sono valori sufficienti per le soglie
	sprintf((char*)&pPayLoad[5], "%08X", (uint32_t)low);
	sprintf((char*)&pPayLoad[13], "%08X", (uint32_t)high);

    int res = sendCanMsg( pPayLoad, 21, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		pPayLoad[21] = 0;
		/* STX | o | ‘G’ | f  | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::sendStripPressureThresholds %c -->Performed %s", m_section + SECTION_CHAR_A, pPayLoad);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::sendStripPressureThresholds %c -->ERROR", m_section + SECTION_CHAR_A);
		res = -1;
	}

	return res;
}

int SectionBoardGeneral::sendProtocolString( string sProtocol, uint16_t usTimeoutMs )
{
	uint8_t		pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t		pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];
	uint32_t	ulLenStrProt;
	uint32_t	ulIdxPktOrder, ulCurrStrProt;
	uint32_t	ulLenBuffTxpld, ulDifflenPkt, ulEndTx;
	uint8_t		ubChecksum;
	string		sProt	= sProtocol;
	const char		*ptrStrProt = NULL;
	ePacketTypeProt ePacketType;

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	ulLenStrProt	= sProt.size();
	ptrStrProt		= sProt.c_str();
	ulIdxPktOrder	= 0;
	ulCurrStrProt	= 0;
	ulEndTx			= 0;
	ePacketType		= ePacketTypeProtBody;

    m_pLogger->log(LOG_INFO, "SectionBoardGeneral::sendProtocolString %c %s", m_section + SECTION_CHAR_A, ptrStrProt);

	/* STX | d | ‘P’ |  packet_type | packet_order | pl1 | pl2 | str | ETX */
	while (1)
	{
		ulDifflenPkt = ( ulLenStrProt - ulCurrStrProt );
		if ( ulDifflenPkt <= MAXLENPKTPROT )
		{
			ePacketType = ePacketTypeProtLast;
			ulEndTx = 1;
		} else
		{
			ulDifflenPkt = MAXLENPKTPROT;
		}

		if (ulIdxPktOrder >= MAXCOUNTERPKT)
		{
            m_pLogger->log(LOG_ERR, "SectionBoardGeneral::sendProtocolString %c --> MAXCOUNTERPKT reached", m_section + SECTION_CHAR_A);
			break;
		}

		/* Append packet content up to 66 bytes */
		pPayLoad[0] = SECTION_GENERAL_CHAR;
		pPayLoad[1] = 'P';
		pPayLoad[2] = ePacketType;
		pPayLoad[3] = counterPacketProt[ulIdxPktOrder];
		sprintf((char*)&pPayLoad[4], "%02X", ( ulDifflenPkt + PROTLENCHECKSUM ));

		/* Append checksum -> 2 bytes */
		ulLenBuffTxpld	= 0;
		ubChecksum		= 0;
		for ( uint32_t ulIdxBuffData = 0; ulIdxBuffData < ulDifflenPkt; ulIdxBuffData++ )
		{
			pPayLoad[ ulLenBuffTxpld + 6 ] = ptrStrProt[ ulCurrStrProt ];
			ubChecksum					 = ubChecksum + ptrStrProt[ ulCurrStrProt ];

			ulLenBuffTxpld++;
			ulCurrStrProt++;
		}

		/* checksum %= MAXLENPKTBODY; //mod 255 */
		sprintf((char*)&pPayLoad[ ulLenBuffTxpld + 6 ], "%02X", ubChecksum);

		uint16_t usLen = strlen((const char*)&pPayLoad);

        m_pLogger->log(LOG_INFO, "SectionBoardGeneral::sendProtocolPacket %c %s", m_section + SECTION_CHAR_A, pPayLoad);

		int res = sendCanMsg( pPayLoad, usLen, pucAnsMsg, usTimeoutMs );
		if ( res == 0 )
		{
			ulIdxPktOrder++;
			/* STX | o | ‘P’ | f | ETX */
            m_pLogger->log(LOG_INFO,"SectionBoardGeneral::sendProtocolString %c -->Performed", m_section + SECTION_CHAR_A);
		}
		else
		{
            m_pLogger->log(LOG_ERR,"SectionBoardGeneral::sendProtocolString %c -->ERROR", m_section + SECTION_CHAR_A);
			return -1;
		}

		if ( ulEndTx == 1 )
			break;
	}

	return 0;
}

int SectionBoardGeneral::executeProtocol( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘X’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'X';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg,usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘X’ | f | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::executeProtocol %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::executeProtocol %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::continueProtocol( uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘y’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'y';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘y’ | f | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::continueProtocol %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::continueProtocol %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::forceProtocol( void )
{

	return 0;
}

int SectionBoardGeneral::abortProtocol( uint32_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘A’ | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'A';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘A’ | f | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::abortProtocol %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::abortProtocol %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}
	return 0;
}

int SectionBoardGeneral::setProtocolRunningFlag(uint8_t active, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 'c' | active | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'c';
	pPayLoad[2] = active;

	int res = sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘c’ | f | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::setProtocolRunningFlag %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::setProtocolRunningFlag %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::sleepOrDisable(uint8_t active, uint8_t mode, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 'E' | active | mode | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'E';
	pPayLoad[2] = active;
	pPayLoad[3] = mode;

	int res = sendCanMsg( pPayLoad, 4, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘E’ | f | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::sleepOrDisable %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::sleepOrDisable %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}

int SectionBoardGeneral::resetSoftware(uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 'T' | ETX */
	pPayLoad[0] = SECTION_GENERAL_CHAR;
	pPayLoad[1] = 'T';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘T’ | f | ETX */
        m_pLogger->log(LOG_INFO,"SectionBoardGeneral::resetSW %c -->Performed", m_section + SECTION_CHAR_A);
	}
	else
	{
        m_pLogger->log(LOG_ERR,"SectionBoardGeneral::resetSW %c -->ERROR", m_section + SECTION_CHAR_A);
		return -1;
	}

	return 0;
}


/*!***************************************************************************
 * PACKETS MANAGER                                                           *
 * ***************************************************************************/
int SectionBoardGeneral::spreadMsg( uint8_t * pMsg )
{
	if ( pMsg == NULL )
	{
		return -1;
	}
	else
	{
        switch ( pMsg[IDX_CAN_TYPE_MSG] )
		{
			case 'Z': // is managed in the notify event
			{
				char      pcDescription[SCTB_MAX_DESCRIPTION];
                decodeNotifyEvent( (char *)pMsg, pcDescription );
                m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::spreadMsg %c -->%s", m_section + SECTION_CHAR_A, pcDescription);
			}
			break;

			case 'z':
			{
                sendToMsgQueueAutoAnsw( (char *)pMsg, strlen((char*)pMsg) );
			}
			break;

			default:
			{
                sendToMsgQueueAnsw( (char *)pMsg, strlen((char*)pMsg) );
                m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::spreadMsg %c -->sendToMsgQueueAnsw num byte %d",
                               m_section + SECTION_CHAR_A, strlen((char*)pMsg));
			}
			break;
		}
	}

	return 0;
}

double SectionBoardGeneral::getTemperatureSPR()
{
	return m_fTemperatureSPR;
}

double SectionBoardGeneral::getTemperatureTray()
{
	return m_fTemperatureTray;
}

uint16_t SectionBoardGeneral::getIntTemperatureSPR()
{
	return (uint16_t)(m_fTemperatureSPR * 100);
}

uint16_t SectionBoardGeneral::getIntTemperatureTray()
{
	return (uint16_t)(m_fTemperatureTray * 100);
}

void SectionBoardGeneral::clearSectionReady()
{
    m_sectionReady = false;
}

bool SectionBoardGeneral::getSectionReady()
{
	return m_sectionReady;
}

void SectionBoardGeneral::clearProtocolFlags()
{
	m_protocolWindowJ = false;
	m_protocolEnd = false;
	m_protocolAborted = false;
}

void SectionBoardGeneral::clearProtocolWindowFlag()
{
	m_protocolWindowJ = false;
}

void SectionBoardGeneral::clearProtocolAbortedFlag()
{
	m_protocolAborted = false;
}

void SectionBoardGeneral::clearProtocolEndFlag()
{
	m_protocolEnd = false;
}

bool SectionBoardGeneral::getProtocolWindowFlag()
{
	return m_protocolWindowJ;
}

bool SectionBoardGeneral::getProtocolAbortedFlag()
{
	return m_protocolAborted;
}

bool SectionBoardGeneral::getProtocolEndFlag()
{
	return m_protocolEnd;
}


eDoorStatus SectionBoardGeneral::getDoorStatus()
{
	return m_doorStatus;
}

std::string SectionBoardGeneral::getDoorStatusAsString()
{
	std::string strStatus;

	strStatus.clear();

	switch(m_doorStatus)
	{
		case eDoorOpen:
			strStatus.assign(SECTION_DOOR_OPEN_STRING);
		break;
		case eDoorClosed:
			strStatus.assign(SECTION_DOOR_CLOSE_STRING);
		break;
		case eDoorUnknown:
		default:
		break;
	}
	return strStatus;
}

bool SectionBoardGeneral::isDoorClosed()
{
	return (m_doorStatus == eDoorClosed);
}

bool SectionBoardGeneral::getSensorPump()
{
	return m_sensorPump;
}

bool SectionBoardGeneral::getSensorSpr()
{
	return m_sensorSpr;
}

void SectionBoardGeneral::setSensorSpr(bool status)
{
	m_sensorSpr = status;
}

bool SectionBoardGeneral::getSensorTray()
{
	return m_sensorTray;
}

bool SectionBoardGeneral::getSensorTower()
{
	return m_sensorTower;
}

bool SectionBoardGeneral::getStartButton()
{
	return m_pushbuttonStatus;
}

void SectionBoardGeneral::clearStartButton()
{
	m_pushbuttonStatus = false;
}

uint16_t SectionBoardGeneral::getVoltage24V()
{
	return m_usVoltageP24V;
}

uint16_t SectionBoardGeneral::getVoltage3V3()
{
	return m_usVoltageP3V3;
}

uint16_t SectionBoardGeneral::getVoltage5V()
{
	return m_usVoltageP5V;
}

uint16_t SectionBoardGeneral::getVoltage1V2()
{
	return m_usVoltageP1V2;
}

int SectionBoardGeneral::decodeAsynchronousMsg( uint8_t *pMsg )
{
	uint8_t uciTypeAsMsg = pMsg[IDX_CAN_FLAG_MSG];
	uint8_t ucIdxMsg = IDX_CAN_DATA_MSG;
	uint16_t movement;

    m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::decodeAsynchronousMsg %c --> %s", m_section + SECTION_CHAR_A, pMsg);

	switch (uciTypeAsMsg)
	{
		case '0':
		{
			// current temperatures
			uint16_t usTempSPR	= getVal4x((char*)&pMsg[ucIdxMsg]);
			ucIdxMsg	= ucIdxMsg + 4;
			uint16_t usTempTray	= getVal4x((char*)&pMsg[ucIdxMsg]);
			double fTempSPRCnv = m_NTCParam.convertNTCValue(usTempSPR);
			double fTempTrayCnv = m_NTCParam.convertNTCValue(usTempTray);
			m_pLogger->log(LOG_INFO, "SectionBoardGeneral %c -> fTempSPR=%.2f (%d) fTempTray=%.2f (%d)",
                           m_section + SECTION_CHAR_A, fTempSPRCnv, usTempSPR, fTempTrayCnv, usTempTray);

			if(fTempSPRCnv != 0) m_fTemperatureSPR = fTempSPRCnv;
			if(fTempTrayCnv != 0) m_fTemperatureTray = fTempTrayCnv;

			if(fTempSPRCnv != 0 || fTempTrayCnv != 0)
			{
				if(m_ptrMessageFunction)
					m_ptrMessageFunction(m_pSectionBoardLink->m_sectionId,
										 SECTION_GENERAL_DEVICE, SECTION_EVENT_TEMPERATURE);
			}
		}
		break;

		case '1':
		{
			/* current number of Pump steps */
			movement = getVal8x((char*)&pMsg[ucIdxMsg]);
			// add to the stored one
			if(m_section == SCT_A_ID)
				infoSingleton()->addInstrumentCounter(eCounterPumpA, movement);
			else
				infoSingleton()->addInstrumentCounter(eCounterPumpB, movement);

            m_pLogger->log(LOG_INFO, "SectionBoardGeneral::decodeAsynchronousMsg %c --> PumpMovement=%d", m_section + SECTION_CHAR_A, movement);
		}
		break;

		case '2':
		{
			/* current number of Tower steps */
			movement = getVal8x((char*)&pMsg[ucIdxMsg]);

			// add to the stored one
			if(m_section == SCT_A_ID)
				infoSingleton()->addInstrumentCounter(eCounterTowerA, movement);
			else
				infoSingleton()->addInstrumentCounter(eCounterTowerB, movement);

            m_pLogger->log(LOG_INFO, "SectionBoardGeneral::decodeAsynchronousMsg %c --> TowerMovement=%d", m_section + SECTION_CHAR_A, movement);
		}
		break;

		case '3':
		{
			/* current number of Tray steps */
			movement = getVal8x((char*)&pMsg[ucIdxMsg]);

			// add to the stored one
			if(m_section == SCT_A_ID)
				infoSingleton()->addInstrumentCounter(eCounterTrayA, movement);
			else
				infoSingleton()->addInstrumentCounter(eCounterTrayB, movement);

            m_pLogger->log(LOG_INFO, "SectionBoardGeneral::decodeAsynchronousMsg %c --> TrayMovement=%d", m_section + SECTION_CHAR_A, movement);
		}
		break;

		case '4':
		{
			/* device that failed during the Mechanic Reset */
			uint8_t	ubDeviceFailed = pMsg[ucIdxMsg];
			switch ( ubDeviceFailed )
			{
				case SECTION_PUMP_CHAR:
				case SECTION_TOWER_CHAR:
				case SECTION_TRAY_CHAR:
				case SECTION_SPR_CHAR:
                    m_pLogger->log(LOG_INFO, "SectionBoardGeneral::decodeAsynchronousMsg %c --> Device Failed=%c",
                                   m_section + SECTION_CHAR_A, ubDeviceFailed);
				break;

				default:
                    m_pLogger->log(LOG_ERR, "SectionBoardGeneral::decodeAsynchronousMsg %c --> Error0", m_section + SECTION_CHAR_A);
					return -1;
				break;
			}
		}
		break;

		case '6':// feedback from powerfail line
            // no more present
		break;

		case '7':
		{
			/* current number of SPR steps */
			movement = getVal8x((char*)&pMsg[ucIdxMsg]);

			// add to the stored one
			if(m_section == SCT_A_ID)
				infoSingleton()->addInstrumentCounter(eCounterSprA, movement);
			else
				infoSingleton()->addInstrumentCounter(eCounterSprB, movement);

            m_pLogger->log(LOG_INFO, "SectionBoardGeneral::decodeAsynchronousMsg %c --> SprMovement=%d", m_section + SECTION_CHAR_A, movement);
		}
		break;

		case '8':
		{
			// home sensor status
			uint8_t	sensorDevice = pMsg[ucIdxMsg];
			uint8_t sensorStatus = pMsg[ucIdxMsg + 1];
			switch ( sensorDevice )
			{
				case SECTION_PUMP_CHAR:
					m_sensorPump = (sensorStatus == '1');
				break;
				case SECTION_TOWER_CHAR:
					m_sensorTower = (sensorStatus == '1');
				break;
				case SECTION_TRAY_CHAR:
					m_sensorTray = (sensorStatus == '1');
				break;
				case SECTION_SPR_CHAR:
					m_sensorSpr = (sensorStatus == '1');
				break;

				default:
                    m_pLogger->log(LOG_ERR, "SectionBoardGeneral::decodeAsynchronousMsg %c --> Error1", m_section + SECTION_CHAR_A);
					return -1;
				break;
			}
            m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardGeneral::sensorstatus %c %d = %d", m_section + SECTION_CHAR_A, sensorDevice, sensorStatus);
		}
		break;

		case '9':
		{
            // start pushbutton status : if the status changes -> send notify to SW
			bool bVidasEp = false;
			if(pMsg[ucIdxMsg] == '1')
			{
                bVidasEp = true;
			}
            m_pLogger->log(LOG_INFO, "SectionBoardGeneral::pushbutton %c --> %d", m_section + SECTION_CHAR_A, bVidasEp);
			if(bVidasEp)
			{
                if(requestStartButton(m_section) == 0)
                {
                    char bufferTmp[64];
                    std::string strTmp;

                    sprintf(bufferTmp, "Section startButton %c --> %d", m_section + SECTION_CHAR_A, bVidasEp);
                    strTmp.assign(bufferTmp);
                    registerGenericEvent(strTmp);
                }
			}
		}
		break;

		case 'A':
		{
			// door sensor status
			uint8_t sensorStatus = pMsg[ucIdxMsg];
			if(sensorStatus == '0')
				m_doorStatus = eDoorOpen;
			else
				m_doorStatus = eDoorClosed;
			// notify to gateway
			requestVidasEp();
            m_pLogger->log(LOG_INFO, "SectionBoardGeneral %c door = %d", m_section + SECTION_CHAR_A, m_doorStatus);

			char bufferTmp[64];
			std::string strTmp;

            sprintf(bufferTmp, "Section %c doorStatus = %d", m_section + SECTION_CHAR_A, m_doorStatus);
			strTmp.assign(bufferTmp);
			registerGenericEvent(strTmp);
		}
		break;

		default:
			//Error
            m_pLogger->log(LOG_ERR, "SectionBoardGeneral::decodeAsynchronousMsg %c --> Error1", m_section + SECTION_CHAR_A);
			return -1;
		break;
	}

	return 0;
}

bool SectionBoardGeneral::decodeNotifyEvent( char * pubMsg, char * pcDescription )
{
	bool bDecoded = false;

	if(pcDescription) *pcDescription = 0;

	// first test the default function that checks the errors defined in SECTION.err file
	if(CanBoardBase::decodeNotifyEvent(pubMsg, pcDescription) == true)
	{
		return true;
	}

	// check other events from sectionl
	char strDescription[64];
	uint8_t uciTypeAsMsg = pubMsg[IDX_CAN_FLAG_MSG];

	switch(uciTypeAsMsg)
	{
		case 'N':
			// Section Events
			switch(pubMsg[IDX_CAN_FLAG_MSG + 1])
			{
				case '0':
					// Board ready
					m_sectionReady = true;
					sprintf(strDescription, "%s -> Section Ready", pubMsg);
					bDecoded = true;
				break;
			}
		break;

		case 'O' :
			// Protocol Events
			switch(pubMsg[IDX_CAN_FLAG_MSG + 1])
			{
				case '0':
					// J window reached
					m_protocolWindowJ = true;
					bDecoded = true;
					sprintf(strDescription, "%s -> J window", pubMsg);
				break;
				case '1':
					// protocol aborted
					m_protocolAborted = true;
					bDecoded = true;
					sprintf(strDescription, "%s -> Protocol Abort", pubMsg);
				break;
				case '2':
					// protocol completed
					m_protocolEnd = true;
					bDecoded = true;
					sprintf(strDescription, "%s -> Protocol End", pubMsg);
				break;
			}
		break;
	}

	if(bDecoded == false)
	{
		sprintf(strDescription, "%s -> (TBD)", pubMsg);
	}

	if(pcDescription) strcpy(pcDescription, strDescription);
    m_pLogger->log(LOG_INFO, "SectionBoardGeneral::decodeNotifyEvent %c %s", m_section + SECTION_CHAR_A, strDescription);

	return true;
}

