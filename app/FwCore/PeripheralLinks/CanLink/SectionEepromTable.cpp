/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionEepromTable.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionEepromTable class.
 @details

 ****************************************************************************
*/

#include "SectionEepromTable.h"

SectionEepromTable::SectionEepromTable()
{
	memset(&m_AspirationLowThreshold, 0, sizeof(m_AspirationLowThreshold));
	memset(&m_AspirationHighThreshold, 0, sizeof(m_AspirationHighThreshold));
	memset(&m_AspirationIndustryThreshold, 0, sizeof(m_AspirationIndustryThreshold));
	memset(&m_Volumes, 0, sizeof(m_Volumes));
	memset(&m_Speed, 0, sizeof(m_Speed));
}

SectionEepromTable::~SectionEepromTable()
{

}

int SectionEepromTable::setVersion( enumSectionEepromTableType eTableType, uint8_t * ubVal )
{
	if ( ubVal == NULL )
	{
		return -1;
	}
	else
	{
		switch (eTableType)
		{
			case eAspirationLowThresholdTable:
			{
				memcpy(m_AspirationLowThreshold.ubVersion, ubVal, SECTION_EEPROM_TABLE_VERSION_SIZE);
			}
			break;

			case eAspirationHighThresholdTable:
			{
				memcpy(m_AspirationHighThreshold.ubVersion, ubVal, SECTION_EEPROM_TABLE_VERSION_SIZE);
			}
			break;

			case eVolumeTable:
			{
				memcpy(m_Volumes.ubVersion, ubVal, SECTION_EEPROM_TABLE_VERSION_SIZE);
			}
			break;

			case eSpeedTable:
			{
				memcpy(m_Speed.ubVersion, ubVal, SECTION_EEPROM_TABLE_VERSION_SIZE);
			}
			break;

			case eAspirationIndustryThresholdTable:
			{
				memcpy(m_AspirationIndustryThreshold.ubVersion, ubVal, SECTION_EEPROM_TABLE_VERSION_SIZE);
			}
			break;

			default:
				return -1;
			break;
		}
	}

	return 0;
}

void SectionEepromTable::printVersion( enumSectionEepromTableType eTableType )
{
	structSectionEepromTable	*sectionTable;

	switch (eTableType)
	{
		case eAspirationLowThresholdTable:
			sectionTable = &m_AspirationLowThreshold;
		break;

		case eAspirationIndustryThresholdTable:
			sectionTable = &m_AspirationIndustryThreshold;
		break;

		case eAspirationHighThresholdTable:
			sectionTable = &m_AspirationHighThreshold;
		break;

		case eVolumeTable:
			sectionTable = &m_Volumes;
		break;

		case eSpeedTable:
			sectionTable = &m_Speed;
		break;

		default:
		break;
	}

	printf("Version: %d.%d.%d.%d.%d.%d\n",	sectionTable->ubVersion[0], sectionTable->ubVersion[1],
											sectionTable->ubVersion[2], sectionTable->ubVersion[3],
											sectionTable->ubVersion[4], sectionTable->ubVersion[5]);
}

int SectionEepromTable::setValAspirationTsTable( enumSectionEepromTableType eType, uint32_t ulVal, uint8_t ubRow, uint8_t ubColumn )
{
	structSectionEepromTable	*sectionTable;

	switch (eType)
	{
		case eAspirationLowThresholdTable:
			sectionTable = &m_AspirationLowThreshold;
		break;

		case eAspirationHighThresholdTable:
			sectionTable = &m_AspirationHighThreshold;
		break;

		case eAspirationIndustryThresholdTable:
			sectionTable = &m_AspirationIndustryThreshold;
		break;

		default:
			printf("SectionEepromTable::setValAspirationTsTable-->No table esists..");
			return -1;
		break;
	}

	if ((ubRow >= SECTION_EEPROM_TABLE_SIZE) || (ubColumn >= SECTION_EEPROM_TABLE_SIZE))
	{
		return -1;
	}
	else
	{
		sectionTable->ulMatrix[ubRow][ubColumn] = ulVal;
	}

	return 0;
}

int SectionEepromTable::setValVolumes( int32_t slVal, uint8_t ubRow )
{
	if (ubRow >= SECTION_EEPROM_TABLE_SIZE)
	{
		return -1;
	}
	else
	{
		m_Volumes.slBuff[ubRow] = slVal;
	}

	return 0;
}

int SectionEepromTable::setValSpeed(int32_t slVal, uint8_t ubRow, uint8_t ubColumn)
{
	if (ubRow >= SECTION_EEPROM_TABLE_SIZE)
	{
		return -1;
	}
	else
	{
		m_Speed.slMatrix[ubRow][ubColumn] = slVal;
	}

	return 0;
}

void SectionEepromTable::printRowAspirationTsTable( enumSectionEepromTableType eType, uint8_t ubIndexRow )
{
	structSectionEepromTable	*sectionTable;

	switch (eType)
	{
		case eAspirationLowThresholdTable:
			sectionTable = &m_AspirationLowThreshold;
		break;

		case eAspirationIndustryThresholdTable:
			sectionTable = &m_AspirationIndustryThreshold;
		break;

		case eAspirationHighThresholdTable:
			sectionTable = &m_AspirationHighThreshold;
		break;

		default:
			printf("SectionEepromTable::setValAspirationTsTable-->No table esists..");
		break;
	}

	printf("-->TableThreshold Row %d: ",ubIndexRow);
	for (int j = 0; j < SECTION_EEPROM_TABLE_SIZE; j++)
	{
		printf(" [%d]=%d,", j, sectionTable->ulMatrix[ubIndexRow][j]);
	}
	printf("\n");
}

void SectionEepromTable::printValAspirationTable( enumSectionEepromTableType eType )
{
	structSectionEepromTable	*sectionTable;

	switch (eType)
	{
		case eAspirationLowThresholdTable:
			sectionTable = &m_AspirationLowThreshold;
		break;

		case eAspirationIndustryThresholdTable:
			sectionTable = &m_AspirationIndustryThreshold;
		break;

		case eAspirationHighThresholdTable:
			sectionTable = &m_AspirationHighThreshold;
		break;

		default:
			printf("SectionEepromTable::setValAspirationTsTable-->No table esists..");
		break;
	}

	for (int i = 0; i < SECTION_EEPROM_TABLE_SIZE; i++)
	{
		printf("-->Threshold Row %d: ",i);
		for (int j = 0; j < SECTION_EEPROM_TABLE_SIZE; j++)
		{
			printf(" [%d]=%d,", j, sectionTable->ulMatrix[i][j]);
		}
		printf("\n");
	}
}

void SectionEepromTable::printValVolumesTable( void )
{

	printf("-->VolumeTable : ");
	for (int j=0; j<SECTION_EEPROM_TABLE_SIZE; j++)
	{
		printf(" [%d]=%d,", j, m_Volumes.slBuff[j]);
	}
	printf("\n");

}

void SectionEepromTable::printValSpeedTable( void )
{
	for (int i=0; i<SECTION_EEPROM_TABLE_SIZE; i++)
	{
		printf("-->SpeedTable Row %d: ",i);
		for (int j=0; j<SECTION_EEPROM_TABLE_SIZE; j++)
		{
			printf(" [%d]=%d,", j, m_Speed.ulMatrix[i][j]);
		}
		printf("\n");
	}
}

void SectionEepromTable::printRowSpeedTable( uint8_t ubIndexRow )
{
	printf("-->SpeedTable Row %d: ",ubIndexRow);
	for (int j=0; j<SECTION_EEPROM_TABLE_SIZE; j++)
	{
		printf(" [%d]=%d,", j, m_Speed.ulMatrix[ubIndexRow][j]);
	}
	printf("\n");
}

int SectionEepromTable::getRowAspirationTsTable( enumSectionEepromTableType eType, uint8_t ubRow, uint32_t * ulBuffRow, uint32_t &ulChecksum )
{
	structSectionEepromTable	*sectionTable;

	switch (eType)
	{
		case eAspirationLowThresholdTable:
			sectionTable = &m_AspirationLowThreshold;
		break;

		case eAspirationIndustryThresholdTable:
			sectionTable = &m_AspirationIndustryThreshold;
		break;

		case eAspirationHighThresholdTable:
			sectionTable = &m_AspirationHighThreshold;
		break;

		default:
			printf("SectionEepromTable::getRowAspirationTsTable-->No table esists..");
			return -1;
		break;
	}

	ulChecksum = 0;
	if ((ubRow < SECTION_EEPROM_TABLE_SIZE) && (ulBuffRow != NULL))
	{
		for (int i = 0; i<SECTION_EEPROM_TABLE_SIZE; i++)
		{
			ulBuffRow[i] = sectionTable->ulMatrix[ubRow][i];
			ulChecksum += ulBuffRow[i];
		}
		return 0;
	}

	return -1;
}

int SectionEepromTable::getTableVolumes(uint8_t ubRow, int32_t *slBuffRow, uint32_t &ulChecksum)
{
	ulChecksum = 0;

	if ((ubRow < 2) && (slBuffRow != NULL))
	{
		for (int i = 0; i < SECTION_EEPROM_TABLE_SIZE; i++)
		{
			slBuffRow[i] = m_Volumes.slBuff[i];
			ulChecksum += slBuffRow[i];
		}
		return 0;
	}

	return -1;
}

int SectionEepromTable::getRowSpeed(uint8_t ubRow, int32_t *slBuffRow, uint32_t &ulChecksum)
{
	ulChecksum = 0;
	if ((ubRow < SECTION_EEPROM_TABLE_SIZE) && (slBuffRow != NULL))
	{
		for (int i = 0; i< SECTION_EEPROM_TABLE_SIZE; i++)
		{
			slBuffRow[i] = m_Speed.slMatrix[ubRow][i];
			ulChecksum += slBuffRow[i];
		}
		return 0;
	}

	return -1;
}

int SectionEepromTable::getVersion(enumSectionEepromTableType eTableType, uint8_t *ubVal)
{
	structSectionEepromTable	*sectionTable;

	switch (eTableType)
	{
		case eAspirationLowThresholdTable:
			sectionTable = &m_AspirationLowThreshold;
		break;

		case eAspirationIndustryThresholdTable:
			sectionTable = &m_AspirationIndustryThreshold;
		break;

		case eAspirationHighThresholdTable:
			sectionTable = &m_AspirationHighThreshold;
		break;

		case eVolumeTable:
			sectionTable = &m_Volumes;
		break;

		case eSpeedTable:
			sectionTable = &m_Speed;
		break;

		default:
		break;
	}

	memcpy(ubVal, sectionTable->ubVersion, SECTION_EEPROM_TABLE_VERSION_SIZE);

	return 0;
}









