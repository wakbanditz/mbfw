/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanBoardBase.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CanBoardBase class.
 @details

 ****************************************************************************
*/

#ifndef CANBOARDBASE_H
#define CANBOARDBASE_H

#include "Thread.h"
#include "CanIfr.h"
#include "Log.h"
#include "Mutex.h"
#include "MessageQueue.h"
#include "CommonInclude.h"
#include "SectionInclude.h"
#include "SectionBoardLink.h"

class SectionError;
class PressureError;
class GeneralError;

/*! *************************************************************************************************
 * @class	CanBoardBase
 * @brief	This is the base class for the managing of sub-systems parts that make up the section board.
 *			It contains the message queues used for the storage of can frame data packet and the pointer
 *			to SectionBoardLink object in order to have the possibility to send the message directly
 *			from that class.
 *			The thread can be used to manage asynchronous messages.
 ****************************************************************************************************/
class CanBoardBase : public Thread
{
	public:

		/*!
		 * @brief constructor
		 * @param section the section identifier
		 */
		CanBoardBase(int section);

		/*!
		 * @brief destructor
		 */
		virtual ~CanBoardBase();

		/*!
		 * @brief init				Initilization of the object.
		 * @param plogger
         * @param smsgQueueAns		name of msgQueue used for the storage answer canFrame
         * @param smsgQueueAutoAns  name of msgQueue used for the storage answer canFrame
		 * @param psectionBoardLink
		 * @return 0 if success, -1 otherwise
		 */
		int init( Log *plogger, const char* smsgQueueAns, const char *smsgQueueAutoAns, SectionBoardLink *psectionBoardLink );

		/*!
		 * @brief closeAndUnlinkMsgQueue
		 * @return 0 if success, -1 otherwise
		 */
		int closeAndUnlinkMsgQueue( void );

		/*!
		 * @brief sendToMsgQueueAnsw
		 * @param pmsgData
		 * @param nlen
		 * @return 0 if success, -1 otherwise
		 */
		int sendToMsgQueueAnsw(char *pmsgData, uint16_t nlen );

		/*!
		 * @brief sendToMsgQueueAutoAnsw
		 * @param pmsgData
		 * @param nlen
		 * @return 0 if success, -1 otherwise
		 */
		int sendToMsgQueueAutoAnsw( char *pmsgData, uint16_t nlen );

		/*!
		 * @brief sendCanMsg		It implements a state machine able to send Msg to the specified CAN link
		 *							and receive the related answer.
		 *							It is composed by three states: the first send the Msg to the link, the second
		 *							waits for command accepted answer
		 *							and the third waits for the real answer.
		 * @param pMsg				the command string
		 * @param nlen				the command string length
		 * @param pucAnsMsg			the reply buffer string
		 * @param usMillisecToWait	the command reply timeout in msecs
		 * @param bErrorSet			if true: in case of error on the command execution set the error in state machine
		 * @return					0 if success, -1 otherwise
		 */
        int sendCanMsg(uint8_t * pMsg, uint16_t nlen, uint8_t *pucAnsMsg, uint32_t usMillisecToWait, bool bErrorSet = true);

		/*!
		 * @brief spreadMsg			The function allows the routing of 'z', 'Z' or normal type messages
		 *							to the specified message queue.
		 * @return					0 if success, -1 otherwise
		 */
		virtual int spreadMsg( uint8_t * pMsg ) = 0;

		/*!
		 * @brief stopAsynchronousThread	Just a wrapper to stopThread function.
		 * @return
		 */
		void stopAsynchronousMsgReceiveThread( void );

		/*!
		 * @brief addCallbackEvent	It defines the callback function to be invoked in case of asynchronous events
		 * @param ptrFunction		pointer to the callback function
		 */
		void addCallbackEvent(int (*ptrFunction)(int section, int device, int event));

		/*!
		 * @brief clearAllErrors	to reset the active flag of all errors (at board initializaztion)
		 */
		void clearAllErrors();

		/*!
		 * @brief getCodedFromLabel	The function searches in the Coded vector the element with the corresponding
		 *		label and returns the linked coded string to be sent to section
		 * @param strLabel		the Label string
		 * @return		the coded string
		 */
		std::string getCodedFromLabel(std::string strLabel);

		/*!
		 * @brief getCalibrationFromLabel	The function searches in the Calibration vector the element with the
		 *  corresponding label and returns the linked Calibration string to be sent to section
		 * @param strLabel		the Label string
		 * @return		the calibration string
		 */
		std::string getCalibrationFromLabel(string strLabel);

		/*!
		 * @brief getCalibrationLabelList	The function fills the vector in input with all the calibration
		 *		labels as read from configuration file
		 * @param pStrVector	pointer to the Label output vector
		 * @return		the number of elements copied
		 */
		int getCalibrationLabelList(std::vector<std::string> * pStrVector);

		/*!
		 * @brief isCalibrationLabelValid	The function checks if the requested lcalibratin label is present
		 * in the list read from file
		 * @param strLabel	the string to be verified
		 * @return	true if strLabel correspond to a calibration
		 */
		bool isCalibrationLabelValid(std::string strLabel);

		/*! ***********************************************************************************************************
		 * @brief updateErrorsTime to set the correct time of the errors occurred when time was not yet set
		 * @param currentMsec the startup time in msecs
		 * @param currentTime pointer to current time structure
		 * @return the number of changed items
		 * ************************************************************************************************************
		 */
		int updateErrorsTime(uint64_t currentMsec, struct tm * currentTime);

		/*! ***********************************************************************************************************
		 * @brief clearTemperatureError to clear the active flag for Temperature errors (when temperature ok is received)
		 * @return true if at least an error has been cleared, false otherwise
		 * ************************************************************************************************************
		 */
		bool clearTemperatureError();

		/*! ***********************************************************************************************************
		 * @brief getErrorList retrieve the errors active on the section: each error corresponds to an element
		 *			in the vector: the string is formatted as timestamp#code#category#description
		 * @param pVectorList pointer to the list
         * @param errorLevel the level error by which the errors are filtered
		 * @return the number of elements in the vector
		 * ************************************************************************************************************
		 */
        int getErrorList(std::vector<std::string> * pVectorList, uint8_t errorLevel = GENERAL_ERROR_LEVEL_NONE);

		/*! ***********************************************************************************************************
		 * @brief removePressureErrors clear the list of active PressureErrors detected during protocol
		 * ************************************************************************************************************
		 */
		void removePressureErrors();

		/*! ***********************************************************************************************************
		 * @brief getPressureErrorListPerStripWell retrieve the pressure errors active on the section
		 *				selecting the well and strip
		 *
		 * @param pVectorList pointer to the list
         * @param errorLevel the level error by which the errors are filtered
         * @param strip the strip index (as char '0'-'5')
		 * @param well the strip index (as char '0'-'9')
		 * @return the number of elements in the vector
		 * ************************************************************************************************************
		 */
		int getPressureErrorListPerStripWell(std::vector<string>* pVectorList, uint8_t strip = SCT_NUM_TOT_SLOTS,
                                                                                uint8_t well = SCT_SLOT_NUM_WELLS,
                                                                                uint8_t errorLevel = GENERAL_ERROR_LEVEL_NONE);

        /*! ***********************************************************************************************************
         * @brief isErrorActive check if at least one ERROR is active (status = error)
         *
         * @return true if error is active
         * ************************************************************************************************************
         */
        bool isErrorActive();

		/*! ***********************************************************************************************************
		 * @brief cleanQueues cleanup of the CSN messages queues
		 * @return true if successful, false otherwise
		 * ************************************************************************************************************
		 */
		bool cleanQueues();

        /*!
         * @brief setEventToDecode wrapper function for the decodeNotifyEvent
         * @param firstChar the first char of the message
         * @param secondChar the second char of the message
         * @return true if error is correctly associated, false otherwise
         */
        bool setEventToDecode(char firstChar, char secondChar);

        /*! ***********************************************************************************************************
         * @brief registerUnknownReplyFromSection if the Section replies with a 'I' the SectionLinkBoard notifies
         *          this message to all the devices
         * @return true if successful (the device was sending a CAN message), false otherwise
         * ************************************************************************************************************
         */
        bool registerUnknownReplyFromSection();

    protected:
        /*!
         * @brief	getAnswTimed	Controls if there is canFrame message in the queue for ntimeToWait ms then
         *							return with payload and the lenght of the packet
         * @param ppayLoad
         * @param ppayloadLenAnsw
         * @param ntimeToWait
         * @return 0 if it returns without errors
         */
        int	getAnswTimed( uint8_t * ppayLoad, uint16_t *ppayloadLenAnsw, uint32_t ntimeToWait );

        /*!
         * @brief waitAnswMsg
         * @param devAddr
         * @param cmdType
         * @param pBuffPayLoad
         * @param ntimeToWait
         * @return
         */
        int	waitAnswMsg(uint8_t devAddr, uint8_t cmdType, uint8_t *pBuffPayLoad, uint32_t ntimeToWait );

        /*!
         * @brief decodeAsynchronousMsg		Manages the type of asynchronous message that is stored in the related message queue.
         * @param pMsg
         * @return 0 if success, -1 otherwise
         */
        virtual int decodeAsynchronousMsg( uint8_t * pMsg ) = 0;

        /*!
         * @brief decodeNotifyEvent
         * Manages notify event message.
         * @param pubMsg the message to be decoded
         * @param pcDescription the output buffer where the error desciption is copied
         * @return true if error is correctly associated, false otherwise
         */
        virtual bool decodeNotifyEvent(char *pubMsg, char *pcDescription);

        /*!
         * @brief buildErrorVector
         * Build the list of Errors associated to the device.
         * @return
         */
        int buildErrorVector();

        /*!
         * @brief buildPositionsVector
         * Build the list of Calibrations and Coded positions associated to the device.
         * @return size of the vector
         */
        int buildPositionsVector();

        /*!
         * @brief	workerThread	The functions is waiting on receive message queue buffer for asynchronous message
         *							 by the section board.
         *							When the buffer has the mq it proceeds with the dispatching of the message data,
         * @return 0 if success, -1 otherwise
         */
        int		workerThread( void );

        /*!
         * @brief	beforeWorkerThread	Function to be called before starting the thread
         */
        void	beforeWorkerThread( void );

        /*!
         * @brief	afterWorkerThread	Function to be called after ending the thread
         */
        void	afterWorkerThread( void );

    private:
		/*!
		 * @brief setAutoAnswMsgQueue	The functions tries to open m_msgQueueAutoAnsw if the queue exists otherwise
		 *								it performs the creation of the specified message queue.
		 * @param smsgQueueAutoAns		name of message queue to create
		 * @return						0 if success, -1 otherwise
		 */
		bool setAutoAnswMsgQueue( const char *smsgQueueAutoAns );

		/*!
		 * @brief parseBoardErrorFile	The functions parse the content of the ERR file defining the list of errors
		 *					and the associated parametrs
		 * @param pVectorErrors		pointer to the error vector corresponding to the file
		 * @param errorType			kind of error object to create
		 * @return					the size of the vector
		 */
		int parseBoardErrorFile(std::vector< GeneralError * > * pVectorErrors, uint8_t errorType);

		/*!
		 * @brief searchEventError	The functions scans the vector in input to find the SectionErr object corresponding
		 *		to the message received from section
		 * @param pubMsg		the buffer containing the message from Section board
		 * @param pVectorErrors		pointer to the error vector to be scanned (Errors or Pressures)
		 * @param bActiveDisabled	if the object has to be still not activated (Error) or don't care (PRESSURE)
		 * @param pSectionErrorFound	output: the pointer to the SectionErr identified
		 * @param pcDescription		buffer to contain the error decription
		 * @return					0 -> no correspondance found, 1 -> object found, 3 -> object found and set
		 */
		int searchEventError(char* pubMsg, std::vector< SectionError * > * pVectorErrors,
							 bool bActiveDisabled,   SectionError * pSectionErrorFound, char* pcDescription);


    protected:

        SectionBoardLink	*m_pSectionBoardLink;
        Log					*m_pLogger;

        MessageQueue		*m_pMsgQueueAnsw;
        MessageQueue		*m_pMsgQueueAutoAnsw;

        // callback function associated to each event defined by user
        int (* m_ptrMessageFunction)(int section, int device, int event);

        std::string			m_strDevice;
        int					m_section;
        std::vector< SectionError * > m_vectorErrors;
        std::vector< SectionError * > m_vectorPressures;
        std::vector< PressureError * > m_vectorPressuresActive;

        std::vector<std::pair<std::string, std::string>> m_vectorCalibration;
        std::vector<std::pair<std::string, std::string>> m_vectorCoded;

        bool    m_bSendInProgress, m_bUnknownReplyFromSection;

};

#endif // CANBOARDBASE_H
