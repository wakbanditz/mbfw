/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanUtility.cpp
 @author  BmxIta FW dept
 @brief   Contains the support functions for the CanLink classes.
 @details

 ****************************************************************************
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include "CanLinkCommonInclude.h"

int getValD(char *p)
{
	int i;
	sscanf(p, "%1d", &i);

	return i;
}

int getVal1x(char *p)
{
	int i;
	sscanf(p, "%1X", &i);

	return(i);
}

int getVal2x( char *p )
{
	int i;
	sscanf(p, "%2X", &i);

	return(i);
}

int getVal4x(char *p)
{
	int i;
	sscanf(p, "%4X", &i);

	return(i);
}

int getVal6x(char *p)
{
	int i;
	sscanf(p, "%6X", &i);

	return(i);
}

int getVal8x(char *p)
{
	int i;
	sscanf(p, "%8X", &i);

	return(i);
}

int getAsciiValue(signed char ubVal)
{
	int iRes = 0;

	if ( (ubVal <= 9) && (ubVal >= 0) )
	{
		iRes = ubVal + 0x30;
	}
	else if ((ubVal >= 10) && (ubVal <= 35))
	{
		iRes = ubVal + 0x37;
	}
	else
	{

	}


	return iRes;
}

