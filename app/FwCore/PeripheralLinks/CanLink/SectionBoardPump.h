/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardPump.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionBoardPump class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONBOARDPUMP_H
#define SECTIONBOARDPUMP_H

#include "CanBoardBase.h"
#include "CanLinkCommonInclude.h"
#include "SectionCommonMessage.h"

/*!
 * @class SectionBoardPump
 * @brief The SectionBoardPump class
 */
class SectionBoardPump : public CanBoardBase
{
	public:

        SectionBoardPump(int section);

        virtual ~SectionBoardPump();

		int addCommonMessage( void );

		/*!***************************************************************************
		 * COMMAND PUMP																 *
		 * ***************************************************************************/
        int getSensor( bool &bHomePump, uint16_t usTimeoutMs  = DEFAULT_REQUEST_DELAY);

        int setSerialNumber( string sSerialNumber, uint16_t usTimeoutMs  = DEFAULT_REQUEST_DELAY);
        int getSerialNumber( string &sSerialNumber, uint16_t usTimeoutMs  = DEFAULT_REQUEST_DELAY);

		int aspirateVolume( uint8_t ubVolTable, uint8_t ubSpeedTable, uint16_t usTimeoutMs );
		int dispenseVolume( uint8_t ubVolTable, uint8_t ubSpeedTable, uint16_t usTimeoutMs );

		/*!***************************************************************************
		 * PACKETS MANAGER                                                           *
		 * ***************************************************************************/
		/*!
		 * @brief spreadMsg
		 *		  The payload message is sent to the related message queue according to the received type message
		 * @param pMsg
		 * @return 0 if success, -1 otherwise
		 */
		int spreadMsg( uint8_t *pMsg );

	protected:
		int decodeAsynchronousMsg( uint8_t * pMsg );
		bool decodeNotifyEvent( char *pubMsg , char *pcDescription );

    public:
        SectionCommonMessage	m_CommonMessage;

};

#endif // SECTIONBOARDPUMP_H
