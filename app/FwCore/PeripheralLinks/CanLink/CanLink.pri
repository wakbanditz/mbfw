INCLUDEPATH += $$PWD/../CanLink/

HEADERS += \
	$$PWD/CanBoardBase.h \
	$$PWD/CanLinkCommonInclude.h \
	$$PWD/CanLinkBoard.h \
	$$PWD/SectionBoardLink.h \
	$$PWD/SectionBoardGeneral.h \
	$$PWD/SectionNTCParameters.h \
	$$PWD/SectionEepromTable.h \
	$$PWD/SectionBoardTray.h \
	$$PWD/SectionCommonMessage.h \
	$$PWD/SectionBoardPump.h \
        $$PWD/SectionBoardTower.h \
        $$PWD/SectionBoardSPR.h \
        $$PWD/SectionBoardPIB.h \
        $$PWD/CanLink.h \
        $$PWD/CanLinkUpFw.h



SOURCES += \
	$$PWD/CanBoardBase.cpp \
	$$PWD/CanLinkBoard.cpp \
	$$PWD/SectionBoardLink.cpp \
	$$PWD/SectionBoardGeneral.cpp \
	$$PWD/SectionNTCParameters.cpp \
	$$PWD/SectionEepromTable.cpp \
	$$PWD/CanUtility.cpp \
	$$PWD/SectionBoardTray.cpp \
	$$PWD/SectionCommonMessage.cpp \
	$$PWD/SectionBoardPump.cpp \
	$$PWD/SectionBoardTower.cpp \
	$$PWD/SectionBoardSPR.cpp \
	$$PWD/SectionBoardPIB.cpp \
        $$PWD/CanLinkUpFw.cpp
