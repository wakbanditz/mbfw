/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardTray.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionBoardTray class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONBOARDTRAY_H
#define SECTIONBOARDTRAY_H

#include "CanBoardBase.h"
#include "CanLinkCommonInclude.h"
#include "SectionCommonMessage.h"

/*!
 * @class SectionBoardTray
 * @brief The SectionBoardTray class
 */
class SectionBoardTray : public CanBoardBase
{
	public:
		SectionBoardTray(int section);
		virtual ~SectionBoardTray();

		int addCommonMessage( void );

		/*!***************************************************************************
		 * COMMAND TRAY																 *
		 * ***************************************************************************/
		int getSensor( bool &bHomeTray, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int getTemperature( float& fTemp, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );
		int setTemperature(uint16_t intTemp, uint16_t intLowTemp, uint16_t intHighTemp,
						   uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		/*!***************************************************************************
		 * PACKETS MANAGER                                                           *
		 * ***************************************************************************/
		/*!
		 * @brief spreadMsg
		 *		  The payload message is sent to the related message queue according to the received type message
		 * @param pMsg
		 * @return 0 if success, -1 otherwise
		 */
		int spreadMsg( uint8_t *pMsg );

	protected:
		int decodeAsynchronousMsg( uint8_t * pMsg );
		bool decodeNotifyEvent( char *pubMsg , char *pcDescription );

    public:
        SectionCommonMessage	m_CommonMessage;

};

#endif // SECTIONBOARDTRAY_H
