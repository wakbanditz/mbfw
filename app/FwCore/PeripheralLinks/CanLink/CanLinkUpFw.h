/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanLinkUpFw.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CanLinkUpFw class.
 @details

 ****************************************************************************
*/

#ifndef CANLINKUPFW_H
#define CANLINKUPFW_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <sys/time.h>
#include <deque>


#include "Thread.h"
#include "CanIfr.h"
#include "Log.h"
#include "Mutex.h"
#include "MessageQueue.h"

class CanLinkBoard;
/*!
 * @brief The CanLinkUpFw class
 */
class CanLinkUpFw : public Thread,
					public Loggable
{
		/* Methods */
	public:

		/*!
		 * @brief CanLinkUpFw
         * @param pCanLinkBoard
         * @param pMsgQueueTx
         * @param pMsgQueueRx
         * @param section
		 */
		CanLinkUpFw(CanLinkBoard *pCanLinkBoard, MessageQueue *pMsgQueueTx, MessageQueue	*pMsgQueueRx, int section);

		/*!
		 * @brief ~CanLinkUpFw
		 */
		virtual ~CanLinkUpFw();


		/*!
		 * @brief initStoreMsgQueue
		 * @param smsgQueueAns
		 * @return
		 */
		int initStoreMsgQueue(string &smsgQueueAns);

		/*!
		 * @brief sendDataUpFw
         * @param usID
		 * @param pdata
		 * @param nlen
		 * @return
		 */
		int sendDataUpFw(uint16_t usID, uint8_t *pdata, uint8_t nlen);

		/*!
		 * @brief getAnswUpFwTimed
		 * @param ppayLoad
		 * @param ppayloadLenAnsw
		 * @param ntimeToWait
		 * @return
		 */
        int getAnswUpFwTimed( uint8_t * ppayLoad, uint16_t ppayloadLenAnsw, uint16_t ntimeToWait = 5000 );

	protected:
		/*!
		 * @brief The function control if the receive message queue has canFrame packet to read.
		 *        Then the packet is passed to the section part using dispatchBaseProt method.
		 * @return 0 if there isn't problem
		 */
		int			workerThread( void );
		void		beforeWorkerThread( void );
		void		afterWorkerThread( void );

    private:
        MessageQueue	*m_pMsgQueueUpFwTx;
        MessageQueue	*m_pMsgQueueUpFwRx;
        MessageQueue	*m_pMsgQueueAnsw;
        int				m_section;

        CanLinkBoard	*m_pCanLinkBoard;

};

#endif // CANLINKUPFW_H
