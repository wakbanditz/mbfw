/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionNTCParameters.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionNTCParameters class.
 @details

 ****************************************************************************
*/

#include <math.h>

#include "SectionNTCParameters.h"
#include "CanLinkCommonInclude.h"


SectionNTCParameters::SectionNTCParameters()
{
	m_eFormulaType	= eNoExpFormula;
	m_usVrXRef		= 0;
	m_usDivisor		= 0;
	m_uiResistance	= 0;
	m_uiParam1NTC	= 0;
	m_uiParam2NTC	= 0;
	m_uiParam3NTC	= 0;

	m_fVrXRef		= 0;
	m_fDivisor		= 0;
	m_fResistance	= 0;

	m_fReftemp		= 0;
	m_fCoeff		= 0;
	m_fResRef		= 0;

	m_fParamA		= 0;
	m_fParamB		= 0;
	m_fParamC		= 0;
}

SectionNTCParameters::~SectionNTCParameters()
{
	/* Nothing to do yet */
}

int SectionNTCParameters::setNTCParameters( uint8_t * pubParamBuff )
{
	printf("SectionNTCParameters::setNTCParameters: START\n");

	m_Mutex.lock();
	// STX | 5 | 'N' | f |	Vr1	 | Vr2  | Vr3  | Vr4  |
	//                    	D1	 | D2   | D3   | D4   |
	//                    	R1	 | R2   | R3   | R4   | R5   | R6   | R7   | R8   |
	//                    	P1,1 | P1,2 | P1,3 | P1,4 | P1,5 | P1,6 | P1,7 | P1,8 |
	//                    	P2,1 | P2,2 | P2,3 | P2,4 | P2,5 | P2,6 | P2,7 | P2,8 |
	//                    	P3,1 | P3,2 | P3,3 | P3,4 | P3,5 | P3,6 | P3,7 | P3,8 | F | ETX

	uint16_t	usIdx = 3;
	m_usVrXRef		= getVal4x((char *)&pubParamBuff[usIdx]);
	usIdx = usIdx + 4;
	m_usDivisor		= getVal4x((char *)&pubParamBuff[usIdx]);
	usIdx = usIdx + 4;
	m_uiResistance  = getVal8x((char *)&pubParamBuff[usIdx]);
	usIdx = usIdx + 8;
	m_uiParam1NTC   = getVal8x((char *)&pubParamBuff[usIdx]);
	usIdx = usIdx + 8;
	m_uiParam2NTC	= getVal8x((char *)&pubParamBuff[usIdx]);
	usIdx = usIdx + 8;
	m_uiParam3NTC	= getVal8x((char *)&pubParamBuff[usIdx]);
	usIdx = usIdx + 8;

	m_eFormulaType = (enumTypeFormulaNTCParams)pubParamBuff[usIdx];

	m_fVrXRef		= ((float)m_usVrXRef) / 1000.0;
	m_fDivisor		= ((float)m_usDivisor);
	m_fResistance	= ((float)m_uiResistance);

	switch ( m_eFormulaType )
	{
		// Exponential formula
		case eExpFormula:
		{
			m_fReftemp	= ((double)m_uiParam1NTC) / 100.0;
			m_fCoeff	= ((double)m_uiParam2NTC);
			m_fResRef	= ((double)m_uiParam3NTC);
		}
		break;
		// Steinhart formula
		case eSteinhartFormula:
		{
			m_fParamA	= ((double)m_uiParam1NTC) / pow(10.0,  9.0);
			m_fParamB	= ((double)m_uiParam2NTC) / pow(10.0, 10.0);
			m_fParamC	= ((double)m_uiParam3NTC) / pow(10.0, 13.0);
		}
		break;

		default:
			return -1;
		break;
	}

	printf("SectionNTCParameters::setNTCParameters: END\n");

	m_Mutex.unlock();


	return 0;
}

void SectionNTCParameters::printNTCParameters( void )
{
	m_Mutex.lock();

	printf("--> eFormulaType:%C\n", m_eFormulaType);	fflush(stdout);
	printf("--> uiParam1NTC:%d\n",	m_uiParam1NTC);		fflush(stdout);
	printf("--> uiParam2NTC:%d\n",	m_uiParam2NTC);		fflush(stdout);
	printf("--> uiParam3NTC:%d\n",	m_uiParam3NTC);		fflush(stdout);
	printf("--> uiResistance:%d\n", m_uiResistance);	fflush(stdout);
	printf("--> usDivisor:%d\n",	m_usDivisor);		fflush(stdout);
	printf("--> usVrXRef:%d\n",		m_usVrXRef);		fflush(stdout);
	printf("-----------------\n");						fflush(stdout);
	printf("--> fVrXRef:%f\n",		m_fVrXRef);			fflush(stdout);
	printf("--> fDivisor:%f\n",		m_fDivisor);		fflush(stdout);
	printf("--> fResistance:%f\n",	m_fResistance);		fflush(stdout);

	switch ( m_eFormulaType )
	{
		// Exponential formula
		case eExpFormula:
		{
			printf("--> ExpFormula\n");					fflush(stdout);
			printf("--> reftemp:%f\n", m_fReftemp);		fflush(stdout);
			printf("--> b_coeff:%f\n", m_fCoeff);		fflush(stdout);
			printf("--> res_ref:%f\n", m_fResRef);		fflush(stdout);
		}
		break;
		// Steinhart formula
		case eSteinhartFormula:
		{
			printf("--> SteinhartFormula\n");
			printf("--> fParamA:%f\n", m_fParamA);		fflush(stdout);
			printf("--> fParamB:%f\n", m_fParamB);		fflush(stdout);
			printf("--> fParamC:%f\n", m_fParamC);		fflush(stdout);
		}
		break;

		case eNoExpFormula:
		default:
		break;
	}

	m_Mutex.unlock();
}

double SectionNTCParameters::convertNTCValue(uint16_t usADCValue)
{
	double fCurr_R;
	double fCurr_V;
	double fTmpCurrV;
	double fTmpTemp;
	double NTCtemp;

	NTCtemp = 0.;

	if(m_eFormulaType == eNoExpFormula) return NTCtemp;

	m_Mutex.lock();

//	printf("SectionNTCParameters::convertNTCValue: START\n");

	// --------------------------------------------------------------------------- //
	fCurr_R		= 0;
	fCurr_V		= 0;
	fTmpTemp	= 0;
	fTmpCurrV	= 0;

	if ( m_fDivisor != 0 )
	{
		fCurr_V = ( (double)( usADCValue / m_fDivisor ) );
	}

	fTmpCurrV = ( m_fVrXRef - fCurr_V );
	if (fTmpCurrV != 0)
	{
		fCurr_R = ((double)(m_fResistance * fCurr_V) / (m_fVrXRef - fCurr_V));
	}

	switch ( m_eFormulaType )
	{
		case eExpFormula:
		{
			// calculate NTC temperature from NTC resistance
			if ( (m_fReftemp != 0) && (m_fResRef != 0) )
			{
				fTmpTemp   = ( (double)(m_fCoeff / ((m_fCoeff / m_fReftemp) + log(fCurr_R / m_fResRef))) );
				NTCtemp = ( fTmpTemp - 273.15 );
			}
		}
		break;

		case eSteinhartFormula:
		{
			if ( fCurr_R != 0 )
			{
				fTmpTemp = ((double)( m_fParamA  +
								  ( m_fParamB * log(fCurr_R) ) +
								  ( m_fParamC * pow(log(fCurr_R), 3 ))));
			}
			if ( fTmpTemp != 0 )
			{
				fTmpTemp   = ( (double)(1.0/ fTmpTemp) );
				NTCtemp = ( fTmpTemp - 273.15 );
			}
		}
		break;

		case eNoExpFormula:
		default:
			 NTCtemp = 0;
		break;
	}

//	printf("SectionNTCParameters::convertNTCValue: END\n");

	m_Mutex.unlock();

	return NTCtemp;
}
