/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanLinkBoard.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CanLinkBoard class.
 @details

 ****************************************************************************
*/

#ifndef CANLINKBOARD_H
#define CANLINKBOARD_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <sys/time.h>
#include <deque>

#include "Thread.h"
#include "CanIfr.h"
#include "Log.h"
#include "Mutex.h"
#include "MessageQueue.h"

/*!
 * @class	CanLinkAux
 * @brief	Base class for the management of the presure data collecting from the can manager. It provides
 *			m_msgQueueReceivedCanFrame queue as mechanism of communication beetween CanManager and CanLink
 *			applications. In the body of the thread there is the
 *			receive of the message from Can Manager application, hence the packet is sent to the related
 *			parsing function parseMsgFromLink. That function is a virtual function and it is well
 *			definited in child class inherited from the topic class.
 */
class CanLinkBoard;
class CanLinkUpFw;

class CanLinkAux : public Thread
{
	/* Methods */
	public:

        /*!
         * @brief constructor
         * @param pCanLink pointer to the CanLink object
         * @param pMsgQueue pointer to the msg qieie object
         * @param section the section index
         * @param channel the channel index
         */
        CanLinkAux(CanLinkBoard	*pCanLink, MessageQueue	*pMsgQueue, int section, int channel);

        /*!
         * @brief destructor
         */
        virtual ~CanLinkAux();

		/*!
		 * @brief initLogger Initilization of the object
		 * @param plogger
		 * @return 0 if success, -1 otherwise
		 */
		void initLogger( Log *plogger );

	protected:

		/*!
		 * @brief The function control if the receive message queue has canFrame packet to read.
		 *        Then the packet is passed to the section part using dispatchBaseProt method.
		 * @return 0 if there isn't problem
		 */
		int			workerThread( void );
		void		beforeWorkerThread( void );
		void		afterWorkerThread( void );

    protected:

        Log				*m_pLogger;
        MessageQueue	*m_pMsgQueue;
        int				m_section, m_channel;
        CanLinkBoard	*m_pCanLink;

};

/*!
 * @class	CanLinkBoard
 * @brief	Base class for the management of the routing of messages from the can manager. It provides
 *			m_msgQueueReceivedCanFrame and m_msgQueueSenderCanFrame queues as mechanism of communication
 *			beetween CanManager and CanLink applications. In the body of the thread there is the
 *			receive of the message from Can Manager application, hence the packet is sent to the related
 *			parsing function parseMsgFromLink. That function is a virtual function and it is well
 *			definited in child class inherited from the topic class.
 *			Supplementary queues m_msgQueueReceivedCanFrameAux have been added to manage the pressure data
 *			sent by Section board
 */
class CanLinkBoard : public Thread
{
	/* Methods */
	public:

        /*!
         * @brief constructor
         */
        CanLinkBoard();

        /*!
         * @brief destructor
         */
        virtual ~CanLinkBoard();

		/*!
		 * @brief initLogger Initilization of the object
		 * @param plogger
		 * @return 0 if success, -1 otherwise
		 */
		int initLogger( Log *plogger );

		/*!
		 * @brief initQueueTx		Initilization of the transmission queue
		 * @param smsgQueueCanFrameSender it should be the same name of the msg queue used by
		 *									canManager application
         * @param idx				the index of the queue 0 -> main, 1 -> upfw
		 * @return 0 if success, -1 otherwise
		 */
		int initQueueTx(const char * smsgQueueCanFrameSender, int idx);

		/*!
		 * @brief initQueueRx		Initilization of the receiver queue
		 * @param smsgQueueCanFrameReceiver it should be the same name of the msg queue used by
		 *									canManager application
		 * @param index				the index of the queue 0 -> main, 1-6 -> aux
		 * @return 0 if success, -1 otherwise
		 */
		int initQueueRx(const char * smsgQueueCanFrameReceiver, int index);

        /*!
         * @brief emptyAllQueues	empty all queues (TX and RX) when board is reset
         * @return 0 if success, -1 otherwise
         */
        int emptyAllQueues();

		/*!
		 * @brief stopReceiveMsgFromManagerThread	Just a wrapper to stopThread funtion.
		 */
		void stopReceiveMsgFromManagerThread( void );

		/*!
		 * @brief closeAndUnlinkMsgQueue
		 * @return
		 */
		int closeAndUnlinkMsgQueue( void );

		/*!
		 * @brief storeAuxData	collect the data received on the Aux CAN channels (pressure data)
		 *							and store them in specific buffers
		 * @param channel the channel index
		 * @param payloadMsg the recevice CAN frame
		 * @param ubLenPayLoad the number of bytes payloadMsg
		 * @return the size of buffer, -1 if error
		 */
		int storeAuxData(uint8_t channel, uint8_t * payloadMsg, uint8_t ubLenPayLoad);

		/*!
		 * @brief getStoredData	get the first element (and deletes it) of the vector of the specific channel
		 * @param channel the channel index
		 * @param dataAux will contain the extracted element
		 * @param ubLenPayLoad the number of bytes to be read
		 * @return true if success, false otherwise (vector empty)
		 */
		bool getStoredData(uint8_t channel, uint8_t * dataAux, uint8_t ubLenPayLoad);

		/*!
		 * @brief clearAuxData	reset the vectors of the aux data (pressure)
		 */
		void clearAuxData();

		/*!
		 * @brief setCanLinkUpFw it creates CanLinkUpFw object in heap memory sharing the message queues initialized
		 *						 by initQueueTx and Rx. It sets the logger and start the related thread in order to
		 *						 receive binary protocol messages.
		 * @return 0 if success, -1 otherwise
		 */
		int setCanLinkUpFw(const char *strAnswMessageQueue );

		/*!
		 * @brief sendUpFwMsg	wrapper to sendDataUpFw function of m_pCanLinkUpFwThread. It controls if data size is
		 *						correct
         * @param usID
		 * @param pdata
		 * @param nlen
		 * @return 0 if success, -1 otherwise
		 */
		int sendUpFwMsg(uint16_t usID, uint8_t *pdata, uint8_t nlen);

		/*!
		 * @brief receiveUpFwBinMsg	wrapper to getAnsUpFwTimed function in order to receive
		 *							binary answered message.
		 * @param ppayload
		 * @param usLen
		 * @param ustimeToWait
		 * @return
		 */
		int receiveUpFwBinMsg(uint8_t *ppayload, uint16_t &usLen, uint16_t ustimeToWait);

	protected:

		/*!
		 * @brief	sendDataFrame	The function stores data message in t_canFrame structure and send that packet to the
		 *							specified message Queue m_msgQueueSenderCanFrame set.
		 * @param id
		 * @param pdata
		 * @param nlen
		 * @return 0 if success, -1 otherwise
		 */
		int			sendDataFrame(uint8_t id, uint8_t *pdata, uint8_t nlen);

		/*!
		 * @brief parseMsgFromLink
		 * @param pcanFrame
		 * @return 0 if success, -1 otherwise
		 */
        virtual int parseMsgFromLink( t_canFrame * pcanFrame) = 0;

		/*!
		 * @brief The function control if the receive message queue has canFrame packet to read.
		 *        Then the packet is passed to the section part using dispatchBaseProt method.
		 * @return 0 if there isn't problem
		 */
		int			workerThread( void );
		void		beforeWorkerThread( void );
		void		afterWorkerThread( void );

    public:

        int				m_sectionId;

    protected:
        Log				*m_pLogger;

    private:

        MessageQueue	 m_msgQueueReceivedCanFrame;
        MessageQueue	 m_msgQueueSenderCanFrame;

        MessageQueue	m_msgQueueReceivedCanFrameUpFw;
        MessageQueue	m_msgQueueSenderCanFrameUpFw;

        MessageQueue	 m_msgQueueReceivedCanFrameAux1;
        MessageQueue	 m_msgQueueReceivedCanFrameAux2;
        MessageQueue	 m_msgQueueReceivedCanFrameAux3;
        MessageQueue	 m_msgQueueReceivedCanFrameAux4;
        MessageQueue	 m_msgQueueReceivedCanFrameAux5;
        MessageQueue	 m_msgQueueReceivedCanFrameAux6;

        vector<MessageQueue *> m_pMsgQueueReceivedAux;

        vector<CanLinkAux *> m_pCanLinkAux;
        deque<uint8_t> m_dataCanLinkAux1;
        deque<uint8_t> m_dataCanLinkAux2;
        deque<uint8_t> m_dataCanLinkAux3;
        deque<uint8_t> m_dataCanLinkAux4;
        deque<uint8_t> m_dataCanLinkAux5;
        deque<uint8_t> m_dataCanLinkAux6;

        CanLinkUpFw			*m_pCanLinkUpFwThread;

};

#endif // CANLINKBOARD_H
