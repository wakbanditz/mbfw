/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanLinkUpFw.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CanLinkUpFw class.
 @details

 ****************************************************************************
*/

#include "CanLinkUpFw.h"
#include "CanLinkBoard.h"
#include "CommonInclude.h"
#include "CanLinkCommonInclude.h"

CanLinkUpFw::CanLinkUpFw(CanLinkBoard *pCanLinkBoard, MessageQueue *pMsgQueueTx, MessageQueue *pMsgQueueRx, int section)
{
	m_pCanLinkBoard = pCanLinkBoard;
	m_pMsgQueueUpFwTx = pMsgQueueTx;
	m_pMsgQueueUpFwRx = pMsgQueueRx;
	m_section = section;
}

CanLinkUpFw::~CanLinkUpFw()
{
	/* Close and unlink message queue */
	bool bRet = m_pMsgQueueAnsw->closeAndUnlink();
	if ( bRet == false )
	{
		m_pLogger->log(LOG_ERR,"CanLinkUpFw: error from mq_unlink");
	}
	else
	{
		m_pLogger->log(LOG_INFO,"CanLinkUpFw: receive m_msgQueueAnsw unlinked");
	}

	SAFE_DELETE(m_pMsgQueueAnsw);
}

int CanLinkUpFw::initStoreMsgQueue(string &smsgQueueAns)
{
	/*!***************************************
	 * MSG QUEUE ANSW                        *
	 *****************************************/
	m_pMsgQueueAnsw = new MessageQueue();
	/* Create message queue for answer from section board */
	bool bRet = m_pMsgQueueAnsw->open(smsgQueueAns.c_str(), O_RDWR);
	if ( bRet == false )
	{
        bRet = m_pMsgQueueAnsw->create(smsgQueueAns.c_str(), O_RDWR, LOCAL_MQ_CANBOARD_MAX_MESSAGES_BIN, LOCAL_MQ_CANLINK_BIN_MSGSIZE);
		if ( bRet == false)
		{
			m_pLogger->log(LOG_ERR, "CanLinkUpFw::init--> Unable to create receive MQ%s. Exit.", smsgQueueAns.c_str());
			return -1;
		}
	}

	/* message queue pre emptying. It performs a cleaning of remained buffers. */
	bRet = m_pMsgQueueAnsw->empty();
	if ( bRet == false )
	{
		m_pLogger->log(LOG_ERR, "CanLinkUpFw::init--> Unable to PRE-EMPTY receive MQ%s. Exit.", smsgQueueAns.c_str());
		return -1;
	}

	return 0;
}

int CanLinkUpFw::sendDataUpFw(uint16_t usID, uint8_t *pdata, uint8_t nlen)
{
	if (nlen > MAX_LEN_PAYLOAD_PACK)
	{
		m_pLogger->log(LOG_ERR,"CanLinkBoard::sendDataUpFw --> Length of data too long");
		return -1;
	}

	//Frame
    t_canFrame	canData;
	canData.Frame.can_id = usID | CAN_EFF_FLAG ;//flag is necessary to enable extended format frame
    canData.Frame.can_dlc = nlen;
	memcpy(&canData.Frame.data, pdata, nlen);

	//TS
	struct timeval timeStamp;
	gettimeofday(&timeStamp, NULL);
	time_t curr_sec = timeStamp.tv_sec;
	int	curr_msec = timeStamp.tv_usec / 1000;
	sprintf(canData.time_msg, "%.19s.%03d", ctime(&curr_sec), curr_msec);

    char pMsg[sizeof(t_canFrame)];
	memset(pMsg, '\0', sizeof(pMsg));
	memcpy((void*)pMsg, &canData, sizeof(pMsg));

    if (!m_pMsgQueueUpFwTx->send(pMsg, sizeof(t_canFrame), 0, 1000))
	{
		m_pLogger->log(LOG_ERR,"CanLinkUpFw::sendDataUpFw --> Send m_pMsgQueueTx error <%s>",
					   strerror(errno));
		return -1;
	}
    m_pLogger->log(LOG_DEBUG_PARANOIC,"CanLinkUpFw::sendDataUpFw --> Send m_pMsgQueueTx id <%i>",canData.Frame.can_id);
	return 0;
}

int CanLinkUpFw::getAnswUpFwTimed(uint8_t * ppayLoad, uint16_t ppayloadLenAnsw, uint16_t ntimeToWait )
{
	char		sInBuffer[MQ_CANBOARD_MAX_MSG_SIZE_BUFF];
	unsigned int nMsgPriority = 0;
	int timeoutMsecs;

	timeoutMsecs = ntimeToWait;
	memset(sInBuffer, 0, sizeof(sInBuffer));

    int nNumBytesReceived = m_pMsgQueueAnsw->receive(sInBuffer, LOCAL_MQ_CANLINK_BIN_MSGSIZE_BUFF, &nMsgPriority, timeoutMsecs);
	if ( nNumBytesReceived == -1 )
	{
		if ( errno == ETIMEDOUT )
		{
            m_pLogger->log(LOG_DEBUG_IPER_PARANOIC, "CanLinkUpFw::getAnswUpFwTimed--> TIME-OUT on mq_timedreceive() MQ");
		}
		else
		{
            //m_pLogger->log(LOG_ERR, "CanLinkUpFw::getAnswUpFwTimed--> error from mq_timedreceive() MQ");
		}
		return -1;
	}
	else
	{
		//Message is available, get it
		memcpy(ppayLoad, sInBuffer, ppayloadLenAnsw);
	}

	return 0;
}



void CanLinkUpFw::beforeWorkerThread()
{
    m_pLogger->log(LOG_DEBUG,"CanLinkUpFw: thread START");
}

void CanLinkUpFw::afterWorkerThread()
{
    m_pLogger->log(LOG_DEBUG,"CanLinkUpFw: thread END");
}

int CanLinkUpFw::workerThread()
{
	char sInBuffer[SCT_MQ_CANLINK_MSG_BUFFER_SIZE];

	while ( isRunning() )
    {
		unsigned int nMsgPriority = 0;
		int nNumBytesReceived = m_pMsgQueueUpFwRx->receive(sInBuffer, SCT_MQ_CANLINK_MSG_BUFFER_SIZE,
                                                               &nMsgPriority, SCT_MQ_TIMEOUT_RX);

		if ( nNumBytesReceived == -1 )
		{
			if ( errno == ETIMEDOUT )
			{
                m_pLogger->log(LOG_DEBUG_IPER_PARANOIC, "CanLinkUpFw::workerThread--> TIMEOUT m_pMsgQueue");
			}
			else
			{
				m_pLogger->log(LOG_ERR, "CanLinkUpFw::workerThread-->ERROR from m_pMsgQueue");
			}

			m_pMsgQueueUpFwRx->empty();
		}
		else
		{
            t_canFrameFD canFrameReceived;
            memcpy(&canFrameReceived, &sInBuffer, sizeof(t_canFrameFD));

			// extract data and store them in a dedicated message queue
            char	payloadMsg[LOCAL_MQ_CANLINK_BIN_MSGSIZE_BUFF];
            memset(payloadMsg, 0, LOCAL_MQ_CANLINK_BIN_MSGSIZE_BUFF);
			memcpy(payloadMsg, canFrameReceived.Frame.data, DIM_SECTIONLINK_BIN_PROTO_SIZE);

            bool res = m_pMsgQueueAnsw->send(payloadMsg, DIM_SECTIONLINK_BIN_PROTO_SIZE, 0, 1000 );
			if (res != true)
			{
				m_pLogger->log(LOG_ERR, "CanLinkUpFw::workerThread--> unable to send on m_msgQueueAnsw");
				return -1;
			}
		}

        usleep(2);
	}
	return 0;
}
