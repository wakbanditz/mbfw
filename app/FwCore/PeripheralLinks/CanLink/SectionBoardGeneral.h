/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardGeneral.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionBoardGeneral class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONBOARDGENERAL_H
#define SECTIONBOARDGENERAL_H

#include "CanBoardBase.h"
#include "SectionNTCParameters.h"
#include "CanLinkCommonInclude.h"
#include "SectionInclude.h"
#include "SectionEepromTable.h"

typedef struct
{
	uint8_t		ubDir;
	uint8_t		ubStepType;
	uint8_t		ubStepMode;
	uint8_t		ubSlopeScan;
	uint8_t		ubDecSlope;
	uint8_t		ubFreqSource;
	uint8_t		ubFreqTable;
	uint16_t	usFreqMotor;
	uint8_t		ubCurr;

} structMotorConfiguration;

typedef enum
{
	eSCT_UPFW_BOOTLOADER		= 0,
	eSCT_UPFW_APPLICATION		= 1,
	eSCT_UPFW_FPGA				= 2
}enumUpgradeFwTypeMemory;


#define CAN0_UPGFW_TYPE_MEM_BOOT '0'
#define CAN0_UPGFW_TYPE_MEM_APPL '1'
#define CAN0_UPGFW_TYPE_MEM_FPGA '2'

#define CAN0_UPGFW_STEP_FIRSTPKT '0'
#define CAN0_UPGFW_STEP_LASTPKT	 '1'
#define CAN0_UPGFW_STEP_CRCPKT	 '2'

/*!
 * @class	SectionBoardGeneral
 * @brief	The SectionBoardGeneral class contains the commands used for the communications with the general section part.
 *			As child of CanBoardBase the thread is used to manage asynchronous message.
 */
class SectionBoardGeneral : public CanBoardBase
{

	public:

		SectionBoardGeneral(int section);
		virtual ~SectionBoardGeneral();

        int  getInternalStatus(uint8_t &ubStatus, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY, bool bErrorSet = true);

		int  getVoltages( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY,
						  uint16_t * us24V0Val = 0, uint16_t  * us5V0Val = 0,
						  uint16_t * us3V3Val = 0, uint16_t * us1V2Val = 0);

		int  getTemperature( uint16_t& fSPRTemp, uint16_t& fTrayTemp, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  getSensors(uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);
		void setSensorSpr(bool status);

		int  getVolumeKFactor( uint32_t& ulKFactor, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);
		int  setVolumeKFactor( uint32_t ulVolumeKFactor , uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  getPressureAlgorithm(uint8_t &ubPressureAlgorithm, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);
		int  setPressureAlgorithm( uint8_t strip, enumSectionPressureAlgorithm ePressureAlgorithm,
								   uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  readEepromParameter(uint16_t usParameter, int64_t &sllValue, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);
		int  writeEepromParameter( uint16_t usParameter, int64_t llValue , uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);
		int  restoreDefaultEepromState( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  setInfoState( enumSectionInfoState eInfoState , uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  setLed( enumSectionLedType eLedType, enumSectionLedFreq eLedFreq,
					 uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  resetMechanic( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  enterMaintenanceState( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  exitMaintenanceState( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  runInitializationProcedure( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int sendInitializationEvents(uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int uploadFirmware(uint8_t eTypeMemoryUpFw, uint8_t ubSeqTypePkt, uint32_t ulNPkt,
						   uint8_t &ubStatusUpg, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int sleepOrDisable(uint8_t active, uint8_t mode, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int resetSoftware(uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		/*!***************************************************************************
		 * SECTION BOARD GENERAL STATUS                                              *
		 * ***************************************************************************
		 */
		string  getFirmwareVersion(enumSectionFirmwareVersion eType, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  getTableVersion( enumSectionEepromTableType eTableType, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  getTable( enumSectionEepromTableType eTableType, uint8_t ubRowIndex,
					   uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  sendTable( uint8_t strip, enumSectionEepromTableType eTableType, enumSectionEepromPacketType ePacketType,
						uint8_t ubRowIndex, SectionEepromTable &sectionEepromTable,
						uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  getNTCParameters( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  getDefaultMotorConfiguration( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		/*!***************************************************************************
		 * PROTOCOL                                                                  *
		 * ***************************************************************************
		 */
		int enablePressureAcquisition( enumSectionPIBStartStateAcq eState, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  sendStripLayout( uint8_t * str1, uint8_t * str2, uint8_t * str3,
							  uint8_t * str4, uint8_t * str5, uint8_t * str6,
                              uint16_t usTimeoutMs = DEFAULT_SETTING_DELAY);

		int  sendStripCheck( uint8_t * str1, uint8_t * str2, uint8_t * str3,
							  uint8_t * str4, uint8_t * str5, uint8_t * str6,
                              uint16_t usTimeoutMs = DEFAULT_SETTING_DELAY);

        int setPressureSendingRate(uint8_t rate1, uint8_t rate2, uint8_t rate3,
                                    uint8_t rate4, uint8_t rate5, uint8_t rate6,
                                    uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);


		int sendStripPressureSettings(uint8_t strip, uint8_t parameter, int16_t value,
                                      uint16_t usTimeoutMs = DEFAULT_SETTING_DELAY);

		int sendStripPressureThresholds(uint8_t strip, uint8_t volume, uint8_t speed, uint64_t low, uint64_t high,
                                        uint16_t usTimeoutMs = DEFAULT_SETTING_DELAY);

		int  sendProtocolString( string sProtocol , uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  executeProtocol( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  continueProtocol( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int  forceProtocol( void );

        int  abortProtocol( uint32_t usTimeoutMs = ABORT_REQUEST_DELAY);

		int setProtocolRunningFlag(uint8_t active, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		void clearProtocolFlags();

		void clearProtocolWindowFlag();

		void clearProtocolAbortedFlag();

		void clearProtocolEndFlag();

		bool getProtocolWindowFlag();

		bool getProtocolAbortedFlag();

		bool getProtocolEndFlag();

		/*!***************************************************************************
		 * PACKETS MANAGER                                                           *
		 * ***************************************************************************/
		/*!
		 * @brief	spreadMsg	The payload message is sent to the related message queue according
		 *						to the received type message.
		 * @param	pMsg
		 * @return	0 if success, -1 otherwise
		 */
		int spreadMsg( uint8_t * pMsg );

		/*!***************************************************************************
		 * GET VARIABLES                                                                  *
		 * ***************************************************************************
		 */
		double getTemperatureSPR();
		double getTemperatureTray();
		uint16_t getIntTemperatureSPR();
		uint16_t getIntTemperatureTray();

		void clearSectionReady();
        bool getSectionReady();

		eDoorStatus getDoorStatus();
		std::string getDoorStatusAsString();
		bool isDoorClosed();

		bool getSensorPump();
		bool getSensorSpr();
		bool getSensorTray();
		bool getSensorTower();

		bool getStartButton();
		void clearStartButton();

		uint16_t getVoltage24V();
		uint16_t getVoltage3V3();
		uint16_t getVoltage5V();
		uint16_t getVoltage1V2();

	protected:

		int decodeAsynchronousMsg( uint8_t * pMsg );
		bool decodeNotifyEvent( char * pubMsg , char * pcDescription );


    private:

        uint16_t					m_usVoltageP24V;	//value*100(V)
        uint16_t					m_usVoltageP3V3;	//value*100(V)
        uint16_t					m_usVoltageP5V;		//value*100(V)
        uint16_t					m_usVoltageP1V2;	//value*100(V)

        SectionNTCParameters		m_NTCParam;
        SectionEepromTable			m_Table;
        structMotorConfiguration	m_stMotorConfig_1;
        structMotorConfiguration	m_stMotorConfig_2;
        structMotorConfiguration	m_stMotorConfig_3;

        double m_fTemperatureSPR;
        double m_fTemperatureTray;

        eDoorStatus m_doorStatus, m_doorStatus1, m_doorStatus2;
        bool m_sectionReady;

        bool m_sensorPump, m_sensorSpr, m_sensorTray, m_sensorTower;
        bool m_pushbuttonStatus;

        bool m_protocolWindowJ, m_protocolEnd, m_protocolAborted;

};

#endif // SECTIONBOARDGENERAL_H
