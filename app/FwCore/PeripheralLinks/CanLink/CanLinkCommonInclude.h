/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanLinkCommonInclude.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the CAN classes.
 @details

 ****************************************************************************
*/

#ifndef CANLINKCOMMONINCLUDE_H
#define CANLINKCOMMONINCLUDE_H

#include <stdint.h>

/* CAN defines */
#define CAN_SEND_FRAME_WITH_TIMESTAMP	//Only for logging of data timestamp when the socket send the message

#define CANLINKBOARD_TO_MQ_RX				12000

//CAN parameters
#define CANBOARDBASE_TIMEOUT_RECEIVE_MQ		10000

#define STX 0x02
#define ETX 0x03
#define ACK_CAN	0x7E

//Index fields of protocol message
#define IDX_CAN_ORIGIN      0
#define IDX_CAN_TYPE_MSG    1
#define IDX_CAN_NOTIFY_MSG  1
#define IDX_CAN_BODY_MSG    2
#define IDX_CAN_FLAG_MSG    2
#define IDX_CAN_DATA_MSG    3
#define IDX_CAN_ASYNC_MSG   2

//Type flag response
#define	CANSTD_ACCEPTED						'0'
#define	CANSTD_REFUSED						'1'
#define	CANSTD_BADFORMAT					'2'
#define	CANSTD_COMPLETED					'3'
#define	CANSTD_DONEWARNING					'4'
#define	CANSTD_HOMENOTFOUND					'5'
#define	CANSTD_MICROSTEPDRVERR				'6'
#define	CANSTD_MICRODRVCOMFAIL				'7'
#define	CANSTD_I2CFAILCOM					'8'
#define	CANSTD_QSPICFAILCOM					'9'
#define	CANSTD_I2C1FAILCOM					'A'
#define	CANSTD_MICROADERROR					'B'
#define	CANSTD_PCLEEPROMNERVERSET			'C'
#define	CANSTD_PCLEEPROMPARAMCORRUPT		'D'
#define	CANSTD_BALANCEEEPORMNEVERSET		'E'
#define	CANSTD_BALANCEEEPROMPARAMCORRUPT	'F'
#define	CANSTD_MOTORENCERR					'G'
#define	CANSTD_SCTDOOROPEN					'L'
#define	CANSTD_CHECKSUMERR					'M'
#define	CANSTD_CURRENTERR					'Q'
#define	CANSTD_HOMESTEPLOST					'R'
#define	CANSTD_HOMENOTDONE					'S'


#define CANSTD_SENSOR_MASK_HOME_PUMP		0x01
#define CANSTD_SENSOR_MASK_HOME_SPR			0x01
#define CANSTD_SENSOR_MASK_HOME_TOWER		0x02
#define CANSTD_SENSOR_MASK_HOME_TRAY		0x04
#define CANSTD_SENSOR_MASK_SPR_BLOCK		0x08
#define CANSTD_SENSOR_MASK_DOOR_LEFT		0x10
#define CANSTD_SENSOR_MASK_DOOR_RIGHT		0x20
#define CANSTD_SENSOR_MASK_DOOR				0x30

typedef enum
{
	eBootloader						= '0',
	eApplication					= '1',
	eFPGAbitstream					= '2',
	eSelector						= '3'

}enumSectionFirmwareVersion;

typedef enum
{
	eDisableProtoMsgRs232			= '0',
	eEnableProtoMsgRs232			= '1'

} enumSectionInfoState;

typedef enum
{
	eLedOff 		= '0',
	eLedBlinkSlow	= '1',
	eLedBlinkHigh	= '2',
	eLedOn			= 'F'

} enumSectionLedFreq;

typedef enum
{
	eLedBlue	= '0',
	eLedGreen 	= '1',
	eLedRed		= '2'
} enumSectionLedType;

typedef enum
{
	eClinic		= '0',
	eIndustry	= '1',
	eVeterinary	= '2'

} enumSectionPressureAlgorithm;

#define SCTB_MAX_DESCRIPTION	256
#define MIN_LEN_CAN_MSGRX		3
#define MAX_LEN_PAYLOAD_PACK	8

//Command Get firmware version
#define	FW_V_BOOTLOADER		'0'
#define FW_V_APP			'1'
#define	FW_V_FPGA			'2'
#define FW_V_SELECTOR		'3'

//Utility functions
int getValD(char *p);
int getVal1x(char *p);
int getVal2x(char *p);
int getVal4x(char *p);
int getVal6x(char *p);
int getVal8x(char *p);

int getAsciiValue(signed char ubVal);
#endif // CANLINKCOMMONINCLUDE_H
