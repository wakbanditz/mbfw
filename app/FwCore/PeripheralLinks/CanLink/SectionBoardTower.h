/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardTower.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionBoardTower class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONBOARDTOWER_H
#define SECTIONBOARDTOWER_H

#include "CanBoardBase.h"
#include "CanLinkCommonInclude.h"
#include "SectionCommonMessage.h"

/*!
 * @class SectionBoardTower
 * @brief The SectionBoardTower class
 */
class SectionBoardTower : public CanBoardBase
{
	public:
		SectionBoardTower(int section);
		virtual ~SectionBoardTower();

		int addCommonMessage( void );

		/*!***************************************************************************
		 * COMMAND TOWER																 *
		 * ***************************************************************************/
		int getSensor( bool &bHomeTower, uint16_t usTimeoutMs );
		int setWellBottom(uint32_t ulNumStep, uint16_t usTimeoutMs);

		/*!***************************************************************************
		 * PACKETS MANAGER                                                           *
		 * ***************************************************************************/
		/*!
		 * @brief spreadMsg
		 *		  The payload message is sent to the related message queue according to the received type message
		 * @param pMsg
		 * @return 0 if success, -1 otherwise
		 */
		int spreadMsg( uint8_t *pMsg );

	protected:
		int decodeAsynchronousMsg( uint8_t * pMsg );
		bool decodeNotifyEvent( char *pubMsg , char *pcDescription );

    public:
        SectionCommonMessage	m_CommonMessage;

};

#endif // SECTIONBOARDTOWER_H
