/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionEepromTable.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionEepromTable class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONEEPROMTABLE_H
#define SECTIONEEPROMTABLE_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "CanLinkCommonInclude.h"

#define SECTION_EEPROM_TABLE_SIZE			36
#define SECTION_EEPROM_TABLE_VERSION_SIZE	6

typedef enum
{
	eAspirationLowThresholdTable		= '0',
	eAspirationIndustryThresholdTable	= '1',
	eVolumeTable						= '2',
	eSpeedTable							= '3',
	eAspirationHighThresholdTable		= '4'

} enumSectionEepromTableType;

typedef enum
{
	eTableVersion					= '0',
	eRow							= '1'

} enumSectionEepromPacketType;

typedef struct
{
	uint8_t		ubVersion[SECTION_EEPROM_TABLE_VERSION_SIZE];
	union
	{
		uint32_t	ulMatrix[SECTION_EEPROM_TABLE_SIZE][SECTION_EEPROM_TABLE_SIZE];
		int32_t		slBuff[SECTION_EEPROM_TABLE_SIZE];
		int32_t		slMatrix[SECTION_EEPROM_TABLE_SIZE][SECTION_EEPROM_TABLE_SIZE];
	};

} structSectionEepromTable;

/*!
 * @class SectionEepromTable
 * @brief The SectionEepromTable class
 */
class SectionEepromTable
{
	public:
		SectionEepromTable();
		virtual ~SectionEepromTable();

		int setValAspirationTsTable( enumSectionEepromTableType eType, uint32_t ulVal, uint8_t ubRow, uint8_t ubColumn );
		int setValVolumes( int32_t slVal, uint8_t ubRow );
		int setValSpeed( int32_t slVal, uint8_t ubRow, uint8_t ubColumn );
		int setVersion(enumSectionEepromTableType eTableType, uint8_t * ubVal );

		int getRowAspirationTsTable( enumSectionEepromTableType eType, uint8_t ubRow, uint32_t * ulBuffRow, uint32_t &ulChecksum );
		int getTableVolumes( uint8_t ubRow, int32_t *slBuffRow, uint32_t &ulChecksum );
		int getRowSpeed( uint8_t ubRow, int32_t *slBuffRow, uint32_t &ulChecksum );
		int getVersion(enumSectionEepromTableType eTableType, uint8_t *ubVal);

		void printValAspirationLowTsTable( void );
		void printValVolumesTable( void );
		void printValSpeedTable( void );
		void printValAspirationTable(enumSectionEepromTableType eType );
		void printVersion(enumSectionEepromTableType eTableType );

		void printRowAspirationTsTable( enumSectionEepromTableType eType, uint8_t ubIndexRow );
		void printRowAspirationHighTsTable( uint8_t ubIndexRow );
		void printRowSpeedTable( uint8_t ubIndexRow );

    private:
        structSectionEepromTable	m_AspirationLowThreshold;
        structSectionEepromTable	m_AspirationHighThreshold;
        structSectionEepromTable	m_AspirationIndustryThreshold;
        structSectionEepromTable	m_Volumes;
        structSectionEepromTable	m_Speed;

};

#endif // SECTIONEEPROMTABLE_H
