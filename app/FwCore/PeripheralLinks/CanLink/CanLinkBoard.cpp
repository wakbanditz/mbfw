/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanLinkBoard.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CanLinkBoard class.
 @details

 ****************************************************************************
*/

#include "CanLinkBoard.h"
#include "CanLinkUpFw.h"
#include "CommonInclude.h"
#include "CanLinkCommonInclude.h"

static Mutex m_mutexDataAux;

CanLinkBoard::CanLinkBoard()
{
	m_pLogger = NULL;
	m_pCanLinkUpFwThread = NULL;

	m_pMsgQueueReceivedAux.push_back(&m_msgQueueReceivedCanFrameAux1);
	m_pMsgQueueReceivedAux.push_back(&m_msgQueueReceivedCanFrameAux2);
	m_pMsgQueueReceivedAux.push_back(&m_msgQueueReceivedCanFrameAux3);
	m_pMsgQueueReceivedAux.push_back(&m_msgQueueReceivedCanFrameAux4);
	m_pMsgQueueReceivedAux.push_back(&m_msgQueueReceivedCanFrameAux5);
	m_pMsgQueueReceivedAux.push_back(&m_msgQueueReceivedCanFrameAux6);

	m_sectionId = SCT_NONE_ID;

	clearAuxData();
}

CanLinkBoard::~CanLinkBoard()
{
	for(unsigned int i = 0; i < m_pCanLinkAux.size(); i++)
	{
		CanLinkAux *pCanLinkAux = m_pCanLinkAux.at(i);
		pCanLinkAux->stopThread();
		SAFE_DELETE(pCanLinkAux);
	}
	clearAuxData();

//	m_pCanLinkUpFwThread->stopThread();
	SAFE_DELETE(m_pCanLinkUpFwThread);
}

void CanLinkBoard::clearAuxData()
{
	m_dataCanLinkAux1.clear();
	m_dataCanLinkAux2.clear();
	m_dataCanLinkAux3.clear();
	m_dataCanLinkAux4.clear();
	m_dataCanLinkAux5.clear();
	m_dataCanLinkAux6.clear();
}

int CanLinkBoard::closeAndUnlinkMsgQueue( void )
{
	bool bRet = false;

	/* Close and unlink message queue */
	bRet = m_msgQueueReceivedCanFrame.closeAndUnlink();
	if ( bRet == false )
	{
		m_pLogger->log(LOG_ERR,"CanLinkBoard::closeAndUnlinkMsgQueue --> error from mq_unlink");
	}
	else
	{
		m_pLogger->log(LOG_INFO,"CanLinkBoard::closeAndUnlinkMsgQueue --> receive m_msgQueueReceivedCanFrame unlinked");
	}

	bRet = m_msgQueueReceivedCanFrameUpFw.closeAndUnlink();
	if ( bRet == false )
	{
		m_pLogger->log(LOG_ERR,"CanLinkBoard::closeAndUnlinkMsgQueue --> error from mq_unlink");
	}
	else
	{
		m_pLogger->log(LOG_INFO,"CanLinkBoard::closeAndUnlinkMsgQueue --> receive m_msgQueueReceivedCanFrameUpFw unlinked");
	}

	for(unsigned int i = 0; i < m_pMsgQueueReceivedAux.size(); i++)
	{
		MessageQueue * pMessageQueue = m_pMsgQueueReceivedAux.at(i);
		bRet = pMessageQueue->closeAndUnlink();
		if ( bRet == false )
		{
			m_pLogger->log(LOG_ERR,"CanLinkBoard::closeAndUnlinkMsgQueue --> error from mq_unlink %d", i);
		}
		else
		{
			m_pLogger->log(LOG_INFO,"CanLinkBoard::closeAndUnlinkMsgQueue --> "
									"receive m_msgQueueReceivedCanFrameAux %d unlinked", i);
		}
	}

	bRet = m_msgQueueSenderCanFrame.closeAndUnlink();
	if ( bRet == false )
	{
		m_pLogger->log(LOG_ERR,"CanLinkBoard::closeAndUnlinkMsgQueue --> error from mq_unlink");
	}
	else
	{
		m_pLogger->log(LOG_INFO,"CanLinkBoard::closeAndUnlinkMsgQueue --> receive m_msgQueueSenderCanFrame unlinked");
	}

	bRet = m_msgQueueSenderCanFrameUpFw.closeAndUnlink();
	if ( bRet == false )
	{
		m_pLogger->log(LOG_ERR,"CanLinkBoard::closeAndUnlinkMsgQueue --> error from mq_unlink");
	}
	else
	{
		m_pLogger->log(LOG_INFO,"CanLinkBoard::closeAndUnlinkMsgQueue --> receive m_msgQueueSenderCanFrameUpFw unlinked");
	}

	return 0;
}

int CanLinkBoard::initLogger(Log *plogger)
{
	if (plogger == NULL)
	{
		return -1;
	}

	m_pLogger = plogger;
	return 0;
}

int CanLinkBoard::initQueueTx(const char* smsgQueueCanFrameSender, int idx)
{
	if (smsgQueueCanFrameSender == NULL)
	{
		m_pLogger->log(LOG_INFO, "CanLinkBoard::initQueueTx--> no mq present");
		return -1;
	}

	MessageQueue * pMessageQueue;

	switch(idx)
	{
		case SCT_IDX_MQ_GPF_CANLINK :
			pMessageQueue = &m_msgQueueSenderCanFrame;
		break;
		case SCT_IDX_MQ_BPF_CANLINK :
			pMessageQueue = &m_msgQueueSenderCanFrameUpFw;
		break;
		default:
			m_pLogger->log(LOG_INFO, "CanLinkBoard::initQueueTx--> index out of range");
			return -1;
		break;
	}

	/* ********************************************************************************************
	 * Create sender message queue
	 * ********************************************************************************************
	 */
	bool bRet = pMessageQueue->open(smsgQueueCanFrameSender, O_WRONLY);
	if ( bRet == false )
	{
		m_pLogger->log(LOG_INFO, "CanLinkBoard::init--> MQ%s doesn't exist, so create it...", smsgQueueCanFrameSender);

        bRet = pMessageQueue->create(smsgQueueCanFrameSender, O_WRONLY, SCT_MQ_CANLINK_MAX_NUM_MESSAGES, SCT_MQ_CANLINK_MAX_MSG_SIZE);
		if ( bRet == false )
		{
			m_pLogger->log(LOG_ERR, "CanLinkBoard::init--> Fail to create receive MQ%s Exit.", smsgQueueCanFrameSender);
			return -1;
		}
		else
		{
			m_pLogger->log(LOG_INFO, "CanLinkBoard::init --> MSGQUEUE %s created", smsgQueueCanFrameSender);
		}
	}
	else
	{
		m_pLogger->log(LOG_INFO, "CanLinkBoard::init--> MQ%s already exists...", smsgQueueCanFrameSender);
	}

	/* ****************************************************************************************************************
	  *	MESSAGE QUEUE PRE EMPTYING
	  * This server can have been respawned, so before starting the normal real-time service
	  * it is necessary to perform a cleaning of the remained buffers
	  * ***************************************************************************************************************
	  */
	bRet = pMessageQueue->empty();
	if ( bRet == false )
	{
		m_pLogger->log(LOG_ERR, "CanLinkBoard::init--> Fail to PRE-EMPTY receive MQ%s Exit", smsgQueueCanFrameSender);
		return -1;
	}

	return 0;
}

int CanLinkBoard::initQueueRx(const char* smsgQueueCanFrameReceiver, int index)
{
	if (smsgQueueCanFrameReceiver == NULL)
	{
		return -1;
	}
	if (index < 0 || index > SCT_NUM_TOT_MQ_CANLINK)
	{
		return -1;
	}

	MessageQueue * pMessageQueue;

	switch(index)
	{
		case 0 :
			pMessageQueue = &m_msgQueueReceivedCanFrame;
		break;
		case 1 :
			pMessageQueue = &m_msgQueueReceivedCanFrameAux1;
		break;
		case 2 :
			pMessageQueue = &m_msgQueueReceivedCanFrameAux2;
		break;
		case 3 :
			pMessageQueue = &m_msgQueueReceivedCanFrameAux3;
		break;
		case 4 :
			pMessageQueue = &m_msgQueueReceivedCanFrameAux4;
		break;
		case 5 :
			pMessageQueue = &m_msgQueueReceivedCanFrameAux5;
		break;
		case 6 :
			pMessageQueue = &m_msgQueueReceivedCanFrameAux6;
		break;
		case 7 :
			pMessageQueue = &m_msgQueueReceivedCanFrameUpFw;
		break;

	}

	/* ********************************************************************************************
	 * Create receive message queue
	 * ********************************************************************************************
	 */
	bool bRet = false;

	bRet = pMessageQueue->open(smsgQueueCanFrameReceiver, O_RDONLY);
	if (bRet == false)
	{
		m_pLogger->log(LOG_INFO, "CanLinkBoard::init--> MQ%s doesn't exist, so create it...", smsgQueueCanFrameReceiver);

        bRet = pMessageQueue->create(smsgQueueCanFrameReceiver, O_RDONLY, SCT_MQ_CANLINK_MAX_NUM_MESSAGES, SCT_MQ_CANLINK_MAX_MSG_SIZE );
		if (bRet == false)
		{
			m_pLogger->log(LOG_ERR, "CanLinkBoard::init--> Unable to create receive MQ%s. Exit.", smsgQueueCanFrameReceiver);
			return -1;
		}
		else
		{
			m_pLogger->log(LOG_INFO, "CanLinkBoard::init --> MSGQUEUE %s created", smsgQueueCanFrameReceiver);
		}
	}
	else
	{
		m_pLogger->log(LOG_INFO, "CanLinkBoard::init--> MQ%s already exists...", smsgQueueCanFrameReceiver);
	}

	/* ****************************************************************************************************************
	  *	MESSAGE QUEUE PRE EMPTYING
	  * This server can have been respawned, so before starting the normal real-time service
	  * it is necessary to perform a cleaning of the remained buffers
	  * ***************************************************************************************************************
	  */
	bRet = pMessageQueue->empty();
	if (bRet == false)
	{
		m_pLogger->log(LOG_ERR, "CanLinkBoard::init--> Unable to PRE-EMPTY receive MQ%s. Exit.", smsgQueueCanFrameReceiver);
		return -1;
	}

	// for every auxiliary queue create a dedicated thread
	if ((index > 0) && (index < SCT_NUM_TOT_MQ_CANLINK))
	{
		CanLinkAux *pCanLinkAux = new CanLinkAux((CanLinkBoard *)this, pMessageQueue, m_sectionId, index);
		pCanLinkAux->initLogger(m_pLogger);
		pCanLinkAux->startThread();

		// add it to the vector
		m_pCanLinkAux.push_back(pCanLinkAux);
	}

	return 0;
}

int CanLinkBoard::emptyAllQueues()
{
    bool bRet = true;

    char		sInBuffer[MQ_CANBOARD_MAX_MSG_SIZE_BUFF];
    unsigned int nMsgPriority = 0;

    while( m_msgQueueSenderCanFrame.receive(sInBuffer, SCT_MQ_CANLINK_MSG_BUFFER_SIZE, &nMsgPriority, 100) != -1) msleep(2);
    while( m_msgQueueSenderCanFrameUpFw.receive(sInBuffer, SCT_MQ_CANLINK_MSG_BUFFER_SIZE, &nMsgPriority, 100) != -1) msleep(2);
    while( m_msgQueueReceivedCanFrame.receive(sInBuffer, SCT_MQ_CANLINK_MSG_BUFFER_SIZE, &nMsgPriority, 100) != -1) msleep(2);
    while( m_msgQueueReceivedCanFrameAux1.receive(sInBuffer, LOCAL_MQ_CANLINK_BIN_MSGSIZE_BUFF, &nMsgPriority, 100) != -1) msleep(2);
    while( m_msgQueueReceivedCanFrameAux2.receive(sInBuffer, LOCAL_MQ_CANLINK_BIN_MSGSIZE_BUFF, &nMsgPriority, 100) != -1) msleep(2);
    while( m_msgQueueReceivedCanFrameAux3.receive(sInBuffer, LOCAL_MQ_CANLINK_BIN_MSGSIZE_BUFF, &nMsgPriority, 100) != -1) msleep(2);
    while( m_msgQueueReceivedCanFrameAux4.receive(sInBuffer, LOCAL_MQ_CANLINK_BIN_MSGSIZE_BUFF, &nMsgPriority, 100) != -1) msleep(2);
    while( m_msgQueueReceivedCanFrameAux5.receive(sInBuffer, LOCAL_MQ_CANLINK_BIN_MSGSIZE_BUFF, &nMsgPriority, 100) != -1) msleep(2);
    while( m_msgQueueReceivedCanFrameAux6.receive(sInBuffer, LOCAL_MQ_CANLINK_BIN_MSGSIZE_BUFF, &nMsgPriority, 100) != -1) msleep(2);

    bRet &= m_msgQueueSenderCanFrame.empty();
    bRet &= m_msgQueueSenderCanFrameUpFw.empty();

    bRet &= m_msgQueueReceivedCanFrame.empty();
    bRet &= m_msgQueueReceivedCanFrameAux1.empty();
    bRet &= m_msgQueueReceivedCanFrameAux2.empty();
    bRet &= m_msgQueueReceivedCanFrameAux3.empty();
    bRet &= m_msgQueueReceivedCanFrameAux4.empty();
    bRet &= m_msgQueueReceivedCanFrameAux5.empty();
    bRet &= m_msgQueueReceivedCanFrameAux6.empty();
    bRet &= m_msgQueueReceivedCanFrameUpFw.empty();

    if(!bRet) m_pLogger->log(LOG_ERR, "CanLinkBoard::emptyAllQueues--> failed");

    return (bRet == true) ? 0 : -1;
}

int CanLinkBoard::setCanLinkUpFw( const char *strAnswMessageQueue )
{
	m_pCanLinkUpFwThread = new CanLinkUpFw((CanLinkBoard *)this, &m_msgQueueSenderCanFrameUpFw, &m_msgQueueReceivedCanFrameUpFw, m_sectionId);
	m_pCanLinkUpFwThread->setLogger(m_pLogger);
	string strNameMq;
	strNameMq = strAnswMessageQueue;
	m_pCanLinkUpFwThread->initStoreMsgQueue(strNameMq);
	if (!m_pCanLinkUpFwThread->startThread())
	{
		m_pLogger->log(LOG_ERR, "CanLinkBoard::setCanLinkUpFw--> thread not started");
		return -1;
	}

	return 0;
}

int CanLinkBoard::sendUpFwMsg(uint16_t usID, uint8_t *pdata, uint8_t nlen)
{
	if (nlen > DIM_SECTIONLINK_BIN_PROTO_SIZE)
	{
		m_pLogger->log(LOG_ERR, "CanLinkBoard::sendUpFwMsg--> Size too large");
		return -1;
	}

	int res = m_pCanLinkUpFwThread->sendDataUpFw(usID, pdata, nlen);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "CanLinkBoard::sendUpFwMsg--> unable to send data");
		return -1;
	}

	return 0;
}


int CanLinkBoard::receiveUpFwBinMsg(uint8_t *ppayload, uint16_t &usLen, uint16_t ustimeToWait)
{
	uint8_t	payloadAns[DIM_SECTIONLINK_BIN_PROTO_SIZE];
	int res = m_pCanLinkUpFwThread->getAnswUpFwTimed(payloadAns, DIM_SECTIONLINK_BIN_PROTO_SIZE, ustimeToWait);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "CanLinkBoard::receiveUpFwBinMsg--> unable to receive data");
		return -1;
	}
	else
	{

		memcpy(ppayload, payloadAns, DIM_SECTIONLINK_BIN_PROTO_SIZE);
		usLen = DIM_SECTIONLINK_BIN_PROTO_SIZE;
	}

	return 0;
}

int CanLinkBoard::storeAuxData(uint8_t channel, uint8_t* payloadMsg, uint8_t ubLenPayLoad)
{
	deque<uint8_t> * pDataCanLinkAux = 0;

	switch(channel)
	{
		case 1:
			pDataCanLinkAux = &m_dataCanLinkAux1;
		break;
		case 2:
			pDataCanLinkAux = &m_dataCanLinkAux2;
		break;
		case 3:
			pDataCanLinkAux = &m_dataCanLinkAux3;
		break;
		case 4:
			pDataCanLinkAux = &m_dataCanLinkAux4;
		break;
		case 5:
			pDataCanLinkAux = &m_dataCanLinkAux5;
		break;
		case 6:
			pDataCanLinkAux = &m_dataCanLinkAux6;
		break;
	}

	if(pDataCanLinkAux == 0) return -1;

    m_mutexDataAux.lock();
	for(uint8_t i = 0; i < ubLenPayLoad; i++, payloadMsg++)
	{
		pDataCanLinkAux->push_back(*payloadMsg);
	}
    int res = pDataCanLinkAux->size();
    m_mutexDataAux.unlock();
    return res;
}

bool CanLinkBoard::getStoredData(uint8_t channel, uint8_t * dataAux, uint8_t ubLenPayLoad)
{
	deque<uint8_t> * pDataCanLinkAux = 0;

	switch(channel)
	{
		case 1:
			pDataCanLinkAux = &m_dataCanLinkAux1;
		break;
		case 2:
			pDataCanLinkAux = &m_dataCanLinkAux2;
		break;
		case 3:
			pDataCanLinkAux = &m_dataCanLinkAux3;
		break;
		case 4:
			pDataCanLinkAux = &m_dataCanLinkAux4;
		break;
		case 5:
			pDataCanLinkAux = &m_dataCanLinkAux5;
		break;
		case 6:
			pDataCanLinkAux = &m_dataCanLinkAux6;
		break;
	}

	if(pDataCanLinkAux == 0) return false;

	if(pDataCanLinkAux->size() == 0 ||
		pDataCanLinkAux->size() < ubLenPayLoad) return false;

    m_mutexDataAux.lock();
    for(uint8_t i = 0; i < ubLenPayLoad; i++, dataAux++)
	{
		*dataAux = pDataCanLinkAux->front();
		pDataCanLinkAux->pop_front();
	}
    m_mutexDataAux.unlock();
    return true;
}

void CanLinkBoard::stopReceiveMsgFromManagerThread( void )
{
    //RBB Stop and delete here CanLinkAux threads
    for(unsigned int i = 0; i < m_pCanLinkAux.size(); i++)
    {
        CanLinkAux *pCanLinkAux = m_pCanLinkAux.at(i);
        pCanLinkAux->stopThread();
        SAFE_DELETE(pCanLinkAux);
    }
    clearAuxData();

	//stop the thread for firmware update (binary protocol format)
    #ifndef __DEBUG_PROTOCOL
	m_pCanLinkUpFwThread->stopThread();
    SAFE_DELETE(m_pCanLinkUpFwThread);
    #endif

	//stop the thread for ascii protocol format (GPF)
	this->stopThread();
}

int CanLinkBoard::sendDataFrame(uint8_t id, uint8_t *pdata, uint8_t nlen)
{
	if (nlen > MAX_LEN_PAYLOAD_PACK)
	{
		m_pLogger->log(LOG_ERR,"CanLinkBoard::sendDataFrame --> Length of data too long");
		return -1;
	}

	//Frame
    t_canFrame  canData;
	canData.Frame.can_id = id;
    canData.Frame.can_dlc = nlen;
	memcpy(&canData.Frame.data, pdata, nlen);

	//TS
    struct timeval timeStamp;
    gettimeofday(&timeStamp, NULL);
    time_t curr_sec = timeStamp.tv_sec;
    int	curr_msec = timeStamp.tv_usec / 1000;
    sprintf(canData.time_msg, "%.19s.%03d", ctime(&curr_sec), curr_msec);

    if ( !m_msgQueueSenderCanFrame.send((char *)&canData, sizeof(t_canFrame), 0, 1000) )
	{
		m_pLogger->log(LOG_ERR,"CanLinkBoard::sendDataFrame --> Send m_msgQueueSenderCanFrame error <%s>",
					   strerror(errno));
		return -1;
	}
	return 0;
}

void CanLinkBoard::beforeWorkerThread()
{
    m_pLogger->log(LOG_DEBUG, "CANLinkBoard: thread START");
}

int CanLinkBoard::workerThread()
{
	char sInBuffer[SCT_MQ_CANLINK_MSG_BUFFER_SIZE];

    while ( isRunning() )
	{
		unsigned int nMsgPriority;
		int nNumBytesReceived = m_msgQueueReceivedCanFrame.receive(sInBuffer, SCT_MQ_CANLINK_MSG_BUFFER_SIZE,
                                                                   &nMsgPriority,  CANLINKBOARD_TO_MQ_RX);

		if ( nNumBytesReceived == -1 )
		{
			if ( errno == ETIMEDOUT )
			{
                m_pLogger->log(LOG_DEBUG_IPER_PARANOIC, "CanLinkBoard::workerThread--> TIMEOUT m_msgQueueReceivedCanFrame <%c>", m_sectionId + A_CHAR);
            }
			else
			{
                m_pLogger->log(LOG_ERR, "CanLinkBoard::workerThread-->ERROR from m_msgQueueReceivedCanFrame <%c>", m_sectionId + A_CHAR);
			}
		}
		else
		{
            m_pLogger->log(LOG_DEBUG_PARANOIC, "CanLinkBoard::workerThread <%c> --> %d", m_sectionId + A_CHAR,  nNumBytesReceived);
            parseMsgFromLink( (t_canFrame *)sInBuffer );
        }

        usleep(2);
	}
	return 0;
}

void CanLinkBoard::afterWorkerThread()
{
    m_pLogger->log(LOG_DEBUG,"CANLinkBoard: thread END");
}

// ------------------------------------------------------------------------------------------ //

CanLinkAux::CanLinkAux(CanLinkBoard	*pCanLink, MessageQueue* pMsgQueue, int section, int channel)
{
	m_pMsgQueue = pMsgQueue;
	m_section = section;
	m_channel = channel;
	m_pCanLink = pCanLink;
}

CanLinkAux::~CanLinkAux()
{

}


void CanLinkAux::initLogger(Log* plogger)
{
	m_pLogger = plogger;
}

int CanLinkAux::workerThread()
{
	char sInBuffer[SCT_MQ_CANLINK_MSG_BUFFER_SIZE];

	m_pLogger->log(LOG_INFO, "CanLinkBoardAux::thread Start %d %d", m_section, m_channel);

	while ( isRunning() )
	{
		unsigned int nMsgPriority;
		int nNumBytesReceived = m_pMsgQueue->receive(sInBuffer, SCT_MQ_CANLINK_MSG_BUFFER_SIZE,
                                                               &nMsgPriority, SCT_MQ_TIMEOUT_RX);

		if ( nNumBytesReceived == -1 )
		{
			if ( errno == ETIMEDOUT )
			{
               m_pLogger->log(LOG_DEBUG_IPER_PARANOIC, "CanLinkAux::workerThread--> TIMEOUT m_msgQueueReceivedCanFrame");
			}
			else
			{
                m_pLogger->log(LOG_ERR, "CanLinkAux::workerThread-->ERROR from m_msgQueueReceivedCanFrame");
			}
		}
		else
		{
            t_canFrame * pCanFrameReceived;
            pCanFrameReceived = (t_canFrame *)(&sInBuffer);

			if(m_pCanLink)
			{
				// extract data and store them in a dedicated structure
                if(pCanFrameReceived->Frame.can_dlc < MAX_LEN_PAYLOAD_PACK)
                {
                    // extend data with zeros if necessary
                    uint8_t i;
                    for(i = pCanFrameReceived->Frame.can_dlc; i < MAX_LEN_PAYLOAD_PACK; i++)
                    {
                        pCanFrameReceived->Frame.data[i] = 0;
                    }
                }

				// the storage is always composed of 8 bytes ( = 0 if not read from CAN)
                int ret = m_pCanLink->storeAuxData(m_channel, pCanFrameReceived->Frame.data, MAX_LEN_PAYLOAD_PACK);
                if(ret == -1)
				{
                    m_pLogger->log(LOG_ERR, "CanLinkAux::workerThreadAux--> --------%d %d", m_section, m_channel);
				}
				else
				{
                    m_pLogger->log(LOG_DEBUG_IPER_PARANOIC, "CanLinkAux::workerThreadAux--> stored : %d %d %d",
								   m_section, m_channel, ret);
				}
			}
		}

        usleep(2);
	}
	return 0;
}

void CanLinkAux::beforeWorkerThread()
{
    m_pLogger->log(LOG_DEBUG,"CANLinkBoardAux: thread START");
}

void CanLinkAux::afterWorkerThread()
{
    m_pLogger->log(LOG_DEBUG,"CANLinkBoardAux: thread END");
}
