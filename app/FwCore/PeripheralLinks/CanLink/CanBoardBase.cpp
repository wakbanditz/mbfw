/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CanBoardBase.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CanBoardBase class.
 @details

 ****************************************************************************
*/


#include <fstream>
#include <time.h>
#include <sys/time.h>

#include "CanBoardBase.h"
#include "CanLinkCommonInclude.h"
#include "CommonInclude.h"
#include "ErrorUtilities.h"
#include "TimeStamp.h"
#include "MainExecutor.h"


#define	CODED_FILE_NAME			"_coded.cfg"
#define	CALIBRATION_FILE_NAME	"_cal.cfg"


// ----------------------------------------------------------------------------- //

CanBoardBase::CanBoardBase(int section)
{
	m_section = section;

	m_pLogger				= NULL;
	m_pSectionBoardLink		= NULL;
	m_pMsgQueueAnsw			= NULL;
	m_pMsgQueueAutoAnsw		= NULL;
	m_ptrMessageFunction	= NULL;
	m_strDevice.clear();
	m_vectorErrors.clear();
	m_vectorPressures.clear();
	m_vectorPressuresActive.clear();
	m_vectorCalibration.clear();
	m_vectorCoded.clear();
    m_bSendInProgress = false;
    m_bUnknownReplyFromSection = false;
}

CanBoardBase::~CanBoardBase()
{
	int i;
	SectionError * pError;

	for(i = m_vectorErrors.size() -1; i >= 0; i--)
	{
		pError = m_vectorErrors.at(i);
        SAFE_DELETE(pError);
	}
	for(i = m_vectorPressures.size() -1; i >= 0; i--)
	{
		pError = m_vectorPressures.at(i);
        SAFE_DELETE(pError);
	}

	removePressureErrors();

	m_vectorErrors.clear();
	m_vectorPressures.clear();
	m_vectorCalibration.clear();
	m_vectorCoded.clear();
}

void CanBoardBase::addCallbackEvent(int (*ptrFunction)(int, int, int))
{
	m_ptrMessageFunction = ptrFunction;
}

void CanBoardBase::clearAllErrors()
{
	SectionError * pError;

	for(size_t i = 0; i < m_vectorErrors.size(); i++)
	{
		pError = m_vectorErrors.at(i);
		pError->setActive(false);
	}
	// probably the following loop is not necessary ...
	for(int i = m_vectorPressures.size() -1; i >= 0; i--)
	{
		pError = m_vectorPressures.at(i);
		pError->setActive(false);
	}

	removePressureErrors();
}

void CanBoardBase::removePressureErrors()
{
	int i;
	PressureError * pError;

	// for the Pressure Error the corresponding vector must be cleared
	// (multiple errors of the same kind are allowed)

	for(i = m_vectorPressuresActive.size() -1; i >= 0; i--)
	{
		pError = m_vectorPressuresActive.at(i);
        SAFE_DELETE(pError);
	}
	m_vectorPressuresActive.clear();
}

int CanBoardBase::closeAndUnlinkMsgQueue( void )
{
	/* Close and unlink message queue */
	bool bRet = m_pMsgQueueAnsw->closeAndUnlink();
	if ( bRet == false )
	{
		m_pLogger->log(LOG_ERR,"CanBoardBase: error from mq_unlink");

		return -1;
	}
	else
	{
		m_pLogger->log(LOG_INFO,"CanBoardBase: receive m_msgQueueAnsw unlinked");
	}
	SAFE_DELETE (m_pMsgQueueAnsw);

	/* Close and unlink message queue */
	if (m_pMsgQueueAutoAnsw != NULL)
	{
		bRet = m_pMsgQueueAutoAnsw->closeAndUnlink();
		if ( bRet == false )
		{
			m_pLogger->log(LOG_ERR,"CanBoardBase::closeAndUnlinkMsgQueue--> error from mq_unlink");

			return -1;
		}
		else
		{
			m_pLogger->log(LOG_INFO,"CanBoardBase::closeAndUnlinkMsgQueue--> receive m_msgQueueAutoAnsw unlinked");
		}

		SAFE_DELETE (m_pMsgQueueAutoAnsw);
	}

	return 0;
}

int CanBoardBase::init(Log *plogger, const char *smsgQueueAns, const char *smsgQueueAutoAns,
					   SectionBoardLink * psectionBoardLink )
{
	if ( (plogger == NULL)         ||
		 (smsgQueueAns == NULL)    ||
		 (psectionBoardLink == NULL))
	{
		return -1;
	}
	else
	{
		m_pLogger = plogger;
		m_pSectionBoardLink = psectionBoardLink;

		/*!***************************************
		 * MSG QUEUE ANSW                        *
		 *****************************************/
		m_pMsgQueueAnsw = new MessageQueue();
		/* Create message queue for answer from section board */
		bool bRet = m_pMsgQueueAnsw->open(smsgQueueAns, O_RDWR);
		if ( bRet == false )
		{
            bRet = m_pMsgQueueAnsw->create(smsgQueueAns, O_RDWR, LOCAL_MQ_CANBOARD_MAX_MESSAGES, MQ_CANBOARD_MAX_MSG_SIZE);
			if ( bRet == false)
			{
				m_pLogger->log(LOG_ERR, "CanBoardBase::init--> Unable to create receive MQ%s. Exit.", smsgQueueAns);
				return -1;
			}
		}

		/* message queue pre emptying. It performs a cleaning of remained buffers. */
		bRet = m_pMsgQueueAnsw->empty();
		if ( bRet == false )
		{
			m_pLogger->log(LOG_ERR, "CanBoardBase::init--> Unable to PRE-EMPTY receive MQ%s. Exit.", smsgQueueAns);
			return -1;
		}

		/*!***************************************
		 * AUTO MSG QUEUE ANSW                   *
		 *****************************************/
		if ( smsgQueueAutoAns != NULL )
		{
			m_pMsgQueueAutoAnsw = new MessageQueue();
			bRet = setAutoAnswMsgQueue( smsgQueueAutoAns );
			if ( bRet == false )
			{
				return -1;
			}
		}

		/*!***************************************
		 * CONFIGURATION FILES           *
		 *****************************************/
		buildErrorVector();
		buildPositionsVector();

	}

	return 0;
}

bool CanBoardBase::setAutoAnswMsgQueue( const char *smsgQueueAutoAns )
{
	bool bRet = m_pMsgQueueAutoAnsw->open(smsgQueueAutoAns, O_RDWR);
	if ( bRet == false )
	{
        bRet = m_pMsgQueueAutoAnsw->create(smsgQueueAutoAns, O_RDWR, LOCAL_MQ_CANBOARD_MAX_MESSAGES, MQ_CANBOARD_MAX_MSG_SIZE);
		if ( bRet == false )
		{
			m_pLogger->log(LOG_ERR, "CanBoardBase::setAutoAnswMsgQueue--> Unable to create receive MQ%s. Exit.",
						   smsgQueueAutoAns);
			return false;
		}
	}

	/* message queue pre emptying. It performs a cleaning of remained buffers. */
	bRet = m_pMsgQueueAutoAnsw->empty();
	if ( bRet == false )
	{
		m_pLogger->log(LOG_ERR, "CanBoardBase::setAutoAnswMsgQueue--> Unable to PRE-EMPTY receive MQ%s. Exit.",
					   smsgQueueAutoAns);
		return false;
	}

	return true;
}

bool CanBoardBase::cleanQueues()
{
	bool bRet = false;


	if(m_pMsgQueueAnsw && m_pMsgQueueAutoAnsw)
	{
        m_pMsgQueueAnsw->enableDebug(true);
        m_pMsgQueueAutoAnsw->enableDebug(true);

        char		sInBuffer[MQ_CANBOARD_MAX_MSG_SIZE_BUFF + 1];
        unsigned int nMsgPriority = 0;

        while( m_pMsgQueueAnsw->receive(sInBuffer, MQ_CANBOARD_MAX_MSG_SIZE_BUFF + 1, &nMsgPriority, 100) != -1) msleep(2);
        while( m_pMsgQueueAutoAnsw->receive(sInBuffer, MQ_CANBOARD_MAX_MSG_SIZE_BUFF + 1, &nMsgPriority, 100) != -1) msleep(2);

        m_pMsgQueueAnsw->enableDebug(false);
        m_pMsgQueueAutoAnsw->enableDebug(false);

        bRet = (m_pMsgQueueAnsw->empty() && m_pMsgQueueAutoAnsw->empty());
    }
/*
	if(bRet == false)
	{
        m_pLogger->log(LOG_ERR, "CanBoardBase::clean--> Unable to PRE-EMPTY receive MQs. Exit.");
    }*/
	return bRet;
}

int CanBoardBase::sendToMsgQueueAnsw( char *pmsgData, uint16_t nlen )
{
	bool bres = m_pMsgQueueAnsw->send( pmsgData, nlen, 0, 1000 );
	if (bres != true)
	{
		m_pLogger->log(LOG_ERR, "CanBoardBase::sendToMsgQueueAnsw--> unable to send on m_msgQueueAnsw");
		return -1;
	}

	return 0;
}

int CanBoardBase::sendToMsgQueueAutoAnsw( char *pmsgData, uint16_t nlen )
{
	bool bRes = m_pMsgQueueAutoAnsw->send( pmsgData, nlen, 0, 1000 );
	if (bRes != true )
	{
		m_pLogger->log(LOG_ERR, "CanBoardBase::sendToMsgQueueAutoAnsw--> unable to send on sendToMsgQueueAutoAnsw");
		return -1;
	}

	return 0;
}

int CanBoardBase::getAnswTimed( uint8_t * ppayLoad, uint16_t *ppayloadLenAnsw, uint32_t ntimeToWait )
{
	uint16_t	nlenAnsw = 0;
	unsigned int nMsgPriority;
	int timeoutMsecs;

    if(ppayLoad == 0)
    {
        return -1;
    }

    timeoutMsecs = (int)ntimeToWait; // the parameter is in msecs

    int nNumBytesReceived = m_pMsgQueueAnsw->receive((char *)ppayLoad, MQ_CANBOARD_MAX_MSG_SIZE_BUFF, &nMsgPriority, timeoutMsecs);
	if ( nNumBytesReceived == -1 )
	{
		if ( errno == ETIMEDOUT )
		{
//            m_pLogger->log(LOG_ERR, "CanBoardBase::getAnswTimed--> TIME-OUT on mq_timedreceive() MQ");
		}
		else
		{
//            m_pLogger->log(LOG_ERR, "CanBoardBase::getAnswTimed--> error from mq_timedreceive() MQ");
		}
		return -1;
	}
	else
	{
		//Message is available, get it
        nlenAnsw = strlen( (char *)ppayLoad);
		*ppayloadLenAnsw = nlenAnsw;
	}

    return 0;
}

bool CanBoardBase::registerUnknownReplyFromSection()
{
    if(m_bSendInProgress == true)
    {
        m_bUnknownReplyFromSection = true;
    }
    return m_bSendInProgress;
}


int CanBoardBase::waitAnswMsg( uint8_t devAddr, uint8_t cmdType, uint8_t * pBuffPayLoad, uint32_t ntimeToWait )
{
	uint16_t	uslenAnswer;

    if(pBuffPayLoad == 0)
    {
        // buffer not valid ? should not happen ...
        return -4;
    }

    int res = getAnswTimed(pBuffPayLoad, &uslenAnswer, ntimeToWait );
    m_pLogger->log(LOG_DEBUG_PARANOIC, "CanBoardBase::waitAnswMsg --> %s dev=%x cmd=%x", pBuffPayLoad, devAddr, cmdType);

	if ( res == 0 )
	{
        if (( devAddr == pBuffPayLoad[ IDX_CAN_ORIGIN ] ) && ( cmdType == pBuffPayLoad[ IDX_CAN_TYPE_MSG ] ))
        {
            switch ( pBuffPayLoad[ IDX_CAN_FLAG_MSG ] )
			{
				case CANSTD_ACCEPTED:
				case CANSTD_COMPLETED:
				{
					//command accepted or executed
				}
				break;

				case CANSTD_REFUSED:
				case CANSTD_BADFORMAT:
				default:
				{
                    // command not accepted
                    m_pLogger->log(LOG_ERR, "CanBoardBase::waitAnswMsg ERROR --> %s dev=%c cmd=%c",
                                   pBuffPayLoad, devAddr, cmdType);
					return -1;
				}
				break;
			}
			return 0;
		}
		else
		{
            // answer not corresponding
            m_pLogger->log(LOG_ERR, "CanBoardBase::waitAnswMsg2--> %s (dev=%c cmd=%c)",
                           pBuffPayLoad, devAddr, cmdType);
            return -2;
		}
	}
	else
	{
        // no answer: did we get a UNKNOWN reply (not linked to the device) ?
        if(m_bUnknownReplyFromSection == true)
        {
            m_bUnknownReplyFromSection = false;
            return -3;
        }
        else
        {
            return -4;
        }
	}

	return 0;
}

int CanBoardBase::sendCanMsg(uint8_t *pMsg, uint16_t nlen, uint8_t * pucAnsMsg, uint32_t usMillisecToWait, bool bErrorSet)
{
    char		sInBuffer[MQ_CANBOARD_MAX_MSG_SIZE_BUFF + 1];
    unsigned int nMsgPriority = 0;

    while( m_pMsgQueueAnsw->receive(sInBuffer, MQ_CANBOARD_MAX_MSG_SIZE_BUFF + 1, &nMsgPriority, 2) != -1) msleep(1);

    if (!m_pMsgQueueAnsw->empty())
	{
		m_pLogger->log(LOG_ERR, "CanBoardBase::sendCanMsg --> Unable to empty the MQ");
		return -1;
	}

    m_bSendInProgress = true;
    m_bUnknownReplyFromSection = false;

    uint8_t retryCounter = 0;

    while(true)
    {
        int ret = m_pSectionBoardLink->sendMsg( pMsg, nlen );
        if ( ret == 0 )
        {
            m_pLogger->log(LOG_DEBUG_PARANOIC, "CanBoardBase::sendCanMsg --> sent ");
        }
        else
        {
            if(bErrorSet)
            {
                // process the error code (created here)
                setEventToDecode('M', '3');
            }
            m_pLogger->log(LOG_ERR, "CanBoardBase::sendCanMsg --> cmd not sent");
            m_bSendInProgress = false;
            sprintf((char *)pucAnsMsg, "%s",  "NoAnswer");
            return -1;
        }

        ret = waitAnswMsg(pMsg[ IDX_CAN_ORIGIN ], pMsg[ IDX_CAN_TYPE_MSG ], pucAnsMsg, usMillisecToWait);

        uint16_t usLen = strlen((char*)pucAnsMsg);
        pucAnsMsg[usLen] = 0;

        if ( ret == 0 )
        {
            //command accepted
            m_pLogger->log(LOG_DEBUG_PARANOIC, "CanBoardBase::sendCanMsg --> Received msgData %s", pucAnsMsg);
            break;
        }
        else
        {
            if(bErrorSet)
            {
                if(ret == -1)
                {
                    // command refused or not correctly formatted
                    while(usLen < 3)
                    {
                        pucAnsMsg[usLen] = '0';
                        usLen++;
                    }
                    // process the error code
                    pucAnsMsg[3] = '0';	// to standardize
                    pucAnsMsg[4] = 0;
                    decodeNotifyEvent((char *)pucAnsMsg, NULL);
                }
                else if(ret == -3 || ret == -4)
                {
                    m_pLogger->log(LOG_INFO, "CanBoardBase::sendCanMsg --> retry %d", retryCounter);
                    sprintf((char *)pucAnsMsg, "%s",  "NoAnswer");
                    // Section notified the command is uncorrect OR
                    // no reply from section: try to Repeat ?
                    if(++retryCounter < 3)
                    {
                        continue;
                    }
                    else
                    {
                        // no answer from section -> communication error
                        setEventToDecode('M', '3');
                    }
                }
                else
                {
                    // not corresponding  answer from section -> communication error
                    setEventToDecode('M', '3');
                }
            }
            m_pLogger->log(LOG_ERR, "CanBoardBase::sendCanMsg --> cmd not accepted");
            m_bSendInProgress = false;
            m_bUnknownReplyFromSection = false;
            return -1;
        }
    }


    int ret = waitAnswMsg(pMsg[IDX_CAN_ORIGIN], pMsg[IDX_CAN_TYPE_MSG], pucAnsMsg, usMillisecToWait);

    int usLen = strlen((char*)pucAnsMsg);
    pucAnsMsg[usLen] = 0;

    if ( ret == 0 )
    {
        m_pLogger->log(LOG_DEBUG_PARANOIC, "CanBoardBase::sendCanMsg --> Received msgData %s", pucAnsMsg);
    }
    else
    {
        m_pLogger->log(LOG_ERR, "CanBoardBase::sendCanMsg --> cmd not executed %s", pucAnsMsg);
        if(bErrorSet)
        {
            if(ret == -1)
            {
                // command not succesfull
                while(usLen < 3)
                {
                    pucAnsMsg[usLen] = '0';
                    usLen++;
                }
                // process the error code
                pucAnsMsg[3] = '0';	// to standardize
                pucAnsMsg[4] = 0;
                decodeNotifyEvent((char *)pucAnsMsg, NULL);
            }
            else
            {
                // no answer from section -> communication error
                sprintf((char *)pucAnsMsg, "%s",  "NoAnswer");
                setEventToDecode('M', '3');
            }
        }
        m_bSendInProgress = false;
        m_bUnknownReplyFromSection = false;
        return -1;
    }

    m_bSendInProgress = false;
    m_bUnknownReplyFromSection = false;
    return 0;
}

void CanBoardBase::stopAsynchronousMsgReceiveThread( void )
{
	this->stopThread(1);
}

void CanBoardBase::beforeWorkerThread( void )
{
    m_pLogger->log(LOG_DEBUG,"CanBoardBase: thread START");
}

int CanBoardBase::workerThread( void )
{
	while( isRunning() )
	{
		uint16_t	uslenAnsw = 0;
		char		sMsg[MQ_CANBOARD_MAX_MSG_SIZE_BUFF];
		unsigned int uiMsgPriority;

		/* Controls if there are some messages in the queue */
		memset(sMsg, 0, sizeof(sMsg));

		int nNumBytesReceived = m_pMsgQueueAutoAnsw->receive(sMsg, MQ_CANBOARD_MAX_MSG_SIZE_BUFF,
															 &uiMsgPriority, CANBOARDBASE_TIMEOUT_RECEIVE_MQ);
		if ( nNumBytesReceived == -1 )
		{
			if ( errno == ETIMEDOUT )
			{
				m_pLogger->log(LOG_DEBUG_IPER_PARANOIC, "CanBoardBase::workerThread--> TIME-OUT on mq_timedreceive()");
			}
			else
			{
				m_pLogger->log(LOG_ERR, "CanBoardBase::workerThread--> error from mq_timedreceive()");
			}
		}
		else
		{
			//Message is available, get it
            uslenAnsw = strlen( sMsg );

            decodeAsynchronousMsg( (uint8_t *)sMsg );
            m_pLogger->log(LOG_DEBUG_PARANOIC, "CanBoardBase::workerThread--> s=%s, NumBytes=%d", sMsg, uslenAnsw);
        }

        usleep(2);
	}

	return 0;
}

void CanBoardBase::afterWorkerThread( void )
{
    m_pLogger->log(LOG_DEBUG_PARANOIC,"CanBoardBase: thread END");
}

int CanBoardBase::buildErrorVector()
{
	int ret = 0;

	if(m_strDevice.empty()) return ret;

	// build the errors list

	m_vectorErrors.clear();
	ret += parseBoardErrorFile((std::vector< GeneralError * > * )&m_vectorErrors, eErrorSection);

	m_vectorPressures.clear();
	ret += parseBoardErrorFile((std::vector< GeneralError * > * ) &m_vectorPressures, eErrorPressure);

	return ret;
}

int CanBoardBase::parseBoardErrorFile(std::vector< GeneralError * > * pVectorErrors, uint8_t errorType)
{
	// from ErrorUtilities
	return parseErrorFile(m_strDevice, pVectorErrors, errorType);
}

int CanBoardBase::buildPositionsVector()
{
	if(m_strDevice.empty()) return 0;

	// build the positions (calibrations and coded) list

	std::string fileName;
	std::string strRead;
	std::ifstream inFile;
	uint8_t counter;

	m_vectorCalibration.clear();
	m_vectorCoded.clear();

	for(counter = 0; counter < 2; counter++)
	{
		fileName.assign(POSITIONS_FOLDER);
		fileName.append(m_strDevice);
		if(counter == 0)
			fileName.append(CALIBRATION_FILE_NAME);
		else
			fileName.append(CODED_FILE_NAME);

		inFile.open(fileName);

		if(inFile.fail())
		{
			//File does not exist code here
			m_pLogger->log(LOG_ERR, "CanBoardBase::error file-->%s %s", fileName.c_str(), strerror(errno));
			continue;
		}

		while(!inFile.eof())
		{
			getline(inFile, strRead);
			if(!strRead.empty())
			{
				// lines beginning with # are comments
				if(strRead.front() != '#')
				{

					// split the line separating the fields
					std::vector<std::string> listStr;

					listStr = splitString(strRead, "=");
					if(listStr.size() == 2)
					{
						// split is valid: add to vector
						if(counter == 0)
						{
							m_vectorCalibration.emplace_back(listStr.at(0), listStr.at(1));
						}
						else
						{
							m_vectorCoded.emplace_back(listStr.at(0), listStr.at(1));
						}
					}
				}
			}
		}
		inFile.close();
	}
	return (m_vectorCalibration.size() + m_vectorCoded.size());
}

std::string CanBoardBase::getCodedFromLabel(std::string strLabel)
{
	std::vector<std::pair<std::string, std::string>>::const_iterator posIterator;
	std::string strCoded;

	strCoded.clear();

	for(posIterator = m_vectorCoded.begin();
		posIterator != m_vectorCoded.end();
		posIterator++)
	{
		if(strcmp(strLabel.c_str(), posIterator->first.c_str()) == 0)
		{
			strCoded.assign(posIterator->second);
			break;
		}
	}
	return strCoded;
}

string CanBoardBase::getCalibrationFromLabel(string strLabel)
{
	std::vector<std::pair<std::string, std::string>>::const_iterator posIterator;
	std::string strCoded;

	strCoded.clear();

	for(posIterator = m_vectorCalibration.begin();
		posIterator != m_vectorCalibration.end();
		posIterator++)
	{
		if(strcmp(strLabel.c_str(), posIterator->first.c_str()) == 0)
		{
			strCoded.assign(posIterator->second);
			break;
		}
	}
	return strCoded;
}

int CanBoardBase::getCalibrationLabelList(std::vector<std::string> * pStrVector)
{
	std::vector<std::pair<std::string, std::string>>::const_iterator posIterator;

	pStrVector->clear();

	for(posIterator = m_vectorCalibration.begin();
		posIterator != m_vectorCalibration.end();
		posIterator++)
	{
			pStrVector->push_back(posIterator->first);
	}
	return pStrVector->size();
}

bool CanBoardBase::isCalibrationLabelValid(std::string strLabel)
{
	std::vector<std::pair<std::string, std::string>>::const_iterator posIterator;
	bool bRet = false;

	for(posIterator = m_vectorCalibration.begin();
		posIterator != m_vectorCalibration.end();
		posIterator++)
	{
		if(strcmp(strLabel.c_str(), posIterator->first.c_str()) == 0)
		{
			bRet = true;
			break;
		}

	}
	return bRet;
}

bool CanBoardBase::setEventToDecode(char firstChar, char secondChar)
{
    char pubMsg[5];
    pubMsg[0] = ZERO_CHAR;
    pubMsg[1] = ZERO_CHAR;
    pubMsg[2] = firstChar;
    pubMsg[3] = secondChar;
    pubMsg[4] = 0;

    return decodeNotifyEvent(pubMsg, 0);
}

bool CanBoardBase::decodeNotifyEvent(char* pubMsg, char* pcDescription)
{
	int ret = 0;
	SectionError * pSectionErrorFound = 0;

	if(pcDescription) *pcDescription = 0;

    m_pLogger->log(LOG_DEBUG, "CanBoardBase::decodeNotifyEvent %c: %s", m_section + A_CHAR, pubMsg);

	ret = searchEventError(pubMsg, &m_vectorErrors, false, pSectionErrorFound, pcDescription);
	if(ret > 0)
	{
		// the error has been identified
		if(ret == 3)
		{
			// new error has been set  ->
			// wake up the VidasEp thread to send the new list of errors
			requestVidasEp();
		}
		return true;
	}

	// for the pressure we don't care if the error is already active as it may happen on multiple situations
	ret = searchEventError(pubMsg, &m_vectorPressures, true, pSectionErrorFound, pcDescription);
	if(ret > 0)
	{
		// the error has been identified
		if(ret == 3)
		{
			// new error has been set  ->
			// register it according to the well / channel
			if(pSectionErrorFound)
			{
				if(strlen(pubMsg) == 6)
				{
					PressureError * pPressureError;
					pPressureError = new PressureError();

					pPressureError->setErrorInfo(pSectionErrorFound);
					pPressureError->setPositionInfo((uint8_t)pubMsg[4], (uint8_t)pubMsg[5]);

					m_vectorPressuresActive.push_back(pPressureError);
					// wake up the VidasEp thread to send the new list of errors
					requestVidasEp();
				}
			}
		}
		return true;
	}

	return false;
}

int CanBoardBase::searchEventError(char* pubMsg, std::vector< SectionError * > * pVectorErrors,
								   bool bActiveDisabled,  SectionError * pSectionErrorFound,
								   char* pcDescription)
{
	uint8_t	ubFlag1 = pubMsg[2];
	uint8_t	ubFlag2 = pubMsg[3];
	int ret = 0;

	pSectionErrorFound = 0;

	m_pLogger->log(LOG_DEBUG_PARANOIC, "CanBoardBase::searchEvent %c%c %d", ubFlag1, ubFlag2, pVectorErrors->size());

	for(size_t i = 0; i < pVectorErrors->size(); i++)
	{
		// check if the message sent from the board correspond to one stored in the list
		SectionError * pSectionError = pVectorErrors->at(i);

		if(pSectionError->compareChars(ubFlag1, ubFlag2))
		{
			// get the info
			std::string strDescription = pSectionError->getDescription();
			if(pcDescription) strcpy(pcDescription, strDescription.c_str());

            m_pLogger->log(LOG_INFO, "CanBoardBase %c device %s : %s - %d - %d %s - %d",
                           m_section + A_CHAR,
						   m_strDevice.c_str(),
                           pSectionError->getCode().c_str(),
						   pSectionError->getLevel(),
						   pSectionError->getEnabled(),
						   strDescription.c_str(),
						   pSectionError->getRestoreEnabled());

			// log the error on Masterboard
			registerErrorEvent(m_section, m_strDevice,
							   pSectionError->getCode(),
							   pSectionError->getLevel(),
							   strDescription,
							   pSectionError->getEnabled(),
							   pSectionError->getActive());

			// pointer to the corresponding SectionErr object
			pSectionErrorFound = pSectionError;
			ret += 1;

			// if the error is enabled (and not yet active)
			// or don't care aboute the status (pressure error can be multiples) -> we activate it
			if ((pSectionError->getEnabled() == true) &&
					((bActiveDisabled == true) || (pSectionError->getActive() == false)))
			{
				// read from the database if the time has been set by Gateway
				pSectionError->setActive(true, infoSingleton()->getSetTime());
				// if the error is identified as Alarm or Error
				if (pSectionError->getLevel() > GENERAL_ERROR_LEVEL_WARNING)
				{
					// change the status of the section to Error
					if(m_section == SCT_A_ID)
						changeDeviceStatus(eIdSectionA, eStatusError);
					else
						changeDeviceStatus(eIdSectionB, eStatusError);

					// request the MasterBoard to set the Module LED
					requestModuleLed();
				}

                m_pLogger->log(LOG_INFO, "CanBoardBase %c error %s : %s - %d  %s - %d %ld",
                               m_section + A_CHAR,
							   m_strDevice.c_str(),
                               pSectionError->getCode().c_str(),
							   pSectionError->getLevel(),
							   strDescription.c_str(),
                               pSectionError->getActive(),
							   pSectionError->getIntTime());
				// the error has been set
				ret += 2;
			}
			break;
		}
	}
	return ret;
}

int CanBoardBase::updateErrorsTime(uint64_t currentMsec, tm* currentTime)
{
	int counter = 0;

	for(size_t i = 0; i < m_vectorErrors.size(); i++)
	{
		// check if the message sent from the board correspond to one stored in the list
		SectionError * pSectionError = m_vectorErrors.at(i);

		if(pSectionError->getActive() == true &&
			pSectionError->getStrTime().empty() )
		{
			// the error is set but no string time is linked to it
			// calculate the difference between the error msecs and the current one

			uint64_t errorMsec = pSectionError->getIntTime();
			uint64_t diffSecs = 0;

			m_pLogger->log(LOG_INFO, "CanBoardBase current %llu : error %llu", currentMsec, errorMsec);

			if(currentMsec > errorMsec)
			{
				diffSecs = currentMsec - errorMsec;
			}

			float clkRatio = infoSingleton()->getClockRatio();
			diffSecs = (uint64_t)((float)diffSecs / clkRatio);

			// secs equivalent to the new time structure
			time_t totalSeconds = mktime(currentTime);
			time_t diffSecToSet = (totalSeconds - (time_t)diffSecs);

            m_pLogger->log(LOG_INFO, "CanBoardBase %c: %llu %llu %llu", m_section + A_CHAR, totalSeconds, diffSecs, diffSecToSet);

			struct tm * setTime = gmtime(&diffSecToSet);

			char bufferTmp[64];
			sprintf(bufferTmp, "%04d%02d%02d%02d%02d%02d",
				setTime->tm_year + 1900, setTime->tm_mon + 1, setTime->tm_mday,
				setTime->tm_hour, setTime->tm_min, setTime->tm_sec);

			std::string strTime;
			strTime.assign(bufferTmp);
			pSectionError->setStrTime(strTime);

			m_pLogger->log(LOG_INFO, "CanBoardBase device %s : %s %s",
						   m_strDevice.c_str(),
						   pSectionError->getStrTime().c_str(),
						   pSectionError->getDescription().c_str());
			counter++;
		}
	}
	return counter;
}

bool CanBoardBase::clearTemperatureError()
{
	bool bErrorCleared = false;

	for(size_t i = 0; i < m_vectorErrors.size(); i++)
	{
		// check if the message sent from the board correspond to one stored in the list
		SectionError * pSectionError = m_vectorErrors.at(i);

        // the error is related to temperature and it's active -> clear it
		if(pSectionError->getActive() == true &&
            pSectionError->getRestoreEnabled() == true )
		{
			// the error is related to temperature and it's active -> clear it
            m_pLogger->log(LOG_INFO, "CanBoardBase %c clear %s %s", m_section + A_CHAR, m_strDevice.c_str(),
																pSectionError->getDescription().c_str());

			pSectionError->setActive(false);
			bErrorCleared = true;
		}
	}
	return bErrorCleared;
}

int CanBoardBase::getErrorList(std::vector<string>* pVectorList, uint8_t errorLevel)
{
	// NOTE don't clear the vector but add elements !
	std::string strError;
	size_t i;

    for(i = 0; i < m_vectorErrors.size(); i++)
	{
		// check if the error is activated
		SectionError * pSectionError = m_vectorErrors.at(i);

		if(pSectionError->getActive() == true &&
			pSectionError->getStrTime().empty() == false)
		{
            if(errorLevel == GENERAL_ERROR_LEVEL_NONE ||
                    pSectionError->getLevel() == errorLevel)
            {
                // the string is formatted as timestamp#code#category#description
                strError.assign(pSectionError->getStrTime());
                strError.append(HASHTAG_SEPARATOR);
                strError.append(pSectionError->getCode());
                strError.append(HASHTAG_SEPARATOR);
                strError.append(pSectionError->getCategory());
                strError.append(HASHTAG_SEPARATOR);
                strError.append(pSectionError->getDescription());

                pVectorList->push_back(strError);
            }
		}
	}

	// same loop for Pressure Errors
    getPressureErrorListPerStripWell(pVectorList, SCT_NUM_TOT_SLOTS, SCT_SLOT_NUM_WELLS, GENERAL_ERROR_LEVEL_NONE);

	return pVectorList->size();
}

int CanBoardBase::getPressureErrorListPerStripWell(std::vector<string>* pVectorList,
                                                   uint8_t strip, uint8_t well, uint8_t errorLevel)
{
	// NOTE don't clear the vector but add elements !
	std::string strError;
	size_t i;

	for(i = 0; i < m_vectorPressuresActive.size(); i++)
	{
		// check if the error is activated
		PressureError * pPressureError = m_vectorPressuresActive.at(i);

		if(pPressureError->getActive() == true &&
			pPressureError->getStrTime().empty() == false)
		{
			// check the correspondence with strip and well (if requested)
			if( (strip == SCT_NUM_TOT_SLOTS || strip == pPressureError->getChannel()) ||
				(well == SCT_SLOT_NUM_WELLS || well == pPressureError->getWell()) )
			{
                if(errorLevel == GENERAL_ERROR_LEVEL_NONE ||
                        pPressureError->getLevel() == errorLevel)
                {
                    // the string is formatted as timestamp#code#category#description#channel#well
                    strError.assign(pPressureError->getStrTime());
                    strError.append(HASHTAG_SEPARATOR);
                    strError.append(pPressureError->getCode());
                    strError.append(HASHTAG_SEPARATOR);
                    strError.append(pPressureError->getCategory());
                    strError.append(HASHTAG_SEPARATOR);
                    strError.append(pPressureError->getDescription());
                    strError.append(HASHTAG_SEPARATOR);
                    strError.push_back(pPressureError->getChannel());
                    strError.append(HASHTAG_SEPARATOR);
                    strError.push_back(pPressureError->getWell());

                    pVectorList->push_back(strError);
                }
			}
		}
	}

	return pVectorList->size();
}


bool CanBoardBase::isErrorActive()
{
    for(size_t i = 0; i < m_vectorErrors.size(); i++)
    {
        // check if the message sent from the board correspond to one stored in the list
        SectionError * pSectionError = m_vectorErrors.at(i);

        // if the error is enabled (and not yet active)
        // or don't care aboute the status (pressure error can be multiples) -> we activate it
        if ((pSectionError->getEnabled() == true) && (pSectionError->getActive() == true))
        {
            // if the error is identified as Alarm or Error
            if (pSectionError->getLevel() > GENERAL_ERROR_LEVEL_WARNING)
            {
                return true;
            }
        }
    }
    return false;
}
