/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionCommonMessage.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionCommonMessage class.
 @details

 ****************************************************************************
*/

#include "SectionCommonMessage.h"

SectionCommonMessage::SectionCommonMessage()
{
//	m_pLogger				= NULL;
	m_pCanBoardSystemBase	= NULL;
	m_ubIdSystem			= SECTION_GENERAL_CHAR;

	for(uint8_t i = 0; i < MAX_CALIBRATIONS_NUM; i++)
	{
		m_calibrationPositions[i] = 0;
	}
	m_motorFactor = 0;
	m_motorMicrostepRes = 0;
	m_motorLowCurrent = 0;
	m_motorHighCurrent = 0;
	m_motorRampFactor = 0;
	m_motorDisactivation = 0;
	m_motorPosition = 0;
}

SectionCommonMessage::~SectionCommonMessage()
{

}

int SectionCommonMessage::init( Log *pLogger, CanBoardBase *pCanBoardSystemBase, uint8_t ubIdSystem )
{

	if (( pLogger == NULL )			   ||
		( pCanBoardSystemBase == NULL) ||
		((ubIdSystem < SECTION_PUMP_CHAR) && (ubIdSystem > SECTION_GENERAL_CHAR)))
	{
		return -1;
	}
	else
	{
		m_pLogger			  = pLogger;
		m_pCanBoardSystemBase = pCanBoardSystemBase;
		m_ubIdSystem		  = ubIdSystem;
	}

	return 0;
}


int SectionCommonMessage::searchHome( uint8_t errorDisable, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

    /* STX | d |’H’ | flag | ETX */
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'H';
    pPayLoad[2] = errorDisable + ZERO_CHAR;

    int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘H’ | f | sign | A1 | A2 | A3 | A4 |ETX
		uint32_t ulValue = (uint32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 1]);

		// Control the sign
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == '-')
		{
			ulValue *= -1LL;
		}
		else
		{
			ulValue *= 1LL;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::searchHome-->Performed %s",pucAnsMsg);
		// store value
		m_motorPosition = ulValue;
	}
	else
	{

		m_pLogger->log(LOG_ERR,"SectionCommonMessage::searchHome-->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::moveByRelativeStep( enumSectionCommonMsgStepDir eDir, enumSectionCommonMsgStepType eTypeStep, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘s’ | dir | steptype | ETX */
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 's';
	pPayLoad[2] = eDir;
	pPayLoad[3] = eTypeStep;

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 4, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘s’ | f | sign | A1 | A2 | A3 | A4 |ETX */
		uint32_t ulValue = (uint32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 1]);

		/* Control the sign */
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == '-')
		{
			ulValue *= -1LL;
		}
		else
		{
			ulValue *= 1LL;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::moveByRelativeStep-->Performed %s",pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::moveByRelativeStep-->ERROR");

		return -1;
	}

	return 0;
}

int SectionCommonMessage::moveToRelativePosition( enumSectionCommonMsgStepDir    eDir,
												  enumSectionCommonMsgStepType   eStepType,
												  enumSectionCommonMsgStepMode   eStepMode,
												  enumSectionCommonMsgSlopeScan  eSlopeScan,
												  enumSectionCommonMsgDecSlope   eDecSlope,
												  enumSectionCommonMsgFreqSource eFreqSource,
												  uint8_t					  ubFreqTable,
												  uint16_t					  usFreq,
												  uint16_t					  usStep,
												  uint16_t usTimeoutMs)
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘p’ | dir | steptype | stepmode | slopescan | decslope | freq_source | freq_table | F1 | F2 | F3 | F4 | Ns1 | Ns2 | Ns3 | Ns4 | ETX */
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'p';
	pPayLoad[2] = eDir;
	pPayLoad[3] = eStepType;
	pPayLoad[4] = eStepMode;
	pPayLoad[5] = eSlopeScan;
	pPayLoad[6] = eDecSlope;
	pPayLoad[7] = eFreqSource;
	pPayLoad[8] = ubFreqTable;

	sprintf((char*)&pPayLoad[9], "%04X%04X", usFreq, usStep );

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 16, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘p’ | f | sign | A1 | A2 | A3 | A4 | ETX */
		uint32_t ulValue = (uint32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 1]);

		/* Control the sign */
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == '-')
		{
			ulValue *= -1LL;
		}
		else
		{
			ulValue *= 1LL;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::moveToRelativePosition-->Performed %s",pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::moveToRelativePosition-->ERROR");
		return -1;
	}

	return 0;
}

int SectionCommonMessage::moveToAbsolutePosition( int16_t ssNumberStep, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘c’ | sign | Ns1 | Ns2 | Ns3 | Ns4 | ETX */
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'c';

	if ( ssNumberStep < 0)
	{
		pPayLoad[2] = '-';
		uint16_t	usModNumberStep = -ssNumberStep;
		sprintf((char*)&pPayLoad[3], "%04X", usModNumberStep);

	}
	else
	{
		pPayLoad[2] = '+';
		uint16_t	usModNumberStep = ssNumberStep;
		sprintf((char*)&pPayLoad[3], "%04X", usModNumberStep);
	}

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 7, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘c’ | f | sign | A1 | A2 | A3 | A4 | ETX */
		uint32_t ulValue = (uint32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 1]);

		/* Control the sign */
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == '-')
		{
			ulValue *= -1LL;
		}
		else
		{
			ulValue *= 1LL;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::moveToAbsolutePosition-->Performed %s",pucAnsMsg);
		// store value
		m_motorPosition = ulValue;
	}
	else
	{

		m_pLogger->log(LOG_ERR,"SectionCommonMessage::moveToAbsolutePosition-->ERROR");
		return -1;
	}

	return 0;
}

int SectionCommonMessage::moveToCodedPosition( uint8_t ubCodedPos, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘P’ | Cpos | ETX */
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'P';
	pPayLoad[2] = ubCodedPos;

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘P’ | f | sign | A1 | A2 | A3 | A4 | ETX */
		uint32_t ulValue = (uint32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 1]);

		/* Control the sign */
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == '-')
		{
			ulValue *= -1LL;
		}
		else
		{
			ulValue *= 1LL;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::moveToCodedPosition-->Performed %s",pucAnsMsg);
		// store value
		m_motorPosition = ulValue;
	}
	else
	{

		m_pLogger->log(LOG_ERR,"SectionCommonMessage::moveToCodedPosition-->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::setNumberRelativeSteps( uint16_t usNumberSteps, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘N’ | N1 | N2 | N3 | N4 |  ETX	*/
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'N';
	sprintf((char*)&pPayLoad[2], "%04X", usNumberSteps);

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 6, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::setNumberRelativeSteps-->Performed %s",pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::setNumberRelativeSteps-->ERROR");
		return -1;
	}

	return 0;
}

int SectionCommonMessage::startMotor( enumSectionCommonMsgStepDir eDir, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘M’ | dir | ETX	*/
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'M';
	pPayLoad[2] = eDir;

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::startMotor-->Performed %s",pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::startMotor-->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::getAbsolutePosition( int32_t &slVal, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘a’ | ETX	*/
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'a';

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘a’ | f  | sign | A1 | A2 | A3 | A4 | ETX */
		slVal = (int32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 1]);

		/* Control the sign */
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == '-')
		{
			slVal *= -1LL;
		}
		else
		{
			slVal *= 1LL;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::getAbsolutePosition-->Performed %s",pucAnsMsg);
		// store value
		m_motorPosition = slVal;
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::getAbsolutePosition-->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::getHomeTolerance( int32_t &slVal, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘G’ | ETX	*/
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'G';

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘G’ | f  | sign | A1 | A2 | A3 | A4 | ETX */
		slVal = (int32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 1]);

		/* Control the sign */
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG ] == '-')
		{
			slVal *= -1LL;
		}
		else
		{
			slVal *= 1LL;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::getHomeTolerance-->Performed %s",pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::getHomeTolerance-->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::setMicrostepResolution( enumSectionCommonMsgStepMode eStepMode, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d |  ‘R’ | stepmode | ETX	*/
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'R';
	pPayLoad[2] = eStepMode;

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘R’ | f | ETX
        m_pLogger->log(LOG_DEBUG,"SectionCommonMessage::setMicrostepResolution-->Performed %s",pucAnsMsg);
		// update the microstep setting
		m_motorMicrostepRes = eStepMode;
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::setMicrostepResolution-->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::setRamp( enumSectionCommonMsgSlopeScan eSlopeScan, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘r’ | slopescan | ETX
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'R';
	pPayLoad[2] = eSlopeScan;

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘r’ | f | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::setRamp-->Performed %s",pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::setRamp-->ERROR");
		return -1;
	}
	return 0;
}


int SectionCommonMessage::getMotorParameters( uint8_t &ubMicrostepRes, uint16_t &usLowCurr, uint16_t &usHighCurr,
											  uint8_t &ubRampFactor, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘E’ | ETX
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'E';

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘E’ | f  | A | B | C | D | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::enabledisableDecererationRamp-->Performed %s",pucAnsMsg);

		ubMicrostepRes	= getVal1x((char*)&pucAnsMsg[ IDX_CAN_DATA_MSG ]);
		usLowCurr		= getVal1x((char*)&pucAnsMsg[ IDX_CAN_DATA_MSG + 1]);
		usHighCurr		= getVal1x((char*)&pucAnsMsg[ IDX_CAN_DATA_MSG + 2]);
		ubRampFactor	= getVal1x((char*)&pucAnsMsg[ IDX_CAN_DATA_MSG + 3 ]);
		// store values
		m_motorMicrostepRes = ubMicrostepRes;
		m_motorLowCurrent = usLowCurr;
		m_motorHighCurrent = usHighCurr;
		m_motorRampFactor = ubRampFactor;

	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::enabledisableDecererationRamp-->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::getMotorActivation( uint8_t &ubActivation, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘V’ | ETX
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'V';

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘V’ | f  | status | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::getMotorActivation-->Performed %s",pucAnsMsg);
		ubActivation	= pucAnsMsg[ IDX_CAN_DATA_MSG ];
		// store values
		m_motorDisactivation = ubActivation - ZERO_CHAR;
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::GetMotorActivation-->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::setFrequency( enumSectionCommonMsgFreqSource eFreqSource, uint8_t ubFreqTable,
										uint16_t usFreq, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘F’ | freq_source | freq_table | F1 | F2 | F3 | F4 | ETX
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'F';
	pPayLoad[2] = eFreqSource;
	pPayLoad[3] = ubFreqTable;

	sprintf((char*)&pPayLoad[4], "%04X", usFreq);

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 8, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘F’ | f | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::setFrequency-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::setFrequency->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::setMotorCurrent( enumSectionCommonMsgCurrent eCurrent, uint8_t usValue, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘C’ | current | value | ETX
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'C';
	pPayLoad[2] = eCurrent;
	pPayLoad[3] = usValue;


	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 4, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘C’ |  f  | ETX
        m_pLogger->log(LOG_DEBUG,"SectionCommonMessage::setMotorCurrent-->Performed %s", pucAnsMsg);
		// store values
		if(eCurrent == eCoil)
		{
			if(usValue == '0')
				// current OFF
				m_motorDisactivation = 1;
			else
				// current ON
				m_motorDisactivation = 0;
		}
		// update motor current
		else if(eCurrent == eStill)
		{
			m_motorLowCurrent = usValue;
		}
		else if(eCurrent == eRun)
		{
			m_motorHighCurrent = usValue;
		}

	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::setMotorCurrent->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::setMotorConversionFactor( uint32_t ulMotorConvertionFactor, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘d’ | F1 | F2 | F3 | F4 | F5 | F6 | F7 | F8 | ETX
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'd';

	sprintf((char*)&pPayLoad[2], "%08X", ulMotorConvertionFactor);

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 10, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘d’ |  f  | ETX
        m_pLogger->log(LOG_DEBUG,"SectionCommonMessage::setMotorConvertionFactor-->Performed %s", pucAnsMsg);
		// store value
		m_motorFactor = ulMotorConvertionFactor;
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::setMotorConvertionFactor->ERROR");
		return -1;
	}
	return 0;
}

int SectionCommonMessage::getMotorConversionFactor( uint32_t &ulMotorConvertionFactor, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d | ‘g’ | ETX
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'g';

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘g’ | f  | A1 | A2 | A3 | A4 | A5 | A6 | A7 | A8 | ETX
		ulMotorConvertionFactor = (uint32_t)getVal8x((char *)&pucAnsMsg[ IDX_CAN_DATA_MSG ]);
		// store value
		m_motorFactor = ulMotorConvertionFactor;

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::getMotorConversionFactor-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::getMotorConversionFactor-->ERROR");
		return -1;
	}

	return 0;
}

int SectionCommonMessage::setDirectCalibration( uint8_t ubType, int16_t ssValue, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	//STX | d |‘U’ | type | sign | A1 | A2 | A3 | A4 | ETX
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'U';
	pPayLoad[2] = ubType;

	if ( ssValue < 0 )
	{
		pPayLoad[3] = '-';
		int16_t ssTmp = -1 * ssValue;
		sprintf((char*)&pPayLoad[4], "%04X", ssTmp);

	}
	else
	{
		pPayLoad[3] = '+';
		sprintf((char*)&pPayLoad[4], "%04X", ssValue);
	}

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 8, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘U’ | f | type| sign | A1 | A2 | A3 | A4 | ETX
		m_pLogger->log(LOG_INFO,"SectionCommonMessage::setDirectOffset-->Performed %s", pucAnsMsg);
		// store value
		m_calibrationPositions[ubType] = ssValue;
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::setDirectOffset-->ERROR");
		return -1;
	}


	return 0;
}

int SectionCommonMessage::saveCalibration( uint8_t ubPos, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d |‘O’ | type | ETX
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'O';
	pPayLoad[2] = ubPos;

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		// STX | o | ‘O’ | f | type| sign | A1 | A2 | A3 | A4 | ETX
		int32_t slVal = (int32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 2]);

		// Control the sign
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG + 1 ] == '-')
		{
			slVal *= -1LL;
		}
		else
		{
			slVal *= 1LL;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::setOffset-->Performed %s",pucAnsMsg);
		// store value
		m_calibrationPositions[ubPos] = slVal;
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::setOffset-->ERROR");
		return -1;
	}

	return 0;
}

int SectionCommonMessage::getCalibration( uint8_t ubType, int32_t &slOffset, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	// STX | d |‘o’ | type |  ETX
	pPayLoad[0] = m_ubIdSystem;
	pPayLoad[1] = 'o';
	pPayLoad[2] = ubType;

	int res = m_pCanBoardSystemBase->sendCanMsg( pPayLoad, 3, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘o’ | f | type | sign | A1 | A2 | A3 | A4 | ETX
		slOffset = (int32_t)getVal4x((char *)&pucAnsMsg[IDX_CAN_DATA_MSG + 2]);

		// Control the sign
		if ( pucAnsMsg[ IDX_CAN_DATA_MSG + 1 ] == '-')
		{
			slOffset *= -1LL;
		}
		else
		{
			slOffset *= 1LL;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionCommonMessage::getOffset-->Performed %s", pucAnsMsg);
		// store value
		if((ubType - ZERO_CHAR) < MAX_CALIBRATIONS_NUM)
		{
			m_calibrationPositions[(ubType - ZERO_CHAR)] = slOffset;
		}
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionCommonMessage::getOffset-->ERROR");
		return -1;
	}

	return 0;
}


int SectionCommonMessage::getAllCalibrations()
{
	int32_t offset;
	int res = 0;

	for(uint8_t i = 0; i < MAX_CALIBRATIONS_NUM; i++)
	{
		if((res = getCalibration(ZERO_CHAR + i, offset)) == -1)
		{
			break;
		}
	}
	return res;
}


int SectionCommonMessage::getAllMotorParameters()
{
	uint32_t factor;
	uint8_t ubMicrostepRes, ubRampFactor;
	uint16_t usLowCurr, usHighCurr;
	int res = 0;

	res = getMotorConversionFactor(factor);
	if(res == -1)
	{
		return res;
	}

	res = getMotorParameters(ubMicrostepRes, usLowCurr, usHighCurr, ubRampFactor);

	return res;
}

int32_t SectionCommonMessage::readCalibrationPosition(uint8_t index)
{
	return m_calibrationPositions[index];
}

uint32_t SectionCommonMessage::readMotorFactor()
{
	return m_motorFactor;
}

uint8_t SectionCommonMessage::readMotorMicrosteps()
{
	return m_motorMicrostepRes;
}

uint16_t SectionCommonMessage::readMotorLowCurrent()
{
	return m_motorLowCurrent;
}

uint16_t SectionCommonMessage::readMotorHighCurrent()
{
	return m_motorHighCurrent;
}

uint8_t SectionCommonMessage::readMotorRampFactor()
{
	return m_motorRampFactor;
}

uint8_t SectionCommonMessage::readMotorDisactivation()
{
	return m_motorDisactivation;
}

int32_t SectionCommonMessage::readMotorPosition()
{
	return m_motorPosition;
}



