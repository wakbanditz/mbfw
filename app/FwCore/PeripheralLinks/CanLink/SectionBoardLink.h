/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardLink.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionBoardLink class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONBOARDLINK_H
#define SECTIONBOARDLINK_H

#include <Mutex.h>

#include "CommonInclude.h"
#include "CanLinkCommonInclude.h"
#include "SectionInclude.h"
#include "CanLinkBoard.h"

typedef enum
{
	CAN_RX_WAIT_STX		= 0,
	CAN_RX_RECV_BODY	= 1,
	CAN_RX_RECV_ETX     = 2
} enumStateReceivedMsg;

typedef enum
{
	CAN_TX_START		= 0,
	CAN_TX_BODY			= 1
} enumStateTxMsg;

typedef struct
{
	uint8_t		msgDataBuff[DIM_SECTIONLINK_MSG_BUFFER_SIZE];
	uint16_t	idxDataBuff;
	char		time_msg[32];
	enumStateReceivedMsg	stateRxMsg;
} structRxMsgData;

typedef struct
{
	uint8_t		msgDataBuff[DIM_SECTIONLINK_MSG_BUFFER_SIZE];
	char		time_msg[32];
	enumStateTxMsg stateTxMachine;
} structTxMsgData;


class SectionBoardGeneral;
class SectionBoardTray;
class SectionBoardPump;
class SectionBoardTower;
class SectionBoardSPR;
class SectionBoardPIB;
class CanBoardBase;

/*!
 * @class	SectionBoardLink
 * @brief	The SectionBoardLink is organized as HW section architecture, it is inherited
 *			from CanLinkBoard and it defines the parsing of data packets read from the
 *			related message queue. That class contains the sub-systems of the section boards,
 *			so the received and parsed message are dispatched to the related systems in order
 *			of received protocol command.
 */
class SectionBoardLink : public CanLinkBoard
{

	public:

		/*! ***********************************************************************************************************
		 * @brief default constructor
		 * ***********************************************************************************************************
		 */
		SectionBoardLink();

		/*! ***********************************************************************************************************
		 * @brief destructor
		 * ***********************************************************************************************************
		 */
		virtual ~SectionBoardLink();

		/*!
		 * @brief	setIDs	Set the ID of the section Board to use.
		 * @param sectionId the index of the section as defined in SectionInclude.h
         * @param	ulIdSectionBoard section id in standard protocol
         * @param	ulIdMasterBoard masterBoard id in standard protocol
         * @param   ulIdSectionBoardBinProt section id in binary protocol
         * @param   ulIdMasterBoardBinProt masterBoard id in binary protocol
		 * @return 0 if success, -1 otherwise
		 */
		int setIDs(int sectionId, uint32_t ulIdSectionBoard, uint32_t ulIdMasterBoard ,
								  uint32_t ulIdSectionBoardBinProt, uint32_t ulIdMasterBoardBinProt);

		/*!
		 * @brief	getID		Returns the ID of the section board used
		 * @return	ID of the section Board, -1 if there are not section boards
		 */
		int getID( void );

        /*!
         * \brief setSectionBoardNumber Set the section number (m_ubNumberSection )of the current object
         * \param ubnumSection
         * \return 0 if success, -1 otherwise
         */
        int setSectionBoardNumber(uint8_t ubnumSection);

        /*!
         * \brief getSectionBoardNumber It  retrives the number of the section which it is in use
         * \return number of the sectionBoardLink in use
         */
        uint8_t getSectionBoardNumber(void);

		/*!
		 * @brief	initSectionParts	Initialization of the section board parts
		 * @return	0 success, -1 if there is an error
		 */
		int initSectionParts( void );

		/*!
		 * @brief destroySectionParts
		 * @return
		 */
		int destroySectionParts( void );

		/*!
		 * @brief	sendMsgProt1	The function split the message data in many data packets with 8 byte buffer lenght. It inserts STX at
		 *							the beginning of the frame and ETX character at the end of the data frame.
		 * @param	pmsgData
		 * @param	nlen
		 * @return	0 if success, -1 otherwise
		 */
		int sendMsg( uint8_t * pmsgData, uint16_t nlen );

		/*!
		 * @brief sendMsgBinProt The function sends the bin protocol message to the related message queue with the
		 *							id of the specific section board.
		 * @param pmsgData
         * @param pmsgAnsData
         * @param ubwaitAck
         * @param ubNumByteToSend
         * @param ustimeout
		 * @return  0 if success, -1 otherwise
		 */
        int sendMsgBinProt(uint8_t *pmsgData, uint8_t *pmsgAnsData, uint8_t ubwaitAck, uint16_t ubNumByteToSend, uint16_t ustimeout = 5000);

		/*!
		 * @brief	sendMsgAck		Formats the message ack and sends the buffer to the specified message queue.
		 * @return	0 is success, -1 otherwise
		 */
		int sendMsgAck( void );

		/*!
		 * @brief addCallbackEvent	It defines the callback function for the CanBoardBase derived objects be invoked in case of asynchronous events
		 * @param ptrFunction		pointer to the callback function
		 */
		void addCallbackEvent(int (*ptrFunction)(int section, int device, int event));

		/*!
		 * @brief sendSecRouteMsg	To send a direct SEC_ROUTE command to the section using the sendCanMsg function.
		 * @param strCommand		the command string
		 * @param usMillisecToWait	the command reply timeout in msecs
		 * @return strReply			the reply buffer string (empty if error)
		 */
		string sendSecRouteMsg(std::string strCommand, uint16_t usMillisecToWait);

		/*!
		 * @brief clearSectionsErrors	to reset the active error status flag in all subparts of the section
		 */
		void clearSectionsErrors();

		/*!
		 * @brief clearSectionPressureErrors	to reset the active Pressure errors in all subparts of the section
		 */
		void clearSectionPressureErrors();

		/*!
		 * @brief enablePressureAcquisition	wrapper function to the same function in PIB board
         * @param eState on-off status
         * @param usTimeoutMs timenout in msecs
         * @return 0 if OK, -1 in case of error
		 */
		int enablePressureAcquisition(enumSectionPIBStartStateAcq eState, uint16_t usTimeoutMs);

		/*!
		 * @brief enableProtocolPressureAcquisition	wrapper function to the same function in GENERAL board
         * @param eState on-off status
         * @param usTimeoutMs timenout in msecs
         * @return 0 if OK, -1 in case of error
         */
		int enableProtocolPressureAcquisition(enumSectionPIBStartStateAcq eState, uint16_t usTimeoutMs);

        /*!
         * @brief mutexCanLock() lock the Mutex for CAN bus
         */
        void mutexCanLock();

        /*!
         * @brief mutexCanUnlock() unlock the Mutex for CAN bus
         */
        void mutexCanUnlock();

        /*!
         * @brief setEnabled to set the enabled status of the section link
         * @param status the flag status
         */
        void setEnabled(bool status);

    protected:

        /*!
         * @brief	parseMsgFromLink	The function reads all the received canFrame and stores the included payloads in a data buffer structure.
         *								Then the buffer is sent to the dispatcher.
         * @param	pcanFrame
         * @return	0 if success, -1 otherwise
         */
        int parseMsgFromLink(t_canFrame *pcanFrame);

    private:

		/*!
		 * @brief	dispatchMsgToSectionPart  The function dispatch the parsed message towards the related section part
		 * @param	msgDataBuff
		 * @return	0 if success, -1 otherwise
		 */
		int dispatchMsgToSectionPart( uint8_t *	msgDataBuff );

		/*!
		 * @brief	dispatchMsgToSectionPart  The function dispatch the parsed message towards the related section part
		 * @param	device the first char of a command that identifies the device
		 * @return	the pointer to the corresponding CanBoardBase object:
		 *				m_pGeneral / m_pTray / m_pPump / m_pTower / m_pSPR /m_pPIB, 0 otherwise

		 */
		CanBoardBase * pointerToDevice(uint8_t device);

    private:

        bool                    m_bEnabled;

        uint32_t				m_ulIdSectionBoard;
        uint32_t				m_ulIdMasterBoard;
        uint32_t				m_ulIdSectionBoardBinProt;
        uint32_t				m_ulIdMasterBoardBinProt;
        structRxMsgData			m_dataPacketFromSection;
        structTxMsgData			m_dataToSend;
        uint8_t                 m_ubNumberSection;

        Mutex					m_MutexCan;	//to prevent operation to the same variables

    public:

        SectionBoardGeneral		*m_pGeneral;
        SectionBoardTray		*m_pTray;
        SectionBoardPump		*m_pPump;
        SectionBoardTower		*m_pTower;
        SectionBoardSPR			*m_pSPR;
        SectionBoardPIB			*m_pPIB;



};

#endif // SECTIONBOARDLINK_H
