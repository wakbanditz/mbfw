/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardTray.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionBoardTray class.
 @details

 ****************************************************************************
*/

#include "SectionBoardTray.h"
#include "MainExecutor.h"

SectionBoardTray::SectionBoardTray(int section) : CanBoardBase(section)
{
	m_strDevice.assign(SECTION_TRAY_STRING);
}

SectionBoardTray::~SectionBoardTray()
{
	/* Nothing to do yet */
}

int SectionBoardTray::addCommonMessage( void )
{
	if ( m_pLogger == NULL )
	{
		return -1;
	}
	else
	{
		m_CommonMessage.init( m_pLogger, this, SECTION_TRAY_CHAR );
	}

	return 0;
}

/*!***************************************************************************
 * COMMAND TRAY
 * ***************************************************************************/
int SectionBoardTray::getSensor( bool &bHomeTray, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘S’ | ETX */
	pPayLoad[0] = SECTION_TRAY_CHAR;
	pPayLoad[1] = 'S';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | 'S' | f | state | ETX */
		uint8_t ubSensorState = pucAnsMsg[IDX_CAN_DATA_MSG] - 0x30;
		ubSensorState = ubSensorState & CANSTD_SENSOR_MASK_HOME_TRAY;
		if ( ubSensorState == 0x00 )
		{
			bHomeTray = false;
		}
		else
		{
			bHomeTray = true;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardTray::getSensor-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardTray::getSensor-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardTray::getTemperature( float& fTemp, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | 't' | ETX */
	pPayLoad[0] = SECTION_TRAY_CHAR;
	pPayLoad[1] = 't';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | ‘t’ | f | T1.1 | T1.2 | T1.3 | T1.4 | T2.1 | T2.2 | T2.3 | T2.4 | ETX */
		//When o = ‘2’ or o=’3’ the T2.1 T2.2 T2.3 T2.4  values are  invalid,
		uint8_t	ucidx = IDX_CAN_DATA_MSG;
		uint16_t	usT1 = getVal4x( (char *)&pucAnsMsg[ucidx] );

		fTemp = (float)usT1 / 100;

		m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardTray::getTemperature--> t1=%d, temp=%f", usT1, fTemp);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardTray::getTemperature-->ERROR");
		return -1;
	}
	return 0;
}

int SectionBoardTray::setTemperature(uint16_t intTemp, uint16_t intLowTemp, uint16_t intHighTemp, uint16_t usTimeoutMs )
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘T’ | T1| T2| T3 | T4 | Rl1 | Rl2 | Rl3 | Rh1 | Rh2 |Rh3 | ETX */
	pPayLoad[0] = SECTION_TRAY_CHAR;
	pPayLoad[1] = 'T';

	// NOTE : the temperature values are already expressed as 0.01 degrees,
	// the low/high range are to be set as relative to target (while as parameters they are absolute values)
	uint16_t	usLowRange	= (intTemp - intLowTemp);
	uint16_t	usHighRange = (intHighTemp - intTemp);
	sprintf((char*)&pPayLoad[2], "%04X%03X%03X", intTemp, usLowRange, usHighRange);

	int res = sendCanMsg( pPayLoad, 12, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | o | ‘T’ | f | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardTray::setTemperature-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardTray::setTemperature-->ERROR");
		return -1;
	}

	return 0;
}


/*!***************************************************************************
 * PACKETS MANAGER                                                           *
 * ***************************************************************************/
int SectionBoardTray::spreadMsg( uint8_t * pMsg )
{
	if ( pMsg == NULL )
	{
		return -1;
	}
	else
	{
        switch ( pMsg[IDX_CAN_TYPE_MSG] )
        {
            case 'Z':
            {
                char      pcDescription[SCTB_MAX_DESCRIPTION];
                decodeNotifyEvent( (char *)pMsg, pcDescription );

                m_pLogger->log(LOG_INFO, "SectionBoardTray::spreadMsg-->%s", pcDescription);
            }
            break;

            default:
            {
                sendToMsgQueueAnsw( (char *)pMsg, strlen((char*)pMsg) );
                m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardTray::spreadMsg-->sendToMsgQueueAnsw %s", (char *)pMsg);
            }
            break;
        }

	}

	return 0;
}

int SectionBoardTray::decodeAsynchronousMsg( uint8_t *pMsg )
{
	m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardTray::decodeAsynchronousMsg-->%s", pMsg);
	return 0;
}

bool SectionBoardTray::decodeNotifyEvent(char *pubMsg, char *pcDescription)
{
	uint8_t	ubFlag1 = pubMsg[2];
	uint8_t	ubFlag2 = pubMsg[3];
	char strDescription[64];

	strDescription[0] = 0;
	if(pcDescription) *pcDescription = 0;

	// first test the default function that checks the errors defined in SECTION.err file
	if(CanBoardBase::decodeNotifyEvent(pubMsg, pcDescription))
	{
		return true;
	}

	// check other events from section
	switch ( ubFlag1 )
	{
		case 'I':	// Units: '5', '3'
		{
			switch ( ubFlag2 )
			{
				case '5':
				{
					sprintf(strDescription,"%s -> Target temperature reached", pubMsg );
                    if(clearTemperatureError())
                    {
                        // a temperature error has been cleared -> invoke SectionBoardManager
                        // to check if the status and leds are to be updated
                        checkErrorsOnSection(m_section);
                    }
                }
				break;
			}
		}
		break;

		//.not applicable
		default:
		break;
	}

	bool bRet = true;

	if (strDescription[0] == 0  )
	{
		sprintf(strDescription, "%s -> (TBD)", pubMsg);
		bRet = false;
	}

	if(pcDescription) strcpy(pcDescription, strDescription);

	m_pLogger->log(LOG_INFO, "SectionBoardTray::decodeNotifyEvent: %s", strDescription);

	return bRet;
}

