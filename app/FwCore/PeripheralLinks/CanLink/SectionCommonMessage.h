/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionCommonMessage.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionCommonMessage class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONCOMMONMESSAGE_H
#define SECTIONCOMMONMESSAGE_H

#include <stdint.h>

#include "CanBoardBase.h"

typedef enum
{
	eIncreasePosition	= '0',
	eDecreasePosition	= '1'

} enumSectionCommonMsgStepDir;

typedef enum
{
	eFullStep			= '0',
	eHalfStep			= '1'

} enumSectionCommonMsgStepType;

typedef enum
{
	eCurrentSet			= '0',
	eFull				= '1',
	e1Step2				= '2',
	e1Step4				= '3',
	e1Step8				= '4',
	e1Step16			= '5',
	e1Step32			= '6'

} enumSectionCommonMsgStepMode;

typedef enum
{
	eCurrentSlpe		= '0',
	eScanBy1			= '1',
	eScanBy2			= '2',
	eScanBy3			= '3',
	eScanBy4			= '4',
	eScanBy5			= '5',
	eScanBy6			= '6',
	eScanBy7			= '7',
	eScanBy8			= '8'

} enumSectionCommonMsgSlopeScan;

typedef enum
{
	eDisable			= '0',
	eEnable				= '1'

} enumSectionCommonMsgDecSlope;

typedef enum
{
	eFxFields			= '0',
	eFreqTable			= '1'

} enumSectionCommonMsgFreqSource;

typedef enum
{
	eCoil			= '0',
	eStill			= '1',
	eRun			= '2'

} enumSectionCommonMsgCurrent;

// maximum number of calibrations per each device as defined in Section fw
#define MAX_CALIBRATIONS_NUM	6

/*! **********************************************************************************************************
 * @class	SectionCommonMessage
 * @brief	The SectionCommonMessage class is used to manage common messages of the section parts 0,1,2,3.
 *			In particular, it is usefull for the motor controlof the related part. Every section part has to contain
 *			and init a SectionCommonMessage object with the related ID.
 *
 * ***********************************************************************************************************
 */
class SectionCommonMessage
{
	public:

		SectionCommonMessage();

		virtual ~SectionCommonMessage();

		int		init( Log *pLogger, CanBoardBase	*pCanBoardSystemBase, uint8_t ubIdSystem );

        int		searchHome( uint8_t errorDisable = 0, uint16_t usTimeoutMs = TH_MOVE_CMD_TIMEOUT );

		int     moveByRelativeStep( enumSectionCommonMsgStepDir eDir, enumSectionCommonMsgStepType eTypeStep,
									uint16_t usTimeoutMs );

		int		moveToRelativePosition( enumSectionCommonMsgStepDir      eDir,
										enumSectionCommonMsgStepType     eStepType,
										enumSectionCommonMsgStepMode     eStepMode,
										enumSectionCommonMsgSlopeScan    eSlopeScan,
										enumSectionCommonMsgDecSlope     eDecSlope,
										enumSectionCommonMsgFreqSource   eFreqSource,
										uint8_t					      ubFreqTable,
										uint16_t					  usFreq,
										uint16_t					  usStep,
										uint16_t					  usTimeoutMs);

		int		moveToAbsolutePosition( int16_t ssNumberStep, uint16_t usTimeoutMs );

		int		moveToCodedPosition( uint8_t ubCodedPos, uint16_t usTimeoutMs );

		int		setNumberRelativeSteps( uint16_t usNumberSteps, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		startMotor( enumSectionCommonMsgStepDir eDir, uint16_t usTimeoutMs );


		int		setMilcrostepResolution( enumSectionCommonMsgStepMode eStepMode,
										 uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		setRamp( enumSectionCommonMsgSlopeScan eSlopeScan,
										uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		setFrequency( enumSectionCommonMsgFreqSource eFreqSource, uint8_t ubFreqTable, uint16_t usFreq,
										uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		setMotorCurrent( enumSectionCommonMsgCurrent eCurrent, uint8_t usValue,
										uint16_t usTimeoutMs  = DEFAULT_REQUEST_DELAY);

		int		setMotorConversionFactor( uint32_t ulMotorConvertionFactor,
										  uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		saveCalibration(uint8_t ubPos, uint16_t usTimeoutMs  = DEFAULT_REQUEST_DELAY);

		int		setDirectCalibration(uint8_t ubType, int16_t ssValue, uint16_t usTimeoutMs  = DEFAULT_REQUEST_DELAY);

		int		setMicrostepResolution( enumSectionCommonMsgStepMode eStepMode,
										uint16_t usTimeoutMs  = DEFAULT_REQUEST_DELAY);

		int		getAbsolutePosition( int32_t &slVal, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		getHomeTolerance( int32_t &slVal, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		getMotorParameters(uint8_t &ubMicrostepRes, uint16_t &usLowCurr, uint16_t &usHighCurr,
								   uint8_t &ubRampFactor, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		getMotorConversionFactor( uint32_t &ulMotorConvertionFactor,
										  uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		getAllCalibrations();

		int		getCalibration( uint8_t ubType, int32_t &slOffset, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		getMotorActivation( uint8_t &ubActivation, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int		getAllMotorParameters();

		/*! *********************************************************************************************
		 * DATA GETTERS
		 * **********************************************************************************************
		 * **/

		int32_t		readCalibrationPosition(uint8_t index);

		uint32_t	readMotorFactor();

		uint8_t		readMotorMicrosteps();

		uint16_t	readMotorLowCurrent();

		uint16_t	readMotorHighCurrent();

		uint8_t		readMotorRampFactor();

		uint8_t		readMotorDisactivation();

		int32_t		readMotorPosition();

    private:
        CanBoardBase	*m_pCanBoardSystemBase;
        Log				*m_pLogger;

        uint8_t			m_ubIdSystem;

        // storage of data per each device
        int32_t			m_calibrationPositions[MAX_CALIBRATIONS_NUM];
        uint32_t		m_motorFactor;
        uint8_t			m_motorMicrostepRes;
        uint16_t		m_motorLowCurrent;
        uint16_t		m_motorHighCurrent;
        uint8_t			m_motorRampFactor;
        uint8_t			m_motorDisactivation;
        int32_t			m_motorPosition;

};

#endif // SECTIONCOMMONMESSAGE_H
