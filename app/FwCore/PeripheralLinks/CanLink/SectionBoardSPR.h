/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardSPR.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionBoardSPR class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONBOARDSPR_H
#define SECTIONBOARDSPR_H

#include "CanBoardBase.h"
#include "CanLinkCommonInclude.h"
#include "SectionCommonMessage.h"

/*!
 * @class SectionBoardSPR
 * @brief The SectionBoardSPR class
 */
class SectionBoardSPR : public CanBoardBase
{
	public:
		SectionBoardSPR(int section);
		virtual ~SectionBoardSPR();

		int addCommonMessage( void );

		/*!***************************************************************************
		 * COMMAND SPR																 *
		 * ***************************************************************************/

		int getSensor( bool &bHomeSPR, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int getTemperature( float& fTemp, uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );
		int setTemperature(uint16_t intTemp, uint16_t intLowTemp, uint16_t intHighTemp,
						   uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY);

		int resetSPRDriver( uint16_t usTimeoutMs = DEFAULT_REQUEST_DELAY );

		int setRegister( uint8_t ubReg, uint8_t ubVal1, uint8_t ubVal2, uint8_t ubVal3,
						 uint16_t usTimeoutMs  = DEFAULT_REQUEST_DELAY);
		int getRegister( uint8_t ubReg, uint8_t &ubVal1, uint8_t &ubVal2, uint8_t &ubVal3,
						 uint16_t usTimeoutMs  = DEFAULT_REQUEST_DELAY);

		/*!***************************************************************************
		 * PACKETS MANAGER                                                           *
		 * ***************************************************************************/
		/*!
		 * @brief spreadMsg
		 *		  The payload message is sent to the related message queue according to the received type message
		 * @param pMsg
		 * @return 0 if success, -1 otherwise
		 */
		int spreadMsg( uint8_t *pMsg );

	protected:
		int decodeAsynchronousMsg( uint8_t * pMsg );
		bool decodeNotifyEvent( char *pubMsg , char *pcDescription );

    public:
        SectionCommonMessage	m_CommonMessage;

};

#endif // SECTIONBOARDSPR_H
