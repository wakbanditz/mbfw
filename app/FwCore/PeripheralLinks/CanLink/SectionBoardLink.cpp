/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardLink.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionBoardLink class.
 @details

 ****************************************************************************
*/

#include "CanLink.h"
#include "CanLinkUpFw.h"
#include "FwUpgrade.h"

#define CAN_WAIT_TIME_SEND_MSG 1000

SectionBoardLink::SectionBoardLink()
{
	m_ulIdSectionBoard					= 0xFFFFFFFF;
	m_ulIdMasterBoard					= 0xFFFFFFFF;
	m_ulIdSectionBoardBinProt			= 0xFFFFFFFF;
	m_ulIdMasterBoardBinProt			= 0xFFFFFFFF;
	m_dataPacketFromSection.stateRxMsg	= CAN_RX_WAIT_STX;
	m_dataPacketFromSection.idxDataBuff = 0;
	m_dataToSend.stateTxMachine			= CAN_TX_START;
    m_ubNumberSection                   = 0;
    m_bEnabled  = false;

	m_pGeneral  = NULL;
	m_pTray     = NULL;
	m_pPump		= NULL;
	m_pTower	= NULL;
	m_pSPR		= NULL;
	m_pPIB		= NULL;
}

SectionBoardLink::~SectionBoardLink()
{
	m_ulIdSectionBoard = -1;
}

int SectionBoardLink::setIDs(int sectionId, uint32_t ulIdSectionBoard, uint32_t ulIdMasterBoard,
							 uint32_t ulIdSectionBoardBinProt, uint32_t ulIdMasterBoardBinProt )
{
	if ((( ulIdSectionBoard == CAN_SECTION_A_BOARD_ID_MASTER ) && ( ulIdMasterBoard == CAN_SECTION_A_BOARD_ID_TX )) ||
		(( ulIdSectionBoard == CAN_SECTION_B_BOARD_ID_MASTER ) && ( ulIdMasterBoard == CAN_SECTION_B_BOARD_ID_TX )))
	{
		m_ulIdSectionBoard = ulIdSectionBoard;
		m_ulIdMasterBoard = ulIdMasterBoard;
		m_pLogger->log(LOG_INFO, "SectionBoardLink::setID--> SetionID:[0x%x] MasterID:[0x%x] set", m_ulIdSectionBoard,
					   m_ulIdMasterBoard);
	}
	else
	{
		m_pLogger->log(LOG_ERR, "SectionBoardLink::setID--> ID:[%i] not set", m_ulIdSectionBoard );
		return -1;
	}


	if ((( ulIdSectionBoardBinProt == CAN_SECTION_A_BOARD_ID_UPFW ) && ( ulIdMasterBoardBinProt == CAN_MASTER_TO_SECTION_A_UPFW_TX )) ||
		(( ulIdSectionBoardBinProt == CAN_SECTION_B_BOARD_ID_UPFW ) && ( ulIdMasterBoardBinProt == CAN_MASTER_TO_SECTION_B_UPFW_TX )))
	{
		m_ulIdSectionBoardBinProt = ulIdSectionBoardBinProt;
		m_ulIdMasterBoardBinProt = ulIdMasterBoardBinProt;
		m_pLogger->log(LOG_INFO, "SectionBoardLink::setID--> SetionID:[0x%x] MasterID:[0x%x] set", m_ulIdSectionBoardBinProt,
					   m_ulIdMasterBoardBinProt);
	}
	else
	{
		m_pLogger->log(LOG_ERR, "SectionBoardLink::setID--> ID:[%i] not set", m_ulIdSectionBoardBinProt );
		return -1;
	}

	m_sectionId = sectionId;
	return 0;
}

int SectionBoardLink::getID( void )
{
	return m_ulIdSectionBoard;
}

int SectionBoardLink::setSectionBoardNumber(uint8_t ubnumSection)
{
    if (ubnumSection >= SCT_NONE_ID)
    {
        m_pLogger->log(LOG_ERR,"SectionBoardLink::setSectionBoardNumber--> %x not valid",ubnumSection);
        return -1;
    }

    m_ubNumberSection = ubnumSection;

    return 0;
}

uint8_t SectionBoardLink::getSectionBoardNumber()
{
    return m_ubNumberSection;
}

int SectionBoardLink::initSectionParts( void )
{
	/*!**************************************
	 * Add the systems to the section board *
	 ****************************************/
	m_pGeneral	= new SectionBoardGeneral(m_sectionId);
	m_pTray		= new SectionBoardTray(m_sectionId);
	m_pPump		= new SectionBoardPump(m_sectionId);
	m_pTower	= new SectionBoardTower(m_sectionId);
	m_pSPR		= new SectionBoardSPR(m_sectionId);
	m_pPIB		= new SectionBoardPIB(m_sectionId);

	/* Init and start related thread */
	switch ( m_sectionId )
	{
		case SCT_A_ID:
		{
			/* Set a message queues for all the parts of the section */
			int iRes = m_pGeneral->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_A_GENERAL_ANSW,
										 SCT_MQ_CAN_LINK_NAME_A_GENERAL_AUTOANSW, this );
			if ( iRes < 0 )
			{
				//Error
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of MQs board ID[0x%x] failed",
							   CAN_SECTION_A_BOARD_ID_MASTER);
				return -1;
			}

			iRes = m_pTray->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_A_TRAY_ANSW, NULL, this );
			if ( iRes < 0 )
			{
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of Tray MQ board ID[0x%x] failed",
							   SCT_MQ_CAN_LINK_NAME_A_TRAY_ANSW);
				return -1;
			}

			iRes = m_pPump->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_A_PUMP_ANSW, NULL, this );
			if ( iRes < 0 )
			{
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of Pump MQ board ID[0x%x] failed",
							   SCT_MQ_CAN_LINK_NAME_A_PUMP_ANSW);
				return -1;
			}

			iRes = m_pTower->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_A_TOWER_ANSW, NULL, this );
			if ( iRes < 0 )
			{
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of Tower MQ board ID[0x%x] failed",
							   SCT_MQ_CAN_LINK_NAME_A_TOWER_ANSW);
				return -1;
			}

			iRes = m_pSPR->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_A_SPR_ANSW, NULL, this );
			if ( iRes < 0 )
			{
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of SPR MQ board ID[0x%x] failed",
							   SCT_MQ_CAN_LINK_NAME_A_SPR_ANSW);
				return -1;
			}

			iRes = m_pPIB->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_A_PIB_ANSW, NULL, this );
			if ( iRes < 0 )
			{
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of PIB MQ board ID[0x%x] failed",
							   SCT_MQ_CAN_LINK_NAME_A_PIB_ANSW);
				return -1;
			}

		}
		break;

		case SCT_B_ID:
		{
			/* Set a message queues for all the parts of the section */
			int iRes = m_pGeneral->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_B_GENERAL_ANSW,
										 SCT_MQ_CAN_LINK_NAME_B_GENERAL_AUTOANSW, this );
			if ( iRes < 0 )
			{
				//Error
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of MQs board ID[0x%x] failed",
							   CAN_SECTION_B_BOARD_ID_MASTER);
				return -1;
			}

			iRes = m_pTray->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_B_TRAY_ANSW, NULL, this );
			if ( iRes < 0 )
			{
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of Tray MQ board ID[0x%x] failed",
							   SCT_MQ_CAN_LINK_NAME_B_TRAY_ANSW);
				return -1;
			}

			iRes = m_pPump->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_B_PUMP_ANSW, NULL, this );
			if ( iRes < 0 )
			{
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of Pump MQ board ID[0x%x] failed",
							   SCT_MQ_CAN_LINK_NAME_B_PUMP_ANSW);
				return -1;
			}

			iRes = m_pTower->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_B_TOWER_ANSW, NULL, this );
			if ( iRes < 0 )
			{
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of Tower MQ board ID[0x%x] failed",
							   SCT_MQ_CAN_LINK_NAME_B_TOWER_ANSW);
				return -1;
			}

			iRes = m_pSPR->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_B_SPR_ANSW, NULL, this );
			if ( iRes < 0 )
			{
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of SPR MQ board ID[0x%x] failed",
							   SCT_MQ_CAN_LINK_NAME_B_SPR_ANSW);
				return -1;
			}

			iRes = m_pPIB->init( m_pLogger, SCT_MQ_CAN_LINK_NAME_B_PIB_ANSW, NULL, this );
			if ( iRes < 0 )
			{
				m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> init of PIB MQ board ID[0x%x] failed",
							   SCT_MQ_CAN_LINK_NAME_B_PIB_ANSW);
				return -1;
			}

		}
		break;

		default:
			m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> no section parts initialized");
			return -1;
		break;
	}

	/* Add object which includes common message */
	m_pTray->addCommonMessage();
	m_pPump->addCommonMessage();
	m_pTower->addCommonMessage();
	m_pSPR->addCommonMessage();

	/* Start the thread related to decode Asynchronous message */
	if ( !m_pGeneral->startThread() )
	{
		m_pLogger->log(LOG_ERR, "SectionBoardLink::initSectionParts--> Asynchronous GENERAL thread not started");
		return -1;
	}

	return 0;
}

int SectionBoardLink::destroySectionParts( void )
{
	/*!**************************************************
	 * Destroy General Section Part
	 ****************************************************/
	if(m_pGeneral)
	{
		m_pGeneral->stopAsynchronousMsgReceiveThread();
		m_pGeneral->closeAndUnlinkMsgQueue();
	}
	//The other parts of section board don't need to close the message queue and
	//stop the execution of the thread yet
	if(m_pTray)		m_pTray->closeAndUnlinkMsgQueue();
	if(m_pPump)		m_pPump->closeAndUnlinkMsgQueue();
	if(m_pTower)	m_pTower->closeAndUnlinkMsgQueue();
	if(m_pSPR)		m_pSPR->closeAndUnlinkMsgQueue();
	if(m_pPIB)		m_pPIB->closeAndUnlinkMsgQueue();

	SAFE_DELETE(m_pGeneral);
	SAFE_DELETE(m_pTray);
	SAFE_DELETE(m_pPump);
	SAFE_DELETE(m_pTower);
	SAFE_DELETE(m_pSPR);
	SAFE_DELETE(m_pPIB);

	return 0;
}

void SectionBoardLink::addCallbackEvent(int (*ptrFunction)(int, int, int))
{
	if(m_pGeneral)	m_pGeneral->addCallbackEvent(ptrFunction);
	if(m_pTray)		m_pTray->addCallbackEvent(ptrFunction);
	if(m_pPump)		m_pPump->addCallbackEvent(ptrFunction);
	if(m_pTower)	m_pTower->addCallbackEvent(ptrFunction);
	if(m_pSPR)		m_pSPR->addCallbackEvent(ptrFunction);
	if(m_pPIB)		m_pPIB->addCallbackEvent(ptrFunction);
}

std::string SectionBoardLink::sendSecRouteMsg(std::string strCommand, uint16_t usMillisecToWait)
{
	std::string strReply;
	strReply.clear();

	// the command is sent from "outside" (e.g. a SEC_ROUTE command) so we decode the involved object
	uint8_t sectionReply[512];
	uint8_t sectionCommand[1024];

	memset(sectionReply, 0, sizeof(sectionReply));

	// convert from std::string to unsigned char
	for(unsigned int i = 0; i < strCommand.length(); i++)
	{
		sectionCommand[i] = strCommand.at(i);
	}

	CanBoardBase * pCanBoardBase = pointerToDevice(sectionCommand[0]);
	if(pCanBoardBase == 0) return strReply;

	// send the command to the section without checking the error code in reply (if present)
	pCanBoardBase->sendCanMsg(sectionCommand, strCommand.length(), sectionReply, usMillisecToWait, true);

	// convert from unsigned char to std::String
	for(unsigned int i = 0; ; i++)
	{
		if(sectionReply[i] == 0) break;
		strReply.push_back(sectionReply[i]);
	}
	m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardLink::sendSecRouteMsg --> reply %s", strReply.c_str());

	return strReply;
}

int SectionBoardLink::parseMsgFromLink( t_canFrame *pcanFrame)
{
    uint8_t	ubLenPayLoad = 0;
    uint8_t ubIdx = 0;
    bool bAckSent = false;

    // if the section is disabled -> no action (spurious messages ?)
    if(m_bEnabled == false)
    {
        return 0;
    }

    /* check if the if packet corresponds with the configured section */
    if ( pcanFrame->Frame.can_id == m_ulIdSectionBoard )
    {
        ubLenPayLoad = pcanFrame->Frame.can_dlc;
        m_pLogger->log(LOG_DEBUG_PARANOIC, "CanLinkBoard::parseMsgFromLink-->%s", pcanFrame->Frame.data);

        while ( ubIdx < ubLenPayLoad )
        {
            switch ( m_dataPacketFromSection.stateRxMsg )
            {
                case CAN_RX_WAIT_STX:
                {
                    if ( pcanFrame->Frame.data[ ubIdx ] == STX )
                    {
                        m_dataPacketFromSection.stateRxMsg = CAN_RX_RECV_BODY;
                    }
                }
                break;

                case CAN_RX_RECV_BODY:
                {
                    if (pcanFrame->Frame.data[ ubIdx ] == STX)
                    {
                        /* Reset of state machine */
                        m_dataPacketFromSection.stateRxMsg = CAN_RX_WAIT_STX;
                        m_dataPacketFromSection.idxDataBuff = 0;
                        memset(m_dataPacketFromSection.msgDataBuff, 0, sizeof(m_dataPacketFromSection.msgDataBuff));
                    }
                    else if ( pcanFrame->Frame.data[ ubIdx ] != ETX )
                    {
                        m_dataPacketFromSection.msgDataBuff[m_dataPacketFromSection.idxDataBuff] = pcanFrame->Frame.data[ubIdx];
                        m_dataPacketFromSection.idxDataBuff++;
                    }
                    else
                    {
                        /* ETX character received */
                        m_dataPacketFromSection.msgDataBuff[m_dataPacketFromSection.idxDataBuff] = '\0';
                        memcpy(m_dataPacketFromSection.time_msg, pcanFrame->time_msg, sizeof(pcanFrame->time_msg));

                        // check if the reply is 'I' -> message not correct or unknown
                        if((m_dataPacketFromSection.idxDataBuff == 1) &&
                           (m_dataPacketFromSection.msgDataBuff[0] == 'I') )
                        {
                            m_pLogger->log(LOG_ERR, "SectionBoardLink::parseMsgFromLink--> I reply");
                            // no need to dispatch the message (but send Ack)
                            sendMsgAck();
                            bAckSent = true;
                            // notify all devices (we don't know which is the running one)
                            if(m_pGeneral) m_pGeneral->registerUnknownReplyFromSection();
                            if(m_pTray) m_pTray->registerUnknownReplyFromSection();
                            if(m_pPump) m_pPump->registerUnknownReplyFromSection();
                            if(m_pTower) m_pTower->registerUnknownReplyFromSection();
                            if(m_pSPR) m_pSPR->registerUnknownReplyFromSection();
                            if(m_pPIB) m_pPIB->registerUnknownReplyFromSection();
                        }
                        else if(bAckSent == false)
                        {
                            // before dispatching the message send Ack to Section to close the communication
                            sendMsgAck();
                            bAckSent = true;

                            /* Send received data to section */
                            dispatchMsgToSectionPart( m_dataPacketFromSection.msgDataBuff );
                        }

                        /* Reset of state machine */
                        m_dataPacketFromSection.stateRxMsg = CAN_RX_WAIT_STX;
                        m_dataPacketFromSection.idxDataBuff = 0;
                        memset(m_dataPacketFromSection.msgDataBuff, 0, sizeof(m_dataPacketFromSection.msgDataBuff));
                    }
                }
                break;

                default:
                    //Error
                    m_pLogger->log(LOG_ERR, "SectionBoardLink::parseMsgFromLink--> default state machine");
                break;
            }

            /* control the maximum idxdatabuff of msgDataBuff: if it excedes the maximum length retrieves with an error */
            if ( m_dataPacketFromSection.idxDataBuff >= sizeof(m_dataPacketFromSection.msgDataBuff) )
            {
                //Error
                m_pLogger->log(LOG_ERR, "SectionBoardLink::parseMsgFromLink-->Exceeded maximum data length");
                return -1;
            }

            ubIdx++;
        }
    }
    else
    {
        //Error
        m_pLogger->log(LOG_ERR, "SectionBoardLink::parseMsgFromLink --> Received ID not correct ");
        return -1;
    }

    /* send ack every received packet */
    if(bAckSent == false)
    {
        sendMsgAck();
    }
    return 0;
}

int SectionBoardLink::dispatchMsgToSectionPart( uint8_t *msgDataBuff )
{
//	m_pLogger->log(LOG_ERR, "CanLinkBoard::dispatchMsgToSectionPart-->%s",msgDataBuff);

	CanBoardBase * pCanBoardBase = pointerToDevice(msgDataBuff[IDX_CAN_ORIGIN]);
	if(pCanBoardBase == 0) return -1;

	pCanBoardBase->spreadMsg(msgDataBuff);

	return 0;
}

int SectionBoardLink::sendMsgAck( void )
{
	int res;
	uint8_t buf[8];
	uint8_t numByteToSend = 3;

	buf[0] = STX;
	buf[1] = ACK_CAN; // ~ 0x7E
	buf[2] = ETX;
	buf[3] = '\0';

    mutexCanLock();

	res = sendDataFrame(m_ulIdMasterBoard, buf, numByteToSend);
	if ( res < 0 )
	{
		//Error
        m_pLogger->log(LOG_ERR, "SectionBoardLink::sendMsgAck--> ACK not sent");
        res = -1;
	}
    else
    {
        m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardLink::sendMsgAck");
        res = 0;
    }

    mutexCanUnlock();
    return res;
}

int SectionBoardLink::sendMsg(uint8_t *pmsgData, uint16_t nlen)
{
	uint16_t cntByteSentTot = 0;
	uint16_t cntNumByteToSend = 0;
	uint16_t cntNumByteToSendTot = 0;
	uint16_t numByteToSend = 0;
	uint16_t idxData = 0;
	uint16_t usIdxDataInput = 0;

//	uint8_t	ubBuffTmp[DIM_SECTIONLINK_MSG_BUFFER_SIZE];
//	memset(ubBuffTmp, 0, sizeof(ubBuffTmp));
//	memcpy(ubBuffTmp, pmsgData, nlen);


	memset(m_dataToSend.msgDataBuff, 0, sizeof(m_dataToSend.msgDataBuff));

	if (( nlen == 0 ) || ( pmsgData == NULL ))
	{
		//Error
		m_pLogger->log(LOG_ERR, "SectionBoardLink::sendMsgProt1--> no data to send");
		return -1;
	}

    m_MutexCan.lock();

    /* Max size to send is nlen byte of msgData + STX and ETX */
	cntNumByteToSend = nlen + 2;
	cntNumByteToSendTot = cntNumByteToSend;

	while ( cntByteSentTot < cntNumByteToSendTot )
	{
		idxData = 0;

		switch( m_dataToSend.stateTxMachine )
		{
			case CAN_TX_START:
			{
                // first char = STX
                m_dataToSend.msgDataBuff[ idxData ] = STX;
                idxData++;

                if (cntNumByteToSend <= MAX_LEN_PAYLOAD_PACK)
				{
					memcpy(&m_dataToSend.msgDataBuff[idxData], pmsgData, nlen);
					idxData = idxData + nlen;
					m_dataToSend.msgDataBuff[ idxData ] = ETX;

					cntByteSentTot = idxData + 2;//
					numByteToSend = cntNumByteToSend;
				}
				else
				{
                    memcpy(&m_dataToSend.msgDataBuff[idxData], pmsgData, (MAX_LEN_PAYLOAD_PACK - 1));

                    usIdxDataInput = usIdxDataInput + (MAX_LEN_PAYLOAD_PACK - 1);

					cntByteSentTot = MAX_LEN_PAYLOAD_PACK;
					cntNumByteToSend = 	cntNumByteToSend - cntByteSentTot;

					m_dataToSend.stateTxMachine = CAN_TX_BODY;
					numByteToSend = cntByteSentTot;
				}

				m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardLink::sendMsgProt1--> NumByte=%d, %s", numByteToSend, m_dataToSend.msgDataBuff);
				int res = sendDataFrame(m_ulIdMasterBoard, m_dataToSend.msgDataBuff, numByteToSend);
				if ( res < 0 )
				{
					//Error
					m_pLogger->log(LOG_ERR, "SectionBoardLink::sendMsgProt1--> data not send");
                    m_MutexCan.unlock();
                    return -1;
				}
			}
			break;

			case CAN_TX_BODY:
			{
				if (cntNumByteToSend <= MAX_LEN_PAYLOAD_PACK)
				{
					int numByteMsgToSend = cntNumByteToSend-1;
					if (numByteMsgToSend > 0)
					{
						//Only there are data message to send
						for (int i = 0; i < numByteMsgToSend; i++)
						{
							m_dataToSend.msgDataBuff[ i ] = pmsgData[usIdxDataInput + i];
						}
						idxData = idxData + numByteMsgToSend;
					}
					m_dataToSend.msgDataBuff[ idxData ] = ETX;

					cntByteSentTot = cntByteSentTot + cntNumByteToSend;
					numByteToSend = cntNumByteToSend;
					cntNumByteToSend = 0;

					m_dataToSend.stateTxMachine = CAN_TX_START;
				}
				else
				{
					memcpy(&m_dataToSend.msgDataBuff[ idxData ], &pmsgData[usIdxDataInput], MAX_LEN_PAYLOAD_PACK);
					idxData = idxData + nlen;
					usIdxDataInput = usIdxDataInput + MAX_LEN_PAYLOAD_PACK;

					cntNumByteToSend = cntNumByteToSend - MAX_LEN_PAYLOAD_PACK;
					cntByteSentTot = cntByteSentTot + MAX_LEN_PAYLOAD_PACK;
					numByteToSend = MAX_LEN_PAYLOAD_PACK;
				}

				m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardLink::sendMsgProt1--> NumByte=%d, %s", numByteToSend, m_dataToSend.msgDataBuff);
				int res = sendDataFrame(m_ulIdMasterBoard, m_dataToSend.msgDataBuff, numByteToSend);
				if ( res < 0 )
				{
					//Error
                    m_pLogger->log(LOG_ERR, "SectionBoardLink::sendMsgProt1--> data not sent");
                    m_MutexCan.unlock();
                    return -1;
				}
            }
			break;

			default:
				//Error
				m_pLogger->log(LOG_ERR, "SectionBoardLink::sendMsgProt1--> state unknown");
			break;
		}

//        usleep(CAN_WAIT_TIME_SEND_MSG);//RBB
	}
    m_MutexCan.unlock();
    return 0;
}


int SectionBoardLink::sendMsgBinProt(uint8_t *pmsgData, uint8_t *pmsgAnsData, uint8_t ubwaitAck, uint16_t ubNumByteToSend, uint16_t ustimeout)
{
	uint16_t numByteToSend = 0;
	uint8_t	ubBuffTmp[DIM_SECTIONLINK_MSG_BUFFER_SIZE];

	if ( pmsgData == NULL )
	{
		//Error
		m_pLogger->log(LOG_ERR, "SectionBoardLink::sendMsgBinProt--> no data to send");
		return -1;
	}

	numByteToSend = ubNumByteToSend;
	memset(ubBuffTmp, 0, sizeof(ubBuffTmp));
	memcpy(ubBuffTmp, pmsgData, numByteToSend);

	/* Max size to send is nlen byte of msgData + STX and ETX */
	int res = sendUpFwMsg(m_ulIdMasterBoardBinProt, ubBuffTmp, numByteToSend);
	if ( res < 0 )
	{
		//Error
		m_pLogger->log(LOG_ERR, "SectionBoardLink::sendMsgBinProt-->Data not send");
		return -1;
	}
	else
	{
		m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardLink::sendMsgBinProt--> NumByte=%d, %s", numByteToSend, pmsgData);
	}

	if (ubwaitAck == ID_ACKWAIT)
	{
		uint16_t	usLenReceived;
		uint8_t	ubBuffReceivedTmp[DIM_SECTIONLINK_MSG_BUFFER_SIZE];
		res = receiveUpFwBinMsg(ubBuffReceivedTmp, usLenReceived, ustimeout);
		if (res != 0)
		{
			m_pLogger->log(LOG_ERR, "-->Data not received");
			return -1;
		}
		else
		{		
			memcpy(pmsgAnsData, ubBuffReceivedTmp, usLenReceived);
		}
	}
	return 0;
}

CanBoardBase * SectionBoardLink::pointerToDevice(uint8_t device)
{
	CanBoardBase * pCanBoardBase = 0;

	switch(device)
	{
		case SECTION_PUMP_CHAR :
			if(m_pPump)
			{
				pCanBoardBase = m_pPump;
			}
		break;
		case SECTION_TOWER_CHAR :
			if(m_pTower)
			{
				pCanBoardBase = m_pTower;
			}
		break;
		case SECTION_TRAY_CHAR :
			if(m_pTray)
			{
				pCanBoardBase = m_pTray;
			}
		break;
		case SECTION_SPR_CHAR :
			if(m_pSPR)
			{
				pCanBoardBase = m_pSPR;
			}
		break;
		case SECTION_PIB_CHAR :
			if(m_pPIB)
			{
				pCanBoardBase = m_pPIB;
			}
		break;
		case SECTION_GENERAL_CHAR :
			if(m_pGeneral)
			{
				pCanBoardBase = m_pGeneral;
			}
		break;
	}
	return pCanBoardBase;
}

void SectionBoardLink::clearSectionsErrors()
{
	if(m_pPump)
	{
		m_pPump->clearAllErrors();
	}
	if(m_pTower)
	{
		m_pTower->clearAllErrors();
	}
	if(m_pTray)
	{
		m_pTray->clearAllErrors();
	}
	if(m_pSPR)
	{
		m_pSPR->clearAllErrors();
	}
	if(m_pPIB)
	{
		m_pPIB->clearAllErrors();
	}
	if(m_pGeneral)
	{
		m_pGeneral->clearAllErrors();
	}
}

void SectionBoardLink::clearSectionPressureErrors()
{
	if(m_pPump)
	{
		m_pPump->removePressureErrors();
	}
	if(m_pTower)
	{
		m_pTower->removePressureErrors();
	}
	if(m_pTray)
	{
		m_pTray->removePressureErrors();
	}
	if(m_pSPR)
	{
		m_pSPR->removePressureErrors();
	}
	if(m_pPIB)
	{
		m_pPIB->removePressureErrors();
	}
	if(m_pGeneral)
	{
		m_pGeneral->removePressureErrors();
	}
}

int SectionBoardLink::enablePressureAcquisition(enumSectionPIBStartStateAcq eState, uint16_t usTimeoutMs)
{
	if(m_pPIB)
	{
		return m_pPIB->enablePressureAcquisition(eState, usTimeoutMs);
	}
	return -1;
}

int SectionBoardLink::enableProtocolPressureAcquisition(enumSectionPIBStartStateAcq eState, uint16_t usTimeoutMs)
{
	if(m_pGeneral)
	{
		return m_pGeneral->enablePressureAcquisition(eState, usTimeoutMs);
	}
    return -1;
}

void SectionBoardLink::mutexCanLock()
{
    m_MutexCan.lock();
}

void SectionBoardLink::mutexCanUnlock()
{
    m_MutexCan.unlock();
}

void SectionBoardLink::setEnabled(bool status)
{
    m_bEnabled = status;
}
