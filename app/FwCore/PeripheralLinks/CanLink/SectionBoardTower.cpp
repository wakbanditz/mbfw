/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionBoardTower.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionBoardTower class.
 @details

 ****************************************************************************
*/

#include "SectionBoardTower.h"

SectionBoardTower::SectionBoardTower(int section) : CanBoardBase(section)
{
	m_strDevice.assign(SECTION_TOWER_STRING);
}

SectionBoardTower::~SectionBoardTower()
{

}

int SectionBoardTower::addCommonMessage()
{
	if ( m_pLogger == NULL )
	{
		return -1;
	}
	else
	{
		m_CommonMessage.init( m_pLogger, this, SECTION_TOWER_CHAR );
	}

	return 0;
}

/*!***************************************************************************
 * COMMAND TOWER
 * ***************************************************************************/
int SectionBoardTower::getSensor(bool &bHomeTower, uint16_t usTimeoutMs)
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	/* STX | d | ‘S’ | ETX */
	pPayLoad[0] = SECTION_TOWER_CHAR;
	pPayLoad[1] = 'S';

	int res = sendCanMsg( pPayLoad, 2, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		/* STX | o | 'S' | f | state | ETX */
		uint8_t ubSensorState = pucAnsMsg[IDX_CAN_DATA_MSG] - 0x30;
		ubSensorState = ubSensorState & CANSTD_SENSOR_MASK_HOME_TOWER;
		if ( ubSensorState == 0x00 )
		{
			bHomeTower = false;
		}
		else
		{
			bHomeTower = true;
		}

		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardTower::getSensor-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardTower::getSensor-->ERROR");
		return -1;
	}

	return 0;
}

int SectionBoardTower::setWellBottom(uint32_t ulNumStep, uint16_t usTimeoutMs)
{
	uint8_t	pucAnsMsg[MQ_CANBOARD_MAX_MSG_SIZE];
	uint8_t pPayLoad[MQ_CANBOARD_MAX_MSG_SIZE];

	memset(pPayLoad, 0, sizeof(pPayLoad));
	memset(pucAnsMsg, 0, sizeof(pucAnsMsg));

	//STX | d | ‘B’ | B1 | B2 | B3 | B4 | ETX
	pPayLoad[0] = SECTION_TOWER_CHAR;
	pPayLoad[1] = 'B';

	sprintf((char*)&pPayLoad[2], "%04X", ulNumStep);

	int res = sendCanMsg( pPayLoad, 6, pucAnsMsg, usTimeoutMs );
	if ( res == 0 )
	{
		//STX | d | ‘B’ | f | ETX
		m_pLogger->log(LOG_DEBUG_PARANOIC,"SectionBoardTower::setWellBottom-->Performed %s", pucAnsMsg);
	}
	else
	{
		m_pLogger->log(LOG_ERR,"SectionBoardTower::setWellBottom-->ERROR");
		return -1;
	}

	return 0;
}

/*!***************************************************************************
 * PACKETS MANAGER                                                           *
 * ***************************************************************************/
int SectionBoardTower::spreadMsg(uint8_t *pMsg)
{
	if ( pMsg == NULL )
	{
		return -1;
	}
	else
	{
        switch ( pMsg[IDX_CAN_TYPE_MSG] )
        {
            case 'Z':
            {
                char      pcDescription[SCTB_MAX_DESCRIPTION];
                decodeNotifyEvent( (char *)pMsg, pcDescription );

                m_pLogger->log(LOG_INFO, "SectionBoardTower::spreadMsg-->%s", pcDescription);
            }
            break;

            default:
            {
                sendToMsgQueueAnsw( (char *)pMsg, strlen((char*)pMsg) );
                m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardTower::spreadMsg-->sendToMsgQueueAnsw %s", (char *)pMsg);
            }
            break;
        }
    }

	return 0;
}

int SectionBoardTower::decodeAsynchronousMsg(uint8_t *pMsg)
{
	m_pLogger->log(LOG_DEBUG_PARANOIC, "SectionBoardTower::decodeAsynchronousMsg-->%s", pMsg);
	return 0;
}

bool SectionBoardTower::decodeNotifyEvent(char *pubMsg, char *pcDescription)
{
	uint8_t	ubFlag1 = pubMsg[2];
//	uint8_t	ubFlag2 = pubMsg[3];
	char strDescription[64];

	strDescription[0] = 0;
	if(pcDescription) *pcDescription = 0;

	// first test the default function that checks the errors defined in SECTION.err file
	if(CanBoardBase::decodeNotifyEvent(pubMsg, pcDescription))
	{
		return true;
	}

	// check other events from section
	switch ( ubFlag1 )
	{
		//.not applicable
		default:
		break;
	}

	bool bRet = true;

	if (strDescription[0] == 0  )
	{
		sprintf(strDescription, "%s -> (TBD)", pubMsg);
		bRet = false;
	}

	if(pcDescription) strcpy(pcDescription, strDescription);

	m_pLogger->log(LOG_INFO, "SectionBoardPIB::decodeNotifyEvent: %s", strDescription);

	return bRet;
}

