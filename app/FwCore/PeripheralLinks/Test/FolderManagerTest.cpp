#include <iostream>

#include "FolderManagerTest.h"
#include "CR8062Include.h"

#define ESC		27


int testFolderManager(void)
{
	FolderManager fileManager;

	vector<string> vsOut;
	string sDefPath("/home/root/");
	string sFoo("");
	string sType;
	int liRes;
	char sCmd[4];

	printf("\n--< FOLDER MANAGER CMD LIST >--\n\n");
	printf("a - Create directory\n");
	printf("b - Delete file or empty directory\n");
	printf("c - Delete directory and everything inside it\n");
	printf("d - Get files in directory\n");
	printf("e - Get files in directory with filter extension\n");
	printf("f - Read file\n\n");

	scanf("%s", sCmd);

	switch (sCmd[0])
	{
		case 'a':
			printf("Please insert the path /home/root/");
			sFoo.clear();
			cin >> sFoo;
			sFoo = sDefPath + sFoo;
			liRes = fileManager.makeDir(sFoo);
			if ( ! liRes )
			{
				printf("Folder %s created with success.\n", sFoo.c_str());
				break;
			}
			printf("Unable to create %s.\n", sFoo.c_str());
		break;

		case 'b':
			printf("Please insert the abs file name /home/root/");
			sFoo.clear();
			cin >> sFoo;
			sFoo = sDefPath + sFoo;
			if ( fileManager.loadFileName(sFoo) == -1 )
			{
				printf("Unable load file %s.\n", sFoo.c_str());
			}
			if ( fileManager.isDir() )
			{
				fileManager.getElementsInDirectory(vsOut, sFoo);
				if ( vsOut.size() )
				{
					printf("Directory not empty, file inside it: %d\n",vsOut.size());
					break;
				}
				liRes = fileManager.deleteEmptyDir();
				printf("Result delete empty dir %d.\n", liRes);
				break;
			}
			liRes = fileManager.deleteFile();
			printf("Result delete file %d.\n", liRes);
		break;

		case 'c':
			printf("Please insert the abs file name /home/root/");
			sFoo.clear();
			cin >> sFoo;
			sFoo = sDefPath + sFoo;
			printf("You can't go back. Are you sure you want to delete the directory?[Y\\N]\n");
			cin >> sCmd[0];

			if ( sCmd[0] != 'y' || sCmd[0] != 'Y' )		break;
			liRes = fileManager.deleteDirRecursively(sFoo.c_str());
			if ( liRes == -1 )
			{
				printf("Unable to delete the directory recursively\n");
			}
		break;

		case 'd':
			printf("Please insert the whole path:\n");
			sFoo.clear();
			cin >> sFoo;
			if ( fileManager.loadFileName(sFoo) == -1 )
			{
				printf("Unable load from %s.\n", sFoo.c_str());
			}
			vsOut.clear();
			fileManager.getElementsInDirectory(vsOut, sFoo);
			for (auto str : vsOut )
			{
				printf("File name: %s\n", str.c_str() );
			}

		break;

		case 'e':
			printf("Please insert the whole path:\n");
			sFoo.clear();
			cin >> sFoo;
			if ( fileManager.loadFileName(sFoo) == -1 )
			{
				printf("Unable load from %s.\n", sFoo.c_str());
			}
			printf("Please press 1 for .tar files, 2 for .txt files, 3 for .xml files.\n");
			sType.clear();
			cin.get();
			sType[0] = cin.get();

			switch ( sType[0] )
			{
				case '1':
					fileManager.getFilesInDirectoryFilter(vsOut, sFoo, fileManager.m_KeyTar);
				break;

				case '2':
					fileManager.getFilesInDirectoryFilter(vsOut, sFoo, fileManager.m_KeyTxt);
				break;

				case '3':
					fileManager.getFilesInDirectoryFilter(vsOut, sFoo, fileManager.m_KeyXml);
				break;

				default:
					printf("Wrong key pressed. I'm sorry.\n");
					sType[1] = 9;
				break;
			}
			if ( sType[1] != 9 )
			{
				if ( ! vsOut.size() )
				{
					printf("No file found with the key desired.");
					break;
				}
				for (auto str : vsOut )
				{
					printf("File name with the key desired: %s\n", str.c_str());
				}
			}

		break;

		case 'f':
		{
			printf("Please insert the abs file name /home/root/");
			sFoo.clear();
			cin >> sFoo;
			if ( fileManager.loadFileName(sFoo) == -1 )
			{
				printf("Unable load from %s.\n", sFoo.c_str());
			}

			unsigned long liFileSize;
			fileManager.getFileLen(liFileSize);
			uint8_t rgcFile[liFileSize] = {0};
			fileManager.readFile(sFoo, rgcFile, liFileSize);

			for (uint i = 0; i < liFileSize; i++ )
			{
				printf("%c", rgcFile[i]);
			}
		}
		break;

		default:
			printf("Wrong key pressed. I'm sorry.\n");
			return -1;
		break;
	}

	return 0;
}
