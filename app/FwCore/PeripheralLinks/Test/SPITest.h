#ifndef SPITEST_H
#define SPITEST_H

#include "MainExecutor.h"


void testSPI(void);
int testNSH(void);
int testMotor(void);
int testEncoderCounter(void);



#endif // SPITEST_H
