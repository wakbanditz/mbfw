#include "MasterUpdateTest.h"

int testMasterUpdate()
{
    cout << "Are you sure you want to test the update of the Master? [y/n]" << endl;
    char cAns;
    cin >> cAns;

    switch (cAns)
    {
        case 'y':
		case 'Y':
		{
			string strFunc("chmod +x /home/root/updateAppl.sh &");
			system(strFunc.c_str());
			strFunc = "/home/root/updateAppl.sh &";
			system(strFunc.c_str());
			MainExecutor::getInstance()->updateApplicative();
		}
        break;

        case 'n':
        case 'N':
            return -1;
        break;

        default:
            cout << "Wrong key pressed" << endl;
            return -1;
        break;
    }

    return 0;
}
