#ifndef TESTMANAGER_H
#define TESTMANAGER_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <Thread.h>

#include "Config.h"
#include "CANTest.h"
#include "SPITest.h"
#include "USBTest.h"
#include "FolderManagerTest.h"
#include "MasterUpdateTest.h"


/*!
 * @brief The TestManager class
 * The class is used for the execution of test management. It's a thread used to choose what test should be run.
 * After the setting of the peripheral test, the function workerThread monitors and executes the related routine test.
 */

class TestManager : public Thread,
					public Loggable
{
	private:

		char		m_ubTestToExecute;
		uint8_t		m_ubTypeCmd;
		uint8_t		m_ubState;
		Config		*m_config;

	public:
		TestManager();
		virtual ~TestManager();

		/*!
		 * @brief	initConfig	The function is used to set the configuration read by MainExecutor object.
		 * @param	pconfig
		 */
		void initConfig(Config * pconfig);

	private:

		/*!
		 * @brief getkey
		 * @return	0 if success | -1 otherwise
		 */
		int getkey(void);

		/*!
		 * @brief ctrlExit
		 * @return	0 if success | -1 otherwise
		 */
		int ctrlExit(void);

		/*!
		 * @brief	setTest		Function for the choice of the device to test.
		 * @return	0 if success | -1 otherwise
		 */
		int	setTest(void);

		/*!
		 * @brief setTypeCmd
		 * @return	0 if success | -1 otherwise
		 */
		int setTypeCmd(void);

		/*!
		 * @brief	executeTest	It performs the related test that was chosen first in the setTest routine.
		 * @return	0 if success | -1 otherwise
		 */
		int executeTest(void);

		int testPowerManager( void );
		int testGpioExpander( void );
		int testGpioExpanderLed( void );
		int testLedDriver(void);
		int testFanController(void);
		int testEeprom(void);
		int testCodecAudio(void);
		int testSection( void );
		int testFunction( void );
		int testMotors( void );
        int testSleepMode( void );


	protected:

		void	beforeWorkerThread(void);

		/*!
		 * @brief workerThread	It manages the execution of setTest and executeTest functions.
		 * @return 0 if success | -1 otherwise
		 */
		int		workerThread(void);

		void	afterWorkerThread(void);

};

#endif // TESTMANAGER_H
