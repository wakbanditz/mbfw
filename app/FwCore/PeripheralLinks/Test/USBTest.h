#ifndef CAMERATEST_H
#define CAMERATEST_H

#include "CR8062Include.h"
#include "MainExecutor.h"

#include <iostream>

#define ESC		27

using namespace std;

int testCamera(void);

#endif // CAMERATEST_H
