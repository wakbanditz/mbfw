/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    TLV320AIC31xx.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the TLV320AIC31xx class.
 @details

 ****************************************************************************
*/

#include "TLV320aic31xx.h"
#include "I2CCommonInclude.h"

TLV320AIC31xx::TLV320AIC31xx()
{
	memcpy(m_ubCardName, "hw:0", sizeof("hw:0"));
	memcpy(m_ubControlSpeakerDriverName, "Speaker Driver", sizeof("Speaker Driver"));
	memcpy(m_ubControlSpeakerAnalogName, "Speaker Analog", sizeof("Speaker Analog"));
	memcpy(m_ubControlDACName, "DAC", sizeof("DAC"));

	m_pHandle = NULL;
}

TLV320AIC31xx::~TLV320AIC31xx()
{
	snd_mixer_close(m_pHandle);
}

int TLV320AIC31xx::setCardName(uint8_t *ubName)
{
	memcpy(m_ubCardName, ubName, sizeof(m_ubCardName));


	//Selection of type volume
	int res = setSndMixerHandle();
	if (res < 0)
	{
		m_pLogger->log(LOG_ERR, "TLV320AIC31xx::setCardName--> Unable to set card name ");
		return -1;
	}

	return 0;
}

int TLV320AIC31xx::setSpeakerAnalogVolume(uint8_t ubVolume)
{
//	snd_mixer_t			*handle = NULL;
	snd_mixer_elem_t	*elem = NULL;

	//Selection of type volume
	elem = selectControl(m_pHandle, m_ubControlSpeakerAnalogName);

	snd_mixer_selem_set_playback_volume_all(elem, ubVolume);

	return 0;
}

int TLV320AIC31xx::setDACGain(uint8_t ubVal)
{
	snd_mixer_elem_t	*elem = NULL;

	//Selection of type volume
	elem = selectControl(m_pHandle, m_ubControlDACName);

	snd_mixer_selem_set_playback_volume_all(elem, ubVal);

	return 0;
}

int TLV320AIC31xx::setSpeakerDriverGain(uint8_t ubVal)
{
	snd_mixer_elem_t	*elem = NULL;

	//Selection of type volume
	elem = selectControl(m_pHandle, m_ubControlSpeakerDriverName);

	snd_mixer_selem_set_playback_volume_all(elem, ubVal);

	return 0;
}

int TLV320AIC31xx::getSpeakerAnalogVolume(long &lVolume)
{
	snd_mixer_elem_t*	elem;
	long lCurrentVolume = 0;
	int res = 0;

	//Selection of type volume
	elem = selectControl(m_pHandle, m_ubControlSpeakerAnalogName);

	res = snd_mixer_selem_get_playback_volume(elem, SND_MIXER_SCHN_FRONT_LEFT, &lCurrentVolume);
	if (res == 0 )
	{
		 m_pLogger->log(LOG_INFO, "TLV320AIC31xx::getSpeakerAnalogVolume--> Vol:%d \n", lCurrentVolume);

		 lVolume = lCurrentVolume;
	}
	else
	{
		m_pLogger->log(LOG_INFO, "TLV320AIC31xx::getSpeakerAnalogVolume--> ERR: %s\n",snd_strerror(res));
		return -1;
	}
	return 0;
}

int TLV320AIC31xx::getSpeakerRange(eTLV320_CONTROLS eTypeCtrl, long &lmin, long &lmax)
{
	snd_mixer_elem_t*	elem;

	switch (eTypeCtrl)
	{
		case eTLV320_ANVOL:
			elem = selectControl(m_pHandle, m_ubControlSpeakerAnalogName);
			break;

		case eTLV320_DVOL:
			elem = selectControl(m_pHandle, m_ubControlSpeakerDriverName);
			break;

		case eTLV320_DAC:
			elem = selectControl(m_pHandle, m_ubControlDACName);
			break;

		default:
			break;
	}

	long lrangeMin, lrangeMax;
	snd_mixer_selem_get_playback_volume_range(elem, &lrangeMin, &lrangeMax);
	lmin = lrangeMin;
	lmax = lrangeMax;

	return 0;
}


snd_mixer_elem_t * TLV320AIC31xx::selectControl(snd_mixer_t *handle, uint8_t * ubControl)
{
	snd_mixer_selem_id_t *sid = NULL;
	snd_mixer_elem_t* elem= NULL;

	// ID
	snd_mixer_selem_id_alloca(&sid);
	snd_mixer_selem_id_set_index(sid, 0);
	snd_mixer_selem_id_set_name(sid, (char*)ubControl);

	elem = snd_mixer_find_selem(handle, sid);

	return elem;
}


int TLV320AIC31xx::setSndMixerHandle(void)
{
	int err = 0;

	// Prepare snd_mixer_t object
	if ((err = snd_mixer_open(&m_pHandle, 0)) < 0)
	{
		m_pLogger->log(LOG_ERR, "TLV320AIC31xx::setSndMixerHandle--> Unable to open mixer: %s", snd_strerror(err));
		return -1;
	}

	// Attach the object with choosen card
	if ((err = snd_mixer_attach(m_pHandle, (char*)&m_ubCardName[0])) < 0)
	{

		m_pLogger->log(LOG_ERR, "TLV320AIC31xx::setSndMixerHandle--> Unable to attach mixer to device default: %s",
					   snd_strerror(err));
		snd_mixer_close(m_pHandle);
		m_pHandle = NULL;
		return -1;
	}

	if ((err = snd_mixer_selem_register(m_pHandle, NULL, NULL)) < 0)
	{
		m_pLogger->log(LOG_ERR, "TLV320AIC31xx::setSndMixerHandle--> Unable to register mixer elements: %s",
					   snd_strerror(err));
		snd_mixer_close(m_pHandle);
		m_pHandle = NULL;
		return -1;
	}

	// Get the mixer controls from the kernel
	if ((err = snd_mixer_load(m_pHandle)) < 0)
	{
		m_pLogger->log(LOG_ERR, "TLV320AIC31xx::setSndMixerHandle--> Unable to load mixer elements: %s",
					   snd_strerror(err));
		snd_mixer_close(m_pHandle);
		m_pHandle = NULL;
		return -1;
	}

	return 0;
}




