/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ADT7470.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the ADT7470 class.
 @details

 ****************************************************************************
*/

#include "ADT7470.h"
#include <math.h>

ADT7470::ADT7470()
{
	/* Nothing to do yet */
}

ADT7470::~ADT7470()
{
	/* Nothing to do yet */
}

/*!****************************************************************************************
 *
 * Operations
 *
 ******************************************************************************************/
int ADT7470::initConfigRegister( void )
{
	int res = 0;
	res = writeConfigRegister2(0xF0);
	msleep(1);
	if (res != 0)
	{
		return -1;
	}

	res = writePWM12ConfigurationRegister(0x30);
	msleep(1);
	if (res != 0)
	{
		return -1;
	}

	res = writePWM34ConfigurationRegister(0x30);
	msleep(1);
	if (res != 0)
	{
		return -1;
	}

    res = writeCurrentDutyCycle(0, 0);
	msleep(1);
	if (res != 0)
	{
		return -1;
	}

    res = writeCurrentDutyCycle(1, 0);
	msleep(1);
	if (res != 0)
	{
		return -1;
	}

    res = writeCurrentDutyCycle(2, 0);
	msleep(1);
	if (res != 0)
	{
		return -1;
	}

    res = writeCurrentDutyCycle(3, 0);
	msleep(1);
	if (res != 0)
	{
		return -1;
	}

	res = writeConfigRegister1(0x61);
	msleep(1);
	if (res != 0)
	{
		return -1;
	}

	res = writeItrMaskReg1(0x7F);
	msleep(1);
	if (res != 0)
	{
		return -1;
	}

	res = writeItrMaskReg2(0x07);
	msleep(1);
	if (res != 0)
	{
		return -1;
	}

	res = writeConfigRegister2(0x70);
	if (res != 0)
	{
		return -1;
	}

	return 0;
}

int ADT7470::config(void)
{
	int res = writeConfigRegister1(0x00);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::config-->Config 1 Failed");
		return -1;
	}

	return 0;
}

int ADT7470::configLowFrequency(void)
{

	int res = writeConfigRegister1(0x61);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::configLowFrequency-->Config 1 Failed");
		return -1;
	}

	res = writeConfigRegister2(0x70);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::configLowFrequency-->Config 2 Failed");
		return -1;
	}

	return 0;
}

int ADT7470::startFanController(void)
{
	uint8_t ubState;

	int res = readConfigRegister1(ubState);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::startFanController-->Read failed");
		return -1;
	}

	ubState |= 0x01;
	res = writeConfigRegister1(ubState);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::startFanController-->Write failed");
		return -1;
	}

	return 0;
}

int ADT7470::stopFanController(void)
{
	uint8_t ubState;

	int res = readConfigRegister1(ubState);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::stopFanController-->Read failed");
		return -1;
	}

	ubState &= 0xFE;
	res = writeConfigRegister1(ubState);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::stopFanController-->Write failed");
		return -1;
	}

	return 0;
}

int ADT7470::setItrRegMask1(eADT7470ItrReg1 eMask, bool bEnable)
{
	uint8_t	ubState = 0;
	int res = readItrMaskReg1(ubState);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::setItrRegMask1-->Read failed");
		return -1;
	}

	if (bEnable == true)
	{
		ubState |= eMask;
	}
	else
	{
		ubState &= ~eMask;
	}
	return 0;
}

int ADT7470::setItrRegMask2(eADT7470ItrReg2 eMask, bool bEnable)
{
	uint8_t	ubState = 0;
	int res = readItrMaskReg2(ubState);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::setItrRegMask2-->Read failed");
		return -1;
	}

	if (bEnable == true)
	{
		ubState |= eMask;
	}
	else
	{
		ubState &= ~eMask;
	}
	return 0;
}

/*!****************************************************************************************
 *
 * PUBLIC WRITE REG operations
 *
 ******************************************************************************************/

int ADT7470::writeCurrentDutyCycle(uint8_t ubNumFan, uint8_t ubDC)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubNumFan >= ADT7470_PWM_COUNT)
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeCurrentDC-->Num FAN Out Of Range");
		return -1;
	}

	if (ubDC > 100)
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeCurrentDC-->% Param out of range");
		return -1;
	}

    uint8_t ubNumDig = ((uint16_t)ubDC * 255) / 100;

	pubTxBuff[0] = ubNumDig;

	uint8_t ubAddPWM = ADT7470_REG_PWM(ubNumFan);
	int res = write(ubAddPWM, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeCurrentDC-->Unable to write register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "ADT7470::writeCurrentDC-->Write register");
	}
	return 0;
}

int ADT7470::writeMaxDC(uint8_t ubNumFan, uint8_t ubDC)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubNumFan >= ADT7470_PWM_COUNT)
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeMinDC-->Num FAN Out Of Range");
		return -1;
	}

	if (ubDC > 100)
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeCurrentDC-->% Duty cycle out of range");
		return -1;
	}

	uint8_t ubNumDig = ((uint16_t)ubDC*255)/100;

	pubTxBuff[0] = ubNumDig;

	uint8_t ubAddPWM = ADT7470_REG_PWM_MAX(ubNumFan);
	int res = write(ubAddPWM, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeMaxDC-->Unable to write register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "ADT7470::writeMaxDC-->Write register");
	}
	return 0;
}

int ADT7470::writeMinDC(uint8_t ubNumFan, uint8_t ubDC)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubNumFan >= ADT7470_PWM_COUNT)
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeMinDC-->Num FAN Out Of Range");
		return -1;
	}

	if (ubDC > 100)
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeCurrentDC-->% Duty cycle out of range");
		return -1;
	}

	uint8_t ubNumDig = ((uint16_t)ubDC*255)/100;

	pubTxBuff[0] = ubNumDig;

	uint8_t ubAddPWM = ADT7470_REG_PWM_MIN(ubNumFan);
	int res = write(ubAddPWM, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeMinDC-->Unable to write register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "ADT7470::writeMinDC-->Write register");
	}
	return 0;
}

int ADT7470::writeTachLimitMin(uint8_t ubTach, uint16_t usLimitMin)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubTach >= ADT7470_FAN_COUNT)
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeTachLimitMin-->Num Tach Out Of Range");
		return -1;
	}

	pubTxBuff[0] = usLimitMin;
	pubTxBuff[1] = usLimitMin>>8;

	uint8_t ubAddTachLim = ADT7470_REG_FAN_MIN(ubTach);
	int res = write(ubAddTachLim, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeTachLimitMin-->Unable to write register");
		return -1;
	}

	res = write(ubAddTachLim+1, &pubTxBuff[1], 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeTachLimitMin-->Unable to write register");
		return -1;
	}

	return 0;
}

int ADT7470::writeTachLimitMax(uint8_t ubTach, uint16_t usLimitMax)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	if (ubTach >= ADT7470_FAN_COUNT)
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeTachLimitMax-->Num Tach Out Of Range");
		return -1;
	}

	pubTxBuff[0] = usLimitMax;
	pubTxBuff[1] = usLimitMax>>8;

	uint8_t ubAddTachLim = ADT7470_REG_FAN_MAX(ubTach);
	int res = write(ubAddTachLim, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeTachLimitMax-->Unable to write register");
		return -1;
	}

	res = write(ubAddTachLim+1, &pubTxBuff[1], 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeTachLimitMax-->Unable to write register");
		return -1;
	}
	return 0;
}

/*!****************************************************************************************
 *
 * PUBLIC READ REG operations
 *
 ******************************************************************************************/

int ADT7470::readDeviceIDRegister(uint8_t &ubDeviceID)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(ADT7470_REG_DEVICE, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readDeviceIDRegister-->Unable to read register");
		return -1;
	}
	else
	{
		ubDeviceID = pubRxBuff[0];
	}
	return 0;
}

int ADT7470::readCompanyIDNumber(uint8_t &ubCompanyID)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(ADT7470_REG_VENDOR, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readCompanyIDNumber-->Unable to read register");
		return -1;
	}
	else
	{
		ubCompanyID = pubRxBuff[0];
	}
	return 0;
}

int ADT7470::readRevisionNumber(uint8_t &ubRevision)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(ADT7470_REG_REVISION, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readRevisionNumber-->Unable to read register");
		return -1;
	}
	else
	{
		ubRevision = pubRxBuff[0];
	}
	return 0;
}

int ADT7470::readTemp(uint8_t ubNumTemp, uint8_t &ubTemp)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubNumTemp >= ADT7470_TEMP_COUNT)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readTemp-->Out Of Range");
		return -1;
	}

	uint8_t ubAddTempRead = ADT7470_TEMP_REG(ubNumTemp);

	int res = read(ubAddTempRead, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readTemp-->Unable to read register");
		return -1;
	}
	else
	{
		ubTemp = pubRxBuff[0];
	}
	return 0;
}

int ADT7470::readFan(uint8_t ubNumFan, uint16_t &usTach)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubNumFan >= ADT7470_FAN_COUNT)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readFan-->Out Of Range");
		return -1;
	}

	uint8_t ubAddFanRead = ADT7470_REG_FAN(ubNumFan);

	int res = read(ubAddFanRead, &pubRxBuff[0], 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readFan-->Unable to read register");
		return -1;
	}

	res = read(ubAddFanRead + 1, &pubRxBuff[1], 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readFan-->Unable to read register");
		return -1;
	}
	// LSB | MSB
	uint16_t usAppo = 0;
	usAppo = (uint16_t)(pubRxBuff[1] << 8);
	usAppo |=  pubRxBuff[0];
	if (usAppo != 0)
	{
		usTach = (90000 * 60) / usAppo;
	}
	else
	{
		usTach = 0;
	}
	return 0;
}

int ADT7470::readCurrentDutyCycle(uint8_t ubNumPWM, uint8_t &ubCurrDC)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubNumPWM >= ADT7470_PWM_COUNT)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readCurrentDutyCycle-->Out Of Range");
		return -1;
	}

	uint8_t ubAddPWMRead = ADT7470_REG_PWM(ubNumPWM);

	int res = read(ubAddPWMRead, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readCurrentDutyCycle-->Unable to read register");
		return -1;
	}
	else
	{
		float fAppo = pubRxBuff[0];
		uint8_t ubNumPerc =  round(((fAppo*100)/255));

		ubCurrDC = ubNumPerc;
	}

	return 0;
}

int ADT7470::readMaxDutyCycle(uint8_t ubNumPWM, uint8_t &ubMaxDC)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubNumPWM >= ADT7470_PWM_COUNT)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readMaxDutyCycle-->Out Of Range");
		return -1;
	}

	uint8_t ubAddPWMRead = ADT7470_REG_PWM_MAX(ubNumPWM);

	int res = read(ubAddPWMRead, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readMaxDutyCycle-->Unable to read register");
		return -1;
	}
	else
	{
		float fAppo = pubRxBuff[0];
		uint8_t ubNumPerc =  round(((fAppo*100)/255));

		ubMaxDC = ubNumPerc;
	}

	return 0;
}

int ADT7470::readMinDutyCycle(uint8_t ubNumPWM, uint8_t &ubMinDC)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubNumPWM >= ADT7470_PWM_COUNT)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readMinDutyCycle-->Out Of Range");
		return -1;
	}

	uint8_t ubAddPWMRead = ADT7470_REG_PWM_MIN(ubNumPWM);

	int res = read(ubAddPWMRead, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readMinDutyCycle-->Unable to read register");
		return -1;
	}
	else
	{
		float fAppo = pubRxBuff[0];
		uint8_t ubNumPerc =  round(((fAppo*100)/255));

		ubMinDC = ubNumPerc;
	}

	return 0;
}

int ADT7470::readTemperatureHighLimit(uint8_t ubNumTemp, uint8_t &ubHighLimit)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubNumTemp >= ADT7470_TEMP_COUNT)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readTemperatureHighLimit-->Out Of Range");
		return -1;
	}

	uint8_t ubAddTempRead = ADT7470_TEMP_MAX_REG(ubNumTemp);

	int res = read(ubAddTempRead, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readTemperatureHighLimit-->Unable to read register");
		return -1;
	}
	else
	{
		ubHighLimit = pubRxBuff[0] ;
	}

	return 0;
}

int ADT7470::readTemperatureLowLimit(uint8_t ubNumTemp, uint8_t &ubLowLimit)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubNumTemp >= ADT7470_TEMP_COUNT)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readTemperatureLowLimit-->Out Of Range");
		return -1;
	}

	uint8_t ubAddTempRead = ADT7470_TEMP_MIN_REG(ubNumTemp);

	int res = read(ubAddTempRead, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readTemperatureLowLimit-->Unable to read register");
		return -1;
	}
	else
	{
		ubLowLimit = pubRxBuff[0] ;
	}

	return 0;
}

int ADT7470::readFanHighLimit(uint8_t ubNumFan, uint16_t &usHighLimit)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubNumFan >= ADT7470_FAN_COUNT)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readFanHighLimit-->Out Of Range");
		return -1;
	}

	uint8_t ubAddFanRead = ADT7470_REG_FAN_MAX(ubNumFan);

	int res = read(ubAddFanRead, &pubRxBuff[0], 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readFanHighLimit-->Unable to read register");
		return -1;
	}

	res = read(ubAddFanRead+1, &pubRxBuff[1], 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readFanHighLimit-->Unable to read register");
		return -1;
	}
	// LSB | MSB
	uint16_t usAppo = 0;
	usAppo = (uint16_t)(pubRxBuff[1] << 8);
	usAppo |=  pubRxBuff[0];
	if (usAppo != 0)
	{
		usHighLimit = (90000*60)/usAppo;
	}
	else
	{
		usHighLimit = 0;
	}

	return 0;
}

int ADT7470::readFanLowLimit(uint8_t ubNumFan, uint16_t &usLowLimit)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubNumFan >= ADT7470_FAN_COUNT)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readFanLowLimit-->Out Of Range");
		return -1;
	}

	uint8_t ubAddFanRead = ADT7470_REG_FAN_MIN(ubNumFan);

	int res = read(ubAddFanRead, pubRxBuff, 2);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readFanLowLimit-->Unable to read register");
		return -1;
	}

	res = read(ubAddFanRead+1, &pubRxBuff[1], 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readFanLowLimit-->Unable to read register");
		return -1;
	}
	// LSB | MSB
	uint16_t usAppo = 0;
	usAppo = (uint16_t)(pubRxBuff[1] << 8);
	usAppo |=  pubRxBuff[0];
	if (usAppo != 0)
	{
		usLowLimit = (90000*60)/usAppo;
	}
	else
	{
		usLowLimit = 0;
	}

	return 0;
}

int ADT7470::readTMin(uint8_t ubNumFan, uint8_t &ubTmin)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	if (ubNumFan >= ADT7470_FAN_COUNT)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readTMin-->Out Of Range");
		return -1;
	}

	uint8_t ubAddTminRead = ADT7470_REG_PWM_TMIN(ubNumFan);

	int res = read(ubAddTminRead, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readTMin-->Unable to read register");
		return -1;
	}
	else
	{
		ubTmin = pubRxBuff[0] ;
	}

	return 0;
}

int ADT7470::readItrStatusRegister1(uint8_t &ubItrReg)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(ADT7470_REG_ALARM1, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readItrStatusRegister1-->Unable to read register");
		return -1;
	}
	else
	{
		ubItrReg = pubRxBuff[0] ;
	}
	return 0;
}

int ADT7470::readItrStatusRegister2(uint8_t &ubItrReg)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(ADT7470_REG_ALARM2, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readItrStatusRegister2-->Unable to read register");
		return -1;
	}
	else
	{
		ubItrReg = pubRxBuff[0] ;
	}
	return 0;
}

int ADT7470::writeConfigRegister1(uint8_t ubReg)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubReg;

	int res = write(ADT7470_REG_CFG1, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeConfigRegister1-->Unable to write register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "ADT7470::writeConfigRegister1-->Write register");
	}
	return 0;
}

int ADT7470::writeConfigRegister2(uint8_t ubReg)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubReg;

	int res = write(ADT7470_REG_CFG2, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeConfigRegister2-->Unable to write register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "ADT7470::writeConfigRegister2-->Write register");
	}
	return 0;
}

int ADT7470::writeFanPulsesRevolution(uint8_t ubPulses)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubPulses;

	int res = write(ADT7470_REG_PULSES, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeFanPulsesRevolution-->Unable to write register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "ADT7470::writeFanPulsesRevolution-->Write register");
	}
	return 0;
}

int ADT7470::writePWM12ConfigurationRegister(uint8_t ubValue)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubValue;
	int res = write(ADT7470_REG_PWM12_CFG, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writePWM12ConfigurationRegister-->Unable to write register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "ADT7470::writePWM12ConfigurationRegister-->Write register");
	}
	return 0;
}

int ADT7470::writePWM34ConfigurationRegister(uint8_t ubValue)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubValue;
	int res = write(ADT7470_REG_PWM34_CFG, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writePWM34ConfigurationRegister-->Unable to write register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "ADT7470::writePWM34ConfigurationRegister-->Write register");
	}
	return 0;
}

int ADT7470::writeItrMaskReg1(uint8_t ubMaskReg)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubMaskReg;

	int res = write(ADT7470_REG_ALARM1_MASK, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeItrMaskReg1-->Unable to write register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "ADT7470::writeItrMaskReg1-->Write register");
	}
	return 0;
}

int ADT7470::writeItrMaskReg2(uint8_t ubMaskReg)
{
	uint8_t pubTxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubTxBuff, 0, sizeof(pubTxBuff));

	pubTxBuff[0] = ubMaskReg;

	int res = write(ADT7470_REG_ALARM2_MASK, pubTxBuff, 1);
	if ( res != 0 )
	{
		m_pLogger->log( LOG_ERR, "ADT7470::writeItrMaskReg2-->Unable to write register");
		return -1;
	}
	else
	{
		m_pLogger->log( LOG_DEBUG_PARANOIC, "ADT7470::writeItrMaskReg2-->Write register");
	}
	return 0;
}

/*!****************************************************************************************
 *
 * PRIVATE READ register
 *
 ******************************************************************************************/

int ADT7470::readConfigRegister1(uint8_t &ubConfigRegister)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(ADT7470_REG_CFG1, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readConfigRegister1-->Unable to read register");
		return -1;
	}
	else
	{
		ubConfigRegister = pubRxBuff[0];
	}
	return 0;
}

int ADT7470::readConfigRegister2(uint8_t &ubConfigRegister)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(ADT7470_REG_CFG2, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readConfigRegister1-->Unable to read register");
		return -1;
	}
	else
	{
		ubConfigRegister = pubRxBuff[0];
	}
	return 0;
}

int ADT7470::readFanPulsesRevolution(uint8_t &ubPulses)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(ADT7470_REG_PULSES, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readFanPulsesRevolution-->Unable to read register");
		return -1;
	}
	else
	{
		ubPulses = pubRxBuff[0];
	}
	return 0;
}

int ADT7470::readItrMaskReg1(uint8_t &ubMaskReg)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(ADT7470_REG_ALARM1_MASK, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readItrMaskReg1-->Unable to read register");
		return -1;
	}
	else
	{
		ubMaskReg = pubRxBuff[0] ;
	}
	return 0;
}

int ADT7470::readItrMaskReg2(uint8_t &ubMaskReg)
{
	uint8_t pubRxBuff[ADT7470_MAX_MSG_SIZE_BUFF];
	memset(pubRxBuff, 0, sizeof(pubRxBuff));

	int res = read(ADT7470_REG_ALARM2_MASK, pubRxBuff, 1);
	if (res != 0)
	{
		m_pLogger->log(LOG_ERR, "ADT7470::readItrMaskReg2-->Unable to read register");
		return -1;
	}
	else
	{
		ubMaskReg = pubRxBuff[0] ;
	}
	return 0;
}






























