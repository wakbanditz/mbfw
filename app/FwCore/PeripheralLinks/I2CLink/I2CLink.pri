INCLUDEPATH += $$PWD/../I2CLink/

HEADERS += \
    $$PWD/AT24C256C.h \
    $$PWD/PCA9955B.h \
    $$PWD/UCD9090a.h \
    $$PWD/PmBusCmd.h \
    $$PWD/I2CBoardBase.h \
    $$PWD/I2CLinkBoard.h \
    $$PWD/PCA9633.h \
    $$PWD/I2CInterfaceCollector.h \
    $$PWD/I2CCommonInclude.h \
    $$PWD/ADT7470.h \
    $$PWD/TLV320aic31xx.h \
    $$PWD/PCA9537.h

   


SOURCES += \
    $$PWD/AT24C256C.cpp \
    $$PWD/PCA9955B.cpp \
    $$PWD/UCD9090a.cpp \
    $$PWD/I2CBoardBase.cpp \
    $$PWD/I2CLinkBoard.cpp \
    $$PWD/PCA9633.cpp \
    $$PWD/I2CInterfaceCollector.cpp \
    $$PWD/ADT7470.cpp \
    $$PWD/TLV320AIC31xx.cpp \
    $$PWD/PCA9537.cpp

    

