/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    AT24C256C.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the AT24C256C class.
 @details

 ****************************************************************************
*/

#ifndef AT24C256C_H
#define AT24C256C_H

#include "I2CBoardBase.h"
#include "ErrorCodes.h"

typedef enum
{
    eWriteBuff = 1,
    eWriteUntilEndPage = 2,
    eWriteAllThePage = 3
} enumEEpromWriteStates;

class AT24C256C : public I2CBoardBase
{

	public:

		/*! **********************************************************************************************************************
		 * @brief AT24C256C default constructor.
		 * ***********************************************************************************************************************
		 */
		AT24C256C();

		/*! **********************************************************************************************************************
		 * @brief ~AT24C256C default destructor.
		 * ***********************************************************************************************************************
		 */
		virtual ~AT24C256C();

		/*! **********************************************************************************************************************
		 * @brief  writeWord writes a word of 8 bit to a designed address.
		 * @param  usStartAddress address where the word is written.
		 * @param  ucWord 8 bit word to be stored in the eeprom.
		 * @return 0 in case of success, -1 in case of error.
		 * ***********************************************************************************************************************
		 */
		int8_t writeWord(uint16_t usStartAddress, uint8_t ucWord);

		/*! **********************************************************************************************************************
		 * @brief  write writes a buffer of a certain size to memory.
		 * @param  usStartAddress start address.
		 * @param  pBuff pointer to the buffer to be stored.
		 * @param  usMsgLen buffer length.
		 * @return 0 in case of success, -1 in case of error.
		 * ***********************************************************************************************************************
		 */
		int8_t write(uint16_t usStartAddress, uint8_t * pBuff, uint16_t usMsgLen);

		/*! **********************************************************************************************************************
		 * @brief  readWord read 8bit word from memory cell.
		 * @param  usStartAddress address of the word to be read.
		 * @param  ucWordRead where the word read is stored.
		 * @return 0 in case of success, -1 in case of error.
		 * ***********************************************************************************************************************
		 */
		int8_t readWord(uint16_t usStartAddress, uint8_t& ucWordRead);

		/*! **********************************************************************************************************************
		 * @brief  read sequential read of the memory.
		 * @param  usStartAddress stard address of the read.
		 * @param  pBuff pointer to buffer where the content read from the Eeprom will be stored.
		 * @param  usMsgLen length of the message we want to read.
		 * @return 0 in case of success, -1 in case of error.
		 * ***********************************************************************************************************************
		 */
		int8_t read(uint16_t usStartAddress, uint8_t * pBuff, uint16_t usMsgLen);

		/*! **********************************************************************************************************************
		 * @brief  eraseMemoryArea write 0xFF on a certain memory area.
		 * @param  usStartAddress start address of the
		 * @param  usAreaLen lenght of the area to be erased.
		 * @return 0 in case of success, -1 in case of error.
		 * ***********************************************************************************************************************
		 */
		int8_t eraseEepromArea(uint16_t usStartAddress, uint16_t usAreaLen);

		/*! **********************************************************************************************************************
		 * @brief  resetEeprom bring Eeprom back to factory default (all 0xFF).
		 * @return 0 in case of success, -1 in case of error.
		 * ***********************************************************************************************************************
		 */
		int8_t resetEeprom();

    private:

        bool waitUntilReady(int32_t slDeviceAddress, uint16_t usStartAddress);

};

#endif // AT24C256C_H
