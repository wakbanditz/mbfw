/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PCA9633.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the PCA9633 class.
 @details

 ****************************************************************************
*/

#ifndef PCA9633_H
#define PCA9633_H

#include "I2CBoardBase.h"
#include "I2CCommonInclude.h"

/*!
 * @brief The PCA9633 class
 * It is able to manage PCA9633 device driver sending I2C messages. There are the functions to read and to write
 * all the register of the device.
 */
class PCA9633 : public I2CBoardBase
{
	public:
		typedef enum
		{
			ePWM0	= 0,
			ePWM1	= 1,
			ePWM2	= 2,
			ePWM3	= 3
		} ePCA9633PwmLed;

		typedef enum
		{
			eLED0	= 0,
			eLED1	= 1,
			eLED2	= 2,
			eLED3	= 3
		} ePCA9633Led;


	public:
		PCA9633();
		virtual ~PCA9633();

		/*!
		 * @brief resetSoft		It performs a reset SW of the device. It initializes the registers to
		 *						their default state causing the outputs to be set HIGH.
		 *						This command is different from the others: it is able to reset the device
		 *						if the address sent is 0x06. After, the master has to send data byte 0xA5
		 *						and 0x5A. Only by sending this sequence PCA9633 cam acknowledge the command.
		 *
		 * @return 0 if success | -1 otherwise
		 */
		int resetSoft( void );

		/*!
		 * @brief config		It performs the initializations of the MODE registers. In particular, it initializes
		 *						all the bits of MODE1 and MODE2 registers with a definite values(0).
		 *						MODE1 is set in sleep mode, without subaddress and not All Call i2c-bus address.
		 *						MODE2 is set in dimming control.
		 * @return 0 if success | -1 otherwise
		 */
		int config( void );

		/*!****************************************************************************************
		 *
		 * WRITE operations
		 *
		 ******************************************************************************************/

		/*!
		 * @brief writeMode1	Write on Mode1 configuration register. Default value is 0x11.
		 *
		 * @param ubMode
		 * @return 0 if success | -1 otherwise
		 */
		int writeMode1( uint8_t ubMode );

		/*!
		 * @brief writeMode2	Write on Mode2 configuration register. Default value is 0x05.
		 * @param ubMode
		 * @return 0 if success | -1 otherwise
		 */
		int writeMode2( uint8_t ubMode );

		/*!
		 * @brief writePWM		It writes the desired brightness in the related PWM register.
		 *						A 97 kHz fixed frequency signal is used for each output. Duty cycle is controlled through
		*						256 linear steps from 00h (0 % duty cycle = LED output off) to FFh. (duty cycle=IDC[7:0]/256)
		 * @param ePwm
		 * @param ubBrightness	[0-100]
		 * @return 0 if success | -1 otherwise
		 */
		int writePWM(uint8_t ePwm, uint8_t ubBrightness );

		/*!
		 * @brief writeLedOut	It sets the output state control of a specific eLed driver.
		 *						- 0 -> Led driver is off
		 *						- 1 -> Led driver is fully on (individual brightness and group dimming/blinking
		 *								not controlled).
		 *						- 2 -> Led driver brightness can be controlled through its PWMx register.
		 *						- 3 -> Led driver brightness and group diiming/blinking can be controlled through its
		 *							   PWM register and GRPPWM registers.
		 * @param eLed			[0-3]
		 * @param ubState		[0-3]
		 * @return 0 if success | -1 otherwise
		 */
		int writeLedOut( uint8_t eLed, uint8_t ubState );

		/*!
		 * @brief writeLedOut	It sets the output state control of a all Led driver in one shot.
		 * @param ubState		bit 7:6 LDR3, bit 5:4 LDR2, bit 3:2 LDR1, bit 1:0 LDR0
		 * @return 0 if success | -1 otherwise
		 */
		int writeLedOut( uint8_t ubState );

		/*!
		 * @brief writeGrpPWM	GRPPWM is used as a global brightness control allowing the LED outputs to be dimmed with the
		 *						same value.
		 *						General brightness for the 4 outputs is controlled through 256 linear steps from 00h
		 *						(0 % duty cycle = LED output off) to FFh (99.6 % duty cycle = maximum brightness).
		 *						Applicable to LED outputs programmed with LDRx = 11 (LEDOUT register).
		 *						Duty cycle = GDC[7:0]/256
		 * @param ubDutyCycle	[0-100]
		 * @return 0 if success | -1 otherwise
		 */
		int writeGrpPWM(uint8_t ubDutyCycle);

		/*!
		 * @brief writeGrpFreq	GRPFREQ is used to program the global blinking period when DMBLNK bit (MODE2
		 *						register) is equal to 1. Value in this register is a ‘Don’t care’ when DMBLNK = 0.
		 *						Blinking period is controlled through 256 linear steps from 00h (41 ms, frequency 24 Hz)
		 *						to FFh (10.73 s).
		 *						global blinking period = (GFRQ[7:0] + 1)/24
		 * @param ubFreq		[0-100]
		 * @return 0 if success | -1 otherwise
		 */
		int writeGrpFreq(uint8_t ubFreq);

		/*!****************************************************************************************
		 *
		 * READ operations
		 *
		 ******************************************************************************************/

		/*!
		 * @brief readMode1
		 * @param ubModeReg
		 * @return 0 if success | -1 otherwise
		 */
		int readMode1( uint8_t &ubModeReg );

		/*!
		 * @brief readMode2
		 * @param ubModeReg
		 * @return 0 if success | -1 otherwise
		 */
		int readMode2( uint8_t &ubModeReg);

		/*!
		 * @brief readPWM		Read Brightness set in the related PWM register.
		 * @param ePwm
		 * @param ubBrightness
		 * @return 0 if success | -1 otherwise
		 */
		int readPWM(uint8_t ePwm, uint8_t &ubBrightness );

		/*!
		 * @brief readLedOut	Read whole register LEDOUT
		 * @param ubState
		 * @return 0 if success | -1 otherwise
		 */
		int readLedOut(uint8_t &ubState );
};

#endif // PCA9633_H
