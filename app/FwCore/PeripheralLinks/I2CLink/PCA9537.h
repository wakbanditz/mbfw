/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PCA9537.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the PCA9537 class.
 @details

 ****************************************************************************
*/

#ifndef PCA9537_H
#define PCA9537_H

#include "I2CBoardBase.h"
#include "I2CCommonInclude.h"

class PCA9537 : public I2CBoardBase
{

	public:
		PCA9537();
		virtual ~PCA9537();

		/*!****************************************************************************************
		 *
		 * WRITE operations
		 *
		 ******************************************************************************************/
		int writeOutputPortRegister(uint8_t ubConf);

		int writePolarityInversionRegister(uint8_t ubConf);

		int writeConfigurationRegister(uint8_t ubConf);

		/*!****************************************************************************************
		 *
		 * READ operations
		 *
		 ******************************************************************************************/
		int readInputPortRegister(uint8_t &ubConf);

		int readOutputPortRegister(uint8_t &ubConf);

		int readPolarityInversionRegister(uint8_t &ubConf);

		int readConfigurationRegister(uint8_t &ubConf);
};

#endif // PCA9537_H
