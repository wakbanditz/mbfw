/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ADT7470.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the ADT7470 class.
 @details

 ****************************************************************************
*/

#ifndef ADT7470_H
#define ADT7470_H

#include "I2CBoardBase.h"
#include "I2CCommonInclude.h"

class ADT7470 : public I2CBoardBase
{
	public:
		ADT7470();
		virtual ~ADT7470();

		/*!
		 * @brief initConfigRegister Initialization of FAN configuration registers for the Modular Vidas instrument.
		 * @return 0 if success | -1 otherwise
		 */
		int initConfigRegister( void );

		/*!
		 * @brief	config	It contains the initializations of the ADT7470 congigurations.
		 * @return	0 if success | -1 otherwise
		 */
		int config(void);

		/*!
		 * @brief	configLowFrequency	It contains the initializations of the ADT7470 configurations for LowFrequency
		 *								settings.
		 * @return	0 if success | -1 otherwise
		 */
		int configLowFrequency(void);

		/*!
		 * @brief	startFanController	It allows the start of fan controller manager
		 * @return 0 if success | -1 otherwise
		 */
		int startFanController(void);

		/*!
		 * @brief	stopFanController	It stop fans and fan controllers manager.
		 * @return	0 if success | -1 otherwise
		 */
		int stopFanController(void);

		/*!
		 * @brief	setItrRegMask1	Set high or low the related Interrupt masked flag.
		 * @param	eMask
		 * @param	bEnable		[true,false]
		 * @return 	0 if success | -1 otherwise
		 */
		int setItrRegMask1(eADT7470ItrReg1 eMask, bool bEnable);

		/*!
		 * @brief	setItrRegMask2	Set high or low the related Interrupt masked flag.
		 * @param	eMask
		 * @param	bEnable		[true, false]
		 * @return	0 if success | -1 otherwise
		 */
		int setItrRegMask2(eADT7470ItrReg2 eMask, bool bEnable);

		/*!****************************************************************************************
		 * WRITE operations
		 ******************************************************************************************/
		/*!
		 * @brief	writeCurrentDC	The PWM current duty cycle registers can be written with 8-bit
		 *							values in manual fan speed control mode to manually adjust the
		 *							speeds of the cooling fans.The PWM duty cycle for each output
		 *							can be set anywhere from 0% to 100%, in steps of 0.39%.
		 *							Value (decimal) = [Val%]/0.39 = x decimal
		 * @param	ubNumFan		[0-3]
		 * @param	ubDC			[0-100]
		 * @return	0 if success | -1 otherwise
		 */
		int writeCurrentDutyCycle(uint8_t ubNumFan, uint8_t ubDC);

		/*!
		 * @brief	writeMaxDC		It is used in automatic fan speed control to set the maximum limit
		 *							of the DC.
		 *							Value (decimal) = Desired PWM duty cycle/0.39
		 * @param	ubNumFan		[0-3]
		 * @param	ubDC			[0-100]
		 * @return	0 if success | -1 otherwise
		 */
		int writeMaxDC(uint8_t ubNumFan, uint8_t ubDC);

		/*!
		 * @brief	writeMinDC		It is used in automatic fan speed control to set the maximum limit
		 *							of the DC.
		 * 							Value (decimal) = Desired PWM duty cycle/0.39
		 * @param	ubNumFan		[0-3]
		 * @param	ubDC			[0-100]
		 * @return	0 if success | -1 otherwise
		 */
		int writeMinDC(uint8_t ubNumFan, uint8_t ubDC);

		/*!
		 * @brief	writeTachLimitMin	Exceeding any of the tach min limit registers shown by 1
		 *								indicates that the fan is running too slowly or has stalled.
		 *								The appropriate status bit is set in Interrupt Status Register
		 *								2 to indicate the fan failure
		 * @param	ubTach				[0-3]
		 * @param	usLimitMin			[0-0xFFFF]
		 * @return	0 if success | -1 otherwise
		 */
		int writeTachLimitMin(uint8_t ubTach, uint16_t usLimitMin);

		/*!
		 * @brief	writeTachLimitMax	Exceeding any of the tach max limit registers shown by 1
		 *								indicates that the fan is running too slowly or has stalled.
		 *								The appropriate status bit is set in Interrupt Status Register
		 *								2 to indicate the fan failure
		 * @param	ubTach				[0-3]
		 * @param	usLimitMax			[0-0xFFFF]
		 * @return	0 if success | -1 otherwise
		 */
		int writeTachLimitMax(uint8_t ubTach, uint16_t usLimitMax);

		/*!****************************************************************************************
		 * READ operations
		 ******************************************************************************************/
		/*!
		 * @brief	readDeviceIDRegister
		 * @param	ubDeviceID
		 * @return	0 if success | -1 otherwise
		 */
		int readDeviceIDRegister(uint8_t &ubDeviceID);

		/*!
		 * @brief	readCompanyIDNumber
		 * @param	ubCompanyID
		 * @return	0 if success | -1 otherwise
		 */
		int readCompanyIDNumber(uint8_t &ubCompanyID);

		/*!
		 * @brief	readRevisionNumber
		 * @param	ubRevision
		 * @return	0 if success | -1 otherwise
		 */
		int readRevisionNumber(uint8_t &ubRevision);

		/*!
		 * @brief	readTemp
		 * @param	ubNumTemp	[0-9]
		 * @param	ubTemp
		 * @return	0 if success | -1 otherwise
		 */
		int readTemp(uint8_t ubNumTemp, uint8_t &ubTemp);

		/*!
		 * @brief	readFan
		 * @param	ubNumFan	[0-3]
		 * @param	usTach
		 * @return	0 if success | -1 otherwise
		 */
		int readFan(uint8_t ubNumFan, uint16_t &usTach);

		/*!
		 * @brief	readCurrentDutyCycle
		 * @param	ubNumPWM	[0-3]
		 * @param	ubCurrDC
		 * @return	0 if success | -1 otherwise
		 */
		int readCurrentDutyCycle(uint8_t ubNumPWM, uint8_t &ubCurrDC);

		/*!
		 * @brief	readMaxDutycycle
		 * @param	ubNumPWM
         * @param	ubMaxDC
		 * @return	0 if success | -1 otherwise
		 */
		int readMaxDutyCycle(uint8_t ubNumPWM, uint8_t &ubMaxDC);

		/*!
		 * @brief readMinDutyCycle
		 * @param ubNumPWM	[0-3]
		 * @param ubMinDC
		 * @return 0 if success | -1 otherwise
		 */
		int readMinDutyCycle(uint8_t ubNumPWM, uint8_t &ubMinDC);

		/*!
		 * @brief	readTemperatureLimit
		 * @param	ubNumTemp
		 * @param	ubLowLimit
		 * @return	0 if success | -1 otherwise
		 */
		int readTemperatureLowLimit(uint8_t ubNumTemp, uint8_t &ubLowLimit);

		/*!
		 * @brief	readTemperatureLimit
		 * @param	ubNumTemp
		 * @param	ubHighLimit
		 * @return	0 if success | -1 otherwise
		 */
		int readTemperatureHighLimit(uint8_t ubNumTemp, uint8_t &ubHighLimit);

		/*!
		 * @brief	readFanLimit
		 * @param	ubNumFan
         * @param	usLowLimit
		 * @return	0 if success | -1 otherwise
		 */
		int readFanLowLimit(uint8_t ubNumFan, uint16_t &usLowLimit);

		/*!
		 * @brief	readFanLimit
		 * @param	ubNumFan
         * @param	usHighLimit
		 * @return	0 if success | -1 otherwise
		 */
		int readFanHighLimit(uint8_t ubNumFan, uint16_t &usHighLimit);

		/*!
		 * @brief	readTMin
		 * @param	ubNumFan
		 * @param	ubTmin
		 * @return	0 if success | -1 otherwise
		 */
		int readTMin(uint8_t ubNumFan, uint8_t &ubTmin);

		/*!
		 * @brief	ADT7470::readItrStatusRegister1
		 * @param	ubItrReg
		 * @return	0 if success | -1 otherwise
		 */
		int readItrStatusRegister1(uint8_t &ubItrReg);

		/*!
		 * @brief	ADT7470::readItrStatusRegister2
		 * @param	ubItrReg
		 * @return	0 if success | -1 otherwise
		 */
		int readItrStatusRegister2(uint8_t &ubItrReg);

//	private:

		/*!****************************************************************************************
		 * WRITE operations
		 ******************************************************************************************/
		/*!
		 * @brief	writeConfigRegister1	It writes the device config 1 registers
		 *			[0] STRT			1-enables monitoring and PWM control outputs based on the limit settings
		 *								0-disables monitoring and PWM control
		 * 			[1] reserved
		 * 			[2] reserved
		 *  		[3] TODIS			1-disables SMBus timeout.
		 *  		[4] LOCK			1-all lockable registers become read-only
		 *  		[5] FST_TCH			0-Tach measurement rate is 1 measurement per second
		 *								1-Tach measurement rate is 1 measurement every 250ms
		 *  		[6] HF_LF			0 (default) - high frequency fan drive
		 *								1 - low frequency fandrive
		 *  		[7] T05_STB			0 (default) =FULL SPEED input.
		 *								1 = TMP05 start pulse output.
		 * @param	ubReg
		 * @return	0 if success | -1 otherwise
		 */
		int writeConfigRegister1(uint8_t ubReg);

		/*!
		 * @brief	writeConfigRegister2	It writes the device config 1 registers
		 *			[7] SHDN				Shutdown/low current mode.
		 *			[6:4] FREQ				These bits control PWM1–PWM4 frequency when the fan drive is configured as a low frequency drive.
		 *			[3] T4_dis				Writing a 1 disables Tach 4 measurements.
		 *			[2] T3_dis				Writing a 1 disables Tach 3 measurements.
		 *			[1] T2_dis				Writing a 1 disables Tach 2 measurements.
		 *			[0] T1_dis				Writing a 1 disables Tach 1 measurements.
		 * @param	ubReg
		 * @return	0 if success | -1 otherwise
		 */
		int writeConfigRegister2(uint8_t ubReg);

		/*!
		 * @brief	writeFanPulsesRevolution	Sets the number of pulses to be counted when measuring Fan Speed. It is possible the
		 *										setting up to 4 FAN.
		 * @param	ubPulses
		 * @return	0 if success | -1 otherwise
		 */
		int writeFanPulsesRevolution(uint8_t ubPulses);

		/*!
		 * @brief writePWM12ConfigurationRegister Sets the value of the PWM1 PWM2 configurationregister.
		 *			[7] BHVR1				This bit assigns fan behavior for PWM1 output(0 manual,1 automatic).
		 *			[6] BHVR2				This bit assigns fan behavior for PWM2 output(0 manual,1 automatic).
		 *			[5] INV1				Setting this bit to 1 inverts the PWM1 output. Default = 0.
		 * 			[4] INV2				Setting this bit to 1 inverts the PWM2 output. Default = 0.
		 *			[3]						Set to 0 (default).
		 *			[2]						Set to 0 (default).
		 *			[1]						Set to 0 (default).
		 *			[0]						Set to 0 (default).
		 *
		 * @param ubValue
		 * @return 0 if success | -1 otherwise
		 */
		int writePWM12ConfigurationRegister(uint8_t ubValue);

		/*!
		 * @brief writePWM12ConfigurationRegister Sets the value of the PWM1 PWM2 configurationregister.
		 *			[7] BHVR3				This bit assigns fan behavior for PWM3 output(0 manual,1 automatic).
		 *			[6] BHVR4				This bit assigns fan behavior for PWM4 output(0 manual,1 automatic).
		 *			[5] INV3				Setting this bit to 1 inverts the PWM3 output. Default = 0.
		 * 			[4] INV4				Setting this bit to 1 inverts the PWM4 output. Default = 0.
		 *			[3]						Set to 0 (default).
		 *			[2]						Set to 0 (default).
		 *			[1]						Set to 0 (default).
		 *			[0]						Set to 0 (default).
		 *
		 * @param ubValue
		 * @return 0 if success | -1 otherwise
		 */
		int writePWM34ConfigurationRegister(uint8_t ubValue);

		/*!
		 * @brief	writeItrMaskReg1		It allows individual interrupt sources to be masked
		 *									out to prevent unwanted SMBALERT interrupts of ITR status register 1.
		 *
         * @param	ubMaskReg
		 * @return	0 if success | -1 otherwise
		 */
		int writeItrMaskReg1(uint8_t ubMaskReg);

		/*!
		 * @brief	writeItrMaskReg2		It allows individual interrupt sources to be masked
		 *									out to prevent unwanted SMBALERT interrupts of ITR status register 2.
		 *
         * @param	ubMaskReg
		 * @return	0 if success | -1 otherwise
		 */
		int writeItrMaskReg2(uint8_t ubMaskReg);

		/*!****************************************************************************************
		 * READ operations
		 ******************************************************************************************/
		/*!
		 * @brief	readConfigRegister1
		 * @param	ubConfigRegister
		 * @return	0 if success | -1 otherwise
		 */
		int readConfigRegister1(uint8_t &ubConfigRegister);
		/*!
		 * @brief	readConfigRegister2
		 * @param	ubConfigRegister
		 * @return	0 if success | -1 otherwise
		 */
		int readConfigRegister2(uint8_t &ubConfigRegister);

		/*!
		 * @brief	readFanPulsesRevolution
		 * @param	ubPulses
		 * @return	0 if success | -1 otherwise
		 */
		int readFanPulsesRevolution(uint8_t &ubPulses);

		/*!
		 * @brief	readItrMaskReg1
		 * @param	ubMaskReg
		 * @return	0 if success | -1 otherwise
		 */
		int readItrMaskReg1(uint8_t &ubMaskReg);

		/*!
		 * @brief	readItrMaskReg2
		 * @param	ubMaskReg
		 * @return	0 if success | -1 otherwise
		 */
		int readItrMaskReg2(uint8_t &ubMaskReg);
};

#endif // ADT7470_H
