/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    I2CLinkBoard.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the I2CLinkBoard class.
 @details

 ****************************************************************************
*/

#include "CommonInclude.h"

#include "I2CLinkBoard.h"
#include "UCD9090a.h"
#include "PCA9633.h"
#include "PCA9955B.h"
#include "ADT7470.h"
#include "AT24C256C.h"
#include "TLV320aic31xx.h"
#include "PCA9537.h"

#include "Log.h"

#define RESET_LINE_LOW_SECTION	1
#define RESET_LINE_HIGH_SECTION 0

I2CLinkBoard::I2CLinkBoard()
{

	m_pPowerManager		= 0;
	m_pGpioExpanderA	= 0;
	m_pLedDriver		= 0;
	m_pFanController	= 0;
	m_pEeprom			= 0;
	m_pCodecAudio		= 0;
	m_pGpioExpanderB	= 0;
}

I2CLinkBoard::~I2CLinkBoard()
{
	SAFE_DELETE(m_pPowerManager);
	SAFE_DELETE(m_pGpioExpanderA);
	SAFE_DELETE(m_pGpioExpanderB);
	SAFE_DELETE(m_pLedDriver);
	SAFE_DELETE(m_pFanController);
	SAFE_DELETE(m_pEeprom);
	SAFE_DELETE(m_pCodecAudio);
	SAFE_DELETE(m_pGpioExpanderLed);
}

int I2CLinkBoard::createI2CInterfaces(void)
{
	m_I2CIfrCollector.setLogger(getLogger());
	int ret = m_I2CIfrCollector.createInterfaces();
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createI2CInterfaces: unable to create I2C interfaces");
		return -1;
	}	
	return 0;
}

void I2CLinkBoard::destroyI2CInterfaces(void)
{
	m_I2CIfrCollector.destroyInterfaces();
}

int I2CLinkBoard::createAndInitPowerManager(void)
{
	SAFE_DELETE(m_pPowerManager);
	m_pPowerManager = new UCD9090a();
	if ( m_pPowerManager == 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitPowerManager: unable to create UCD9090a, memory error");
		return -1;
	}

	int ret = m_pPowerManager->init(getLogger(), m_I2CIfrCollector.getInterfaceByIndex(eI2C_3));
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitPowerManager: unable to initialize PowerManager");
		return -1;
	}

	ret = m_pPowerManager->setDeviceAddress(I2C_TEST_UCD9090_DEVICE_ADDRESS);
	if ( ret != 0 )
	{
        log(LOG_ERR, "I2CLinkBoard::createAndInitPowerManager: unable to set PowerManager I2C address");
		return -1;
	}

    ret = m_pPowerManager->writeClearFaults();
    if ( ret != 0 )
    {
        log(LOG_ERR, "I2CLinkBoard::createAndInitPowerManager: unable to clear PowerManager I2C errors");
        return -1;
    }

    ret = m_pPowerManager->reloadButton();
    if ( ret != 0 )
    {
        log(LOG_ERR, "I2CLinkBoard::createAndInitPowerManager: unable to reload BUTTON");
        return -1;
    }


	return 0;
}

int I2CLinkBoard::destroyPowerManager()
{
	SAFE_DELETE(m_pPowerManager);
	return 0;
}

int I2CLinkBoard::createAndInitGpioExpanderA(void)
{
	//GPIO EXPANDER SECTION A
	SAFE_DELETE(m_pGpioExpanderA);
	m_pGpioExpanderA = new PCA9633();
	if (m_pGpioExpanderA == 0)
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderA: unable to create PCA9633, memory error");
		return -1;
	}

	int ret = m_pGpioExpanderA->init(getLogger(), m_I2CIfrCollector.getInterfaceByIndex(eI2C_2));
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderA: unable to initialize GPIOExpander");
		return -1;
	}

    // setting of m_pGpioExpander device address is related to section board and configurations
	uint8_t ubGpioExpA = (~CAN_SECTION_A_BOARD_ID_TX) & 0x001F;

	log(LOG_INFO, "I2CLinkBoard::createAndInitGpioExpanderA: set address GpioExpA: <0x%x>",ubGpioExpA);
	ret = m_pGpioExpanderA->setDeviceAddress(ubGpioExpA);
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderA: unable to set GPIOExpander I2C address");
		return -1;
	}

	ret = m_pGpioExpanderA->config();
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderA: unable to config GPIOExpander");
		return -1;
	}

	return 0;
}

int I2CLinkBoard::createAndInitGpioExpanderB(void)
{
	//GPIO EXPANDER SECTION B
	SAFE_DELETE(m_pGpioExpanderB);
	m_pGpioExpanderB = new PCA9633();
	if (m_pGpioExpanderB == 0)
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderB: unable to create PCA9633, memory error");
		return -1;
	}

	int ret = m_pGpioExpanderB->init(getLogger(), m_I2CIfrCollector.getInterfaceByIndex(eI2C_2));
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderB: unable to initialize GPIOExpander");
		return -1;
	}

    // setting of m_pGpioExpander device address is related to section board and configurations
	uint8_t ubGpioExpB = (~CAN_SECTION_B_BOARD_ID_TX) & 0x001F;

	log(LOG_INFO, "I2CLinkBoard::createAndInitGpioExpanderB: set address GpioExpB: <0x%x>",ubGpioExpB);
	ret = m_pGpioExpanderB->setDeviceAddress(ubGpioExpB);
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderB: unable to set GPIOExpander I2C address");
		return -1;
	}

	ret = m_pGpioExpanderB->config();
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderB: unable to config GPIOExpander");
		return -1;
	}

	return 0;
}

int I2CLinkBoard::destroyGpioExpanderA(void)
{
	SAFE_DELETE(m_pGpioExpanderA);
	return 0;
}

int I2CLinkBoard::destroyGpioExpanderB(void)
{
	SAFE_DELETE(m_pGpioExpanderB);
	return 0;
}

int I2CLinkBoard::resetSectionBoard(uint8_t section)
{
    int res = 2;

    if(m_pGpioExpanderA && section == SCT_A_ID)
    {
        res = m_pGpioExpanderA->writeLedOut(RESET_LINE_LOW_SECTION);
        if (res != 0)
        {
            res = -1;
        }
        else
        {
            msleep(500);
            res = m_pGpioExpanderA->writeLedOut(RESET_LINE_HIGH_SECTION);
        }
    }
    else if(m_pGpioExpanderB && section == SCT_B_ID)
    {
        res = m_pGpioExpanderB->writeLedOut(RESET_LINE_LOW_SECTION);
        if (res != 0)
        {
            res = -1;
        }
        else
        {
            msleep(500);
            res = m_pGpioExpanderB->writeLedOut(RESET_LINE_HIGH_SECTION);
        }
    }

    msleep(500);
    return res;
}

int I2CLinkBoard::createLedDriver(bool bExpLedEnabled)
{
	SAFE_DELETE(m_pLedDriver);
	m_pLedDriver = new PCA9955b();
	if (m_pLedDriver == 0)
	{
        log(LOG_ERR, "I2CLinkBoard::createLedDriver: unable to create PCALED, memory error");
		return -1;
	}

	//Prova LED
	int ret = m_pLedDriver->init(getLogger(), m_I2CIfrCollector.getInterfaceByIndex(eI2C_2));
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createLedDriver: unable to initialize LedDriver");
        SAFE_DELETE(m_pLedDriver);
        return -1;
	}


    log(LOG_INFO, "I2CLinkBoard::createLedDriver: set address LedDriver address: <0x%x>",I2C_TEST_PCALED_DEVICE_ADDRESS);
    ret = m_pLedDriver->setDeviceAddress(I2C_TEST_PCALED_DEVICE_ADDRESS);
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createLedDriver: unable to set LedDriver I2C address");
        SAFE_DELETE(m_pLedDriver);
        return -1;
	}

	ret = m_pLedDriver->resetSoft();
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createLedDriver: unable to reset LedDriver");
        SAFE_DELETE(m_pLedDriver);
        return -1;
	}

    ret = m_pLedDriver->config(PCALED_MODE2_BLNK_CTL);
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createLedDriver: unable to config LedDriver	");
        SAFE_DELETE(m_pLedDriver);
        return -1;
	}

    ret = m_pLedDriver->writeIrefAll(40);
    if ( ret != 0 )
    {
        log(LOG_ERR, "I2CLinkBoard::createLedDriver: unable to config IrefAll");
        SAFE_DELETE(m_pLedDriver);
        return -1;
    }

    ret = m_pLedDriver->clearLedOut();
    if ( ret != 0 )
    {
        log(LOG_ERR, "I2CLinkBoard::createLedDriver: unable to clear Leds");
        SAFE_DELETE(m_pLedDriver);
        return -1;
    }

    if (bExpLedEnabled == false)
    {
        //In case of STATUS LED board without GPIO EXPANDER
        //Set the Iref with the minimum value because with high R and high Iref the chip could read the the pin as open circuit
        ret = m_pLedDriver->writeIref(OUTPUT_LED_DIS_OSC, 1);
        if ( ret != 0 )
        {
            log(LOG_ERR, "I2CLinkBoard::createLedDriver: unable to config Iref LED9");
            SAFE_DELETE(m_pLedDriver);
            return -1;
        }

        ret = m_pLedDriver->writeLedOut(OUTPUT_LED_DIS_OSC , 1);
        if ( ret != 0 )
        {
            log(LOG_ERR, "I2CLinkBoard::createLedDriver: unable to config Iref LED9");
            SAFE_DELETE(m_pLedDriver);
            return -1;
        }
    }

//    m_pLedDriver->writeIref(OUTPUT_LED_GREEN, 25);
    m_pLedDriver->writePWM(OUTPUT_LED_GREEN, 50);
    m_pLedDriver->writeGrpPWM(50);

//    m_pLedDriver->writeIref(OUTPUT_LED_ORANGE, 25);
    m_pLedDriver->writePWM(OUTPUT_LED_ORANGE, 50);
    m_pLedDriver->writeGrpPWM(50);

//    m_pLedDriver->writeIref(OUTPUT_LED_RED, 25);
    m_pLedDriver->writePWM(OUTPUT_LED_RED, 50);
    m_pLedDriver->writeGrpPWM(50);

    uint8_t ubPeriod = 0;
    ubPeriod = (15.26 * BLK_TIME_LED) - 1;
    m_pLedDriver->writeGrpFreq(ubPeriod);

    // reset any pending error
    ret = m_pLedDriver->clearErrorMode2() ;
    if ( ret != 0 )
    {
        log(LOG_ERR, "I2CLinkBoard::createLedDriver: unable to clear Error");
        return -1;
    }

	return 0;
}

int I2CLinkBoard::destroyLedDriver()
{
	SAFE_DELETE(m_pLedDriver);
	return 0;
}

int I2CLinkBoard::createAndInitFanController(void)
{
	SAFE_DELETE(m_pFanController);
	m_pFanController = new ADT7470();
	if (m_pFanController == 0)
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitFanController: unable to create ADT7470, memory error");
		return -1;
	}

	int ret = m_pFanController->init(getLogger(), m_I2CIfrCollector.getInterfaceByIndex(eI2C_3));
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitFanController: unable to initialize FanController");
		return -1;
	}

	log(LOG_INFO, "I2CLinkBoard::createAndInitFanController: set address FanController address: <0x%x>",I2C_TEST_ADT7470_DEVICE_ADDRESS);
	ret = m_pFanController->setDeviceAddress(I2C_TEST_ADT7470_DEVICE_ADDRESS);
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitFanController: unable to set FanController I2C address");
		return -1;
	}

	ret = m_pFanController->initConfigRegister();
	if ( ret != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitFanController: unable to config FanController");
		return -1;
	}

    ret = m_pFanController->startFanController();
    if ( ret != 0 )
    {
        log(LOG_ERR, "I2CLinkBoard::createAndInitFanController: unable to start FanController");
        return -1;
    }

	return 0;
}

int I2CLinkBoard::destroyFanController(void)
{
    m_pFanController->stopFanController();
    SAFE_DELETE(m_pFanController);

	return 0;
}

int I2CLinkBoard::createAndInitEEprom(void)
{
	SAFE_DELETE(m_pEeprom);

	m_pEeprom = new AT24C256C();
	if ( m_pEeprom == 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitEEprom: unable to create EEprom, memory error");
		return -1;
	}

	int liRet = m_pEeprom->init(getLogger(), m_I2CIfrCollector.getInterfaceByIndex(eI2C_3));
	if ( liRet != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitEEprom: unable to initialize EEprom");
		return -1;
	}

	liRet = m_pEeprom->setDeviceAddress(I2C_TEST_AT24C256C_DEVICE_ADDRESS);
	if ( liRet != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitEEprom: unable to set m_pEeprom I2C address");
		return -1;
	}

	return 0;
}

int I2CLinkBoard::destroyEeprom(void)
{
	SAFE_DELETE(m_pEeprom);
	return 0;
}

int I2CLinkBoard::createAndInitCodecAudio(void)
{
	SAFE_DELETE(m_pCodecAudio);

	m_pCodecAudio = new TLV320AIC31xx();
	if (m_pCodecAudio == 0)
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitCodecAudio: unable to create codec Audio, memory error");
		return -1;
	}

    int liRet = m_pCodecAudio->init(getLogger(), m_I2CIfrCollector.getInterfaceByIndex(eI2C_2));
	if ( liRet != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitCodecAudio: unable to initialize codec Audio");
		return -1;
	}

	liRet = m_pCodecAudio->setDeviceAddress(0x18);
	if ( liRet != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitCodecAudio: unable to set codecAudio I2C address");
		return -1;
	}

	liRet = m_pCodecAudio->setCardName((uint8_t*)"hw:0");
	if ( liRet != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitCodecAudio: unable to set codecAudio card name");
		return -1;
	}

	//TODO
	// Add configurations about volume and so on...

	return 0;
}

int I2CLinkBoard::destroyCodecAudio(void)
{
	SAFE_DELETE(m_pCodecAudio);
	return 0;
}

int I2CLinkBoard::createAndInitGpioExpanderLed(void)
{
	SAFE_DELETE(m_pGpioExpanderLed);
	m_pGpioExpanderLed = new PCA9537();
	if ( m_pGpioExpanderLed == 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderLed: unable to create GPIOEXP LED");
		return -1;
	}

    int liRet = m_pGpioExpanderLed->init(getLogger(), m_I2CIfrCollector.getInterfaceByIndex(eI2C_2));
	if ( liRet != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderLed: unable to initialize GPIOEXP LED");
		return -1;
	}

	liRet = m_pGpioExpanderLed->setDeviceAddress(I2C_TEST_PCA9537_DEVICE_ADDRESS);
	if ( liRet != 0 )
	{
		log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderLed: unable to set GPIOEXP LED address");
		return -1;
	}

    // Disable oscillator
    liRet =m_pGpioExpanderLed->writeConfigurationRegister(0xF0);
    if ( liRet != 0 )
    {
        log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderLed: unable to set oconfig reg");
        return -1;
    }
    liRet =m_pGpioExpanderLed->writeOutputPortRegister(0xF8);
    if ( liRet != 0 )
    {
        log(LOG_ERR, "I2CLinkBoard::createAndInitGpioExpanderLed: unable to set output port reg");
        return -1;
    }

	log(LOG_INFO, "I2CLinkBoard::createLedDriver: set address LedDriver address: <0x%x>",I2C_TEST_PCA9537_DEVICE_ADDRESS);

	return 0;
}

int I2CLinkBoard::destroyGpioExpanderLed(void)
{
	SAFE_DELETE(m_pGpioExpanderLed);
	return 0;
}
