/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderProtocol.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the NSHReaderProtocol class.
 @details

 ****************************************************************************
*/

#include "NSHReaderProtocol.h"

NSHReaderProtocol::NSHReaderProtocol()
{
	m_bConfigOk = false;
	m_strReadPkt = "@000";	// command used to read from NSH - "Read in progress"
	m_ucCmdIdToBeSent = 0;

	union
	{
		uint32_t uliTestValue;
		uint8_t ucBuff[4];
	} endiannessTest;

	endiannessTest.uliTestValue = {0x01020304};

	if ( endiannessTest.ucBuff[0] == 4 )
	{
		m_eEndianness = eLittleEndian;
	}
	else
	{
		m_eEndianness = eBigEndian;
	}

	m_pNSHError = nullptr;
}


NSHReaderProtocol::~NSHReaderProtocol()
{
}


int8_t NSHReaderProtocol::sendNSHMessage(int32_t& cErrCode, eSPIProtoType typeProt, uint32_t uliTimeOutMsec)
{
	if ( m_ucCmdIdToBeSent == 0 )	return -1;

	uint8_t rgcRxBuff[8] = {0};
	eNSHProtocolErr	eNSHMsgRcv;

	/*!* Header transmission ***/
	if ( sendSingleMsgSPIProt(m_NSHMsgStructTx.ucCmdBlock, rgcRxBuff, uliTimeOutMsec) )	return -1;
	eNSHMsgRcv = checkErrSPIMsgReceived(rgcRxBuff, cErrCode, typeProt);

	if ( eNSHMsgRcv != eNSHProtNoError)	return -2;

	/*!* Payload transmission ***/
	for (int8_t i = 0; i< m_NSHMsgStructTx.ucPayloadPacketNum; i++)
	{
		if ( sendSingleMsgSPIProt(&m_NSHMsgStructTx.ucPayloadBlock[i*NSH_READER_SPI_PKT_SIZE], rgcRxBuff, uliTimeOutMsec) )	return -1;
		eNSHMsgRcv = checkErrSPIMsgReceived(rgcRxBuff, cErrCode, typeProt);
		
		if ( eNSHMsgRcv != eNSHProtNoError)	return -2;
	}

	/*!* Checksum trasmission ***/
	if ( sendSingleMsgSPIProt(m_NSHMsgStructTx.ucChecksumBlock, rgcRxBuff, uliTimeOutMsec) )	return -1;
	eNSHMsgRcv = checkErrSPIMsgReceived(rgcRxBuff, cErrCode, typeProt);
	
	if ( eNSHMsgRcv != eNSHProtNoError)	return -2;

	return 0;
}


int8_t NSHReaderProtocol::sendSingleMsgSPIProt(uint8_t* pTxBuffer, uint8_t* pRxBuffer, uint32_t uliTimeOutMsec)
{
	if ( pTxBuffer == NULL )	return -1;
	if ( pRxBuffer == NULL )	return -1;

	if ( checkDRDYstatusWithTimeout(uliTimeOutMsec) < 0 )
	{
		m_pLogger->log(LOG_ERR, "NSHReaderProtocol::handleCompleteTransfer: timeout elapsed in Header transmission, DSP still busy.");
		return -2;
	}

	/*! The scanner head has big endianness. **/
	if ( m_eEndianness == eLittleEndian )
	{
		littleToBigEndian32(pTxBuffer);
	}

	if ( !m_pSPIIfr->singleTransfer(pTxBuffer, pRxBuffer, NSH_READER_SPI_PKT_SIZE) )
	{
		m_pLogger->log(LOG_ERR, "NSHReaderProtocol::sendSingleMsgSPIProt: error in transferring data.");
		return -3;
	}

	/*! The scanner head has big endianness. **/
	if ( m_eEndianness == eLittleEndian )
	{
		littleToBigEndian32(pTxBuffer);
		littleToBigEndian32(pRxBuffer);
	}

	return 0;
}


int8_t NSHReaderProtocol::receiveNSHResponse(string& strResponse, int32_t& cErrCode, eSPIProtoType typeProt, uint32_t uliTimeOutMsec)
{
	bool bRxLoop = true;
	uint8_t rgcRxBuff[8] = {0};
	uint8_t ucDmyPktNum = 0;
	int8_t  cResult = 0;
	eNSHProtocolErr	eNSHMsgRcv;

	if ( typeProt == eSPIAscii )
	{
		uint8_t ucDataPktNum = 0;
		uint8_t rgcChecksumBuf[NSH_READER_SPI_PKT_SIZE] = {0};

		while ( bRxLoop )
		{
			if ( sendSingleMsgSPIProt((uint8_t*)m_strReadPkt.c_str(), rgcRxBuff, uliTimeOutMsec) )
			{
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::receiveNSHResponse: Error sending Reading cmd.");
				cResult = -1;
				break;
			}

			eNSHMsgRcv = checkErrSPIMsgReceived(rgcRxBuff, cErrCode, typeProt);
			if ( eNSHMsgRcv != eNSHProtNoError)
			{
				cResult = -2;
				break;
			}

			switch( rgcRxBuff[NSH_READER_SPI_PKT_HEAD_IDX] )
			{
				case eSPIDmyPktId:

					/* Only one dummy packet is allowed */
					if ( ucDmyPktNum == 0 )
					{
						ucDmyPktNum++;
					}
					else
					{
						m_pLogger->log(LOG_ERR, "NSHReaderProtocol::receiveNSHResponse: DUMMY!");
						bRxLoop = false;
						cResult = -3;
					}
				break;

				case eSPISinglePktId:

					/* Implemented for debug purpose only. At present it is not processed. */
					bRxLoop = false;
					cResult = 0;
				break;

				case eSPIFirstPktID:

					/* Command packet. Starts processing the incoming stream */
					memcpy(m_NSHMsgStructRx.ucCmdBlock, rgcRxBuff, NSH_READER_SPI_PKT_SIZE);
					cResult = 0;
				break;

				case eSPIChkPktId:

					/* Check checksum correctness for packets loss */
					computeChecksum(&m_NSHMsgStructRx, rgcChecksumBuf);

					for (int i = 0; i < NSH_READER_SPI_PKT_SIZE; i++)
					{
						if ( rgcChecksumBuf[i] != rgcRxBuff[i] )
						{
							m_pLogger->log(LOG_ERR, "NSHReaderProtocol::receiveNSHResponse: Wrong Checksum received.");
							cResult = -3;
							break;
						}
					}

					memcpy(m_NSHMsgStructRx.ucChecksumBlock, rgcRxBuff, NSH_READER_SPI_PKT_SIZE);
					cResult = parseSubUnitAnswer(strResponse);

					if ( cResult != eNSHProtNoError )
					{
						m_pLogger->log(LOG_ERR, "NSHReaderProtocol::receiveNSHResponse: Error in parsing answer.");
					}

					bRxLoop = false;

				break;

				default:

					if ( (rgcRxBuff[NSH_READER_SPI_PKT_HEAD_IDX] >= NSH_READER_SPI_PKT_A) && (rgcRxBuff[NSH_READER_SPI_PKT_HEAD_IDX] <= NSH_READER_SPI_PKT_Z) )
					{
						if ( ucDataPktNum == 0 )
						{
							m_NSHMsgStructRx.ucPayloadPacketNum = rgcRxBuff[NSH_READER_SPI_PKT_HEAD_IDX] - NSH_READER_SPI_PKT_A + 1;
							ucDataPktNum = rgcRxBuff[NSH_READER_SPI_PKT_HEAD_IDX];
						}
						else
						{
							ucDataPktNum--;
						}

						if ( rgcRxBuff[NSH_READER_SPI_PKT_HEAD_IDX] != ucDataPktNum )
						{
							// This means (at least) a packet is lost.
							bRxLoop = false;
							m_pLogger->log(LOG_ERR, "NSHReaderProtocol::receiveNSHResponse: missing packet rx.");
							cResult = -3;
						}

						int16_t siTmp = m_NSHMsgStructRx.ucPayloadPacketNum - ucDataPktNum + NSH_READER_SPI_PKT_A -1;
						memcpy(&m_NSHMsgStructRx.ucPayloadBlock[4*siTmp], rgcRxBuff, NSH_READER_SPI_PKT_SIZE);
					}
					else
					{
						m_pLogger->log(LOG_INFO, "NSHReaderProtocol::receiveNSHResponse: key not recognised.");
						bRxLoop = false;
						cResult = -3;
					}
				break;
			}
		}
	}
	cErrCode = cResult;
	return cResult;
}


eNSHProtocolErr NSHReaderProtocol::checkErrSPIMsgReceived(uint8_t* pRxBuffer, int32_t& liErrCode, eSPIProtoType typeProt)
{
	if ( pRxBuffer == NULL ) return eNSHParamError;

	string strErr;
	liErrCode = 0;

	if  ( ( typeProt == eSPIAscii ) && ( pRxBuffer[0] != eSPIErrPktId ) )	return eNSHProtNoError;

	if ( typeProt == eSPIbin )
	{
		if (( pRxBuffer[0] != eSPIEndBinPktId ) &&
			( pRxBuffer[0] != eSPIErrBinPktId ) &&
			( pRxBuffer[0] != eSPIErrBinProtId ) )
		{
			return eNSHProtNoError;
		}
		else if ( pRxBuffer[0] == eSPIEndBinPktId )
		{
			liErrCode = eSPINextPktCRC;
			return eNSHProtNoError;
		}

		m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Error on msg received with extended protocol.\n", eNSHBinProtErrCnt);
	}

	uint8_t ucStart = (pRxBuffer[NSH_READER_SPI_PKT_HEAD_LEN] == '0') ? 1 : 0;

	for (int8_t i = ucStart; i < NSH_READER_SPI_PKT_DATA_LEN; i++)
	{
		strErr.push_back(pRxBuffer[i + NSH_READER_SPI_PKT_HEAD_LEN]);
	}

	stringstream strMsg;
	strMsg << strErr;
	strMsg >> std::hex >> liErrCode;
	// for binary prot the error is the packet counter,
	// for ascii prot the error is the code error	
	if ( typeProt == eSPIAscii)
	{
		m_pNSHError->decodeNotifyEvent(liErrCode); // controlla se l'errore è tra quelli noti e chiama vidasep

		// TODO... this has to be removed
		switch (liErrCode)
		{
			case eNSHErrStreamLen:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Wrong stream length.\n", liErrCode);
			break;

			case eNSHErrUnknownCmd:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Unknown command.\n", liErrCode);
			break;

			case eNSHErrBadParams:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Bad command parameters.\n", liErrCode);
			break;

			case eNSHErrStrFormat:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Wrong stream format.\n", liErrCode);
			break;

			case eNSHErrCheckSum:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Checksum error.\n", liErrCode);
			break;

			case eNSHErrADCConvTimeout:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. ADC conversion timeout.\n", liErrCode);
			break;

			case eNSHErrLedOutOfRange:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Led out of range.\n", liErrCode);
			break;

			case eNSHErrRefOutOfRange:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Reference out of range.\n", liErrCode);
			break;

			case eNSHErrFluoOutOfRange:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Fluorescence out of range.\n", liErrCode);
			break;

			case eNSHErrPreAmpADOutOfTemp:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Preamp A/D out of range.\n", liErrCode);
			break;

			case eNSHErrOutOfAirWarning:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Out of air warning limit.\n", liErrCode);
			break;

			case eNSHErrOutOfAirSupLimit:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Out of air superior error limit.\n", liErrCode);
			break;

			case eNSHErrOutOfAirInfLimit:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Out of air inferior error limit.\n", liErrCode);
			break;

			case eNSHErrSolidStdWarning:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Solid standard auto calibration warning.\n", liErrCode);
			break;

			case eNSHErrSolidStdAutoErr:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Auto calibration error.\n", liErrCode);
			break;

			case eNSHErrSolidCalibEEpromErr:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Calibration error or EEPROM error.\n", liErrCode);
			break;

			case eNSHErrFluoCalibErr:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Fluorescence calibration error.\n", liErrCode);
			break;

			case eNSHErrReferenceCalibErr:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Reference calibration error.\n", liErrCode);
			break;

			case eNSHErrExtFluCalibErr:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Ext ange flou calibration error.\n", liErrCode);
			break;

			case eNSHErrExtRefCalibErr:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Extended range ref calibration error.\n", liErrCode);
			break;

			case eNSHErrExtFluoRefErr:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Ext range flou/ref calibration error.\n", liErrCode);
			break;

			case eNSHErrAirExtRangeErr:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Air ext range calibration error.\n", liErrCode);
			break;

			case eNSHErrAirCalibErr:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Air calibration error.\n", liErrCode);
			break;

			case eNSHErrCalibSeqError:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Calibration sequence error.\n", liErrCode);
			break;

			case eNSHErrEEpromReadWrite:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. EEprom data read/write error.\n", liErrCode);
			break;

			case eNSHErrMonitorModeActive:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Monitor mode active.\n", liErrCode);
			break;

			case eNSHErrBarcodeRead:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error message received, Code[%d]. Barcode reade error.\n", liErrCode);
			break;

			default:
				m_pLogger->log(LOG_ERR, "NSHReaderProtocol::checkErrSPIMsgReceived: error not managed, Code[%d].\n", liErrCode);
			break;
		}
	}
	return eNSHProtError;
}

bool NSHReaderProtocol::setNSHErrors(SPIErrors* pNSHError)
{
	m_bConfigOk = false;
	if ( pNSHError == nullptr )	return false;

	m_pNSHError = pNSHError;
	m_bConfigOk = true;
	return true;
}

eMachineEndianness NSHReaderProtocol::getMachineEndiannes()
{
	return m_eEndianness;
}

void NSHReaderProtocol::littleToBigEndian32(uint8_t* pcBuffer)
{
	uint8_t ucFirst = pcBuffer[0];
	uint8_t ucSecond = pcBuffer[1];

	pcBuffer[0] = pcBuffer[3];
	pcBuffer[1] = pcBuffer[2];
	pcBuffer[2] = ucSecond;
	pcBuffer[3] =  ucFirst;
}
