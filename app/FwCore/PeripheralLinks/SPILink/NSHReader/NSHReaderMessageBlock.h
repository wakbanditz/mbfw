/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderMessageBlock.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the NSHReaderMessageBlock class.
 @details

 ****************************************************************************
*/

#ifndef NSHREADERMESSAGEBLOCK_H
#define NSHREADERMESSAGEBLOCK_H

#include <stdint.h>
#include <stdio.h>
#include <string>
#include <string.h>

#include "Loggable.h"
#include "NSHReaderCommonInclude.h"

using std::string;

static const char cLeftPktId[NSH_READER_SPI_PKT_MAX_NUM+1] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};

class NSHReaderMessageBlock : public Loggable
{
	public:

		/*! ***********************************************************************************************************
		 * @brief NSHReaderMessageBlock default constructor.
		 * ************************************************************************************************************
		 */
		NSHReaderMessageBlock();

		/*! ***********************************************************************************************************
		 * @brief ~NSHReaderMessageBlock virtual destructor.
		 * ************************************************************************************************************
		 */
		virtual ~NSHReaderMessageBlock();

		/*! ***********************************************************************************************************
		 * @brief  getLog used to get the logger.
         * @param  pLogger logger.
		 * @return -1 in case of success, 0 otherwise.
		 * ************************************************************************************************************
		 */
        int8_t setLog(Log* pLogger);

		/*! ***********************************************************************************************************
		 * @brief  buff2MsgBlocks converts a msg buffer into msg block of 4B each.
         * @param  pcBuff buffer containing the message.
         * @param  usBuffLength length of the message.
         * @param  typeProt protocol type
		 * @return -1 in case of success, 0 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t buff2MsgBlocks(const char* pcBuff, uint16_t usBuffLength, eSPIProtoType typeProt);

		/*! ***********************************************************************************************************
		 * @brief  msgBlocks2Buff takes the msg block (4B) and extracts the payload.
		 * @param  pcBuff pointer to the message block.
		 * @param  uliBuffLen reference lenght.
		 * @return -1 in case of success, 0 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t msgBlocks2Buff(uint8_t* pcBuff, uint32_t& uliBuffLen);

		/*! ***********************************************************************************************************
		 * @brief  computeChecksum compute vertical checksum.
         * @param pSPIMsgStruct message structure
         * @param  pCheckSumBuf pkt with the checksum value.
		 * @return -1 in case of success, 0 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t computeChecksum(structSPImsgBlock* pSPIMsgStruct, uint8_t* pCheckSumBuf);


		/*! ***********************************************************************************************************
		 * @brief reset clear structs.
		 * ************************************************************************************************************
		 */
		void reset(void);

    private:

        uint16_t	m_siPktNum;
        string		m_strBinMsgs;

    protected:

        structSPImsgBlock	m_NSHMsgStructTx;
        structSPImsgBlock	m_NSHMsgStructRx;

};

#endif // NSHREADERMESSAGEBLOCK_H
