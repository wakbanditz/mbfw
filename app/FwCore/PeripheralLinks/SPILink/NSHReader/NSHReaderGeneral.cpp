/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderGeneral.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the NSHReaderGeneral class.
 @details

 ****************************************************************************
*/

#include "NSHReaderGeneral.h"

#define NSH_TO_COMMA_FACTOR			100000000
#define NSH_HW_RESET_READY_STATE	'4'


NSHReaderGeneral::NSHReaderGeneral()
{
	m_nGoldenSS = 0;
}


NSHReaderGeneral::~NSHReaderGeneral()
{

}


bool NSHReaderGeneral::initNSH(Log* pLogger, SPIInterface* pSPIIfr, GpioInterface* pGpio, uint8_t ucDRDYGpioIdx, uint8_t ucResetGpioIdx)
{
	m_bConfigOk = false;

	if ( pLogger == NULL )			return false;
	if ( pSPIIfr == NULL )			return false;
	if ( pGpio == NULL )			return false;

	m_pLogger = pLogger;
	m_pSPIIfr = pSPIIfr;

	if ( initDRDY(pLogger, pGpio, ucDRDYGpioIdx, ucResetGpioIdx))	return false;

	m_bConfigOk = true;

	return true;
}


int8_t NSHReaderGeneral::getFwVersion(enumFwVersion eFwVersion, string& strFWversion, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )					return -1;
	if ( eFwVersion == eVersionNone )	return -1;

	char rgcCmd[8] = {0};
	int32_t liErrorCode;

	sprintf(rgcCmd, "gsw%C", eFwVersion);
	m_ucCmdIdToBeSent = eGetFirmwareId;

	buff2MsgBlocks(rgcCmd, NSH_READER_SPI_PKT_SIZE, eSPIAscii);
	if ( sendNSHMessage(liErrorCode, eSPIAscii, uliTimeOutMsec) )					return liErrorCode;
	if ( receiveNSHResponse(strFWversion, liErrorCode, eSPIAscii, uliTimeOutMsec) )	return liErrorCode;

	// Need to discard spaces.
	while ( strFWversion.back() == ' ' )
	{
		strFWversion.pop_back();
	}

	int16_t liFWVersion;
	stringstream strMsg;
	strMsg << strFWversion;
	strMsg >> std::hex >> liFWVersion;
    // write fw version correctly
    strFWversion = to_string(liFWVersion);
    for ( size_t i = 1; i < strFWversion.size(); i+=2 )
    {
        strFWversion.insert(i, sizeof(char), '.');
    }

	switch ( eFwVersion )
	{
		case eAppVersion:
            m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getFwVersion: firmware version is %s", strFWversion.c_str());
		break;

		case eBootVersion:
            m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getFwVersion: bootloader version is %s", strFWversion.c_str());
		break;

		case eSelectorVersion:
            m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getFwVersion: selector version is %s", strFWversion.c_str());
		break;

		case eBoardHwVersion:
			m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getFwVersion: board HW version version is %c.%c.%c", strFWversion[0], strFWversion[1], strFWversion[2]);
		break;

		default:
			m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getFwVersion: version not handled");
		break;

	}

	return 0;
}


int8_t NSHReaderGeneral::getNSHStatus(structNSHStatus* pNSHStatus, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )				return -1;
	if ( pNSHStatus == nullptr )	return -1;

	char rgcCmd[8] = "gst";
	int32_t liErrorCode;
	string strBoardStatus("");

	m_ucCmdIdToBeSent = eGetNSHStatusId;
	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liErrorCode, eSPIAscii, uliTimeOutMsec) )						return liErrorCode;
	if ( receiveNSHResponse(strBoardStatus, liErrorCode, eSPIAscii, uliTimeOutMsec) )	return liErrorCode;

	pNSHStatus->ucApplication	= strBoardStatus[0];
	pNSHStatus->ucBoot1			= strBoardStatus[1];
	pNSHStatus->ucBoot2			= strBoardStatus[2];
	pNSHStatus->ucEEprom		= strBoardStatus[3];
	pNSHStatus->ucRam			= strBoardStatus[4];
	pNSHStatus->ucCameraID		= strBoardStatus[5];
	pNSHStatus->ucSpare1		= strBoardStatus[6];
	pNSHStatus->ucSpare2		= strBoardStatus[7];
	pNSHStatus->ucStep			= strBoardStatus[8];

	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHReaderGeneral::getNSHStatus: Application: %c.", pNSHStatus->ucApplication);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHReaderGeneral::getNSHStatus: Boot1: %c.", pNSHStatus->ucBoot1);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHReaderGeneral::getNSHStatus: Boot2: %c.", pNSHStatus->ucBoot2);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHReaderGeneral::getNSHStatus: EEPROM: %c.", pNSHStatus->ucEEprom);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHReaderGeneral::getNSHStatus: Ram: %c.", pNSHStatus->ucRam);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHReaderGeneral::getNSHStatus: Camera ID: %c.", pNSHStatus->ucCameraID);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHReaderGeneral::getNSHStatus: Spare1: %c.", pNSHStatus->ucSpare1);
	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHReaderGeneral::getNSHStatus: Spare2: %c.", pNSHStatus->ucSpare2);
    m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHReaderGeneral::getNSHStatus: Step: %c.", pNSHStatus->ucStep);

	return 0;
}


int8_t NSHReaderGeneral::swReset(uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )		return -1;

	char rgcCmd[8] = "rst";
	int32_t liError;
	string strNSHReset;

	m_ucCmdIdToBeSent = eNSHResetId;
	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )						return liError;
	if ( receiveNSHResponse(strNSHReset, liError, eSPIAscii, uliTimeOutMsec) )		return liError;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::resetNSH: Reset done.");

	return strNSHReset.compare(NSH_DONE);
}

int8_t NSHReaderGeneral::hwReset(uint32_t uliLowTime)
{
	if ( !m_bConfigOk )		return -1;
	int8_t liRes = pullLowResetLine(uliLowTime);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "NSHReaderGeneral::hwReset: unable to pull low reset line.");
		return liRes;
	}

	liRes = checkForHwReset();
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "NSHReaderGeneral::hwReset: unable to check for hw reset.");
		return liRes;
	}

    liRes = 0;
    uint8_t timeoutCnt = 0;

	structNSHStatus pNSHStatus;
	getNSHStatus(&pNSHStatus);
	// wait until NSH is ready
	while ( pNSHStatus.ucStep != NSH_HW_RESET_READY_STATE )
	{
        if(timeoutCnt++ > 30)
        {
            // 10 secs timeout to be in ready
            m_pLogger->log(LOG_ERR, "NSHReaderGeneral::hwReset: timeout waiting for ready %c", pNSHStatus.ucStep);
            liRes = -1;
            break;
        }
        sleep(2);
		getNSHStatus(&pNSHStatus);
        m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHReaderGeneral::hwReset: waiting for ready %c", pNSHStatus.ucStep);
    }

    return liRes;
}

int8_t NSHReaderGeneral::hwResetForFwUpdate(uint32_t uliLowTime)
{
	if ( !m_bConfigOk )		return -1;
	int8_t liRes = pullLowResetLine(uliLowTime);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "NSHReaderGeneral::hwReset: unable to pull low reset line.");
	}
	return liRes;
}

int8_t NSHReaderGeneral::restoreDefaultParam(enumRestoreParam eParam, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )		return -1;

	char rgcCmd[8] = {0};
	int32_t liError;
	string strNSHRestore;

	sprintf(rgcCmd,"rdp%C", eParam);

	m_ucCmdIdToBeSent = eNSHRestoreParamId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )						return liError;
	if ( receiveNSHResponse(strNSHRestore, liError, eSPIAscii, uliTimeOutMsec) )	return liError;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::restoreDefaultParam: Restored default parameters.");

	return strNSHRestore.compare(NSH_DONE);
}


int8_t NSHReaderGeneral::getNSHFluoRead(int8_t cRFUConv, structNSHFluoRead* pNSHFluoRead, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )							return -1;
	if ( pNSHFluoRead == NULL )					return -1;

	char rgcCmd[8] = {0};
	int32_t liErrorCode;

	//set Convertion Fluo Reading
	string strConvRx;
	liErrorCode = convNSHRead(cRFUConv, strConvRx);
	if ( liErrorCode )		return -1;

	m_ucCmdIdToBeSent = eGetFluoReadId;
	strConvRx.clear();
	sprintf(rgcCmd,"mfl1");
	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liErrorCode, eSPIAscii, uliTimeOutMsec) )					return liErrorCode;
	if ( receiveNSHResponse(strConvRx, liErrorCode, eSPIAscii, uliTimeOutMsec) )	return liErrorCode;

	stringstream strMsg;
	strMsg << strConvRx;
	strMsg >> std::hex >> pNSHFluoRead->liRFUValue;
	strMsg >> std::hex >> pNSHFluoRead->uliStdDev;
	strMsg >> std::hex >> pNSHFluoRead->liMaxRFVValue;
	strMsg >> std::hex >> pNSHFluoRead->liMinRFVValue;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getNSHFluoRead: RFV value: %d", pNSHFluoRead->liRFUValue);
	m_pLogger->log(LOG_DEBUG, "NSHReaderGeneral::getNSHFluoRead: RFV standard deviation: %d", pNSHFluoRead->uliStdDev);
	m_pLogger->log(LOG_DEBUG, "NSHReaderGeneral::getNSHFluoRead: RFV max value: %d", pNSHFluoRead->liMaxRFVValue);
	m_pLogger->log(LOG_DEBUG, "NSHReaderGeneral::getNSHFluoRead: RFV min value: %d", pNSHFluoRead->liMinRFVValue);

	return 0;
}


int8_t NSHReaderGeneral::getLastFluoRawValuemV(structNSHLastRFV* pNSHLastRFV, uint8_t usStripPos, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )				return -1;
	if ( pNSHLastRFV == nullptr )	return -1;

	char rgcCmd[8] = "rfl";
	int32_t liError;
	string strLastRFV;

	m_ucCmdIdToBeSent = eGetLastFluoValue;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strLastRFV, liError, eSPIAscii, uliTimeOutMsec) )	return -1;

	stringstream strMsg;
	strMsg << strLastRFV;
	strMsg >> std::hex >> pNSHLastRFV->liRes[0];
	strMsg >> std::hex >> pNSHLastRFV->liRes[1];
	strMsg >> std::hex >> pNSHLastRFV->liRes[2];
	strMsg >> std::hex >> pNSHLastRFV->liFactor;

	if ( pNSHLastRFV->liFactor != NSH_DEFAULT_FACTOR )
	{
		m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getLastFluoRawValuemV: NSH factor set to default.");
		pNSHLastRFV->liFactor = NSH_DEFAULT_FACTOR;
	}

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getLastFluoRawValuemV: Strip[%d]: %d %d %d %d", usStripPos, pNSHLastRFV->liRes[0],
			pNSHLastRFV->liRes[1], pNSHLastRFV->liRes[2], pNSHLastRFV->liFactor);

	pNSHLastRFV->liReading = filterValuesRead(pNSHLastRFV->liRes, 3, pNSHLastRFV->liFactor, &pNSHLastRFV->liMinValueRead, &pNSHLastRFV->liMaxValueRead);

	static double dFactor = (double)pNSHLastRFV->liFactor / 1000.0;
	static double dResultAv = (double)pNSHLastRFV->liReading * dFactor;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getLastFluoRawValuemV: Strip[%d]: Filtered result [%d]*[%.3f]=[%.3f]",
				   usStripPos, pNSHLastRFV->liReading, dFactor, dResultAv);
	return 0;
}


int8_t NSHReaderGeneral::getLastRefRawValuemV(structNSHLastReference* pNSHLastRef, uint8_t usStripPos, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )				return -1;
	if ( pNSHLastRef == nullptr )	return -1;

	memset(pNSHLastRef, 0x00, sizeof(structNSHLastReference));

	char rgcCmd[8] = "rre";
	int32_t liError;
	string strLastRef;

	m_ucCmdIdToBeSent = eGetLastRefValue;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strLastRef, liError, eSPIAscii, uliTimeOutMsec) )	return -1;

	stringstream strMsg;
	strMsg << strLastRef;
	strMsg >> std::hex >> pNSHLastRef->liRes[0];
	strMsg >> std::hex >> pNSHLastRef->liRes[1];
	strMsg >> std::hex >> pNSHLastRef->liRes[2];
	strMsg >> std::hex >> pNSHLastRef->liFactor;

	if ( pNSHLastRef->liFactor != NSH_DEFAULT_FACTOR )
	{
		m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getLastRefRawValuemV: NSH factor set to default.");
		pNSHLastRef->liFactor = NSH_DEFAULT_FACTOR;
	}

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getLastRefRawValuemV: Strip[%d]: %d %d %d %d", usStripPos, pNSHLastRef->liRes[0],
				   pNSHLastRef->liRes[1], pNSHLastRef->liRes[2], pNSHLastRef->liFactor);

	pNSHLastRef->liReading = filterValuesRead(pNSHLastRef->liRes, 3, pNSHLastRef->liFactor, &pNSHLastRef->liMinValueRead, &pNSHLastRef->liMaxValueRead);

	static double dFactor = (double)pNSHLastRef->liFactor / 1000.0;
	static double dResultAv = (double)pNSHLastRef->liReading * dFactor;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getLastRefRawValuemV: Strip[%d]: Filtered result [%d]*[%.3f]=[%.3f]",
				   usStripPos, pNSHLastRef->liReading, dFactor, dResultAv);
	return 0;
}


int8_t NSHReaderGeneral::getSHIdentification(structSHIdentification *pSHId, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )		return -1;

	char rgcCmd[8] = "gid";
	int32_t liError;
	string strSHId;

	m_ucCmdIdToBeSent = eGetSHId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strSHId, liError, eSPIAscii, uliTimeOutMsec) )		return -1;

	stringstream strMsg;
	strMsg << strSHId;
	strMsg >> std::hex >> pSHId->uliMostSignifSHId;
	strMsg >> std::hex >> pSHId->uliMediumSHId;
	strMsg >> std::hex >> pSHId->uliLeastSignifSHId;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getSHIdentification: SH identification MSB: %d.", pSHId->uliMostSignifSHId);
	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getSHIdentification: SH identification: %d.", pSHId->uliMediumSHId);
	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getSHIdentification: SH identification LSB: %d.", pSHId->uliLeastSignifSHId);

	return 0;
}


int8_t NSHReaderGeneral::readAir(int32_t& liAirRead, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )							return -1;

	char rgcCmd[8] = "caf";
	int32_t liError;
	string strAir;

	m_ucCmdIdToBeSent = eGetAirReadingId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strAir, liError, eSPIAscii, uliTimeOutMsec) )		return -1;

	stringstream strMsg;
	strMsg << strAir;
	strMsg >> std::hex >> liAirRead;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::readAir: Air reading value: %d.", liAirRead);

	return 0;
}


int8_t NSHReaderGeneral::readSolidStandardRFU(int32_t& liSolidStdRFU, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )							return -1;

	char rgcCmd[8] = "mss";
	int32_t liError;
	string strSSvalue;

	m_ucCmdIdToBeSent = eReadSolidStdRFUId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strSSvalue, liError, eSPIAscii, uliTimeOutMsec) )	return -1;

	stringstream strMsg;
	strMsg << strSSvalue;
	strMsg >> std::hex >> liSolidStdRFU;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::readSolidStandardRFU: Solid standard RFU value: %d.", liSolidStdRFU);

	return 0;
}


int8_t NSHReaderGeneral::switchUVLed(enumNSHLed eLedEnable, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )							return -1;

	char rgcCmd[8] = {0};
	int32_t liError;
	string strNSHLed;

	switch ( eLedEnable )
	{
		case eNSHLedEnabled:	sprintf(rgcCmd,"lonENA");		break;
		case eNSHLedDisabled:	sprintf(rgcCmd,"lonDIS");		break;
		default:				sprintf(rgcCmd,"lonENA");		break;
	}

	m_ucCmdIdToBeSent = eSetLedId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strNSHLed, liError, eSPIAscii, uliTimeOutMsec) )	return -1;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::switchUVLed: Led value: %d.", !eLedEnable);

	return strNSHLed.compare(NSH_DONE);
}


int8_t NSHReaderGeneral::getSolidStdRFUStored(int32_t& liRFUValue, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )				return -1;

	char rgcCmd[8] = "gss";
	int32_t liError;
	string strRFU;

	m_ucCmdIdToBeSent = eGetSolidStdRFUId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strRFU, liError, eSPIAscii, uliTimeOutMsec) )		return -1;

	stringstream strMsg;
	strMsg << strRFU;
	strMsg >> std::hex >> liRFUValue;

	m_nGoldenSS = liRFUValue;
	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getSolidStdRFUStored: Stored RFU value of solid standard: %d.", liRFUValue);

	return 0;
}


int8_t NSHReaderGeneral::storeSolidStdRFU(const int32_t liRFUValue, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )			return -1;

	char rgcCmd[8] = {0};
	int32_t liError;
	string strIsRFUStored;

	sprintf(rgcCmd, "sss%0X", liRFUValue);
	m_ucCmdIdToBeSent = eStoreSolidStdRFUId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )							return -1;
	if ( receiveNSHResponse(strIsRFUStored, liError, eSPIAscii, uliTimeOutMsec) )		return -1;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::storeSolidStdRFU: RFU value of solid standard stored.");

	return strIsRFUStored.compare(NSH_DONE);
}


int8_t NSHReaderGeneral::autoCalibrateSolidStd(int32_t& liError, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )			return -1;

	char rgcCmd[8] = "css";
	string strAutoCal;

	m_ucCmdIdToBeSent = eAutoCalSolidStdId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )							return -1;
	if ( receiveNSHResponse(strAutoCal, liError, eSPIAscii, uliTimeOutMsec) )			return -1;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::autoCalibrateSolidStd: solid standard autocalibrated.");

	return strAutoCal.compare(NSH_DONE);
}


int8_t NSHReaderGeneral::calibrateAirFact(uint8_t ucExpectedAirRFU, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )																				return -1;
	if ( ucExpectedAirRFU < AIR_RFU_CAL_LOW_LIMIT || ucExpectedAirRFU > AIR_RFU_CAL_UP_LIMIT )		return -1;

	char rgcCmd[8] = {0};
	int32_t liError;
	string strAns;

	sprintf(rgcCmd, "caa%0X", ucExpectedAirRFU);
	m_ucCmdIdToBeSent = eCalibAirFactoryId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )							return -1;
	if ( receiveNSHResponse(strAns, liError, eSPIAscii, uliTimeOutMsec) )				return -1;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::calibrateAirFact: Calibration performed.");

	return strAns.compare(NSH_DONE);
}


int8_t NSHReaderGeneral::optCalibrateRFU(uint16_t usExpectedRFU, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )																			return -1;
	if ( usExpectedRFU < RFU_3144_CAL_LOW_LIMIT || usExpectedRFU > RFU_3144_CAL_UP_LIMIT )		return -1;

	char rgcCmd[8] = {0};
	int32_t liError;
	string strAns;

	sprintf(rgcCmd, "cfs%0X", usExpectedRFU);
	m_ucCmdIdToBeSent = eCalibAirFactoryId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )							return -1;
	if ( receiveNSHResponse(strAns, liError, eSPIAscii, uliTimeOutMsec) )				return -1;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::optCalibrateRFU: Calibration performed.");

	return strAns.compare(NSH_DONE);
}


int8_t NSHReaderGeneral::printLimits(uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )		return -1;

	char rgcCmd[8] = "gcl0";
	int32_t liError;
	string strAns;

	m_ucCmdIdToBeSent = eLimitId;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strAns, liError, eSPIAscii, uliTimeOutMsec) )		return -1;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::printLimits: %s", strAns.c_str());

	return 0;
}

int8_t NSHReaderGeneral::upgradeNSHFirmwareRequest(int8_t liFwType, uint32_t uliLenFile, uint32_t uliTimeOutMsec)
{
	char rgcCmd[16] = {0};
	sprintf(rgcCmd,"upd%C %0X", liFwType, uliLenFile);

	int32_t liError;
	string strAns;

	m_ucCmdIdToBeSent = eFwUpg;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strAns, liError, eSPIAscii, uliTimeOutMsec) )		return -1;

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::upgradeNSHFirmware: upgrade firmware command sent");

	return 0;
}


int8_t NSHReaderGeneral::getCalibrParams(strucCalibrParam* pSSCalibParam, uint32_t uliTimeOutMsec)
{
	char rgcCmd[8] = "gca0";

	int32_t liError;
	string strAns;

	m_ucCmdIdToBeSent = eGetCalibParam;

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strAns, liError, eSPIAscii, uliTimeOutMsec) )		return -1;

	int liFoo;
	stringstream strMsg;
	strMsg << strAns;
	strMsg >> std::hex >> liFoo;
	pSSCalibParam->fSSCalibPar = float(liFoo) / float(NSH_TO_COMMA_FACTOR);
	strMsg >> std::hex >> liFoo;
	pSSCalibParam->fSectionACalibPar = float(liFoo) / float(NSH_TO_COMMA_FACTOR);
	strMsg >> std::hex >> liFoo;
	pSSCalibParam->fSectionBCalibPar = float(liFoo) / float(NSH_TO_COMMA_FACTOR);

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::getSSCalibrParam: SS [%f], SecA [%f], SecB[%f]",
				   pSSCalibParam->fSSCalibPar, pSSCalibParam->fSectionACalibPar, pSSCalibParam->fSectionBCalibPar);

	return 0;
}


int NSHReaderGeneral::getGoldenSS()
{
	return m_nGoldenSS;
}

int NSHReaderGeneral::nshRouteCmd(string& strCmd, string& strAns, uint32_t uliTimeOutMsec)
{
	strAns.clear();
	int32_t liError;

	m_ucCmdIdToBeSent = eRouteCmdId;

	buff2MsgBlocks(strCmd.c_str(), strCmd.size(), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return -1;
	if ( receiveNSHResponse(strAns, liError, eSPIAscii, uliTimeOutMsec) )		return -1;

	return 0;
}


int32_t NSHReaderGeneral::filterValuesRead(int32_t* rgiValues, uint8_t ucNumVal, int32_t liFactor, int32_t* pliMin, int32_t* pliMax)
{
	if ( rgiValues == nullptr )	return -1;
	if ( ucNumVal == 0 )		return -1;

	double dTmpValue, dTmpFactor;
	double dResFiltering = 0;
	double dMinValue = (double)INT32_MAX;
	double dMaxValue = 0;

	for (uint8_t i = 0; i < ucNumVal; i++)
	{
		dTmpValue = (double)rgiValues[i];
		dTmpFactor = (double)liFactor;
		dTmpValue /= dTmpFactor;
		dResFiltering += dTmpValue;

		if ( dTmpValue <= dMinValue )
		{
			dMinValue = dTmpValue;
		}
		if ( dTmpValue >= dMaxValue )
		{
			dMaxValue = dTmpValue;
		}
	}

	if ( ucNumVal > 1 )
	{
		dResFiltering -= dMinValue;
		dResFiltering /= ((double)(ucNumVal - 1));
	}

	if ( pliMin != nullptr )
	{
		*pliMin = lround(dMinValue * FACTOR_MULTIPLY);
	}
	if ( pliMax != nullptr )
	{
		*pliMax = lround(dMaxValue * FACTOR_MULTIPLY);
	}

	dResFiltering *= FACTOR_MULTIPLY;

	return lround(dResFiltering);
}


int8_t NSHReaderGeneral::convNSHRead(int8_t cRFUConv, string& strConvRes, uint32_t uliTimeOutMsec)
{
	if ( !m_bConfigOk )		return -1;

	char rgcCmd[8] = {0};
	int32_t liError;

	m_ucCmdIdToBeSent = eConvFluoReadId;

	switch ( cRFUConv )
	{
		case eNSHModelDisabled:
		case eNSHModelEnabled:	{	/* value is ok*/	}	break;
		default:				{	cRFUConv = 0;		}	break;
	}

    sprintf(rgcCmd, "itm%d", cRFUConv);

    m_pLogger->log(LOG_INFO, "NSHReaderGeneral::convNSHRead: %s", rgcCmd);

	buff2MsgBlocks(rgcCmd, strlen(rgcCmd), eSPIAscii);
	if ( sendNSHMessage(liError, eSPIAscii, uliTimeOutMsec) )					return liError;
	if ( receiveNSHResponse(strConvRes, liError, eSPIAscii, uliTimeOutMsec) )	return liError;

	return strConvRes.compare(NSH_DONE);
}


int8_t NSHReaderGeneral::parseSubUnitAnswer(string& strRes)
{
	if ( !m_ucCmdIdToBeSent )	return -1;

	int8_t cResult;

	switch ( m_ucCmdIdToBeSent )
	{
		case eGetNSHStatusId:
			cResult = decGetBoardStatus(strRes);
		break;

		case eGetSHId:
		case eGetFirmwareId:
		case eGetFluoReadId:
		case eNSHResetId:
		case eNSHRestoreParamId:
		case eGetLastFluoValue:
		case eGetLastRefValue:
		case eGetAirReadingId:
		case eReadSolidStdRFUId:
		case eSetLedId:
		case eGetSolidStdRFUId:
		case eStoreSolidStdRFUId:
		case eConvFluoReadId:
		case eAutoCalSolidStdId:
		case eLimitId:
		case eCalibAirFactoryId:
		case eFwUpg:
		case eGetCalibParam:
		case eRouteCmdId:
			cResult = decGeneralCmd(strRes);
		break;

		default:
			m_pLogger->log(LOG_ERR, "NSHReaderGeneral::parseSubUnitAnswer: Command not managed.");
			cResult = -1;
		break;
	}

	m_ucCmdIdToBeSent = 0;

	return cResult;
}


int8_t NSHReaderGeneral::decGeneralCmd(string& strRx)
{
	if ( m_NSHMsgStructRx.ucPayloadPacketNum == 0 )		return -1;

	strRx.clear();

	for (int16_t m = 0; m < m_NSHMsgStructRx.ucPayloadPacketNum; m++)
	{
		for (int8_t n = 1; n < NSH_READER_SPI_PKT_SIZE; n++)
		{
			strRx.push_back(m_NSHMsgStructRx.ucPayloadBlock[m*NSH_READER_SPI_PKT_SIZE + n]);
		}
	}

	return 0;
}


int8_t NSHReaderGeneral::decGetBoardStatus(string& strBoardStatus)
{
	if ( m_NSHMsgStructRx.ucPayloadPacketNum == 0 )		return -1;

	strBoardStatus.clear();

	if ( m_NSHMsgStructRx.ucPayloadPacketNum == 3 ) // 9B of payload
	{
		for (int16_t m = 0; m < m_NSHMsgStructRx.ucPayloadPacketNum; m++)
		{
			for (int8_t n = 1; n < NSH_READER_SPI_PKT_SIZE; n++ )
			{
				strBoardStatus.push_back(m_NSHMsgStructRx.ucPayloadBlock[m*NSH_READER_SPI_PKT_SIZE + n]);
			}
		}

		return 0;
	}


	for (uint8_t i = 0; i < sizeof(structNSHStatus); i++)
	{
		strBoardStatus.push_back(eNSHStatusCheckNotDone);
	}

	m_pLogger->log(LOG_INFO, "NSHReaderGeneral::decGetBoardStatus: check not done.");
	return 0;
}


