/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderGeneral.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the NSHReaderGeneral class.
 @details

 ****************************************************************************
*/

#ifndef NSHREADERGENERAL_H
#define NSHREADERGENERAL_H

#include <math.h>
#include <limits.h>

#include "NSHReaderProtocol.h"
#include "NSHReaderCommonInclude.h"
#include "DataCollector.h"
#include "NSHReaderCommonInclude.h"


class NSHReaderGeneral : public NSHReaderProtocol
{
	public:

		/*! ***********************************************************************************************************
		 * @brief NSHReaderGeneral default constructor.
		 * ************************************************************************************************************
		 */
		NSHReaderGeneral();

		/*! ***********************************************************************************************************
		 * @brief ~NSHReaderGeneral virtual destructor.
		 * ************************************************************************************************************
		 */
		virtual ~NSHReaderGeneral();

		/*! ***********************************************************************************************************
		 * @brief  initNSH initialize hte class assigning Log, SPIInterface and GpioInterface.
		 * @param  pLogger pointer to the logger used.
		 * @param  pSPIIfr pointer to the SPIInterface used.
		 * @param  pGpio needed to initialize the pin where DRDY signal is checked.
		 * @param  ucDRDYGpioIdx index of DRDY gpio in CONFIG file.
         * @param ucResetGpioIdx to reset the line
		 * @return true in case of success, false otherwise.
		 * ************************************************************************************************************
		 */
		bool initNSH(Log* pLogger, SPIInterface* pSPIIfr, GpioInterface* pGpio, uint8_t ucDRDYGpioIdx, uint8_t ucResetGpioIdx);

		/*! ***********************************************************************************************************
		 * @brief  getFwVersion retrieve FW version of what specified in eFwVersion.
		 * @param  eFwVersion what we want.
		 * @param  strFWversion string where the FW version will be saved.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t getFwVersion(enumFwVersion eFwVersion, string& strFWversion, uint32_t uliTimeOutMsec = eGetFwVersionTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  getNSHStatus get the actual status of the NSH.
		 * @param  pNSHStatus structure where the status is stored.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t getNSHStatus(structNSHStatus* pNSHStatus, uint32_t uliTimeOutMsec = eGetNSHStatusTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  swReset reset the DSP that drives the NSH.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t swReset(uint32_t uliTimeOutMsec = eGetSwResetTimeoutMsec);


		/*! ***********************************************************************************************************
		 * @brief  hwReset reset the DSP that drives the NSH pulling down the control line.
         * @param  uliLowTime in which the reset line is pulled low.
		 * @return 0 in case of success, < 0 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t hwReset(uint32_t uliLowTime = eGetHwResetTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  restoreDefaultParam ask for a restore to default of the parameters.
		 * @param  eParam all of the parameters or just a part of them.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t restoreDefaultParam(enumRestoreParam eParam, uint32_t uliTimeOutMsec = eGetRestoreParamTimeoutMec);

		/*! ***********************************************************************************************************
		 * @brief  getNSHFluoRead get the reading of the fluorescence.
		 * @param  cRFUConv 0 to enable or 1 to disable conversion model.
		 * @param  pNSHFluoRead structure where the answer is stored.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t getNSHFluoRead(int8_t cRFUConv, structNSHFluoRead* pNSHFluoRead, uint32_t uliTimeOutMsec = eGetFluoReadTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  getLastFluoRawValuemV get the last fluorescence value read in mV.
		 * @param  pNSHLastRFV structure where the answer is stored.
		 * @param  usStripPos position of the strip.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t getLastFluoRawValuemV(structNSHLastRFV* pNSHLastRFV, uint8_t usStripPos,  uint32_t uliTimeOutMsec = eGetLastFluoValTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  getLastRefRawValuemV get the last reference value read in mV.
		 * @param  pNSHLastRef structure where the answer is stored.
		 * @param  usStripPos position of the strip.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t getLastRefRawValuemV(structNSHLastReference* pNSHLastRef, uint8_t usStripPos, uint32_t uliTimeOutMsec = eGetLastRefValTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  getSHIdentification get SH Id.
		 * @param  pSHId structure where the answer is stored.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t getSHIdentification(structSHIdentification *pSHId, uint32_t uliTimeOutMsec = eGetSHIdTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  getAirReading perform a reading in air.
		 * @param  liAirRead where the reading result will be stored.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t readAir(int32_t& liAirRead, uint32_t uliTimeOutMsec = eGetAirReadingTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  readSolidStandardRFU retrieve solid standard RFU value.
		 * @param  liSolidStdRFU where the reading result will be stored.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t readSolidStandardRFU(int32_t& liSolidStdRFU, uint32_t uliTimeOutMsec = eGetSSReadingTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  switchUVLed enable or disable the UltraViolet led.
		 * @param  eLedEnable enable or disable.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t switchUVLed(enumNSHLed eLedEnable, uint32_t uliTimeOutMsec = eGetLedEnableTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  getSolidStdRFUStored get the RFU value previously stored.
		 * @param  liRFUValue where the RFU value is stored.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t getSolidStdRFUStored(int32_t& liRFUValue, uint32_t uliTimeOutMsec = eGetSSStoredTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  storeSolidStdRFU store the RFu of the solid standard.
		 * @param  liRFUValue value to be stored.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t storeSolidStdRFU(const int32_t liRFUValue, uint32_t uliTimeOutMsec = eStoreRFUTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  autoCalibrateSolidStd perform autocalibration.
         * @param liError the error value
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t autoCalibrateSolidStd(int32_t& liError, uint32_t uliTimeOutMsec = eAutoCalSSTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  calibrateAirFact calibrate air (factory).
		 * @param  ucExpectedAirRFU expected air value.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t calibrateAirFact(uint8_t ucExpectedAirRFU, uint32_t uliTimeOutMsec = eCalibrAirTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  calibrate3144RFUFact calibrate to known value.
		 * @param  ucExpectedRFU expected RFU value.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t optCalibrateRFU(uint16_t ucExpectedRFU, uint32_t uliTimeOutMsec = eCalibr3144RFUTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  printLimits print NSH limits.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t printLimits(uint32_t uliTimeOutMsec = eCalibrAndReadLimitsMsec);

		/*! ***********************************************************************************************************
		 * @brief  upgradeNSHFirmwareRequest send command to start FW update.
		 * @param  liFwType type of fw to be updated.
		 * @param  uliLenFile length of file to be sent.
		 * @param  uliTimeOutMsec timeout of reading.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t upgradeNSHFirmwareRequest(int8_t liFwType, uint32_t uliLenFile, uint32_t uliTimeOutMsec = eFirmwareUpgradeTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  getSSCalibrParam function used to get SS calibration parameter (gain).
         * @param  pSSCalibParam variable where the SS gain will be stored.
		 * @param  uliTimeOutMsec max timeout of reading.
		 * @return fSSCalibParam.
		 * ************************************************************************************************************
		 */
		int8_t getCalibrParams(strucCalibrParam* pSSCalibParam, uint32_t uliTimeOutMsec = eSSCalibrParamsTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  getGoldenSS get value of m_nGoldenSS.
		 * @return m_nGoldenSS.
		 * ************************************************************************************************************
		 */
		int getGoldenSS(void);

		/*! ***********************************************************************************************************
		 * @brief  nshRouteCmd route command for nsh
		 * @param  strCmd command to be routed to the nsh
		 * @param  strAns answer from the camera
         * @param  uliTimeOutMsec max timeout of command
         * @return 0 in case of success, -1 otherwise
		 * ************************************************************************************************************
		 */
		int nshRouteCmd(string& strCmd, string& strAns, uint32_t uliTimeOutMsec = eGetDRDYLineWighTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  hwResetForFwUpdate pull low reset line
		 * @param  uliLowTime max timeout of hw reset
		 * @return 0 in case of success, -1 otherwise
		 * ************************************************************************************************************
		 */
		int8_t hwResetForFwUpdate(uint32_t uliLowTime = eGetDRDYLineWighTimeoutMsec);

		/*! ***********************************************************************************************************
		 * @brief  convNSHRead enable or disable conversion model.
		 * @param  cRFUConv to decide if the conversion model has to be enabled or not.
		 * @param  strConvRes string where the answer is stored.
		 * @param  uliTimeOutMsec in which the DRDY line is watched and time max where the NSH answer is expected.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t convNSHRead(int8_t cRFUConv, string& strConvRes, uint32_t uliTimeOutMsec = eGetConvFluoTimeoutMsec);

	private:

		/*! ***********************************************************************************************************
		 * @brief  filterValuesRead used to get the value from the different reading, used in getLastRef/FluoRawValuemV.
		 * @param  rgiValues pointer to values array.
		 * @param  ucNumVal number of value to be filtered.
		 * @param  liFactor values get from getLastRefRawValuemV or getLastFluoRawValuemV.
		 * @param  pliMin store min value.
		 * @param  pliMax store max value.
		 * @return result filtered.
		 * ************************************************************************************************************
		 */
		int32_t filterValuesRead(int32_t* rgiValues, uint8_t ucNumVal, int32_t liFactor, int32_t* pliMin, int32_t* pliMax);

		/*! ***********************************************************************************************************
		 * @brief  parseSubUnitAnswer parse the answer received.
		 * @param  strRes where the answer is copyed.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t parseSubUnitAnswer(string& strRes);

		/*! ***********************************************************************************************************
		 * @brief  decGetBoardStatus to parse board status.
		 * @param  strBoardStatus where the answer is copyed.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t decGetBoardStatus(string& strBoardStatus);

		/*! ***********************************************************************************************************
		 * @brief  decGeneralCmd to parse a general commmand.
		 * @param  strRx where the answer is copyed.
		 * @return 0 in case of success, -1 otherwise.
		 * ************************************************************************************************************
		 */
		int8_t decGeneralCmd(string& strRx);

    private:

        int m_nGoldenSS;

};

#endif // NSHREADERGENERAL_H
