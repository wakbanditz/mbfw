/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHReaderDRDY.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the NSHReaderDRDY class.
 @details

 ****************************************************************************
*/

#ifndef NSHREADERDRDY_H
#define NSHREADERDRDY_H

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#include "stdint.h"
#include "TimeStamp.h"
#include "Loggable.h"
#include "GpioInterface.h"
#include "CommonInclude.h"
#include "NSHReaderCommonInclude.h"


class NSHReaderGPIO
{
	public:

		/*! **********************************************************************************************************************
		 * @brief NSHReaderDRDY default constructor.
		 * ***********************************************************************************************************************
		 */
		NSHReaderGPIO();

		/*! **********************************************************************************************************************
		 * @brief ~NSHReaderDRDY virtual destructor.
		 * ***********************************************************************************************************************
		 */
		virtual ~NSHReaderGPIO();

		/*! **********************************************************************************************************************
		 * @brief  getDRDYvalue reads DRDY gpio's value.
		 * @return -1 in case of error, gpio's value otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t getDRDYvalue(void);

		/*! **********************************************************************************************************************
		 * @brief  checkDRDYstatusWithTimeout monitors with timeout DRDY line until it is high.
		 * @param  uliTimeOutMsec timeout during which the gpio is checked.
		 * @return 0 if DRDY = high, -1 if timeout elapsed and DRDY still low.
		 * ***********************************************************************************************************************
		 */
		int8_t checkDRDYstatusWithTimeout(uint32_t uliTimeOutMsec = eGetDRDYLineWighTimeoutMsec);

		/*! **********************************************************************************************************************
		 * @brief  pullLowResetLine set reset line to 0 for uliLowTime(ms). Used for hardware reset.
		 * @param  uliLowTime ms when the line is pulled low.
		 * @return 0 in case of success, -1 if is not possible to write gpio, -2 if a writing error occurs.
		 * ***********************************************************************************************************************
		 */
		int8_t pullLowResetLine(uint32_t uliLowTime);

		/*! **********************************************************************************************************************
		 * @brief checkForHwReset need to do a double check of the irq (bootloader and applicative).
         * @param  uliTimeOutMsec ms when the irq is attended.
		 * @return 0 in case of success, -1 if is not possible to write gpio, -2 if a writing error occurs.
		 * ***********************************************************************************************************************
		 */
		int8_t checkForHwReset(uint32_t uliTimeOutMsec = eGetDRDYLineWighTimeoutMsec);

	protected:

		/*! **********************************************************************************************************************
		 * @brief  initDRDY initializes NSHBoardDRDY class.
		 * @param  plogger pointer to logger object.
		 * @param  pGpio pointer to Gpio object.
         * @param ucDRDYGpioIdx
         * @param  ucResetGpioIdx
		 * @return 0 in case of success, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t initDRDY(Log *plogger, GpioInterface* pGpio, uint8_t ucDRDYGpioIdx, uint8_t ucResetGpioIdx);

    private:

        bool			m_bConfigOk;
        GpioInterface*	m_pGpio;
        Log*			m_pLog;
        int8_t			m_nDRDYGpioIdx;
        int8_t			m_nResetGpioIdx;

};

#endif // NSHREADERDRDY_H
