/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SPIErrors.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SPIErrors class.
 @details

 ****************************************************************************
*/

#ifndef SPIERRORS_H
#define SPIERRORS_H

#include "ErrorUtilities.h"
#include "NSHError.h"
#include "Log.h"

class SPIErrors
{
	public:

		/*! *************************************************************************************************
		 * @brief SPIErrors default constructor
		 * **************************************************************************************************
		 */
		SPIErrors();

		/*! *************************************************************************************************
		 * @brief ~SPIErrors virtual destructor
		 * **************************************************************************************************
		 */
		virtual ~SPIErrors();

		/*! *************************************************************************************************
		 * @brief  initSPIErr init class building ErrorVector
		 * @param  pLogger pointer to logger
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
		int initSPIErr(Log *pLogger);

		/*! *************************************************************************************************
		 * @brief clearAllErrors set inactive all the errors
		 * **************************************************************************************************
		 */
		void clearAllErrors(void);

		/*! *************************************************************************************************
		 * @brief  updateErrorsTime update time
         * @param ulCurrentMsec current msecs value
         * @param currentTime current time structure
		 * @return num of timings updated
		 * **************************************************************************************************
		 */
		int updateErrorsTime(uint64_t ulCurrentMsec, struct tm * currentTime);

		/*! *************************************************************************************************
		 * @brief  getErrorList function used to retrieve the active errors
		 * @param  pVectorList pointer to the error vector to be updated
         * @param errorLevel the level error to filter
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
        int getErrorList(vector<string> * pVectorList, uint8_t errorLevel = GENERAL_ERROR_LEVEL_NONE);

		/*! *************************************************************************************************
		 * @brief  decodeNotifyEvent manages notify event message
		 * @param  liEventCode code related to the error
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool decodeNotifyEvent(int liEventCode);

        /*! *************************************************************************************************
         * @brief  clearError clear the error corresponding to the code
         * @param  liEventCode code related to the error
         * @return true if error has been reset, false otherwise
         * **************************************************************************************************
         */
        bool clearError(int liEventCode);

        /*! *************************************************************************************************
         * @brief  checkErrors checks if critical errors are still active (and set the device status accordingly)
         * @return true if error has been set, false otherwise
         * **************************************************************************************************
         */
        bool checkErrors();

	private:

		/*! *************************************************************************************************
		 * @brief  buildErrorVector build the vector parsing error file
		 * @return size of error vector
		 * **************************************************************************************************
		 */
		int buildErrorVector(void);

		/*! *************************************************************************************************
		 * @brief  searchEventError search error among the list
		 * @param  strErrCode error string
		 * @param  pNSHErrorFound error class related to the wanted one
		 * @return 0 if error is found & active, 1 if is found but not active, -1 if there is no such an err
		 * **************************************************************************************************
		 */
		int searchEventError(const string& strErrCode, NSHError*& pNSHErrorFound);

    private:

        Log					*m_pLogger;
        vector< NSHError* >	m_vectorErrors;

};

#endif // SPIERRORS_H
