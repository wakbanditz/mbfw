/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    LS7366RBase.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the LS7366RBase class.
 @details

 ****************************************************************************
*/

#include "LS7366RBase.h"

LS7366RBase::LS7366RBase()
{
	m_pLogger = 0;
	m_pSPIIfr = 0;
	m_bConfigOk = false;
}

LS7366RBase::~LS7366RBase()
{

}

int8_t LS7366RBase::readReg(enumReadOpCode eOpCode, uint8_t ucDataSizeBytes)
{
	if ( ! m_bConfigOk )	return ERR_NSH_ENCODER_CONFIG_NOK;

	m_rgcTxBuf[0] = eOpCode;
	memset(&m_rgcTxBuf[1], 0x00, ucDataSizeBytes);
	// Send command + data
	bool bSendOpRes = m_pSPIIfr->singleTransfer(m_rgcTxBuf, m_rgcRxBuf, ucDataSizeBytes+1);

	if ( bSendOpRes == false )
	{
		return ERR_NSH_ENCODER_COMMUNICATION;
	}
	return 0;
}

int8_t LS7366RBase::writeReg(enumWriteOpCode eOpCode, uint32_t uliData, uint8_t ucDataSizeBytes)
{	
	if ( ! m_bConfigOk )								return ERR_NSH_ENCODER_CONFIG_NOK;
	if ( ucDataSizeBytes > REGISTER_SIZE_BYTES )		return ERR_NSH_ENCODER_BAD_PARAM;

	m_rgcTxBuf[0] = eOpCode;
	//memcpy(&m_rgcTxBuf[1], &uliData, ucDataSizeBytes);

	switch(ucDataSizeBytes)
	{
		case 1:
			m_rgcTxBuf[1] = (uint8_t)(uliData);
		break;

		case 2:
			m_rgcTxBuf[1] = (uint8_t)(uliData >> 8);
			m_rgcTxBuf[2] = (uint8_t)(uliData);
		break;

		case 3:
			m_rgcTxBuf[1] = (uint8_t)(uliData >> 16);
			m_rgcTxBuf[2] = (uint8_t)(uliData >> 8);
			m_rgcTxBuf[3] = (uint8_t)(uliData);
		break;

		case 4:
			m_rgcTxBuf[1] = (uint8_t)(uliData >> 24);
			m_rgcTxBuf[2] = (uint8_t)(uliData >> 16);
			m_rgcTxBuf[3] = (uint8_t)(uliData >> 8);
			m_rgcTxBuf[4] = (uint8_t)(uliData);
		break;

		default:
			m_pLogger->log(LOG_ERR,"LS7366RBase::writeReg: unaccepted message size");
			return ERR_NSH_ENCODER_BAD_PARAM;
		break;
	}

	// Send command + data
	bool bSendOpRes = m_pSPIIfr->singleTransfer(m_rgcTxBuf, m_rgcRxBuf, ucDataSizeBytes+1);
	if ( bSendOpRes == false )
	{
		return ERR_NSH_ENCODER_COMMUNICATION;
	}
	return 0;
}

int8_t LS7366RBase::clearReg(enumClearOpCode eOpCode)
{
	if ( ! m_bConfigOk )	return ERR_NSH_ENCODER_CONFIG_NOK;

	m_rgcTxBuf[0] = eOpCode;
	bool bSendOpRes = m_pSPIIfr->singleTransfer(m_rgcTxBuf, m_rgcRxBuf, 1);
	if ( bSendOpRes == false )
	{
		return ERR_NSH_ENCODER_COMMUNICATION;
	}
	return 0;
}

int8_t LS7366RBase::loadReg(enumLoadOpCode eOpCode)
{
	if ( ! m_bConfigOk )	return ERR_NSH_ENCODER_CONFIG_NOK;

	m_rgcTxBuf[0] = eOpCode;
	bool bSendOpRes = m_pSPIIfr->singleTransfer(m_rgcTxBuf, m_rgcRxBuf, 1);
	if ( bSendOpRes == false )
	{
		return ERR_NSH_ENCODER_COMMUNICATION;
	}
	return 0;
}

