/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SPIInterfaceCollector.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SPIInterfaceCollector class.
 @details

 ****************************************************************************
*/

#include "SPIInterfaceCollector.h"

SPIInterfaceCollector::SPIInterfaceCollector()
{
	m_pSPI1 = NULL;
	m_pSPI2 = NULL;
	m_pSPI3 = NULL;

	m_NSHMotorBusyGpioIdx = 0;
	m_NSHReaderDRDYGpioIdx = 0;
	m_NSHReaderResetGpioIdx = 0;
}


SPIInterfaceCollector::~SPIInterfaceCollector()
{
	destroyInterfaces();
}

int8_t SPIInterfaceCollector::createAndInitInterfaces(const char* sConfigFile)
{
	// read parameters from config file.
	if ( ! readConfigFile(sConfigFile) )
	{
		log(LOG_ERR, "SPIInterfaceCollector::createAndInitInterfaces: unable to read config file");
		return -1;
	}

	SAFE_DELETE(m_pSPI1);
	m_pSPI1 = new SPIInterface();
	if ( !m_pSPI1 )
	{
		log(LOG_ERR, "SPIInterfaceCollector::createAndInitInterfaces: unable to create SPI interface 1, memory error");
		return -1;
	}

	SAFE_DELETE(m_pSPI2);
	m_pSPI2 = new SPIInterface();
	if ( !m_pSPI2 )
	{
		log(LOG_ERR, "SPIInterfaceCollector::createAndInitInterfaces: unable to create SPI interface 2, memory error");
		return -1;
	}

	SAFE_DELETE(m_pSPI3);
	m_pSPI3 = new SPIInterface();
	if ( !m_pSPI3 )
	{
		log(LOG_ERR, "SPIInterfaceCollector::createAndInitInterfaces: unable to create SPI interface 3, memory error");
		return -1;
	}

	/*!*************************** NSH Reader *****************************/
	if ( !m_pSPI1->init(m_Config.GetString(CFG_SPI_BASE_DIR_1), m_Config.GetInt(CFG_SPI_MODE_1),
						m_Config.GetInt(CFG_SPI_SPEED_1), m_Config.GetInt(CFG_SPI_BITS_PER_WORD_1)) )
	{
		log(LOG_ERR,
			"SPIInterfaceCollector::createAndInitInterfaces: error initializing %s.",
			m_Config.GetString(CFG_SPI_BASE_DIR_1) );
		return -1;
	}
	m_pSPI1->setLogger(getLogger());
	m_NSHReaderDRDYGpioIdx = m_Config.GetInt(CFG_SPI_GPIO_IDX_1_1);
	m_NSHReaderResetGpioIdx = m_Config.GetInt(CFG_SPI_GPIO_IDX_1_2);

	/*!*************************** NSH Motor *****************************/
	if ( !m_pSPI2->init(m_Config.GetString(CFG_SPI_BASE_DIR_2), m_Config.GetInt(CFG_SPI_MODE_2),
						m_Config.GetInt(CFG_SPI_SPEED_2), m_Config.GetInt(CFG_SPI_BITS_PER_WORD_2)) )
	{
		log(LOG_ERR,
			"SPIInterfaceCollector::createAndInitInterfaces: error initializing %s.",
			m_Config.GetString(CFG_SPI_BASE_DIR_2) );
		return -1;
	}
	m_pSPI2->setLogger(getLogger());
	m_NSHMotorBusyGpioIdx = m_Config.GetInt(CFG_SPI_GPIO_IDX_2_1);
	m_NSHMotorFaultGpioIdx = m_Config.GetInt(CFG_SPI_GPIO_IDX_2_2);

	/*!*************************** NSH Encoder *****************************/
	if ( !m_pSPI3->init(m_Config.GetString(CFG_SPI_BASE_DIR_3), m_Config.GetInt(CFG_SPI_MODE_3),
						m_Config.GetInt(CFG_SPI_SPEED_3), m_Config.GetInt(CFG_SPI_BITS_PER_WORD_3)) )
	{
		log(LOG_ERR,
			"SPIInterfaceCollector::createAndInitInterfaces: error initializing %s.",
			m_Config.GetString(CFG_SPI_BASE_DIR_3) );
		return -1;
	}
	m_pSPI3->setLogger(getLogger());


	return 0;
}

SPIInterface* SPIInterfaceCollector::getInterface(enumSPIInterface eSPIIfr)
{
	SPIInterface* pSPIifr = NULL;

	switch ( eSPIIfr )
	{
		case eSPI_1:	pSPIifr = m_pSPI1;	break;
		case eSPI_2:	pSPIifr = m_pSPI2;	break;
		case eSPI_3:	pSPIifr = m_pSPI3;	break;
		case eSPI_0:

		default:
			//log(LOG_ERR, "SPIInterfaceCollector::getChannel: channel not available");
			return NULL;
		break;
	}

	return pSPIifr;
}


void SPIInterfaceCollector::destroyInterfaces()
{
	SAFE_DELETE(m_pSPI1);
	SAFE_DELETE(m_pSPI2);
	SAFE_DELETE(m_pSPI3);
}


bool SPIInterfaceCollector::readConfigFile(const char* sConfigFileName)
{
	// Reads configuration file
	config_key_t config_keys[] =
	{
		// Single SPI configuration keys
		{ CFG_SPI_BASE_DIR_1, (char*)"spi.1.baseDir", (char*)"GENERAL", T_string, (char*)NSHR_DEF_SPI_BUS_NAME, DEFAULT_ACCEPTED},
		{ CFG_SPI_MODE_1, (char*)"spi.1.mode", (char*)"GENERAL", T_int, (char*)NSHR_DEF_SPI_MODE, DEFAULT_ACCEPTED},
		{ CFG_SPI_SPEED_1, (char*)"spi.1.speed", (char*)"GENERAL", T_int, (char*)NSHR_DEF_SPI_SPEED_HZ, DEFAULT_ACCEPTED},
		{ CFG_SPI_BITS_PER_WORD_1, (char*)"spi.1.bitsPerWord", (char*)"GENERAL", T_int, (char*)NSHR_DEF_SPI_BITS_PER_WORD, DEFAULT_ACCEPTED},
		{ CFG_SPI_GPIO_IDX_1_1, (char*)"spi.1.gpioIdx", (char*)"GENERAL", T_int, (char*)NSHR_DEF_GPIO_IDX_DRDY, DEFAULT_ACCEPTED},
		{ CFG_SPI_GPIO_IDX_1_2, (char*)"spi.1.gpioIdx2", (char*)"GENERAL", T_int, (char*)NSHR_DEF_GPIO_IDX_RESET, DEFAULT_ACCEPTED},

		// Single SPI configuration keys
		{ CFG_SPI_BASE_DIR_2, (char*)"spi.2.baseDir", (char*)"GENERAL", T_string, (char*)NSHM_DEF_SPI_BUS_NAME, DEFAULT_ACCEPTED},
		{ CFG_SPI_MODE_2, (char*)"spi.2.mode", (char*)"GENERAL", T_int, (char*)NSHM_DEF_SPI_MODE, DEFAULT_ACCEPTED},
		{ CFG_SPI_SPEED_2, (char*)"spi.2.speed", (char*)"GENERAL", T_int, (char*)NSHM_DEF_SPI_SPEED_HZ, DEFAULT_ACCEPTED},
		{ CFG_SPI_BITS_PER_WORD_2, (char*)"spi.2.bitsPerWord", (char*)"GENERAL", T_int, (char*)NSHM_DEF_SPI_BITS_PER_WORD, DEFAULT_ACCEPTED},
		{ CFG_SPI_GPIO_IDX_2_1, (char*)"spi.2.gpioIdx", (char*)"GENERAL", T_int, (char*)NSHM_DEF_GPIO_IDX_BUSYLINE, DEFAULT_ACCEPTED},
		{ CFG_SPI_GPIO_IDX_2_2, (char*)"spi.2.gpioIdx2", (char*)"GENERAL", T_int, (char*)NSHM_DEF_GPIO_IDX_FAULT_LINE, DEFAULT_ACCEPTED},

		// Single SPI configuration keys
		{ CFG_SPI_BASE_DIR_3, (char*)"spi.3.baseDir", (char*)"GENERAL", T_string, (char*)NSHE_DEF_SPI_BUS_NAME, DEFAULT_ACCEPTED},
		{ CFG_SPI_MODE_3, (char*)"spi.3.mode", (char*)"GENERAL", T_int, (char*)NSHE_DEF_SPI_MODE, DEFAULT_ACCEPTED},
		{ CFG_SPI_SPEED_3, (char*)"spi.3.speed", (char*)"GENERAL", T_int, (char*)NSHE_DEF_SPI_SPEED_HZ, DEFAULT_ACCEPTED},
		{ CFG_SPI_BITS_PER_WORD_3, (char*)"spi.3.bitsPerWord", (char*)"GENERAL", T_int, (char*)NSHE_DEF_SPI_BITS_PER_WORD, DEFAULT_ACCEPTED},
		{ CFG_SPI_GPIO_IDX_3, (char*)"spi.3.gpioIdx", (char*)"GENERAL", T_int, (char*)NSHE_DEF_GPIO_IDX_BUSYLINE, DEFAULT_ACCEPTED}
	};

	int num_keys = sizeof(config_keys) / sizeof(config_key_t);

	if ( !m_Config.Init(config_keys, num_keys, sConfigFileName) )
	{
		char error[1024];
		Config::GetLastError(error, 1024);
		log(LOG_ERR, "Impossible to read parameter from <%s> (error: <%s>)", sConfigFileName, error);
		m_Config.Free();
		return false;
	}

	return true;
}
