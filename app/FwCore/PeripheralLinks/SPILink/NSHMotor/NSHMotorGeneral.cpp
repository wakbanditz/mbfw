/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHMotorGeneral.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the L6472Busy class.
 @details

 ****************************************************************************
*/

#include "NSHMotorGeneral.h"

NSHMotorGeneral::NSHMotorGeneral()
{
	m_pLogger = 0;
	m_pGpio = 0;

    // this is set @ higher level through initL6472()
	m_nBusyGpioIdx = 0;
	m_nFaultGpioIdx = 0;
}


NSHMotorGeneral::~NSHMotorGeneral()
{

}


int8_t NSHMotorGeneral::getBUSYGpioVal(void)
{
	if ( m_pGpio == 0 )				return -1;
	if ( m_pLogger == 0 )			return -1;
	if ( m_nBusyGpioIdx == 0 )		return -1;

	int8_t cGpioValue = m_pGpio->readValue(m_nBusyGpioIdx);

	int8_t siRetVal = -1;
	switch(cGpioValue)
	{
		case GPIO_VAL_UNDEF:
			m_pLogger->log(LOG_ERR, "NSHMotorGeneral::getBUSYGpioVal: Impossible to read gpio.\n");
		break;

		case GPIO_VAL_LOW:
			siRetVal = GPIO_VAL_LOW;
		break;

		case GPIO_VAL_HIGH:
			siRetVal = GPIO_VAL_HIGH;
		break;

		default:
		break;
	}
	return siRetVal;
}

int8_t NSHMotorGeneral::checkBUSYValWithTimeout(uint32_t uliTimeOutMsec)
{
	if ( m_pGpio == 0 )				return -1;
	if ( m_pLogger == 0 )			return -1;
	if ( m_nBusyGpioIdx == 0 )	return -1;

	int8_t cBusyLineValue = getBUSYGpioVal();

	if ( cBusyLineValue < 0 )	return -1;
	else if ( cBusyLineValue )	return 0;

	if ( !m_pGpio->waiForIrq(m_nBusyGpioIdx, uliTimeOutMsec) )
	{
		m_pLogger->log(LOG_ERR, "NSHMotorGeneral::checkBUSYValWithTimeout: Timeout expired, L6472 still busy.");
		stopMotor();
		m_pLogger->log(LOG_ERR, "NSHMotorGeneral::checkBUSYValWithTimeout: Motor Hard stopped after timeout expired.");
		return -2;
	}

	m_pLogger->log(LOG_DEBUG_PARANOIC, "NSHMotorGeneral::checkBUSYValWithTimeout: got IRQ");

	return 0;
}

int8_t NSHMotorGeneral::getFaultGpioVal()
{
	if ( m_pGpio == 0 )				return -1;
	if ( m_pLogger == 0 )			return -1;
	if ( m_nFaultGpioIdx == 0 )		return -1;

	int8_t cGpioValue = m_pGpio->readValue(m_nFaultGpioIdx);

	int8_t siRetVal = -1;
	switch(cGpioValue)
	{
		case GPIO_VAL_UNDEF:
			m_pLogger->log(LOG_ERR, "NSHMotorGeneral::getFaultGpioVal: Impossible to read gpio.\n");
		break;

		case GPIO_VAL_LOW:
			siRetVal = GPIO_VAL_LOW;
		break;

		case GPIO_VAL_HIGH:
			siRetVal = GPIO_VAL_HIGH;
		break;

		default:
		break;
	}
	return siRetVal;
}
