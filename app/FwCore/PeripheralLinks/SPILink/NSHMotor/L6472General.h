/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    L6472General.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the L6472General class.
 @details

 ****************************************************************************
*/

#ifndef L6472GENERAL_H
#define L6472GENERAL_H

#include "L6472Base.h"

typedef enum
{
    eReverseDir = 1,
    eForwardDir = 2
} enumMotorDirection;

class L6472General : private L6472Base
{

	public:

		/*! **********************************************************************************************************************
         * @brief L6472General default constructor.
		 * ***********************************************************************************************************************
		 */
		L6472General();

		/*! **********************************************************************************************************************
         * @brief ~L6472General virtual destructor.
		 * ***********************************************************************************************************************
		 */
		virtual ~L6472General();

		/*! **********************************************************************************************************************
		 * @brief  initL6472 initialize the motor driver.
		 * @param  pLogger pointer to Log instance.
		 * @param  pSPIIfr pointer to SPIInterface instance.
		 * @param  pGpio pointer to GpioInterface instance.
         * @param  ucBusyGpioIdx to monitor busy line. Has to be set from CONFIG file.
         * @param  ucFaultGpioIdx to fault line. Has to be set from CONFIG file.
         * @return true if initialization ended with success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool initL6472(Log* pLogger, SPIInterface* pSPIIfr, GpioInterface* pGpio, uint8_t ucBusyGpioIdx, uint8_t ucFaultGpioIdx);

		/*! **********************************************************************************************************************
		 * @brief  getMotorStatus get the status of the motor.
		 * @return motor status if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int16_t getMotorStatus(void);

		/*! **********************************************************************************************************************
		 * @brief  stopMotor action executed in case the motor timeout is expired.
		 * @return true if success, false if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		bool stopMotor(void);


        /*! **********************************************************************************************************************
         * @brief  setHiZ action executed to put the motor in HiZ.
         * @return true if success, false if m_bConfigOK = false.
         * ***********************************************************************************************************************
         */
        bool setHiZ();

		/*! **********************************************************************************************************************
		 * @brief  isSensorAtHome function used to notice if the sensor is at home or not.
		 * @return true if the sensor is at home, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool isSensorAtHome(void);

		/*! **********************************************************************************************************************
		 * @brief  moveRelativeSteps function used to move the motor of a certain number of steps.
		 * @param  eDir needed to set the motor direction (backward, forward).
         * @param  uliMicroStepsNum number of steps the motor has to perform.
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t moveRelativeSteps(enumMotorDirection eDir, uint32_t uliMicroStepsNum);

		/*! **********************************************************************************************************************
		 * @brief  moveAbsolutePosition function used to move the motor to an absolute position. L6472 makes the shortest path.
         * @param  liPosition absolute position.
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t moveToAbsolutePosition(int liPosition);

		/*! **********************************************************************************************************************
		 * @brief  searchForHome init the home procedure. 1) find home high speed; 2) tune find home with lower speed.
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t searchForHome(void);

		/*! **********************************************************************************************************************
		 * @brief  sendReset reset all the registers of the stepper motor driver to their defaults.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t sendReset(void);

		/*! **********************************************************************************************************************
		 * @brief  resetPos reset the home position (to be called at the end of the home procedure).
		 * @return 0 if success, -1 if motor busy (hardstopped anyway), -2 if cmd not menaged.
		 * ***********************************************************************************************************************
		 */
		int8_t resetPos();

		/*! **********************************************************************************************************************
		 * @brief  setAcceleration write on the apposite register the acceleration value desired.
		 * @param  usAcc acceleration value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t setAcceleration(uint16_t usAcc);

		/*! **********************************************************************************************************************
		 * @brief  setDeceleration write on the apposite register the deceleration value desired.
		 * @param  usDec deceleration value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t setDeceleration(uint16_t usDec);

		/*! **********************************************************************************************************************
		 * @brief  setMaxSpeed write on the apposite register the max speed value desired.
		 * @param  usMaxSpeed max speed value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t setMaxSpeed(uint16_t usMaxSpeed);

		/*! **********************************************************************************************************************
		 * @brief  setMinSpeed write on the apposite register the min speed value desired.
		 * @param  usMinSpeed min speed value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t setMinSpeed(uint16_t usMinSpeed);

		/*! **********************************************************************************************************************
		 * @brief  setFullSpeed write on the apposite register the full speed value desired.
		 * @param  usFullSpeed full speed value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t setFullSpeed(uint16_t usFullSpeed);

		/*! **********************************************************************************************************************
		 * @brief  setMotorCurrent write on the apposite register the value of current desired (31.25mA to 4000.0mA).
         * @param eCmd command identifier
         * @param  fCurrentVal current value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t setMotorCurrent(enumL6472Commands eCmd, float fCurrentVal);

		/*! **********************************************************************************************************************
		 * @brief  setAbsPosition write to the apposite register the absolute position.
		 * @param  liAbsPos variable where is be stored the absolute position.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t setAbsPosition(int liAbsPos);

		/*! **********************************************************************************************************************
		 * @brief  getMotorCurrent read from the apposite register the current value.
         * @param  cCurrentVal variable where will be stored the current value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t getStillMotorCurrent(uint8_t& cCurrentVal);

		/*! **********************************************************************************************************************
		 * @brief  getRunningMotorCurrent read from the apposite register the current value.
         * @param  cCurrentVal variable where will be stored the current value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t getRunningMotorCurrent(uint8_t& cCurrentVal);

		/*! **********************************************************************************************************************
		 * @brief  getAcceleration read from the apposite register the acceleration value.
		 * @param  usAcc variable where will be stored the acceleration value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t getAcceleration(uint16_t& usAcc);

		/*! **********************************************************************************************************************
		 * @brief  getDeceleration read from the apposite register the deceleration value.
		 * @param  usDec variable where will be stored the deceleration value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t getDeceleration(uint16_t& usDec);

		/*! **********************************************************************************************************************
		 * @brief  getMaxSpeed read from the apposite register the max speed value.
		 * @param  usMaxSpeed variable where will be stored the max speed value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t getMaxSpeed(uint16_t& usMaxSpeed);

		/*! **********************************************************************************************************************
		 * @brief  getMinSpeed read from the apposite register the min speed value.
		 * @param  usMinSpeed variable where will be stored the min speed value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t getMinSpeed(uint16_t& usMinSpeed);

		/*! **********************************************************************************************************************
		 * @brief  getFullSpeed read from the apposite register the full speed value.
		 * @param  usFullSpeed variable where will be stored the full speed value.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t getFullSpeed(uint16_t& usFullSpeed);

		/*! **********************************************************************************************************************
		 * @brief  getAbsPosition read from the apposite register the absolute position.
         * @param  liAbsPos variable where will be stored the absolute position.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t getAbsPosition(int& liAbsPos);

		/*! **********************************************************************************************************************
		 * @brief  getStepModePosition read from the apposite register the config of the step mode.
         * @param  ucStepMode variable where will be stored the step mode val.
		 * @return 0 if success, -3 if communication error, -4 if m_bConfigOK = false.
		 * ***********************************************************************************************************************
		 */
		int8_t getStepResolution(uint8_t& ucStepMode);

		/*! **********************************************************************************************************************
		 * @brief  isMotorEnabled wrapper to m_bIsMotorEnabled variable. Tells if the current is 0 or not.
		 * @return true if motor is enabled, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool isMotorEnabled(void);

		/*! **********************************************************************************************************************
		 * @brief  isMotorRunningOk check fault line if is high or low.
		 * @return true if motor is ok, false in case of failure.
		 * ***********************************************************************************************************************
		 */
		bool isMotorRunningOk(void);


		// Data Getters

		/*! **********************************************************************************************************************
		 * @brief readMotorResolution
		 * ***********************************************************************************************************************
		 */
		int8_t		readMotorResolution();

		/*! **********************************************************************************************************************
		 * @brief readMotorHighCurrent
		 * ***********************************************************************************************************************
		 */
		uint16_t	readMotorHighCurrent();

		/*! **********************************************************************************************************************
		 * @brief readMotorLowCurrent
		 * ***********************************************************************************************************************
		 */
		uint16_t	readMotorLowCurrent();

		/*! **********************************************************************************************************************
		 * @brief readMotorPosition
		 * ***********************************************************************************************************************
		 */
		int32_t		readMotorPosition();

		/*! **********************************************************************************************************************
		 * @brief readAccelerationFactor
		 * ***********************************************************************************************************************
		 */
		int8_t		readAccelerationFactor();

		/*! **********************************************************************************************************************
		 * @brief readConversionFactor
		 * ***********************************************************************************************************************
		 */
		uint32_t	readConversionFactor();

    private :

        int8_t		m_intResolution;
        uint16_t	m_intHighCurrent, m_intLowCurrent;
        int32_t		m_intPosition;

};

#endif // L6472GENERAL_H
