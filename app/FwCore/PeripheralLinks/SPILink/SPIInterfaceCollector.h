/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SPIInterfaceCollector.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SPIInterfaceCollector class.
 @details

 ****************************************************************************
*/

#ifndef SPIINTERFACECOLLECTOR_H
#define SPIINTERFACECOLLECTOR_H


#include <linux/spi/spidev.h>

#include "SPIInterface.h"
#include "CommonInclude.h"
#include "SPILinkCommonInclude.h"
#include "Config.h"


typedef enum
{

	eSPI_0 = 0,
	eSPI_1 = 1,
	eSPI_2 = 2,
	eSPI_3 = 3,

} enumSPIInterface;


class SPIInterfaceCollector : public Loggable
{
	public:

		/*! **********************************************************************************************************************
		 * @brief SPIInterfaceCollector default constructor.
		 * ***********************************************************************************************************************
		 */
		SPIInterfaceCollector();

		/*! **********************************************************************************************************************
		 * @brief ~SPIInterfaceCollector virtual destructor.
		 * ***********************************************************************************************************************
		 */
		virtual ~SPIInterfaceCollector();

		/*! **********************************************************************************************************************
		 * @brief  createInterfaces creates all the SPI Interfaces.
		 * @param  sConfigFile CONF_FILE from which parameters are read.
		 * @return 0 in case of success, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int8_t createAndInitInterfaces(const char* sConfigFile);

		/*! **********************************************************************************************************************
		 * @brief  getInterface retrieve the requested SPI interface.
         * @param  eSPIIfr enum of SPIchannel interface.
		 * @return pointer to SPIInterface object. In case of unsuccess it is NULL.
		 * ***********************************************************************************************************************
		 */
		SPIInterface* getInterface(enumSPIInterface eSPIIfr);

		/*! **********************************************************************************************************************
		 * @brief destroyInterfaces destroy all the interfaces created with createInterface.
		 * ***********************************************************************************************************************
		 */
		void destroyInterfaces(void);

	private:

		/*! **********************************************************************************************************************
		 * @brief  readConfigFile reads configuratin file and stores the info in m_Config.
		 * @param  sConfigFileName configuration file to be read.
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool readConfigFile(const char* sConfigFileName);

    public:

        // Needed to handle the BUSY line of the NSH motor
		uint8_t	m_NSHMotorBusyGpioIdx;
		// Needed to handle the FAULT line of the NSH motor
		uint8_t m_NSHMotorFaultGpioIdx;
        // Needed to handle the DRDY line of the NSH reader
        uint8_t	m_NSHReaderDRDYGpioIdx;
        // Needed to handle the reset line of the NSH reader
        uint8_t	m_NSHReaderResetGpioIdx;

    private:

        SPIInterface* m_pSPI1;
        SPIInterface* m_pSPI2;
        SPIInterface* m_pSPI3;
        Config		  m_Config;

};

#endif // SPIINTERFACECOLLECTOR_H
