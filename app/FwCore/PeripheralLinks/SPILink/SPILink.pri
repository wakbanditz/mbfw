INCLUDEPATH += $$PWD/../SPILink/
INCLUDEPATH += $$PWD/../SPILink/NSHMotor/
INCLUDEPATH += $$PWD/../SPILink/NSHReader/
INCLUDEPATH += $$PWD/../SPILink/NSHEncoder/

HEADERS += \
        $$PWD/NSHMotor/NSHMotorGeneral.h \
        $$PWD/SPILinkBoard.h \
        $$PWD/SPIInterfaceCollector.h \
        $$PWD/SPILinkCommonInclude.h \
        $$PWD/NSHReader/NSHReaderCommonInclude.h \
        $$PWD/NSHReader/NSHReaderDRDY.h \
        $$PWD/NSHReader/NSHReaderGeneral.h \
        $$PWD/NSHReader/NSHReaderLink.h \
        $$PWD/NSHReader/NSHReaderProtocol.h \
        $$PWD/NSHReader/NSHReaderMessageBlock.h \
        $$PWD/NSHReader/NSHReaderFwUpdate.h \
        $$PWD/NSHEncoder/LS7366RBase.h \
        $$PWD/NSHEncoder/LS7366RCommonInclude.h \
        $$PWD/NSHEncoder/LS7366RGeneral.h \
        $$PWD/NSHMotor/L6472Base.h \
        $$PWD/NSHMotor/L6472General.h \
        $$PWD/NSHMotor/L6472Library.h \
        $$PWD/SPIErrors.h

SOURCES += \
        $$PWD/NSHMotor/NSHMotorGeneral.cpp \
        $$PWD/SPILinkBoard.cpp \
        $$PWD/SPIInterfaceCollector.cpp \
        $$PWD/NSHReader/NSHReaderDRDY.cpp \
        $$PWD/NSHReader/NSHReaderGeneral.cpp \
        $$PWD/NSHReader/NSHReaderLink.cpp \
        $$PWD/NSHReader/NSHReaderProtocol.cpp \
        $$PWD/NSHReader/NSHReaderMessageBlock.cpp \
        $$PWD/NSHReader/NSHReaderFwUpdate.cpp \
        $$PWD/NSHEncoder/LS7366RBase.cpp \
        $$PWD/NSHEncoder/LS7366RGeneral.cpp \
        $$PWD/NSHMotor/L6472Base.cpp \
        $$PWD/NSHMotor/L6472General.cpp \
        $$PWD/SPIErrors.cpp





