/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    USBErrors.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the USBErrors class.
 @details

 ****************************************************************************
*/

#ifndef CR8062ERRORS_H
#define CR8062ERRORS_H

#include "ErrorUtilities.h"
#include "CameraError.h"
#include "Log.h"

class USBErrors
{
	public:

		/*! *************************************************************************************************
		 * @brief CR8062Errors default constructor
		 * **************************************************************************************************
		 */
		USBErrors();

		/*! *************************************************************************************************
		 * @brief ~CR8062Errors virtual destructor
		 * **************************************************************************************************
		 */
		virtual ~USBErrors();

		/*! *************************************************************************************************
		 * @brief  initCameraErr init class building ErrorVector
		 * @param  pLogger pointer to logger
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
		int initCameraErr(Log *pLogger);

		/*! *************************************************************************************************
		 * @brief clearAllErrors set inactive all the errors
		 * **************************************************************************************************
		 */
		void clearAllErrors(void);

		/*! *************************************************************************************************
		 * @brief  updateErrorsTime update time
		 * @return num of timings updated
		 * **************************************************************************************************
		 */
		int updateErrorsTime(uint64_t ulCurrentMsec, struct tm * currentTime);

		/*! *************************************************************************************************
		 * @brief  getErrorList function used to retrieve the active errors
		 * @param  pVectorList pointer to the error vector to be updated
         * @param errorLevel the level of errors to retrieve
         * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
        int getErrorList(vector<string> * pVectorList, uint8_t errorLevel);

		/*! *************************************************************************************************
		 * @brief  decodeNotifyEvent manages notify event message
		 * @param  liEventCode code related to the error
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool decodeNotifyEvent(int liEventCode);

	private:

		/*! *************************************************************************************************
		 * @brief  buildErrorVector build the vector parsing error file
		 * @return size of error vector
		 * **************************************************************************************************
		 */
		int buildErrorVector(void);

		/*! *************************************************************************************************
		 * @brief  searchEventError search error among the list
		 * @param  strErrCode error string
		 * @param  pCameraErrorFound error class related to the wanted one
		 * @return 0 if error is found & active, 1 if is found but not active, -1 if there is no such an err
		 * **************************************************************************************************
		 */
		int searchEventError(const string& strErrCode, CameraError*& pCameraErrorFound);

    private:

        Log						*m_pLogger;
        vector< CameraError* >	m_vectorErrors;

};

#endif // CR8062ERRORS_H
