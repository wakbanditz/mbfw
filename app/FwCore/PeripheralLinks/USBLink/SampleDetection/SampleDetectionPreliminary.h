/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionPreliminary.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SampleDetectionPreliminary class.
 @details

 ****************************************************************************
*/

#ifndef SAMPLEDETECTIONPRELIMINARY_H
#define SAMPLEDETECTIONPRELIMINARY_H

#include "SampleDetectionBase.h"

typedef struct
{
	int32_t ulXLeft;
	int32_t ulXRight;
} structBlackLimits;


class SampleDetectionPreliminary : public SampleDetectionBase
{
	public:

		/*! *************************************************************************************************
		 * @brief SampleDetectionPreliminary default constructor
		 * **************************************************************************************************
		 */
		SampleDetectionPreliminary();

		/*! *************************************************************************************************
		 * @brief ~SampleDetectionPreliminary default constructor
		 * **************************************************************************************************
		 */
		virtual ~SampleDetectionPreliminary();

		/*! *************************************************************************************************
		 * @brief  init init the class
		 * @param  pLogger pointer to logger
		 * @param  pZeroZeroCropRect pointer to crop rectangle to be saved as function member
		 * @param  cWellNum well number
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool init(Log *pLogger, cropRectangle* pZeroZeroCropRect, int8_t cWellNum);

		/*! *************************************************************************************************
		 * @brief  performPreliminaryChecks check if there is white, lateral boundaries and foil presence
		 * @param  pulData pointer to the full image
		 * @param  vCroppedData cropped image obtained from the function
		 * @param  uliImageWidth width of the full image
		 * @param  bLiquid liquid presence if true
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool performPreliminaryChecks(uint32_t* pulData, vector<uint32_t>& vCroppedData, uint32_t uliImageWidth, bool& bLiquid);

		/*! *************************************************************************************************
		 * @brief  getDiff get the shift along y direction due to the foil
		 * @return offset value
		 * **************************************************************************************************
		 */
		int8_t getDiff(void);

		/** *************************************************************************************************
		 * @brief  getDiffVerX0 get the shift along y direction due to the foil X0 vertical algo
		 * @return offset value
		 * **************************************************************************************************
		 */
		int8_t getDiffVerX0(void);

		/** *************************************************************************************************
		 * @brief getXShift getter method to get the shift along x direction
		 * @return shift in pixels
		 * **************************************************************************************************
		 */
		int32_t getXShift(void);

		/*! *************************************************************************************************
		 * @brief clearAll reset all the class
		 * **************************************************************************************************
		 */
		void clearAll(void);


	private:

		/*! *************************************************************************************************
		 * @brief  cropImage crop the image
		 * @param  pulData pointer to the full data
		 * @param  pulCroppedData pointer to the data where the cropped image will be stored
		 * @param  uliImageWidth width of the full image
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool cropImage(uint32_t* pulData, uint32_t* pulCroppedData, uint32_t uliImageWidth);

		/*! *************************************************************************************************
		 * @brief  checkFastLiquidPresence check presence of liquid simply looking at the center of the image.
		 *		   If >20% or >30% of the pixels are over a certain threshold than there is the liquid
		 * @param  pulData pointer to the data
		 * @param  ucWell well number
		 * @return percentage of pixels over the threshold
		 * **************************************************************************************************
		 */
		float checkFastLiquidPresence(uint32_t* pulData, uint8_t ucWell);

		/*! *************************************************************************************************
		 * @brief  checkLateralBoundaries check label presence on the border
		 * @param  vData image passed as vector
		 * @return struct with eventual offset found
		 * **************************************************************************************************
		 */
		structBlackLimits checkLateralBoundaries(vector<uint32_t>& vData);

		/*! *************************************************************************************************
		 * @brief  findFoilPresence find presence of the foil
		 * @param  pulData pointer to the data
		 * @param  pZeroZeroCropRect pointer to the crop rectangle
		 * @param  bBoundChanged tell if foil has been found and boundaries have been changed
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool findFoilPresence(uint32_t* pulData, cropRectangle* pZeroZeroCropRect, bool& bBoundChanged);

		/*! *************************************************************************************************
		 * @brief  scanImageAndCreateWaves function used to get waves from images
		 * @param  pulData pointer to the image
		 * @param  pulWaves pointer to the wave to be created
		 * @param  uliStartIndex start index from where getting the waves
		 * @param  uliStopIndex stop index from where getting the waves
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool scanImageAndCreateWaves(uint32_t* pulData, uint32_t* pulWaves, uint32_t uliStartIndex, uint32_t uliStopIndex, uint8_t);

		/*! *************************************************************************************************
		 * @brief  findPeaks find all the peaks in the data passed
		 * @param  pulData pointer to data
		 * @param  uliSize size of the data to be considered
		 * @param  pPeaksStruct pointer to peaks struct where the result is stored
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool findPeaks(uint32_t* pulData, uint32_t uliSize, peaksStruct* pPeaksStruct);

		/*! *************************************************************************************************
		 * @brief  foilDiscover the idea is that the foil is similar to a circumference arch. The aim of the
		 *		   function is to find out if we can be confident enough that some artifacts are due to the
		 *		   foil
		 * @param  pulData pointer to data
		 * @param  xPeak0 X coordinate of the first point where the circumference should pass
		 * @param  yPeak0 Y coordinate of the first point where the circumference should pass
		 * @param  xPeak1 X coordinate of the second point where the circumference should pass
		 * @param  yPeak1 Y coordinate of the second point where the circumference should pass
		 * @param  pCoefficients coefficients of the circumference if found, alse all zeros
		 * @return percentage of similitude to the foil
		 * **************************************************************************************************
		 */
		float foilDiscover(uint32_t* pulData, uint32_t xPeak0, uint32_t yPeak0, uint32_t xPeak1, uint32_t yPeak1, circumferenceCoefficients* pCoefficients);

		/*! *************************************************************************************************
		 * @brief  getCircumferenceParameters get the parameter of the circumference a, b and c starting from
		 *		   the 2 point and the radius. (x^2 + y^2 + ax + by + c = 0)
		 * @param  x0 X coordinate of the first point where the circumference should pass
		 * @param  y0 Y coordinate of the first point where the circumference should pass
		 * @param  x1 X coordinate of the second point where the circumference should pass
		 * @param  y1 yPeak1 Y coordinate of the second point where the circumference should pass
		 * @param  r radius of the circumf
		 * @param  strUpDown to know if we are looking for the upper or lower half of the circ
		 * @param  pCoefficients pointer to the coeff struct that should be the output
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getCircumferenceParameters(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int8_t r, string strUpDown, circumferenceCoefficients* pCoefficients);

		/*! *************************************************************************************************
		 * @brief  findClosest to get the id of the peak closest to the value asked
		 * @param  pPeaksStruct pointer to peaks struct
		 * @param  value compared to the peak is closest
		 * @param  strDirection to know if we are looking for the upper or lower half of the circ
		 * @return the ID in case of success, otherwise -1
		 * **************************************************************************************************
		 */
		int32_t findClosest(peaksStruct* pPeaksStruct, int32_t value, string strDirection);

		/*! *************************************************************************************************
		 * @brief  solveEyePatternIssue check if there is an "eye" effect. Filter the image and then calls
		 *		   findEyePatternAndFill
		 * @param  pulData pointer to data
		 * @param  bHasEyePattern result of the function
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool solveEyePatternIssue(uint32_t* pulData, bool& bHasEyePattern);

		/*! *************************************************************************************************
		 * @brief  findEyePatternAndFill check if there is an "eye" effect, like  ________		___  ___
				   If it is the case, fill the picture avoiding the issue			  __          <__>
				   due to the reflection.										   __/  \__		__/  \__
		 * @param  pulData pointer to data that will be filled if the "eye" is present
		 * @param  rgImFiltered pointer to the filtered image
		 * @param  bHasEyePattern result of the function
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool findEyePatternAndFill(uint32_t* pulData, uint32_t* rgImFiltered, bool& bHasEyePattern);

		/*! *************************************************************************************************
		 * @brief  findPatternLowerEdge function used to find out if there is liquid and if the pattern is
		 *		   shifted along y direction
		 * @param  vImage vector with the image
		 * @param  liXShift shift due to the boundaries check
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool findPatternLowerEdge(vector<uint32_t>& vImage, int32_t liXShift);

		/*! *************************************************************************************************
		 * @brief  patternSelector function that isolates the pattern
		 * @param  vData reference to the image where we want to study
		 * @param  vFilteredImage image filtered with the pattern isolated
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool patternSelector(const vector<uint32_t>& vData, vector<uint32_t>& vFilteredImage);

		/*! *************************************************************************************************
		 * @brief  findOutliers given a vector of data, return a bool vector that indicates if each element is
		 *		   an outlier or not
		 * @param  vData data to be analyzed
		 * @param  vIsOutlier tells if the elements are outliers (=true) or not (=false)
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool findOutliers(vector<uint32_t>& vData, vector<bool>& vIsOutlier);

    public:

        int32_t m_nImageSize;
        int32_t m_nInitialWidth;

    private:

        bool	m_bConfigOK;
        int32_t	m_nXShift;
        int8_t	m_cDiff;
        int8_t	m_cDiffVert;

};

#endif // SAMPLEDETECTIONPRELIMINARY_H
