/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionPreliminary.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SampleDetectionPreliminary class.
 @details

 ****************************************************************************
*/

#include <algorithm>

#include "SampleDetectionPreliminary.h"
#include "limits.h"

#define SAMPLE_DETECTION_FAST_CHECK_WIDTH			8
#define SAMPLE_DETECTION_FAST_CHECK_HEIGHT			8
#define SAMPLE_DETECTION_FAST_CHECK_THRESH_X0		220000
#define SAMPLE_DETECTION_FAST_CHECK_THRESH_X3		220000
#define SAMPLE_DETECTION_FAST_CHECK_PERC_THRESH_X0	20.0f
#define SAMPLE_DETECTION_FAST_CHECK_PERC_THRESH_X3	30.0f
#define SAMPLE_DETECTION_FAST_CHECK_MAGIC_NUM		10
#define SAMPLE_DETECTION_LOW_DIFF					6
#define SAMPLE_DETECTION_HIGH_DIFF					8
#define SAMPLE_DETECTION_CONTINUITY_CHECK_THRESH	240000

#define MIN_Y_CABLED							4
#define MAX_Y_CABLED							10
#define MAX_WIDTH_PATTERN_CABLED				12
#define REALSTART_CABLED						9
#define REALSTOP_CABLED							23

#define CENTER_EYE_CABLED						12
#define CENTER_EYE_MAGIC_VALUE_1				1
#define CENTER_EYE_MAGIC_VALUE_7				7
#define CENTER_EYE_MAGIC_VALUE_10				10
#define CENTER_EYE_MAGIC_VALUE_13				13
#define CENTER_EYE_MAGIC_VALUE_24				24
#define CENTER_EYE_MAGIC_VALUE_29				29
#define CENTER_EYE_X_POSITIONS_NUM				8

#define PATTERN_WIDTH							8
#define HISTOGRAM_BINS_NUM						10
#define SAMPLE_DETECTION_PATTERN_CHECK_THRESH	( 230 * DECIMAL_DIGITS_FACTOR )
#define SAMPLE_DETECTION_PATTERN_CHECK_THRESH_2	( 240 * DECIMAL_DIGITS_FACTOR )
#define SAMPLE_DETECTION_OUTLIER_ACCEPT			0.86f


SampleDetectionPreliminary::SampleDetectionPreliminary()
{
	m_nImageSize = -1;
	m_nInitialWidth = -1;
	m_nXShift = 0;
	m_cDiff = 0;
	m_cDiffVert = 0;
	m_bConfigOK = false;
}

SampleDetectionPreliminary::~SampleDetectionPreliminary()
{

}

bool SampleDetectionPreliminary::init(Log* pLogger, cropRectangle* pZeroZeroCropRect, int8_t cWellNum)
{
	m_bConfigOK = false;

	if ( ! setLogger(pLogger) )						return false;
	if ( ! setCropRectangle(pZeroZeroCropRect) )	return false;
	if ( ! setWellNumber(cWellNum) )				return false;


	setImageWidth(pZeroZeroCropRect->width);
	setImageHeight(pZeroZeroCropRect->height);
	m_nInitialWidth = m_nImageWidth;
	m_nImageSize = m_nImageHeight * m_nImageWidth;
	m_bConfigOK = true;

	return true;
}

bool SampleDetectionPreliminary::cropImage(uint32_t* pulData, uint32_t* pulCroppedData, uint32_t uliImageWidth)
{
	if ( pulData == NULL )			return false;
	if ( pulCroppedData == NULL )	return false;
	if ( m_bConfigOK == false )		return false;

	uint32_t uliStartCropX = m_cropRectangle.xStart;;
	uint32_t uliStartCropY = m_cropRectangle.yStart;
	uint32_t uliEndCropX = m_cropRectangle.xStart + m_cropRectangle.width;
	uint32_t uliEndCropY = m_cropRectangle.yStart + m_cropRectangle.height;
	uint32_t uliSize =  m_cropRectangle.width * m_cropRectangle.height;

	uint32_t uliPositionFullImage;

	for ( uint32_t i = uliStartCropY; i < uliEndCropY; ++i )
	{
		for ( uint32_t j = uliStartCropX; j < uliEndCropX; ++j  )
		{
			uliPositionFullImage = i * uliImageWidth + j;
			*pulCroppedData = *(pulData + uliPositionFullImage);
			++pulCroppedData;
		}
	}
	pulCroppedData -= uliSize;

	return true;
}

bool SampleDetectionPreliminary::performPreliminaryChecks(uint32_t* pulData, vector<uint32_t>& vCroppedData, uint32_t uliImageWidth, bool& bLiquid)
{
	if ( pulData == nullptr )		return false;
	if ( vCroppedData.empty() )		return false;
	if ( m_bConfigOK == false )		return false;

	bLiquid = false;

	cropImage(pulData, vCroppedData.data(), uliImageWidth);

	structBlackLimits lateralLimits;
	lateralLimits = checkLateralBoundaries(vCroppedData);
	if ( lateralLimits.ulXLeft > 0 )
	{
		// update variables
		m_nXShift = lateralLimits.ulXLeft;
		m_cropRectangle.xStart += lateralLimits.ulXLeft;
		m_cropRectangle.width -= lateralLimits.ulXLeft;
		m_nImageWidth = m_cropRectangle.width;
		m_nImageSize = m_cropRectangle.width*m_cropRectangle.height;
		vCroppedData.resize(m_nImageSize);
		cropImage(pulData, vCroppedData.data(), uliImageWidth);
	}
	else if ( lateralLimits.ulXRight > 0 )
	{
		// update all variables, but m_nXShift.
		// I need to update it only if the left part of the image is cropped, so that also the center is shifted.
		// If it is cropped the right part, the real center (and the other points) remains the same.
		m_cropRectangle.width -= (lateralLimits.ulXRight);
		m_nImageWidth = m_cropRectangle.width;
		m_nImageSize = m_cropRectangle.width*m_cropRectangle.height;
		vCroppedData.resize(m_nImageSize);
		cropImage(pulData, vCroppedData.data(), uliImageWidth);
	}

	// Foil presence and pattern deformation are checked in X0 only
	if ( getWellNumber() == WELL_ZERO )
	{
		bool bBoundHasChanged = false;
		cropRectangle tmpCropRectangle = m_cropRectangle;

		if ( ! findFoilPresence(vCroppedData.data(), &tmpCropRectangle, bBoundHasChanged) )		return false;
		if ( bBoundHasChanged )
		{
			m_cDiff = (int)(m_cropRectangle.yStart - tmpCropRectangle.yStart);
			m_cDiffVert = m_cDiff;
			m_cropRectangle = tmpCropRectangle;
			cropImage(pulData, vCroppedData.data(), uliImageWidth);
		}

		// strong pattern deformation is present only in X0
		bool bHasEyePattern = false;
		solveEyePatternIssue(vCroppedData.data(), bHasEyePattern);
	}

	bool bRes = findPatternLowerEdge(vCroppedData, lateralLimits.ulXLeft);
	if ( ! bRes )
	{
		bLiquid = true;
		return true;
	}

	return true;
}

int8_t SampleDetectionPreliminary::getDiff()
{
	if ( m_bConfigOK == false )
	{
		return 0;
	}

	return m_cDiff;
}

int8_t SampleDetectionPreliminary::getDiffVerX0()
{
	if ( m_bConfigOK == false )
	{
		return 0;
	}

	return m_cDiffVert;
}

int32_t SampleDetectionPreliminary::getXShift()
{
	return m_nXShift;
}

void SampleDetectionPreliminary::clearAll()
{
	m_nImageSize = -1;
	m_cDiff = 0;
	m_bConfigOK = false;
	m_pLogger = 0;
	m_cWellNumber = -1;
	memset(&m_cropRectangle, 0x00, sizeof(cropRectangle));
}

float SampleDetectionPreliminary::checkFastLiquidPresence(uint32_t* pulData, uint8_t ucWell)
{
	if ( pulData == nullptr )		return false;
	if ( m_bConfigOK == false )		return false;

	float fLiquidPercentage = 0;

	switch ( ucWell )
	{
		case WELL_ZERO:
		{
			uint8_t ucWidth = SAMPLE_DETECTION_FAST_CHECK_WIDTH;
			uint8_t ucHeight = SAMPLE_DETECTION_FAST_CHECK_HEIGHT;

			int xStart = round(m_nImageWidth/2 - ucWidth/2);
			int xEnd = round(m_nImageWidth/2 + ucWidth/2);
			int yStart = round(m_nImageHeight/2 - ucHeight/2);
			int yEnd = round(m_nImageHeight/2 + ucHeight/2);
			if ( xStart < 0 || yStart < 0 )		return 0;

			int liValue = 0;

			for ( int i = yStart; i <= yEnd; i++ )
			{
				for ( int j = xStart; j <= xEnd; j++ )
				{
					liValue = *(pulData + i*m_nImageWidth + j);
					if ( liValue > SAMPLE_DETECTION_FAST_CHECK_THRESH_X0 )
					{
						fLiquidPercentage++;
					}
				}
			}

			fLiquidPercentage *= 100;
			fLiquidPercentage /=  ( ( xEnd - xStart + 1 ) * ( yEnd - yStart + 1 ) );
			break;
		}

		case WELL_THREE:
		{
			int liDelta = 10;
			int xStart = liDelta;
			int xEnd = m_nImageWidth - liDelta;
			int yStart = m_nImageHeight - liDelta - 3;
			int yEnd = m_nImageHeight - 3;
			if ( ( xStart < 0 || yStart < 0 ) || xStart > m_nImageWidth )		return 0;

			int liValue = 0;

			for ( int i = yStart; i <= yEnd; i++ )
			{
				for ( int j = xStart; j <= xEnd; j++ )
				{
					liValue = *(pulData + i*m_nImageWidth + j);
					if ( liValue > SAMPLE_DETECTION_FAST_CHECK_THRESH_X3 )
					{
						fLiquidPercentage++;
					}
				}
			}

			fLiquidPercentage *= 100;
			fLiquidPercentage /=  ( ( xEnd - xStart + 1 ) * ( yEnd - yStart + 1 ) );
			break;
		}
	}

	return fLiquidPercentage;
}

structBlackLimits SampleDetectionPreliminary::checkLateralBoundaries(vector<uint32_t>& vData)
{
	if ( vData.empty() )			return {-1,-1};
	if ( m_bConfigOK == false )		return {-1,-1};
	if ( m_nImageWidth < 2*SAMPLE_DETECTION_FAST_CHECK_MAGIC_NUM )	return {-1,-1};

	structBlackLimits limits;
	limits.ulXLeft = 0;
	limits.ulXRight = 0;
	unsigned long liThreshold = 240 * DECIMAL_DIGITS_FACTOR;
	unsigned long liShift = 0;
	unsigned long liCntWhite = 0;

	for ( uint8_t i = 0; i < SAMPLE_DETECTION_FAST_CHECK_MAGIC_NUM; i++ )
	{
		int yIdx = (i+1) * floor(m_nImageHeight / SAMPLE_DETECTION_FAST_CHECK_MAGIC_NUM) - 1;

		if ( yIdx < 0 )							yIdx = 0;
		else if ( yIdx == m_nImageHeight )		yIdx -= 2;

		if ( vData[yIdx*m_nImageWidth] >= liThreshold )
		{
			liCntWhite++;
			for ( uint8_t j = 0; j != SAMPLE_DETECTION_FAST_CHECK_MAGIC_NUM; j++ )
			{
				if ( (vData[yIdx*m_nImageWidth+j] > liThreshold ) && ( j > liShift ) )
				{
					liShift = j;
				}
			}
		}
	}

	// If there is white for around 80% of the samples considered than there is the need to shift the crop.
	if ( liCntWhite >= (SAMPLE_DETECTION_FAST_CHECK_MAGIC_NUM-2) )
	{
		limits.ulXLeft = liShift + 1; // the shif is actually of these number of pixels, the counter starts from 0
		m_pLogger->log(LOG_INFO, "SampleDetectionPreliminary::checkLateralBoundaries: a new crop is needed, shifted of %d pixels.", liShift+1);
		return limits;
	}

	// Perform a check on the right.
	liShift = 0;
	liCntWhite = 0;

	for ( uint8_t i = 0; i < SAMPLE_DETECTION_FAST_CHECK_MAGIC_NUM; i++ )
	{
		int yIdx = (i+1) * floor(m_nImageHeight / SAMPLE_DETECTION_FAST_CHECK_MAGIC_NUM);

		if ( yIdx < 0 )							yIdx = 0;
		else if ( yIdx == m_nImageHeight )		yIdx -= 2;

		if ( vData[yIdx*m_nImageWidth-1] >= liThreshold )
		{
			liCntWhite++;
			for ( uint8_t j = 0; j != SAMPLE_DETECTION_FAST_CHECK_MAGIC_NUM; j++ )
			{
				if ( (vData[yIdx*m_nImageWidth-1-j] > liThreshold ) && ( j > liShift ) )
				{
					liShift = j;//m_nImageWidth-j-1;
				}
			}
		}
	}

	// If there is white for around 80% of the samples considered than there is the need to shift the crop.
	if ( liCntWhite >= (SAMPLE_DETECTION_FAST_CHECK_MAGIC_NUM-2) )
	{
		limits.ulXRight = liShift + 1; // the shif is actually of these number of pixels, the counter starts from 0
		m_pLogger->log(LOG_INFO, "SampleDetectionPreliminary::checkLateralBoundaries: a new crop is needed, shifted of %d pixels.", liShift+1);
	}

	return limits;
}

bool SampleDetectionPreliminary::findFoilPresence(uint32_t* pulData, cropRectangle* pZeroZeroCropRect, bool& bBoundHasChanged)
{
	bBoundHasChanged = false;

	if ( m_bConfigOK == false )			return false;
	if ( pulData == nullptr )			return false;
	if ( pZeroZeroCropRect == nullptr )	return false;

	vector<uint32_t> rgEdgeData(m_nImageSize);
	bool bRes = cannyFilter(pulData, rgEdgeData.data(), m_nImageWidth, m_nImageHeight);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionPreliminary::findFoilPresence: unable to apply canny filter");
		return bRes;
	}

	// Find ranges for the waves
	int liCenter = m_nImageWidth / 2 - 1;
	int liLowLimitLeft = liCenter - SAMPLE_DETECTION_HIGH_DIFF;
	int liHighLimitLeft = liCenter - SAMPLE_DETECTION_LOW_DIFF;
	int liLowLimitCenter= liCenter - 1;
	int liHighLimitCenter = liCenter + 1;
	int liLowLimitRight = liCenter + SAMPLE_DETECTION_LOW_DIFF;
	int liHighLimitRight = liCenter + SAMPLE_DETECTION_HIGH_DIFF;

	uint16_t usiArraySize = m_nImageHeight;
	vector<uint32_t> rguiWavesCenter(usiArraySize);
	vector<uint32_t> rguiWavesRight(usiArraySize);
	vector<uint32_t> rguiWavesLeft(usiArraySize);

	// Previously there was a different scanImageAndCreateWaves for X3
	scanImageAndCreateWaves(rgEdgeData.data(), rguiWavesLeft.data(), liLowLimitLeft, liHighLimitLeft, 0);
	scanImageAndCreateWaves(rgEdgeData.data(), rguiWavesCenter.data(), liLowLimitCenter,liHighLimitCenter, 0);
	scanImageAndCreateWaves(rgEdgeData.data(), rguiWavesRight.data(), liLowLimitRight, liHighLimitRight, 0);

	peaksStruct leftPeaks;
	peaksStruct centralPeaks;
	peaksStruct rightPeaks;

	bRes = findPeaks(rguiWavesLeft.data(), usiArraySize, &leftPeaks);

	if ( bRes )
	{
		bRes = findPeaks(rguiWavesCenter.data(), usiArraySize, &centralPeaks);
	}
	if ( bRes )
	{
		bRes = findPeaks(rguiWavesRight.data(), usiArraySize, &rightPeaks);
	}
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionPreliminary::findFoilPresence: error in finding peaks");
		return false;
	}

	// Clear for more heap space
	rguiWavesCenter.clear();
	rguiWavesRight.clear();
	rguiWavesLeft.clear();

	possibleFoil PossibleFoil;
	memset(&PossibleFoil, 0x00, sizeof(possibleFoil)); // set all to false

	struct Indexes {int liLeft; int liLeftId; int liCenter; int liCenterId; int liRight; int liRightId; };
	Indexes RealStart = {0, 0, 0, 0, 0, 0};
	Indexes RealStop  = {0, 0, 0, 0, 0, 0};

	if ( leftPeaks.peaksNum + centralPeaks.peaksNum + rightPeaks.peaksNum == 0 )
	{
		// no points where to check the foil presence, do nothing
	}
	else if ( ( ( leftPeaks.peaksNum == 1 ) && ( centralPeaks.peaksNum == 1 ) ) &&
			  ( rightPeaks.peaksNum == 1 ) )
	{
		// If there is only one peak that it may be the pattern, do nothing
	}
	else
	{
		unsigned long liMinAdmittedYStart = MIN_Y_CABLED;
		unsigned long liMaxAdmittedYStart = MAX_Y_CABLED;
		unsigned long liMaxWidthPattern = MAX_WIDTH_PATTERN_CABLED;

		RealStart.liLeft = 0;
		RealStop.liLeft = m_nImageHeight;
		RealStart.liLeftId = 0;
		RealStop.liLeftId = 0;

		// Consider only peaks inside the central threshold - LEFT
		for ( unsigned long i = 0; i != leftPeaks.peaksNum; i++ )
		{
			if ( ( leftPeaks.peaksPos[i] > liMinAdmittedYStart ) &&
				 ( leftPeaks.peaksPos[i] < liMaxAdmittedYStart ) &&
				 ( RealStart.liLeft == 0 ) )
			{
				RealStart.liLeft = leftPeaks.peaksPos[i];
				RealStart.liLeftId = i;
			}
			else if ( leftPeaks.peaksPos[i] < liMinAdmittedYStart )
			{
				PossibleFoil.cLeftLowLimit = 1;
			}
			else if ( ( leftPeaks.peaksPos[i] < (RealStart.liLeft+liMaxWidthPattern) ) &&
					  ( RealStart.liLeft > 0 ) )
			{
				RealStop.liLeft = leftPeaks.peaksPos[i];
				RealStop.liLeftId = i;
			}
			else
			{
				PossibleFoil.cLeftHighLimit = 1;
			}
		}

		if ( RealStart.liLeft == 0 )
		{
			RealStart.liLeft = REALSTART_CABLED;
			RealStart.liLeftId = findClosest(&leftPeaks, liMinAdmittedYStart, "down");
		}

		if ( RealStop.liLeft == m_nImageHeight )
		{
			RealStop.liLeft = REALSTOP_CABLED;
			RealStop.liLeftId = findClosest(&leftPeaks, RealStop.liLeft, "up");
		}

		// Consider only peaks inside the central threshold - CENTER
		RealStart.liCenter = 0;
		RealStop.liCenter = m_nImageHeight;
		RealStart.liCenterId = 0;
		RealStop.liCenterId = 0;
		for ( unsigned long i = 0; i != centralPeaks.peaksNum; i++ )
		{
			if ( ( centralPeaks.peaksPos[i] > liMinAdmittedYStart ) &&
				 ( centralPeaks.peaksPos[i] < liMaxAdmittedYStart ) &&
				 ( RealStart.liCenter == 0 ) )
			{
				RealStart.liCenter = centralPeaks.peaksPos[i];
				RealStart.liCenterId = i;
			}
			else if ( centralPeaks.peaksPos[i] < liMinAdmittedYStart )
			{
				PossibleFoil.cCenterLowLimit = 1;
			}
			else if ( ( centralPeaks.peaksPos[i] < (RealStart.liCenter+liMaxWidthPattern) ) &&
					  ( RealStart.liCenter > 0 ) )
			{
				RealStop.liCenter = centralPeaks.peaksPos[i];
				RealStop.liCenterId = i;
			}
			else
			{
				PossibleFoil.cCenterHighLimit = 1;
			}
		}

		if ( RealStart.liCenter == 0 )
		{
			RealStart.liCenter = REALSTART_CABLED;
			RealStart.liCenterId = findClosest(&centralPeaks, liMinAdmittedYStart, "down");
		}

		if ( RealStop.liCenter == m_nImageHeight )
		{
			RealStop.liCenter = REALSTOP_CABLED;
			RealStop.liCenterId = findClosest(&centralPeaks, RealStop.liCenter, "up");
		}

		// Consider only peaks inside the central threshold - RIGHT
		RealStart.liRight = 0;
		RealStop.liRight = m_nImageHeight;
		RealStart.liRightId = 0;
		RealStop.liRightId = 0;
		for ( unsigned long i = 0; i != rightPeaks.peaksNum; i++ )
		{
			if ( ( rightPeaks.peaksPos[i] > liMinAdmittedYStart ) &&
				 ( rightPeaks.peaksPos[i] < liMaxAdmittedYStart ) &&
				 ( RealStart.liRight == 0 ) )
			{
				RealStart.liRight = rightPeaks.peaksPos[i];
				RealStart.liRightId = i;
			}
			else if ( rightPeaks.peaksPos[i] < liMinAdmittedYStart )
			{
				PossibleFoil.cRightLowLimit = 1;
			}
			else if ( ( rightPeaks.peaksPos[i] < (RealStart.liRight+liMaxWidthPattern) ) &&
					  ( RealStart.liRight > 0 ) )
			{
				RealStop.liRight = rightPeaks.peaksPos[i];
				RealStop.liRightId = i;
			}
			else
			{
				PossibleFoil.cRightHighLimit = 1;
			}
		}

		if ( RealStart.liRight == 0 )
		{
			RealStart.liRight = REALSTART_CABLED;
			RealStart.liRightId = findClosest(&rightPeaks, liMinAdmittedYStart, "down");
		}

		if ( RealStop.liRight == m_nImageHeight )
		{
			RealStop.liRight = REALSTOP_CABLED;
			RealStop.liRightId = findClosest(&rightPeaks, RealStop.liRight, "up");
		}
	}

	int foilPresenceLow = PossibleFoil.cLeftLowLimit + PossibleFoil.cCenterLowLimit + PossibleFoil.cRightLowLimit;
	int foilPresenceHigh = PossibleFoil.cLeftHighLimit + PossibleFoil.cCenterHighLimit + PossibleFoil.cRightHighLimit;
	float fFoilPercentage = 0;

	// If I detected 1 point foreach scan where to build the circumference
	int32_t liLeftID = 0;
	int32_t liRightID = 0;

	if ( foilPresenceLow == 3 )
	{
		uint32_t uliScanLeftX = HALVE_AND_ROUND_UP(liLowLimitLeft + liHighLimitLeft);
		uint32_t uliScanRightX = HALVE_AND_ROUND_UP(liLowLimitRight + liHighLimitRight);

		liLeftID = ( RealStart.liLeftId == 0 ) ? RealStart.liLeftId : RealStart.liLeftId-1;
		liRightID = ( RealStart.liRightId == 0 ) ? RealStart.liRightId : RealStart.liRightId-1;

		if ( ( liLeftID == -1 ) || ( liRightID == -1) )
		{
			return false;
		}
		circumferenceCoefficients circCoef;
		fFoilPercentage = foilDiscover(rgEdgeData.data(), uliScanLeftX, leftPeaks.peaksPos[liLeftID], uliScanRightX, rightPeaks.peaksPos[liRightID], &circCoef);
	}

	if ( ( foilPresenceHigh == 3 ) && ( fFoilPercentage <= VER_FOIL_THRESHOLD ) )
	{
		uint32_t uliScanLeftX = HALVE_AND_ROUND_UP(liLowLimitLeft + liHighLimitLeft);
		uint32_t uliScanRightX = HALVE_AND_ROUND_UP(liLowLimitRight + liHighLimitRight);

		liLeftID = ( RealStop.liLeftId == (int)leftPeaks.peaksNum-1 ) ? RealStop.liLeftId : RealStop.liLeftId+1;
		liRightID = ( RealStop.liRightId == (int)rightPeaks.peaksNum-1 ) ? RealStop.liRightId : RealStop.liRightId+1;

		if ( ( liLeftID == -1 ) || ( liRightID == -1) )
		{
			return false;
		}

		circumferenceCoefficients circCoef;
		fFoilPercentage = foilDiscover(rgEdgeData.data(), uliScanLeftX, leftPeaks.peaksPos[liLeftID], uliScanRightX, rightPeaks.peaksPos[liRightID], &circCoef);
	}

	if ( fFoilPercentage > VER_FOIL_THRESHOLD )
	{
		int8_t cDelta = 3;
		if ( foilPresenceLow == 3 )
		{
			int yMin;
			if ( leftPeaks.peaksPos[liLeftID] > rightPeaks.peaksPos[liRightID] )
			{
				yMin = leftPeaks.peaksPos[liLeftID] + cDelta;
			}
			else
			{
				yMin = rightPeaks.peaksPos[liRightID] + cDelta;
			}

			if ( yMin < m_nImageHeight )
			{
				pZeroZeroCropRect->yStart += (int)( yMin - m_nImageHeight - 2 );
			}
			else
			{
				pZeroZeroCropRect->yStart += (int)( yMin - m_nImageHeight - 2 * cDelta );
			}
		}
		else
		{
			int yMax;
			if ( leftPeaks.peaksPos[liLeftID] > rightPeaks.peaksPos[liRightID] )
			{
				yMax = rightPeaks.peaksPos[liRightID] - cDelta;
			}
			else
			{
				yMax = leftPeaks.peaksPos[liLeftID] - cDelta;
			}
			// +1 below is needed for images with foil too close to the pattern
			pZeroZeroCropRect->yStart += (int)( yMax - (m_nImageHeight-1) + 1);
		}

		bBoundHasChanged = true;
		return true;
	}

	return true;
}

bool SampleDetectionPreliminary::scanImageAndCreateWaves(uint32_t* pulData, uint32_t* pulWaves, uint32_t uliStartIndex, uint32_t uliStopIndex, uint8_t)
{
	if ( pulData == NULL )						return false;
	if ( pulWaves == NULL )						return false;
	if ( uliStartIndex >= uliStopIndex )		return false;

	uint32_t uliImageHeight = m_cropRectangle.height;
	uint32_t uliImageWidth = m_cropRectangle.width;

	// Create Waves from pulData
	for (uint32_t y = 0; y < uliImageHeight; ++y)
	{
		for (uint32_t x = uliStartIndex; x <= uliStopIndex; ++x)
		{
			*(pulWaves + y) += *(pulData + uliImageWidth*y + x);
		}
		*(pulWaves + y) /= (uliStopIndex - uliStartIndex);
	}

	return true;
}

bool SampleDetectionPreliminary::findPeaks(uint32_t* pulData, uint32_t uliSize, peaksStruct* pPeaksStruct)
{
	if ( pulData == nullptr )			return false;
	if ( pPeaksStruct == nullptr )		return false;

	pPeaksStruct->peaksNum = 0;
	pPeaksStruct->centeredPeakId = 0;
	pPeaksStruct->peaksAmplitude.clear();
	pPeaksStruct->peaksPos.clear();

	/*!*************************  DESCRIPTION  *******************************************
	 * A point is considered to be a peak if is greater than its two neighboring.
	 * A point is also considered to be a peak if it's the first of n equal value and the
	 * previous one and the following one are smaller.
	 * If the point is the last but one element of the array and is greater than the
	 * previous one and equal to the next one, it is also considered as peak.
	*************************************************************************************/

	for (uint32_t i = 1; i < (uliSize-1); ++i)
	{
		if ( (*(pulData+i) > *(pulData+i-1)) && (*(pulData+i) > *(pulData+i+1)) )
		{
			pPeaksStruct->peaksNum++;
			pPeaksStruct->peaksAmplitude.push_back(*(pulData+i));
			pPeaksStruct->peaksPos.push_back(i);
		}
		else if ( i < (uliSize-2) )
		{
			if ( (*(pulData+i) > *(pulData+i-1)) && (*(pulData+i) == *(pulData+i+1)) && (*(pulData+i) >= *(pulData+i+2)) )
			{
				pPeaksStruct->peaksNum++;
				pPeaksStruct->peaksAmplitude.push_back(*(pulData+i));
				pPeaksStruct->peaksPos.push_back(i);
			}
		}
		else if ( (*(pulData+i) > *(pulData+i-1)) && (*(pulData+i) == *(pulData+i+1)) )
		{
			pPeaksStruct->peaksNum++;
			pPeaksStruct->peaksAmplitude.push_back(*(pulData+i));
			pPeaksStruct->peaksPos.push_back(i);
		}
	}

	return true;
}

float SampleDetectionPreliminary::foilDiscover(uint32_t* pulData, uint32_t xPeak0, uint32_t yPeak0, uint32_t xPeak1, uint32_t yPeak1, circumferenceCoefficients* pCoefficients)
{
	if ( pulData == nullptr )			return -2;
	if ( pCoefficients == nullptr )		return -2;

	// circuimference equation = x^2 + y^2 + ax + by + c = 0
	uint8_t r = VER_FOIL_RADIUS;	// radius in pixel
	uint32_t uliBoundary = m_cropRectangle.height / 2;

	if ( yPeak0 > uliBoundary && yPeak1 > uliBoundary )
		getCircumferenceParameters(xPeak0, yPeak0, xPeak1, yPeak1, r, "UP", pCoefficients);
	else if ( yPeak0 < uliBoundary && yPeak1 < uliBoundary )
		getCircumferenceParameters(xPeak0, yPeak0, xPeak1, yPeak1, r, "DOWN", pCoefficients);
	else
	{
		pCoefficients->a = 0;
		pCoefficients->b = 0;
		pCoefficients->c = 0;
		return 0;
	}

	int32_t xStart;
	int32_t xEnd;

	// Find extremes of the circ.
	if ( ((-pCoefficients->a)/2 - r) > 0 &&
		 ((-pCoefficients->a)/2 - r) < (m_cropRectangle.width - 2) ) // exclude that is bigger than xEnd
		xStart = ceil( (-pCoefficients->a) / 2 - r );				// Matlab index starts from 1
	else
		xStart = 0;

	if ( (-pCoefficients->a)/2 + r < (m_cropRectangle.width - 1) &&
		 (-pCoefficients->a)/2 + r > 0 )							// exclude errors
		xEnd = floor( (-pCoefficients->a)/2 + r );
	else
		xEnd = m_cropRectangle.width - 2;


	// Array used to save the y position of the circ.
	vector<int32_t> yDown(xEnd - xStart);
	vector<int32_t> yUp(xEnd - xStart);
	int32_t y0, y1;
	float fDelta;

	for (int32_t x = xStart; x < xEnd; ++x)
	{
		fDelta = pow(pCoefficients->b,2) - 4 *(pow(x,2) + pCoefficients->a*x + pCoefficients->c);

		if ( fabs(fDelta) < 1 )
			fDelta = 0;

		if ( fDelta < 0 )
		{
			pCoefficients->a = 0;
			pCoefficients->b = 0;
			pCoefficients->c = 0;
			return 0;
		}

		// It is necessary to calculate y0 and y1 in 2 steps because of int-float conversion.
		y0 = -pCoefficients->b + sqrt(fDelta);	// first half of the circ.
		y0 = HALVE_AND_ROUND_UP(y0);
		y1 = -pCoefficients->b - sqrt(fDelta);
		y1 = HALVE_AND_ROUND_UP(y1);			// second half of the circ.
		yDown[x - xStart] = y0;
		yUp[x - xStart] = y1;
	}

	// Pattern found: do the high values stand in the circular crown (height 5)?
	uint8_t  cDeltaCrown = VER_HALF_CROWN;
	uint16_t usiSamplesNotToBeConsidered = 0;
	float fFoilPercentage = 0.0f;
	int16_t yValue;
	// pArray is a pointer to a vector composed as follows: deltaCrown bytes (vertical) for each point from xStart to xEnd.
	vector<uint32_t> vArray(2*cDeltaCrown*(xEnd-xStart));

	for (int32_t x = xStart; x < xEnd; ++x)
	{
		yValue = yDown[x - xStart];

		for (uint32_t i = 0; i < 2*cDeltaCrown; ++i)
		{
			if ((int)(yValue + i - cDeltaCrown) >= 0)
			{
				if ((yValue + i - cDeltaCrown + 2 ) < m_cropRectangle.height)
					vArray[(2*cDeltaCrown)*(x-xStart) + i] = *(pulData +  m_cropRectangle.width*(yValue+i-cDeltaCrown + 1) + x);
			}
			else									// if the sample stands out the cropped image, then is obvious that the point
				++usiSamplesNotToBeConsidered;		//  has not to be considered when calculating the total length
		}

		uint32_t uilMax = 0;
		uint32_t uilMin = UINT32_MAX;

		for (uint32_t cnt = 0; cnt < 2*cDeltaCrown; ++cnt)
		{
			if ( uilMax < vArray[cnt+(2*cDeltaCrown)*(x-xStart)] )
				uilMax = vArray[cnt+(2*cDeltaCrown)*(x-xStart)];

			if ( uilMin > vArray[cnt+(2*cDeltaCrown)*(x-xStart)] )
				uilMin = vArray[cnt+(2*cDeltaCrown)*(x-xStart)];
		}

		if ( (uilMax - uilMin) >= 1*DECIMAL_DIGITS_FACTOR )		++fFoilPercentage;
	}

	fFoilPercentage =  fFoilPercentage * 100 / (xEnd - xStart - 1 - usiSamplesNotToBeConsidered);

	return fFoilPercentage;
}

bool SampleDetectionPreliminary::getCircumferenceParameters(int32_t x0, int32_t y0, int32_t x1, int32_t y1, int8_t r, string strUpDown, circumferenceCoefficients* pCoefficients)
{
	// Circumference equation x^2 + y^2 + ax + by + c = 0
	// Radius equation r = sqrt((a^2 + b^2)/4 - c)
	// Center of the circumf C = (-a/2, -b/2)

	if ( pCoefficients == NULL )	return false;
	if ( x0 == x1 )					return false; // otherwise m and other variables are NaN.

	// NOTE. cast is needed, otherwise the function gives completely wrong results.
	float m = (float)(y1 - y0) / (x0 - x1);
	float h = (float)( pow(x0,2) - pow(x1,2) + pow(y0,2) - pow(y1,2) ) / ( x0 - x1 );

	float F = (float)( m*x0 + y0 - h*m/2 ) * 4/( 1 + pow(m,2) );
	float G = (float)( pow(x0,2) + pow(y0,2) - x0*h - pow(r,2) + pow(h,2) /4) * 4/( 1 + pow(m,2) );
	float delta = pow(F,2)  - 4 * G;

	if ( delta < 0 )
		return NULL;

	if ( !strUpDown.compare("DOWN") )
		pCoefficients->b = ( - F - sqrt(delta) ) / 2;
	else if ( !strUpDown.compare("UP") )
		pCoefficients->b = ( - F + sqrt(delta) ) / 2;

	pCoefficients->a = pCoefficients->b * ( y1 - y0 ) / ( x0 - x1 )  - h;
	pCoefficients->c = -(pow(r,2)) + pow(pCoefficients->b,2) * ( 1 + pow(m,2) ) / 4 - pCoefficients->b*m*h/2 + pow(h,2) / 4;

	return true;
}

int32_t SampleDetectionPreliminary::findClosest(peaksStruct* pPeaksStruct, int32_t liValue, string strDirection)
{
	if ( pPeaksStruct == 0 )											return -2;
	if ( strDirection.compare("down") && strDirection.compare("up") )	return -2;
	int liID = -1;
	int liFoo = 100;

	for ( unsigned long i = 0; i != pPeaksStruct->peaksNum; i++ )
	{
		if ( ! strDirection.compare("down") )
		{
			if ( ( (int)(liValue - pPeaksStruct->peaksAmplitude[i]) >= 0 ) && ( (int)(liValue - pPeaksStruct->peaksAmplitude[i]) < liFoo) )
			{
				liID = i;
				liFoo = liValue - pPeaksStruct->peaksAmplitude[i];
			}
		}
		else
		{
			if ( ( (int)(liValue - pPeaksStruct->peaksAmplitude[i]) <= 0 ) && ( (int)(liValue - pPeaksStruct->peaksAmplitude[i]) > liFoo) )
			{
				liID = i;
				liFoo = pPeaksStruct->peaksAmplitude[i] - liValue;
			}
		}
	}

	return liID;
}

bool SampleDetectionPreliminary::solveEyePatternIssue(uint32_t* pulData, bool& bHasEyePattern)
{
	// The value of CENTER_EYE_CABLED depends on the relative angle between the camera and the pattern
	// For this reason 13 is not intended the final value, but some tests have to be performed.
	uint8_t ucCenter = CENTER_EYE_CABLED + m_cDiff;
	int liLowLimit = ( ucCenter > CENTER_EYE_MAGIC_VALUE_10 ) ? ucCenter-CENTER_EYE_MAGIC_VALUE_10 : CENTER_EYE_MAGIC_VALUE_1;
	int liHighLimit = ( ucCenter < CENTER_EYE_MAGIC_VALUE_24 ) ? ucCenter+CENTER_EYE_MAGIC_VALUE_7 : CENTER_EYE_MAGIC_VALUE_29;

	vector<uint32_t> vImFiltered(pulData, pulData+m_nImageSize);

	if ( ! customImadjust(vImFiltered.data(), liLowLimit, liHighLimit-liLowLimit+1) )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionPreliminary::solveEyePatternProblem: unable to apply customized imadjust filter");
		return false;
	}

	bool bRes = highPassFilter(vImFiltered.data(), m_nImageSize);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionPreliminary::solveEyePatternProblem: unable to apply high pass filter");
		return false;
	}

	// Clean the image a little bit
	for ( int j = 0; j != m_nImageHeight; ++j )
	{
		for ( int k = 0; k != m_nImageWidth-6; ++k )
		{
			if ( ! (!vImFiltered[j*m_nImageWidth+k]) == ! (!vImFiltered[j*m_nImageWidth+k+1]) )
			{
				if ( ! (!vImFiltered[j*m_nImageWidth+k+1]) != ! (!vImFiltered[j*m_nImageWidth+k+2]) )
				{
					if ( ! (!vImFiltered[j*m_nImageWidth+k+2]) != ! (!vImFiltered[j*m_nImageWidth+k+3]) )
					{
						if ( ! (!vImFiltered[j*m_nImageWidth+k+3]) == ! (!vImFiltered[j*m_nImageWidth+k+4]) )
						{
							vImFiltered[j*m_nImageWidth+k+2] = vImFiltered[j*m_nImageWidth+k+1];
						}
						else if ( ( ! (!vImFiltered[j*m_nImageWidth+k+5]) != ! (!vImFiltered[j*m_nImageWidth+k+4]) ) &&
								  ( ! (!vImFiltered[j*m_nImageWidth+k+5]) == ! (!vImFiltered[j*m_nImageWidth+k+6]) ) )
						{
							vImFiltered[j*m_nImageWidth+k+2] = vImFiltered[j*m_nImageWidth+k+1];
							vImFiltered[j*m_nImageWidth+k+4] = vImFiltered[j*m_nImageWidth+k+5];
						}
					}
				}
			}
		}
	}

	bRes = findEyePatternAndFill(pulData, vImFiltered.data(), bHasEyePattern);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionPreliminary::solveEyePatternProblem: unable to apply fill Eye pattern filter");
		return false;
	}

	if ( bHasEyePattern )
	{
		m_pLogger->log(LOG_INFO, "SampleDetectionPreliminary::solveEyePatternProblem: a distorted pattern has been detected (eye shape)");
	}

	return true;
}

bool SampleDetectionPreliminary::findEyePatternAndFill(uint32_t* pulData, uint32_t* rgImFiltered, bool& bHasEyePattern)
{	
	if ( pulData == nullptr )		return false;
	if ( rgImFiltered == nullptr )	return false;

	bHasEyePattern = false;

	// The idea is to find one fo the two  ________		___  ___
	// shapes here reported					  __          <__>
	//									   __/  \__		__/  \__

	vector<uint32_t> vPosXToBeChecked(CENTER_EYE_X_POSITIONS_NUM);
	vector<vector<uint32_t>> vWaves;	// 2D vector
	vWaves.resize(CENTER_EYE_X_POSITIONS_NUM);
	for (int i = 0; i < CENTER_EYE_X_POSITIONS_NUM; ++i)
	{
		vWaves[i].resize(m_nImageHeight);
	}

	for ( int i = 1; i <= CENTER_EYE_X_POSITIONS_NUM; i++ )
	{
		float fFoo = m_nImageWidth * i;
		fFoo /= ( CENTER_EYE_X_POSITIONS_NUM + 1 );
		vPosXToBeChecked[i-1] = round(fFoo) - 1;
	}

	// Avoid borders ( to avoid segmentation fault!)
	if ( vPosXToBeChecked[0] == 0 )
	{
		vPosXToBeChecked[0] = 1;
	}

	if ( vPosXToBeChecked[CENTER_EYE_X_POSITIONS_NUM] == (unsigned long)m_nImageWidth )
	{
		vPosXToBeChecked[CENTER_EYE_X_POSITIONS_NUM] = m_nImageWidth - 1;
	}

	// Make AVG waves in the 8 columns
	uint32_t liBoundary;
	for ( int m = 0; m != m_nImageHeight; m++ )
	{
		for ( int n = 0; n != CENTER_EYE_X_POSITIONS_NUM; n++ )
		{
			liBoundary = vPosXToBeChecked[n];
			vWaves[n][m] = *(rgImFiltered+m*m_nImageWidth+liBoundary-1);
			vWaves[n][m] += *(rgImFiltered+m*m_nImageWidth+liBoundary);
			vWaves[n][m] += *(rgImFiltered+m*m_nImageWidth+liBoundary+1);
			vWaves[n][m] /= 3;
		}
	}

	vector<uint32_t> vPosYSaved(CENTER_EYE_X_POSITIONS_NUM);
	for ( int n = 0; n != CENTER_EYE_X_POSITIONS_NUM; n++ )
	{
		// I'm supposing that the pattern is always between 5th and 20th pixels
		for ( uint32_t m = 5; m != 20; m++ )
		{
			if ( vWaves[n][m] )
			{
				vPosYSaved[n] = m;
				break;
			}
		}
	}

	// free heap, not used anymore.
	vWaves.clear();

	uint32_t uliMaxY = 0;
	uint32_t uliMinY = m_nImageHeight;
	for ( int i = 0; i != CENTER_EYE_X_POSITIONS_NUM; i++ )
	{
		if ( vPosYSaved[i] > uliMaxY )
		{
			uliMaxY = vPosYSaved[i];
		}
		if ( uliMinY > vPosYSaved[i] )
		{
			uliMinY = vPosYSaved[i];
		}
	}

	// We are in case 1 if the eye is complete, 2 if is partial. 0 if there is no eye
	uint8_t ucEyeCase = 0;
	uint32_t uliMaxLow = 0;

	if ( uliMaxY - uliMinY <= 3 )
	{
		bHasEyePattern = true;
		ucEyeCase = 1;
	}
	else
	{
		float fThreshold = ( uliMaxY + uliMinY ) / 2;
		uint32_t uliMinLow = m_nImageHeight;
		uint32_t uliMinHigh = m_nImageHeight;
		uint32_t uliMaxHigh = 0;

		for ( int i = 0; i != CENTER_EYE_X_POSITIONS_NUM; i++ )
		{
			if ( vPosYSaved[i] < fThreshold )
			{
				if ( vPosYSaved[i] < uliMinLow )
				{
					uliMinLow = vPosYSaved[i];
				}
				if ( vPosYSaved[i] > uliMaxLow )
				{
					uliMaxLow = vPosYSaved[i];
				}
			}
			else
			{
				if ( vPosYSaved[i] < uliMinHigh )
				{
					uliMinHigh = vPosYSaved[i];
				}
				if ( vPosYSaved[i] > uliMaxHigh )
				{
					uliMaxHigh = vPosYSaved[i];
				}
			}
		}

		if ( uliMaxHigh - uliMinHigh <= 1 )
		{
			if ( uliMaxLow - uliMinLow <= 1 )
			{
				bHasEyePattern = true;
				ucEyeCase = 2;
			}
		}
		vPosYSaved.clear();
	}

	if ( ! bHasEyePattern )
	{
		m_pLogger->log(LOG_INFO, "SampleDetectionPreliminary::findEyePatternAndFill: no eye pattern found in this picture");
		return true;
	}


	/*!************  SECOND PART OF THE ALGORITHM  **************/
	/*!************  The peaks have to be	 __	   **************/
	/*!************  like this				/  \   **************/


	vector<uint32_t> vPosYSavedEllipse(CENTER_EYE_X_POSITIONS_NUM);
	vector<uint32_t> vEdgeData(m_nImageSize);
	bool bRes = cannyFilter(rgImFiltered, vEdgeData.data(), m_nImageWidth, m_nImageHeight);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionPreliminary::findEyePatternAndFill: unable to apply canny filter");
		return bRes;
	}

	uint8_t ucMinPosY = UINT8_MAX;
	uint8_t ucMinPosYId = UINT8_MAX;

	for ( int m = 0; m != CENTER_EYE_X_POSITIONS_NUM; m++ )
	{
		for ( int n = m_nImageHeight-1; n != CENTER_EYE_MAGIC_VALUE_13+m_cDiff; n-- )
		{
			liBoundary = vPosXToBeChecked[m];
			if ( vEdgeData[n*m_nImageWidth+liBoundary] )
			{
				vPosYSavedEllipse[m] = n;
				if ( vPosYSavedEllipse[m] < ucMinPosY )
				{
					ucMinPosY = vPosYSavedEllipse[m];
					ucMinPosYId = m;
				}
				break;
			}
		}
	}

	if ( ucMinPosYId != 0 )
	{
		for ( int i = ucMinPosYId-1; i >= 0; i-- )
		{
			if ( vPosYSavedEllipse[i+1] > vPosYSavedEllipse[i] )
			{
				bHasEyePattern = false;
				break;
			}
		}
	}

	if ( bHasEyePattern )
	{
		for ( int i = ucMinPosYId+1; i < CENTER_EYE_X_POSITIONS_NUM; i++ )
		{
			if ( vPosYSavedEllipse[i-1] > vPosYSavedEllipse[i] )
			{
				bHasEyePattern = false;
				break;
			}
		}
	}

	if ( ! bHasEyePattern )
	{
		m_pLogger->log(LOG_INFO, "SampleDetectionPreliminary::findEyePatternAndFill: no eye pattern found in this picture");
		return true;
	}

	/*!* Fill the image if an eye pattern is found ***/

	int yLine;

	switch ( ucEyeCase )
	{
		case 1:
			yLine = uliMaxY;
		break;

		case 2:
			yLine = uliMaxLow;
		break;

		default:
			return false;
	}


	for ( int j = yLine; j < ucMinPosY; j++ )
	{
		int liAVGgray = 0;
		int liCnt = 0;

		for ( int k = 3; k != m_nImageWidth-4; k++ )
		{
			if ( rgImFiltered[j*m_nImageWidth+k] )
			{
				liCnt++;
				liAVGgray += pulData[j*m_nImageWidth+k];
			}
		}

		if ( liCnt == 0 )
		{
			continue;
		}

		float fFoo = liAVGgray;
		fFoo /= ( DECIMAL_DIGITS_FACTOR * liCnt );
		liAVGgray = round(fFoo) * DECIMAL_DIGITS_FACTOR;

		for ( int k = 3; k != m_nImageWidth-5; k++ )
		{
			if ( rgImFiltered[j*m_nImageWidth+k] == 0 )
			{
				fFoo =  liAVGgray * 1.05;
				fFoo /= DECIMAL_DIGITS_FACTOR;
				int liMaxRandVal = round(fFoo);

				fFoo =  liAVGgray * 0.95;
				fFoo /= DECIMAL_DIGITS_FACTOR;
				int liMinRandVal = round(fFoo);

				// Fill the image with randomic values between plus 5% and minus 5%
				// Randomic values are used because no strong edges are then foud with sobel in the preproc phase
				pulData[j*m_nImageWidth+k] = DECIMAL_DIGITS_FACTOR * ( rand()%(liMaxRandVal-liMinRandVal + 1) + liMinRandVal );

				// These filling lines are addded to increase algorithm power (fills a bigger area)
				if (  rgImFiltered[j*m_nImageWidth+k-1] != 0 )
				{
					pulData[j*m_nImageWidth+k-1] = DECIMAL_DIGITS_FACTOR * ( rand()%(liMaxRandVal-liMinRandVal + 1) + liMinRandVal );
				}

				if (  rgImFiltered[j*m_nImageWidth+k+1] != 0 )
				{
					pulData[j*m_nImageWidth+k+1] = DECIMAL_DIGITS_FACTOR * ( rand()%(liMaxRandVal-liMinRandVal + 1) + liMinRandVal );
				}

				if ( j > 0 )
				{
					if (  rgImFiltered[(j-1)*m_nImageWidth+k] != 0 )
					{
						pulData[(j-1)*m_nImageWidth+k] = DECIMAL_DIGITS_FACTOR * ( rand()%(liMaxRandVal-liMinRandVal + 1) + liMinRandVal );
					}
				}

				if ( j < m_nImageHeight-1 )
				{
					if (  rgImFiltered[(j+1)*m_nImageWidth+k] != 0 )
					{
						pulData[(j+1)*m_nImageWidth+k] = DECIMAL_DIGITS_FACTOR * ( rand()%(liMaxRandVal-liMinRandVal + 1) + liMinRandVal );
					}
				}
			}
		}
	}

	return true;
}

bool SampleDetectionPreliminary::findPatternLowerEdge(vector<uint32_t>& vImage, int32_t liXShift)
{
	vector<uint32_t> vFilteredImage;
	bool bRes = patternSelector(vImage, vFilteredImage);
	if ( ! bRes )
	{
		// In case of errors in the filtering function, the algorithm has to go on
		return true;
	}

	// Basically, if the picture is only 0 or 255, than is obvious that there is liquid inside the well
	bool bIsLiquidAbsent = false;
	for ( int i = 0; i < m_nImageWidth; i++ )
	{
		if ( bIsLiquidAbsent )	break;

		for ( int j = 0; j < m_nImageHeight; j++ )
		{
			if ( (vFilteredImage[j*m_nImageWidth+i] < SAMPLE_DETECTION_PATTERN_CHECK_THRESH) &&
				 (vFilteredImage[j*m_nImageWidth+i] != 0) )
			{
				bIsLiquidAbsent = true;
			}
		}
	}

	if ( ! bIsLiquidAbsent )
	{
		m_pLogger->log(LOG_INFO, "SampleDetectionPreliminary::findPatternLowerEdge liquid found");
		return false;
	}

	if ( getWellNumber() == WELL_THREE )
	{
		m_pLogger->log(LOG_INFO, "SampleDetectionPreliminary::findPatternLowerEdge liquid not found [X3]");
		return true;
	}

	// Find boundaries where the check has to be performed
	// If the image has been cropped on the left because of the foil, then we have to shift the start
	if ( liXShift > 7 )
	{
		liXShift = 7;
	}

	int liXStart = 9 - liXShift;
	int liXEnd = m_nImageWidth - 11 - liXShift;
	int liYStart = ( 1+m_cDiffVert > 0) ? (1 + m_cDiffVert) : 1;
	int liYEnd;

	if ( liYStart+PATTERN_WIDTH > (m_nImageHeight-1) )
	{
		liYEnd = m_nImageHeight - 2;
	}
	else if ( liYStart+PATTERN_WIDTH > (m_nImageHeight-6) )
	{
		liYEnd = m_nImageHeight - 4;
	}
	else
	{
		liYEnd = m_nImageHeight - 6;
	}

	// Find a more accurate yStart and yEnd
	char cArraySize = 0;
	if ( m_nImageWidth >= 35 )
	{
		cArraySize = 5;
	}
	else
	{
		// 15 is the starting point, 5 is the step
		cArraySize = floor(float(m_nImageWidth-15)/5.0f);
	}

	if ( cArraySize <= 0 )		return true; // continue the algo

	vector<uint32_t> rgYPos(cArraySize);
	int liTop = 14 + cArraySize*5;
	int liSum = 0;
	for (int i = 14; i < liTop; i+=5 )
	{
		for ( int j = liYStart; j <= liYEnd; j++ )
		{
			if ( vFilteredImage[j*m_nImageWidth+i] != 0 )
			{
				rgYPos[(i+1)/5 - 3] = j;
				liSum += j;
				break;
			}
		}
	}

	// Update liYStart with a more accurate value
	if ( round(float(liSum)/float(cArraySize)) > 4 )
	{
		liYStart = round(float(liSum)/float(cArraySize) - 4);
	}

	if ( liYStart + 20  < m_nImageHeight )
	{
		liYEnd = liYStart + 20;
	}
	else
	{
		liYEnd = m_nImageHeight -1;
	}

	// Now I want to select the lower edge of the pattern
	vector<uint32_t> vYPos(liXEnd-liXStart+1);
	for ( int i = liXStart; i <= liXEnd; i++ )
	{
		for ( int j = liYEnd; j >= liYStart; j-- )
		{
			if ( (vFilteredImage[j*m_nImageWidth+i] < SAMPLE_DETECTION_PATTERN_CHECK_THRESH_2) &&
				 (vFilteredImage[j*m_nImageWidth+i] != 0) )
			{
				vYPos[i-liXStart] = j;
				break;
			}
		}
	}

	// Perform a check on the results: if all the elements of the array are liYEnd
	// than it is super mega probable that is due to a liquid reflection.
	float liqCnt = 0;
	for ( auto i : vYPos )
	{
		if ( i == (unsigned int)liYEnd )	liqCnt++;
	}

	if ( liqCnt > float(0.6f * float(vYPos.size())) )
	{
		m_pLogger->log(LOG_INFO, "SampleDetectionPreliminary::findPatternLowerEdge: highly probable (but not 100% sure) liquid presence");
		return true;
	}


	// Need to remove very evident outliers
	vector<bool> vIsOutlier;
	bRes = findOutliers(vYPos, vIsOutlier);
	if ( ! bRes )
	{
		// In case of errors in findOutlier function, the algorithm has to proceed
		return true;
	}

	unsigned int liMaxPos = 0;
	unsigned int liMinPos = UINT_MAX;

	for ( size_t i = 0; i < vYPos.size(); i++ )
	{
		if ( ! vIsOutlier[i] )
		{
			if ( vYPos[i] > liMaxPos )
			{
				liMaxPos = vYPos[i];
			}
			if ( vYPos[i] < liMinPos )
			{
				liMinPos = vYPos[i];
			}
		}
	}

	int liYMaxDiff = liMaxPos - liMinPos;
	// We still need to be in some limits
	if ( liYMaxDiff >= 5 )
	{
		return true; // nothing changes
	}

	// Calculate AVG val of the algorithm
	int ans = count(vIsOutlier.begin(), vIsOutlier.end(), false);
	int liAVG = 0;
	for ( size_t i = 0; i < vIsOutlier.size(); i++ )
	{
		liAVG += (!vIsOutlier[i]) * vYPos[i]; // consider only non outlier elements
	}
	liAVG = round(float(liAVG)/float(ans));

	char foo = -1;
	int tmp = round(m_nImageWidth/2) - liXStart;
	int liTmpNewYCenter = -1;
	// Assign the temporary value for y center
	for ( size_t i = 0; i < vYPos.size(); i++ )
	{
		tmp += foo * i;
		if ( ! vIsOutlier[tmp] )
		{
			liTmpNewYCenter = liAVG;
			break;
		}
		foo *= (-1);
	}

	if ( (liTmpNewYCenter > 10+m_cDiffVert) && (liTmpNewYCenter < 21+m_cDiffVert) )
	{
		m_cDiffVert = liTmpNewYCenter -  m_cropRectangle.height/2;
	}

	return true;
}

bool SampleDetectionPreliminary::patternSelector(const vector<uint32_t>& vData, vector<uint32_t>& vFilteredImage)
{
	if ( vData.empty() )		return false;
	if ( m_nImageWidth < 10 )	return false;
	if ( m_nImageHeight < 10 )	return false;

	vFilteredImage = vData;

	// Select a window 9+(width-6) where to evaluate filters limits
	int liXStart = round(m_nImageWidth / 2) - 5;
	int liXEnd = round(m_nImageWidth / 2) + 4;
	int liYStart = 2;
	int liYEnd = m_nImageHeight - 3;
	int liSize = (liXEnd-liXStart)*(liYEnd-liYStart);

	// Crop the image and save the slice
	vector<uint32_t> vSlicedImage(liSize);
	uint32_t* pulSlicedImage;
	pulSlicedImage = &vSlicedImage[0];
	for ( int i = liYStart; i < liYEnd; ++i )
	{
		for ( int j = liXStart; j < liXEnd; ++j  )
		{
			int liPos = i * m_nImageWidth + j;
			*pulSlicedImage = vData[liPos];
			++pulSlicedImage;
		}
	}

	vector<uint32_t> vOccurrences(HISTOGRAM_BINS_NUM);
	vector<float> vEdges(HISTOGRAM_BINS_NUM+1);
	bool bRes = histCounts(vSlicedImage, HISTOGRAM_BINS_NUM, vOccurrences, vEdges);
	if ( ! bRes )		return false;

	size_t uliMaxPos = max_element(vOccurrences.begin(), vOccurrences.end()) - vOccurrences.begin();
	int highLimit = 0;

	if ( vOccurrences.size() > (uliMaxPos+1) )
	{
		highLimit = vEdges[uliMaxPos+1] + 10*DECIMAL_DIGITS_FACTOR;
	}
	else
	{
		highLimit = vEdges[uliMaxPos];
	}

	highPassFilter(vFilteredImage.data(), m_nImageSize, highLimit);

	return true;
}

bool SampleDetectionPreliminary::findOutliers(vector<uint32_t>& vData, vector<bool>& vIsOutlier)
{
	if ( vData.empty() )	return false;

	vIsOutlier.resize(vData.size());
	fill(vIsOutlier.begin(), vIsOutlier.end(), false);

	// Calculate mode
	uint32_t liModeVal;
	bool bRes = mode(vData.data(), vData.size(), &liModeVal);
	if ( ! bRes )	return false;

	int liCnt = 0;

	for ( size_t i = 0; i < vData.size(); i++ )
	{
		if ( abs(int(liModeVal)-int(vData[i])) <= 3 )
		{
			liCnt++;
		}
		else
		{
			vIsOutlier[i] = true;
		}
	}

	// If we have too many "outliers" than it means that thee is something strange in the image -> consider all values
	if ( float(liCnt)/float(vData.size()) < SAMPLE_DETECTION_OUTLIER_ACCEPT )
	{
		fill(vIsOutlier.begin(), vIsOutlier.end(), false);
	}

	return true;
}


