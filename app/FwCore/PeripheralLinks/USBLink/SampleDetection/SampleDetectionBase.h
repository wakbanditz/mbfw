/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionBase.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SampleDetectionBase class.
 @details

 ****************************************************************************
*/

#ifndef SAMPLEDETECTIONBASE_H
#define SAMPLEDETECTIONBASE_H

#include "Filters.h"
#include "SampleDetectionInclude.h"
#include "Log.h"

class SampleDetectionBase : public Filters
{
	public:

		/*! *************************************************************************************************
		 * @brief SampleDetectionBase default constructor
		 * **************************************************************************************************
		 */
		SampleDetectionBase();

		/*! *************************************************************************************************
		 * @brief ~SampleDetectionBase virtual destructor
		 * **************************************************************************************************
		 */
		virtual ~SampleDetectionBase();

		/*! *************************************************************************************************
		 * @brief  setLogger set the logger
		 * @param  pLogger pointer to the log to be set
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool setLogger(Log* pLogger);

		/*! *************************************************************************************************
		 * @brief  getWellNumber get well number, can be 0 or 3
		 * @return well number
		 * **************************************************************************************************
		 */
		int8_t getWellNumber(void);

		/*! *************************************************************************************************
		 * @brief  setWellNumber set the well number
		 * @param  cWellNum number to be set
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool setWellNumber(int8_t cWellNum);

		/*! *************************************************************************************************
		 * @brief  setCropRectangle set external crop rectangle to member one
		 * @param  pCropRectangle pointer to the crop rectangle (X0, Y0, width, height)
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool setCropRectangle(const cropRectangle* pCropRectangle);

		/*! *************************************************************************************************
		 * @brief  getCropRectangle get the crop rectangle
		 * @return pointer to crop rectangle (X0, Y0, width, height)
		 * **************************************************************************************************
		 */
		cropRectangle* getCropRectangle(void);

		/*! *************************************************************************************************
		 * @brief  pretreatImage perform sobel+imadjust+medFilt2
		 * @param  pulData data to be filtered
		 * @param  rgcMedianFilterMatrix filter matrix for medFilt2
		 * @param  uliWidth width of the image
		 * @param  uliHeight height of the image
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool pretreatImage(uint32_t *pulData, uint8_t rgcMedianFilterMatrix[2], uint32_t uliWidth, uint32_t uliHeight);

		/*! *************************************************************************************************
		 * @brief virtual classes redefined in children classes
		 * **************************************************************************************************
		 */
		virtual void clearAll(void) = 0;
		virtual bool getLimits(void) {return true;} // vertical or horizontal
		virtual bool scanImageAndCreateWaves(uint32_t*, uint32_t*, uint32_t, uint32_t, uint8_t) { return true; }
		virtual bool findPeaks(uint32_t*, uint32_t, peaksStruct*) { return true; }
		virtual float foilDiscover(uint32_t*, uint32_t, uint32_t, uint32_t, uint32_t, circumferenceCoefficients*) { return 0.0f; }
		virtual bool getCircumferenceParameters(int32_t, int32_t, int32_t, int32_t, int8_t, string, circumferenceCoefficients*) { return true; }

    protected:

        int8_t			m_cWellNumber;
        cropRectangle	m_cropRectangle;
        Log*			m_pLogger;


};

#endif // SAMPLEDETECTIONBASE_H
