/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionLink.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SampleDetectionLink class.
 @details

 ****************************************************************************
*/

#ifndef SAMPLEDETECTIONLINK_H
#define SAMPLEDETECTIONLINK_H

#include "SampleDetectionZeroZero.h"
#include "SampleDetectionPreliminary.h"
#include "SampleDetectionVertical.h"
#include "SampleDetectionHorizontal.h"

typedef enum
{
	eFactoryParams,
	eUpdateParams
} enumParamsType;


class SampleDetectionLink
{
	public:

		/*! *************************************************************************************************
		 * @brief SampleDetectionLink default constructor
		 * **************************************************************************************************
		 */
		SampleDetectionLink();

		/*! *************************************************************************************************
		 * @brief ~SampleDetectionLink virtual destructor
		 * **************************************************************************************************
		 */
		virtual ~SampleDetectionLink();

		/*! *************************************************************************************************
		 * @brief  initZeroZero function to be called before analyzeZeroZero
		 * @param  pLogger pointer to the logger
		 * @param  uliWidth width of the image expected
		 * @param  uliHeight height of the image expected
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool initZeroZero(Log *pLogger, uint32_t uliWidth, uint32_t uliHeight);

		/*! *************************************************************************************************
		 * @brief  analyzeZeroZero function used to analyze the patter and get the cropRectangle, or
		 *         (x0, y0, width, height) coordinates
		 * @param  pulData pointer to the image with the pattern to be analyzed
		 * @param  ucSlotNum slot number
		 * @param  ucWellNum well number
		 * @param  eType needed to know if its performed during the life of the instrument or in factory
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool analyzeZeroZero(uint32_t* pulData, uint8_t ucSlotNum, uint8_t ucWellNum, enumParamsType eType = eUpdateParams);

		/*! *************************************************************************************************
		 * @brief  initAlgo init sample detection algorithm. To be called before sampleDetectionAlgorithm
		 * @param  pLogger pointer to the logger
		 * @param  ucSlotNum slot number
         * @param  cWellNum well number
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool initAlgo(Log *pLogger, uint8_t ucSlotNum, int8_t cWellNum);

		/*! *************************************************************************************************
		 * @brief  sampleDetectionAlgorithm
		 * @param  pulData pointer to the image with the pattern to be analyzed
		 * @param  uliImageWidth width of the image expected
		 * @param  fVerResult output of the vertical algorithm.
		 * @param  fHorResult output of the horizontal algorithm.
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool sampleDetectionAlgorithm(const uint32_t* pulData, uint32_t uliImageWidth, float& fVerResult, float& fHorResult);

		/*! *************************************************************************************************
		 * @brief  sampleDetectionAlgorithm
		 * @param  pulData pointer to the image with the pattern to be analyzed
		 * @param  uliImageWidth width of the image expected
		 * @param  fResult output of the algorithm. If > 70.0f the sample is not present
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool sampleDetectionAlgorithm(const uint32_t* pulData, uint32_t uliImageWidth, float& fResult);

		/*! *************************************************************************************************
		 * @brief  readBMP used to read bmp images
		 * @param  prgData pointer to the data to be read
		 * @param  strFileName name of the image file
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool readBMP(uint32_t* prgData, string strFileName);

		/*! *************************************************************************************************
		 * @brief getCropRectangleZeroZero get the crop rectangle of the zero zero processing
		 * @param pCropRectangle
		 * **************************************************************************************************
		 */
		void getCropRectangleZeroZero(cropRectangle* pCropRectangle);

	private:

		/*! *************************************************************************************************
		 * @brief  getCropRectangleFromFile read condif file file and get cropRectangle
		 * @param  pCropRectangle pointer to cropRectangle to be get
		 * @param  ucSlotNum slot number
		 * @param  ucWellNum well number
		 * @param  eType needed to know if its performed during the life of the instrument or in factory
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getCropRectangleFromFile(cropRectangle* pCropRectangle, uint8_t ucSlotNum, uint8_t ucWellNum, enumParamsType eType = eUpdateParams);

		/*! *************************************************************************************************
		 * @brief  setCropRectangleToFile save crop rectangle in the config file
		 * @param  pCropRectangle pointer to cropRectangle to be set
		 * @param  ucSlotNum slot number
		 * @param  ucWellNum well number
		 * @param  eType needed to know if its performed during the life of the instrument or in factory
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool setCropRectangleToFile(cropRectangle* pCropRectangle, uint8_t ucSlotNum, uint8_t ucWellNum, enumParamsType eType);

    private:

        SampleDetectionZeroZero		m_ZeroZero;
        SampleDetectionPreliminary	m_Preliminary;
        SampleDetectionVertical		m_Vertical;
        SampleDetectionHorizontal	m_Horizontal;

        Log* m_pLogger;

};

#endif // SAMPLEDETECTIONLINK_H
