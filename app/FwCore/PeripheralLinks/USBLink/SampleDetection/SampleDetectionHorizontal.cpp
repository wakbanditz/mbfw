/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionHorizontal.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SampleDetectionHorizontal class.
 @details

 ****************************************************************************
*/

#include "SampleDetectionHorizontal.h"

#define DEFAULT_CENTER_POSITION		12
#define MAGIC_VALUE_1				1
#define MAGIC_VALUE_4				4
#define MAGIC_VALUE_5				5
#define MAGIC_VALUE_7				7
#define MAGIC_VALUE_9				9
#define MAGIC_VALUE_10				10
#define MAGIC_VALUE_24				24
#define MAGIC_VALUE_25				25
#define MAGIC_VALUE_29				29

SampleDetectionHorizontal::SampleDetectionHorizontal()
{
	m_nImageSize = -1;
	m_bConfigOK = false;

	memset(&m_HorizontalLimits, 0x00, sizeof(horizontalScansLimits));
}

SampleDetectionHorizontal::~SampleDetectionHorizontal()
{

}

bool SampleDetectionHorizontal::init(Log* pLogger, cropRectangle* pPreliminaryCropRect, int8_t cWellNum, int liOriginalWidth)
{
	m_bConfigOK = false;

	if ( ! setLogger(pLogger) )						return false;
	if ( ! setCropRectangle(pPreliminaryCropRect) )	return false;
	if ( ! setWellNumber(cWellNum) )				return false;

	m_nInitialWidth = liOriginalWidth;
	setImageWidth(pPreliminaryCropRect->width);
	setImageHeight(pPreliminaryCropRect->height);
	m_nImageSize = m_nImageHeight * m_nImageWidth;

	m_bConfigOK = true;

	return true;
}

bool SampleDetectionHorizontal::horizontalProcessing(uint32_t* pCroppedImage, int8_t cDiff, float& fScore)
{
	if ( pCroppedImage == nullptr )			return false;
	if ( m_bConfigOK == false )				return false;

	// Calculate horizontal scans limits.
	getLimits();

	/*! ********************************************************************************
	 ***** Take care about the output of the vertical processing - label presence. *****
	 ********************* Recalculate x boundaries if necessary. **********************
	********************************************************************************* **/

	if ( cDiff != 0 )
	{
		if ( cDiff < 0 )
		{
			if ( int(m_HorizontalLimits.yStartIndexHighBand) < cDiff )
			{
				// To be sure we do not overlap the central scan
				m_HorizontalLimits.yStartIndexHighBand = m_HorizontalLimits.yStartIndexLowBand;
				m_HorizontalLimits.yEndIndexHighBand = m_HorizontalLimits.yEndIndexLowBand;
			}
			else
			{
				m_HorizontalLimits.yStartIndexHighBand -= cDiff;
				m_HorizontalLimits.yEndIndexHighBand -= cDiff;
			}
			m_HorizontalLimits.yStartIndexCentralBand -= cDiff;
			m_HorizontalLimits.yEndIndexCentralBand -= cDiff;
		}
		else
		{
			if ( m_HorizontalLimits.yEndIndexLowBand + cDiff < int(m_cropRectangle.height) )
			{
				m_HorizontalLimits.yStartIndexLowBand += cDiff;
				m_HorizontalLimits.yEndIndexLowBand += cDiff;
			}
			else
			{
				// To be sure we do not overlap the central scan
				m_HorizontalLimits.yStartIndexLowBand = m_HorizontalLimits.yStartIndexHighBand;
				m_HorizontalLimits.yEndIndexLowBand = m_HorizontalLimits.yEndIndexHighBand;
			}
			m_HorizontalLimits.yStartIndexCentralBand += cDiff;
			m_HorizontalLimits.yEndIndexCentralBand += cDiff;
		}
	}

	// Filter cropped image to find optimal data where to apply horizontal algorithm
	int liCenter = DEFAULT_CENTER_POSITION + cDiff;
	int liLowLimit  = ( liCenter > MAGIC_VALUE_9 ) ?	liCenter-MAGIC_VALUE_10 : MAGIC_VALUE_1;
	int liHighLimit = ( liCenter < MAGIC_VALUE_24 ) ?	liCenter+MAGIC_VALUE_7 : MAGIC_VALUE_29;

	vector<uint32_t> vImFiltered(pCroppedImage, pCroppedImage+m_nImageSize);

	if ( ! customImadjust(vImFiltered.data(), liLowLimit, liHighLimit-liLowLimit+1) )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionPreliminary::solveEyePatternProblem: unable to apply customized imadjust filter");
		return false;
	}

	bool bRes = highPassFilter(vImFiltered.data(), m_nImageSize);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionPreliminary::solveEyePatternProblem: unable to apply high pass filter");
		return false;
	}

	// Try. Adjust the central line and build the central band around.
	uint32_t uliBandWidth = m_HorizontalLimits.xEndIndex - m_HorizontalLimits.xStartIndex + 1;
	uint32_t uliBandHeight = m_HorizontalLimits.yEndIndexHighBand - m_HorizontalLimits.yStartIndexHighBand + 1;

	// use always upperBand, TODO: if this is the algo chosen, change bitCollection (only two fields), AVGstruct and difference struct
	bitCollectionStruct bitCollection;
	bitCollection.ulUpperBand.resize(uliBandWidth*uliBandHeight);
	bitCollection.ulCentralBand.resize(uliBandWidth*uliBandHeight);

	int8_t cOffset = 0;
	cOffset = findOptimalCentralScanRecursively(vImFiltered.data(), &bitCollection, cOffset);
	if ( abs(cOffset) > HOR_MAX_OFFSET_ADMITTED ) // Out of limits - restore old values
	{
		m_HorizontalLimits.yStartIndexCentralBand -= cOffset;
		m_HorizontalLimits.yEndIndexCentralBand -= cOffset;
		cOffset = 0;
	}
	else
	{
		// there is no need to adjust upper and lower limits
	}

	// Store bitcollection.
	if ( getWellNumber() == WELL_ZERO )
	{
		int liHighOffset = cOffset;
		liHighLimit = int(m_HorizontalLimits.yStartIndexHighBand) - MAGIC_VALUE_5 + liHighOffset;
		if ( liHighLimit <= 0 )
		{
			liHighOffset = MAGIC_VALUE_5 - m_HorizontalLimits.yStartIndexHighBand;
		}

		for (uint8_t i = 0; i < uliBandWidth; ++i)
		{
			for (uint8_t j = 0; j < uliBandHeight; ++j)
			{
				bitCollection.ulUpperBand[uliBandWidth*j + i]   = vImFiltered[m_cropRectangle.width*(m_HorizontalLimits.yStartIndexHighBand+j-4+liHighOffset) +
																			 (i+m_HorizontalLimits.xStartIndex)];
				bitCollection.ulCentralBand[uliBandWidth*j + i] = vImFiltered[m_cropRectangle.width*(m_HorizontalLimits.yStartIndexCentralBand+j) +
																			 (i+m_HorizontalLimits.xStartIndex)];
			}
		}
	}
	else if ( getWellNumber() == WELL_THREE )
	{
		int liLowOffset = cOffset;
		liLowLimit = m_HorizontalLimits.yStartIndexLowBand + uliBandHeight + MAGIC_VALUE_4 + liLowOffset;
		if ( liLowLimit >= int(m_cropRectangle.height) )
		{
			liLowOffset = m_cropRectangle.height - m_HorizontalLimits.yStartIndexLowBand - uliBandHeight - MAGIC_VALUE_4 - 1;
		}

		for (uint8_t i = 0; i < uliBandWidth; ++i)
		{
			for (uint8_t j = 0; j < uliBandHeight; ++j)
			{
				bitCollection.ulUpperBand[uliBandWidth*j + i]	= vImFiltered[m_cropRectangle.width*(m_HorizontalLimits.yStartIndexLowBand+j+liLowOffset+1) +
																			 (i+m_HorizontalLimits.xStartIndex)];
				bitCollection.ulCentralBand[uliBandWidth*j + i] = vImFiltered[+ m_cropRectangle.width*(m_HorizontalLimits.yStartIndexCentralBand+j) +
																			 (i+m_HorizontalLimits.xStartIndex)];
			}
		}
	}

	// Build AVG array.
	AVGstruct horAVGstruct;
	horAVGstruct.ulUpperBand.resize(uliBandWidth);
	horAVGstruct.ulCentralBand.resize(uliBandWidth);

	for (uint8_t i = 0; i < uliBandWidth; ++i)
	{
		for (uint8_t j = 0; j < uliBandHeight; ++j)
		{
			horAVGstruct.ulUpperBand[i] += bitCollection.ulUpperBand[uliBandWidth*j + i];
			horAVGstruct.ulCentralBand[i] += bitCollection.ulCentralBand[uliBandWidth*j + i];
		}
		horAVGstruct.ulUpperBand[i] /= uliBandHeight;
		horAVGstruct.ulCentralBand[i] /= uliBandHeight;
	}

	// Build differences array and integrals.
	differenceStruct horDiffStruct;
	horDiffStruct.ilUpperDifference.resize(uliBandWidth);

	for ( uint i = 0; i < uliBandWidth; ++i )
	{
		horDiffStruct.ilUpperDifference[i] = horAVGstruct.ulCentralBand[i] - horAVGstruct.ulUpperBand[i];
	}


	/*! ********************************************************************************
	 **************** Compute all calculations to find out the output ******************
	********************************************************************************* **/

	fScore = 0;

	for ( uint i = 0; i < uliBandWidth; ++i )
	{
		int liLowThresh = int(horAVGstruct.ulCentralBand[i]) - 30*DECIMAL_DIGITS_FACTOR;
		int liHighThresh = int(horAVGstruct.ulCentralBand[i]) + 1*DECIMAL_DIGITS_FACTOR;
		int liCurrVal = horDiffStruct.ilUpperDifference[i];

		if ( liCurrVal > liLowThresh && liCurrVal < liHighThresh )
		{
			if ( liCurrVal == 0 )
			{
				fScore += 50;
			}
			else
			{
				fScore += 100;
			}
		}
		else if ( liCurrVal > liLowThresh-20*DECIMAL_DIGITS_FACTOR && liCurrVal < liHighThresh )
		{
			fScore += 70;
		}
		else if ( liCurrVal <= 0 )
		{
			fScore -= 150;
		}
	}

	fScore /= uliBandWidth;

	if ( fScore < LOWER_LIMIT_SCORE )		fScore = LOWER_LIMIT_SCORE;

	return true;
}

void SampleDetectionHorizontal::clearAll()
{
	m_nImageSize = -1;
	m_bConfigOK = false;
	m_pLogger = 0;
	m_cWellNumber = -1;

	memset(&m_HorizontalLimits, 0x00, sizeof(horizontalScansLimits));
	memset(&m_cropRectangle, 0x00, sizeof(cropRectangle));
}

bool SampleDetectionHorizontal::getLimits()
{
	if ( m_bConfigOK == false )		return -1;

    // It is important to keep the original center in x, because it is the correct one. (label do not change the real position of the center!)
	uint32_t xCenter = m_nInitialWidth / 2 - 1;				// because the index starts from 0 and not from 1 like Matlab
	uint32_t yCenter = m_cropRectangle.height / 2 - 1;		// because the index starts from 0 and not from 1 like Matlab

	if ( yCenter < BLACK_BAND_DISPLACEMENT_FROM_CENTER+BANDS_WIDTH )							return -1;
	if ( yCenter+BLACK_BAND_DISPLACEMENT_FROM_CENTER+BANDS_WIDTH >= m_cropRectangle.height )		return -1;

	// The two cases are the same for the moment, but there is the possibility to make them different
	switch( getWellNumber() )
	{
		case 0:
			m_HorizontalLimits.yStartIndexHighBand = yCenter - BLACK_BAND_DISPLACEMENT_FROM_CENTER - BANDS_WIDTH;
			m_HorizontalLimits.yEndIndexHighBand = yCenter - BLACK_BAND_DISPLACEMENT_FROM_CENTER;
			m_HorizontalLimits.yStartIndexCentralBand = yCenter - HALVE_AND_ROUND_UP(BANDS_WIDTH);
			m_HorizontalLimits.yEndIndexCentralBand = yCenter + HALVE_AND_ROUND_UP(BANDS_WIDTH);
			m_HorizontalLimits.yStartIndexLowBand = yCenter + BLACK_BAND_DISPLACEMENT_FROM_CENTER;
			m_HorizontalLimits.yEndIndexLowBand = yCenter + BLACK_BAND_DISPLACEMENT_FROM_CENTER + BANDS_WIDTH;
			m_HorizontalLimits.xStartIndex = ( xCenter >= (BANDS_HEIGHT-1)/2 ) ? ( xCenter - (BANDS_HEIGHT-1) / 2 ) : 0;
			m_HorizontalLimits.xEndIndex = (( xCenter + BANDS_HEIGHT / 2 ) < m_cropRectangle.width ) ?
											( xCenter + BANDS_HEIGHT / 2 ) : (m_cropRectangle.width - 1);
		break;

		case 3:
			m_HorizontalLimits.yStartIndexHighBand = yCenter - BLACK_BAND_DISPLACEMENT_FROM_CENTER - BANDS_WIDTH;
			m_HorizontalLimits.yEndIndexHighBand = yCenter - BLACK_BAND_DISPLACEMENT_FROM_CENTER;
			m_HorizontalLimits.yStartIndexCentralBand = yCenter - HALVE_AND_ROUND_UP(BANDS_WIDTH);
			m_HorizontalLimits.yEndIndexCentralBand = yCenter + HALVE_AND_ROUND_UP(BANDS_WIDTH);
			m_HorizontalLimits.yStartIndexLowBand = yCenter + BLACK_BAND_DISPLACEMENT_FROM_CENTER;
			m_HorizontalLimits.yEndIndexLowBand = yCenter + BLACK_BAND_DISPLACEMENT_FROM_CENTER + BANDS_WIDTH;
			m_HorizontalLimits.xStartIndex = ( xCenter >= (BANDS_HEIGHT-1)/2 ) ? ( xCenter - (BANDS_HEIGHT-1) / 2 ) : 0;
			m_HorizontalLimits.xEndIndex = (( xCenter + BANDS_HEIGHT / 2 ) < m_cropRectangle.width )?
											( xCenter + BANDS_HEIGHT / 2 ) : (m_cropRectangle.width - 1);
		break;

		default:
			m_pLogger->log(LOG_ERR, "SampleDetectionHorizontal::getLimits: wrong wellNumber set (%d)", getWellNumber());
			return false;
		break;
	}

	return true;
}

int8_t SampleDetectionHorizontal::findOptimalCentralScanRecursively(uint32_t* pulData, bitCollectionStruct* pCollection, int8_t &cOffset)
{
	if ( pulData == nullptr )		return 0;
	if ( pCollection == nullptr )	return 0;
	if ( m_bConfigOK == false )		return 0;

	uint32_t uliBandWidth = m_HorizontalLimits.xEndIndex - m_HorizontalLimits.xStartIndex + 1;
	uint32_t uliBandHeight = m_HorizontalLimits.yEndIndexHighBand - m_HorizontalLimits.yStartIndexHighBand + 1;

	if ( ( (uint)m_nImageHeight <= (m_HorizontalLimits.yStartIndexCentralBand+uliBandHeight+HOR_MAX_OFFSET_FOR_EACH_RETRY) ) ||
	   ( ( m_HorizontalLimits.yStartIndexCentralBand-HOR_MAX_OFFSET_FOR_EACH_RETRY) < 0 ) )
	{
		return 0;
	}


	uint8_t cSize = HOR_MAX_OFFSET_FOR_EACH_RETRY * 2 + 1;	 // 7 because the control is performed from -3 to 3 pixel below/above
	vector<uint32_t> vliRefArray(cSize);
	vector<uint32_t> vliAVGarray(uliBandWidth);

	for (int8_t h = -HOR_MAX_OFFSET_FOR_EACH_RETRY; h <= HOR_MAX_OFFSET_FOR_EACH_RETRY; ++h)
	{
		for (uint8_t i = 0; i < uliBandWidth; ++i)
		{
			for (uint8_t j = 0; j < uliBandHeight; ++j)
				pCollection->ulCentralBand[uliBandWidth*j + i] = *(pulData + m_cropRectangle.width*(m_HorizontalLimits.yStartIndexCentralBand+j+h) + (i+m_HorizontalLimits.xStartIndex) );
		}

		fill(vliAVGarray.begin(), vliAVGarray.end(), 0);

		for (uint8_t i = 0; i < uliBandWidth; ++i)
		{
			for (uint8_t j = 0; j < uliBandHeight; ++j)
			{
				vliAVGarray[i] += pCollection->ulCentralBand[j*uliBandWidth + i];
			}
			vliAVGarray[i] /= uliBandHeight;
		}

		for (uint8_t i = 0; i < uliBandWidth; ++i)
			vliRefArray[h+HOR_MAX_OFFSET_FOR_EACH_RETRY] += vliAVGarray[i];

		vliRefArray[h+HOR_MAX_OFFSET_FOR_EACH_RETRY] /= uliBandWidth;
	}

	// ucIdX is assigned to be 3 by default, because it corresponds to a 0 offset in the next switch case.
	uint8_t ucIdX = 3;
	uint32_t uliMaxVal = 0;
	for (uint8_t i = 0; i < cSize; ++i)
	{
		if ( uliMaxVal < vliRefArray[i] )
		{
			uliMaxVal = vliRefArray[i];
			ucIdX = i;
		}
	}

	int8_t cOffs;

	switch ( ucIdX )
	{
		case 0:		cOffs = -3;		break;
		case 1:		cOffs = -2;		break;
		case 2:		cOffs = -1;		break;
		case 3:		cOffs = 0;		break;
		case 4:		cOffs = 1;		break;
		case 5:		cOffs = 2;		break;
		case 6:		cOffs = 3;		break;

		default:	cOffs = 0;		break;
	}

	m_HorizontalLimits.yStartIndexCentralBand += cOffs;
	m_HorizontalLimits.yEndIndexCentralBand += cOffs;

	cOffset += cOffs;

	if ( ( int(abs(cOffs)) == HOR_MAX_OFFSET_FOR_EACH_RETRY ) && (m_HorizontalLimits.yEndIndexCentralBand < int(m_HorizontalLimits.yStartIndexLowBand-uliBandHeight)) )
	{
		if ( abs(cOffset) < HOR_MAX_OFFSET_ADMITTED )
			findOptimalCentralScanRecursively(pulData, pCollection, cOffset);
	}

	return cOffset;
}

float SampleDetectionHorizontal::calculateScore(differenceStruct* pHorDiffStruct, AVGstruct* pAVGstruct)
{
	uint32_t uliBandWidth = m_HorizontalLimits.xEndIndex - m_HorizontalLimits.xStartIndex + 1;
	int32_t  liMinAVGcentral = INT32_MAX;
	int32_t  liMaxAVGcentral = 0;
	int32_t  liMinUpperDiff = INT32_MAX;
	int32_t  liMinLowerDiff = INT32_MAX;
	float fScore = 0;

	for (uint8_t i=0; i<uliBandWidth; ++i)
	{
		liMinAVGcentral = liMinAVGcentral > pAVGstruct->ulCentralBand[i] ? pAVGstruct->ulCentralBand[i] : liMinAVGcentral;
		liMaxAVGcentral = liMaxAVGcentral < pAVGstruct->ulCentralBand[i] ? pAVGstruct->ulCentralBand[i] : liMaxAVGcentral;

		liMinUpperDiff = liMinUpperDiff > pHorDiffStruct->ilUpperDifference[i] ? pHorDiffStruct->ilUpperDifference[i] : liMinUpperDiff;
		liMinLowerDiff = liMinLowerDiff > pHorDiffStruct->ilLowerDifference[i] ? pHorDiffStruct->ilLowerDifference[i] : liMinLowerDiff;
	}

	int32_t liMedianValueCentralScan = median(pAVGstruct->ulCentralBand);
	int32_t liDeltaMax = liMaxAVGcentral - liMedianValueCentralScan;
	int32_t liDeltaMin = liMedianValueCentralScan - liMinAVGcentral;
	int32_t liMedianValue;
	int32_t liLowThreshold;
	int32_t liHighThreshold;
	uint8_t cSwitch;

	/*!*  Find the which difference array to consider, if the upper or lower difference.  ***/

	if ( liMinUpperDiff <= 0 && liMinLowerDiff <= 0 )
	{
		if ( liMinUpperDiff < 0 && liMinLowerDiff < 0 )
			fScore -= (0.3 * uliBandWidth);

		if ( pHorDiffStruct->ilUpperIntegral > pHorDiffStruct->ilLowerIntegral )
			cSwitch = 0;
		else
			cSwitch = 1;
	}
	else if ( liMinUpperDiff > 0 && liMinLowerDiff <= 0 )
		cSwitch = 0;
	else if ( liMinUpperDiff <= 0 && liMinLowerDiff > 0 )
		cSwitch = 1;
	else if ( pHorDiffStruct->ilUpperIntegral > pHorDiffStruct->ilLowerIntegral )
		cSwitch = 0;
	else
		cSwitch = 1;

	switch ( cSwitch )
	{
		case 0:
			liMedianValue = median(pHorDiffStruct->ilUpperDifference);
			liLowThreshold = liMedianValue - liDeltaMin;
			liHighThreshold = liMedianValue + liDeltaMax;

			liLowThreshold = liLowThreshold < 0 ? 0 : liLowThreshold;
			liHighThreshold = liHighThreshold < 0 ? 0 : liHighThreshold;

			for (uint8_t i = 0; i < uliBandWidth; ++i)
			{
				if ( (pHorDiffStruct->ilUpperDifference[i] > liLowThreshold) && (pHorDiffStruct->ilUpperDifference[i] < liHighThreshold) )
					fScore += HOR_POSITIVE_SCORE;
				else if ( pHorDiffStruct->ilUpperDifference[i] <= 0 )
					fScore -= HOR_NEGATIVE_SCORE;
			}
		break;

		case 1:
			liMedianValue = median(pHorDiffStruct->ilLowerDifference);
			liLowThreshold = liMedianValue - liDeltaMin;
			liHighThreshold = liMedianValue + liDeltaMax;

			liLowThreshold = liLowThreshold < 0 ? 0 : liLowThreshold;
			liHighThreshold = liHighThreshold < 0 ? 0 : liHighThreshold;

			for (uint8_t i=0; i<uliBandWidth; ++i)
			{
				if ( (pHorDiffStruct->ilLowerDifference[i] > liLowThreshold) && (pHorDiffStruct->ilLowerDifference[i] < liHighThreshold) )
					fScore += HOR_POSITIVE_SCORE;
				else if ( pHorDiffStruct->ilLowerDifference[i] <= 0 )
					fScore -= HOR_NEGATIVE_SCORE;
			}
		break;

		default:	break;
	}

	fScore /= uliBandWidth;

	if ( fScore < LOWER_LIMIT_SCORE )		fScore = LOWER_LIMIT_SCORE;

	return fScore;
}

int32_t SampleDetectionHorizontal::median(vector<int32_t> vect)
{
	size_t usiSize = vect.size();

	if ( usiSize == 0 )  return -255;

	/*!*  Sort first the array to be able to get the median then.  ***/
	int32_t liTmpPos;
	int32_t liTmp;

	for (uint32_t i = 0; i < vect.size(); ++i)
	{
		liTmpPos = i;
		for (uint32_t j = i + 1; j < vect.size(); ++j)
		{
			if ( vect[j] < vect[liTmpPos] )
			{
				liTmpPos = j;
			}
		}

		liTmp = vect[liTmpPos];
		vect[liTmpPos] = vect[i];
		vect[i] = liTmp;
	}

	/*!*********************  Get the median.  ***********************/

	if (usiSize % 2 == 0)
		return (vect[usiSize / 2 - 1] + vect[usiSize / 2]) / 2;
	else
		return vect[usiSize / 2];
}
