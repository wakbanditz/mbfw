/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionUtility.cpp
 @author  BmxIta FW dept
 @brief   Contains the support functions for the SampleDetection classes.
 @details

 ****************************************************************************
*/

#include "SampleDetectionInclude.h"


// To get directory files.
int getdir (string strDir, vector<string> &svFiles)
{
	DIR *dp;
	struct dirent *dirp;
	if( (dp  = opendir(strDir.c_str())) == NULL)
	{
		printf("Error(%s) opening %s", strerror(errno), strDir.c_str());
		return errno;
	}

	while ( (dirp = readdir(dp)) != NULL)
	{
		string strFileName = string(dirp->d_name);		// Needed to exclude "." and ".." files.
		if ( strFileName.length() > 4 )
			svFiles.push_back(string(dirp->d_name));
	}
	closedir(dp);
	return 0;
}



int32_t getVal4(char *p)
{
	union
	{
		uint32_t i;
		unsigned char c[4];
	} endiannessTest;

	endiannessTest.i = {0x01020304};

if ( endiannessTest.c[0] == 4 )
{
	// little endian
	endiannessTest.c[0]=*p++;
	endiannessTest.c[1]=*p++;
	endiannessTest.c[2]=*p++;
	endiannessTest.c[3]=*p++;
}
else
{
	// big endian
	endiannessTest.c[3]=*(p++);
	endiannessTest.c[2]=*(p++);
	endiannessTest.c[1]=*(p++);
	endiannessTest.c[0]=*(p++);
}

	return endiannessTest.i;
}
