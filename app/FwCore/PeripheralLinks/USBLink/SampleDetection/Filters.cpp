/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    Filters.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the Filters class.
 @details

 ****************************************************************************
*/

#include <deque>
#include <algorithm>

#include "Filters.h"

#define ABS_FOR_FLOAT(x)	( ((x) >= 0 ) ? (x) : (-(x)) )
#define WHITE				DECIMAL_DIGITS_FACTOR //255
#define BLACK				0

Filters::Filters()
{
	m_nKernelSize = -1;
	m_nImageWidth = -1;
	m_nImageHeight = -1;
}

Filters::~Filters()
{

}

void Filters::setImageWidth(int32_t liWidth)
{
	m_nImageWidth = liWidth;
}

void Filters::setImageHeight(int32_t liHeight)
{
	m_nImageHeight = liHeight;
}

bool Filters::movingAverageFilter(uint32_t* pulRawData, int32_t liSize, int32_t liWindowSize)
{
	if ( pulRawData == nullptr )	return false;

	uint32_t* rgulTmpData = new uint32_t[liSize]();
	memcpy(rgulTmpData, pulRawData, liSize * sizeof(uint32_t));
	memset(pulRawData, 0x00, liSize);

	uint32_t uliTmp;

	for (int32_t i = 0; i < liSize; ++i)
	{
		uliTmp = 0;
		for (int32_t h = 0; h < liWindowSize; ++h)
		{
			if ( (i-h) >= 0 )
				uliTmp += *(rgulTmpData + i - h);
		}
		uliTmp /= liWindowSize;
		*(pulRawData+i) = uliTmp;
	}

	 delete[] rgulTmpData;

	return true;
}

bool Filters::sobel(uint32_t* pulRawData, uint32_t uliWidth, uint32_t uliHeight)
{
	if ( pulRawData == nullptr )	return false;

	int32_t liPixel;
	int32_t liTmp1, liTmp2, liTmp3;
	int32_t size = uliWidth * uliHeight;
	uint32_t* tmpData = new uint32_t[size]();
	memcpy(tmpData, pulRawData, size * sizeof(uint32_t));

	// process all the matrix with the exception of first&last line&column
	for (uint32_t y = 1; y < (uliHeight-1); ++y)
	{
		for (uint32_t x = 1; x < (uliWidth-1); ++x)
		{
			liTmp1 = uliWidth * (y-1);
			liTmp2 = uliWidth * (y+1);
			liTmp3 = uliWidth * y;

			liPixel = *(tmpData +   liTmp1 + (x-1))
					+(*(tmpData + ((liTmp1 +  x ))) << 1)
					+ *(tmpData +   liTmp1 + (x+1))
					- *(tmpData +   liTmp2 + (x-1))
					-(*(tmpData + ((liTmp2 +  x ))) << 1)
					- *(tmpData +   liTmp2 + (x+1));

			if ( liPixel > MAX_VALUE )	 liPixel = MAX_VALUE;
			else if( liPixel < 0 )		 liPixel = 0;
			*(pulRawData + liTmp3 + x) = liPixel;
		}
	}

	// Replicate - for the first&last line, no corner
	for (uint32_t x = 1; x < (uliWidth-1); ++x)
	{
		// first lineliPixel
		liPixel = *(tmpData +  (x-1))
				+(*(tmpData +   x   ) << 1 )
				+ *(tmpData +  (x+1))
				- *(tmpData +  uliWidth + (x-1))
				-(*(tmpData + (uliWidth +  x  )) << 1)
				- *(tmpData +  uliWidth + (x+1));

		if ( liPixel > MAX_VALUE )		liPixel = MAX_VALUE;
		else if( liPixel < 0 )			liPixel = 0;
		*(pulRawData + x) = liPixel;

		// last line
		liTmp1 = uliWidth * (uliHeight-2);
		liTmp2 = uliWidth * (uliHeight-1);

		liPixel = *(tmpData +   liTmp1 + (x-1))
				+(*(tmpData + ((liTmp1 +  x ))) << 1)
				+ *(tmpData +   liTmp1 + (x+1))
				- *(tmpData +   liTmp2 + (x-1))
				-(*(tmpData + ((liTmp2 +  x ))) << 1)
				- *(tmpData +   liTmp2 + (x+1));

		if ( liPixel > MAX_VALUE )		liPixel = MAX_VALUE;
		else if ( liPixel < 0 )			liPixel = 0;
		*(pulRawData + liTmp2 + x)	 = liPixel;
	}

	// Replicate - for the first&last column, no corner
	for (uint32_t y=1; y<(uliHeight-1); ++y)
	{
		// first column
		liPixel = *(tmpData + (y-1)*uliWidth)
				+(*(tmpData + (y-1)*uliWidth) << 1)
				+ *(tmpData + (y-1)*uliWidth  +  1)
				- *(tmpData + (y+1)*uliWidth)
				-(*(tmpData + (y+1)*uliWidth) << 1)
				- *(tmpData + (y+1)*uliWidth  +  1);

		if ( liPixel > MAX_VALUE )		liPixel = MAX_VALUE;
		else if ( liPixel < 0 )			liPixel = 0;
		*(pulRawData + y*uliWidth)	  = liPixel;

		// last column
		liPixel = *(tmpData + uliWidth *  y    - 2)
				+(*(tmpData + uliWidth *  y    - 1) << 1)
				+ *(tmpData + uliWidth *  y    - 1)
				- *(tmpData + uliWidth * (y+2) - 2)
				-(*(tmpData + uliWidth * (y+2) - 1) << 1)
				- *(tmpData + uliWidth * (y+2) - 1);

		if ( liPixel > MAX_VALUE )		liPixel = MAX_VALUE;
		else if ( liPixel < 0 )			liPixel = 0;
		*(pulRawData + (y+1)*uliWidth - 1) = liPixel;
	}

	// Corner are missing (0;0)
	liPixel = * tmpData
			+(* tmpData << 1)
			+ *(tmpData + 1)
			- *(tmpData + uliWidth)
			-(*(tmpData + uliWidth) << 1 )
			- *(tmpData + uliWidth   + 1 );

	if ( liPixel > MAX_VALUE )		liPixel = MAX_VALUE;
	else if( liPixel < 0 )			liPixel = 0;
	*pulRawData = liPixel;

	// Corner are missing (0;width-1)
	liPixel = *(tmpData +   uliWidth - 2)
			+(*(tmpData +   uliWidth - 1) << 1)
			+ *(tmpData +   uliWidth - 1)
			- *(tmpData + 2*uliWidth - 2)
			-(*(tmpData + 2*uliWidth - 1) << 1)
			- *(tmpData + 2*uliWidth - 1);

	if ( liPixel > MAX_VALUE )		liPixel = MAX_VALUE;
	else if ( liPixel < 0 )			liPixel = 0;
	*(pulRawData + uliWidth - 1) = liPixel;

	// Corner are missing (height,0)
	liTmp1 = (uliHeight-1) * uliWidth;
	liPixel = *(tmpData +  liTmp1 - uliWidth)
			+(*(tmpData +  liTmp1 - uliWidth) << 1)
			+ *(tmpData +  liTmp1 - uliWidth   + 1)
			- *(tmpData +  liTmp1)
			-(*(tmpData + (liTmp1)) << 1)
			- *(tmpData +  liTmp1    + 1);

	if ( liPixel > MAX_VALUE )		liPixel = MAX_VALUE;
	else if ( liPixel < 0 )			liPixel = 0;
	*(pulRawData + liTmp1)	   = liPixel;

	// Corner are missing (height;width-1)
	liTmp1 = uliHeight * uliWidth - 1;
	liPixel = *(tmpData + liTmp1 - uliWidth  -  1)
			+(*(tmpData + liTmp1 - uliWidth) << 1)
			+ *(tmpData + liTmp1 - uliWidth)
			- *(tmpData + liTmp1   -  1)
			-(*(tmpData + liTmp1) << 1)
			- *(tmpData + liTmp1);

	if ( liPixel > MAX_VALUE )		liPixel = MAX_VALUE;
	else if ( liPixel < 0 )			liPixel = 0;
	*(pulRawData + liTmp1)	   = liPixel;

	delete[] tmpData;
	return true;
}

bool Filters::imAdjust(uint32_t* pulRawData, uint32_t uliSize)
{
	if ( pulRawData == nullptr )	return false;

	uint32_t* rgulTmpData = new uint32_t[uliSize]();
	memcpy(rgulTmpData, pulRawData, uliSize*sizeof(uint32_t));

	std::sort(rgulTmpData, rgulTmpData+uliSize);
//	quickSort(rgulTmpData, 0, uliSize);

	uint32_t uliLowLimit = uliSize * 1/100;

	uliLowLimit = (uliLowLimit > 0) ? uliLowLimit : 1;

	uint32_t uliUpperLimit = uliSize * 99/100;
	uint32_t uliMinColorValue = *(rgulTmpData + uliLowLimit - 1);
	uint32_t uliMaxColorValue = *(rgulTmpData + uliUpperLimit);

	if ( uliMaxColorValue == uliMinColorValue )	return true;

	for (uint32_t i = 0; i < uliSize; ++i)
	{
		if ( *(pulRawData+i) < uliMinColorValue )
		{
			*(pulRawData + i) = 0;
		}
		else if ( *(pulRawData+i) > uliMaxColorValue )
		{
			*(pulRawData + i) = MAX_VALUE;
		}
		else
		{
			float fFoo = MAX_PIXEL_VALUE * (*(pulRawData + i) - uliMinColorValue );
			fFoo /= float( uliMaxColorValue - uliMinColorValue );
			*(pulRawData + i) = fFoo * DECIMAL_DIGITS_FACTOR;
							// changing this order, the variable overflows
		}
	}

	delete[] rgulTmpData;
	return true;
}

bool Filters::medFilt2(uint32_t *pulRawData, int32_t liRowWidth, int32_t liColumnHeight, uint8_t liMedianFilterMatrix[2])
{
	if ( pulRawData == nullptr )	return false;

	uint32_t uliImageSize = liRowWidth * liColumnHeight;
	uint32_t uliSize = liMedianFilterMatrix[0] * liMedianFilterMatrix[1];
	uint32_t* pulPortionToBeFiltered = new uint32_t[uliSize];
	uint32_t* pulFilteredData = new uint32_t[uliImageSize];
	memcpy(pulFilteredData, pulRawData, uliImageSize * sizeof(uint32_t));

	uint32_t uliCondition; // odd/even sizes of filter lead to different techniques.

	if ( ( liMedianFilterMatrix[0]%2 == 0 ) && ( liMedianFilterMatrix[1]%2 == 0 ) )
		uliCondition = 0;
	else if ( ( liMedianFilterMatrix[0]%2 == 1 ) && ( liMedianFilterMatrix[1]%2 == 0 ) )
		uliCondition = 1;
	else if ( ( liMedianFilterMatrix[0]%2 == 0 ) && ( liMedianFilterMatrix[1]%2 == 1 ) )
		uliCondition = 2;
	else
		uliCondition = 3;

	switch (uliCondition)
	{
		case 0:
			for (int32_t j = 0; j < liColumnHeight; ++j)
			{
				for (int32_t i = 0; i < liRowWidth; ++i)
				{
					// for every element of the matrix I have to consider a 2D subMatrix
					for (int32_t k = (-liMedianFilterMatrix[0]/2+1); k <= liMedianFilterMatrix[0]/2; ++k)
					{
						for (int32_t l = (-liMedianFilterMatrix[1]/2+1); l <= liMedianFilterMatrix[1]/2; ++l)
						{
							if ( ((i+l < 0) || (j+k < 0)) || ((i+l >= liRowWidth) || (j+k >= liColumnHeight)))
							{
								*pulPortionToBeFiltered = 0;
							}
							else
							{
								*pulPortionToBeFiltered = *( pulFilteredData + i + l + (j+k)*liRowWidth );
							}
							++pulPortionToBeFiltered;
						}
					}
					pulPortionToBeFiltered -= uliSize;
					*(pulRawData + i + j*liRowWidth ) = median(pulPortionToBeFiltered, uliSize);
				}
			}
		break;

		case 3:
		{
			int k_l = -(liMedianFilterMatrix[0]-1)/2;
			int k_r = -k_l;
			int l_l = -(liMedianFilterMatrix[1]-1)/2;
			int l_r = -l_l;
			for (int32_t j = 0; j < liColumnHeight; ++j)
			{
				for (int32_t i = 0; i < liRowWidth; ++i)
				{
					// for every element of the matrix I have to consider a 2D subMatrix
					for (int32_t k = k_l; k <= k_r; ++k)
					{
						for (int32_t l = l_l; l <= l_r; ++l)
						{
							if ( ((i+l < 0) || (j+k < 0)) || ((i+l >= liRowWidth) || (j+k >= liColumnHeight)))
							{
								*pulPortionToBeFiltered = 0;
							}
							else
							{
								*pulPortionToBeFiltered = *( pulFilteredData + i + l + (j+k)*liRowWidth );
							}
							++pulPortionToBeFiltered;
						}
					}
					pulPortionToBeFiltered -= uliSize;
					*(pulRawData + i + j*liRowWidth ) = median(pulPortionToBeFiltered, uliSize);
				}
			}
		}
		break;

		default:
			delete[] pulPortionToBeFiltered;
			delete[] pulFilteredData;
		return false; // this has to be implemented - not used at the moment
	}
	delete[] pulPortionToBeFiltered;
	delete[] pulFilteredData;

	return true;
}

bool Filters::_opt_medFilt2(unsigned char* pucRawData, int32_t liRowWidth, int32_t liColumnHeight, uint8_t liMedianFilterMatrix[2])
{
	if ( pucRawData == nullptr )	return false;

	uint32_t uliImageSize = liRowWidth * liColumnHeight;
	uint32_t uliSize = liMedianFilterMatrix[0] * liMedianFilterMatrix[1];
	unsigned char* pucPortionToBeFiltered = new unsigned char[uliSize];
	unsigned char* pcFilteredData = new unsigned char[uliImageSize];
	memcpy(pcFilteredData, pucRawData, uliImageSize * sizeof(unsigned char));

	uint32_t uliCondition; // odd/even sizes of filter lead to different techniques.

	if ( ( liMedianFilterMatrix[0]%2 == 0 ) && ( liMedianFilterMatrix[1]%2 == 0 ) )
		uliCondition = 0;
	else if ( ( liMedianFilterMatrix[0]%2 == 1 ) && ( liMedianFilterMatrix[1]%2 == 0 ) )
		uliCondition = 1;
	else if ( ( liMedianFilterMatrix[0]%2 == 0 ) && ( liMedianFilterMatrix[1]%2 == 1 ) )
		uliCondition = 2;
	else
		uliCondition = 3;

	int cnt = 0;

	switch (uliCondition)
	{
		case 0:
		{
			int k_l = -liMedianFilterMatrix[0] / 2 + 1;
			int k_r =  liMedianFilterMatrix[0] / 2;
			int l_l = -liMedianFilterMatrix[1] / 2 + 1;
			int l_r =  liMedianFilterMatrix[1] / 2;

			for (int32_t j = 0; j < liColumnHeight; ++j)
			{
				for (int32_t i = 0; i < liRowWidth; ++i)
				{
					// for every element of the matrix I have to consider a 2D subMatrix
					for (int32_t k = k_l; k <= k_r; ++k)
					{
						for (int32_t l = l_l; l <= l_r ; ++l)
						{
							if ( ((i+l < 0) || (j+k < 0)) || ((i+l >= liRowWidth) || (j+k >= liColumnHeight)))
							{
								*pucPortionToBeFiltered = 0;
							}
							else
							{
								*pucPortionToBeFiltered = *( pcFilteredData + i + l + (j+k)*liRowWidth );
							}
							++pucPortionToBeFiltered;
						}
					}
					pucPortionToBeFiltered -= uliSize;
					*(pucRawData + i + j*liRowWidth ) = median(pucPortionToBeFiltered, uliSize);
				}
			}
		}
		break;

		case 3:
		{
			int k_l = -(liMedianFilterMatrix[0]-1)/2;
			int k_r = -k_l;
			int l_l = -(liMedianFilterMatrix[1]-1)/2;
			int l_r = -l_l;
			for (int32_t j = 0; j < liColumnHeight; ++j)
			{
				for (int32_t i = 0; i < liRowWidth; ++i)
				{
					// for every element of the matrix I have to consider a 2D subMatrix
					for (int32_t k = k_l; k <= k_r; ++k)
					{
						for (int32_t l = l_l; l <= l_r; ++l)
						{
							if ( ((i+l < 0) || (j+k < 0)) || ((i+l >= liRowWidth) || (j+k >= liColumnHeight)))
							{
								pucPortionToBeFiltered[cnt] = 0;
							}
							else
							{
								pucPortionToBeFiltered[cnt] = *( pcFilteredData + i + l + (j+k)*liRowWidth );
							}
							++cnt;
						}
					}
					cnt = 0;
					*(pucRawData + i + j*liRowWidth ) = _opt_median(pucPortionToBeFiltered, uliSize);
				}
			}
		}
		break;

		// the others have to be implemented - not used at the moment
		default:
			delete[] pucPortionToBeFiltered;
			delete[] pcFilteredData;
			return false;
			break;
	}
	delete[] pucPortionToBeFiltered;
	delete[] pcFilteredData;

	return true;
}

bool Filters::medFilt1(uint32_t* pulRawData, int32_t liDataLength, uint8_t ucFilterSize)
{
	if ( pulRawData == nullptr )	return false;
	if ( ucFilterSize%2 != 0 )		return false; // this is only because it hasn't been implemented yet - it's different the second for

	uint32_t* rgPortionToBeFiltered = new uint32_t[ucFilterSize];
	uint32_t* rgFilteredData = new uint32_t[liDataLength];

	memcpy(rgFilteredData, pulRawData, liDataLength * sizeof(uint32_t));

	for ( int i = 0; i < liDataLength; i++ )
	{
		for ( int k = (-ucFilterSize/2+1); k <= ucFilterSize/2; k++ )
		{
			if ( ( i+k < 0 ) ||  ( i+k >= liDataLength ) )
			{
				rgPortionToBeFiltered[k+ucFilterSize/2-1] = 0;
			}
			else
			{
				rgPortionToBeFiltered[k+ucFilterSize/2-1] = *( rgFilteredData + k + i );
			}
		}
		*(pulRawData + i) = median(rgPortionToBeFiltered, ucFilterSize);
	}

	delete[] rgPortionToBeFiltered;
	delete[] rgFilteredData;
	return true;
}

bool Filters::cannyFilter(uint32_t* pulRawData, uint32_t* pulEdgeData, uint32_t uliRowWidth, uint32_t uliColumnHeight)
{
	if ( pulRawData == nullptr )	return false;
	if ( pulEdgeData == nullptr )	return false;

	uint32_t uliImageSize = uliRowWidth * uliColumnHeight;
	m_nImageWidth = uliRowWidth;
	m_nImageHeight = uliColumnHeight;

	float* rgfTmpData = new float[uliImageSize];

	for ( unsigned long i = 0; i != uliImageSize; i++ )
	{
		rgfTmpData[i] = pulRawData[i];
	}

	normalizeImage(rgfTmpData, uliImageSize);

	// Gradient along x-axis and y-axis
	float* rgGX = new float[uliImageSize];
	float* rgGY = new float[uliImageSize];

	if ( ! smoothGradient(rgfTmpData, rgGX, rgGY) )		return false;

	/*! ************************************************************************************
	 * Calculate magnitude of the gradient and normalize for threshold selection
	 * ************************************************************************************/

	float* rgMagnitude = new float[uliImageSize];
	elevArraysToPot(rgGX, rgGY, rgMagnitude, uliImageSize);

	float uliMaxMagn = 0;

	for ( unsigned long i = 0; i != uliImageSize; i++ )
	{
		if ( uliMaxMagn < rgMagnitude[i] )
		{
			uliMaxMagn = rgMagnitude[i];
		}
	}

	if ( uliMaxMagn )
	{
		for ( unsigned long i = 0; i != uliImageSize; i++ )
		{
			rgMagnitude[i] /= uliMaxMagn;
		}
	}
	else // if there are issues and the array is all null
	{
		delete[] rgfTmpData;
		delete[] rgGX;
		delete[] rgGY;
		delete[] rgMagnitude;
		return false;
	}


	/*! ************************************************************************************
	 * Determine hysteresis thresholds
	 * ************************************************************************************/

	structCannyThresholds cannyThresholds;
	cannyThresholds = selectThresholds(rgMagnitude, uliImageSize);
	if ( cannyThresholds.fLowThresh == 0.0f && cannyThresholds.fHighThresh == 0.0f )
	{
		delete[] rgfTmpData;
		delete[] rgGX;
		delete[] rgGY;
		delete[] rgMagnitude;
		return false;
	}


	/*! ************************************************************************************
	 * Perform Non-Maximum suppression thining and hysteresis thresholding of edge strength
	 * ************************************************************************************/

	std::vector<int> vIdxStrong;
	// Make the image binary.
	memset(rgfTmpData, BLACK, uliImageSize*sizeof(float));
	bool bRes;

	for (uint8_t ucDir = 0; ucDir != 4; ucDir++ )
	{
		std::vector<int> vIdxLocalMax;
		bRes = cannyFindLocalMaxima(ucDir, rgGX, rgGY, rgMagnitude, vIdxLocalMax);
		if ( ! bRes )
		{
			delete[] rgfTmpData;
			delete[] rgGX;
			delete[] rgGY;
			delete[] rgMagnitude;
			return false;
		}

		std::vector<int> vIdxWeak;

		for ( auto a : vIdxLocalMax )
		{
			if ( rgMagnitude[a] > cannyThresholds.fHighThresh )
			{
				vIdxWeak.push_back(a);
				vIdxStrong.push_back(a);
				rgfTmpData[a] = WHITE;
			}
			else if ( rgMagnitude[a] > cannyThresholds.fLowThresh )
			{
				vIdxWeak.push_back(a);
				rgfTmpData[a] = WHITE;
			}
		}
	}

	bRes = objectSelects(rgfTmpData, pulEdgeData, vIdxStrong);

	delete[] rgfTmpData;
	delete[] rgGX;
	delete[] rgGY;
	delete[] rgMagnitude;

	return bRes;
}

bool Filters::highPassFilter(uint32_t* pulRawData, uint32_t liImageSize, uint32_t uliExtreme)
{
	if ( pulRawData == 0 )	return false;

	for ( unsigned long i = 0; i < liImageSize; i++ )
	{
		if ( *(pulRawData+i) <= uliExtreme )
		{
			*(pulRawData+i) = 0;
		}
	}

	return true;
}

// uliHeight e uliWidth sono quelli dei crop, la dimensione dell'imagine è data dalle variabili membro.
bool Filters::customImadjust(uint32_t* pulRawData, uint32_t y0, uint32_t uliHeight, uint32_t uliWidth)
{
	if ( pulRawData == nullptr )	return false;
	if ( m_nImageWidth == 0 )		return false;

	// I need to consider only the part of interest
	uint32_t uliStartCropX = 19;
	uint32_t uliStartCropY = y0;
	uint32_t uliEndCropX = uliStartCropX + uliWidth;
	uint32_t uliEndCropY = uliStartCropY + uliHeight;
	uint32_t uliSize =  uliWidth * uliHeight;
	uint32_t* rgTmpArr = new uint32_t[uliSize]();
	uint32_t uliPositionFullImage;
	int liCurrPos = 0;

	for ( uint32_t i = uliStartCropY; i < uliEndCropY; ++i )
	{
		for ( uint32_t j = uliStartCropX; j < uliEndCropX; ++j  )
		{
			uliPositionFullImage = i * m_nImageWidth + j;
			rgTmpArr[liCurrPos] = *(pulRawData + uliPositionFullImage);
			liCurrPos++;
		}
	}

	std::sort(rgTmpArr, rgTmpArr+uliSize);
	int liLowIdx = ( uliSize%100 > 50)? uliSize/100+1 : uliSize/100; // only because ARM always round down
	uint32_t uliLowVal;

	if ( liLowIdx > 1 )
	{
		uliLowVal = rgTmpArr[liLowIdx-1];
	}
	else
	{
		uliLowVal = rgTmpArr[1];
	}

	int liHighIdx = floor(99*uliSize/100);
	uint32_t uliHighVal = rgTmpArr[liHighIdx-1];

	for ( int i = 0; i != m_nImageHeight; i++ )
	{
		for ( int j = 0; j != m_nImageWidth; j++ )
		{
			if ( *(pulRawData+i*m_nImageWidth+j) < uliLowVal )
			{
				*(pulRawData+i*m_nImageWidth+j) = 0;
			}
			else if ( *(pulRawData+i*m_nImageWidth+j) > uliHighVal )
			{
				*(pulRawData+i*m_nImageWidth+j) = MAX_PIXEL_VALUE * DECIMAL_DIGITS_FACTOR;
			}
			else
			{
				float foo = *(pulRawData+i*m_nImageWidth+j) - uliLowVal;
				foo *= MAX_PIXEL_VALUE;
				foo /= ( ( uliHighVal - uliLowVal ) / DECIMAL_DIGITS_FACTOR );
				*(pulRawData+i*m_nImageWidth+j) = round(foo / DECIMAL_DIGITS_FACTOR) * DECIMAL_DIGITS_FACTOR;
			}
		}
	}
	delete[] rgTmpArr;
	return true;
}

bool Filters::histCounts(const std::vector<uint32_t>& vData, int liBinNum, std::vector<uint32_t>& vOccurrences, std::vector<float>& vEdges)
{
	if ( liBinNum <= 1 )	return false;

	vOccurrences.resize(liBinNum, 0);
	vEdges.resize(liBinNum+1, 0);

	// first find edges
	uint32_t liMinVal = INT32_MAX;
	uint32_t liMaxVal = 0;
	int liSize = vData.size();

	for ( int i = 0; i < liSize; i++ )
	{
		if ( vData[i] < liMinVal )		liMinVal = vData[i];
		if ( vData[i] > liMaxVal )		liMaxVal = vData[i];
	}

	int liRange = liMaxVal - liMinVal;
	float fRawBinWidth = liRange / liBinNum;

	// The bandwidth has to be at least liBinNum width
	if ( fRawBinWidth < DECIMAL_DIGITS_FACTOR )		return false;

	// Choose the bin width as a "nice" value
	int liPowOfTen = pow(10, floor(log10(fRawBinWidth)));
	float fRelSize = fRawBinWidth / liPowOfTen; // guaranteed in [1, 10)

	// Temporarily set bin width to a nice 10pow, thanit will be set again
	int liBinWidth = liPowOfTen * floor(fRelSize);

	// Set the left edge at multiplies of the raw bin width. Adjust then bin width suche that
	// all bins are of the same size and liMaxVal fall unto the rightmost bin
	// Be careful liBinWidth*(liMinVal/liBinWidth) has integer approssimation, not always = liMinVal
	float fLeftEdge = std::min(liBinWidth*(liMinVal/liBinWidth), liMinVal);

	// binWidth lower and upper bounds
	float fLl = ( float(liMaxVal) - fLeftEdge ) / float(liBinNum);
	float fUl = ( float(liMaxVal) - fLeftEdge ) / float(liBinNum-1);
	int lip10 = pow(10, floor(log10(fUl-fLl)));
	liBinWidth = lip10 * ceil(fLl/lip10);

	float fRightEdge = std::max(fLeftEdge+liBinNum*liBinWidth, float(liMaxVal));

	for ( size_t i = 0; i < vEdges.size()-1; i++ )
	{
		vEdges[i] = fLeftEdge + float(liBinWidth*i);
	}
	vEdges.back() = fRightEdge + 1; // plus 1 is to include also the last val

	// Now I have the edges, I need to find the occurrences
	for ( int m = 0; m < liSize; m++ )
	{
		for ( int n = 0; n < liBinNum; n++ )
		{
			if ( float(vData[m]) >= vEdges[n] && float(vData[m]) < vEdges[n+1] )
			{
				vOccurrences[n]++;
				break;
			}
		}
	}

	return true;
}

bool Filters::mode(uint32_t* pData, size_t uliDataLength, uint32_t* pModeVal)
{
	if ( pData == nullptr )		return false;
	if ( uliDataLength == 0 )	return false;

	quickSort(pData, 0, uliDataLength);

	int liCnt = 1;
	int liMaxCnt = 0;
	*pModeVal = pData[0];
	for (size_t i = 0; i < uliDataLength-1;  i++)
	{
	   if ( pData[i] == pData[i+1] )
	   {
		  liCnt++;
		  if ( liCnt > liMaxCnt )
		  {
			  liMaxCnt = liCnt;
			  *pModeVal = pData[i];
		  }
	   }
	   else
	   {
		  liCnt = 1; // reset counter.
	   }
	}

	return true;
}

uint32_t Filters::median(uint32_t* uliRawData, uint32_t uliSize)
{
	if ( uliRawData == nullptr )	return 0;

	uint32_t* rgulTmpData = new uint32_t[uliSize];
	memcpy(rgulTmpData, uliRawData, uliSize*sizeof(uint32_t));
	std::sort(rgulTmpData, rgulTmpData+uliSize);

	uint32_t uliMedianValue = 0;

	if ( uliSize%2 == 0 )
	{
		uliMedianValue = ( *(rgulTmpData + uliSize/2) + *(rgulTmpData + uliSize/2 - 1) ) / 2;
		// This is needed to be perfectly aligned with Matlab output
		uliMedianValue = ROUND_TO_DECIMAL_FIGURES(uliMedianValue);
	}
	else
	{
		uliMedianValue = *( rgulTmpData + (uliSize-1)/2 ); // I need to take the element in the middle!
	}

	delete[] rgulTmpData;
	return uliMedianValue;
}

unsigned char Filters::median(unsigned char* ucRawData, uint32_t uliSize)
{
	if ( ucRawData == nullptr )	return 0;

	std::sort(ucRawData, ucRawData+uliSize);
	if ( uliSize%2 == 0 )
	{
		int foo = ucRawData[uliSize/2];
		foo += ucRawData[uliSize/2 -1];
		return (unsigned char)(foo/2);
	}
	else
	{
		return ucRawData[(uliSize-1)/2]; // I need to take the element in the middle!
	}
}

unsigned char Filters::_opt_median(unsigned char* ucData, int size)
{
	int liHalf = (size+1) / 2;
	unsigned char ucLeft[liHalf], cRight[liHalf], cMedian, *p;
	unsigned char _Left, _Right;

	// pick first value as median candidate
	p = ucData;
	cMedian = *p++;
	_Left = _Right = 1;

	for (;;)
	{
		// get next value
		unsigned char cVal = *p++;

		// if value is smaller than median, append to left heap
		if ( cVal < cMedian )
		{
			// move biggest value to the heap top
			unsigned char cChild = _Left++, cParent = (cChild - 1) / 2;

			while ( cParent &&  cVal>ucLeft[cParent] )
			{
				ucLeft[cChild] = ucLeft[cParent];
				cChild = cParent;
				cParent = (cParent - 1) / 2;
			}
			ucLeft[cChild] = cVal;

			// if left heap is full
			if ( _Left == 13 )
			{
				// for each remaining value
				for ( unsigned char ucCurr = size - (p - ucData); ucCurr; --ucCurr )
				{
					// get next value
					cVal = *p++;

					// if value is to be inserted in the left heap
					if ( cVal < cMedian )
					{
						cChild = ucLeft[2] > ucLeft[1] ? 2 : 1;
						if ( cVal >= ucLeft[cChild] )
						{
							cMedian = cVal;
						}
						else
						{
							cMedian = ucLeft[cChild];
							cParent = cChild;
							cChild = cParent*2 + 1;

							while ( cChild < liHalf )
							{
								if ( (cChild < liHalf-1) && ucLeft[cChild+1] > ucLeft[cChild] )
								{
									++cChild;
								}
								if ( cVal >= ucLeft[cChild] )
								{
									break;
								}

								ucLeft[cParent] = ucLeft[cChild];
								cParent = cChild;
								cChild = cParent*2 + 1;
							}
							ucLeft[cParent] = cVal;
						}
					}
				}
				return cMedian;
			}
		}
		// else append to right heap
		else
		{
			// move smallest value to the heap top
			unsigned char cChild = _Right++, cParent = (cChild - 1) / 2;

			while ( cParent && cVal<cRight[cParent] )
			{
				cRight[cChild] = cRight[cParent];
				cChild = cParent;
				cParent = (cParent - 1) / 2;
			}
			cRight[cChild] = cVal;

			// if right heap is full
			if ( _Right == liHalf )
			{
				// for each remaining value
				for ( unsigned char ucCurr = size - (p - ucData); ucCurr; --ucCurr )
				{
					// get next value
					cVal = *p++;

					// if value is to be inserted in the right heap
					if ( cVal > cMedian )
					{
						cChild = cRight[2] < cRight[1] ? 2 : 1;
						if( cVal <= cRight[cChild] )
						{
							cMedian = cVal;
						}
						else
						{
							cMedian = cRight[cChild];
							cParent = cChild;
							cChild = cParent*2 + 1;

							while ( cChild < liHalf )
							{
								if ( (cChild < liHalf-1) && cRight[cChild+1] < cRight[cChild] )
								{
									++cChild;
								}
								if ( cVal <= cRight[cChild] )
								{
									break;
								}

								cRight[cParent] = cRight[cChild];
								cParent = cChild;
								cChild = cParent*2 + 1;
							}
							cRight[cParent] = cVal;
						}
					}
				}
				return cMedian;
			}
		}
	}
}

//unsigned char Filters::_opt_median(unsigned char* ucData)
//{
//   unsigned char ucLeft[13], cRight[13], cMedian, *p;
//   unsigned char _Left, _Right;

//   // pick first value as median candidate
//   p = ucData;
//   cMedian = *p++;
//   _Left = _Right = 1;

//   for (;;)
//   {
//	   // get next value
//	   unsigned char cVal = *p++;

//	   // if value is smaller than median, append to left heap
//	   if ( cVal < cMedian )
//	   {
//		   // move biggest value to the heap top
//		   unsigned char cChild = _Left++, cParent = (cChild - 1) / 2;

//		   while ( cParent &&  cVal>ucLeft[cParent] )
//		   {
//			   ucLeft[cChild] = ucLeft[cParent];
//			   cChild = cParent;
//			   cParent = (cParent - 1) / 2;
//		   }
//		   ucLeft[cChild] = cVal;

//		   // if left heap is full
//		   if ( _Left == 13 )
//		   {
//			   // for each remaining value
//			   for ( unsigned char ucCurr = 25 - (p - ucData); ucCurr; --ucCurr )
//			   {
//				   // get next value
//				   cVal = *p++;

//				   // if value is to be inserted in the left heap
//				   if ( cVal < cMedian )
//				   {
//					   cChild = ucLeft[2] > ucLeft[1] ? 2 : 1;
//					   if ( cVal >= ucLeft[cChild] )
//					   {
//						   cMedian = cVal;
//					   }
//					   else
//					   {
//						   cMedian = ucLeft[cChild];
//						   cParent = cChild;
//						   cChild = cParent*2 + 1;

//						   while ( cChild < 13 )
//						   {
//							   if ( cChild < 12 && ucLeft[cChild+1] > ucLeft[cChild] )
//							   {
//								   ++cChild;
//							   }
//							   if ( cVal >= ucLeft[cChild] )
//							   {
//								   break;
//							   }

//							   ucLeft[cParent] = ucLeft[cChild];
//							   cParent = cChild;
//							   cChild = cParent*2 + 1;
//						   }
//						   ucLeft[cParent] = cVal;
//					   }
//				   }
//			   }
//			   return cMedian;
//		   }
//	   }
//	   // else append to right heap
//	   else
//	   {
//		   // move smallest value to the heap top
//		   unsigned char cChild = _Right++, cParent = (cChild - 1) / 2;

//		   while ( cParent && cVal<cRight[cParent] )
//		   {
//			   cRight[cChild] = cRight[cParent];
//			   cChild = cParent;
//			   cParent = (cParent - 1) / 2;
//		   }
//		   cRight[cChild] = cVal;

//		   // if right heap is full
//		   if ( _Right == 13 )
//		   {
//			   // for each remaining value
//			   for ( unsigned char ucCurr = 25 - (p - ucData); ucCurr; --ucCurr )
//			   {
//				   // get next value
//				   cVal = *p++;

//				   // if value is to be inserted in the right heap
//				   if ( cVal > cMedian )
//				   {
//					   cChild = cRight[2] < cRight[1] ? 2 : 1;
//					   if( cVal <= cRight[cChild] )
//					   {
//						   cMedian = cVal;
//					   }
//					   else
//					   {
//						   cMedian = cRight[cChild];
//						   cParent = cChild;
//						   cChild = cParent*2 + 1;

//						   while ( cChild < 13 )
//						   {
//							   if ( cChild < 12 && cRight[cChild+1] < cRight[cChild] )
//							   {
//								   ++cChild;
//							   }
//							   if ( cVal <= cRight[cChild] )
//							   {
//								  break;
//							   }

//							   cRight[cParent] = cRight[cChild];
//							   cParent = cChild;
//							   cChild = cParent*2 + 1;
//						   }
//						   cRight[cParent] = cVal;
//					   }
//				   }
//			   }
//			   return cMedian;
//		   }
//	   }
//   }
//}

void Filters::swap( uint32_t* a, uint32_t* b )
{
	uint32_t t = *a;
	*a = *b;
	*b = t;
}

uint32_t Filters::partition( uint32_t* pulArr, uint32_t uliFirst, uint32_t uliLast )
{
	uint32_t uliPivot = pulArr[uliFirst];
	uint32_t i = uliFirst;
	uint32_t j;

	for( j = uliFirst+1; j < uliLast; j++ )
	{
		if( pulArr[j] <= uliPivot )
		{
			i++;
			swap( &pulArr[i], &pulArr[j] );
		}
	}
	swap( &pulArr[i], &pulArr[uliFirst] );
	return i;
}

void Filters::quickSort( uint32_t* pulArr,  uint32_t uliFirst, uint32_t uliLast )
{
	uint32_t uliTmp;
	if( uliFirst < uliLast )
	{
		uliTmp = partition( pulArr, uliFirst,uliLast );
		quickSort( pulArr, uliFirst, uliTmp );
		quickSort( pulArr, uliTmp+1, uliLast );
	}
}

void Filters::normalizeImage(float* pfData, uint32_t uliImageSize)
{
	if ( pfData == 0 )		return;

	float fMaxVal = MAX_PIXEL_VALUE * DECIMAL_DIGITS_FACTOR;

	for ( unsigned long i = 0; i != uliImageSize; i++)
	{
		pfData[i] /= fMaxVal;
	}
}

bool Filters::smoothGradient(float* pulRawData, float* rgGX, float* rgGY)
{
	if ( ! pulRawData )	return false;
	if ( ! rgGX )		return false;
	if ( ! rgGY )		return false;

	float fSigma = sqrt(2);
	uint8_t ucFilterLength = ceil(4*fSigma);
	m_nKernelSize = ucFilterLength * 2 + 1;

	int x[m_nKernelSize];

	for ( int i = -ucFilterLength; i <= ucFilterLength; i++ )
	{
		x[i+ucFilterLength] = i;
	}

	float fCoeff = 1 / (sqrt(2*M_PI)*fSigma);

	// Need to truncate to 4 decimal figures - Matlab does so
	fCoeff *= 10000;
	int liTmp = round(fCoeff);
	fCoeff = liTmp;

	float rgfGaussKernel[m_nKernelSize];
	float fDenom = 2 * fSigma * fSigma;
	float fSum = 0;

	// Create 1-D Gaussian kernel with sigma = sqrt(2)
	for ( uint8_t i = 0; i < m_nKernelSize; i++)
	{
		rgfGaussKernel[i] = fCoeff;
		rgfGaussKernel[i] *= exp(-(x[i]*x[i])/fDenom);
		fSum += rgfGaussKernel[i];
	}

	// Normalize to ensure kernel sums to one
	for ( int i = 0; i < m_nKernelSize; i++)
	{
		rgfGaussKernel[i] /= fSum;
	}

	// Create 1-D derivative of Gaussian Kernel - size = 6
	float rgfDerGaussKernel[m_nKernelSize];
	if ( ! gradient(rgfGaussKernel, rgfDerGaussKernel) )	return false;

	// Normalize to ensure kernel sums to zero
	float fSumPositive = 0;
	float fSumNegative = 0;

	for ( int i = 0; i < m_nKernelSize; i++)
	{
		if ( rgfDerGaussKernel[i] > 0 )
		{
			fSumPositive += rgfDerGaussKernel[i];
		}
		else
		{
			fSumNegative += rgfDerGaussKernel[i];
		}
	}

	for ( int i = 0; i < m_nKernelSize; i++)
	{
		rgfDerGaussKernel[i] /= ( rgfDerGaussKernel[i] > 0 ) ? fSumPositive : -fSumNegative;
	}

	// Compute smoothed numerical gradient of the data along x direction (horizontal).
	// GX = dG/dx, where G is the Gaussian Smoothed version of the data.

	// Apply Geuss kernel vertically
	imageFilterB(pulRawData, rgGX, rgfGaussKernel, eVertical);

	// Apply derivative Gauss kernel horizontally
	imageFilterA(rgGX, rgGX, rgfDerGaussKernel, eHorizontal);

	// Apply Geuss kernel horizontally
	imageFilterB(pulRawData, rgGY, rgfGaussKernel, eHorizontal);

	// Apply derivative Gauss kernel vertically
	imageFilterA(rgGY, rgGY, rgfDerGaussKernel, eVertical);

	return true;
}

bool Filters::gradient(float* pfInData, float* pfOutFilt)
{
	if ( pfInData == 0 )		return false;
	if ( pfOutFilt == 0 )		return false;

	uint8_t rgucArray[m_nKernelSize];

	for ( int i = 0; i != m_nKernelSize; i++ )
	{
		rgucArray[i] = i;
	}

	// Take forward differences on left and right edges
	if ( m_nKernelSize > 1 )
	{
		pfOutFilt[0] = ( pfInData[1] - pfInData[0] ) / ( rgucArray[1] - rgucArray[0] );
		pfOutFilt[m_nKernelSize-1]  = ( pfInData[m_nKernelSize-1] - pfInData[m_nKernelSize-2] );
		pfOutFilt[m_nKernelSize-1] /= ( rgucArray[m_nKernelSize-1] - rgucArray[m_nKernelSize-2] ); // always 1
	}

	// Take centered differences on interior points
	if ( m_nKernelSize > 2 )
	{
		for ( int i = 1; i != m_nKernelSize-1; i++ )
		{
			pfOutFilt[i]  = pfInData[i+1] - pfInData[i-1];
			pfOutFilt[i] /= ( rgucArray[i+1] - rgucArray[i-1] ); // always 2
		}
	}

	return true;
}

bool Filters::imageFilterA(float* pfData, float* pfFilteredData, float* pfKernel, eCannyDirection eDir)
{
	if ( pfData == nullptr )			return false;
	if ( pfKernel == nullptr )		return false;
	if ( m_nImageWidth == 0 )	return false;
	if ( m_nImageHeight == 0 )	return false;

	// First thing to do is to pad the input based on the dimension of the filter kernel

	uint8_t ucKernelSize = m_nKernelSize;
	uint8_t ucFilterCenter = floor( (ucKernelSize+1) / 2);
	uint8_t ucPadSize = ucKernelSize - ucFilterCenter;

	int liSize;

	if ( eDir == eVertical )
	{
		liSize = m_nImageWidth * ( m_nImageHeight + 2*ucPadSize );
	}
	else
	{
		liSize = ( m_nImageWidth + 2*ucPadSize ) *  m_nImageHeight;
	}

	float* rgfPaddedArray = new float[liSize];

	switch (eDir)
	{
		case eVertical:
		{
			// Add padding
			for ( int i = 0; i != ucPadSize; i++ )
			{
				memcpy(rgfPaddedArray+i*m_nImageWidth, pfData, m_nImageWidth*sizeof(float));
			}

			int liStartPtr = ucPadSize * m_nImageWidth;
			memcpy(rgfPaddedArray+liStartPtr, pfData, m_nImageWidth*m_nImageHeight*sizeof(float));

			liStartPtr = m_nImageWidth * ( m_nImageHeight - 1 );

			for ( int i = m_nImageHeight+ucPadSize; i != m_nImageHeight+2*ucPadSize; i++ )
			{
				memcpy(rgfPaddedArray+i*m_nImageWidth, pfData+liStartPtr, m_nImageWidth*sizeof(float));
			}

			// Now the kernel can be applied to the image
			uint32_t liRealSize = m_nImageWidth * m_nImageHeight;
			float fFoo;

			for ( unsigned long j = 0; j != liRealSize; j++ )
			{
				fFoo = 0;
				for ( int k = 0; k < m_nKernelSize; k++ )
				{
					fFoo -= ( rgfPaddedArray[j+k*m_nImageWidth] * pfKernel[k] );
				}
				pfFilteredData[j] = fFoo;
			}

			break;
		}

		case eHorizontal:
		{
			// Add padding
			uint16_t usFoo = 0;
			uint16_t usNewWidth = m_nImageWidth + 2 * ucPadSize;

			for ( int i = 0; i != m_nImageHeight; i++ )
			{

				for ( int k = 0; k != m_nImageWidth; k++)
				{
					rgfPaddedArray[i*usNewWidth + k + ucPadSize] = pfData[i*m_nImageWidth + k];
				}

				usFoo = ( i + 1 ) * m_nImageWidth - 1;
				for ( int j = 0; j != ucPadSize; j++ )
				{
					rgfPaddedArray[i*usNewWidth + j] = pfData[i*m_nImageWidth];
					rgfPaddedArray[i*usNewWidth + m_nImageWidth + ucPadSize + j] = pfData[usFoo];
				}

			}

			// Now the kernel can be applied to the image
			uint32_t liPaddedWidth = m_nImageWidth + 2 * ucPadSize;

			float fFoo;
			for ( int m = 0; m != m_nImageHeight; m++ )
			{
				for ( int n = 0; n != m_nImageWidth; n++ )
				{
					fFoo = 0;
					for ( int k = 0; k < m_nKernelSize; k++ )
					{
						fFoo -= ( rgfPaddedArray[n+k+m*liPaddedWidth] * pfKernel[k] );
					}
					pfFilteredData[m*m_nImageWidth + n] = fFoo;
				}
			}
			break;
		}

		default:
			delete[] rgfPaddedArray;
			return false;
	}
	delete[] rgfPaddedArray;
	return true;
}

bool Filters::imageFilterB(float* pulData, float* pulFilteredData, float* pfKernel, eCannyDirection eDir)
{
	if ( pulData == nullptr )		return false;
	if ( pfKernel == nullptr )		return false;
	if ( m_nImageWidth == 0 )		return false;
	if ( m_nImageHeight == 0 )		return false;

	// First thing to do is to pad the input based on the dimension of the filter kernel

	uint8_t ucKernelSize = m_nKernelSize;
	uint8_t ucFilterCenter = floor( (ucKernelSize+1) / 2);
	uint8_t ucPadSize = ucKernelSize - ucFilterCenter;

	int liSize;

	if ( eDir == eVertical )
	{
		liSize = m_nImageWidth * ( m_nImageHeight + 2*ucPadSize );
	}
	else
	{
		liSize = ( m_nImageWidth + 2*ucPadSize ) *  m_nImageHeight;
	}

	float* rgfPaddedArray = new float[liSize];

	switch (eDir)
	{
		case eVertical:
		{
			// Add padding
			for ( int i = 0; i != ucPadSize; i++ )
			{
				memcpy(rgfPaddedArray+i*m_nImageWidth, pulData, m_nImageWidth*sizeof(float));
			}

			int liStartPtr = ucPadSize * m_nImageWidth;
			memcpy(rgfPaddedArray+liStartPtr, pulData, m_nImageWidth*m_nImageHeight*sizeof(float));

			liStartPtr = m_nImageWidth * ( m_nImageHeight - 1 );

			for ( int i = m_nImageHeight+ucPadSize; i != m_nImageHeight+2*ucPadSize; i++ )
			{
				memcpy(rgfPaddedArray+i*m_nImageWidth, pulData+liStartPtr, m_nImageWidth*sizeof(float));
			}

			// Now the kernel can be applied to the image
			uint32_t liRealSize = m_nImageWidth * m_nImageHeight;
			float fFoo;

			for ( unsigned long j = 0; j != liRealSize; j++ )
			{
				fFoo = 0;
				for ( int k = 0; k < m_nKernelSize; k++ )
				{
					fFoo += ( rgfPaddedArray[j+k*m_nImageWidth] * pfKernel[k] );
				}
				pulFilteredData[j] = fFoo;
			}
			break;
		}

		case eHorizontal:
		{
			// Add padding
			uint16_t usFoo = 0;
			uint16_t usNewWidth = m_nImageWidth + 2 * ucPadSize;

			for ( int i = 0; i != m_nImageHeight; i++ )
			{

				for ( int k = 0; k != m_nImageWidth; k++)
				{
					rgfPaddedArray[i*usNewWidth + k + ucPadSize] = pulData[i*m_nImageWidth + k];
				}

				usFoo = ( i + 1 ) * m_nImageWidth - 1;
				for ( int j = 0; j != ucPadSize; j++ )
				{
					rgfPaddedArray[i*usNewWidth + j] = pulData[i*m_nImageWidth];
					rgfPaddedArray[i*usNewWidth + m_nImageWidth + ucPadSize + j] = pulData[usFoo];
				}
			}

			// Now the kernel can be applied to the image
			uint32_t liPaddedWidth = m_nImageWidth + 2 * ucPadSize;
			float fFoo;

			for ( int m = 0; m != m_nImageHeight; m++ )
			{
				for ( int n = 0; n != m_nImageWidth; n++ )
				{
					fFoo = 0;
					for ( int k = 0; k < m_nKernelSize; k++ )
					{
						fFoo += ( rgfPaddedArray[n+k+m*liPaddedWidth] * pfKernel[k] );
					}
					pulFilteredData[m*m_nImageWidth + n] = fFoo;
				}
			}
			break;
		}

		default:
			delete[] rgfPaddedArray;
			return false;
	}

	delete[] rgfPaddedArray;
	return true;
}

void Filters::elevArraysToPot(float* pArrA, float* pArrB, float* pArrRes, uint32_t uliLength)
{
	for ( unsigned long i = 0; i != uliLength; i++ )
	{
		pArrRes[i]  = pArrA[i] * pArrA[i];
		pArrRes[i] += pArrB[i] * pArrB[i];
		pArrRes[i]  = sqrt(pArrRes[i]);
	}
}

structCannyThresholds Filters::selectThresholds(float* prgMagn, uint32_t uliMagnLength)
{
	structCannyThresholds cannyThresholds = {0,0};

	if ( prgMagn == nullptr )
	{
		return cannyThresholds;
	}

	// Values ranges in thresholds table: (p-1.5)/(64-1) <= x < (p-0.5)/(64-1), where p is the pth interval.
	// 63 values for 64 ranges.

	float liThresholdsTable64[FILTER_HISTO_BINS_NUM-1];
	for ( int i = 1; i != FILTER_HISTO_BINS_NUM; i++ )
	{
		liThresholdsTable64[i-1] = ( i - 0.5f ) / ( FILTER_HISTO_BINS_NUM - 1 );
	}


	// Binary search is the fastest way to have a 64 bins histogram O(n).

	uint32_t rgiCounts[FILTER_HISTO_BINS_NUM] = {0};

	for ( unsigned long i = 0; i != uliMagnLength; i++ )
	{
		binarySearch(prgMagn+i, rgiCounts, liThresholdsTable64);
	}

	int liSum = 0;
	int liCnt = 0;
	float fPercentage = 0;

	while ( fPercentage < FILTER_CANNY_PERC_OF_PIXELS_NOT_EDGES )
	{
		liSum += rgiCounts[liCnt];
		fPercentage = 100 * liSum / uliMagnLength;
		liCnt++;
	}

	cannyThresholds.fHighThresh = (float)liCnt / FILTER_HISTO_BINS_NUM;
	cannyThresholds.fLowThresh = cannyThresholds.fHighThresh * FILTER_CANNY_THRESHOLD_RATIO;

	return cannyThresholds;
}

void Filters::binarySearch(const float* prgMagn, uint32_t* rgiCounts, float* liThresholdsTable64)
{
	if ( prgMagn == nullptr )		return;
	if ( rgiCounts == nullptr )		return;

	// Corner cases
	if ( *prgMagn < liThresholdsTable64[0] )
	{
		rgiCounts[0]++;
	}
	else if ( *prgMagn >= liThresholdsTable64[FILTER_HISTO_BINS_NUM-2] )
	{
		rgiCounts[FILTER_HISTO_BINS_NUM-1]++;
	}

	int liIdxLow = 0;
	int liIdxHigh = FILTER_HISTO_BINS_NUM - 2;
	int liMid = 0;

	while ( liIdxLow < liIdxHigh )
	{
		liMid = ( liIdxLow + liIdxHigh ) / 2;

		if ( liThresholdsTable64[liMid] == *prgMagn )
		{
			rgiCounts[liMid]++;
			return;
		}

		// If prgMagn is less then threshold table middle element, then search left
		if ( *prgMagn < liThresholdsTable64[liMid] )
		{
			if ( ( liMid > 0 ) && ( *prgMagn >= liThresholdsTable64[liMid-1] ) )
			{
				rgiCounts[liMid]++;
				return;
			}
			liIdxHigh = liMid;
		}
		// If prgMagn is greater then threshold table middle element, then search right
		else
		{
			if ( ( liMid <= liIdxHigh-1 ) && ( *prgMagn < liThresholdsTable64[liMid+1] ) )
			{
				rgiCounts[liMid+1]++;
				return;
			}
			liIdxLow = liMid + 1;
		}
	}

	return;
}

bool Filters::cannyFindLocalMaxima(uint8_t ucDir, float* rgGX, float* rgGY, float* rgMagnitude, std::vector<int>& vIdxLocalMax)
{
	// This sub-function helps with the non-maximum supression in the Canny
	// edge detector.  The input parameters are:

	//		direction - the index of which direction the gradient is pointing,
	//	               read from the diagram below. direction is 0, 1, 2, or 3.
	//		ix        - input image filtered by derivative of gaussian along x
	//		iy        - input image filtered by derivative of gaussian along y
	//		mag       - the gradient magnitude image

	//		there are 4 cases:

	//							The X marks the pixel in question, and each
	//			2     1         of the quadrants for the gradient vector
	//		  O----0----0       fall into two cases, divided by the 45
	//		3 |         | 0     degree line.  In one case the gradient
	//		  |         |       vector is more horizontal, and in the other
	//		  O    X    O       it is more vertical.  There are eight
	//		  |         |       divisions, but for the non-maximum supression
	//	   (0)|         |(3)    we are only worried about 4 of them since we
	//		  O----O----O       use symmetric points about the center pixel.
	//			(1)   (2)

	if ( rgGX == nullptr )			return false;
	if ( rgGY == nullptr )			return false;
	if ( rgMagnitude == nullptr )	return false;
	if ( m_nImageWidth <= 0)		return false;
	if ( m_nImageHeight <= 0)		return false;
	if ( ucDir > 3 )				return false;

	// Index vector
	std::vector<int> vIdx;

	int uliMagnLength = m_nImageWidth * m_nImageHeight;
	int liFoo = uliMagnLength - m_nImageWidth;

	// Find the indices of all points whose gradient is going in the direction we are looking at.

	for ( int i = 0; i != uliMagnLength; i++ )
	{
		// I do not want to consider the border
		if ( i >= m_nImageWidth && i < liFoo )
		{
			if ( ( i%m_nImageWidth != 0 ) && ( (i+1)%m_nImageWidth != 0 ) )
			{
				switch (ucDir)
				{
					case 0:
						if ( ( rgGY[i] <= 0 && rgGX[i] > -rgGY[i] ) || ( rgGY[i] >= 0 && rgGX[i] < -rgGY[i] ) )
						{
							vIdx.push_back(i);
						}
					break;

					case 1:
						if ( ( rgGX[i] > 0 && -rgGY[i] >= rgGX[i] ) || ( rgGX[i] < 0 && -rgGY[i] <= rgGX[i] ) )
						{
							vIdx.push_back(i);
						}
					break;

					case 2:
						if ( ( rgGX[i] <= 0 && rgGX[i] > rgGY[i] ) || ( rgGX[i] >= 0 && rgGX[i] < rgGY[i] ) )
						{
							vIdx.push_back(i);
						}
					break;

					case 3:
						if ( ( rgGY[i] < 0 && rgGX[i] <= rgGY[i] ) || ( rgGY[i] > 0 && rgGX[i] >= rgGY[i] ) )
						{
							vIdx.push_back(i);
						}
					break;

					default:	return false;
				}
			}
		}
	}

	// I am considering only edge pixels
	uint16_t usSize = vIdx.size();
	float* rgX = new float[usSize];
	float* rgY = new float[usSize];
	float* rgMag = new float[usSize];
	float* rgMag1 = new float[usSize];
	float* rgMag2 = new float[usSize];

	for ( int i = 0; i != usSize; i++ )
	{
		rgX[i] = rgGX[vIdx.at(i)];
		rgY[i] = rgGY[vIdx.at(i)];
		rgMag[i] = rgMagnitude[vIdx.at(i)];
	}

	float fTmp;

	switch (ucDir)
	{
		case 0:
			for ( int i = 0 ; i != usSize; i++ )
			{
				fTmp = ( rgX[i] != 0 ) ? ABS_FOR_FLOAT( rgY[i] / rgX[i] ) : 0;
				rgMag1[i] = rgMagnitude[vIdx.at(i)+1] * ( 1 - fTmp );
				rgMag1[i] += rgMagnitude[vIdx.at(i)-m_nImageWidth+1] * fTmp;
				rgMag2[i] = rgMagnitude[vIdx.at(i)-1] * ( 1 - fTmp );
				rgMag2[i] += rgMagnitude[vIdx.at(i)+m_nImageWidth-1] * fTmp;
			}
		break;

		case 1:
			for ( int i = 0 ; i != usSize; i++ )
			{
				fTmp = ( rgY[i] != 0 ) ? ABS_FOR_FLOAT( rgX[i] / rgY[i] ) : 0;
				rgMag1[i] = rgMagnitude[vIdx.at(i)-m_nImageWidth] * ( 1 - fTmp ); // Height or width??
				rgMag1[i] += rgMagnitude[vIdx.at(i)-m_nImageWidth+1] * fTmp;
				rgMag2[i] = rgMagnitude[vIdx.at(i)+m_nImageWidth] * ( 1 - fTmp ); // Height or width??
				rgMag2[i] += rgMagnitude[vIdx.at(i)+m_nImageWidth-1] * fTmp;
			}
		break;

		case 2:
			for ( int i = 0 ; i != usSize; i++ )
			{
				fTmp = ( rgY[i] != 0 ) ? ABS_FOR_FLOAT( rgX[i] / rgY[i] ) : 0;;
				rgMag1[i] = rgMagnitude[vIdx.at(i)-m_nImageWidth] * ( 1 - fTmp ); // Height or width??
				rgMag1[i] += rgMagnitude[vIdx.at(i)-m_nImageWidth-1] * fTmp;
				rgMag2[i] = rgMagnitude[vIdx.at(i)+m_nImageWidth] * ( 1 - fTmp ); // Height or width??
				rgMag2[i] += rgMagnitude[vIdx.at(i)+m_nImageWidth+1] * fTmp;
			}
		break;

		case 3:
			for ( int i = 0 ; i != usSize; i++ )
			{
				fTmp = ( rgX[i] != 0 ) ? ABS_FOR_FLOAT( rgY[i] / rgX[i] ) : 0;
				rgMag1[i] = rgMagnitude[vIdx.at(i)-1] * ( 1 - fTmp ); // Height or width??
				rgMag1[i] += rgMagnitude[vIdx.at(i)-m_nImageWidth-1] * fTmp;
				rgMag2[i] = rgMagnitude[vIdx.at(i)+1] * ( 1 - fTmp ); // Height or width??
				rgMag2[i] += rgMagnitude[vIdx.at(i)+m_nImageWidth+1] * fTmp;
			}
		break;

		default:
			delete[] rgX;
			delete[] rgY;
			delete[] rgMag;
			delete[] rgMag1;
			delete[] rgMag2;
		return false;
	}

	vIdxLocalMax.clear();

	for ( int i = 0; i != usSize; i++ )
	{
		if ( ( rgMag[i] >= rgMag1[i] ) && ( rgMag[i] >= rgMag2[i] ) )
		{
			vIdxLocalMax.push_back(vIdx.at(i));
		}
	}

	delete[] rgX;
	delete[] rgY;
	delete[] rgMag;
	delete[] rgMag1;
	delete[] rgMag2;
	return true;
}

bool Filters::objectSelects(float* pData, uint32_t* pMarker, std::vector<int> vMaskIdx)
{
	// The idea behind this function is to check if the weak edges are connected with the strong ones (marker idx).
	// If they are not, then are discarded, keeped otherwise.

	if ( pData == nullptr )		return false;

	memset(pMarker, 0x00, m_nImageWidth*m_nImageHeight*sizeof(float));

	for ( auto a : vMaskIdx )
	{
		pMarker[a] = WHITE;
	}

	bool bRes = true;
	std::vector<int> vMaskIdChecked;

	for ( auto a : vMaskIdx )
	{
		bRes = floodFill(pData, pMarker, a, vMaskIdChecked);
		if ( bRes == false )
		{
			return bRes;
		}
	}

	return bRes;
}

bool Filters::floodFill(float* pData, uint32_t* pImgMarker, uint32_t liMaskId, std::vector<int>& vMaskIdChecked)
{
	if ( pData == nullptr )			return false;
	if ( pImgMarker == nullptr )	return false;

	uint32_t ulicColor = pData[liMaskId];

	// If the index of the mask is already checked there is no need to repeat the algorithm.

	if ( this->find(vMaskIdChecked, liMaskId) )
	{
		return true;
	}
	else
	{
		vMaskIdChecked.push_back(liMaskId);
	}

	// Queue where to store pixels id to be checked.

	int rgPositions[FILTER_FILL_SIZE] = {-m_nImageWidth-1, -m_nImageWidth, -m_nImageWidth+1, -1,
										  1, m_nImageWidth-1, m_nImageWidth, m_nImageWidth+1 };
	std::deque<int> dq;
	dq.push_back(liMaskId);

	while ( ! dq.empty() )
	{
		int liElem = dq[0];
		dq.pop_front();

		// For the moment the algorithm is implemented only with a 8 pixel connection.
		for ( int i = 0; i<FILTER_FILL_SIZE; i++ )
		{
			if ( ! this->find(vMaskIdChecked, liElem+rgPositions[i] ) )
			{
				if ( pData[liElem+rgPositions[i]] == ulicColor )
				{
					pImgMarker[liElem+rgPositions[i]] = ulicColor;
					dq.push_back(liElem+rgPositions[i]);
					vMaskIdChecked.push_back(liElem+rgPositions[i]);
				}
			}
		}
	}

	return true;
}

bool Filters::find(const std::vector<int>& vVector, int liValue)
{
	uint32_t first = 0;
	uint32_t last = vVector.size();

	while ( first != last )
	{
		if ( vVector[first] == liValue )
		{
			return true;
		}
		first++;
	}

	return false;
}







