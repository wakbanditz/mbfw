/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionHorizontal.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SampleDetectionHorizontal class.
 @details

 ****************************************************************************
*/

#ifndef SAMPLEDETECTIONHORIZONTAL_H
#define SAMPLEDETECTIONHORIZONTAL_H

#include "SampleDetectionBase.h"

class SampleDetectionHorizontal : public SampleDetectionBase
{
	public:

		/*! *************************************************************************************************
		 * @brief SampleDetectionHorizontal default constructor
		 * **************************************************************************************************
		 */
		SampleDetectionHorizontal();

		/*! *************************************************************************************************
		 * @brief ~SampleDetectionHorizontal virtual destructor
		 * **************************************************************************************************
		 */
		virtual ~SampleDetectionHorizontal();
		
		// NO ZEROZERO!!!
		/*! *************************************************************************************************
		 * @brief  init initialize SampleDetectionHorizontal class
		 * @param  pLogger pointer to the logger
		 * @param  pPreliminaryCropRect pointer to the crop rectangle that comes out from the preliminary phase
		 * @param  cWellNum number of the well
		 * @param  liOriginalWidth original width of the cropped image. Needed to find correct hor limits
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool init(Log *pLogger, cropRectangle* pPreliminaryCropRect, int8_t cWellNum, int liOriginalWidth);

		// pCroppedImage is the image preprocessed
		/*! *************************************************************************************************
		 * @brief  horizontalProcessing processing along x direction
		 * @param  pCroppedImage pointer to the cropped image
		 * @param  cDiff offset along y axis due to the foil presence
		 * @param  fScore output of the horizontal algorithm
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool horizontalProcessing(uint32_t* pCroppedImage, int8_t cDiff, float& fScore);

		/*! *************************************************************************************************
		 * @brief clearAll reset all the class
		 * **************************************************************************************************
		 */
		void clearAll(void);
		
	private:

		/*! *************************************************************************************************
		 * @brief  getLimits get the limits for horizontal scan
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool getLimits();

		/*! *************************************************************************************************
		 * @brief  findOptimalCentralScanRecursively ceter again correctly for a perfect horizontal scan
		 * @param  pulData pointer to the image
		 * @param  pCollection pointer to bitCollectionStruct filled by this function
		 * @param  cOffset offset along y axis due to the foil presence
		 * @return shift needed to find the central scan
		 * **************************************************************************************************
		 */
		int8_t findOptimalCentralScanRecursively(uint32_t* pulData, bitCollectionStruct* pCollection, int8_t& cOffset);

		/*! *************************************************************************************************
		 * @brief  calculateScore calculate the score of the algorithm from the two structs
		 * @param  pHorDiffStruct contains the difference between the central and the upper scans and between
		 *		   the central end the lower scans. Also the integral of these 2 curves.
		 * @param  pAVGstruct contains the 3 scans (upper, central and lower)
		 * @return score
		 * **************************************************************************************************
		 */
		float calculateScore(differenceStruct* pHorDiffStruct, AVGstruct* pAVGstruct);

		/*! *************************************************************************************************
		 * @brief  median calculate the median value of the vector passed as input
		 * @param  vect where the median has to be calculated
		 * @return median value
		 * **************************************************************************************************
		 */
		int32_t median(vector<int32_t> vect);

    private:

        int32_t					m_nImageSize;
        int32_t					m_nInitialWidth;
        bool					m_bConfigOK;

        horizontalScansLimits	m_HorizontalLimits;

};

#endif // SAMPLEDETECTIONHORIZONTAL_H
