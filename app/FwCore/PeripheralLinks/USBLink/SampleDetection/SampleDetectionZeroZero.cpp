/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SampleDetectionZeroZero.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SampleDetectionZeroZero class.
 @details

 ****************************************************************************
*/

#include <valarray>
#include <numeric>

#include "SampleDetectionZeroZero.h"
#include "limits.h"
#include "numeric"

#define DX	1
#define DY	4
#define DW	0
#define DH	(-8)

SampleDetectionZeroZero::SampleDetectionZeroZero()
{
	m_nImageSize = -1;
	m_bConfigOK = false;
}

SampleDetectionZeroZero::~SampleDetectionZeroZero()
{

}

bool SampleDetectionZeroZero::init(Log *pLogger, uint32_t uliWidth, uint32_t uliHeight)
{
	m_bConfigOK = false;

	if ( ! setLogger(pLogger) )	return false;
	if ( uliWidth == 0 )		return false;
	if ( uliHeight == 0 )		return false;


	setImageHeight(uliHeight);
	setImageWidth(uliWidth);
	m_nImageSize = m_nImageHeight * m_nImageWidth;
	m_bConfigOK = true;

	return true;
}

bool SampleDetectionZeroZero::getCropRectangleFromZeroZero_Otsu(uint32_t* pulData)
{
	if ( pulData == NULL )		return false;
	if ( m_bConfigOK == false )	return false;

	uint32_t cropRectangle[4] = {0};

	if ( !searchPatternFromImage_Otsu(pulData, m_nImageWidth, m_nImageHeight, cropRectangle) )	return false;

	int32_t liIdCenterPos = checkRectangleCorrectness(pulData, cropRectangle);

	if ( liIdCenterPos == -1 )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionZeroZero::getCropRectangleFromZeroZero: Error in pattern Positioning. Recalibration needed.");
		return false;
	}

	float xCenter;
	float yCenter;
	float uliWidth;
	float uliHeight;

	xCenter = (liIdCenterPos == 0) ? (float(cropRectangle[0]) + float(cropRectangle[2]) / 2.0f) : liIdCenterPos;
	yCenter = float(cropRectangle[1]) + float(cropRectangle[3]) / 2.0f;

	uliWidth = WIDTH_IMAGE_TO_BE_PROCESSED;
	uliHeight = HEIGHT_IMAGE_TO_BE_PROCESSED;

	m_cropRectangle.xStart = round(xCenter - uliWidth / 2.0f);
	m_cropRectangle.yStart = round(yCenter - uliHeight / 2.0f);
	m_cropRectangle.width  = uliWidth + 1;
	m_cropRectangle.height = uliHeight + 1;

	return true;
}

bool SampleDetectionZeroZero::getCropRectangleFromZeroZero(uint32_t* pulData)
{
	if ( pulData == NULL )		return false;
	if ( m_bConfigOK == false )	return false;

	uint32_t cropRectangle[4] = {0};

	if ( !searchPatternFromImage(pulData, m_nImageWidth, m_nImageHeight, cropRectangle) )	return false;

	int32_t liIdCenterPos = checkRectangleCorrectness(pulData, cropRectangle);

	if ( liIdCenterPos == -1 )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionZeroZero::getCropRectangleFromZeroZero: Error in pattern Positioning. Recalibration needed.");
		return false;
	}

	uint32_t xCenter;
	uint32_t yCenter;
	uint32_t uliWidth;
	uint32_t uliHeight;

	xCenter = (liIdCenterPos == 0) ? (cropRectangle[0] + cropRectangle[2] / 2) : liIdCenterPos;
	yCenter = cropRectangle[1] + cropRectangle[3] / 2;

	uliWidth = WIDTH_IMAGE_TO_BE_PROCESSED;
	uliHeight = HEIGHT_IMAGE_TO_BE_PROCESSED;

	m_cropRectangle.xStart = xCenter - HALVE_AND_ROUND_UP(uliWidth);
	m_cropRectangle.yStart = yCenter - HALVE_AND_ROUND_UP(uliHeight);
	m_cropRectangle.width  = uliWidth + 1;
	m_cropRectangle.height = uliHeight + 1;

	return true;
}

bool SampleDetectionZeroZero::convertCropRectangleZeroZero()
{
	if ( m_bConfigOK == false )	return false;

	m_cropRectangle.xStart += DX;
	m_cropRectangle.yStart += DY;
	m_cropRectangle.width  += DW;
	m_cropRectangle.height += DH;

	return true;
}

void SampleDetectionZeroZero::clearAll()
{
	m_nImageSize = -1;
	m_bConfigOK = false;
	m_pLogger = 0;
	m_cWellNumber = -1;
	memset(&m_cropRectangle, 0x00, sizeof(cropRectangle));
}

int32_t SampleDetectionZeroZero::checkRectangleCorrectness(uint32_t* pulData, const uint32_t* rgCropRectangle)
{
	if ( pulData == 0 )			return -1;
	if ( m_bConfigOK == false )	return -1;

	int32_t liRealCenter = 0;
	uint32_t xStartIndex;
	uint32_t xEndIndex;
	uint32_t yStartIndex;
	uint32_t yEndIndex;

	xStartIndex = rgCropRectangle[0];
	xEndIndex = xStartIndex + rgCropRectangle[2];
	yStartIndex = rgCropRectangle[1];
	yEndIndex = yStartIndex + rgCropRectangle[3];


	int liCenterPosLow = 0;
	int liCenterPosHigh = 0;
	int8_t cIdSwitch = 0;

	if ( (xStartIndex == 0) || (int(xEndIndex) == m_nImageWidth) )
	{
		int liYLow = ( yStartIndex > DISTANCE_BETWEEN_PATTERNS ) ? yStartIndex - DISTANCE_BETWEEN_PATTERNS : 1;
		int liYHigh = ( (int)yEndIndex < m_nImageHeight-DISTANCE_BETWEEN_PATTERNS ) ?
						yEndIndex + DISTANCE_BETWEEN_PATTERNS : m_nImageHeight-2;

		unsigned int* rgLowArr = new unsigned int[m_nImageWidth-8];
		unsigned int* rgHighArr = new unsigned int[m_nImageWidth-8];
		unsigned int liMaxLow = 0;
		unsigned int liMaxHigh = 0;

		for ( int i = 4; i < m_nImageWidth-4; i++ )
		{
			rgLowArr[i-4] = *(pulData+liYLow*m_nImageWidth+i);
			rgHighArr[i-4] = *(pulData+liYHigh*m_nImageWidth+i);
		}

		// Apply a median filter 1-D to be sure the max value is due to the pattern and not small artefacts
//		uint8_t ucFilterSize = 4;
//		if ( ! medFilt1(rgLowArr, m_nImageWidth, ucFilterSize) )	return -1;
//		if ( ! medFilt1(rgHighArr, m_nImageWidth, ucFilterSize) )	return -1;

		for ( int i = 4; i < m_nImageWidth-4; i++ )
		{
			if ( liMaxLow < rgLowArr[i-4] )
			{
				liMaxLow = rgLowArr[i-4];
				liCenterPosLow = i;
			}

			if ( liMaxHigh < rgHighArr[i-4] )
			{
				liMaxHigh = rgHighArr[i-4];
				liCenterPosHigh = i;
			}
		}

		delete[] rgLowArr;
		delete[] rgHighArr;
	}
	else
	{
		cIdSwitch = 3;
	}

	if ( ! cIdSwitch )
	{
		if ( liCenterPosHigh && liCenterPosLow )		cIdSwitch = 0;
		else if ( liCenterPosLow )						cIdSwitch = 1;
		else if ( liCenterPosHigh )						cIdSwitch = 2;
		else											cIdSwitch = 4;
	}

	switch ( cIdSwitch )
	{
		case 0:
			liRealCenter = ( liCenterPosHigh + liCenterPosLow ) / 2;
		break;

		case 1:
			liRealCenter = liCenterPosLow;
		break;

		case 2:
			liRealCenter = liCenterPosHigh;
		break;

		case 3:
			return 0;

		case 4:
			m_pLogger->log(LOG_ERR, "SampleDetectionZeroZero::checkRectangleCorrectness: vertical sign of the pattern not detected, system needs to be recalibrated");
			return -1;
	}

	//*!*  If the pattern is not centered, than perform additional checks  ***//

	if ( liRealCenter < (WIDTH_IMAGE_TO_BE_PROCESSED_2 + 1) )
	{
		liRealCenter =  -1;
		m_pLogger->log(LOG_ERR, "SampleDetectionZeroZero::checkRectangleCorrectness: vertical sign of the pattern detected but too shifted, system needs to be recalibrated");
	}
	else if ( (m_nImageWidth-liRealCenter) < (WIDTH_IMAGE_TO_BE_PROCESSED_2 + 1) )
	{
		liRealCenter =  -1;
		m_pLogger->log(LOG_ERR, "SampleDetectionZeroZero::checkRectangleCorrectness: vertical sign of the pattern detected but too shifted, system needs to be recalibrated");
	}

	return liRealCenter;
}

bool SampleDetectionZeroZero::searchPatternFromImage(uint32_t* pulData, uint32_t uliImageWidth, uint32_t uliImageHeight, uint32_t uliCropRectangle[4])
{
	if ( pulData == nullptr )									return false;
	if ( m_bConfigOK == false )									return false;
	if ( ( uliImageWidth == 0 ) || ( uliImageHeight == 0 ) )	return false;

	//*!**********************   Compute vertical waves   ************************//

	uint32_t* prgWavesVertical = new uint32_t[uliImageHeight]();
	uint32_t uliMaxRaw = 0;
	uint16_t usiIdMaxRaw = 0;

	for (uint16_t i = 0; i < uliImageHeight; ++i)
	{
		for (uint16_t h = 0; h < uliImageWidth; ++h)
		{
			*(prgWavesVertical + i) += *(pulData + i*uliImageWidth + h);
		}
		*(prgWavesVertical + i) /= uliImageWidth;

		// find position of the pattern. It is located where the sum is bigger.
		if ( *(prgWavesVertical + i) > uliMaxRaw )
		{
			uliMaxRaw = *(prgWavesVertical + i);
			usiIdMaxRaw = i;
		}
	}


	uint32_t yStartIndex = 0;
	float fRatioForEdgeDetection;

	for (uint32_t i = usiIdMaxRaw; i > VERTICAL_MARGIN; i--)
	{
		fRatioForEdgeDetection = float(*(prgWavesVertical + i)) / float(*(prgWavesVertical + i - VERTICAL_MARGIN));
		if ( fRatioForEdgeDetection > VERTICAL_RATIO )
		{
			yStartIndex = i - VERTICAL_MARGIN;
			break;
		}
	}

	uint32_t yEndIndex = uliImageHeight - 1;

	for (uint32_t i = usiIdMaxRaw + 1; i < (uliImageHeight-VERTICAL_MARGIN); ++i)
	{
		fRatioForEdgeDetection = float(*(prgWavesVertical + i)) / float(*(prgWavesVertical + i + VERTICAL_MARGIN));
		if ( fRatioForEdgeDetection > VERTICAL_RATIO )
		{
			yEndIndex = i + VERTICAL_MARGIN;
			break;
		}
	}

	delete[] prgWavesVertical;
	//*!****  Compute horizontal waves over the image already cropped in y  ******//

	uint32_t* prgWavesHorizontal = new uint32_t[uliImageWidth]();

	for (uint32_t h = 0; h < uliImageWidth; ++h)
	{
		for (uint32_t i = yStartIndex; i <= yEndIndex; ++i)
		{
			*(prgWavesHorizontal + h) += *(pulData + i*uliImageWidth + h);
		}
		*(prgWavesHorizontal + h) /= (yEndIndex - yStartIndex + 1);
	}

	uint32_t xStartIndex = 0;
	uint32_t xEndIndex = uliImageWidth;
	uint32_t uliCenterSearch = HALVE_AND_ROUND_UP(uliImageWidth);
	int liDiffForEdgeDetection;

	for (uint32_t i = (uliCenterSearch-HORIZONTAL_MARGIN); i > xStartIndex; i--)
	{
		liDiffForEdgeDetection = int(*(prgWavesHorizontal + i + HORIZONTAL_MARGIN)) - int(*(prgWavesHorizontal + i));
		if ( liDiffForEdgeDetection > HORIZONTAL_EDGE_WIDTH )
		{
			xStartIndex = i + HALVE_AND_ROUND_UP(HORIZONTAL_MARGIN);
			break;
		}
	}

	for (uint32_t i = uliCenterSearch; i < (uliImageWidth-HORIZONTAL_MARGIN); ++i)
	{
		liDiffForEdgeDetection = int(*(prgWavesHorizontal + i)) - int(*(prgWavesHorizontal + i + HORIZONTAL_MARGIN));
		if ( liDiffForEdgeDetection > HORIZONTAL_EDGE_WIDTH )
		{
			xEndIndex = i + HALVE_AND_ROUND_UP(HORIZONTAL_MARGIN);
			break;
		}
	}

	delete[] prgWavesHorizontal;

	//*!**************  Save results in a cropRectangle array  ******************//

	uliCropRectangle[0] = xStartIndex;
	uliCropRectangle[1] = yStartIndex;
	uliCropRectangle[2] = xEndIndex - xStartIndex;
	uliCropRectangle[3] = yEndIndex - yStartIndex;

	return true;
}

bool SampleDetectionZeroZero::searchPatternFromImage_Otsu(uint32_t* pulData, int32_t liImageWidth, int32_t liImageHeight, uint32_t uliCropRectangle[4])
{
	if ( pulData == nullptr )									return false;
	if ( m_bConfigOK == false )									return false;
	if ( ( liImageWidth <= 0 ) || ( liImageHeight <= 0 ) )		return false;

	//************************   Compute vertical waves   ************************//

	vector<uint32_t> vWavesVertical(liImageHeight);
	uint32_t uliMaxRaw = 0;
	uint16_t usiIdMaxRaw = 0;

	for (uint16_t i = 0; i < liImageHeight; ++i)
	{
		for (uint16_t h = 0; h < liImageWidth; ++h)
		{
			vWavesVertical[i] += *(pulData + i*liImageWidth + h);
		}
		vWavesVertical[i] /= liImageWidth;

		// find position of the pattern. It is located where the sum is bigger.
		if ( vWavesVertical[i] > uliMaxRaw )
		{
			uliMaxRaw = vWavesVertical[i];
			usiIdMaxRaw = i;
		}
	}

	// Find threshold using otsu method
	uint32_t yStartIndex = 0;
	float fThresh = 0.0f;
	int liRes = otsuThresh(vWavesVertical.data(), liImageHeight, fThresh);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionZeroZero::searchPatternFromImage_Otsu: unable to find threshold");
	}

	liRes = binarize(vWavesVertical.data(), liImageHeight, fThresh);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionZeroZero::searchPatternFromImage_Otsu: unable to binarize waves");
	}

	// TODO check if it is enough or at least 3 consecutives have to be 0
	for ( int16_t i = usiIdMaxRaw; i >= 0; --i )
	{
		if ( vWavesVertical[i] == 0 )
		{
			yStartIndex = i;
			break;
		}
	}

	uint32_t yEndIndex = liImageHeight - 1;
	for ( int16_t i = usiIdMaxRaw; i < liImageHeight; ++i )
	{
		if ( vWavesVertical[i] == 0 )
		{
			yEndIndex = i;
			break;
		}
	}

	vWavesVertical.clear();
    vWavesVertical.shrink_to_fit();

	//******  Compute horizontal waves over the image already cropped in y  ******//

	vector<uint32_t> vWavesHorizontal;
	vWavesHorizontal.resize(liImageWidth);

	for (int32_t h = 0; h < liImageWidth; ++h)
	{
		for (uint32_t i = yStartIndex; i <= yEndIndex; ++i)
		{
			vWavesHorizontal[h] += *(pulData + i*liImageWidth + h);
		}
		vWavesHorizontal[h] /= (yEndIndex - yStartIndex + 1);
	}

	// Find threshold using otsu method
	fThresh = 0.0f;
	liRes = otsuThresh(vWavesHorizontal.data(), liImageWidth, fThresh);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionZeroZero::searchPatternFromImage_Otsu: unable to find threshold");
	}

	liRes = binarize(vWavesHorizontal.data(), liImageWidth, fThresh);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "SampleDetectionZeroZero::searchPatternFromImage_Otsu: unable to binarize waves");
	}

	uint32_t xStartIndex = 0;
	uint32_t uliCenterSearch = HALVE_AND_ROUND_UP(liImageWidth);
	for ( int16_t i = uliCenterSearch; i >= 0; --i )
	{
		if ( vWavesHorizontal[i] == 0 )
		{
			xStartIndex = i;
			break;
		}
	}

	uint32_t xEndIndex = liImageWidth - 1;
	for ( int16_t i = uliCenterSearch; i < liImageWidth; ++i )
	{
		if ( vWavesHorizontal[i] == 0 )
		{
			xEndIndex = i;
			break;
		}
	}

	//****************  Save results in a cropRectangle array  ******************//

	uliCropRectangle[0] = xStartIndex;
	uliCropRectangle[1] = yStartIndex;
	uliCropRectangle[2] = xEndIndex - xStartIndex;
	uliCropRectangle[3] = yEndIndex - yStartIndex;

	return true;
}

int SampleDetectionZeroZero::otsuThresh(const uint32_t* pulData, size_t liSize, float& fOtsuThresh)
{
	if ( pulData== nullptr )	return -1;

	// convert the image to uint8_t in order to get only values from 0 to 255
	vector<uint32_t> vImage(pulData, pulData+liSize);
	for ( size_t i = 0; i != liSize; ++i )
	{
		if ( vImage[i]%DECIMAL_DIGITS_FACTOR > DECIMAL_DIGITS_FACTOR/2 )
		{
			// Cut in the correct way
			vImage[i] /= DECIMAL_DIGITS_FACTOR;
			vImage[i]++;
		}
		else
		{
			vImage[i] /= DECIMAL_DIGITS_FACTOR;
		}
	}

	// create image histogram
	size_t liNumBins = 256;
	valarray<float> aBins;
	aBins.resize(liNumBins);
	aBins = 0;
	for ( size_t i = 0; i != liSize; ++i )
	{
		aBins[vImage[i]]++;
	}

	valarray<float> vProbability;
	vProbability.resize(liNumBins);
	vProbability = aBins / aBins.sum();
	vImage.clear();

	valarray<float> aOmega(liNumBins);
	float fCumSum = 0;
	for ( size_t i = 0; i != liNumBins; ++i )
	{
		fCumSum += vProbability[i];
		aOmega[i] = fCumSum;
	}

	valarray<float> aMu;
	aMu.resize(liNumBins);
	fCumSum = 0;
	for ( size_t i = 0; i != liNumBins; ++i )
	{
		fCumSum += ( vProbability[i] * i );
		aMu[i] = fCumSum;
	}

	// Get last element of mu vararray
	float fLastMu = aMu[aMu.size()-1];
	valarray<float> sSigmaSquared;
	sSigmaSquared.resize(liNumBins);
	sSigmaSquared = fLastMu * aOmega;
	sSigmaSquared -= aMu;
	sSigmaSquared *= sSigmaSquared;

	// Prevent num/0 and 0/0
	for ( size_t i = 0; i != liNumBins; ++i )
	{
		if ( aOmega[i] == 0.0f || aOmega[i] == 1.0f )
		{
			sSigmaSquared[i] = 0;
		}
		else
		{
			sSigmaSquared[i] /= ( aOmega[i] * ( 1.0f - aOmega[i] ) );
		}
	}

	float fMaxVal = sSigmaSquared.max();

	if ( fMaxVal !=  0 )
	{
		vector<uint32_t> vIdx;
		for ( size_t i = 0; i != liNumBins; ++i )
		{
			if ( sSigmaSquared[i] == fMaxVal )		vIdx.push_back(i);
		}
		float fAVGIdx = accumulate(vIdx.begin(), vIdx.end(), 0.0f) / float(vIdx.size());
		fOtsuThresh = fAVGIdx / ( liNumBins - 1 );
	}
	else
	{
		fOtsuThresh = 0.0f;
	}

	return 0;
}

int SampleDetectionZeroZero::binarize(uint32_t* pulData, size_t liSize, const float fOtsuThresh)
{
	if ( pulData == nullptr )	return -1;
	if ( liSize == 0 )			return -1;

	float fVal = fOtsuThresh * float(UINT8_MAX) * DECIMAL_DIGITS_FACTOR;
	for ( size_t i = 0; i != liSize; ++i )
	{
		pulData[i] = ( float(pulData[i]) > fVal ) ? 1 : 0;
	}

	return 0;
}
