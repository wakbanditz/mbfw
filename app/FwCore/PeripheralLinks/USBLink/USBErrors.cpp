/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    USBErrors.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the USBErrors class.
 @details

 ****************************************************************************
*/

#include "USBErrors.h"
#include "MainExecutor.h"

USBErrors::USBErrors()
{
	m_pLogger = nullptr;
	m_vectorErrors.clear();
}

USBErrors::~USBErrors()
{
	CameraError * pError;

	for ( int i = m_vectorErrors.size() -1; i >= 0; i-- )
	{
		pError = m_vectorErrors.at(i);
		delete pError;
	}

	m_vectorErrors.clear();
}

int USBErrors::initCameraErr(Log* pLogger)
{
	if ( pLogger == nullptr )	return -1;

	m_pLogger = pLogger;

	int liVectorSize = buildErrorVector();
	if ( liVectorSize == 0  )
	{
		return -1;
	}

	return 0;
}

void USBErrors::clearAllErrors()
{
	CameraError * pError;

	for(size_t i = 0; i < m_vectorErrors.size(); i++)
	{
		pError = m_vectorErrors.at(i);
		pError->setActive(false);
	}
}

int USBErrors::updateErrorsTime(uint64_t ulCurrentMsec, tm* currentTime)
{
	int counter = 0;

	for ( size_t i = 0; i < m_vectorErrors.size(); i++ )
	{
		// check if the message sent from the board correspond to one stored in the list
		CameraError* pCameraError = m_vectorErrors.at(i);

		if ( pCameraError->getActive() == true && pCameraError->getStrTime().empty() )
		{
			// the error is set but no string time is linked to it
			// calculate the difference between the error msecs and the current one

			uint64_t ulErrorMsec = pCameraError->getIntTime();
			uint64_t ulDiffSecs = 0;

			m_pLogger->log(LOG_INFO, "CR8062Errors current %llu : error %llu", ulCurrentMsec, ulErrorMsec);

			if ( ulCurrentMsec > ulErrorMsec )
			{
				ulDiffSecs = ulCurrentMsec - ulErrorMsec;
			}

			float fClkRatio = infoSingleton()->getClockRatio();
			ulDiffSecs = (uint64_t)((float)ulDiffSecs / fClkRatio);

			// secs equivalent to the new time structure
			time_t totalSeconds = mktime(currentTime);
			time_t diffSecToSet = (totalSeconds - (time_t)ulDiffSecs);

			m_pLogger->log(LOG_INFO, "CR8062Errors %llu %llu %llu", totalSeconds, ulDiffSecs, diffSecToSet);

			struct tm * setTime = gmtime(&diffSecToSet);

			char bufferTmp[64];
			sprintf(bufferTmp, "%04d%02d%02d%02d%02d%02d",
				setTime->tm_year + 1900, setTime->tm_mon + 1, setTime->tm_mday,
				setTime->tm_hour, setTime->tm_min, setTime->tm_sec);

			string strTime;
			strTime.assign(bufferTmp);
			pCameraError->setStrTime(strTime);

			m_pLogger->log(LOG_INFO, "CR8062Errors: %s %s",
						   pCameraError->getStrTime().c_str(),
						   pCameraError->getDescription().c_str());
			counter++;
		}
	}
	return counter;
}

int USBErrors::getErrorList(vector<string>* pVectorList, uint8_t errorLevel)
{
	// NOTE don't clear the vector but add elements !
	string strError("");
	size_t i;

	for(i = 0; i < m_vectorErrors.size(); i++)
	{
		// check if the error is activated
		CameraError* pCameraError = m_vectorErrors.at(i);

        if ( pCameraError->getActive() == true && pCameraError->getStrTime().empty() == false
             && pCameraError->getLevel() == errorLevel)
		{
			// the string is formatted as timestamp#code#category#description
			strError.assign(pCameraError->getStrTime());
			strError.append(HASHTAG_SEPARATOR);
			strError.append(pCameraError->getCode());
			strError.append(HASHTAG_SEPARATOR);
			strError.append(pCameraError->getCategory());
			strError.append(HASHTAG_SEPARATOR);
			strError.append(pCameraError->getDescription());

			pVectorList->push_back(strError);
		}
	}

	return pVectorList->size();
}

int USBErrors::buildErrorVector()
{
	// build the errors list
	string strDevice("Camera");
	m_vectorErrors.clear();
	int ret = parseErrorFile(strDevice, (vector<GeneralError*>*)&m_vectorErrors, eErrorCamera);

	return ret;
}

bool USBErrors::decodeNotifyEvent(int liEventCode)
{
	CameraError* pErr = nullptr;
	int liRes = searchEventError(to_string(liEventCode), pErr);

	switch (liRes)
	{
        case 0:
        case 1:
            if(liRes == 0)
            {
                requestVidasEp();
            }
            m_pLogger->log(LOG_INFO, "USBErrors::decodeNotifyEvent: error Flag[%s] Code[%s] Description[%s]",
								   pErr->getFlag().c_str(), pErr->getCode().c_str(), pErr->getDescription().c_str());
        break;

		default:	break;
	}

	return ( liRes != -1 );
}

int USBErrors::searchEventError(const string& strErrCode, CameraError*& pCameraErrorFound)
{
	int liRes = -1;
	pCameraErrorFound = nullptr;

	for ( size_t i = 0; i < m_vectorErrors.size(); i++ )
	{
		// check if the message sent from the board correspond to one stored in the list
		CameraError* pErr = m_vectorErrors.at(i);

		if ( ! pErr->getFlag().compare(strErrCode) )
		{
			// get the info
			string strDescription(pErr->getDescription());

			m_pLogger->log(LOG_INFO, "CR8062Errors::searchEventError: %s - %d - %d - %s - %d",
						   pErr->getCode().c_str(), pErr->getLevel(), pErr->getEnabled(),
						   strDescription.c_str(), pErr->getRestoreEnabled());

			// log the error on Masterboard
			registerErrorEvent(CAMERA_ID, "Camera", pErr->getCode(), pErr->getLevel(),
							   strDescription, pErr->getEnabled(), pErr->getActive());

			pCameraErrorFound = m_vectorErrors.at(i);
			liRes = 1;

			// if the error is enabled (and not yet active)
			// or don't care aboute the status (pressure error can be multiples) -> we activate it
			if ( ((pCameraErrorFound->getEnabled() == true) && (pCameraErrorFound->getActive() == true)) ||
				  (pCameraErrorFound->getActive() == false))
			{
				// read from the database if the time has been set by Gateway
				pErr->setActive(true, infoSingleton()->getSetTime());

				m_pLogger->log(LOG_INFO, "CR8062Errors::searchEventError: %s - %d  %s - %ld",
							   pErr->getCode().c_str(), pErr->getLevel(), strDescription.c_str(),
							   pErr->getIntTime());
				liRes = 0;
			}
			break;
		}
	}

	return liRes;
}
