/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Camera.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CR8062Camera class.
 @details

 ****************************************************************************
*/

#include <limits.h>
#include <algorithm>
#include <fstream>

#include "CR8062Camera.h"

CR8062Camera::CR8062Camera(int liPayloadSize)
{
	setPayloadSize(liPayloadSize);
	m_eImageFormat = eInvalidFormat;
}

CR8062Camera::~CR8062Camera()
{

}

bool CR8062Camera::init(USBInterface* pUSBIfr, Log* pLogger)
{
	bool bRes = CR8062Base::init(pUSBIfr, pLogger);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_INFO, "CR8062Camera::init: unable to init camera");
	}

	string strCDCmode("CMMOPCMUC");
	int liRes = savePlatformSettings(strCDCmode);
	if ( liRes != ERR_NONE )
	{
		pLogger->log(LOG_ERR, "CR8062Camera::init: unable to set CDC communication mode as platform setting");
		return false;
	}

	string strFullSpeed("CMUBSFS1"); // set camera in full speed as platform setting -> never high speed
	liRes = savePlatformSettings(strFullSpeed);
	if ( liRes != ERR_NONE )
	{
		pLogger->log(LOG_ERR, "CR8062Camera::init: unable to set full speed as platform setting");
		return false;
	}
	// check if already updated, if not just do it
	liRes = setFullSpeedCommunication(true);
	if ( liRes != ERR_NONE )
	{
		pLogger->log(LOG_ERR, "CR8062Camera::init: unable to set full speed as platform setting");
		return false;
	}

	string strPacketMode("CMCPPPM1");
	liRes = savePlatformSettings(strPacketMode);
	if ( liRes != ERR_NONE )
	{
		pLogger->log(LOG_ERR, "CR8062Camera::init: unable to set packet mode communication as platform setting");
		return false;
	}
	// check if already updated, if not just do it
	liRes = setPacketMode();
	if ( liRes != ERR_NONE )
	{
		pLogger->log(LOG_ERR, "CR8062Camera::init: unable to set full speed as platform setting");
		return false;
	}

	liRes = setImageMode(eRAW);
	if ( liRes != ERR_NONE )
	{
		pLogger->log(LOG_ERR, "CR8062Camera::init: set image format");
		return false;
	}

	return true;
}

int CR8062Camera::getPicture(vector<unsigned char>& vcImage, structInfoImage* pInfoImage)
{
	if ( pInfoImage == nullptr )	return ERR_CR8062_IN_PARAMETERS;

	if ( ! sendCmd("CDTPXEV1") )	return -1;

	int liPayloadSize = 0;
	int liRes = readAndParseMsg(eAction, liPayloadSize, eGetImageTimeOutMsec);
	if ( liRes < 0 || liRes < (liPayloadSize-75) ) // TODO.. to be deleted once codecorp fixes the bug
	{
		liRes = readAndParseMsg(eAction, liPayloadSize, 100);
	}

	if ( liRes <= 0 || ( liRes < (liPayloadSize-75) ) )
	{
		int liErrCode = ERR_CR8062_TAKING_PICTURE;
		m_pCamErr->decodeNotifyEvent(liErrCode);
		m_pLogger->log(LOG_ERR, "CR8062Camera::getPicture: byte received: %d, payload size: %d",
					   liRes, liPayloadSize);
		return -ERR_CR8062_TAKING_PICTURE;
	}

	// save picture in vector
	pInfoImage->liSize = liRes;
	liRes = parsePicturePayload(pInfoImage, vcImage);

	return liRes;
}

int CR8062Camera::getFWversion(string& strOutFwVersion, uint32_t uliTimeoutMsec)
{
	strOutFwVersion.clear();

	sendCmd("RDFWGVS");
	int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Camera");
	if ( liRes )					return liRes;

	if ( m_strAnswer.empty() )		return ERR_USB_PERIPH_COMMUNICATION;

	// Payload: <RD><FW VS="1.14.1" /></RD>
	size_t usiFoundFirst = 0, usiFoundSecond = 0;

	do
	{
		if ( m_strAnswer[usiFoundFirst] == '"')
			break;
		++usiFoundFirst;
	}
	while( usiFoundFirst < sizeof(m_strAnswer) );

	usiFoundSecond = usiFoundFirst+1;

	// find last quotation Marks
	do
	{
		if ( m_strAnswer[usiFoundSecond] == '"')
			break;
		++usiFoundSecond;
	}
	while( usiFoundSecond < sizeof(m_strAnswer) );

	for (size_t i = usiFoundFirst+1; i < usiFoundSecond; ++i)
		strOutFwVersion.push_back(m_strAnswer[i]);

	if ( strOutFwVersion.empty() )
	{
		m_pLogger->log(LOG_ERR, "CR8062Camera::getFWversion: impossible to retrieve camera FW version. Empty string.");
		return ERR_USB_PERIPH_COMMUNICATION;
	}

	m_pLogger->log(LOG_INFO, "CR8062Camera::getFWversion: camera firmware version is %s.", strOutFwVersion.c_str());
	return ERR_NONE;
}

int CR8062Camera::setCropCoordinates(uint16_t x0, uint16_t y0, uint16_t usiWidth, uint16_t usiHeight, uint32_t uliTimeoutMsec)
{
	if ( usiWidth > MAX_WIDTH || usiHeight > MAX_HEIGHT )					return ERR_CR8062_IN_PARAMETERS;
	if ( (x0+usiWidth) > MAX_WIDTH+1 || (y0+usiHeight) > MAX_HEIGHT+1 )		return ERR_CR8062_IN_PARAMETERS;

	// Get actual parameters
	int X0 = -1, Y0 = -1, siWidth = -1, siHeight = -1;
	bool bEnabled = false;
	int liRes = getCropCoordinates(&X0, &Y0, &siWidth, &siHeight, &bEnabled, uliTimeoutMsec);
	if ( liRes != ERR_NONE )
	{
		m_pLogger->log(LOG_ERR, "CR8062Camera::setCropCoordinates: Unable to get crop coordinates");
		return -1;
	}

	string strCmd("");

	if ( ! bEnabled )
	{
		// enable CROP
		strCmd = "IMCPPEN1";
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Camera");
		if ( liRes )	return liRes;
		msleep(5);
	}

	if ( X0 != x0 )
	{
		// set X origin
		strCmd = "IMCPPWS";
		strCmd.append(to_string(x0));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Camera");
		if ( liRes )	return liRes;
		msleep(5);
	}

	if ( Y0 != y0 )
	{
		// set Y origin
		strCmd = "IMCPPHS";
		strCmd.append(to_string(y0));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Camera");
		if ( liRes )	return liRes;
		msleep(5);
	}

	if ( siWidth != usiWidth )
	{
		// set width of the image
		strCmd = "IMCPPWL";
		strCmd.append(to_string(usiWidth));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Base");
		if ( liRes )	return liRes;
		msleep(5);
	}

	if ( siHeight != usiHeight )
	{
		// set height of the image.
		strCmd = "IMCPPHL";
		strCmd.append(to_string(usiHeight));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Base");
		if ( liRes )	return liRes;
		msleep(5);
	}

	return ERR_NONE;
}

int CR8062Camera::getCropCoordinates(int* pX0, int* pY0, int* pWidth, int* pHeight, bool* bIsEnabled, uint32_t uliTimeoutMsec)
{
	if ( pX0 == nullptr )		return -1;
	if ( pY0 == nullptr )		return -1;
	if ( pWidth == nullptr )	return -1;
	if ( pHeight == nullptr )	return -1;

	// Get actual parameters
	sendCmd("IMCPG");
	int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Camera");
	if ( liRes )	return liRes;

	parseCoordParam(bIsEnabled, pX0, pY0, pWidth, pHeight);

	return ERR_NONE;
}

int CR8062Camera::disableCrop(uint32_t uliTimeoutMsec)
{
	// enable CROP
	sendCmd("IMCPPEN0");

	return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Camera");
}

int CR8062Camera::setJPEGQuality(uint8_t ucJPEGQuality, uint32_t uliTimeoutMsec)
{
	if ( ucJPEGQuality > 100 )	return ERR_COMMAND_PARAMETERS;

	string strSetQuality("ENIMPJQ");
	strSetQuality.append(to_string(ucJPEGQuality));
	sendCmd(strSetQuality);

	return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Camera");
}

int CR8062Camera::camRouteCmd(string& strCmd, vector<unsigned char>& vAns, uint32_t uliTimeoutMsec)
{
	if ( strCmd.empty() )		return -1;

	vAns.clear();
	if ( ! sendCmd(strCmd) )
	{
		string strAns("ERROR");
		copy(strAns.begin(), strAns.end(), back_inserter(vAns));
		return -1;
	}

	string strCommandFamily(strCmd.substr(0,2));
	enumRequestType eRequest = eNoReq;

	/** Look for command type (set/get) **/

	if ( ! strCommandFamily.compare("CF") )
	{
		msleep(200);
		eRequest = ( strCmd[2] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of configuration manager family [%s]", strCmd.c_str());
	}
	if ( ! strCommandFamily.compare("CM") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of communication mode parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("PM") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of power manager parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("AG") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of AGC parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("SC") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of scene manager parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("CD") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of decoder parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("SY") )
	{
		eRequest = ( strCmd[6] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of symbology parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("PK") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of protocol parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("IM") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of image sensor parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("EN") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of encoder image parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("FW") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of firmware parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("RD") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of reader parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("FB") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of feedback parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("LA") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of language parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("MD") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of motion detection family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("ST") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of storage parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("JS") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of javascript parameters family [%s]", strCmd.c_str());
	}
	else if ( ! strCommandFamily.compare("BT") )
	{
		eRequest = ( strCmd[4] == 'G' ) ? eGet : eSet;
		m_pLogger->log(LOG_INFO, "CR8062Camera::camRouteCmd: received command of bluethoot parameters family [%s]", strCmd.c_str());
	}

	vAns.clear();
	if ( eRequest != eNoReq )
	{
		int liPayloadSize = 0;
		int liErrCode = readAndParseMsg(eRequest, liPayloadSize, uliTimeoutMsec);
		if ( liPayloadSize <= 0 && liErrCode )
		{
			string strAns("ERROR");
			copy(strAns.begin(), strAns.end(), back_inserter(vAns));
		}
		else
		{
			vAns.assign(m_PayloadStruct.pktPayload.begin(), m_PayloadStruct.pktPayload.begin()+liPayloadSize);
			vAns.insert(vAns.begin(), '!');
			vAns.push_back('!');
		}
	}

	return ERR_NONE;
}

int CR8062Camera::getImageMode(eImageFormat& eFormat, uint32_t uliTimeoutMsec)
{
	// enable CROP
	string strCmd = "CDIMGET";
	sendCmd(strCmd);
	int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Camera");
	if ( liRes )				return liRes;
	if ( m_strAnswer.empty() )	return ERR_USB_PERIPH_COMMUNICATION;

	signed char cType = -1;

	for ( uint i = 0; i != m_strAnswer.size(); i++ )
	{
		if ( m_strAnswer[i] == '"' )
		{
			cType = m_strAnswer[i+1];
			break;
		}
	}

	if ( cType == -1 )
	{
		return -1;
	}

	eFormat = ( cType == '1' ) ?  eRAW : eJPEG;
	m_eImageFormat = eFormat;
	return 0;
}

eImageFormat CR8062Camera::getImageFormat()
{
	return m_eImageFormat;
}

void CR8062Camera::setImageFormat(eImageFormat eFormat)
{
	m_eImageFormat = eFormat;
}

int CR8062Camera::getReady4Videos(int liQuality, uint32_t uliTimeoutMsec)
{
	if ( liQuality < 1 || liQuality > 100 )	return -1;

	// Full image
	int liRes = disableCrop();
	if ( liRes )	return liRes;
	msleep(5);

	// targeting bar off
	sendCmd("CDTPPTT0");
	liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
	if ( liRes )	return liRes;
	msleep(5);

	// decimate data
	sendCmd("FWTSPID1");
	liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
	if ( liRes )	return liRes;
	msleep(5);

	// set jpeg
	liRes = setImageMode(eJPEG);
	if ( liRes )	return liRes;
	msleep(5);

	// set quality
	string sCmd("ENIMPJQ");
	sCmd.append(to_string(liQuality));
	sendCmd(sCmd);
	liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
	if ( liRes )	return liRes;
	msleep(5);

	// set AGC iterations
	sendCmd("CDTPPAB1");
	liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
	if ( liRes )	return liRes;
	msleep(5);

	return liRes;
}

int CR8062Camera::restoreDefaultAfterVideo(uint32_t uliTimeoutMsec)
{
	// decimate data
	int8_t liRes = 0;

	// perform it twice to prevent errors (in case of video lock)
	for ( int i = 0; i != 2; ++i )
	{
		sendCmd("FWTSPID0");
		liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
		msleep(5);
		if ( ! liRes )	break;
	}
	if ( liRes )	return liRes;

	// targeting bar off
	sendCmd("CDTPPTT100");
	liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
	if ( liRes )	return liRes;
	msleep(5);

	// set AGC iterations
	sendCmd("CDTPPAB9");
	liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
	if ( liRes )	return liRes;
	msleep(5);

	return liRes;
}

int CR8062Camera::_debug_loop(int liQuality, int liLoopsNum)
{
	if ( liLoopsNum < 0 )	return -1;

	int liRes = getReady4Videos(liQuality);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "CR8062Camera::_debug_loop: unable to prepare for videos");
		return -1;
	}

	vector<unsigned char> vcImage;
	int liCnt = 0;
	for ( int i = 0; i != liLoopsNum; ++i )
	{
		// take picture cms
		sendCmd("CDTPXEV1");
		int liPayloadSize = 0;
		int liRes = readAndParseMsg(eAction, liPayloadSize, eGetImageTimeOutMsec);

		/** ************************************************************************
		 * SCENARIOS:
		 *
		 * 1) camera sends the whole image with the exception of the last 62 bytes
		 * 2) after 1) the cmd is sent in the next loop iteration. first read is
		 *    the remaining 62B, than the new image.
		 ************************************************************************* */
		if ( liRes <= 0 || liRes < (liPayloadSize-75) ) // this will be removed when everything will work correctly
		{
			liRes = readAndParseMsg(eAction, liPayloadSize, 800);
		}

		if ( liRes <= 0 || ( liRes < (liPayloadSize-75) ) )
		{
			int liErrCode = ERR_CR8062_TAKING_PICTURE;
			m_pCamErr->decodeNotifyEvent(liErrCode);
			return -1;
		}

		// save picture in vector
		structInfoImage InfoImage;
        InfoImage.liSize = liRes;
		liRes = parsePicturePayload(&InfoImage, vcImage);
		if ( liRes )
		{
			return -1;
		}
		liCnt++;
		m_pLogger->log(LOG_INFO, "Picture num [%d], size [%d]", liCnt, vcImage.size());
		msleep(10);
	}

	liRes = restoreDefaultAfterVideo();
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "CR8062Camera::_debug_loop: unable to restore default after videos");
		return -1;
	}

    return 0;
}

int CR8062Camera::showROI(vector<unsigned char>& vImage, structInfoImage* pInfoImage, uint32_t uliTimeoutMsec)
{
	if ( pInfoImage == nullptr )        return -1;
	if ( pInfoImage->eFormat != eRAW )     return -1;

    // get ROI
    sendCmd("CDOPG");
    int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Camera");
    if ( liRes )
    {
        m_pLogger->log(LOG_ERR, "CR8062Camera::showROI: unable to get ROI coordinates");
        return -1;
	}
	rect rectROI = {};
	liRes = parseRoiParam(&rectROI.x0, &rectROI.y0, &rectROI.width, &rectROI.height);
    if ( liRes )
    {
        m_pLogger->log(LOG_ERR, "CR8062Camera::showROI: unable to parse ROI coordinates");
        return -1;
    }
	msleep(5);

	rect rectCrop = {};
	bool bEnabled = false;
	getCropCoordinates(&rectCrop.x0, &rectCrop.y0, &rectCrop.width, &rectCrop.height, &bEnabled);

	rect rectDraw = {};
	getROIDrawCoord(rectROI, rectCrop, bEnabled, rectDraw);

	// create 2 pixels thick ROI
	for ( int i = 0; i < rectDraw.width; ++i )
    {
		if ( rectDraw.x0+i < pInfoImage->liWidth )
        {
			int idx = pInfoImage->liWidth * rectDraw.y0 + rectDraw.x0 + i;
            vImage[idx] = UINT8_MAX;
			if ( idx-pInfoImage->liWidth >= 0 )
				vImage[idx-pInfoImage->liWidth] = UINT8_MAX;

			idx += ( (rectDraw.height-1) * pInfoImage->liWidth );
            vImage[idx] = UINT8_MAX;
			if ( idx+pInfoImage->liWidth < pInfoImage->liSize )
				vImage[idx+pInfoImage->liWidth] = UINT8_MAX;
        }
    }

	for ( int i = 0; i < rectDraw.height; ++i )
    {
		if ( rectDraw.y0+i < pInfoImage->liHeight )
        {
			int idx = pInfoImage->liWidth * (rectDraw.y0+i) + rectDraw.x0;
            vImage[idx] = UINT8_MAX;
			if ( idx-1 >= 0 )
				vImage[idx-1] = UINT8_MAX;

			idx += ( rectDraw.width - 1 );
            vImage[idx] = UINT8_MAX;
			if ( idx+1 < pInfoImage->liSize )
				vImage[idx+1] = UINT8_MAX;
        }
    }

	return 0;
}

int CR8062Camera::showCross(vector<unsigned char>& vImage, structInfoImage* pInfoImage)
{
	if ( pInfoImage->eFormat == eJPEG )		return -1;

	int liCrossX = pInfoImage->liWidth / 2;
	int liCrossy = pInfoImage->liHeight / 2;

	for ( int i = 0; i < pInfoImage->liHeight; ++i )
	{
		vImage[i*pInfoImage->liWidth+liCrossX] = UINT8_MAX;
	}

	for ( int i = 0; i < pInfoImage->liWidth; ++i )
	{
		vImage[liCrossy*pInfoImage->liWidth+i] = UINT8_MAX;
	}

	return 0;
}

int CR8062Camera::setPictureFOI(uint8_t ucField, uint32_t uliTimeoutMsec)
{
	if ( ucField > FOI_ENTIRE_IMAGE )	return ERR_CR8062_IN_PARAMETERS;

	string strCmd = "CDTPPPF";
	strCmd.append(to_string(ucField));
	sendCmd(strCmd);

	return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Camera");
}

int CR8062Camera::setAGCIterations(uint8_t ucNum, uint32_t uliTimeoutMsec)
{
	string strSetIterations("CDTPPAB");
	strSetIterations.append(to_string(ucNum));
	sendCmd(strSetIterations);

	return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Camera");
}

int CR8062Camera::reconfigureAGCCurves(uint32_t* pValueTable, uint8_t ucWellNum, uint32_t uliTimeoutMsec)
{
	uint32_t rgLocalTable[AGC_TABLE_SIZE];

	if ( pValueTable == NULL )
	{
		switch (ucWellNum)
		{
			case 0:		memcpy(rgLocalTable, AGCTabX0, sizeof(AGCTabX0));	break;
			default:	memcpy(rgLocalTable, AGCTabX3, sizeof(AGCTabX3));	break;
		}
	}
	else
	{
		memcpy(rgLocalTable, pValueTable, sizeof(rgLocalTable));
	}

	// reconfigure exposure time curve.
	stringstream ssMsg;
	ssMsg << "AGNOSE1" << rgLocalTable[0] << ",E2" <<  rgLocalTable[1] << ",E3" <<  rgLocalTable[2]
		  <<     ",E4" << rgLocalTable[3] << ",E5" <<  rgLocalTable[4] << ",E6" <<  rgLocalTable[5];

	sendCmd(ssMsg.str());
	int liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Camera");
	if ( liRes )	return liRes;

	// reconfigure gain curve.
	ssMsg.str("");
	ssMsg << "AGNOSG1" << rgLocalTable[6] << ",G2" <<  rgLocalTable[7] << ",G3" <<  rgLocalTable[8]
		  <<     ",G4" << rgLocalTable[9] << ",G5" <<  rgLocalTable[10] << ",G6" <<  rgLocalTable[11];
	sendCmd(ssMsg.str());
	liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Camera");
	if ( liRes )	return liRes;

	// reconfigure illumination curve.
	ssMsg.str("");
	ssMsg << "AGNOSI1" << rgLocalTable[6] << ",I2" <<  rgLocalTable[7] << ",I3" <<  rgLocalTable[8]
		  <<     ",I4" << rgLocalTable[9] << ",I5" <<  rgLocalTable[10] << ",I6" <<  rgLocalTable[11];
	sendCmd(ssMsg.str());
	liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Camera");
	if ( liRes )	return liRes;

	return ERR_NONE;
}

int CR8062Camera::switch2HIDclass(uint32_t uliTimeoutMsec)
{
	sendCmd("CMMOSCMUN");

	return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Camera");
}

int CR8062Camera::switch2CDCclass(uint32_t uliTimeoutMsec)
{
	sendCmd("CMMOSCMUC");

	return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Camera");
}

bool CR8062Camera::saveBMPimage(uint8_t* pcBuff, uint32_t uliBufLength, uint32_t uliWidth, uint32_t uliHeight, string strFileName)
{
	if ( pcBuff == NULL )	return false;

	// Take care of padding
	int padBytes = 0;
	unsigned char cPad = 0;
	if ( uliWidth % 4 != 0 )
	{
		padBytes = 4 - ( uliWidth % 4 );
		uliBufLength = uliHeight * (uliWidth + padBytes);
	}

	int32_t idx, idxW, idxH;
	bool bResult = false;
	uint32_t uliValPalette, idxVal;
	unsigned char rgcBuff[BMP_HEADER_SIZE];
	BmpHeader sBmpHeader;
	BmpInfoHeader sBmpInfoHeader;

	sBmpHeader.magic[0] = MAGIC_MSB;
	sBmpHeader.magic[1] = MAGIC_LSB;
	sBmpHeader.creator1 = CREATOR1;
	sBmpHeader.creator2 = CREATOR2;
	sBmpHeader.bmp_offset = sizeof(sBmpHeader) + sizeof(sBmpInfoHeader) + PALETTE_LEN;
	sBmpHeader.filesz = sBmpHeader.bmp_offset + uliBufLength;
	sBmpInfoHeader.header_sz = sizeof(sBmpInfoHeader);
	sBmpInfoHeader.width = uliWidth;
	sBmpInfoHeader.height = uliHeight;
	sBmpInfoHeader.nplanes = 1;
	sBmpInfoHeader.bitspp = 8;
	sBmpInfoHeader.compress_type = 0;
	sBmpInfoHeader.bmp_bytesz = uliBufLength;
	sBmpInfoHeader.hres = HRES;
	sBmpInfoHeader.vres = VRES;
	sBmpInfoHeader.ncolors = 0;
	sBmpInfoHeader.nimpcolors = 0;

	memcpy(rgcBuff, &sBmpHeader, sizeof(sBmpHeader));
	memcpy(&rgcBuff[14], &sBmpInfoHeader, sizeof(sBmpInfoHeader));

	FILE* pfile = fopen(strFileName.c_str(), "wb");
	if ( pfile != NULL )
	{
		fwrite(rgcBuff, 1, BMP_HEADER_SIZE, pfile);
		for (idx = 0; idx < PALETTE_NUM; ++idx)
		{
			idxVal = idx;
			uliValPalette = (idxVal << 16) + (idxVal << 8) + idxVal;
			//fwrite(&ValPalette, 1, sizeof(unsigned long), pfile);
			fwrite(&uliValPalette, 1, sizeof(uint32_t), pfile);
		}

		for (idxH = (uliHeight-1); idxH >= 0; idxH--)
		{
			for (idxW = 0; idxW < sBmpInfoHeader.width; ++idxW)
			{
				idx = (idxH * uliWidth) + idxW;
				fwrite(&(pcBuff[idx]), 1, 1, pfile);
			}
			// add padding if present
			for ( int i = 0 ; i < padBytes; ++i )
			{
				fwrite(&cPad, 1, 1, pfile);
			}
		}

		bResult = true;
		fclose(pfile);
	}

	return bResult;
}

void CR8062Camera::swapY(vector<unsigned char>& vImage, int liImageWidth, int liImageHeight, int liTimes)
{
	if ( liTimes >= liImageHeight/2)    return;

	auto begin = vImage.begin() + liTimes * liImageWidth;
	auto end = begin + liImageWidth;
	auto beginLast = vImage.end() - liImageWidth*(liTimes+1);
	swap_ranges(begin, end, beginLast);

	return swapY(vImage, liImageWidth, liImageHeight, ++liTimes);
}

bool CR8062Camera::saveJPEGimage(const vector<unsigned char>& vImage, const string& strFileName)
{
	if ( vImage.empty() || strFileName.empty() )	return false;

	ofstream pic;
	pic.open(strFileName, ios::out | ios::trunc | ios::binary);
	for (const auto &e : vImage)
	{
		pic << e;
	}
	pic.close();
	return true;
}

uint8_t* CR8062Camera::getPicturePayload()
{
	return &m_PayloadStruct.pktPayload[0];
}

int CR8062Camera::setImageMode(eImageFormat eFormat, uint32_t uliTimeoutMsec)
{

	string strCmd("");

	switch (eFormat)
	{
		case eRAW:
			strCmd = "ENIMPET1";
		break;

		case eJPEG:
			strCmd = "ENIMPET3";
		break;

		default:
			return -1;
		break;
	}
	sendCmd(strCmd);

	int liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Camera");
	if ( liRes )	return liRes;
	m_eImageFormat = eFormat;

	return ERR_NONE;
}

int CR8062Camera::parsePicturePayload(structInfoImage* image, vector<unsigned char>& vcData)
{
	if ( image == nullptr )     return -ERR_CR8062_IN_PARAMETERS;
	if ( image->liSize <= 0 )   return -ERR_CR8062_IN_PARAMETERS;

	image->liWidth = 0;
	image->liHeight = 0;

	uint8_t* pPic = getPicturePayload();
	if ( pPic == nullptr )
	{
		return -ERR_CR8062_TAKING_PICTURE;
	}
	else if ( pPic[0] != '<' )
	{
		m_pLogger->log(LOG_ERR, "CR8062Camera::parsePicturePayload: not found '<' char");
		return -ERR_CR8062_TAKING_PICTURE; // invalid picture
	}

	// Find correct start
	int c;
	for ( c = 1; c < image->liSize; c++ )
	{
		if ( pPic[c] == '>' )
		{
			break;
		}
	}
	c++;
	image->liSize -= c;

	// get info
	vcData.clear();
	image->eFormat = getImageFormat();

	string strTmp("");
	bool bWFound = false;
	bool bHFound = false;
	for ( int i = 0; i < c-6; i++ )
	{
		if ( ! bWFound )
		{
			if ( pPic[i] == 'W' && pPic[i+1] == 'I' && pPic[i+2] == 'D' && pPic[i+3] == 'T' && pPic[i+4] == 'H' )
			{
				for ( int j = i+5; j < c; j++ )
				{
					if ( pPic[j] == '"' )
					{
						int l = j+1;
						while ( pPic[l] != '"' )
						{
							strTmp.push_back(pPic[l]);
							l++;
						}
						image->liWidth = stoi(strTmp);
						strTmp.clear();
						bWFound = true;
						break;
					}
				}
			}
		}

		if ( ! bHFound )
		{
			if ( pPic[i] == 'H' && pPic[i+1] == 'E' && pPic[i+2] == 'I' &&
				 pPic[i+3] == 'G' &&  pPic[i+4] == 'H' && pPic[i+5] == 'T' )
			{
				for ( int j = i+6; j < c; j++ )
				{
					if ( pPic[j] == '"' )
					{
						int l = j+1;
						while ( pPic[l] != '"' )
						{
							strTmp.push_back(pPic[l]);
							l++;
						}
						image->liHeight = stoi(strTmp);
						bHFound = true;
						strTmp.clear();
						break;
					}
				}
			}
		}
		if ( bWFound && bHFound )	break;
	}

	if ( image->eFormat == eRAW )
	{
		if ( image->liWidth && image->liHeight )
		{
			image->liSize = image->liWidth * image->liHeight;
		}
	}

	vcData.assign(pPic+c, pPic+c+image->liSize);

	return 0;
}

int CR8062Camera::parseCoordParam(bool* bEnabled, int* X0, int* Y0, int* siWidth, int* siHeight)
{
	if ( bEnabled == nullptr )	return -1;
	if ( X0 == nullptr )		return -1;
	if ( Y0 == nullptr )		return -1;
	if ( siWidth == nullptr )	return -1;
	if ( siHeight == nullptr )	return -1;

	// Expected response <IM><CP TM="0" ME="1" XE="65535" EN="0" WS="0" HS="0" WL="1280" HL="960"/></IM>
	string strVal;
	string strWhat("EN=\"");
	size_t pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		*bEnabled = ( ! strVal.compare("1") ) ? true : false;
	}

	strWhat.assign("WS=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, X0);
	}

	strWhat.assign("HS=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, Y0);
	}

	strWhat.assign("WL=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, siWidth);
	}

	strWhat.assign("HL=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, siHeight);
	}

    return 0;
}

int CR8062Camera::parseRoiParam(int *X0, int *Y0, int *siWidth, int *siHeight)
{
    if ( X0 == nullptr )		return -1;
    if ( Y0 == nullptr )		return -1;
    if ( siWidth == nullptr )	return -1;
    if ( siHeight == nullptr )	return -1;

    // Expected response <CD><OP PR="1" RO="0" RL="0" RT="0" RW="0" RH="0" LC="1" ZR="0"
    // EC="0" DL="0" SP="0" QD="0" PT="0" CI="0" SE="0" AP="115" AT="0" SD="0" FQ="0" CE="0"
    // UT="1" MD="0" DI="0" RD="0" AS="0" VF="0" GB="0" NC="0" N2="0" WN="0" DF="0" DV="0"
    // FO="0" PX="" SX="" FC="0" FD="" SM="" GP="" FP="" UD="" IS="" IO="" SR="0" /></CD>

    string strVal;
    string strWhat("RL=\"");
    size_t pos = m_strAnswer.find(strWhat);
    if ( pos != string::npos )
    {
        size_t size = strWhat.size();
        size_t end = m_strAnswer.find('"', pos+size);
        strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
        wrapStoi(strVal, X0);
    }

    strWhat.assign("RT=\"");
    pos = m_strAnswer.find(strWhat);
    if ( pos != string::npos )
    {
        size_t size = strWhat.size();
        size_t end = m_strAnswer.find('"', pos+size);
        strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
        wrapStoi(strVal, Y0);
    }

    strWhat.assign("RW=\"");
    pos = m_strAnswer.find(strWhat);
    if ( pos != string::npos )
    {
        size_t size = strWhat.size();
        size_t end = m_strAnswer.find('"', pos+size);
        strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
        wrapStoi(strVal, siWidth);
    }

    strWhat.assign("RH=\"");
    pos = m_strAnswer.find(strWhat);
    if ( pos != string::npos )
    {
        size_t size = strWhat.size();
        size_t end = m_strAnswer.find('"', pos+size);
        strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
        wrapStoi(strVal, siHeight);
    }

	strWhat.assign("PF=\"");
    pos = m_strAnswer.find(strWhat);
    int FOI = -1;
    if ( pos != string::npos )
    {
        size_t size = strWhat.size();
        size_t end = m_strAnswer.find('"', pos+size);
        strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
        wrapStoi(strVal, &FOI);
    }

    // field of interest in wide field
	if ( FOI == 1 )
    {
        *X0 += ( MAX_WIDTH / 2);
    }

	return 0;
}

void CR8062Camera::getROIDrawCoord(rect& roi, rect& crop, bool bCropEnabled, rect& draw)
{
	// if all parameters are set to 0, it means no crop is applied
	if ( (crop.x0 == 0 && crop.y0 == 0 && crop.width == 0 && crop.height == 0) || ! bCropEnabled )
	{
		draw = roi;
		return;
	}
	else if ( crop.x0 == roi.x0 && crop.y0 == roi.y0 &&
			  crop.width == roi.width && crop.height == roi.height )
	{
		draw = roi;
		return;
	}

	if ( roi.x0 < crop.x0 )
	{
		draw.x0 = 0;
		draw.width = roi.x0 + roi.width - crop.x0;
	}
	else
	{
		draw.x0 = roi.x0 - crop.x0;
		draw.width = roi.width;
	}

	if ( roi.y0 < crop.y0 )
	{
		draw.y0 = 0;
		draw.height = roi.y0 + roi.height - crop.y0;
	}
	else
	{
		draw.y0 = roi.y0 - crop.y0;
		draw.height = roi.height;
	}

	return;
}
