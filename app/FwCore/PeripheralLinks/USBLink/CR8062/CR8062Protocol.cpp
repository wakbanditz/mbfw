/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Protocol.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CR8062Protocol class.
 @details

 ****************************************************************************
*/

#include "CR8062Protocol.h"

CR8062Protocol::CR8062Protocol()
{
	m_pLogger = nullptr;
	m_pUSBIfr = nullptr;
	m_pHIDIfr = nullptr;
	m_pCamErr = nullptr;

	m_bConfigOk = false;
	m_usTransactionNum = 1;
	m_usiCrc = 0;
	m_liByteReceived = 0;
	m_liByteCopied = 0;
	m_liPktReceived = 0;
	m_liLastError = ERR_NONE;

	memset(&m_HeaderStruct, 0x00, sizeof(m_HeaderStruct));
	memset(&m_ProtocolStruct, 0x00, sizeof(m_ProtocolStruct));
	memset(&m_PayloadStruct.uRequestID, 0x00, sizeof(m_PayloadStruct.uRequestID));
	memset(&m_rgcBuff, 0x00, sizeof(m_rgcBuff));
}


CR8062Protocol::~CR8062Protocol()
{
	// m_pUSBIfr and m_pHIDIfr are used once at a time
	if ( m_pUSBIfr != 0 )
	{
		m_pUSBIfr->closeInterface();
	}
	if ( m_pHIDIfr != 0 )
	{
		m_pHIDIfr->closeHID();
	}
}

bool CR8062Protocol::sendPktCDC(const string& strCmd)
{
	if ( ! m_bConfigOk )		return false;
	if ( ! m_pUSBIfr )			return false;

	// packet size without StartOfFrame, PacketVersion and Packet Length
	int32_t liMsgSize;
	liMsgSize = strCmd.length() + MSG_FIXED_PART_LENGTH;
	uint8_t prgcMsg[liMsgSize + MSG_FIRST_PART_HEADER];
	m_liPktReceived = 0;

	setFixedParameters(prgcMsg);
	*(prgcMsg + 4) = liMsgSize >> MSG_CRC_CHAR_BITS;
	*(prgcMsg + 5) = liMsgSize & 0xFF;
	*(prgcMsg + 15) = PROTO_FLAGS;
	*(prgcMsg + 16) = PROTO_PAYLOAD_CMD;

	if (PROTO_FLAGS & 0x01)
	{
		*(prgcMsg + 17) = 0x00;
		*(prgcMsg + 18) = m_usTransactionNum;
	}
	else
	{
		*(prgcMsg + 17) = 0x00;
		*(prgcMsg + 18) = 0x00;
	}

	*(prgcMsg + 19) = 0x00;
	*(prgcMsg + 20) = m_usTransactionNum;

	*(prgcMsg + 21) = PROTO_REQUEST_ID_0 + ( m_usTransactionNum >> 8 );
	*(prgcMsg + 22) = ( m_usTransactionNum & UINT8_MAX );

	// use only 15 bits of the var, the las one is reserved
	m_usTransactionNum = ( m_usTransactionNum == INT16_MAX ) ? 1 : m_usTransactionNum+1;

//	*(prgcMsg + 21) = PROTO_REQUEST_ID_0;
//	*(prgcMsg + 22) = m_usTransactionNum;

//	m_usTransactionNum = ( m_usTransactionNum == 255 ) ? 1 : m_usTransactionNum+1;

	memcpy((prgcMsg + 23), strCmd.c_str(), strCmd.length()); // command

	crc_t usiCrc = createCRC((prgcMsg+MSG_FIRST_PART_HEADER), liMsgSize);
	*(prgcMsg + 23 + strCmd.length()) = usiCrc >> MSG_CRC_CHAR_BITS;
	*(prgcMsg + 23 + strCmd.length() + 1) = usiCrc & 0xFF;

	// size of the whole packet
	liMsgSize += MSG_FIRST_PART_HEADER;

	if ( m_pUSBIfr->writeBuffer(prgcMsg, liMsgSize, m_liLastError) == -1 )
	{
		m_pLogger->log(LOG_ERR, "CR8062Protocol::sendCmd: error transmitting the packet via USB, %s", errno2CamOperationResult(m_liLastError).c_str());
		return false;
	}

	return true;
}

bool CR8062Protocol::sendPktHID(const string& strCmd)
{
	if ( !m_bConfigOk )		return false;
	if ( ! m_pHIDIfr )		return false;

	// packet size without StartOfFrame, PacketVersion and Packet Length
	int32_t liMsgSize;
	liMsgSize = strCmd.length() + MSG_FIXED_PART_LENGTH;

	m_liPktReceived = 0;

	vector<uint8_t> vMsg;
	vMsg.resize(MSG_LENGTH_UNTIL_PAYLOAD);

	setFixedParameters(vMsg.data());
	vMsg[4] = liMsgSize >> MSG_CRC_CHAR_BITS;
	vMsg[5] = liMsgSize & 0xFF;
	vMsg[15] = PROTO_FLAGS;
	vMsg[16] = PROTO_PAYLOAD_CMD;
	vMsg[17] = 0x00;
	vMsg[18] = 0x00;
	vMsg[19] = 0x00;
	vMsg[20] = m_usTransactionNum;
	vMsg[21] = PROTO_REQUEST_ID_0;
	vMsg[22] = m_usTransactionNum;

	m_usTransactionNum = ( m_usTransactionNum == 255 ) ? 1 : m_usTransactionNum+1;

	for ( auto foo : strCmd )
	{
		vMsg.push_back(foo);
	}

	vector<uint8_t> vSubMsg(vMsg.begin()+MSG_FIRST_PART_HEADER, vMsg.begin()+MSG_FIRST_PART_HEADER+liMsgSize);
	crc_t usiCrc = createCRC(vSubMsg.data(), liMsgSize);

	vMsg.push_back(usiCrc >> MSG_CRC_CHAR_BITS);
	vMsg.push_back(usiCrc & 0xFF);

	// 65 is the maximum admitted, but 1 is the PROTO_HID_REPORT_NUMBER and the other one is the packet size
	int liMsgToBeSent = vMsg.size() / 63;
	if ( vMsg.size() % 63  != 0 )
	{
		liMsgToBeSent++;
	}

	int liByteSent = 0;
	for ( int i = 0; i < liMsgToBeSent; i++ )
	{
		int liBytesToBeSent = 0;
		if ( (vMsg.size()-liByteSent) < 63 )
		{
			liBytesToBeSent = vMsg.size()-liByteSent;
		}
		else
		{
			liBytesToBeSent = 63;
		}
		vector<uint8_t> vTmp(vMsg.begin()+liByteSent, vMsg.begin()+liByteSent+liBytesToBeSent);

		vTmp.insert(vTmp.begin(), vTmp.size());
		vTmp.insert(vTmp.begin(), PROTO_HID_REPORT_NUMBER);
		vTmp.resize(65); // to fill with zeros

		if ( m_pHIDIfr->write(vTmp.data()) == -1 )
		{
			m_pLogger->log(LOG_ERR, "CR8062Protocol::sendCmd: error transmitting the packet via USB, %s",
						   errno2CamOperationResult(m_liLastError).c_str());
			return false;
		}

		liByteSent += liBytesToBeSent;
	}

	return true;
}

bool CR8062Protocol::sendAckCDC()
{
	unsigned char rgAckPkt[27]{}; // actually the size will always be 21 or 27 B
	rgAckPkt[0]  = PROTO_SOH;
	rgAckPkt[1]  = PROTO_START_OF_FRAME_C;
	rgAckPkt[2]  = PROTO_START_OF_FRAME_T;
	rgAckPkt[3]  = PROTO_PACKET_VERSION;
	// dest and source are inverted with respect to the msg received
	rgAckPkt[6]  = (unsigned char)(m_HeaderStruct.sourceAddress >> 24);
	rgAckPkt[7]  = (unsigned char)(m_HeaderStruct.sourceAddress >> 16);
	rgAckPkt[8]  = (unsigned char)(m_HeaderStruct.sourceAddress >> 8);
	rgAckPkt[9]  = (unsigned char)(m_HeaderStruct.sourceAddress);
	rgAckPkt[10] = (unsigned char)(m_HeaderStruct.destAddress >> 24);
	rgAckPkt[11] = (unsigned char)(m_HeaderStruct.destAddress >> 16);
	rgAckPkt[12] = (unsigned char)(m_HeaderStruct.destAddress >> 8);
	rgAckPkt[13] = (unsigned char)(m_HeaderStruct.destAddress);
	rgAckPkt[14] = PROTO_CONNECTION_ID;
	rgAckPkt[16] = PROTO_CONNECTION_ACK;
	rgAckPkt[17] = m_ProtocolStruct.transactionNumber >> 8;
	rgAckPkt[18] = (unsigned char)m_ProtocolStruct.transactionNumber;

	int liSize = 0;
	if ( m_ProtocolStruct.uFlag.fragmentIncluded == 0 )
	{
		rgAckPkt[4] = 0x00;
		rgAckPkt[5] = 0x0F;
		rgAckPkt[15] = 0x01;
		crc_t usiCrc = createCRC((rgAckPkt+MSG_FIRST_PART_HEADER), rgAckPkt[5]);
		rgAckPkt[19] = usiCrc >> MSG_CRC_CHAR_BITS;
		rgAckPkt[20] = usiCrc & 0xFF;
		liSize = 21;
	}
	else
	{
		rgAckPkt[4] = 0x00;
		rgAckPkt[5] = 0x15;
		uFlag tmp;
		tmp.pkt = m_ProtocolStruct.uFlag;
		tmp.pkt.ACK = 0x01;
		rgAckPkt[15] = tmp.data; // because we are sending an ack -> ack bit = 1;
		rgAckPkt[19] = 0x00;
		rgAckPkt[20] = 0x00;
		rgAckPkt[21] = (unsigned char)(m_ProtocolStruct.fragmentOffsetSize >> 24);
		rgAckPkt[22] = (unsigned char)(m_ProtocolStruct.fragmentOffsetSize >> 16);
		rgAckPkt[23] = (unsigned char)(m_ProtocolStruct.fragmentOffsetSize >> 8);
		rgAckPkt[24] = (unsigned char)(m_ProtocolStruct.fragmentOffsetSize);
		crc_t usiCrc = createCRC((rgAckPkt+MSG_FIRST_PART_HEADER), rgAckPkt[5]);
		rgAckPkt[25] = usiCrc >> MSG_CRC_CHAR_BITS;
		rgAckPkt[26] = usiCrc & 0xFF;
		liSize = 27;
	}

	if ( m_pUSBIfr->writeBuffer(rgAckPkt, liSize, m_liLastError) == -1 )
	{
		m_pLogger->log(LOG_ERR, "CR8062Protocol::sendAckCDC: error transmitting the packet via USB, %s", errno2CamOperationResult(m_liLastError).c_str());
		return false;
	}

	m_pLogger->log(LOG_DEBUG, "CR8062Protocol::sendAckCDC: Ack msg: Flag: %.2X TransNum: %.2X %.2X Fragm: %.2X %.2X %.2X %.2X",
				   rgAckPkt[15], rgAckPkt[17], rgAckPkt[18], rgAckPkt[21], rgAckPkt[22], rgAckPkt[23], rgAckPkt[24]);
	return true;
}

bool CR8062Protocol::sendAckHID()
{
	unsigned char rgAckPkt[65]{}; // actually the size will always be 21 or 27 B
	rgAckPkt[0]  = PROTO_HID_REPORT_NUMBER;
	rgAckPkt[2]  = PROTO_SOH;
	rgAckPkt[3]  = PROTO_START_OF_FRAME_C;
	rgAckPkt[4]  = PROTO_START_OF_FRAME_T;
	rgAckPkt[5]  = PROTO_PACKET_VERSION;
	// dest and source are inverted with respect to the msg received
	rgAckPkt[8]  = (unsigned char)(m_HeaderStruct.sourceAddress >> 24);
	rgAckPkt[9]  = (unsigned char)(m_HeaderStruct.sourceAddress >> 16);
	rgAckPkt[10]  = (unsigned char)(m_HeaderStruct.sourceAddress >> 8);
	rgAckPkt[11]  = (unsigned char)(m_HeaderStruct.sourceAddress);
	rgAckPkt[12] = (unsigned char)(m_HeaderStruct.destAddress >> 24);
	rgAckPkt[13] = (unsigned char)(m_HeaderStruct.destAddress >> 16);
	rgAckPkt[14] = (unsigned char)(m_HeaderStruct.destAddress >> 8);
	rgAckPkt[15] = (unsigned char)(m_HeaderStruct.destAddress);
	rgAckPkt[16] = PROTO_CONNECTION_ID;
	rgAckPkt[18] = PROTO_CONNECTION_ACK;
	rgAckPkt[19] = m_ProtocolStruct.transactionNumber >> 8;
	rgAckPkt[20] = (unsigned char)m_ProtocolStruct.transactionNumber;

	if ( m_ProtocolStruct.uFlag.fragmentIncluded == 0 )
	{
		rgAckPkt[6] = 0x00;
		rgAckPkt[7] = 0x0F;
		rgAckPkt[17] = 0x01;
		crc_t usiCrc = createCRC((rgAckPkt+2+MSG_FIRST_PART_HEADER), rgAckPkt[7]);
		rgAckPkt[21] = usiCrc >> MSG_CRC_CHAR_BITS;
		rgAckPkt[22] = usiCrc & 0xFF;
		rgAckPkt[1] = 23;
	}
	else
	{
		rgAckPkt[6] = 0x00;
		rgAckPkt[7] = 0x15;
		uFlag tmp;
		tmp.pkt = m_ProtocolStruct.uFlag;
		tmp.pkt.ACK = 0x01;
		rgAckPkt[17] = tmp.data; // because we are sending an ack -> ack bit = 1;
		rgAckPkt[21] = 0x00;
		rgAckPkt[22] = 0x00;
		rgAckPkt[23] = (unsigned char)(m_ProtocolStruct.fragmentOffsetSize >> 24);
		rgAckPkt[24] = (unsigned char)(m_ProtocolStruct.fragmentOffsetSize >> 16);
		rgAckPkt[25] = (unsigned char)(m_ProtocolStruct.fragmentOffsetSize >> 8);
		rgAckPkt[26] = (unsigned char)(m_ProtocolStruct.fragmentOffsetSize);
		crc_t usiCrc = createCRC((rgAckPkt+2+MSG_FIRST_PART_HEADER), rgAckPkt[7]);
		rgAckPkt[27] = usiCrc >> MSG_CRC_CHAR_BITS;
		rgAckPkt[28] = usiCrc & 0xFF;
		rgAckPkt[1] = 29;
	}

	if ( m_pHIDIfr->write(rgAckPkt) == -1 )
	{
		m_pLogger->log(LOG_ERR, "CR8062Protocol::sendAckHID: error transmitting the packet via USB, %s", errno2CamOperationResult(m_liLastError).c_str());
		return false;
	}

	m_pLogger->log(LOG_DEBUG_PARANOIC, "CR8062Protocol::sendAckHID: Ack msg: Flag: %.2X TransNum: %.2X %.2X Fragm: %.2X %.2X %.2X %.2X",
				   rgAckPkt[15], rgAckPkt[17], rgAckPkt[20], rgAckPkt[23], rgAckPkt[24], rgAckPkt[25], rgAckPkt[26]);

	return true;
}

bool CR8062Protocol::parseMsgForFWUpgrade()
{
	if ( !m_bConfigOk )				return false;
	if ( m_PayloadStruct.pktPayload.empty() )	return -1;
	if ( m_liByteReceived <= 0 )	return false;

	// If the payload is full, some issues have occurred
	int liPayloadMaxSize = m_PayloadStruct.pktPayload.size();
	uint8_t* pcPosZero;						// needed to calculate payload length
	uint8_t* pcZero;						// needed to exit from while cycle
	uint8_t* pcRxBuf;
	pcRxBuf = &m_rgcBuff[0];
	uint8_t ucSwitchProt = PROTO_START;

	pcRxBuf = ( getConfig() == eCDCDevice ) ? pcRxBuf : pcRxBuf+1;
	pcZero = pcRxBuf;

	int32_t liAddresDiff = pcRxBuf-pcZero;

	while ( liAddresDiff < m_liByteReceived )
	{
		switch(ucSwitchProt)
		{
			case PROTO_START:
				pcPosZero = pcRxBuf;

				if ( ((*pcRxBuf == 0x01) && (*(pcRxBuf+1) == 0x043)) && (*(pcRxBuf+2) == 0x054))
				{
					parseHeader(pcRxBuf);
					pcRxBuf += 14;
					ucSwitchProt = PROTO_TYPE_SELECT;
				}
				else //if ( ((*pcRxBuf == 0x00) && (*(pcRxBuf+1) == 0x00)) && (*(pcRxBuf+2) == 0x00))
				{
					pcRxBuf += 64; // to be sure to exit the loop
				}
			break;

			case PROTO_TYPE_SELECT:
				m_HeaderStruct.protocolType = *(pcRxBuf);
				++pcRxBuf;
				ucSwitchProt = PROTO_CONNECTION;
			break;

			case PROTO_CONNECTION:
				memcpy(&m_ProtocolStruct.uFlag, pcRxBuf, sizeof(uint8_t));
				m_ProtocolStruct.payloadProtocol = *(pcRxBuf+1);
				m_ProtocolStruct.ACKnumber = (*(pcRxBuf+2) << 8) + *(pcRxBuf+3);
				pcRxBuf += 4;

				if ( m_ProtocolStruct.payloadProtocol == PROTO_CONNECTION_ACK )
					ucSwitchProt = PROTO_ACK;
				else if ( m_ProtocolStruct.payloadProtocol == PROTO_CMD )
					ucSwitchProt = PROTO_COMMAND;
			break;

			case PROTO_ACK:
				memcpy(&m_PayloadStruct.uRequestID, pcRxBuf, sizeof(uint16_t));
				ucSwitchProt = PROTO_CRC;
			break;

			case PROTO_COMMAND:
				m_ProtocolStruct.transactionNumber  = (*(pcRxBuf) << 8) + *(pcRxBuf+1);
				pcRxBuf += 2;
				if ( m_ProtocolStruct.uFlag.fragmentIncluded )
				{
					m_ProtocolStruct.fragmentOffsetSize = (*pcRxBuf << 24) + (*(pcRxBuf+1) << 16) + (*(pcRxBuf+2) << 8) + *(pcRxBuf+3);
					pcRxBuf += 4;
				}
				ucSwitchProt = PROTO_PAYLOAD;
			break;

			case PROTO_PAYLOAD:
				int32_t liPayloadByteSize;
				liPayloadByteSize = m_HeaderStruct.packetLength - (pcRxBuf - pcPosZero) + MSG_FIRST_PART_HEADER - sizeof(crc_t) - sizeof(requestID);
				m_PayloadStruct.uRequestID.ID = (*pcRxBuf << 8) + *(pcRxBuf+1);
				m_PayloadStruct.uRequestID.originatorBit = ((*pcRxBuf << 8) + *(pcRxBuf+1)) >> 15;

				// If the payload is full, some issues have occurred
				if ( liPayloadMaxSize < m_liByteCopied+liPayloadByteSize )		return -1;

				copy((pcRxBuf+2), (pcRxBuf+2+liPayloadByteSize), m_PayloadStruct.pktPayload.begin()+m_liByteCopied);
				pcRxBuf += (liPayloadByteSize + sizeof(requestID));
				ucSwitchProt = PROTO_CRC;
			break;

			case PROTO_CRC:
				m_usiCrc = (*pcRxBuf << 8) + *(pcRxBuf+1);
				pcRxBuf += sizeof(crc_t);
				if ( !checkCRCforPacketLoss(pcRxBuf) ) return false;

				liAddresDiff = pcRxBuf-pcZero;
				ucSwitchProt = PROTO_START;
			break;

			default:
				ucSwitchProt = PROTO_START;
			break;
		}

		liAddresDiff = pcRxBuf-pcZero;
	}

	return true;
}

int CR8062Protocol::readAndParseMsg(enumRequestType eType, int& liPayloadSize,int liTimeOutMs)
{
	if ( ! m_bConfigOk )						return -ERR_CR8062_IN_PARAMETERS;
	if ( m_PayloadStruct.pktPayload.empty() )	return -ERR_CR8062_IN_PARAMETERS;

	// Clean all
	memset(&m_HeaderStruct, 0x00, sizeof(m_HeaderStruct));
	memset(&m_ProtocolStruct, 0x00, sizeof(m_ProtocolStruct));
	memset(&m_rgcBuff, 0x00, sizeof(m_rgcBuff));
	memset(&m_PayloadStruct.uRequestID, 0x00, sizeof(m_PayloadStruct.uRequestID));
	fill(m_PayloadStruct.pktPayload.begin(), m_PayloadStruct.pktPayload.end(), 0x00);

	m_liByteCopied = 0;
	liPayloadSize = 0;

	uint8_t* pcPosZero;						// needed to calculate payload length
	uint8_t* pcZero;						// needed to exit from while cycle
	uint8_t* pcRxBuf;
	pcRxBuf = &m_rgcBuff[0];
	uint8_t ucSwitchProt = PROTO_START;

	pcRxBuf = ( getConfig() == eCDCDevice ) ? pcRxBuf : pcRxBuf+1;
	pcZero = pcRxBuf;

	int32_t liAddresDiff = pcRxBuf-pcZero;

	bool bContinueReading = true;
	int liPayloadFirstMsgSize = 0;
	int liTotBytesRead = 0; // needed to avoid cases when payload is not given
	int liBytesToBeCopiedNextTime = 0;
	int liHalfPayload = -1;
	int liPayloadMaxSize = m_PayloadStruct.pktPayload.size();

	vector<unsigned char> tmp;

	// TODO remove all the cout in this function

	while ( bContinueReading )
	{
		pcRxBuf = pcZero;
		liAddresDiff = pcRxBuf-pcZero;

		int liBytesRead = readMsgReceived(liTimeOutMs);
		if ( liBytesRead <= 0)
		{
			if ( eType == eSet && liTotBytesRead <= MSG_ACK_LENGTH )
			{
				return ERR_NONE;
			}
			else if ( m_ProtocolStruct.payloadProtocol == PROTO_IMAGE_TRANSFER )
			{
				return m_liByteCopied; // check if ever enters here or not
			}
			else
			{
				cout << "Timeout is: " << liTimeOutMs << endl;
				return -ERR_USB_PERIPH_TIMEOUT;
			}
		}
		liTotBytesRead += liBytesRead;

		while ( liAddresDiff < liBytesRead )
		{
			switch(ucSwitchProt)
			{
				case PROTO_START:
					pcPosZero = pcRxBuf;

					if ( ((*pcRxBuf == 0x01) && (*(pcRxBuf+1) == 0x043)) && (*(pcRxBuf+2) == 0x054))
					{
						tmp.clear();
						tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+14);

						parseHeader(pcRxBuf);
						pcRxBuf += 14;
						ucSwitchProt = PROTO_TYPE_SELECT;
					}
					else
					{
						if ( m_ProtocolStruct.payloadProtocol == PROTO_IMAGE_TRANSFER )
							ucSwitchProt = PROTO_STORE_IMAGE;
						// TODO remove part on the right when fw of the camera is fixed
						else if ( m_ProtocolStruct.payloadProtocol == PROTO_DECODE_DATA || m_ProtocolStruct.payloadProtocol == PROTO_CMD )
							ucSwitchProt = PROTO_STORE_CODE;
						else
							return -ERR_UNDEFINED;
					}
				break;

				case PROTO_TYPE_SELECT:
					tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+1);

					m_HeaderStruct.protocolType = *(pcRxBuf);
					++pcRxBuf;
					ucSwitchProt = PROTO_CONNECTION;
				break;

				case PROTO_CONNECTION:
					tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+4);

					memcpy(&m_ProtocolStruct.uFlag, pcRxBuf, sizeof(uint8_t));
					m_ProtocolStruct.payloadProtocol = *(pcRxBuf+1);
					m_ProtocolStruct.ACKnumber = (*(pcRxBuf+2) << 8) + *(pcRxBuf+3);
					pcRxBuf += 4;

					if ( m_ProtocolStruct.payloadProtocol == PROTO_CONNECTION_ACK )
						ucSwitchProt = PROTO_ACK;
					else if ( m_ProtocolStruct.payloadProtocol == PROTO_DECODE_DATA )
						ucSwitchProt = PROTO_DECODE;
					else if ( m_ProtocolStruct.payloadProtocol == PROTO_CMD )
						ucSwitchProt = PROTO_COMMAND;
					else if ( m_ProtocolStruct.payloadProtocol == PROTO_IMAGE_TRANSFER )
						ucSwitchProt = PROTO_IMAGE;
					else if ( m_ProtocolStruct.payloadProtocol == PROTO_LOGGER )
						ucSwitchProt = PROTO_LOG;
				break;

				case PROTO_ACK:
					memcpy(&m_PayloadStruct.uRequestID, pcRxBuf, sizeof(uint16_t));
					ucSwitchProt = PROTO_CRC;
					if ( eType == eAction || eType == eGet )
					{
						bContinueReading = true; // I am expecting always something after ack
					}
				break;

				case PROTO_DECODE:
					tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+2);
					m_ProtocolStruct.transactionNumber  = (*(pcRxBuf) << 8) + *(pcRxBuf+1);
					pcRxBuf += 2;
					ucSwitchProt = PROTO_PAYLOAD;
				break;

				case PROTO_COMMAND:
					tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+2);

					m_ProtocolStruct.transactionNumber  = (*(pcRxBuf) << 8) + *(pcRxBuf+1);
					pcRxBuf += 2;
					if ( m_ProtocolStruct.uFlag.fragmentIncluded )
					{
						tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+4);

						m_ProtocolStruct.fragmentOffsetSize = (*pcRxBuf << 24) + (*(pcRxBuf+1) << 16) + (*(pcRxBuf+2) << 8) + *(pcRxBuf+3);
						pcRxBuf += 4;
					}

					// Msg Ended after the payload and crc check - there is no need of other readings.
					if ( ( eType == eSet ) || (eType == eGet ) )
					{
						 bContinueReading = false;
					}
					ucSwitchProt = PROTO_PAYLOAD;
				break;

				case PROTO_IMAGE:

					if ( liHalfPayload == -1 )
					{
						tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+2);

						m_ProtocolStruct.transactionNumber  = (*(pcRxBuf) << 8) + *(pcRxBuf+1);
						pcRxBuf += 2;
					}

					if ( m_ProtocolStruct.uFlag.fragmentIncluded )
					{
						if ( m_ProtocolStruct.uFlag.firstFragment )
						{
							if ( liHalfPayload == -1 )
							{
								tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+4);

								m_ProtocolStruct.fragmentOffsetSize = (*pcRxBuf << 24) + (*(pcRxBuf+1) << 16) + (*(pcRxBuf+2) << 8) + *(pcRxBuf+3);
								liPayloadSize = m_ProtocolStruct.fragmentOffsetSize;
								pcRxBuf += 4;
							}

							// copy this first part of payload
							int liBytesToBeCopied = m_HeaderStruct.packetLength - MSG_FIXED_PART_LENGTH - sizeof(crc_t);

							// if the payload is not complete
							if ( liHalfPayload == 0 )
							{
								liBytesToBeCopied = liBytesToBeCopiedNextTime;
								liBytesToBeCopiedNextTime = 0;
								liHalfPayload++;
							}
							else if ( liBytesToBeCopied > liBytesRead-(pcRxBuf-pcZero) )
							{
								liBytesToBeCopiedNextTime = liBytesToBeCopied - (liBytesRead-(pcRxBuf-pcZero));
								liBytesToBeCopied = liBytesRead-(pcRxBuf-pcZero);
								liHalfPayload = 0;
							}

							// If the payload is full, some issues have occurred
							if ( liPayloadMaxSize < m_liByteCopied+liBytesToBeCopied )
							{
								cout << "case PROTO_IMAGE ------> liPayloadMaxSize [" << liPayloadMaxSize << "] < m_liByteCopied ["
									 << m_liByteCopied << "] + liPayloadByteSize [" << liBytesToBeCopied << "]";
								return -ERR_USB_PERIPH_COMMUNICATION;
							}

							tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+liBytesToBeCopied);

							copy(pcRxBuf, (pcRxBuf+liBytesToBeCopied), m_PayloadStruct.pktPayload.begin()+m_liByteCopied);
							m_liByteCopied += liBytesToBeCopied;
							pcRxBuf += liBytesToBeCopied;
							liPayloadFirstMsgSize = m_HeaderStruct.packetLength - MSG_FIXED_PART_LENGTH - sizeof(crc_t);

							if ( liHalfPayload == -1 )
							{
								ucSwitchProt = PROTO_CRC;
							}
							else if ( liHalfPayload != 0 )
							{
								liHalfPayload = -1; // set default
								ucSwitchProt = PROTO_CRC;
							}
						}
						else
						{
							tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+4);

							m_ProtocolStruct.fragmentOffsetSize = (*pcRxBuf << 24) + (*(pcRxBuf+1) << 16) + (*(pcRxBuf+2) << 8) + *(pcRxBuf+3);
							pcRxBuf += 4;
							ucSwitchProt = PROTO_STORE_IMAGE;
						}
					}
					else
					{
						liPayloadSize = m_HeaderStruct.packetLength - (MSG_FIXED_PART_LENGTH-(sizeof(crc_t)));
						ucSwitchProt = PROTO_STORE_IMAGE;
					}
				break;

				case PROTO_LOG:
					tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+2);
					m_ProtocolStruct.transactionNumber  = (*(pcRxBuf) << 8) + *(pcRxBuf+1);
					pcRxBuf += 2;
					ucSwitchProt = PROTO_LOG_PAYLOAD;
				break;

				case PROTO_PAYLOAD:
					int32_t liPayloadByteSize;
					liPayloadByteSize = m_HeaderStruct.packetLength - (pcRxBuf - pcPosZero) + MSG_FIRST_PART_HEADER - sizeof(crc_t) - sizeof(requestID);
					m_PayloadStruct.uRequestID.ID = (*pcRxBuf << 8) + *(pcRxBuf+1);
					m_PayloadStruct.uRequestID.originatorBit = ((*pcRxBuf << 8) + *(pcRxBuf+1)) >> 15;

					// If the payload is full, some issues have occurred
					if ( liPayloadMaxSize < m_liByteCopied+liPayloadByteSize )
					{
						cout << "case PROTO_PAYLOAD ------> liPayloadMaxSize [" << liPayloadMaxSize << "] < m_liByteCopied ["
							 << m_liByteCopied << "] + liPayloadByteSize [" << liPayloadByteSize << "]";
						return -ERR_USB_PERIPH_COMMUNICATION;
					}

					tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+2+liPayloadByteSize);

					copy((pcRxBuf+2), (pcRxBuf+2+liPayloadByteSize), m_PayloadStruct.pktPayload.begin()+m_liByteCopied);
					pcRxBuf += (liPayloadByteSize + sizeof(requestID));
					liPayloadSize = liPayloadByteSize;

					if ( m_ProtocolStruct.payloadProtocol == PROTO_DECODE_DATA )		bContinueReading = false;
					ucSwitchProt = PROTO_CRC;
				break;

				case PROTO_LOG_PAYLOAD:
					liPayloadByteSize = m_HeaderStruct.packetLength - (pcRxBuf - pcPosZero) + MSG_FIRST_PART_HEADER - sizeof(crc_t) - sizeof(requestID);
					m_PayloadStruct.uRequestID.ID = (*pcRxBuf << 8) + *(pcRxBuf+1);
					m_PayloadStruct.uRequestID.originatorBit = ((*pcRxBuf << 8) + *(pcRxBuf+1)) >> 15;
					m_PayloadStruct.pktPayload[m_liByteCopied] = '<';
					m_liByteCopied++;

					// If the payload is full, some issues have occurred
					if ( liPayloadMaxSize <= m_liByteCopied+liPayloadByteSize )
					{
						cout << "case PROTO_LOG_PAYLOAD ------> liPayloadMaxSize [" << liPayloadMaxSize << "] < m_liByteCopied ["
							 << m_liByteCopied << "] + liPayloadByteSize [" << liPayloadByteSize << "]";
						return -ERR_USB_PERIPH_COMMUNICATION;
					}

					tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+2+liPayloadByteSize);

					copy((pcRxBuf+2), (pcRxBuf+2+liPayloadByteSize), m_PayloadStruct.pktPayload.begin()+m_liByteCopied);
					m_liByteCopied += liPayloadByteSize;
					m_PayloadStruct.pktPayload[m_liByteCopied] = '>'; // to separate the fields and then parse them correctly
					m_liByteCopied++;
					pcRxBuf += (liPayloadByteSize + sizeof(requestID));
					liPayloadSize += liPayloadByteSize+2;
					ucSwitchProt = PROTO_CRC;
					bContinueReading = false;
				break;

				case PROTO_CRC:

					tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+2);
					m_usiCrc = (*pcRxBuf << 8) + *(pcRxBuf+1);
					pcRxBuf += sizeof(crc_t);
					// TODO remove right part of the end
					if ( !checkCRCforPacketLoss(tmp.data(), tmp.size()) /*&& m_ProtocolStruct.payloadProtocol == PROTO_IMAGE_TRANSFER*/ )
					{
						return -ERR_CR8062_CRC_ERR;
					}

					if ( m_ProtocolStruct.payloadProtocol != PROTO_CONNECTION_ACK )
					{
						sendACK();
					}

					liAddresDiff = pcRxBuf-pcZero;
					ucSwitchProt = PROTO_START;
				break;

				case PROTO_STORE_IMAGE:

					int32_t liByteToBeCopied;

					if ( (m_liByteCopied - liPayloadFirstMsgSize + liBytesRead) >= (MSG_PAYLOAD_MAX_LENGTH * (m_liPktReceived+1)) )
					{
						liByteToBeCopied = MSG_PAYLOAD_MAX_LENGTH * (m_liPktReceived+1) - (m_liByteCopied-liPayloadFirstMsgSize);

						// If the payload is full, some issues have occurred
						if ( liPayloadMaxSize < m_liByteCopied+liByteToBeCopied )
						{
							cout << "case PROTO_STORE_IMAGE_up ------> liPayloadMaxSize [" << liPayloadMaxSize << "] < m_liByteCopied ["
								 << m_liByteCopied << "] + liPayloadByteSize [" << liPayloadByteSize << "]";
							return -ERR_USB_PERIPH_COMMUNICATION;
						}

						tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+liByteToBeCopied);

						copy(pcRxBuf, (pcRxBuf+liByteToBeCopied), m_PayloadStruct.pktPayload.begin()+m_liByteCopied);
						m_liByteCopied += (liByteToBeCopied);
						++m_liPktReceived;

						pcRxBuf += liByteToBeCopied;
						ucSwitchProt = PROTO_CRC;
						break;
					}

					if (liPayloadSize < (liBytesRead - liAddresDiff)) // is it needed? TODO check if it enters here at least once
						liByteToBeCopied = liPayloadSize;
					else if ((liPayloadSize - m_liByteCopied) < liBytesRead)
						liByteToBeCopied = liPayloadSize - m_liByteCopied;
					else
						liByteToBeCopied = liBytesRead - liAddresDiff;

					// If the payload is full, some issues have occurred
					if ( liPayloadMaxSize < m_liByteCopied+liByteToBeCopied )
					{
						cout << "case PROTO_STORE_IMAGE_down ------> liPayloadMaxSize [" << liPayloadMaxSize << "] < m_liByteCopied ["
							 << m_liByteCopied << "] + liPayloadByteSize [" << liPayloadByteSize << "]";
						return -ERR_USB_PERIPH_COMMUNICATION;
					}

					tmp.insert(tmp.end(), pcRxBuf, pcRxBuf+liByteToBeCopied);

					copy(pcRxBuf, (pcRxBuf+liByteToBeCopied), m_PayloadStruct.pktPayload.begin()+m_liByteCopied);
					m_liByteCopied += liByteToBeCopied;
					// Actually the CRC is present only in the last packet of the image, but is not important, the purpose is to go out of the cycle.
					pcRxBuf += (liByteToBeCopied);

					if ( uint(m_liByteCopied) >= liPayloadSize-sizeof(crc_t) )
					{
						bContinueReading = false;
						ucSwitchProt = PROTO_CRC;
					}
				break;

				case PROTO_STORE_CODE:
				{
//					TODO: delete or not? it enters here only with the old fw version
					liByteToBeCopied = liBytesRead - liAddresDiff;
					m_PayloadStruct.uRequestID.ID = 0;
					m_PayloadStruct.uRequestID.originatorBit = 0;
					fill(m_PayloadStruct.pktPayload.begin(), m_PayloadStruct.pktPayload.end(), 0x00);

					// I want to store the info about how many bytes is the data matrix/barcode
					liPayloadSize = liByteToBeCopied;

					copy(pcRxBuf, (pcRxBuf+liByteToBeCopied), m_PayloadStruct.pktPayload.begin());

					cout << "CODE READ IS: ";
					for ( int i = 0; i < liByteToBeCopied; ++i )
					{
						cout << m_PayloadStruct.pktPayload[i];
					}
					cout << endl;

					// TODO delete return 0 and decomment other 2 lines
					pcRxBuf += liByteToBeCopied; // to exit the cycle
					bContinueReading = false;
				}
				break;

				default:
					ucSwitchProt = PROTO_START;
				break;
			}

			liAddresDiff = pcRxBuf-pcZero;
		}
	}

	return m_liByteCopied;
}

int32_t CR8062Protocol::receivePktCDC(int32_t liTimeoutMs)
{
	m_liByteReceived = m_pUSBIfr->readBufferWithTimeout(m_rgcBuff, MSG_PACKET_MAX_LENGTH, m_liLastError, liTimeoutMs);
	return m_liByteReceived;
}

int32_t CR8062Protocol::receivePktHID(int32_t liTimeoutMs)
{
	m_liByteReceived = m_pHIDIfr->read(m_rgcBuff, liTimeoutMs);
	return m_liByteReceived;
}

void CR8062Protocol::clearRxBuff()
{
	memset(m_rgcBuff, 0x00, sizeof(m_rgcBuff));
}

void CR8062Protocol::setPayloadSize(int liSize)
{
	if ( liSize <= 0 )	liSize = DEF_CAMERA_PAYLOAD_SIZE;

	m_PayloadStruct.pktPayload.resize(liSize);
}

void CR8062Protocol::setFixedParameters(unsigned char* pucCmdBuf)
{
	*pucCmdBuf		  = PROTO_SOH;
	*(pucCmdBuf + 1)  = PROTO_START_OF_FRAME_C;
	*(pucCmdBuf + 2)  = PROTO_START_OF_FRAME_T;
	*(pucCmdBuf + 3)  = PROTO_PACKET_VERSION;
	*(pucCmdBuf + 6)  = 0x0F;
	*(pucCmdBuf + 7)  = 0xFF;
	*(pucCmdBuf + 8)  = 0xFF;
	*(pucCmdBuf + 9)  = 0xFF;
	*(pucCmdBuf + 10) = 0x40;
	*(pucCmdBuf + 11) = 0x00;
	*(pucCmdBuf + 12) = 0x00;
	*(pucCmdBuf + 13) = 0x00;
	*(pucCmdBuf + 14) = PROTO_CONNECTION_ID; // protocol type - has to be settable?
}

string CR8062Protocol::errno2CamOperationResult(int32_t liErrno)
{
	switch ( liErrno )
	{
		case EIO:
			return "ERR_I/O";
			break;

		case ETIMEDOUT:
			return "ERR_PERIPH_TIMEOUT";
			break;

		case EAGAIN:
			return "ERR_PERIPH_TIMEOUT";
			break;

		case ENOTCONN:
			return "ERR_PERIPH_COMMUNICATION";
			break;

		case ECONNRESET:
			return "ERR_PERIPH_COMMUNICATION";
			break;

		case EINTR:
			return "ERR_WRITE_DUE_TO_SIGNAL";
			break;

		default:
			return "ERR_UNDEFINED";
			break;
	}
}

crc_t CR8062Protocol::createCRC(const unsigned char* pucBuff, size_t usiLength)
{
	crc_t usiCRC = 0;
	usiLength = usiLength - 2; // CRC bytes not to be considered
	while ( usiLength-- )
	{
		usiCRC = ( usiCRC << MSG_CRC_CHAR_BITS ) ^ crcTab[( usiCRC >> MSG_CRC_DIFF_BITS ) ^ *pucBuff++];
	}

	return usiCRC;
}

bool CR8062Protocol::setCR8062Errors(USBErrors* pCameraError)
{
	m_bConfigOk = false;
	if ( pCameraError == nullptr )	return false;

	m_pCamErr = pCameraError;
	m_bConfigOk = true;
	return true;
}

USBErrors* CR8062Protocol::getCamErr()
{
	return m_pCamErr;
}

//bool CR8062Protocol::checkCRCforPacketLoss(const uint8_t* pucRxBuf, uint16_t usiLength)
//{
//	crc_t usiCrcCalculated;
//	if (usiLength == 0)
//		usiCrcCalculated = createCRC((pucRxBuf-m_HeaderStruct.packetLength), m_HeaderStruct.packetLength);
//	else
//		usiCrcCalculated = createCRC((pucRxBuf-usiLength), usiLength);

//	return (usiCrcCalculated == m_usiCrc) ? true : false;
//}

bool CR8062Protocol::checkCRCforPacketLoss(const uint8_t* pucRxBuf, uint32_t usiLength)
{
	crc_t usiCrcCalculated;

	/*! *********************************************************************************
		We enter in the first case when what is passed is the first byte of the CRC.
		When is passed the first element of the data, than we have to go in the second
		case (useful when the msg is sent in more than one pkt and is reconstructed).
	************************************************************************************/
	if ( usiLength == 0 )
	{
		usiCrcCalculated = createCRC((pucRxBuf-m_HeaderStruct.packetLength), m_HeaderStruct.packetLength);
	}
	else
	{
		int foo = usiLength - m_HeaderStruct.packetLength;
		usiCrcCalculated = createCRC((pucRxBuf+foo), m_HeaderStruct.packetLength);
	}

	return (usiCrcCalculated == m_usiCrc) ? true : false;
}

void CR8062Protocol::parseHeader(const unsigned char* pucRxBuf)
{
	m_HeaderStruct.startFrame    = (*pucRxBuf << 16)+ (*(pucRxBuf+1) << 8) + *(pucRxBuf+2);
	m_HeaderStruct.packetVersion =  *(pucRxBuf+3);
	m_HeaderStruct.packetLength  = (*(pucRxBuf+4) << 8) + *(pucRxBuf+5);
	m_HeaderStruct.destAddress   = (*(pucRxBuf+6) << 24) + (*(pucRxBuf+7) << 16) + (*(pucRxBuf+8) << 8) + *(pucRxBuf+9);
	m_HeaderStruct.sourceAddress = (*(pucRxBuf+10) << 24) + (*(pucRxBuf+11) << 16)+ (*(pucRxBuf+12) << 8) + *(pucRxBuf+13);
}
