/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Reader.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CR8062Reader class.
 @details

 ****************************************************************************
*/

#include <algorithm>
#include <iostream>
#include <fstream>
#include <chrono>

#include "SprDetection/SprDetectionLink.h"
#include "CR8062Reader.h"
#include "MainExecutor.h"
#include "SectionBoardSPR.h"
#include "SectionBoardTray.h"

#define DEFAULT_READING_TIMEOUT	1000
#define RETRY_DECODING_TIME		200
#define READ_EXTRA_TIME_MSEC	150

#define DEFAULT_LOCATION_READER		"/home/root/CAMERA/reader.test"
#define DEFAULT_LOCATION_RESULTS	"/home/root/CAMERA/results.test"
#define TIME		"Time"
#define SEQUENCE	"Sequence"
#define RETRY		'R'
#define MOVE_SPR	'S'
#define MOVE_CW		'C'
#define MOVE_TRAY	'T'
#define BYPASS		'B'
#define BYPASS_GAIN	"Bg"
#define BYPASS_EXP	"Be"
#define BYPASS_ILL	"Bi"


CR8062Reader::CR8062Reader(int liPayloadSize)
{
	setPayloadSize(liPayloadSize);
	m_nDecodeTimeOutMsec = -1;
}

CR8062Reader::~CR8062Reader()
{

}

bool CR8062Reader::init(USBInterface* pUSBIfr, Log* pLogger)
{
    bool bRes = CR8062Base::init(pUSBIfr, pLogger);
    if ( ! bRes )
    {
		m_pLogger->log(LOG_INFO, "CR8062Reader::init: unable to init reader");
    }

    int liDecodingTime = -1;
    int8_t liRes = getDecodingTimeMsec(liDecodingTime);
    if ( liRes )
    {
		m_pLogger->log(LOG_INFO, "CR8062Reader::init: unable to get decoding timeout");
    }

    if ( liDecodingTime <= 0 )
    {
		liRes = setDecodingTimeMsec(DEFAULT_READING_TIMEOUT);
		if ( liRes )
		{
			m_pLogger->log(LOG_INFO, "CR8062Reader::init: unable to set decoding timeout");
			return false;
		}
    }

    return true;
}

int CR8062Reader::getLinearBarcode(string& strOutLinBarcode,  bool bIsBmx)
{
	if ( m_nDecodeTimeOutMsec <= 0 )	return -ERR_CR8062_IN_PARAMETERS;

    strOutLinBarcode.clear();

    // read linear barcode
	bool bRes = sendCmd("RDCMXEV7");
	if ( ! bRes )	return -1;

    int liLinearBarcodeSize = 0;
	int liDecodingTime = m_nDecodeTimeOutMsec + READ_EXTRA_TIME_MSEC;
	int liRes = readAndParseMsg(eAction, liLinearBarcodeSize, liDecodingTime);
    if ( liRes == -ERR_USB_PERIPH_TIMEOUT || liLinearBarcodeSize == 0)
    {
		int liErrCode = ERR_CR8062_READING_BARCODE;
//		m_pCamErr->decodeNotifyEvent(liErrCode);	TODO: decomment?
		return liErrCode;
    }

    vector<uint8_t>::iterator start = m_PayloadStruct.pktPayload.begin();
    vector<uint8_t>::iterator end = m_PayloadStruct.pktPayload.begin();
    advance(end, liLinearBarcodeSize);

    string strDefault("Response Val");
    if ( search(start, end, strDefault.begin(), strDefault.end()) != end )
    {
		m_pLogger->log(LOG_ERR, "CR8062Reader::getLinearBarcode: unable to read linear barcode");
		return ERR_CR8062_READING_BARCODE;
    }

    strOutLinBarcode.assign(start, end);
    if ( bIsBmx )
    {
		bool bRes = checksumControl(strOutLinBarcode);
		if ( ! bRes )
		{
			m_pLogger->log(LOG_ERR, "CR8062Reader::getLinearBarcode: linear barcode read [%s], but checksum control failed,"
															"data lost.", strOutLinBarcode.c_str());
			return ERR_CR8062_READING_BARCODE;
		}
    }

    m_pLogger->log(LOG_INFO, "CR8062Reader::getLinearBarcode: linear barcode read: %s.", strOutLinBarcode.c_str());
    return ERR_NONE;
}

int CR8062Reader::getDataMatrix(string& strOutDataMatrix)
{
	if ( m_nDecodeTimeOutMsec <= 0 )	return -ERR_CR8062_IN_PARAMETERS;

    strOutDataMatrix.clear();

    // read linear barcode
	bool bRes = sendCmd("RDCMXEV7");
	if ( ! bRes )	return -1;

    int liDataMatrixSize = 0;
	int liDecodingTime = m_nDecodeTimeOutMsec + READ_EXTRA_TIME_MSEC;
	int liRes = readAndParseMsg(eAction, liDataMatrixSize, liDecodingTime);
	m_pLogger->log(LOG_INFO, "Decoding TimeOut is [%d], Result read is [%d]", liDecodingTime, liRes);
    if ( liRes == -ERR_USB_PERIPH_TIMEOUT || liDataMatrixSize == 0 )
    {
		int liErrCode = ERR_CR8062_READING_DATAMATRIX;
		return liErrCode;
    }

    vector<uint8_t>::iterator start = m_PayloadStruct.pktPayload.begin();
    vector<uint8_t>::iterator end = m_PayloadStruct.pktPayload.begin();
    advance(end, liDataMatrixSize);

    string strDefault("Response Val");
    if ( search(start, end, strDefault.begin(), strDefault.end()) != end )
	{
		m_pLogger->log(LOG_ERR, "CR8062Reader::getDataMatrix: unable to read data matrix");
		return ERR_CR8062_READING_DATAMATRIX;
    }

    strOutDataMatrix.assign(start, end);
    m_pLogger->log(LOG_INFO, "CR8062Reader::getDataMatrix: data matrix read: %s.", strOutDataMatrix.c_str());

    return ERR_NONE;
}

int CR8062Reader::setDecodingTimeMsec(uint32_t uliMaxReadingTimeMsec, uint32_t uliTimeoutMsec)
{
    string strCmd = "CDOPPAT" + to_string(uliMaxReadingTimeMsec);
	if ( ! sendCmd(strCmd) )	return ERR_USB_PERIPH_COMMUNICATION;

    int liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Reader");
	if ( liRes )				return liRes;

    m_nDecodeTimeOutMsec = uliMaxReadingTimeMsec;

    return ERR_NONE;
}

int CR8062Reader::getDecodingTimeMsec(int32_t& uliMaxReadingTimeMsec, uint32_t uliTimeoutMsec)
{
	if ( ! sendCmd("CDOPGAT") )	return ERR_USB_PERIPH_COMMUNICATION;

    int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Reader");
    if ( liRes )				return liRes;
    if ( m_strAnswer.empty() )	return ERR_USB_PERIPH_COMMUNICATION;

    // expected ans: <CD><OP AT="0"/></CD>
    string strReadingVal("");
    for ( char c : m_strAnswer )
    {
		if ( isdigit(c) )
		{
			strReadingVal.push_back(c);
		}
    }

    uliMaxReadingTimeMsec = stoi(strReadingVal);
    m_nDecodeTimeOutMsec = uliMaxReadingTimeMsec;

    return ERR_NONE;
}

int CR8062Reader::setDecoderFOI(uint8_t ucField, uint32_t uliTimeoutMsec)
{
    if ( ucField > FOI_ENTIRE_IMAGE )	return ERR_CR8062_IN_PARAMETERS;

    string strCmd = "CDOPPPF";
    strCmd.append(to_string(ucField));
    sendCmd(strCmd);

    return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Reader");
}

int CR8062Reader::setROI(uint16_t x0, uint16_t y0, uint16_t usiWidth, uint16_t usiHeight, uint32_t uliTimeoutMsec)
{
    /** ****************************************************************************************
	 * ROI can go from 0 to 640.
     * FOI in HDF or WF will determine if is in the right or left part of the picture
     * If the coordinates are set from 0 to 1280, than it is rescaled automatically to 0-640.
     * The reason of this choice is that acting like this, we have the same system of
     * coordinates for CROP, ROI and CW.
     * FOI must be set before calling this function.
     ***************************************************************************************** */

	if ( x0 >= MAX_WIDTH/2 )	x0 -= MAX_WIDTH / 2;

    if ( (x0+usiWidth) >= MAX_WIDTH/2 )		return ERR_CR8062_IN_PARAMETERS;
	if ( (y0+usiHeight) > MAX_HEIGHT )		return ERR_CR8062_IN_PARAMETERS;
    if ( x0 == 0 || y0 == 0 )				return ERR_CR8062_IN_PARAMETERS;

    uint8_t cZeroCounter = 0;
    if ( usiWidth == 0 )	++cZeroCounter;
    if ( usiHeight == 0 )	++cZeroCounter;

    // usiWidth and usiHeight can only be 0 if also x0 and y0 are. In case of 4 zeros then ROI = FOI.
    switch (cZeroCounter)
    {
		case 1: return ERR_CR8062_IN_PARAMETERS;

		case 2:
				if ( x0 == 0 && y0 == 0 ) { break; }
				else return ERR_CR8062_IN_PARAMETERS;

		default: break;
    }

    // get info and send only necessary commands
    sendCmd("CDOPG");
	int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Reader");
    if ( liRes )		return liRes;

    int X0 = -1, Y0 = -1, siWidth = -1, siHeight = -1;
    bool bEnabled = false;
	parseRoiSettings(&bEnabled, &X0, &Y0, &siWidth, &siHeight);

 if ( ! bEnabled )
    {
		// enable ROI
		sendCmd("CDOPPRO1");
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Reader");
		if ( liRes )		return liRes;
		msleep(5);
    }

    string strCmd("");

    if ( X0 != x0 )
    {
		// Set ROI's x0
		strCmd = "CDOPPRL";
		strCmd.append(to_string(x0));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Reader");
		if ( liRes )		return liRes;
		msleep(5);
    }

    if ( Y0 != y0 )
    {
		//Set ROI's y0
		strCmd = "CDOPPRT";
		strCmd.append(to_string(y0));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Reader");
		if ( liRes )		return liRes;
		msleep(5);
    }

    if ( siWidth != usiWidth )
    {
		// Set ROI's width
		strCmd = "CDOPPRW";
		strCmd.append(to_string(usiWidth));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Reader");
		if ( liRes )		return liRes;
		msleep(5);
    }

    if ( siHeight != usiHeight )
    {
		// Set ROI's height
		strCmd = "CDOPPRH";
		strCmd.append(to_string(usiHeight));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Reader");
		if ( liRes )		return liRes;
		msleep(5);
    }

    return ERR_NONE;
}

// to be used only when parsing bath files
void CR8062Reader::setDecodingTimeoutMs(int liDecodeTimeoutMsec)
{
    m_nDecodeTimeOutMsec = liDecodeTimeoutMsec;
}

int CR8062Reader::getDecodingTimeoutMs()
{
	return m_nDecodeTimeOutMsec;
}

int CR8062Reader::_debug_DM_read(string& strDataMatrix, uint8_t ucSection)
{
	if ( ucSection >= SCT_NONE_ID )	return -1;

	m_pLogger->log(LOG_INFO, "CR8062Reader::_debug_DM_read: getting DM 0");
	int liRes = getDataMatrix(strDataMatrix);
	if ( liRes == ERR_NONE )
	{
		return 0;
	}
	msleep(20);

	unordered_map<string, vector<int>> mData;
	liRes = getParameters(mData);
	if ( liRes )
	{
		return -1;
	}
	if ( mData.empty() )
	{
		m_pLogger->log(LOG_INFO, "CR8062Reader::_debug_DM_read: no valid instructions");
		return 0;
	}

	// Start chrono
	auto start = chrono::steady_clock::now();

	if ( mData.find(TIME) != mData.end() )
	{
		int liTime = mData[TIME].front();
		liRes = setDecodingTimeMsec(liTime);
		if ( liRes )
		{
			m_pLogger->log(LOG_ERR, "CR8062Reader::_debug_DM_read: unable to set %d as decoding time", liTime);
			return liRes;
		}
	}

	// Rebuild sequence from data
	string strSequence("");
	if ( mData.find(SEQUENCE) != mData.end() )
	{
		for ( auto foo : mData[SEQUENCE] )
		{
			strSequence.push_back((char)foo);
		}
	}

	structCounter myCnt = {0, 0, 0, 0, 0};
	vector<logAGCparameters> vAgcLog = {};

	auto it = strSequence.begin();
	bool bRead = false;
	while ( it != strSequence.end() && ! bRead )
	{
		m_pLogger->log(LOG_DEBUG, "CR8062Reader::_debug_DM_read: trying [%c]", *it);
		switch ( *it )
		{
			case RETRY:
				myCnt.retryCnt++;
				m_pLogger->log(LOG_INFO, "CR8062Reader::_debug_DM_read: getting DM retry");
				liRes = getDataMatrix(strDataMatrix);
				msleep(20);
				if ( liRes == ERR_NONE )		bRead = true;
			break;

			case MOVE_SPR:
			{
				if ( ! MainExecutor::getInstance()->isSectionEnabled(ucSection) )	break;

				string s("");
				s.push_back(*it);
				if ( mData.find(s) == mData.end() )		break;

				vector<int> vSPR = mData[s];
				for ( int cycles = 1, sign = 1, i = vSPR.front(); cycles <= vSPR.back(); ++cycles, sign*=-1, i = (sign*cycles*vSPR.front()) )
				{
					// I should have called an action, but this is only for debug purpose, so...
					myCnt.sprCnt++;
					enumSectionCommonMsgStepDir eDir = ( i < 0 ) ? eDecreasePosition : eIncreasePosition;
					uint16_t usSteps = abs(i);
					MainExecutor::getInstance()->m_sectionBoardLink[ucSection].m_pSPR->m_CommonMessage.setNumberRelativeSteps(usSteps, 0);
					MainExecutor::getInstance()->m_sectionBoardLink[ucSection].m_pSPR->m_CommonMessage.startMotor(eDir, 0);

					m_pLogger->log(LOG_INFO, "CR8062Reader::_debug_DM_read: getting DM spr");
					liRes = getDataMatrix(strDataMatrix);
					msleep(30);
					if ( liRes == ERR_NONE )
					{
						bRead = true;
						break;
					}
				}

				// Take it back
				string strCoded = sectionBoardLinkSingleton(ucSection)->m_pSPR->getCodedFromLabel("Data_matrix");
				if ( ! strCoded.empty() )
				{
					char ubPosCode = *strCoded.c_str();
					sectionBoardLinkSingleton(ucSection)->m_pSPR->m_CommonMessage.moveToCodedPosition(ubPosCode, 0);
				}
				msleep(5);
			}
			break;

			case MOVE_TRAY:
			{
				if ( ! MainExecutor::getInstance()->isSectionEnabled(ucSection) )	break;

				string s("");
				s.push_back(*it);
				if ( mData.find(s) == mData.end() )		break;

				vector<int> vTray = mData[s];
				for ( uint i = 0; i < vTray.size(); ++i )
				{
					// I should have called an action, but this is only for debug purpose, so...
					myCnt.trayCnt++;
					enumSectionCommonMsgStepDir eDir = ( vTray[i] < 0 ) ? eDecreasePosition : eIncreasePosition;
					uint16_t usSteps = abs(vTray[i]);
					MainExecutor::getInstance()->m_sectionBoardLink[ucSection].m_pTray->m_CommonMessage.setNumberRelativeSteps(usSteps, 0);
					MainExecutor::getInstance()->m_sectionBoardLink[ucSection].m_pTray->m_CommonMessage.startMotor(eDir, 0);

					m_pLogger->log(LOG_INFO, "CR8062Reader::_debug_DM_read: getting DM tray");
					liRes = getDataMatrix(strDataMatrix);
					msleep(30);
					if ( liRes == ERR_NONE )
					{
						bRead = true;
						break;
					}
				}

				// Take it back
				string strCoded = sectionBoardLinkSingleton(ucSection)->m_pTray->getCodedFromLabel("Strip_BC");
				if ( ! strCoded.empty() )
				{
					char ubPosCode = *strCoded.c_str();
					sectionBoardLinkSingleton(ucSection)->m_pSPR->m_CommonMessage.moveToCodedPosition(ubPosCode, 0);
				}
				msleep(5);
			}
			break;

			case MOVE_CW:
			{
				string s("");
				s.push_back(*it);
				if ( mData.find(s) == mData.end() )		break;

				vector<int> vCW = mData[s];

				int X0 = -1, Y0 = -1, siWidth = -1, siHeight = -1;
				bool bEnabled = false;

				int liRes =  getContrastWindow(&X0, &Y0, &siWidth, &siHeight, &bEnabled);
				if ( liRes != ERR_NONE )
				{
					m_pLogger->log(LOG_ERR, "CR8062Reader::_debug_DM_read: unable to get contrast window");
					return liRes;
				}
				msleep(5);

				int sign = -1;
				int newY0 = Y0;
				for ( int cycles = 1; cycles <= vCW.back(); ++cycles )
				{
					sign *= -1;
					myCnt.cwCnt++;
					newY0 += ( sign * cycles * vCW.front() );
					m_pLogger->log(LOG_INFO, ".................Setting y0 CW: %d", newY0);

					// Check CW correctness
					if ( newY0-siHeight/2 < -MAX_HEIGHT/2 )		liRes = setY0ContrastWindow(-MAX_HEIGHT/2+siHeight/2+1, eFast);
					else if ( newY0+siHeight/2 > MAX_HEIGHT/2 )	liRes = setY0ContrastWindow(MAX_HEIGHT/2-siHeight/2-1, eFast);
					else										liRes = setY0ContrastWindow(newY0, eFast);

					if ( liRes != ERR_NONE )
					{
						m_pLogger->log(LOG_ERR, "CR8062Reader::_debug_DM_read: unable to set Y0 of CW [%d]", newY0);
						return liRes;
					}
					msleep(5);

					m_pLogger->log(LOG_INFO, "CR8062Reader::_debug_DM_read: getting DM cw");
					liRes = getDataMatrix(strDataMatrix);
					m_pLogger->log(LOG_INFO, ".................getting DM done %s", strDataMatrix.c_str());
					msleep(30);
					if ( liRes == ERR_NONE )
					{
						bRead = true;
						msleep(5);
						break;
					}
				}
				if ( next(it) != strSequence.end() )
				{
					setY0ContrastWindow(Y0, eFast);
					m_pLogger->log(LOG_INFO, "CR8062Reader::_debug_DM_read: set Y0 of CW back to [%d]", Y0);
				}
			}
			break;

			case BYPASS:
			{
				if ( mData.find(BYPASS_GAIN) == mData.end() )		break;
				if ( mData.find(BYPASS_EXP) == mData.end() )		break;
				if ( mData.find(BYPASS_ILL) == mData.end() )		break;

				if ( mData[BYPASS_GAIN].size() < 1 || mData[BYPASS_EXP].size() < 1  ||
					 mData[BYPASS_ILL].size() < 1 )
					break;
				if ( mData[BYPASS_EXP].size() > 1 && mData[BYPASS_ILL].size() > 1 )
					break;

				int liGain = mData[BYPASS_GAIN].front();
				int liMax = max(mData[BYPASS_ILL].size(), mData[BYPASS_EXP].size());
				for ( int i = 0; i != liMax; ++i )
				{
					myCnt.bypassCnt++;
					int liIll = 0, liExp = 0;
					if ( mData[BYPASS_ILL].size() < mData[BYPASS_EXP].size() )
					{
						liIll = mData[BYPASS_ILL].front();
						liExp = mData[BYPASS_EXP].at(i);
					}
					else
					{
						liIll = mData[BYPASS_ILL].at(i);
						liExp = mData[BYPASS_EXP].front();
					}

					liRes = setAGCparam(liIll, liExp, liGain);
					if ( liRes != ERR_NONE )
					{
						m_pLogger->log(LOG_ERR, "CR8062Reader::_debug_DM_read: unable to set AGC params"
												"[Ill=%d Exp=%d Gain=%d]", liIll, liExp, liGain);
						return liRes;
					}
					msleep(5);

					m_pLogger->log(LOG_INFO, "CR8062Reader::_debug_DM_read: getting DM");
					liRes = getDataMatrix(strDataMatrix);
					if ( liRes == ERR_NONE )
					{
						bRead = true;
						break;
					}
					msleep(30);
				}
			}

			default:
			break;
		}
		it++;
	}

	auto end = chrono::steady_clock::now();
	chrono::duration<double> elapsedSeconds = end-start;
	updateResultsFile(strSequence, myCnt, strDataMatrix, elapsedSeconds.count(), vAgcLog);

	return 0;
}

bool CR8062Reader::checksumControl(string& strToBeChecked)
{
    if ( strToBeChecked.empty() )	return false;

    char cCheckSumOverTheStrip = strToBeChecked[strToBeChecked.length()-1];
    uint32_t uliSumValue = 0;

    for (uint32_t i = 0; i < (strToBeChecked.size()-1); ++i)
    {
            if ( strToBeChecked[i] > '9' )
                    uliSumValue += (strToBeChecked[i] - '7' ); // discarded special values
            else
                    uliSumValue += (strToBeChecked[i] - '0');
    }

    uliSumValue %= CHECKSUM_BARCODE_MODULE;

    char cCheckSumCalculated =  checkSumTable[uliSumValue];

    if ( cCheckSumOverTheStrip != cCheckSumCalculated )
            return false;

    return true;
}

void CR8062Reader::clearPayload()
{
    memset(&m_PayloadStruct.uRequestID, 0x00, sizeof(m_PayloadStruct.uRequestID));
    fill(m_PayloadStruct.pktPayload.begin(), m_PayloadStruct.pktPayload.end(), 0x00);
    return;
}

void CR8062Reader::parseRoiSettings(bool* bEnabled, int* x0, int* y0, int* siWidth, int* siHeight)
{
    if ( bEnabled == nullptr )	return;
    if ( x0 == nullptr )		return;
    if ( y0 == nullptr )		return;
    if ( siWidth == nullptr )	return;
    if ( siHeight == nullptr )	return;

    // Expected response <CD><OP PR="1" RO="0" RL="0" RT="0" RW="0" RH="0" LC="1" ZR="0"
    //					 EC="0" DL="0" SP="0" QD="0" PT="0" CI="0" SE="0" AP="115" AT="0"
    //					 SD="0" FQ="0" CE="1" UT="1" MD="0" DI="0" RD="0" AS="0" PI="1"
    //					 VF="0" GB="0" NC="0" N2="0" WN="0" DF="0" DV="0" FO="0" PX="" SX=""
    //					 FC="0" FD="" SM="" GP="" FP="" UD="" IS="" IO="" SR="0" PF="2"
    //					 LA="0" XX=""/></CD>

    string strVal;
    string strWhat("<RO=\"");
    size_t pos = m_strAnswer.find(strWhat);
    if ( pos != string::npos )
    {
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		*bEnabled = ( ! strVal.compare("1") ) ? true : false;
    }

    strWhat.assign("RL=\"");
    pos = m_strAnswer.find(strWhat);
    if ( pos != string::npos )
    {
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, x0);
    }

    strWhat.assign("RT=\"");
    pos = m_strAnswer.find(strWhat);
    if ( pos != string::npos )
    {
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, y0);
    }

    strWhat.assign("RW=\"");
    pos = m_strAnswer.find(strWhat);
    if ( pos != string::npos )
    {
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, siWidth);
    }

    strWhat.assign("RH=\"");
    pos = m_strAnswer.find(strWhat);
    if ( pos != string::npos )
    {
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, siHeight);
    }

    return;
}

int CR8062Reader::recoverStrategy(string& strItem, eTarget eDevice)
{
    strItem.clear();

    int liOldDecodingTimeOut = m_nDecodeTimeOutMsec;
    int liRes = setDecodingTimeMsec(RETRY_DECODING_TIME);
    if ( liRes )	return liRes;

    /******************************************************************************************
     * Recover retry 1: move contrast window to let the reader decode the barcode/data matrix
     * with different AGC settings (MONO)
     *****************************************************************************************/
    int liLoopsNum = 4;
    int liStepSize = 3;

    int X0 = -1, Y0 = -1, siWidth = -1, siHeight = -1;
    bool bEnabled = false;
    liRes =  getContrastWindow(&X0, &Y0, &siWidth, &siHeight, &bEnabled);
    if ( liRes )	return liRes;

    m_pLogger->log(LOG_DEBUG, "CR8062Reader::recoverStrategy: failed in decoding with CW=[%d %d %d %d]. Starting "
                                                      "first step of recover", X0, Y0, siWidth, siHeight);

    for ( int cycles = 1, i = liStepSize; cycles <= liLoopsNum; cycles++, i *= (-cycles) )
    {
            int y0 = Y0 + i;
            m_pLogger->log(LOG_DEBUG, "CR8062Reader::recoverStrategy: attempt [%d] to decode with CW=[%d %d %d %d]...",
                                       cycles-1, X0, y0, siWidth, siHeight);

            liRes =  setContrastWindow(X0, y0, siWidth, siHeight, bEnabled);
            if ( liRes )
            {
                    m_pLogger->log(LOG_ERR, "CR8062Reader::recoverStrategy: unable to set CW");
                    return liRes;
            }

            // Avoid memory overflow of the camera
            clearLogAGCparameters();
            liRes = ( eDevice == eTarget::eBarcode ) ? getLinearBarcode(strItem) : getDataMatrix(strItem);
            if ( liRes == 0 )	return 0;
    }

    /******************************************************************************************
     * Recover retry 2: get the last AGC settings used for decoding and change them in MOBY
     *****************************************************************************************/
    liStepSize = 500;
    vector<logAGCparameters> vParams;
    liRes = getLogAGCparameters(vParams);
    if ( liRes != ERR_NONE || vParams.empty() )
    {
            m_pLogger->log(LOG_ERR, "CR8062Reader::recoverStrategy: empty or disabled AGC LOG");
            return -1;
    }
    int liIllum = vParams.back().liIllumination;
    int liGain = vParams.back().liGain;
    int liExp = vParams.back().liExposure;

    if ( liRes )	return liRes;

    m_pLogger->log(LOG_DEBUG, "CR8062Reader::recoverStrategy: failed in decoding with Ill=[%d], Gain=[%d], Exp=[%d]."
                                                      "Starting second step of recover", liIllum, liGain, liExp);

    for ( int cycles = 1, i = liStepSize; cycles <= liLoopsNum; cycles++, i*=(-cycles) )
    {
            int liIll = liIllum + i;
            m_pLogger->log(LOG_DEBUG, "CR8062Reader::recoverStrategy: attempt [%d] to decode with Ill=[%d], Gain=[%d], Exp=[%d]...",
                                       cycles-1, liIll, liGain, liExp);

            liRes = setAGCparam(liIll, liGain, liExp);
            if ( liRes )
            {
                    m_pLogger->log(LOG_ERR, "CR8062Reader::recoverStrategy: unable to set AGC params");
                    return liRes;
            }

            liRes = ( eDevice == eTarget::eBarcode ) ? getLinearBarcode(strItem) : getDataMatrix(strItem);
            if ( liRes == 0 )	break;
    }

    // Restore original params
    if ( ! setAGCnormalMode() )
    {
            liRes = setDecodingTimeMsec(liOldDecodingTimeOut);
    }

	return liRes;
}

int CR8062Reader::getParameters(unordered_map<string, vector<int>>& mData)
{
	mData.clear();

	ifstream file(DEFAULT_LOCATION_READER);
	if ( ! file.good() )
	{
		m_pLogger->log(LOG_ERR, "CR8062Reader::getParameters: unable to open %s", DEFAULT_LOCATION_READER);
		return -1;
	}

	string strTmp("");
	while ( getline(file, strTmp) )
	{
		// Remove white spaces
		strTmp.erase(remove(strTmp.begin(), strTmp.end(), ' '), strTmp.end());
		if ( strTmp.empty() )		continue;
		if ( strTmp[0] == '/' )		continue;

		// remove comments
		size_t pos = strTmp.find("//");
		if ( pos != string::npos )		strTmp.erase(pos);

		pos = strTmp.find("=");
		if ( pos != string::npos )
		{
			string s = strTmp.substr(0, pos);
			if ( mData.find(s) == mData.end() )
			{
				vector<string> vsData = splitString(strTmp.substr(pos+1), "#");
				vector<int> vData;

				for ( size_t i = 0; i < vsData.size(); ++i )
				{
					if ( vsData[i].empty() )	continue;

					// Sequence is string
					if ( strTmp.find(SEQUENCE) != string::npos )
					{
						vData.push_back(vsData[i].front());
					}
					else
					{
						vData.push_back(stoi(vsData[i]));
					}
				}
				mData[s] = vData;
			}
		}
	}

	return 0;
}

int CR8062Reader::updateResultsFile(string& strSequence, const structCounter& myCnt, string& strResult,
									double dMsec, vector<logAGCparameters>& vAgcLog)
{
	ofstream oFile;
	oFile.open(DEFAULT_LOCATION_RESULTS, ofstream::out | ofstream::app);
	if ( ! oFile.good() )
	{
		m_pLogger->log(LOG_ERR, "CR8062Reader::updateResultsFile: unable to open %s", DEFAULT_LOCATION_RESULTS);
		return -1;
	}

	if ( vAgcLog.empty() )
	{
		vAgcLog.resize(1);
	}

	string strSeparator = ";";
	string strAttempts("[");
	strAttempts += to_string(myCnt.retryCnt);
	strAttempts += " ";
	strAttempts += to_string(myCnt.sprCnt);
	strAttempts += " ";
	strAttempts += to_string(myCnt.trayCnt);
	strAttempts += " ";
	strAttempts += to_string(myCnt.cwCnt);
	strAttempts += " ";
	strAttempts += to_string(myCnt.bypassCnt);
	strAttempts.push_back(']');

	if ( strResult.empty() )	strResult = "______"; // this code means that the dtmx hasn't been decoded.
													  // We still don't know if it is because of the reader
													  // because it was not present.
	if ( strSequence.empty() )	strSequence = "NULL";

	oFile <<  strSequence  << strSeparator << strAttempts << strSeparator << vAgcLog.back().liGain <<
			  strSeparator << vAgcLog.back().liExposure	<< strSeparator << vAgcLog.back().liIllumination <<
			  strSeparator << strResult << strSeparator << dMsec << strSeparator << getFormattedTime(true) << endl;

	oFile.close();

	return 0;
}
