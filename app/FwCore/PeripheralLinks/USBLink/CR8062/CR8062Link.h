/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Link.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CR8062Link class.
 @details

 ****************************************************************************
*/

#ifndef CR8062LINK_H
#define CR8062LINK_H

#include "CR8062Camera.h"
#include "CR8062Reader.h"
#include "CR8062FwUpdateManager.h"
#include "Loggable.h"
#include "CameraConfiguration.h"

class CR8062Link : public Loggable
{
	public:

		/*! ***********************************************************************************************************
		 * @brief CodeCorpCR8062 default constructor. Initialize all the members.
		 * ************************************************************************************************************
		 */
		CR8062Link();

		/*! ***********************************************************************************************************
		 * @brief ~CodeCorpCR8062 virtual destructor of the class. Close Serial interface.
		 * ************************************************************************************************************
		 */
		virtual ~CR8062Link();

		/*! ***********************************************************************************************************
		 * @brief  init initialize CR8062Link.
		 * @param  pLogger pointer to the logger.
		 * @param  pUSBIfr pointer to the USB interface.
		 * @return true in case of success, false otherwise.
		 * ************************************************************************************************************
		 */
		bool init(Log* pLogger, USBInterface* pUSBIfr);

		/*! ***********************************************************************************************************
		 * @brief  updateCR8062FW update camera fw
		 * @param  strFileName name of the file to be updated
         * @param  llength size of file
         * @param  strVersionExpected vector we expect to have (to find out if the update really ended succesfully)
		 * @return true in case of success, false otherwise
		 * ************************************************************************************************************
		 */
		bool updateCR8062FW(const string& strFileName, long llength, const string& strVersionExpected);

		/*! ***********************************************************************************************************
		 * @brief  autodetectCR8062Config detect camera config (CDC, HID keyboard or HID device class)
		 * @return class type
		 * ************************************************************************************************************
		 */
		enumDeviceType autodetectCR8062Config(void);

		/*! ***********************************************************************************************************
		 * @brief  uploadBatchFile upload batch to give the camera a sequence of commands. Of course batch file is used
		 *		   to set all parameters needed in a particular scenario, there can't be get or action cmds.
		 * @param  pcFileName pointer to the filename
		 * @return 0 in case of success, -1 if unable to open batch file, error code otherwise
		 * ************************************************************************************************************
		 */
		int uploadGeneralBatchFile(const char* pcFileName);

		// used when the instrument runs
		int uploadBatchFile(uint8_t ucSlot, eBatchTarget eTarget);

		// used in calibration phase
		int uploadCalibrBatchFile(string& strCodedTarget);

		void clearBatchFlag(void);

		/*! ***********************************************************************************************************
		 * @brief  resetCamera reset all not platform parameters
		 * @param  liTimeoutMs timeout for reading
		 * @return true in case of success, false otherwise
		 * ************************************************************************************************************
		 */
		bool resetCamera(int32_t liTimeoutMs = eGetDefaultTimeOutMsec);

		/*! ***********************************************************************************************************
		 * @brief  bringToLife during pictures loop, camera could disconnect because of the overwork. If the camera
		 *		   reboots, then a new fd has to be found. NOTE: you have to set the camera configurations again
		 *		   (image type, reading timeouts, crops, AGC curves and so on).
		 * @return true in case of resurreciton, false otherwise
		 * ************************************************************************************************************
		 */
		bool bringToLife(void);

		/** ***********************************************************************************************************
		 * @brief  ruotate180degree since the camera is 180° ruotated, than I have to convert bakc the image in the
		 *		   correct direction
		 * @param  prgData picture
		 * @param  uliWidth width of the image
		 * @param  uliHeight height of the image
		 * @return true in case of success, false otherwise
		 * ************************************************************************************************************
		 */
		template<typename T> bool ruotate180degree(T* prgData, uint32_t uliWidth, uint32_t uliHeight)
		{
			if ( prgData == nullptr )	return false;

			T* pImage = new T[uliWidth*uliHeight];
			uint32_t H = uliHeight, W = uliWidth;

			for ( uint32_t y = 0; y < H; y++ )
			{
			   for ( uint32_t x = 0; x < W; x++ )
			   {
				   pImage[(y * W + x)] = prgData[((H - 1 - y) * W + (W - 1 - x))];
			   }
			}

			memcpy(prgData, pImage, (uliWidth*uliHeight)*sizeof(T));
			delete[] pImage;
			return true;
		}

		bool addBMPHeader(const structInfoImage* pImage, vector<unsigned char>& vImage);

		bool readBmp(unsigned char*prgData, string strFileName);

	private:

		/*! ***********************************************************************************************************
		 * @brief findConfig
		 * @param liClassId
		 * @param liSubClassId
		 * @return
		 * ************************************************************************************************************
		 */
		enumDeviceType findConfig(int liClassId, int liSubClassId);

		/*! ***********************************************************************************************************
		 * @brief  switchHID2CDC change camera config from HID to CDC
		 * @param  eType starting camera config
		 * @return 0 in case of success, -1 otherwise
		 * ************************************************************************************************************
		 */
		int switchHID2CDC(enumDeviceType eType);

		/*! ***********************************************************************************************************
		 * @brief switchCDC2HID change camera config from CDC to HID
		 * @param eOldConfig starting camera config
		 * @return 0 in case of success, -1 otherwise
		 * ************************************************************************************************************
		 */
		int switchCDC2HID(enumDeviceType eOldConfig);

		/*! ***********************************************************************************************************
		 * @brief  getVidPid get vendor id and product id of the camera
		 * @return true in case of success, false otherwise
		 * ************************************************************************************************************
		 */
		bool getVidPid(void);

		/*! ***********************************************************************************************************
		 * @brief  reInitAfterFwUpdate initialize again the camera after fw update
		 * @param  strFWVersionUpd new fw version
		 * @param  pUSBIfr pointer to usb ifr
		 * @return true in case of success, false otherwise
		 * ************************************************************************************************************
		 */
		bool reInitAfterFwUpdate(const string& strFWVersionUpd, USBInterface* pUSBIfr);

		/*! ***********************************************************************************************************
		 * @brief  compareWithWildCard compare two string with a char that is wildcard from str1.
		 * @param  str1 string to be compared
		 * @param  str2 string used as default
		 * @param  cWildCardChar char not considered in the comparison
		 * @return 0 in case the strings are equal (not considering the wildcard), false otherwise
		 * ************************************************************************************************************
		 */
		bool compareWithWildCard(const string& str1, const string& str2, char cWildCardChar);


		int enlargeYourCrop(structRect* pReal, structRect* pNew, int liDeltaX = 50, int liDeltaY = 50);
		int enlargeYourCrop_CW(structRect* pReal, structRect* pNew, int liDeltaX, int liDeltaY);
		void swapY(vector<unsigned char>& vImage, int liImageWidth, int liImageHeight, int liTimes = 0);

    public:

        CR8062Camera			m_Camera;
        CR8062Reader			m_Reader;
        CR8062FwUpdateManager*	m_pFwUpdate;

        int		m_nVID;
        int		m_nPID;
        int		m_nInterfClass;
        int		m_nInterfSubClass;
		pair<int, eBatchTarget> m_pLastBatchSent;

    private:

        USBInterface*	m_pUSBIfr;

};

#endif // CR8062LINK_H
