/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Base.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CR8062Base class.
 @details

 ****************************************************************************
*/

#include "CR8062Base.h"

#define MIN_PKT_SIZE	21

CR8062Base::CR8062Base()
{
	m_strAnswer.clear();
}


CR8062Base::~CR8062Base()
{

}

bool CR8062Base::init(USBInterface* pUSBIfr, Log* pLogger)
{
	m_bConfigOk = false;

	if ( pUSBIfr == NULL )	return false;
	if ( pLogger == NULL )	return false;

	m_pUSBIfr = pUSBIfr;
	m_pLogger = pLogger;
	m_bConfigOk = true;

	return true;
}

int CR8062Base::setFullSpeedCommunication(bool bWantFullSpeed, uint32_t uliTimeoutMsec)
{
	// check comm mode and change if is the case
	sendCmd("CMUBGFS");
	int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Base");
	if ( liRes != ERR_NONE )	return liRes;

	// ans: <CM><UB FS="1"/></CM>
	bool bIsFullSpeed = false;
	for ( auto c : m_strAnswer )
	{
		if ( isdigit(c) )
		{
			if ( c == '1' )		bIsFullSpeed = true;
			else				bIsFullSpeed = false;
			break;
		}
	}

	if ( bIsFullSpeed != bWantFullSpeed )
	{
		( bWantFullSpeed ) ? sendCmd("CMUBPFS1") : sendCmd("CMUBPFS0");
		liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
	}
	return liRes;
}

int CR8062Base::savePlatformSettings(string& strCmd, uint32_t uliTimeoutMsec)
{
	if ( strCmd.empty() )	return -1;

	// Get platform settings
	sendCmd("CFG[^PL]");
	int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Base");
	if ( liRes != ERR_NONE )	return liRes;

	size_t pos = m_strAnswer.find(strCmd);
	if ( pos == string::npos )
	{
		// Param not saved yet
		string strPlatfSet = "RDCMXPL[" + strCmd + "]";
		sendCmd(strPlatfSet);
		liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
	}
	return liRes;
}

int CR8062Base::setContrastWindow(int x0, int y0, int usiWidth, int usiHeight, uint32_t uliTimeoutMsec)
{
	/** ****************************************************************************************
	 * CW can go from 0 to 640.
	 * FOI in HDF or WF will determine if it is in the right or left part of the picture
	 * If the coordinates are set from 0 to 1280, than it is rescaled automatically to 0-640.
	 * The reason of this choice is that acting like this, we have the same system of
	 * coordinates for CROP, ROI and CW.
	 * FOI must be set before calling this function.
	 ***************************************************************************************** */

	if ( x0 >= MAX_WIDTH/2 )	x0 -= MAX_WIDTH / 2;
	// X0 and Y0 have their origin in (320,480), so the inputs have to be rescaled
	x0 -= ( MAX_WIDTH/4  - usiWidth/2 );
	y0 -= ( MAX_HEIGHT/2 - usiHeight/2 );

	if ( x0 < (-MAX_WIDTH/4  + usiWidth/2 ) )		return ERR_CR8062_IN_PARAMETERS;
	if ( y0 < (-MAX_HEIGHT/2 + usiHeight/2) )		return ERR_CR8062_IN_PARAMETERS;
	if ( x0 > ( MAX_WIDTH/4  - usiWidth/2 ) )		return ERR_CR8062_IN_PARAMETERS;
	if ( y0 > ( MAX_HEIGHT/2 - usiHeight/2) )		return ERR_CR8062_IN_PARAMETERS;

	int32_t liRetValue = setAGCnormalMode(eFast);
	if ( liRetValue != ERR_NONE )	return liRetValue;


	m_pLogger->log(LOG_INFO, ".................Getting CW params");
	int X0 = -1, Y0 = -1, siWidth = -1, siHeight = -1;
	bool bEnabled = false;
	int liRes =  getContrastWindow(&X0, &Y0, &siWidth, &siHeight, &bEnabled);
	if ( liRes )
	{
		m_pLogger->log(LOG_ERR, "CR8062Base::setContrastWindow: unable to get CW");
		return liRes;
	}

	m_pLogger->log(LOG_INFO, ".................Setting CW params");

	if ( ! bEnabled )
	{
		// enable CW
		sendCmd("AGCRPES1");
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Base");
		if ( liRes )	return liRes;
	}

	string strCmd("");

	if ( siWidth != usiWidth )
	{
		// set contrast window's width
		strCmd = "AGCRPCX";
		strCmd.append(to_string(usiWidth));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Base");
		if ( liRes )	return liRes;
	}

	if ( siHeight != usiHeight )
	{
		// set contrast window's height
		strCmd = "AGCRPCY";
		strCmd.append(to_string(usiHeight));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Base");
		if ( liRes )	return liRes;
	}

	if ( X0 != x0 )
	{
		// set contrast window's x_center
		strCmd = "AGCRPPX";
		strCmd.append(to_string(x0));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Base");
		if ( liRes )	return liRes;
	}

	if ( Y0 != y0 )
	{
		// set contrast window's y_center
		strCmd = "AGCRPPY";
		strCmd.append(to_string(y0));
		sendCmd(strCmd);
		liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
		if ( liRes )	return liRes;
	}

	return ERR_NONE;
}

int CR8062Base::setY0ContrastWindow(int y0, uint32_t uliTimeoutMsec)
{
	// set contrast window's x_center
	string strCmd = "AGCRPPY";
	strCmd.append(to_string(y0));
	if ( ! sendCmd(strCmd) )	return ERR_USB_PERIPH_COMMUNICATION;
	return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
}

int CR8062Base::getContrastWindow(int* x0, int* y0, int* liWidth, int* liHeight, bool* bEnabled, uint32_t uliTimeoutMsec)
{
	if ( x0 == nullptr )		return -1;
	if ( y0 == nullptr )		return -1;
	if ( liWidth == nullptr )	return -1;
	if ( liHeight == nullptr )	return -1;
	if ( bEnabled == nullptr )	return -1;

	if ( ! sendCmd("AGCRG") )	return ERR_USB_PERIPH_COMMUNICATION;
	int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Reader");
	if ( liRes )				return liRes;

	parseCWParams(bEnabled, x0, y0, liWidth, liHeight);

	return 0;
}

int CR8062Base::setPacketMode(uint32_t uliTimeoutMsec)
{
	sendCmd("CMCPGPM");
	int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Base");
	if ( liRes != ERR_NONE )	return liRes;

	// ans: <CM><CP PM="1"/></CM>
	bool bIsPktMode = false;
	for ( auto c : m_strAnswer )
	{
		if ( isdigit(c) )
		{
			if ( c == '1' )		bIsPktMode = true;
			else				bIsPktMode = false;
			break;
		}
	}
	// Already in packet mode
	if ( bIsPktMode )	return 0;

	// Set packet mode communication
	sendCmd("CMCPPPM1");
	return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
}

int CR8062Base::setCustomParam(string& strCmd, uint32_t uliTimeoutMsec)
{
	bool bRes = sendCmd(strCmd);
	if ( ! bRes )		return -1;

	return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
}

int CR8062Base::setAGCnormalMode(uint32_t uliTimeoutMsec)
{
	// Check if already in mono
	sendCmd("SCSPGMO");
	int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Base");
	if ( liRes )	return liRes;

	string strVal;
	string strWhat("MO=\"");
	size_t pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
	}

	if ( strVal.compare("NO") )
	{
		// Set normal AGC mode
		sendCmd("SCSPPMONO");
		liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
	}

	return liRes;
}

int CR8062Base::setAGCparam(uint8_t ucLux, uint32_t usiExpositionTime, uint8_t ucGain, uint32_t uliTimeoutMsec)
{
	// Control parameters limit
	if ( ucLux > MAX_ILLUMINATION )			return ERR_CR8062_IN_PARAMETERS;
	if ( usiExpositionTime > MAX_EXPOSURE )	return ERR_CR8062_IN_PARAMETERS;
	if ( ucGain > MAX_GAIN )				return ERR_CR8062_IN_PARAMETERS;

	// Get camera actual settings to send only the necessary commands
	int liIllum = -1, liGain = -1, liExp = -1;
	char pcMode[2] = {};

	m_pLogger->log(LOG_INFO, ".................Getting AGC params");
	int liRes =  getAGCparam(pcMode, &liIllum, &liGain, &liExp, uliTimeoutMsec);
	string strCmd("");


	m_pLogger->log(LOG_INFO, ".................Setting AGC params");
	if ( strcmp(pcMode, "BY") )
	{
		// Bypass automatic settings of the AGC to set them manually
		strCmd = "SCSPPMOBY";
		if ( ! sendCmd(strCmd) ) return ERR_USB_PERIPH_COMMUNICATION;
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Base");
		if ( liRes )			 return liRes;
	}

	if ( liIllum != int(ucLux) )
	{
		// Set illumination value
		strCmd = "AGBYPIL";
		strCmd.append(to_string(ucLux));
		if ( ! sendCmd(strCmd) ) return ERR_USB_PERIPH_COMMUNICATION;
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Base");
		if ( liRes )			 return liRes;
	}

	if ( liExp != int(usiExpositionTime) )
	{
		// set exposure value
		strCmd = "AGBYPEX";
		strCmd.append(to_string(usiExpositionTime));
		if ( ! sendCmd(strCmd) ) return ERR_USB_PERIPH_COMMUNICATION;
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Base");
		if ( liRes )			 return liRes;
	}

	if ( liGain != int(ucGain) )
	{
		// Set gain value
		strCmd = "AGBYPGN";
		strCmd.append(to_string(ucGain));
		if ( ! sendCmd(strCmd) ) return ERR_USB_PERIPH_COMMUNICATION;
		liRes = checkIfAnsCorrectly(eSet, eFast, "CR8062Base");
		if ( liRes )			 return liRes;
	}

	return ERR_NONE;
}

int CR8062Base::getAGCparam(char* pcMode, int* pIllum, int* pGain, int* pExp, uint32_t uliTimeoutMsec)
{
	if ( pcMode == nullptr )		return -1;
	if ( pIllum == nullptr )		return -1;
	if ( pGain == nullptr )			return -1;
	if ( pExp == nullptr )			return -1;

	sendCmd("SCSPG");
	int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Base");
	if ( liRes )	return liRes;

	parseAGCParam(pcMode, pIllum, pGain, pExp);

	return 0;
}

int CR8062Base::enableLogAGCparameters(bool bEnable, uint32_t uliTimeoutMsec)
{
    string strCmd = bEnable ? "CDOPPLA2" : "CDOPPLA0";
    sendCmd(strCmd);
    return checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
}

int CR8062Base::clearLogAGCparameters(uint32_t uliTimeoutMsec)
{
    if ( ! sendCmd("RDCMXCL") )	return ERR_USB_PERIPH_COMMUNICATION;

    int liRes = checkIfAnsCorrectly(eSet, uliTimeoutMsec, "CR8062Base");
    msleep(5);
    return liRes;
}

int CR8062Base::getLogAGCparameters(vector<logAGCparameters>& vAGCStruct, uint32_t uliTimeoutMsec)
{
    sendCmd("RDCMXRL");
    msleep(80); // just to be sure to have all the logs
    int liRes = checkIfAnsCorrectly(eGet, uliTimeoutMsec, "CR8062Base");
    if ( liRes )	return liRes;

    bool bRes = updateAGCparams(vAGCStruct);
    if ( ! bRes )
    {
        return ERR_USB_PERIPH_COMMUNICATION;
    }

    for ( uint i = 0; i != vAGCStruct.size(); i++ )
    {
        m_pLogger->log(LOG_DEBUG, "CR8062Base::getLogAGCparameters: AGC parameters #%d", vAGCStruct[i].liAttempNum);
        m_pLogger->log(LOG_DEBUG, "CR8062Base::getLogAGCparameters: Multiplier: %d", vAGCStruct[i].liMultiplier);
        m_pLogger->log(LOG_DEBUG, "CR8062Base::getLogAGCparameters: Index: %%%d", vAGCStruct[i].liIndex);
        m_pLogger->log(LOG_DEBUG, "CR8062Base::getLogAGCparameters: Exposure: %d", vAGCStruct[i].liExposure);
        m_pLogger->log(LOG_DEBUG, "CR8062Base::getLogAGCparameters: Illumination: %%%d", vAGCStruct[i].liIllumination);
        m_pLogger->log(LOG_DEBUG, "CR8062Base::getLogAGCparameters: Gain: %%%d", vAGCStruct[i].liGain);
    }

    return ERR_NONE;
}

int CR8062Base::checkIfAnsCorrectly(enumRequestType eType, uint32_t uliTimeoutMsec, const char* strClassName, const char* strFuncName)
{
	int liPayloadSize = 0;
	int liErrCode = readAndParseMsg(eType, liPayloadSize, uliTimeoutMsec);
	if ( liErrCode < 0 )
	{
		if ( liErrCode != (-ERR_CR8062_IN_PARAMETERS) )
		{
			m_pCamErr->decodeNotifyEvent((-liErrCode));
			m_pLogger->log(LOG_ERR, "%s::%s: error, %s", strClassName, strFuncName, errno2CamOperationResult(-m_liLastError).c_str());
		}
		return liErrCode;
	}

	switch ( eType )
	{
		case eSet: // I expect only command accepted answ
		{
			if ( liPayloadSize == 0 )	return liErrCode; // this is the case when the camera gives only an ACK

			string strAns(m_PayloadStruct.pktPayload.begin(), m_PayloadStruct.pktPayload.begin()+liPayloadSize);
			string strDef("<Response Val=\"");
			size_t pos = strAns.find(strDef);

			if  ( (strAns.size() < strDef.size()+pos+1) || ( pos == string::npos ) )
			{
				cout << "CR8062Base::checkIfAnsCorrectly: ERROR and payload is " << strAns << endl;
				liErrCode = -ERR_USB_PERIPH_COMMUNICATION;
			}
			else if ( strAns[pos+strDef.size()] != '0' )
			{
				liErrCode = findErrorCode(strAns);
			}

			if ( liErrCode >= 0 )	liErrCode = ERR_NONE; // in this case is the payload length
			else if ( liErrCode )	m_pCamErr->decodeNotifyEvent((-liErrCode));
		}
		break;

		case eGet:
		{
			string strAns(m_PayloadStruct.pktPayload.begin(), m_PayloadStruct.pktPayload.begin()+liPayloadSize);
			string strDef("<Response Val=\"-"); // usually err ans is -x
			if ( strAns.find(strDef) != string::npos )
			{
				liErrCode = findErrorCode(strAns);
				m_pCamErr->decodeNotifyEvent((-liErrCode));
			}
			else
			{
				m_strAnswer.assign(strAns);
				liErrCode = ERR_NONE;
			}
		}
		break;

		default:
			// for eAction do not do anything
		break;
	}

	// just to be sure, flush data received but not read
	m_pUSBIfr->cleanAll(); // TODO - test if is fine
	msleep(5);
	return liErrCode;
}

bool CR8062Base::updateAGCparams(vector<logAGCparameters>& vAGCStruct)
{
	if ( m_strAnswer.empty() )	return false;

	vAGCStruct.clear();

	// First find AGC params num
	vector<size_t> liPositions;
	size_t found = 0;
	bool bEnd = false;
	while ( ! bEnd )
	{
		found = m_strAnswer.find("AGC Normal Analysis", found+1);
		liPositions.push_back(found); // save also npos
		if ( found == string::npos )
		{
			bEnd = true;
		}
	}

	int liOccurrence = liPositions.size() - 1; // avoid npos
	vAGCStruct.resize(liOccurrence);

	for ( int i = 0; i < liOccurrence; i++ )
	{
		vAGCStruct[i].liAttempNum = i+1;
		vAGCStruct[i].liMultiplier = getValueAfterFirstOccurrenceInString("Multiplier: ", m_strAnswer.substr(liPositions[i], liPositions[i+1]));
		vAGCStruct[i].liIndex = getValueAfterFirstOccurrenceInString("Index: %", m_strAnswer.substr(liPositions[i], liPositions[i+1]));
		vAGCStruct[i].liExposure = getValueAfterFirstOccurrenceInString("Exposure: ", m_strAnswer.substr(liPositions[i], liPositions[i+1]));
		vAGCStruct[i].liIllumination = getValueAfterFirstOccurrenceInString("Illumination: %", m_strAnswer.substr(liPositions[i], liPositions[i+1]));
		vAGCStruct[i].liGain = getValueAfterFirstOccurrenceInString("Gain: %", m_strAnswer.substr(liPositions[i], liPositions[i+1]));
	}

	return true;
}

int CR8062Base::getValueAfterFirstOccurrenceInString(string strToBeFound, const string& strFull)
{
	size_t found = strFull.find(strToBeFound);
	if ( found == string::npos )
	{
		return -1;
	}

	string strTmp("");
	for ( size_t i = found+strToBeFound.size(); i < strFull.size(); i++ )
	{
		if ( isdigit(strFull[i]) )
		{
			strTmp.push_back(strFull[i]);
		}
		else
		{
			break;
		}
	}

	int liVal = 0;
	if ( ! wrapStoi(strTmp, &liVal) )	return -1;
	return liVal;
}

void CR8062Base::parseAGCParam(char* pcMode, int* pIllum, int* pGain, int* pExp)
{
	if ( pcMode == nullptr )	return;
	if ( pIllum == nullptr )	return;
	if ( pGain == nullptr )		return;
	if ( pExp == nullptr )		return;

	// <SP MO="NO" IL="50" EX="4000" GN="0" FP="50"/>
	string strVal;
	string strWhat("MO=\"");
	size_t pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		strcpy(pcMode, strVal.c_str());
	}

	strWhat.assign("IL=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, pIllum);
	}

	strWhat.assign("EX=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, pExp);
	}

	strWhat.assign("GN=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, pGain);
	}

	return;
}

void CR8062Base::parseCWParams(bool* bEnabled, int* x0, int* y0, int* siWidth, int* siHeight)
{
	if ( bEnabled == nullptr )	return;
	if ( x0 == nullptr )		return;
	if ( y0 == nullptr )		return;
	if ( siWidth == nullptr )	return;
	if ( siHeight == nullptr )	return;

	// Expected answer <AG>	<CR CX="300" CY="300" PX="0" PY="0" ES="0" ED="0" CT="227" LT="8"
	//				   LP="200" HT="85" HP="200" DL="11" ME="7"/> </AG>

	string strVal;
	string strWhat("<RO=\"");
	size_t pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		*bEnabled = ( ! strVal.compare("1") ) ? true : false;
	}

	strWhat.assign("PX=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, x0);
	}

	strWhat.assign("PY=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, y0);
	}

	strWhat.assign("CX=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, siWidth);
	}

	strWhat.assign("CY=\"");
	pos = m_strAnswer.find(strWhat);
	if ( pos != string::npos )
	{
		size_t size = strWhat.size();
		size_t end = m_strAnswer.find('"', pos+size);
		strVal = m_strAnswer.substr(pos+strWhat.size(), end-pos-size);
		wrapStoi(strVal, siHeight);
	}

	return;
}

int CR8062Base::findErrorCode(string& strAnsw)
{
	string strDef("<Response Val=\"-");
	size_t pos = strAnsw.find(strDef);
	if ( pos == string::npos )
	{
		return -ERR_CR8062_IN_PARAMETERS;
	}

	string strTmp("");
	for ( size_t i = pos+strDef.size(); i < strAnsw.size(); i++ )
	{
		if ( isdigit(strAnsw[i]) )
		{
			strTmp.push_back(strAnsw[i]);
		}
		else
		{
			break;
		}
	}

	int liErrorCode = 0;
	if ( wrapStoi(strTmp, &liErrorCode) ) return -ERR_CR8062_IN_PARAMETERS;

	switch (liErrorCode)
	{
		case 6:
			return -ERR_CR8062_IN_PARAMETERS;

		case ERR_CR8062_NOT_SUPPORTED_CMD:
			return -ERR_CR8062_NOT_SUPPORTED_CMD;

		case ERR_CR8062_FLASH_PROGR:
			return -ERR_CR8062_FLASH_PROGR;

		default:
			return -ERR_CR8062_IN_PARAMETERS;
	}
}

int CR8062Base::wrapStoi(const string& str, int* pVal, size_t* pos, int base)
{
	// wrapping std::stoi because it may throw an exception
	try
	{
		*pVal = std::stoi(str, pos, base);
		return 0;
	}

	catch (const std::invalid_argument& ia)
	{
		std::cerr << "Invalid argument: " << ia.what() << std::endl;
		return -1;
	}

	catch (const std::out_of_range& oor)
	{
		std::cerr << "Out of Range error: " << oor.what() << std::endl;
		return -2;
	}

	catch (const std::exception& e)
	{
		std::cerr << "Undefined error: " << e.what() << std::endl;
		return -3;
	}
}

