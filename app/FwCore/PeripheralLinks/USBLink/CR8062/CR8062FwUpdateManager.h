/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062FwUpdateManager.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CR8062FwUpdateManager class.
 @details

 ****************************************************************************
*/

#ifndef CR8062FWUPDATEMANAGER_H
#define CR8062FWUPDATEMANAGER_H

#include <sys/stat.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include <string>
#include <math.h>

#include "CR8062Protocol.h"

using namespace std;

class CR8062FwUpdateManager : public CR8062Protocol
{
	public:

		/*! *************************************************************************************************
		 * @brief CR8062FwUpdateManager default constructor
		 * **************************************************************************************************
		 */
		CR8062FwUpdateManager(int liPayloadSize = DEF_FWUPDATE_PAYLOAD_SIZE);

		/*! *************************************************************************************************
		 * @brief ~CR8062FwUpdateManager virtual destructor
		 * **************************************************************************************************
		 */
		virtual ~CR8062FwUpdateManager();

		/*! *************************************************************************************************
		 * @brief  init initialize the class setting HID interface and logger
		 * @param  pHIDIfr pointer to the interface
		 * @param  pLogger pointer to the logger
         * @param pErr pointer to the error structure
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool init(HIDInterface* pHIDIfr, Log* pLogger, USBErrors* pErr);

		/*! *************************************************************************************************
		 * @brief  upgradeFirmware function used to upgrade the firmware of the camera
		 * @param  strFileName name of the file to be sent
         * @param  llength size of file
		 * @param  uliTimeoutMsec reading timeout in milliseconds
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool upgradeFirmware(string strFileName, long llength, uint32_t uliTimeoutMsec = eGetReady4FwUpdateTimeOutMsec);

		/*! *************************************************************************************************
		 * @brief  waitForInstallationCompleted wait for a message from the camera after the new fw
		 *		   installing phase has started
		 * @param  uliTimeoutMsec reading timeout in milliseconds
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool waitForInstallationCompleted(uint32_t uliTimeoutMsec = eFwUpdateTimeOutMsec);

		/*! *************************************************************************************************
		 * @brief  checkIfAccepted check if the command sent has been accepted by the camera
		 * @param  uliTimeoutMsec reading timeout in milliseconds
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool checkIfAccepted(uint32_t uliTimeoutMsec);

	private:

		/*! *************************************************************************************************
		 * @brief  readBinFile read binary file and save the contents in the buffer passed as argument
		 * @param  strFileName name of the file
		 * @param  rgcFile pointer to the buffer where the file will be stored
		 * @param  liFileSize size of the file read
		 * @return 0 in case of success, -1 otherwise
		 * **************************************************************************************************
		 */
		int readBinFile(string strFileName, unsigned char* rgcFile, int32_t liFileSize);

		/*! *************************************************************************************************
		 * @brief  file2Pkts function used to create 65kB pkts ready to be sent to the camera from the bin file
		 * @param  pBinFile pointer to the buffer to be sent
		 * @param  liMsgLength length of the buffer
		 * @return vector of pkts
		 * **************************************************************************************************
		 */
		vector<vector<unsigned char>> file2Pkts(const unsigned char* pBinFile, int liMsgLength);

		/*! *************************************************************************************************
		 * @brief  copyFirstHugePktToVector create first huge pkt (65k)
		 * @param  vFirstPkt vector where the packet will be saved
		 * @param  pBinFile pointer to the file to be converted in pkt
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool copyFirstHugePktToVector(vector<unsigned char> &vFirstPkt, const unsigned char *pBinFile);

		/*! *************************************************************************************************
		 * @brief  copyIntermediateHugePktToVector create last huge pkt (65k)
		 * @param  vPkt vector where the packet will be saved
		 * @param  pBinFile pointer to the file to be converted in pkt
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool copyIntermediateHugePktToVector(vector<unsigned char> &vPkt, const unsigned char *pBinFile);

		/*! *************************************************************************************************
		 * @brief  copyLastHugePktToVector create last huge pkt (what is missing, < 65k)
		 * @param  vPkt vector where the packet will be saved
		 * @param  liBytesLeft bytes left to be sent
		 * @param  pBinFile pointer to the file to be converted in pkt
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool copyLastHugePktToVector(vector<unsigned char>& vPkt, int liBytesLeft, const unsigned char *pBinFile);

		/*! *************************************************************************************************
		 * @brief  createCRCfromVector create crc16 from the vector passed as argument
		 * @param  vBuff buffer where the crc16 has to be calculated
		 * @param  usiLength length of the buffer
		 * @return crc16
		 * **************************************************************************************************
		 */
		crc_t createCRCfromVector(vector<unsigned char> &vBuff, size_t usiLength);

		/*! *************************************************************************************************
		 * @brief  sendCustomPkt function used to send 65B out of 65kB pkts created with file2Pkts
		 * @param  vPkt packet to be sent
		 * @param  liMsgLength length of the packet
		 * @param  liOffset to send different part of the whole packet (65kB)
		 * @return true in case of success, false otherwise
		 * **************************************************************************************************
		 */
		bool sendCustomPkt(const vector<unsigned char>& vPkt, int liMsgLength, int liOffset);

        /*! *************************************************************************************************
         * @brief  findErrorCode returns the numeric code corresponding to the string
         * @param  strAnsw the error string
         * @return the corresponding numeric code
         * **************************************************************************************************
         */
        int findErrorCode(string& strAnsw);

    private:

        int32_t m_nBytesSent;
        int16_t m_siRequestId;
        int32_t m_liFwSize;
        int32_t m_liOffset;
        crc_t	m_nCRCHugePkt;
        unsigned char m_nBuffTx[MAX_SIZE];
        unsigned char m_nBuffRx[MAX_SIZE];
        unsigned char m_usTransactionNum;

};

#endif // CR8062FWUPDATEMANAGER_H
