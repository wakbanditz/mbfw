/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Link.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CR8062Link class.
 @details

 ****************************************************************************
*/

#include <algorithm>

#include "CR8062Link.h"
#include "libusb-1.0/libusb.h"
#include "MainExecutor.h"

#define CR8062_VID					0x11FA
#define HID_KEYBOARD_SUBCLASS_ID	1
#define HID_DEVICE_SUBCLASS_ID		0
#define SWITCH_CLASS_TIME_SEC		4
#define MIN_SIZE_FOR_CHECK			30

#define CALIBR_ID					0
#define USAGE_ID					1
#define GENERAL_BATCH_CALIBR_FOLDER	"/home/root/CAMERA/BATCH_CALIBR/"
#define GENERAL_BATCH_USAGE_FOLDER	"/home/root/CAMERA/BATCH_USAGE/"
#define GENERAL_BATCH_X0_NAME		"X0.batch"
#define GENERAL_BATCH_X3_NAME		"X3.batch"
#define GENERAL_BATCH_BC_NAME		"1D.batch"
#define GENERAL_BATCH_DM_NAME		"2D.batch"
#define GENERAL_BATCH_BC_ABS_NAME	"ConeAbsence.batch"

#define CODED_NAME_X0				"Sample0_"
#define CODED_NAME_X3				"Sample3_"
#define CODED_NAME_BC				"BC_"
#define CODED_NAME_DM				"DataMatrix_"
#define CODED_NAME_BC_ABS			"ConeAbsence_"


CR8062Link::CR8062Link()
{
	m_nVID = -1;
	m_nPID = -1;
	m_nInterfClass = -1;
	m_nInterfSubClass = -1;
	m_pLastBatchSent.first = -1;
	m_pLastBatchSent.second = eBatchTarget::eNone;

	eUSBType = eNoDevice;
}

CR8062Link::~CR8062Link()
{

}

/*!*************************************************************************
* The device is HID_keyboard class by default.
* We want to use it in CDC class, but there is no way to change it directly
* (the camera do not allow to do that).
* The steps to perform are: HID_keyboard -> HID_device -> CDC_device.
****************************************************************************/
bool CR8062Link::init(Log * pLogger, USBInterface* pUSBIfr)
{
	if ( pUSBIfr == NULL )	return false;
	if ( pLogger == NULL )	return false;

	setLogger(pLogger);
	m_pUSBIfr = pUSBIfr;

	int liDevClass = autodetectCR8062Config();
	int liRes = 0;

	switch ( liDevClass )
	{
		case eCDCDevice:
		break;

		case eHIDKeyboard:
			liRes = switchHID2CDC(eHIDKeyboard);
			if ( liRes < 0 )
			{
				pLogger->log(LOG_ERR, "CR8062Link::init: unable to set device in CDC class");
				return false;
			}
		break;

		case eHIDDevice:
			liRes = switchHID2CDC(eHIDDevice);
			if ( liRes < 0 )
			{
				pLogger->log(LOG_ERR, "CR8062Link::init: unable to set device in CDC class");
				return false;
			}
		break;

		default:
			pLogger->log(LOG_ERR, "CR8062Link::init: unable to find any USB class device");
			if ( m_Camera.getCamErr() != nullptr )
			{
				m_Camera.getCamErr()->decodeNotifyEvent(ERR_CAMERA_NOT_DETECTED);
			}
			return false;
		break;
	}

	pLogger->log(LOG_INFO, "CR8062Link::init: device set in CDC class");

	// Now the device is set in CDC class - perform additive check on portName
	if ( ! pUSBIfr->isOpen() )
	{
		if ( pUSBIfr->openInterface() )
		{
			pLogger->log(LOG_ERR, "CR8062Link::init: unable to open USB Interface");
			return false;
		}
	}

	bool bRes = m_Camera.init(pUSBIfr, pLogger); // m_pCamError can be passed as arguent in case of error
	if ( ! bRes )	return false;
	bRes = m_Reader.init(pUSBIfr, pLogger);
	if ( ! bRes )	return false;

	return true;
}

bool CR8062Link::updateCR8062FW(const string& strFileName, long llength, const string& strNewFWVers)
{
	if ( strFileName.empty() )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::updateCR8062FW: invalid fileName");
		return false;
	}

	/*! ******* Get ready for FW Update ******** **/

	m_pLogger->log(LOG_INFO, "CR8062Link::updateCR8062FW: starting fw update procedure");

	enumDeviceType eConfig = autodetectCR8062Config();
	if ( switchCDC2HID(eConfig) < 0 )	return false;

	m_pLogger->log(LOG_INFO, "CR8062Link::updateCR8062FW: device set to HID Device class");

	bool bRes = getVidPid();
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::updateCR8062FW: unable to get VID and PID");
		return false;
	}

	HIDInterface HIDIfr;
	bRes = HIDIfr.init(m_nVID, m_nPID, getLogger());
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::updateCR8062FW: unable to initialize HID interface");
		return false;
	}

	// Now the camera is set as HID_Device class
	SAFE_DELETE(m_pFwUpdate)
	m_pFwUpdate = new CR8062FwUpdateManager();
	bRes = m_pFwUpdate->init(&HIDIfr, getLogger(), m_Camera.getCamErr());
	if ( ! bRes )
	{
		return false;
	}

	/*! ********** Start FW Update  *********** **/

	bRes = m_pFwUpdate->upgradeFirmware(strFileName, llength);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::updateCR8062FW: unable to upgrade fw from file %s", strFileName.c_str());
		SAFE_DELETE(m_pFwUpdate);
		return false;
	}

	m_pLogger->log(LOG_INFO, "CR8062Link::updateCR8062FW: installing...");
	bRes = m_pFwUpdate->waitForInstallationCompleted();
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::updateCR8062FW: unable to upgrade fw from file %s", strFileName.c_str());
		SAFE_DELETE(m_pFwUpdate);
		return false;
	}

	SAFE_DELETE(m_pFwUpdate);

	m_pLogger->log(LOG_INFO, "CR8062Link::updateCR8062FW: rebooting");
	msleep(4000);

	/*! ******* Reinit the device to CDC ******** **/

	bRes = reInitAfterFwUpdate(strNewFWVers, m_pUSBIfr);

	return bRes;
}

static libusb_context *usb_contextt = NULL;

enumDeviceType CR8062Link::autodetectCR8062Config()
{
	libusb_device **devs;	

	m_nInterfClass = -1;
	m_nInterfSubClass = -1;
	m_nVID = -1;
	m_nPID = -1;

	if ( hid_init() < 0 )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::autodetectConfig: unable to init hid library");
		return eNoDevice;
	}

	ssize_t iDevNum = libusb_get_device_list(usb_contextt, &devs);
	if ( iDevNum < 0)
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::autodetectConfig: no usb device found");
		return eNoDevice;
	}

	libusb_device *dev;
	struct libusb_config_descriptor *confDesc = NULL;

	int i = 0;
	while ( ( dev = devs[i++] ) != NULL )
	{
		struct libusb_device_descriptor desc;

		int liRes = libusb_get_device_descriptor(dev, &desc);
		if ( liRes )
		{
			continue;
		}
		if ( desc.idVendor != CR8062_VID )
		{
			continue;
		}
		m_nVID = desc.idVendor;
		m_nPID = desc.idProduct;

		liRes = libusb_get_config_descriptor(dev, 0, &confDesc);
		if ( liRes )
		{
			continue;
		}
		m_nInterfClass = confDesc->interface->altsetting->bInterfaceClass;
		m_nInterfSubClass = confDesc->interface->altsetting->bInterfaceSubClass;

		if ( m_nVID != -1 && m_nPID != -1 )
		{
			break;
		}
	}

	enumDeviceType liDevice = findConfig(m_nInterfClass, m_nInterfSubClass);

	eUSBType = liDevice;

	return liDevice;
}

int CR8062Link::uploadGeneralBatchFile(const char* pcFileName)
{
	ifstream file(pcFileName);
	if ( ! file.good() )
	{
		return -1;
	}

	vector<string> vCommands;
	string strTmp("");

	// Save all the lines in a different string
	while ( getline(file, strTmp) )
	{
		// Remove white spaces, '/r' and tab spaces ('/t')
		strTmp.erase(remove(strTmp.begin(), strTmp.end(), ' '), strTmp.end());
		strTmp.erase(remove(strTmp.begin(), strTmp.end(), '\r'), strTmp.end());
		strTmp.erase(remove(strTmp.begin(), strTmp.end(), '\t'), strTmp.end());

		// Do not consider if empty
		if ( strTmp.empty() )		continue;
		// Remove comments
		if ( strTmp[0] == '/' && strTmp[1] == '/')		continue;

		string strCommand("");
		for ( auto c : strTmp )
		{
			if ( c == '/' )
			{
				break;
			}
			strCommand.push_back(c);
		}

		// If some particular commands are found skip them - general like IMCPPWS*
		if ( strCommand.find("*") != string::npos )	continue;

		vCommands.push_back(strCommand);
	}
	file.close();

	// send commands
	m_pLogger->log(LOG_DEBUG, "CR8062Link::uploadBatchFile: Starting to upload batch file");
	int foo = 0;
	for ( auto& sCmd : vCommands )
	{
		int liListenMs = ( sCmd.find("CFR") != string::npos ) ? 200 : 110;
		int liRes = m_Camera.setCustomParam(sCmd, liListenMs);
		if ( liRes != ERR_NONE )
		{
			return liRes;
		}

		// this is needed to be able to store the decoding timeout
		string strDecodeTimeoutCmd("CDOP*AT"); // CDOP*AT* actually
		string subStrCmd(sCmd, 0, strDecodeTimeoutCmd.size());
		if ( compareWithWildCard(strDecodeTimeoutCmd, subStrCmd, '*') )
		{
			// To be sure is a set with a number and not a get
			if ( sCmd.size() > strDecodeTimeoutCmd.size() )
			{
				int liDecodeTimeoutMsec = stoi(sCmd.substr(strDecodeTimeoutCmd.size()));
				m_Reader.setDecodingTimeoutMs(liDecodeTimeoutMsec);
			}
		}

		// this is needed to set the correct image format we are expecting
		string strSetFormatCmd("ENIM*ET"); // ENIM*ET* actually
		string subStrFormatCmd(sCmd, 0, strSetFormatCmd.size());
		if ( compareWithWildCard(strSetFormatCmd, subStrFormatCmd, '*') )
		{
			eImageFormat eFormat = ( sCmd.back() == '1' ) ? eRAW : eJPEG;
			m_Camera.setImageFormat(eFormat);
		}
		foo++;
		msleep(8);
		m_pLogger->log(LOG_DEBUG, "CR8062Link::uploadBatchFile: Percentage: %d%%", 100*foo/vCommands.size());
	}

	return 0;
}

int CR8062Link::uploadBatchFile(uint8_t ucSlot, eBatchTarget eTarget)
{
	if ( ucSlot <= 0 )										return -1;
	if ( ucSlot > SCT_NUM_TOT_SLOTS*SCT_NUM_TOT_SECTIONS )	return -1;

	string strFileNameGeneral = GENERAL_BATCH_USAGE_FOLDER;
	string strCodedTarget("");
	switch ( eTarget )
	{
		case eBatchTarget::eSampleX0:
			strFileNameGeneral += GENERAL_BATCH_X0_NAME;
			strCodedTarget.assign(CODED_NAME_X0);
		break;

		case eBatchTarget::eSampleX3:
			strFileNameGeneral += GENERAL_BATCH_X3_NAME;
			strCodedTarget.assign(CODED_NAME_X3);
		break;

		case eBatchTarget::eLinearBarcode:
			strFileNameGeneral += GENERAL_BATCH_BC_NAME;
			strCodedTarget.assign(CODED_NAME_BC);
		break;

		case eBatchTarget::eDataMatrix:
			strFileNameGeneral += GENERAL_BATCH_DM_NAME;
			strCodedTarget.assign(CODED_NAME_DM);
		break;

		case eBatchTarget::eConeAbsence:
			strFileNameGeneral += GENERAL_BATCH_BC_ABS_NAME;
			strCodedTarget.assign(CODED_NAME_BC_ABS);
		break;

		case eBatchTarget::eNone:
			return -1; // No target
		break;
	}

	// if the target is the same there is no need to send the general batch
	int liRes = 0;
	// if the last batch is related to calibr but we switch to usage (or vice versa),
	// then the general batch has to be sent again
	if ( m_pLastBatchSent.first != USAGE_ID || m_pLastBatchSent.second != eTarget )
	{
		// Upload common settings (section/slot independent)
		liRes = uploadGeneralBatchFile(strFileNameGeneral.c_str());
		m_pLastBatchSent.first = USAGE_ID;
		m_pLastBatchSent.second = eBatchTarget::eNone;
		if ( liRes )
		{
			m_pLogger->log(LOG_ERR, "CR8062Link::uploadBatchFile: unable to set general settings");
			return -1;
		}
	}

	// Build new filename to read specific settings.
	// If the target is the sample, only CW and Crop have to be set, otherwise also the ROI
	strCodedTarget += to_string(ucSlot);
	structRect rect;

	enum { eStart, eSetCrop, eSetRoi, eSetCw, eClear, eRecover, eEnd };
	unsigned char cState = eStart;
	vector<unsigned char> cLastState;
	bool bRes = false;

	while ( cState != eEnd )
	{
		switch (cState)
		{
			case eStart:
				cState = eSetCrop;
			break;

			case eSetCrop:
				bRes = cameraBoardManagerSingleton()->m_pConfig->getItemValue(strCodedTarget, rect);
				if ( ! bRes )
				{
					m_pLogger->log(LOG_ERR, "CR8062Link::uploadBatchFile: target not coded [%s]", strCodedTarget.c_str());
					return -1;
				}

				m_pLogger->log(LOG_DEBUG, "CR8062Link::uploadBatchFile: setting crop coordinates");
				liRes = m_Camera.setCropCoordinates(rect.x, rect.y, rect.width, rect.height);
				if ( liRes )
				{
					m_pLogger->log(LOG_ERR, "CR8062Link::uploadBatchFile: unable to set crop coordinates");
					cLastState.push_back(eSetCrop);
					cState = eRecover;
				}

                cState = ( eTarget == eBatchTarget::eLinearBarcode || eTarget == eBatchTarget::eDataMatrix ) ? eSetRoi : eSetCw;
			break;

			case eSetRoi:
				msleep(5);
				m_pLogger->log(LOG_DEBUG, "CR8062Link::uploadBatchFile: setting ROI");
				liRes = m_Reader.setROI(rect.x, rect.y, rect.width, rect.height);
				if ( ! bRes )
				{
					m_pLogger->log(LOG_ERR, "CR8062Link::uploadBatchFile: unable to set ROI");
					cLastState.push_back(eSetRoi);
					cState = eRecover;
				}
				cState = eSetCw;
			break;

			case eSetCw:
				// Data matrix and cone absence have the same CW called CW_DataMatrix_xx
				if ( rect.eTarget == eBatchTarget::eConeAbsence )
				{
					string strCwCone(TARGET_NAME_DATAMATRIX);
					strCwCone.push_back('_');
					for ( auto c : strCodedTarget )
					{
						if ( isdigit(c) )	strCwCone.push_back(c);
					}
					strCodedTarget.assign(strCwCone);
				}

				m_pLogger->log(LOG_DEBUG, "CR8062Link::uploadBatchFile: setting CW");
				strCodedTarget.insert(0, "CW_");
				bRes = cameraBoardManagerSingleton()->m_pConfig->getItemValue(strCodedTarget, rect);
				if ( ! bRes )
				{
					m_pLogger->log(LOG_ERR, "CR8062Link::uploadBatchFile: target not coded[%s]", strCodedTarget.c_str());
					return -1;
				}

				msleep(5);
				liRes = m_Camera.setContrastWindow(rect.x, rect.y, rect.width, rect.height);
				if ( liRes )
				{
					m_pLogger->log(LOG_ERR, "CR8062Link::uploadBatchFile: unable to set contrast window");
					cLastState.push_back(eSetCw);
					cState = eRecover;
				}
				cState = eClear;
			break;

			case eClear:
                if ( rect.eTarget == eBatchTarget::eLinearBarcode || rect.eTarget == eBatchTarget::eDataMatrix )
				{

					m_pLogger->log(LOG_INFO, ".................Clearing log AGC params");
					// Clear log every time, to avoid overflow of the buffer
					msleep(5);
					liRes = m_Reader.clearLogAGCparameters();
					if ( liRes )
					{
						m_pLogger->log(LOG_ERR, "CR8062Link::uploadBatchFile: unable to clear AGC log");
						cLastState.push_back(eClear);
						cState = eRecover;
					}
				}
				cState = eEnd;
			break;

			case eRecover:
				if ( cLastState.size() > 1 )
				{
					m_pLogger->log(LOG_ERR, "CR8062Link::uploadBatchFile: unable to recover");
					return -1;
				}
				else
				{
					msleep(4000);
					bringToLife();
					cState = eStart;

					int liRes = uploadGeneralBatchFile(strFileNameGeneral.c_str());
					if ( liRes )
					{
						m_pLogger->log(LOG_ERR, "CR8062Link::uploadBatchFile: unable to recover");
						return -1;
					}
				}
			break;

			default:
				cState = eEnd;
			break;
		}
	}
	// Update last batch sent
	m_pLastBatchSent.second = eTarget;

	return 0;
}

int CR8062Link::uploadCalibrBatchFile(string& strCodedTarget)
{
	structRect rect;
    bool bRes = cameraBoardManagerSingleton()->m_pConfig->getItemValue(strCodedTarget, rect);
	if ( ! bRes )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::uploadCalibrBatchFile: target not coded [%s]", strCodedTarget.c_str());
		return -1;
	}

	string strFileNameGeneral = GENERAL_BATCH_CALIBR_FOLDER;
	switch ( rect.eTarget )
	{
		case eBatchTarget::eSampleX0:
			strFileNameGeneral += GENERAL_BATCH_X0_NAME;
		break;

		case eBatchTarget::eSampleX3:
			strFileNameGeneral += GENERAL_BATCH_X3_NAME;
		break;

		case eBatchTarget::eLinearBarcode:
			strFileNameGeneral += GENERAL_BATCH_BC_NAME;
		break;

		case eBatchTarget::eDataMatrix:
			strFileNameGeneral += GENERAL_BATCH_DM_NAME;
		break;

		case eBatchTarget::eConeAbsence:
			strFileNameGeneral += GENERAL_BATCH_BC_ABS_NAME;
		break;

		case eBatchTarget::eNone:
			return -1; // No target
		break;
	}

	int liRes = 0;
	if ( m_pLastBatchSent.first != CALIBR_ID || m_pLastBatchSent.second != rect.eTarget )
	{
		// Upload common settings (section/slot independent)
		liRes = uploadGeneralBatchFile(strFileNameGeneral.c_str());
		m_pLastBatchSent.first = CALIBR_ID;
		m_pLastBatchSent.second = eBatchTarget::eNone;
		if ( liRes )
		{
			m_pLogger->log(LOG_ERR, "CR8062Link::uploadCalibrBatchFile: unable to set general settings");
			return -1;
		}
	}

	// Upload specific settings slot and section dependent
	enum { eStart, eSetCrop, eSetRoi, eSetCw, eClear, eRecover, eEnd };
	unsigned char cState = eStart;
	vector<unsigned char> cLastState;

	while ( cState != eEnd )
	{
		switch (cState)
		{
			case eStart:
				cState = eSetCrop;
			break;

			case eSetCrop:
				// In order to let the user see a bigger image, the crop is temporary enlarged
				structRect rectEnl;
				if ( ! strCodedTarget.substr(0,3).compare("CW_") )		enlargeYourCrop_CW(&rect, &rectEnl, 140, 280);
				else													enlargeYourCrop(&rect, &rectEnl);
				m_pLogger->log(LOG_DEBUG, "CR8062Link::uploadCalibrBatchFile: setting crop coordinates");
				liRes = m_Camera.setCropCoordinates(rectEnl.x, rectEnl.y, rectEnl.width, rectEnl.height);
				if ( liRes )
				{
					m_pLogger->log(LOG_ERR, "CR8062Link::uploadCalibrBatchFile: unable to set crop coordinates");
					cLastState.push_back(eSetCrop);
					cState = eRecover;
				}
				cState = ( rect.eTarget==eBatchTarget::eLinearBarcode || rect.eTarget==eBatchTarget::eDataMatrix ) ? eSetRoi : eSetCw;
			break;

			case eSetRoi:
				msleep(5);
				m_pLogger->log(LOG_DEBUG, "CR8062Link::uploadCalibrBatchFile: setting ROI");
				liRes = m_Reader.setROI(rect.x, rect.y, rect.width, rect.height);
				if ( bRes )
				{
					m_pLogger->log(LOG_ERR, "CR8062Link::uploadCalibrBatchFile: unable to set ROI");
					cLastState.push_back(eSetRoi);
					cState = eRecover;
				}
				cState = eSetCw;
			break;

			case eSetCw:
				// Data matrix and cone absence have the same CW called CW_DataMatrix_xx
				if ( rect.eTarget == eBatchTarget::eConeAbsence )
				{
					string strCwCone(TARGET_NAME_DATAMATRIX);
					strCwCone.push_back('_');
					for ( auto c : strCodedTarget )
					{
						if ( isdigit(c) )	strCwCone.push_back(c);
					}
					strCodedTarget.assign(strCwCone);
				}

				// AGC contrast window - if is already CW, use the same coordinates as the crop and roi
				if ( strCodedTarget.substr(0,3).compare("CW_") )
				{
					strCodedTarget.insert(0, "CW_");
					bRes = cameraBoardManagerSingleton()->m_pConfig->getItemValue(strCodedTarget, rect);
					if ( ! bRes )
					{
						m_pLogger->log(LOG_ERR, "CR8062Link::uploadCalibrBatchFile: target not coded [%s]", strCodedTarget.c_str());
						return -1;
					}
				}

				m_pLogger->log(LOG_DEBUG, "CR8062Link::uploadCalibrBatchFile: setting CW");
				msleep(5);
				liRes = m_Camera.setContrastWindow(rect.x, rect.y, rect.width, rect.height);
				if ( liRes )
				{
					m_pLogger->log(LOG_ERR, "CR8062Link::uploadCalibrBatchFile: unable to set crop coordinates");
					cLastState.push_back(eSetCw);
					cState = eRecover;
				}
				cState = eClear;
			break;

			case eClear:
				if ( rect.eTarget==eBatchTarget::eLinearBarcode || rect.eTarget==eBatchTarget::eDataMatrix )
				{
					// Clear log every time, to avoid overflow of the buffer
					m_pLogger->log(LOG_DEBUG, "CR8062Link::uploadCalibrBatchFile: clearing AGC parameters");
					msleep(5);
					liRes = m_Reader.clearLogAGCparameters();
					if ( liRes )
					{
						m_pLogger->log(LOG_ERR, "CR8062Link::uploadCalibrBatchFile: unable to clear AGC log");
						cLastState.push_back(eClear);
						cState = eRecover;
					}
				}
				cState = eEnd;
			break;

			case eRecover:
				if ( cLastState.size() > 1 )
				{
					m_pLogger->log(LOG_ERR, "CR8062Link::uploadCalibrBatchFile: unable to recover");
					return -1;
				}
				else
				{
					msleep(4000);
					bringToLife();
					cState = eStart;
					int liRes = uploadGeneralBatchFile(strFileNameGeneral.c_str());
					if ( liRes )
					{
						m_pLogger->log(LOG_ERR, "CR8062Link::uploadCalibrBatchFile: unable to recover");
						return -1;
					}
				}
			break;

			default:
				cState = eEnd;
			break;
		}
	}
	// Update last batch sent
	m_pLastBatchSent.second = rect.eTarget;

	return 0;
}

void CR8062Link::clearBatchFlag()
{
	m_pLastBatchSent.second = eBatchTarget::eNone;
}

bool CR8062Link::resetCamera(int32_t liTimeoutMs)
{
	m_Camera.sendCmd("CFR");
	int32_t liMsgSize = m_Camera.readMsgReceived(liTimeoutMs);
	if ( liMsgSize < 1 )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::resetCamera: error reading from USB port");
		return false;
	}
	msleep(200);
	return true;
}

bool CR8062Link::bringToLife()
{
	int liDevClass = autodetectCR8062Config();

	switch ( liDevClass )
	{
		case eCDCDevice:
		break;

		case eHIDKeyboard:
			switchHID2CDC(eHIDKeyboard);
		break;

		case eHIDDevice:
			switchHID2CDC(eHIDDevice);
		break;

		default:
			m_pLogger->log(LOG_ERR, "CR8062Link::init: unable to find any USB class device");
			if ( m_Camera.getCamErr() != nullptr )
			{
				m_Camera.getCamErr()->decodeNotifyEvent(ERR_CAMERA_NOT_DETECTED);
			}
			return false;
		break;
	}

	m_pLogger->log(LOG_INFO, "CR8062Link::getFdAfterCameraDisconnection: device set in CDC class");

	// Now the device is set in CDC class - perform additive check on portName
	if ( m_pUSBIfr->openInterface() )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::getFdAfterCameraDisconnection: unable to find new fd");
		return false;
	}

	return true;
}

bool CR8062Link::addBMPHeader(const structInfoImage* pImage, vector<unsigned char>& vImage)
{
	if ( pImage->eFormat != eRAW )		return -1;
	if ( pImage == nullptr )			return -1;

	/* ************************************************************************
	 * Raw picture from the camera is:     What bmp image has to be like:
	 *      0------------->                     ------------->n
	 *      -------------->                     -------------->
	 *      -------------->n                    0------------->
	 * For this reason y-axis have to be inverted
	 * ************************************************************************/
	swapY(vImage, pImage->liWidth, pImage->liHeight);

	// Take care of padding
	unsigned int uliBufLength = 0;
	int padBytes = 0;
	if ( pImage->liWidth % 4 != 0 )
	{
		padBytes = 4 - ( pImage->liWidth % 4 );
		uliBufLength = pImage->liHeight * (pImage->liWidth + padBytes);
	}

	unsigned char rgcBuff[BMP_HEADER_SIZE + PALETTE_LEN];
	BmpHeader sBmpHeader;
	BmpInfoHeader sBmpInfoHeader;

	sBmpHeader.magic[0] = MAGIC_MSB;
	sBmpHeader.magic[1] = MAGIC_LSB;
	sBmpHeader.creator1 = CREATOR1;
	sBmpHeader.creator2 = CREATOR2;
	sBmpHeader.bmp_offset = sizeof(sBmpHeader) + sizeof(sBmpInfoHeader) + PALETTE_LEN;
	sBmpHeader.filesz = sBmpHeader.bmp_offset + uliBufLength;
	sBmpInfoHeader.header_sz = sizeof(sBmpInfoHeader);
	sBmpInfoHeader.width = pImage->liWidth;
	sBmpInfoHeader.height = pImage->liHeight;
	sBmpInfoHeader.nplanes = 1;
	sBmpInfoHeader.bitspp = 8;
	sBmpInfoHeader.compress_type = 0;
	sBmpInfoHeader.bmp_bytesz = uliBufLength;
	sBmpInfoHeader.hres = HRES;
	sBmpInfoHeader.vres = VRES;
	sBmpInfoHeader.ncolors = 0;
	sBmpInfoHeader.nimpcolors = 0;

	memcpy(rgcBuff, &sBmpHeader, sizeof(sBmpHeader));
	memcpy(&rgcBuff[14], &sBmpInfoHeader, sizeof(sBmpInfoHeader));

	uint32_t uliValPalette = 0;
	uint32_t uliOffset = BMP_HEADER_SIZE;
	for ( int idx = 0; idx < PALETTE_NUM; ++idx )
	{
		uliValPalette = (idx << 16) + (idx << 8) + idx;
		memcpy(rgcBuff+uliOffset, &uliValPalette, sizeof(uint32_t));
		uliOffset += sizeof(uint32_t);
	}

	// add padding
	if ( padBytes != 0 )
	{
		vector<unsigned char> vPadding(padBytes);
		for ( int i = 0; i < pImage->liHeight; ++i )
		{
			auto idx = vImage.begin() + i*(pImage->liWidth+padBytes) + pImage->liWidth;
			vImage.insert(idx, vPadding.begin(), vPadding.end());
		}
	}

	vImage.insert(vImage.begin(), rgcBuff, rgcBuff+BMP_HEADER_SIZE+PALETTE_LEN);

	return 0;
}

enumDeviceType CR8062Link::findConfig(int liClassId, int liSubClassId)
{
	if ( liClassId == LIBUSB_CLASS_COMM )
	{
		return eCDCDevice;
	}
	else if ( liClassId == LIBUSB_CLASS_HID )
	{
		switch ( liSubClassId )
		{
			case HID_DEVICE_SUBCLASS_ID:
				return eHIDDevice;
			break;

			case HID_KEYBOARD_SUBCLASS_ID:
				return eHIDKeyboard;
			break;
		}
	}

	return eNoDevice;
}

int CR8062Link::switchHID2CDC(enumDeviceType eType)
{
	if ( eType == eNoDevice )
	{
		return -1;
	}
	else if ( eType == eCDCDevice )
	{
		m_pLogger->log(LOG_INFO, "CR8062Link::switchHID2CDC: already a CDC class device");
		return 0;
	}

	HIDInterface* pHIDIfr = new HIDInterface();

	bool bRes = pHIDIfr->init(m_nVID, m_nPID, getLogger());
	if ( ! bRes )	return -1;

	int liRes;

	if ( eType == eHIDKeyboard )
	{
		eUSBType = eHIDKeyboard;

		uint8_t prgMsg[9] = {0};
		prgMsg[0] = 0x00;	// report number
		prgMsg[1]  = 0x43;	// Subclass code 0x43
		prgMsg[2]  = 0x03;	// SET_FEATURE or protocol 0x03

		liRes = pHIDIfr->sendFeatureReport(prgMsg, 9);
		if ( liRes < 0 )
		{
			return liRes;
		}

		sleep(SWITCH_CLASS_TIME_SEC);
		pHIDIfr->closeHID();

		// Changing device configuration, the product ID changes!
		enumDeviceType eDevType = autodetectCR8062Config();

		if ( eDevType != eHIDDevice )
		{
			// Unable to switch from HID keyboard to HID device
			return -1;
		}

		eUSBType = eHIDDevice;
		bRes = pHIDIfr->init(m_nVID, m_nPID, getLogger());
		if ( ! bRes )	return -1;
	}

	SAFE_DELETE(m_pFwUpdate)

	m_pFwUpdate = new CR8062FwUpdateManager();
	// now CR8062 is an HID Device - send command to move to CDC mode
	m_pFwUpdate->init(pHIDIfr, getLogger(), m_Camera.getCamErr());
//	m_pFwUpdate->sendCmd("CMMOSCMUC");
	m_pFwUpdate->sendCmd("CMMOPCMUC"); // To preserve flash memory - should be platform?
	if ( ! m_pFwUpdate->checkIfAccepted(100) )	return -1;

	sleep(SWITCH_CLASS_TIME_SEC);

	eUSBType = eCDCDevice;

	SAFE_DELETE(m_pFwUpdate);
	SAFE_DELETE(pHIDIfr);

	return liRes;
}

int CR8062Link::switchCDC2HID(enumDeviceType eOldConfig)
{
	switch ( eOldConfig )
	{
		case eHIDDevice:
			m_pLogger->log(LOG_INFO, "CR8062Link::switchCDC2HID: device already in HID class");
			eUSBType = eHIDDevice;
		break;

		case eHIDKeyboard:
		{
			HIDInterface *pHIDIfr = new HIDInterface();

			bool bRes = pHIDIfr->init(m_nVID, m_nPID, getLogger());
			if ( ! bRes )
			{
				return -1;
			}

			uint8_t prgMsg[9] = {0};
			prgMsg[0] = 0x00;	// report number
			prgMsg[1]  = 0x43;	// Subclass code 0x43
			prgMsg[2]  = 0x03;	// SET_FEATURE or protocol 0x03

			int liRes = pHIDIfr->sendFeatureReport(prgMsg, 9);
			if ( liRes < 0 )
			{
				m_pLogger->log(LOG_INFO, "CR8062Link::switchCDC2HID: unable to send feature report");
				return liRes;
			}

			eUSBType = eHIDKeyboard;
			sleep(SWITCH_CLASS_TIME_SEC);
			SAFE_DELETE(pHIDIfr);
		}
		break;

		case eCDCDevice:
			m_Camera.switch2HIDclass();
			sleep(SWITCH_CLASS_TIME_SEC);
			eUSBType = eHIDDevice;
		break;

		default:
			m_pLogger->log(LOG_INFO, "CR8062Link::switchCDC2HID: no device detected");
			return -1;
		break;
	}

	return 0;
}

bool CR8062Link::getVidPid(void)
{
	libusb_device **devs;

	m_nInterfClass = -1;
	m_nInterfSubClass = -1;
	m_nVID = -1;
	m_nPID = -1;

	if ( hid_init() < 0 )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::autodetectConfig: unable to init hid library");
                return false;
	}

	ssize_t iDevNum = libusb_get_device_list(usb_contextt, &devs);
	if ( iDevNum < 0)
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::autodetectConfig: no usb device found");
                return false;
	}

	libusb_device *dev;
	struct libusb_config_descriptor *confDesc = NULL;

	int i = 0;
	while ( ( dev = devs[i++] ) != NULL )
	{
		struct libusb_device_descriptor desc;

		int liRes = libusb_get_device_descriptor(dev, &desc);
		if ( liRes )
		{
			continue;
		}
		if ( desc.idVendor != CR8062_VID )
		{
			continue;
		}
		m_nVID = desc.idVendor;
		m_nPID = desc.idProduct;

		liRes = libusb_get_config_descriptor(dev, 0, &confDesc);
		if ( liRes )
		{
			continue;
		}
		m_nInterfClass = confDesc->interface->altsetting->bInterfaceClass;
		m_nInterfSubClass = confDesc->interface->altsetting->bInterfaceSubClass;

		if ( m_nVID != -1 && m_nPID != -1 )
		{
			break;
		}
	}

	if ( m_nVID == -1 && m_nPID == -1 )
	{
		return false;
	}

	return true;
}

bool CR8062Link::reInitAfterFwUpdate(const string& strFWVersionUpd, USBInterface* pUSBIfr)
{
	if ( pUSBIfr == 0 )	return false;

	if ( switchHID2CDC(eUSBType) == -1 )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::reInitAfterFwUpdate: unable to set devide to CDC class");
		return false;
	}
	m_pLogger->log(LOG_INFO, "CR8062Link::reInitAfterFwUpdate: device set to CDC class");

	// Now the device is set in CDC class.
	if ( pUSBIfr->openInterface() )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::reInitAfterFwUpdate: unable to open USB Interface");
		return false;
	}

	m_Camera.init(pUSBIfr, m_pLogger);
	m_Reader.init(pUSBIfr, m_pLogger);

	string strFWVersion;
	m_Camera.getFWversion(strFWVersion);

	if ( strFWVersion.empty() )
	{
		m_pLogger->log(LOG_ERR, "CR8062Link::reInitAfterFwUpdate: unable to comunicate with the camera");
		return false;
	}

	if ( ! strFWVersion.compare(strFWVersionUpd) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool CR8062Link::compareWithWildCard(const string& str1, const string& str2, char cWildCardChar)
{
	if ( str1.size() != str2.size() )	return false;

	int liSize = str1.size();

	for ( int i = 0; i != liSize; i++ )
	{
		if ( str1[i] != cWildCardChar )
		{
			if ( str1[i] != str2 [i] )
			{
				return false;
			}
		}
	}
	return true;
}

int CR8062Link::enlargeYourCrop(structRect* pReal, structRect* pNew, int liDeltaX, int liDeltaY)
{
	if ( pReal == nullptr )	return -1;
	if ( pNew == nullptr )	return -1;
	if ( pReal->x+pReal->width > MAX_WIDTH/2 )	return -1;
	if ( pReal->y+pReal->height > MAX_HEIGHT )	return -1;

	*pNew = *pReal;
	pNew->x -= liDeltaX;
	pNew->y -= liDeltaY;
	pNew->width += (liDeltaX*2);
	pNew->height += (liDeltaY*2);

	if ( pNew->x <= 0 )							pNew->x = 1;
	if ( pNew->y <= 0 )							pNew->y = 1;
	if ( pNew->x+pNew->width > MAX_WIDTH/2 )	pNew->width = MAX_WIDTH/2 - pNew->x - 1;
	if ( pNew->y+pNew->height > MAX_HEIGHT )	pNew->height = MAX_HEIGHT - pNew->y - 1;


//	if ( pNew->x <= 0 )
//	{
//		pNew->width -= ( 2* pNew->x );
//		pNew->x = 1;
//	}

//	if ( pNew->y <= 0 )
//	{
//		pNew->height += ( 2* pNew->y );
//		pNew->y = 1;
//	}

//	if ( pNew->x+pNew->width > MAX_WIDTH/2 )
//	{
//		int liDiff = MAX_WIDTH/2 - ( pReal->x + pReal->width);
//		pNew->x = pReal->x - liDiff;
//		pNew->width = pReal->width + 2 * liDiff;
//	}

//	if ( pNew->y+pNew->height > MAX_HEIGHT )
//	{
//		int liDiff = MAX_HEIGHT - ( pReal->y + pReal->height);
//		pNew->y = pReal->y - liDiff;
//		pNew->height = pReal->height + 2 * liDiff;
//	}

	return 0;
}

int CR8062Link::enlargeYourCrop_CW(structRect* pReal, structRect* pNew, int liDeltaX, int liDeltaY)
{
	if ( pReal == nullptr )	return -1;
	if ( pNew == nullptr )	return -1;	
	if ( pReal->x+pReal->width > MAX_WIDTH/2 )	return -1;
	if ( pReal->y+pReal->height > MAX_HEIGHT )	return -1;

	*pNew = *pReal;
	pNew->x -= liDeltaX;
	pNew->y -= liDeltaY;
	pNew->width += (liDeltaX*2);
	pNew->height += (liDeltaY*2);

	if ( pNew->x <= 0 )							pNew->x = 1;
	if ( pNew->y <= 0 )							pNew->y = 1;
	if ( pNew->x+pNew->width > MAX_WIDTH/2 )	pNew->width = MAX_WIDTH/2 - pNew->x - 1;
	if ( pNew->y+pNew->height > MAX_HEIGHT )	pNew->height = MAX_HEIGHT - pNew->y - 1;

	return 0;
}

bool CR8062Link::readBmp(unsigned char*prgData, string strFileName)
{
	unsigned char nImageHeader[BMP_HEADER_SIZE];

	FILE* picture = fopen(strFileName.c_str(), "rb"); // rb means: read as a binary file
	if ( picture == nullptr )
	{
		return false;
	}

	if ( fread(nImageHeader, sizeof(unsigned char), BMP_HEADER_SIZE, picture) != BMP_HEADER_SIZE )
	{
		printf("Image not succesfully read. \nBe careful, the name can be wrong.\n");
		return false;
	}

	int size = getVal4((char*)&nImageHeader[2]);
	int bfOffBits = getVal4((char*)&nImageHeader[10]);
	int width = getVal4((char*)&nImageHeader[18]);
	int height = getVal4((char*)&nImageHeader[22]);

	unsigned char* raw_data  = new unsigned char[size];
	fread(raw_data, sizeof(unsigned char), size, picture);

	fclose(picture);

	raw_data += ( bfOffBits - BMP_HEADER_SIZE );

	// Need 2 take care about the padding
	int padBytes;

	if ( width%4 == 0 )
	{
		padBytes = 0;
	}
	else
	{
		padBytes = 4 - width%4;
	}

	width += padBytes;

	// fRead goes from left to right and from bottom to top, while Matlab and the camera
	// go from left to right and from top to bottom.

	int idx, counter = 0;
	for (int idxH = (height-1); idxH >= 0; --idxH)
	{
		for (int idxW = 0; idxW < width-padBytes; ++idxW)
		{
			idx = (idxH * width) + idxW;

			*(prgData+counter) = *(raw_data+idx);
			++counter;
		}
	}
	// this is actually needed because the delete has to have the correct start
	raw_data -=  (bfOffBits - BMP_HEADER_SIZE );
	delete[] raw_data;
	return true;
}

void CR8062Link::swapY(vector<unsigned char>& vImage, int liImageWidth, int liImageHeight, int liTimes)
{
	if ( liTimes >= liImageHeight/2)    return;

	auto begin = vImage.begin() + liTimes * liImageWidth;
	auto end = begin + liImageWidth;
	auto beginLast = vImage.end() - liImageWidth*(liTimes+1);
	swap_ranges(begin, end, beginLast);

	return swapY(vImage, liImageWidth, liImageHeight, ++liTimes);
}
