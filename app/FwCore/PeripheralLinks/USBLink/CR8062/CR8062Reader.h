/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Reader.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CR8062Reader class.
 @details

 ****************************************************************************
*/

#ifndef CR8062READER_H
#define CR8062READER_H

#include <unordered_map>

#include "CR8062Base.h"
#include "TimeStamp.h"

// Note: if no batch has been sent before, we need to send setDecodingTimeMsec first

class CR8062Reader : public CR8062Base
{
	private:

		struct structByPass	{ int gain; vector<int> vExposure; vector<int> vIllumination; };
		struct structCounter { int retryCnt; int sprCnt; int trayCnt; int cwCnt; int bypassCnt; };
		enum class eTarget { eBarcode, eDataMatrix };
		int m_nDecodeTimeOutMsec;

	public:

		/*! **********************************************************************************************************************
		 * @brief CR8062Reader constructor. Initialize all the members.
         * @param liPayloadSize payload size
		 * ***********************************************************************************************************************
		 */
		CR8062Reader(int liPayloadSize = DEF_READER_PAYLOAD_SIZE);

		/*! **********************************************************************************************************************
		 * @brief ~CR8062Reader virtual destructor.
		 * ***********************************************************************************************************************
		 */
		virtual ~CR8062Reader();

		/*! **********************************************************************************************************************
		 * @brief  init initialize reader.
		 * @param  pUSBIfr pointer to an USBInterface object.
         * @param  pLogger pointer to the logger.
         * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		virtual bool init(USBInterface* pUSBIfr, Log* pLogger);

		/*! **********************************************************************************************************************
		 * @brief  getLinearBarcode used to retrieve the linear barcode. A checksum control is performed to find out if the reading
		 *		   outcome is correct or not.
		 * @param  strOutLinBarcode is the string where the code of the barcode will be saved.
		 * @param  bIsBmx if true, then perform the barcode check (only for bioMerieux barcodes)
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int getLinearBarcode(string& strOutLinBarcode, bool bIsBmx = true);

		/*! *********************************************************************************************************************
		 * @brief  getDataMatrix used to retrieve the data Matrix.
		 * @param  strOutDataMatrix string where the data matrix's code will be saved.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int getDataMatrix(string& strOutDataMatrix);

		/*! **********************************************************************************************************************
		 * @brief  setROI used to set the area where the barcode is expected to be. You have to set WIDE FIELD or
		 * @param  x0 - crop top left origin, x coordinate. 0 is not allowed.
		 * @param  y0 - crop top left origin, y coordinate. 0 is not allowed
		 * @param  usiWidth is the width of the window.
		 * @param  usiHeight is the height of the window.
		 * @param  uliTimeoutMsec is the timeout for the USB read, equal to eGetRoiTimeOutMsec by default.
         * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setROI(uint16_t x0, uint16_t y0, uint16_t usiWidth, uint16_t usiHeight, uint32_t uliTimeoutMsec = eGetRoiTimeOutMsec);

		/*! **********************************************************************************************************************
         * @brief  setDecoderFOI used to set the Field Of Interest for barcode/data matrix decoding.
		 * @param  ucField can only be FOI_HIGH_DENSITY_FIELD, FOI_WIDE_FIELD or FOI_ENTIRE_IMAGE.
		 * @param  uliTimeoutMsec is the timeout for the USB to read, equal to eGetFoiTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setDecoderFOI(uint8_t ucField, uint32_t uliTimeoutMsec = eGetFoiTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  setDecodingTimeMsec set max duration of decoding attempts in msec.
		 * @param  uliMaxReadingTimeMsec max time of reading.
		 * @param  uliTimeoutMsec is the timeout for the USB read, equal to eGetDataMatrixTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setDecodingTimeMsec(uint32_t uliMaxReadingTimeMsec, uint32_t uliTimeoutMsec = eGetDataMatrixTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  getDecodingTimeMsec get decoding time the camera is using right now.
		 * @param  uliMaxReadingTimeMsec int where the value got will be stored.
		 * @param  uliTimeoutMsec is the timeout for the USB read, equal to eGetDataMatrixTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int getDecodingTimeMsec(int32_t& uliMaxReadingTimeMsec, uint32_t uliTimeoutMsec = eGetDataMatrixTimeOutMsec);

		// setter and getter to be used only when parsing bath files
		/*! **********************************************************************************************************************
         * @brief setDecodingTimeoutMs set value to internal member (no msg sent to the camera).
		 * @param liDecodeTimeoutMsec decodiing value to be set.
		 * ***********************************************************************************************************************
		 */
		void setDecodingTimeoutMs(int liDecodeTimeoutMsec);

		/*! **********************************************************************************************************************
         * @brief  getDecodingTimeoutMs read of internal member (no msg sent to the camera)
		 * @return decoding timeout
		 * ***********************************************************************************************************************
		 */
        int getDecodingTimeoutMs(void);

		int _debug_DM_read(string& strDataMatrix, uint8_t ucSection);


	private:

		/*! **********************************************************************************************************************
		 * @brief  checksumControl compute the checksum (module 37) over the string passed as argument.
		 * @param  strToBeChecked string where the check is applied.
		 * @return true if success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool checksumControl(string& strToBeChecked);

		/*! **********************************************************************************************************************
		 * @brief clearPayload
		 * ***********************************************************************************************************************
		 */
		void clearPayload();

		void parseRoiSettings(bool* bEnabled, int* x0, int* y0, int* siWidth, int* siHeight);

		int recoverStrategy(string& strItem, eTarget eDevice);

		int getParameters(unordered_map<string, vector<int>>& mData);

		int updateResultsFile(string& strSequence, const structCounter& myCnt, string&strResult,
							  double dMsec, vector<logAGCparameters>&vAgcLog);
};

#endif // CR8062READER_H
