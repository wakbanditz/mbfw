/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Camera.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CR8062Camera class.
 @details

 ****************************************************************************
*/

#ifndef CR8062CAMERA_H
#define CR8062CAMERA_H

#include "CR8062Base.h"


struct rect { int x0; int y0; int width; int height; };

class CR8062Camera : public CR8062Base
{
	public:

		/*! **********************************************************************************************************************
		 * @brief CodeCorpCR8062 default constructor.
         * @param liPayloadSize the size of the camera payload
		 * ***********************************************************************************************************************
		 */
		CR8062Camera(int liPayloadSize = DEF_CAMERA_PAYLOAD_SIZE);

		/*! **********************************************************************************************************************
		 * @brief ~CodeCorpCR8062 virtual destructor of the class.
		 * ***********************************************************************************************************************
		 */
		virtual ~CR8062Camera();

		/*! **********************************************************************************************************************
		 * @brief  init initialize camera.
		 * @param  pUSBIfr pointer to an USBInterface object.
         * @param pLogger poiter to the logger object
         * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		virtual bool init(USBInterface* pUSBIfr, Log* pLogger);

		/*! **********************************************************************************************************************
		 * @brief  getPicture command used to get a picture.
		 * @param  vcImage vectore where the image will be stored
         * @param pInfoImage
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int getPicture(vector<unsigned char>& vcImage, structInfoImage* pInfoImage);

		/*! **********************************************************************************************************************
		 * @brief  getFWversion is used to ask the camera the version of its FW
		 * @param  strOutFwVersion is the string where the FW version will be saved.
		 * @param  uliTimeoutMsec is the timeout for the read, equal to eGetFwVersionTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int getFWversion(string& strOutFwVersion, uint32_t uliTimeoutMsec = eGetFwVersionTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  setFOI used to set the Field Of Interest for barcode/data matrix decoding.
		 * @param  ucField can only be FOI_HIGH_DENSITY_FIELD, FOI_WIDE_FIELD or FOI_ENTIRE_IMAGE.
		 * @param  uliTimeoutMsec is the timeout for the USB to read, equal to eGetFoiTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setPictureFOI(uint8_t ucField, uint32_t uliTimeoutMsec = eGetFoiTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  setAGCIterations set the number of iterations where the AGC makes its evaluations.
		 * @param  ucNum num of iterations desired.
		 * @param  uliTimeoutMsec is the timeout for USB read.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setAGCIterations(uint8_t ucNum, uint32_t uliTimeoutMsec = eGetFoiTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  reconfigureAGCCurves reconfigure curves of AGC as desired
		 * @param  pValueTable pointer to the table with the curves wanted
		 * @param  ucWellNum used to give default curves depending on the well
		 * @param  uliTimeoutMsec is the timeout for USB read.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int reconfigureAGCCurves(uint32_t* pValueTable, uint8_t ucWellNum, uint32_t uliTimeoutMsec = eGetAGCTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief switch2HIDclass command to switch the camera to HID class
		 * @param uliTimeoutMsec is the timeout for USB read.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int switch2HIDclass(uint32_t uliTimeoutMsec = eSwitch2HIDTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief switch2CDCclass command to switch the camera to CDC class
		 * @param uliTimeoutMsec is the timeout for USB read.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int switch2CDCclass(uint32_t uliTimeoutMsec = eSwitch2CDCTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  saveBMPimage used to save the picture sent from the camera in bmp format.
		 * @param  pcBuff pointer to the payload.
		 * @param  uliBufLength length of the data to be saved.
		 * @param  uliWidth of the image we want to obtain.
		 * @param  uliHeight of the image we want to obtain.
		 * @param  strFileName name of the image that will be saved.
		 * @return true if succeeded, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool saveBMPimage(uint8_t* pcBuff, uint32_t uliBufLength, uint32_t uliWidth, uint32_t uliHeight, string strFileName);

		/*! **********************************************************************************************************************
		 * @brief swapY allowes you to swap Y coordinates (useful before saving bmp images).
		 * @param vImage vector with the image to be swapped
		 * @param liImageWidth width of the image
		 * @param liImageHeight height of the image
		 * @param liTimes used with recursion, has to be 0
		 * ***********************************************************************************************************************
		 */
		void swapY(vector<unsigned char>& vImage, int liImageWidth, int liImageHeight, int liTimes = 0);

		/*! **********************************************************************************************************************
		 * @brief  saveJPEGimage save the image as JPEG. NOTE: vImage is what received from the camera, aka a JPEG image with all
		 *		   the headers and format needed. The function only saves the data in a file, without any changes.
		 * @param  vImage image already in JPEG format.
		 * @param  strFileName name of the image that will be saved.
		 * @return true if succeeded, false otherwise.
		 * ***********************************************************************************************************************
		 */
		bool saveJPEGimage(const vector<unsigned char>& vImage, const string& strFileName);

		/*! **********************************************************************************************************************
		 * @brief  getPicturePayload get the picture payload.
		 * @return pointer to the payload.
		 * ***********************************************************************************************************************
		 */
		uint8_t* getPicturePayload();

		/*! **********************************************************************************************************************
		 * @brief setImageMode set Image decoder in BMP or RAW mode.
		 * @param eFormat can be eJPEG or eBMP.
		 * @param uliTimeoutMsec is the timeout for USB read.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setImageMode(eImageFormat eFormat, uint32_t uliTimeoutMsec = eGetDefaultTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief getImageType get if the image is BMP or JPEG.
		 * @param eFormat where the format is saved.
		 * @param uliTimeoutMsec is the timeout for USB read.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int getImageMode(eImageFormat& eFormat, uint32_t uliTimeoutMsec = eGetDefaultTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  setCropCoordinates send the camera the desired cropWindow.
		 * @param  x0 - crop top left origin, x coordinate. Can be from 0 to 1280.
		 * @param  y0 - crop top left origin, y coordinate. Can be from 1 to 960.
		 * @param  usiWidth width of the image taken.
		 * @param  usiHeight height of the image taken.
		 * @param  uliTimeoutMsec is the timeout for the USB read, equal to eGetCropCoordTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setCropCoordinates(uint16_t x0, uint16_t y0, uint16_t usiWidth, uint16_t usiHeight,  uint32_t uliTimeoutMsec = eGetCropCoordTimeOutMsec);

		int getCropCoordinates(int* pX0, int* pY0, int* pWidth, int* pHeight, bool* bIsEnabled, uint32_t uliTimeoutMsec = eGetCropCoordTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief disableCrop tell the camera to disable crop
		 * @param uliTimeoutMsec is the timeout for USB read.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int disableCrop(uint32_t uliTimeoutMsec = eGetDefaultTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief setJPEGQuality set the JPEG quality when the format will be set to JPEG
		 * @param ucJPEGQuality quality val (0-100)
		 * @param uliTimeoutMsec is the timeout for USB read.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setJPEGQuality(uint8_t ucJPEGQuality, uint32_t uliTimeoutMsec = eGetDefaultTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief camRouteCmd cam route command. Send any commands and receive the related ans.
		 * @param strCmd command we want to send.
         * @param vAns response we get
		 * @param uliTimeoutMsec is the timeout for USB read.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
        int camRouteCmd(string &strCmd, vector<unsigned char> &vAns, uint32_t uliTimeoutMsec);


		// Getter commands
		/*! **********************************************************************************************************************
		 * @brief  getImageFormat get the format of the image (no command sent).
		 * @return the variable stored as class member.
		 * ***********************************************************************************************************************
		 */
		eImageFormat getImageFormat(void);

		/*! **********************************************************************************************************************
		 * @brief setImageFormat set the format of the image (no command sent).
		 * @param eFormat format to update class member.
		 * ***********************************************************************************************************************
		 */
		void setImageFormat(eImageFormat eFormat);


		int getReady4Videos(int liQuality, uint32_t uliTimeoutMsec = eGetReady4VideosMsec);
		int restoreDefaultAfterVideo(uint32_t uliTimeoutMsec = eRestoreDefaultMsec);
		int _debug_loop(int liQuality, int liLoopsNum);

        int showROI(vector<unsigned char>& vImage, structInfoImage *pInfoImage, uint32_t uliTimeoutMsec = eGetDefaultTimeOutMsec);
		int showCross(vector<unsigned char>& vImage, structInfoImage *pInfoImage);


	private:

		/*! **********************************************************************************************************************
		 * @brief  parsePicturePayload parse picture payload removing descriptors.
		 * @param  liImageSize size of the image.
		 * @param  vcData vector where the image is stored.
		 * @return 0 in case of success, -1 otherwise.
		 * ***********************************************************************************************************************
		 */
		int parsePicturePayload(structInfoImage* image, vector<unsigned char>& vcData);

		int parseCoordParam(bool* bEnabled, int* X0, int* Y0, int* siWidth, int* siHeight);

        // x0 is the leftmost pixel, yo is the topmost pixel
        int parseRoiParam(int* X0, int* Y0, int* siWidth, int* siHeight);

		void getROIDrawCoord(rect& roi, rect& crop, bool bCropEnabled, rect& draw);


    private:

		eImageFormat m_eImageFormat;
};

#endif // CR8062CAMERA_H
