/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CR8062Base.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CR8062Base class.
 @details

 ****************************************************************************
*/

#ifndef CR8062BASE_H
#define CR8062BASE_H

#include <sstream>


#include "CR8062Protocol.h"

class CR8062Base : public CR8062Protocol
{
	public:

		/*! **********************************************************************************************************************
		 * @brief CR8062Base default constructor.
		 * ***********************************************************************************************************************
		 */
		CR8062Base();

		/*! **********************************************************************************************************************
		 * @brief ~CodeCorpCR8062 virtual destructor of the class.
		 * ***********************************************************************************************************************
		 */
		virtual ~CR8062Base();

		/*! **********************************************************************************************************************
		 * @brief  init initialize camera.
		 * @param  pUSBIfr pointer to an USBInterface object.
         * @param pLogger poiter to the logger object
		 * @return true in case of success, false otherwise.
		 * ***********************************************************************************************************************
		 */
		virtual bool init(USBInterface* pUSBIfr, Log* pLogger);

		/*! **********************************************************************************************************************
		 * @brief  setFullSpeedCommunication enable or disable fullSpeed communication
		 * @param  bIsFullSpeed true if we want full speed comm, false otherwise.
         * @param uliTimeoutMsec timeout in msecs
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setFullSpeedCommunication(bool bIsFullSpeed, uint32_t uliTimeoutMsec = eFullSpeedTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  savePlatformSettings save setting as platform one (unchangeable with CFR command)
		 * @param  strCmd string related to the command to be saved as platform one
		 * @param  uliTimeoutMsec is the timeout for the USB read
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int savePlatformSettings(string& strCmd, uint32_t uliTimeoutMsec = eSavePlatformSettingsMsec);

		/*! **********************************************************************************************************************
		 * @brief  setContrastWindow used to set the region where the AGC will compute its calculation.
		 * @param  x0 - crop top left origin, x coordinate.
		 * @param  y0 - crop top left origin, y coordinate.
		 * @param  usiWidth is the width of the window.
		 * @param  usiHeight is the height of the window.
		 * @param  uliTimeoutMsec is the timeout for the USB read, equal to eGetContrWindTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setContrastWindow(int x0, int y0, int usiWidth, int usiHeight, uint32_t uliTimeoutMsec = eGetContrWindTimeOutMsec);
		int setY0ContrastWindow(int y0, uint32_t uliTimeoutMsec = eFast);

		int getContrastWindow(int*x0, int*y0, int*liWidth, int*liHeight, bool* bEnabled, uint32_t uliTimeoutMsec = eGetContrWindTimeOutMsec);


		/*!* AGC handling ***/

		/*! **********************************************************************************************************************
		 * @brief  setAGCnormalMode used to enable the AGC.
		 * @param  uliTimeoutMsec is the timeout for the USB read, equal to eGetAGCTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setAGCnormalMode(uint32_t uliTimeoutMsec = eGetAGCTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  setAGCparam used to use the AGC in by-pass mode, setting manually its parameters.
		 * @param  ucLux by passes illumination setting and set this value (range 0-MAX_ILLUMINATION).
         * @param  usiExpositionTime by passes exposure setting and set this value (range 0-MAX_EXPOSURE).
		 * @param  ucGain by passes gain setting and set this value (range 0-MAX_GAIN).
		 * @param  uliTimeoutMsec is the timeout for the USB read, equal to eGetAGCTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setAGCparam(uint8_t ucLux, uint32_t usiExpositionTime, uint8_t ucGain, uint32_t uliTimeoutMsec = eGetAGCTimeOutMsec);

		int getAGCparam(char*pcMode, int*pIllum, int*pGain, int*pExp, uint32_t uliTimeoutMsec = eGetAGCTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  enableLogAGCparameters used to enable or disable the log of the parameters. Must be sent before taking a
		 *		   picture. CodeCorp suggest to use logging only for debug purpose, because can slow down the image capture process.
         * @param  bEnable if 1 enable logging, disable otherwise.
		 * @param  uliTimeoutMsec is the timeout for the USB read, equal to eGetAGCTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int enableLogAGCparameters(bool bEnable, uint32_t uliTimeoutMsec = eGetAGCTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  clearLogAGCparameters if this functin is not called, than getLogAGCparameters returns all the parameters of the
		 *		   last n acquisition
		 * @param  uliTimeoutMsec is the timeout for the USB read, equal to eGetAGCTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int clearLogAGCparameters(uint32_t uliTimeoutMsec = eGetAGCTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  getLogAGCparameters get the settings of the AGC used in the LAST image.
         * @param  vAGCStruct pointer to logAGCparameters struct, where will be stored all the settings.
		 * @param  uliTimeoutMsec is the timeout for the USB read, equal to eGetAGCTimeOutMsec by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int getLogAGCparameters(vector<logAGCparameters>& vAGCStruct, uint32_t uliTimeoutMsec = eGetAGCTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  getReadyForDecodingOrImageCapturing used to set packed mode communication and set raw data option.
		 * @param  uliTimeoutMsec is the timeout for the read, equal to eGetReadyTimeout by default.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setPacketMode(uint32_t uliTimeoutMsec = eGetReadyTimeOutMsec);

		/*! **********************************************************************************************************************
		 * @brief  setCustomParam function used to send the set param command desired.
		 * @param  strCmd string containing the cmd.
		 * @param  uliTimeoutMsec timeoude for the read.
		 * @return error code.
		 * ***********************************************************************************************************************
		 */
		int setCustomParam(string& strCmd, uint32_t uliTimeoutMsec);

	protected:

		/*! **********************************************************************************************************************
		 * @brief  checkIfAnsCorrectly check if the answer received is correct (as expected) depending on the request
		 * @param  eType depends on what type the command is (get, set, action)
		 * @param  uliTimeoutMsec is the timeout for the read
		 * @param  strClassName name of the class for logger
		 * @param  strFuncName name of the function that called this one for the logger
		 * @return 0 in case of success, -ErrorCode otherwise
		 * ***********************************************************************************************************************
		 */
        int checkIfAnsCorrectly(enumRequestType eType, uint32_t uliTimeoutMsec,
                                const char* strClassName = __PRETTY_FUNCTION__, const char* strFuncName = __builtin_FUNCTION());

		/*! **********************************************************************************************************************
		 * @brief wrapStoi wrapper for the stoi function
		 * @param str
		 * @param pVal
		 * @param pos
		 * @param base
		 * @return integer value
		 * ***********************************************************************************************************************
		 */
		int wrapStoi(const string& str, int* pVal, size_t* pos = 0, int base = 10);

	private:

		/*! **********************************************************************************************************************
		 * @brief updateAGCparams used in the parser, to parserize the LOG message.
         * @param  vAGCStruct pointer to logAGCparameters struct, where will be stored all the settings.
         * ***********************************************************************************************************************
		 */
		bool updateAGCparams(vector<logAGCparameters>& vAGCStruct);

		/*! **********************************************************************************************************************
		 * @brief getValueAfterFirstOccurrenceInString used to get the number after a substring in a string
		 * @param strToBeFound substring
		 * @param strFull full string
		 * @return value found
		 * ***********************************************************************************************************************
		 */
		int getValueAfterFirstOccurrenceInString(string strToBeFound, const string& strFull);


		void parseAGCParam(char* pcMode, int* pIllum, int* pGain, int* pExp);
		void parseCWParams(bool* bEnabled, int* x0, int* y0, int* siWidth, int* siHeight);

        /*! **********************************************************************************************************************
         * @brief findErrorCode returns the error code corresponding to a string
         * @param strAnsw the error string
         * @return the error code
         * ***********************************************************************************************************************
         */
        int findErrorCode(string& strAnsw);

    protected:

        string						m_strAnswer;

};

#endif // CR8062BASE_H
