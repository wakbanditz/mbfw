/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    USBLinkCommonInclude.h
 @author  BmxIta FW dept
 @brief   Contains the defines for the USB classes.
 @details

 ****************************************************************************
*/

#ifndef USBLINKCOMMONINCLUDE_H
#define USBLINKCOMMONINCLUDE_H


// Config keys - Camera reader

#define CFG_USB_CDC_BASE_DIR_1			1
#define CFG_USB_CDC_BITS_PER_WORD_1		2
#define CFG_USB_CDC_PARITY_1			3
#define CFG_USB_CDC_HAS_RTS_1			4
#define CFG_USB_CDC_HAS_XON_1			5




/*! *******************************************************************************************************************
 * NSH Reader management
 * ********************************************************************************************************************
 */

#define CAMERA_USB_BUS_NAME				"/dev/ttyACM0"
#define CAMERA_USB_BITS_PER_WORD		"8"
#define CAMERA_USB_PARITY				"N"
#define CAMERA_USB_HAS_RTS				"0"
#define CAMERA_USB_HAS_XON				"0"



#endif // USBLINKCOMMONINCLUDE_H
