/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SprDetectionEllipse.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SprDetectionEllipse class.
 @details

 ****************************************************************************
*/

#include <numeric>
#include <cmath>

#include "SprDetectionEllipse.h"

#define ELLIPSE_MIN_AXIS_SIZE		120
#define ELLIPSE_WAVES_DELTA			2500
#define ELLIPSE_PIXEL_THRESH		90
#define HOLDER_CROWN_HALF_PIXELS	10

SprDetectionEllipse::SprDetectionEllipse()
{
	m_nWidth = -1;
	m_nHeight = -1;
	m_Edge = {};
	m_pLogger = nullptr;
	m_bConfigOk = false;
	m_pvImage = nullptr;
	m_dCoordMinus.clear();
	m_dCoordPlus.clear();
}

SprDetectionEllipse::~SprDetectionEllipse()
{

}

int SprDetectionEllipse::init(vector<unsigned char>* pvImage, Log* pLogger, uint32_t uliWidth, uint32_t uliHeight)
{
	if ( pvImage == nullptr )	return -1;
	if ( pLogger == nullptr )	return -1;

	m_pLogger = pLogger;
	m_pvImage = pvImage;
	m_nWidth = int(uliWidth);
	m_nHeight = int(uliHeight);

	m_bConfigOk = true;

	return 0;
}

int SprDetectionEllipse::perform(const stripEdgesStruct* Edge, eSprResults& eRes)
{
	// Unknown yet
	eRes = eSprResults::eUnknown;

	if ( ! m_bConfigOk )	return -1;
	if ( Edge == nullptr )	return -1;

	m_Edge = *Edge;

	vector<unsigned int> vVerticalWaves(m_nHeight);
	vector<unsigned int> vHorizontalWaves(m_nWidth);
	int liRes = getWaves(m_pvImage, vVerticalWaves, vHorizontalWaves);
	if ( liRes )
	{
		eRes = eSprResults::eError;
		m_pLogger->log(LOG_ERR, "SprDetectionEllipse::perform: impossible to create waves");
		return -1;
	}

	// Find bowndaries
	infoEllipseStruct infoEllipse;
	getEllipseInfo(vVerticalWaves, vHorizontalWaves, &infoEllipse);
	if ( infoEllipse.fAxisX < ELLIPSE_MIN_AXIS_SIZE || infoEllipse.fAxisY < ELLIPSE_MIN_AXIS_SIZE )
	{
		eRes = eSprResults::eEllipseOutOfLimits;
		m_pLogger->log(LOG_ERR, "SprDetectionEllipse::perform: ellipse has too small axis,"
								"axis x [%d] axis y [%d]", infoEllipse.fAxisX, infoEllipse.fAxisY);
		return 0;
	}

	// Calculate ellipse
	int liXStart = infoEllipse.fCenterX - infoEllipse.fAxisX/2.0f;
	int liXEnd = liXStart + infoEllipse.fAxisX;

	vector<point<float>> vCoordMinus(liXEnd-liXStart+1);
	vector<point<float>> vCoordPlus(liXEnd-liXStart+1);
	for ( int i = liXStart; i <= liXEnd; ++i )
	{
		getEllipseCoord(&infoEllipse, i, &vCoordMinus[i-liXStart], &vCoordPlus[i-liXStart]);
	}

	// Find if the coordinates found matches the ellipse
	float fRes = 0.0f;
	fRes = calculateEllipseSimilitude(vCoordMinus, vCoordPlus);
	if ( fRes > 50.0f )
	{
		eRes = eSprResults::eEllipseMatches;
	}
	else
	{
		eRes = eSprResults::eNoMatch;
	}

	return 0;
}

const deque<point<int>>& SprDetectionEllipse::getIntCoordMinus()
{
	return m_dCoordMinus;
}

const deque<point<int>>& SprDetectionEllipse::getIntCoordPlus()
{
	return m_dCoordPlus;
}

int SprDetectionEllipse::getWaves(vector<unsigned char>* pvImage, vector<uint32_t>& vVertWaves, vector<uint32_t>& vHorizWaves)
{
	if ( pvImage == nullptr )	return -1;

	vVertWaves.resize(m_nHeight);
	vHorizWaves.resize(m_nWidth);

	for ( int i = 0; i < m_nHeight; ++i )
	{
		vVertWaves[i] = accumulate(&m_pvImage->at(i*m_nWidth), &m_pvImage->at(i*m_nWidth)+m_nWidth, 0);
	}

	// Do not consider the part with the label for horizontal waves
	for ( int i = 0; i < m_nWidth; ++i )
	{
		for ( int j = 0; j < m_nHeight; j++ )
		{
			if ( m_Edge.liLowerEdge == 0 || m_Edge.liUpperEdge == 0 )
			{
				vHorizWaves[i] += m_pvImage->at(j*m_nWidth+i);
			}
			else if ( j < m_Edge.liLowerEdge || j > m_Edge.liUpperEdge )
			{
				vHorizWaves[i] += m_pvImage->at(j*m_nWidth+i);
			}
		}
	}

	// linearize the part of the image cut
	if ( m_Edge.liLowerEdge != 0 )
	{
		int liVal0 = vVertWaves[m_Edge.liLowerEdge-1];
		int liVal1 = vVertWaves[m_Edge.liUpperEdge+1];
		for ( auto c = m_Edge.liLowerEdge; c < m_Edge.liUpperEdge; ++c )
		{
			if ( c < m_Edge.liLowerEdge+10 )
			{
				vVertWaves[c] = liVal0 - abs((c-m_Edge.liLowerEdge)*(liVal1-liVal0)/10);
			}
			else
			{
				vVertWaves[c] = liVal1;
			}
		}
	}

	return 0;
}

int SprDetectionEllipse::getEllipseInfo(vector<uint32_t>& vVertWaves, vector<uint32_t>& vHorizWaves, infoEllipseStruct* pEllipse)
{
	if ( pEllipse == nullptr )								return -1;
	if ( vVertWaves.size() < 3*HOLDER_CROWN_HALF_PIXELS )	return -1;
	if ( vHorizWaves.size() < 3*HOLDER_CROWN_HALF_PIXELS )	return -1;

	int xStart = 0;
	int xEnd = m_nWidth-1;

	for ( int liStart = 0; liStart < m_nWidth-HOLDER_CROWN_HALF_PIXELS; ++liStart )
	{
		if ( (vHorizWaves[liStart] < vHorizWaves[liStart+1]) && (vHorizWaves[liStart+1] < vHorizWaves[liStart+2]) )
		{
			if ( int(vHorizWaves[liStart+3])-int(vHorizWaves[liStart]) > ELLIPSE_WAVES_DELTA )
			{
				xStart = liStart;
				break;
			}
		}
	}
	xStart += HOLDER_CROWN_HALF_PIXELS; // the circular crown of the holder is at least 20 pixels

	for ( int liEnd = m_nWidth-1; liEnd > xStart+HOLDER_CROWN_HALF_PIXELS; --liEnd )
	{
		if ( int(vHorizWaves[liEnd-3])-int(vHorizWaves[liEnd]) > ELLIPSE_WAVES_DELTA )
		{
			xEnd = liEnd;
			break;
		}
	}
	xEnd -= HOLDER_CROWN_HALF_PIXELS;

	pEllipse->fCenterX = float(xStart+xEnd) / 2;
	pEllipse->fAxisX = xEnd - xStart;

	int yStart = 0;
	int yEnd = m_nHeight-1;

	for ( int liStart = 0; liStart < m_nHeight-HOLDER_CROWN_HALF_PIXELS; ++liStart )
	{
		if ( int(vVertWaves[liStart+3])-int(vVertWaves[liStart]) > ELLIPSE_WAVES_DELTA )
		{
			yStart = liStart;
			break;
		}
	}
	yStart += HOLDER_CROWN_HALF_PIXELS;

	for ( int liEnd = m_nHeight-1; liEnd > yStart+HOLDER_CROWN_HALF_PIXELS; --liEnd )
	{
		if ( int(vVertWaves[liEnd-3])-int(vVertWaves[liEnd]) > ELLIPSE_WAVES_DELTA )
		{
			yEnd = liEnd;
			break;
		}
	}
	yEnd -= HOLDER_CROWN_HALF_PIXELS;

	pEllipse->fCenterY = float(yStart+yEnd) / 2;
	pEllipse->fAxisY = yEnd - yStart;

	return 0;
}

int SprDetectionEllipse::getEllipseCoord(infoEllipseStruct* pEllipse, float fXCurrent, point<float>* pMinus, point<float>* pPlus)
{
	if ( pEllipse == nullptr )	return -1;
	if ( pMinus == nullptr )	return -1;
	if ( pPlus == nullptr )		return -1;

	float xRadius = pEllipse->fAxisX / 2.0f;
	float yRadius = pEllipse->fAxisY / 2.0f;

	// Calculation to get y from ellipse formula
	float fFoo = ( fXCurrent - pEllipse->fCenterX ) * ( fXCurrent - pEllipse->fCenterX );
	fFoo /= ( xRadius * xRadius );
	fFoo = 1 - fFoo;
	fFoo *= ( yRadius * yRadius );

	if ( fFoo < 0.0f )
	{
		pMinus->set(fXCurrent, -1.0f);
		pPlus->set(fXCurrent, -1);
		return 0;
	}

	pMinus->set(fXCurrent, pEllipse->fCenterY-sqrt(fFoo));
	pPlus->set(fXCurrent, pEllipse->fCenterY+sqrt(fFoo));

	return 0;
}

float SprDetectionEllipse::calculateEllipseSimilitude(vector<point<float>>& vCoordMinus, vector<point<float>>& vCoordPlus)
{
	size_t size = vCoordMinus.size();

	m_dCoordMinus.resize(size);
	m_dCoordPlus.resize(size);

	for ( size_t i = 0; i < size; ++i )
	{
		m_dCoordMinus[i].x = ceil(vCoordMinus[i].x);
		m_dCoordMinus[i].y = ceil(vCoordMinus[i].y);
		m_dCoordPlus[i].x = floor(vCoordPlus[i].x);
		m_dCoordPlus[i].y = floor(vCoordPlus[i].y);
	}

	/* We are going to fill the y array with all the missing values.  The
	 * 4 quadrands are as indicated.
	 * 					|
	 * 					|
	 * 			II		|		 I
	 * 					|
	 * 					|
	 *       ----------------------------
	 * 					|
	 * 					|
	 * 		   III		|		IV
	 * 					|
	 * 					|
	 */

	int liCntMin = 0;
	int liCntPlus = 0;

	// II quadrant
	for ( size_t i = 0; i != (size-1); ++i )
	{
		int tmp = m_dCoordMinus[i+liCntMin].y - m_dCoordMinus[i+liCntMin+1].y;
		// if there are some missing ordinates, then must be added
		if ( tmp > 1 )
		{
			for ( int m = 1; m < tmp; m++ )
			{
				int liIdx = i+liCntMin+m;
				point<int> newPoint;
				newPoint.set(m_dCoordMinus[i+liCntMin].x, m_dCoordMinus[liIdx-1].y-1);
				m_dCoordMinus.insert(m_dCoordMinus.begin()+liIdx, newPoint);
			}
			liCntMin += (tmp-1);
		}
	}

	// III quadrant
	for ( size_t i = 0; i != (size-1); ++i )
	{
		int tmp = m_dCoordPlus[i+liCntPlus+1].y - m_dCoordPlus[i+liCntPlus].y;
		// if there are some missing ordinates, then must be added
		if ( tmp > 1 )
		{
			for ( int m = 1; m < tmp; m++ )
			{
				int liIdx = i+liCntPlus+m;
				point<int> newPoint;
				newPoint.set(m_dCoordPlus[i+liCntPlus].x, m_dCoordPlus[liIdx-1].y+1);
				m_dCoordPlus.insert(m_dCoordPlus.begin()+liIdx, newPoint);
			}
			liCntPlus += (tmp-1);
		}
	}

	// I quadrant
	for ( size_t i = 0; i != (size-1); ++i )
	{
		int tmp = m_dCoordMinus[i+liCntMin+1].y - m_dCoordMinus[i+liCntMin].y;
		// if there are some missing ordinates, then must be added
		if ( tmp > 1 )
		{
			for ( int m = 1; m < tmp; m++ )
			{
				int liIdx = i+liCntMin+m;
				point<int> newPoint;
				newPoint.set(m_dCoordMinus[i+liCntMin].x, m_dCoordMinus[liIdx-1].y+1);
				m_dCoordMinus.insert(m_dCoordMinus.begin()+liIdx, newPoint);
			}
			liCntMin += (tmp-1);
		}
	}

	// IV quadrant
	for ( size_t i = 0; i != (size-1); ++i )
	{
		int tmp = m_dCoordPlus[i+liCntPlus].y - m_dCoordPlus[i+liCntPlus+1].y;
		// if there are some missing ordinates, then must be added
		if ( tmp > 1 )
		{
			for ( int m = 1; m < tmp; m++ )
			{
				int liIdx = i+liCntPlus+m;
				point<int> newPoint;
				newPoint.set(m_dCoordPlus[i+liCntPlus].x, m_dCoordPlus[liIdx-1].y-1);
				m_dCoordPlus.insert(m_dCoordPlus.begin()+liIdx, newPoint);
			}
			liCntPlus += (tmp-1);
		}
	}

	// Now the ellipse has to be filled

	size_t sizeM = m_dCoordMinus.size();
	int liImageSize = m_nWidth * m_nHeight;
	float fEllipseCnt = 0.0f;

	for ( size_t x = 0; x != sizeM; ++x )
	{
		for ( int m = -1; m <= 1; ++m )
		{
			int idx = m_dCoordMinus[x].y*m_nWidth + m_dCoordMinus[x].x + m;

			if ( idx < 0 )					idx = 0;
			else if ( idx >= liImageSize )	idx = liImageSize - 1;

			if ( m_pvImage->at(idx) > ELLIPSE_PIXEL_THRESH )
			{
				fEllipseCnt++;
				break;
			}
		}
	}

	size_t sizeP = m_dCoordPlus.size();
	for ( size_t x = 0; x != sizeP; ++x )
	{
		for ( int m = -1; m <= 1; ++m )
		{
			int idx = m_dCoordPlus[x].y*m_nWidth + m_dCoordPlus[x].x + m;

			if ( idx < 0 )					idx = 0;
			else if ( idx >= liImageSize )	idx = liImageSize - 1;

			if ( m_pvImage->at(idx) > ELLIPSE_PIXEL_THRESH )
			{
				fEllipseCnt++;
				break;
			}
		}
	}

	float fRes = 100.0f * fEllipseCnt / float(sizeM+sizeP);

	m_pLogger->log(LOG_DEBUG, "SprDetectionEllipse::calculateEllipseSimilitude: picture is the holder "
							  "with a probability of [%.2f%%]", fRes);

	return fRes;
}

