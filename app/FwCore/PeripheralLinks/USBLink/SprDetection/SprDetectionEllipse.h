/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SprDetectionEllipse.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SprDetectionEllipse class.
 @details

 ****************************************************************************
*/

#ifndef SPRDETECTIONELLIPSE_H
#define SPRDETECTIONELLIPSE_H

#include "SprDetectionCommonInclude.h"

class SprDetectionEllipse
{
	public:

		SprDetectionEllipse();

		virtual ~SprDetectionEllipse();

		int init(vector<unsigned char>* pvImage, Log *pLogger, uint32_t uliWidth, uint32_t uliHeight);

		int perform(const stripEdgesStruct* Edge, eSprResults& eRes);

		const deque<point<int>>& getIntCoordMinus();

		const deque<point<int>>& getIntCoordPlus();

	private:

		int getWaves(vector<unsigned char>* pvImage, vector<uint32_t>& vVertWaves, vector<uint32_t>& vHorizWaves);

		int getEllipseInfo(vector<uint32_t>& vVertWaves, vector<uint32_t>& vHorizWaves, infoEllipseStruct* pEllipse);

		int getEllipseCoord(infoEllipseStruct* pEllipse, float fXCurrent, point<float>* pMinus, point<float>* pPlus);

		float calculateEllipseSimilitude(vector<point<float>>& vCoordMinus, vector<point<float>>& vCoordPlus);

    private:

        int  m_nWidth;
        int  m_nHeight;
        Log* m_pLogger;

        bool m_bConfigOk;
        vector<unsigned char>* m_pvImage;
        stripEdgesStruct  m_Edge;
        deque<point<int>> m_dCoordMinus;
        deque<point<int>> m_dCoordPlus;

};

#endif // SPRDETECTIONELLIPSE_H
