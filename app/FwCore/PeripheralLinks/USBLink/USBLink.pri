INCLUDEPATH += $$PWD/../USBLink/
INCLUDEPATH += $$PWD/../USBLink/SampleDetection/
INCLUDEPATH += $$PWD/../USBLink/CR8062/


HEADERS += \
	$$PWD/SampleDetection/Filters.h \
        $$PWD/SampleDetection/SampleDetectionInclude.h \
        $$PWD/CR8062/CR8062Protocol.h \
        $$PWD/CR8062/CR8062Reader.h \
        $$PWD/CR8062/CR8062Camera.h \
        $$PWD/CR8062/CR8062Link.h \
        $$PWD/CR8062/CR8062Base.h \
        $$PWD/CR8062/CR8062Include.h \
        $$PWD/USBLinkBoard.h \
        $$PWD/USBInterfaceCollector.h \
        $$PWD/USBLinkCommonInclude.h \
        $$PWD/CR8062/CR8062FwUpdateManager.h \
        $$PWD/SampleDetection/SampleDetectionBase.h \
        $$PWD/SampleDetection/SampleDetectionZeroZero.h \
        $$PWD/SampleDetection/SampleDetectionPreliminary.h \
        $$PWD/SampleDetection/SampleDetectionVertical.h \
        $$PWD/SampleDetection/SampleDetectionHorizontal.h \
        $$PWD/SampleDetection/SampleDetectionLink.h \
        $$PWD/USBComHandler.h \
        $$PWD/USBErrors.h \
    $$PWD/SprDetection/SprDetectionEllipse.h \
    $$PWD/SprDetection/SprDetectionPreliminary.h \
    $$PWD/SprDetection/SprDetectionLink.h \
    $$PWD/SprDetection/SprDetectionCommonInclude.h

SOURCES += \
	$$PWD/SampleDetection/Filters.cpp \
        $$PWD/SampleDetection/SampleDetectionUtility.cpp \
        $$PWD/CR8062/CR8062Protocol.cpp \
        $$PWD/CR8062/CR8062Reader.cpp \
        $$PWD/CR8062/CR8062Camera.cpp \
        $$PWD/CR8062/CR8062Link.cpp \
        $$PWD/CR8062/CR8062Base.cpp \
        $$PWD/USBLinkBoard.cpp \
        $$PWD/USBInterfaceCollector.cpp \
        $$PWD/CR8062/CR8062FwUpdateManager.cpp \
        $$PWD/SampleDetection/SampleDetectionBase.cpp \
        $$PWD/SampleDetection/SampleDetectionZeroZero.cpp \
        $$PWD/SampleDetection/SampleDetectionPreliminary.cpp \
        $$PWD/SampleDetection/SampleDetectionVertical.cpp \
        $$PWD/SampleDetection/SampleDetectionHorizontal.cpp \
        $$PWD/SampleDetection/SampleDetectionLink.cpp \
        $$PWD/USBComHandler.cpp \
        $$PWD/USBErrors.cpp \
    $$PWD/SprDetection/SprDetectionEllipse.cpp \
    $$PWD/SprDetection/SprDetectionPreliminary.cpp \
    $$PWD/SprDetection/SprDetectionLink.cpp

