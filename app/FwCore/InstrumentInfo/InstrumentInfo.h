/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InstrumentInfo.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the InstrumentInfo class.
 @details

 ****************************************************************************
*/

#ifndef INSTRUMENTINFO_H
#define INSTRUMENTINFO_H

#include <string>
#include <time.h>
#include <vector>

#include "CommonInclude.h"

// !!!!! TODO inserire aggiornamento valori da Camera

typedef enum
{
	eCounterProtocolsA = 0,
	eCounterReadSecA,
    eCounterPumpA,
    eCounterTowerA,
    eCounterTrayA,
    eCounterSprA,
    eCounterProtocolsB,
	eCounterReadSecB,
    eCounterPumpB,
    eCounterTowerB,
    eCounterTrayB,
    eCounterSprB,
    eCounterDataMatrix,
    eCounterBarcode,
    eCounterWell0,
    eCounterWell3,
    eCounterReading,
    eCounterAutoCalibration,
    eCounterSecondsStart,
    eCounterMax
}eCounterId;

#define COUNTER_PROTOCOLS_A_STRING	"PROTOCOLS_A"
#define COUNTER_PUMP_A_STRING		"PUMP_A"
#define COUNTER_TOWER_A_STRING		"TOWER_A"
#define COUNTER_TRAY_A_STRING		"TRAY_A"
#define COUNTER_SPR_A_STRING		"SPR_A"
#define COUNTER_PROTOCOLS_B_STRING	"PROTOCOLS_B"
#define COUNTER_PUMP_B_STRING		"PUMP_B"
#define COUNTER_TOWER_B_STRING		"TOWER_B"
#define COUNTER_TRAY_B_STRING		"TRAY_B"
#define COUNTER_SPR_B_STRING		"SPR_B"
#define COUNTER_DATA_MATRIX_STRING	"DATA_MATRIX"
#define COUNTER_BARCODE_STRING		"BARCODE"
#define COUNTER_WELL_0_STRING		"WELL_0"
#define COUNTER_WELL_3_STRING		"WELL_3"
#define COUNTER_READINGS_STRING		"READINGS"
#define COUNTER_CALIBRATION_STRING	"AUTO_CALIBRATION"
#define COUNTER_UP_TIME_STRING		"UP_TIME"



// list of string identifiers corresponding to the above enum in the same order

using namespace std;

/*! ***********************************************************************************************************
 * @brief InstrumentInfo container of all data regarding the instrument
 * ************************************************************************************************************
 */

class InstrumentInfo
{
	public:

		/*! *******************************************************************************************************
		 * @brief InstrumentInfo void constructor
		 * ********************************************************************************************************
		 */
		InstrumentInfo();

		/*! *******************************************************************************************************
		 * @brief init initialization function
		 * @return true if succesfull, false otherwise
		 * ********************************************************************************************************
		 */
		bool init();

		/*! *******************************************************************************************************
		 * @brief stop final function
		 * @return true if succesfull, false otherwise
		 * ********************************************************************************************************
		 */
		bool stop();

		/*! *******************************************************************************************************
		 * @brief  getMasterApplicationRelease read the hardcoded application software release
		 * @return the string m_strMasterApplicationRelease
		 * ********************************************************************************************************
		 */
		std::string getMasterApplicationRelease();

		/*! *******************************************************************************************************
		 * @brief  getMasterBootRelease read the MasterBoard boot software release
		 * @return the string m_strMasterBoot
		 * ********************************************************************************************************
		 */
		std::string getMasterBootRelease();

		/*! *******************************************************************************************************
		 * @brief  getMasterKernelRelease read the MasterBoard kernel software release
		 * @return the string m_strMasterKernel
		 * ********************************************************************************************************
		 */
		std::string getMasterKernelRelease();

		/*! *******************************************************************************************************
		 * @brief  getMasterMiniKernelRelease read the MasterBoard miniKernel software release
		 * @return the string m_strMasterMiniKernel
		 * ********************************************************************************************************
		 */
		std::string getMasterMiniKernelRelease();

		/*! *******************************************************************************************************
		 * @brief  getCameraApplication read the Camera software release
		 * @return the string m_strCameraApplication
		 * ********************************************************************************************************
		 */
		std::string getCameraApplicationRelease();

		/*! *******************************************************************************************************
		 * @brief  setCameraApplication to set the Camera software release
		 * @param  release the string copied to m_strCameraApplication
		 * ********************************************************************************************************
		 */
		void setCameraApplicationRelease(std::string release);

		/*! *******************************************************************************************************
		 * @brief  getNshApplication read the NSH software release
		 * @return the string m_strNshApplication
		 * ********************************************************************************************************
		 */
		std::string getNshApplicationRelease();

		/*! *******************************************************************************************************
		 * @brief  setNshApplication to set the NSH software release
		 * @param  release the string copied to m_strNshApplication
		 * ********************************************************************************************************
		 */
		void setNshApplicationRelease(std::string release);

		/*! *******************************************************************************************************
		 * @brief  getNshBoot read the NSH Boot software release
		 * @return the string m_strNshBoot
		 * ********************************************************************************************************
		 */
		std::string getNshBootRelease();

		/*! *******************************************************************************************************
		 * @brief  setNshBoot to set the NSH BOOT software release
		 * @param  release the string copied to m_strNshBoot
		 * ********************************************************************************************************
		 */
		void setNshBootRelease(std::string release);

		/*! *******************************************************************************************************
		 * @brief  getSectionAApplication read the SectionA software release
		 * @return the string m_strSectionAApplication
		 * ********************************************************************************************************
		 */
		std::string getSectionAApplicationRelease();

		/*! *******************************************************************************************************
		 * @brief  setSectionApplication to set the Section software release
		 * @param section the index of the section
		 * @param  release the string copied to m_strSectionApplication
		 * ********************************************************************************************************
		 */

		void setSectionApplicationRelease(int section, std::string release);

		/*! *******************************************************************************************************
		 * @brief  setSectionBootRelease to set the Section BOOT software release
		 * @param section the index of the section
		 * @param  release the string copied to m_strSectionBoot
		 * ********************************************************************************************************
		 */
		void setSectionBootRelease(int section, std::string release);

		/*! *******************************************************************************************************
		 * @brief  setSectionFPGARelease to set the Section FPGA software release
		 * @param section the index of the section
		 * @param  release the string copied to m_strSectionFpga
		 * *******************************************************************************************************
		 */
		void setSectionFPGARelease(int section, std::string release);

		/*! *******************************************************************************************************
		 * @brief  getSectionABoot read the SectionA Boot software release
		 * @return the string m_strSectionABoot
		 * ********************************************************************************************************
		 */
		std::string getSectionABootRelease();

		/*! *******************************************************************************************************
		 * @brief  getSectionAFpga read the SectionA FPGA software release
		 * @return the string m_strSectionAFpga
		 * ********************************************************************************************************
		 */
		std::string getSectionAFpgaRelease();

		/*! *******************************************************************************************************
		 * @brief  getSectionBApplication read the SectionB software release
		 * @return the string m_strSectionBApplication
		 * *******************************************************************************************************
		 */
		std::string getSectionBApplicationRelease();

		/*! *******************************************************************************************************
		 * @brief  getSectionBBoot read the SectionB Boot software release
		 * @return the string m_strSectionBBoot
		 * ********************************************************************************************************
		 */
		std::string getSectionBBootRelease();

		/*! ******************************************************************************************************
		 * @brief  getSectionBFpga read the SectionB FPGA software release
		 * @return the string m_strSectionBFpga
		 * ********************************************************************************************************
		 */
		std::string getSectionBFpgaRelease();

		/*! *******************************************************************************************************
		 * @brief  getInstrumentSerialNumber read the Instrument Serial Number
		 * @return the string m_strInstrumentSerial
		 * *******************************************************************************************************
		 */
		std::string getInstrumentSerialNumber();

		/*! ******************************************************************************************************
		 * @brief  setInstrumentSerialNumber to set the Instrument Serial Number
		 * @param  serial the string copied to m_strInstrumentSerialNumber
		 * ********************************************************************************************************
		 */
		void setInstrumentSerialNumber(std::string serial);

		/*! ******************************************************************************************************
		 * @brief  getMasterSerialNumber read the MasterBoard Serial Number
		 * @return the string m_strMasterSerial
		 * ********************************************************************************************************
		 */
		std::string getMasterSerialNumber();

		/*! *******************************************************************************************************
		 * @brief  setMasterSerialNumber to set the Master Serial Number
		 * @param  serial the string copied to m_strMasterSerialNumber
		 * ********************************************************************************************************
		 */
		void setMasterSerialNumber(std::string serial);

		/*! *******************************************************************************************************
		 * @brief  getCameraSerialNumber read the Camera Serial Number
		 * @return the string m_strCameraSerial
		 * ********************************************************************************************************
		 */
		std::string getCameraSerialNumber();

		/*! *******************************************************************************************************
		 * @brief  setCameraSerialNumber to set the Camera Serial Number
		 * @param  serial the string copied to m_strCameraSerialNumber
		 * ********************************************************************************************************
		 */
		void setCameraSerialNumber(std::string serial);

		/*! *******************************************************************************************************
		 * @brief  getNshSerialNumber read the NSH Serial Number
		 * @return the string m_strNshSerial
		 * ********************************************************************************************************
		 */
		std::string getNshSerialNumber();

		/*! *******************************************************************************************************
		 * @brief  setNshSerialNumber to set the NSH Serial Number
		 * @param  serial the string copied to m_strNshSerialNumber
		 * *******************************************************************************************************
		 */
		void setNshSerialNumber(std::string serial);

		/*! *******************************************************************************************************
		 * @brief  getSectionASerialNumber read the SectionA Serial Number
		 * @return the string m_strSectionASerial
		 * ********************************************************************************************************
		 */
		std::string getSectionASerialNumber();

		/*! *******************************************************************************************************
		 * @brief  setSectionASerialNumber to set the SectionA Serial Number
		 * @param  serial the string copied to m_strSectionASerialNumber
		 * ********************************************************************************************************
		 */
		void setSectionASerialNumber(std::string serial);

		/*! *******************************************************************************************************
		 * @brief  getSectionBSerialNumber read the SectionB Serial Number
		 * @return the string m_strSectionBSerial
		 * ********************************************************************************************************
		 */
		std::string getSectionBSerialNumber();

		/*! *******************************************************************************************************
		 * @brief  setSectionBSerialNumber to set the SectionB Serial Number
		 * @param  serial the string copied to m_strSectionBSerialNumber
		 * *******************************************************************************************************
		 */
		void setSectionBSerialNumber(std::string serial);

		/*! ******************************************************************************************************
		 * @brief  getMaintenanceMode read the MaintenanceMode flag
		 * @return the MaintenanceMode flag
		 * *******************************************************************************************************
		 */
		bool getMaintenanceMode();

		/*! ******************************************************************************************************
		 * @brief  setMaintenanceMode to set the Maintenance Mode flag
		 * @param  bStatus the flag status
		 * ********************************************************************************************************
		 */
		void setMaintenanceMode(bool bStatus);

		/*! ******************************************************************************************************
		 * @brief  getSetTime read the TimeSet flag
		 * @return the TimeSet flag
		 * *******************************************************************************************************
		 */
		bool getSetTime();

		/*! ******************************************************************************************************
		 * @brief  setSetTime to set the TimeSet flag
		 * @param  bStatus the flag status
		 * ********************************************************************************************************
		 */
		void setSetTime(bool bStatus);

		/*! ******************************************************************************************************
		 * @brief  setStartTime to record the time at sw startup
         * @param  pTimeStart	tm structure
		 * @param  clocksStart	equivalent clocks
		 * ********************************************************************************************************
		 */
		void setStartTime(struct tm * pTimeStart, unsigned long long clocksStart);

		/*! ******************************************************************************************************
		 * @brief  setClockRatio to record the ratio between clocks and seconds
		 * @param  ratio	factor
		 * ********************************************************************************************************
		 */
		void setClockRatio(float ratio);

		/*! ******************************************************************************************************
		 * @brief  getClockRatio to get the ratio between clocks and seconds
		 * @return  ratio	factor
		 * ********************************************************************************************************
		 */
		float getClockRatio();

		/*! ******************************************************************************************************
		 * @brief  getClockStart to get the start clock value
		 * @return  the value
		 * ********************************************************************************************************
		 */
		uint64_t getClockStart();

		/*! *************************************************************************************************
		 * @brief TEMPERATURE DATA SETTERS as coming from GLOBAL_SETTINGS
		 * **************************************************************************************************
		 */
		void setSprMinTemperature(uint8_t section, int value);
		void setSprMaxTemperature(uint8_t section, int value);
		void setSprTargetTemperature(uint8_t section, int value);
		void setSprToleranceTemperature(uint8_t section, int value);

		void setTrayMinTemperature(uint8_t section, int value);
		void setTrayMaxTemperature(uint8_t section, int value);
		void setTrayTargetTemperature(uint8_t section, int value);
		void setTrayToleranceTemperature(uint8_t section, int value);

		void setInternalMinTemperature(int value);
		void setInternalMaxTemperature(int value);

		void setForceTemperature(bool status);

		/*! *************************************************************************************************
		 * @brief TEMPERATURE DATA GETTERS
		 * **************************************************************************************************
		 */
		int getSprMinTemperature(uint8_t section);
		int getSprMaxTemperature(uint8_t section);
		int getSprTargetTemperature(uint8_t section);
		int getSprToleranceTemperature(uint8_t section);

		int getTrayMinTemperature(uint8_t section);
		int getTrayMaxTemperature(uint8_t section);
		int getTrayTargetTemperature(uint8_t section);
		int getTrayToleranceTemperature(uint8_t section);

		int getInternalMinTemperature();
		int getInternalMaxTemperature();

		bool getForceTemperature();

		/*! *************************************************************************************************
		 * @brief instrument counters get and set
		 * **************************************************************************************************
		 */

		bool setInstrumentCounter(eCounterId counter, uint16_t value);

		bool addInstrumentCounter(eCounterId counter, uint16_t value);

		uint16_t getInstrumentCounter(eCounterId counter);

		void saveCountersToFile();

		int8_t getInstrumentCounterIndexFromString(std::string strCounter);

        /*! *************************************************************************************************
         * @brief OPT id get and set
         * **************************************************************************************************
         */

        std::string getOPTid();

        void setOPTid(const std::string strID);

        /*! *************************************************************************************************
         * @brief Solid Standard value and gain get and set
         * **************************************************************************************************
         */

        std::string getSSvalue();
        std::string getSSgain();

        void setSSvalue(const std::string strID);
        void setSSgain(const std::string strID);

    private:

		/*! ******************************************************************************************************
		 * @brief  readSerialFile read from file SerialNumber.cfg the Instrument and MAsterBoard serial numbers
		 * *******************************************************************************************************
		 */
		void readSerialFile();

        /*! ******************************************************************************************************
         * @brief  readReleaseFile read from file releaseLabel.cfg the MAsterBoard software release
         * @param strReleaseFile the name of the file: /etc/sw-versions for Linux, releaseLabel.cfg for the application
         * *******************************************************************************************************
         */
        void readReleaseFile(std::string strReleaseFile);

        /*! ******************************************************************************************************
		 * @brief  writeSerialFile write to file SerialNumber.cfg the Instrument and MAsterBoard serial numbers
		 * *******************************************************************************************************
		 */
		void writeSerialFile();

		/*! ******************************************************************************************************
		 * @brief  readCountersFile read from file CountersNumber.cfg the Instrument counters
		 * *******************************************************************************************************
		 */
		void readCountersFile();

		/*! ******************************************************************************************************
		 * @brief  writeCountersFile write to file CountersNumber.cfg the Instrument counters
		 * *******************************************************************************************************
		 */
		void writeCountersFile();

        /*! ******************************************************************************************************
         * @brief  readOPTFile read from file OPTID.cfg the current OPT calibration identifier
         * *******************************************************************************************************
         */
        void readOPTFile();


        /*! ******************************************************************************************************
         * @brief  writeOPTFile writes to file OPTID.cfg the current OPT calibration identifier
         * *******************************************************************************************************
         */
        void writeOPTFile();

    private:

		// software versions
        std::string m_strMasterApplicationRelease;
        std::string m_strMasterBoot, m_strMasterKernel, m_strMasterFileSystem;
		std::string m_strCameraApplication;
		std::string m_strNshBoot, m_strNshApplication;
		std::string m_strSectionABoot, m_strSectionAFpga, m_strSectionAApplication;
		std::string m_strSectionBBoot, m_strSectionBFpga, m_strSectionBApplication;

		// serial numbers
		std::string m_strInstrumentSerial, m_strMasterSerial, m_strCameraSerial;
		std::string m_strNshSerial, m_strSectionASerial, m_strSectionBSerial;

		bool m_bMaintenanceMode;

		bool m_bSetTime;
		struct tm m_timeStart;
		unsigned long long m_clocksStart;
		float m_clockRatio;

		int m_SprMin[SCT_NUM_TOT_SECTIONS], m_SprMax[SCT_NUM_TOT_SECTIONS];
		int m_SprTarget[SCT_NUM_TOT_SECTIONS], m_SprTolerance[SCT_NUM_TOT_SECTIONS];
		int m_TrayMin[SCT_NUM_TOT_SECTIONS], m_TrayMax[SCT_NUM_TOT_SECTIONS];
		int m_TrayTarget[SCT_NUM_TOT_SECTIONS], m_TrayTolerance[SCT_NUM_TOT_SECTIONS];
		bool m_bTemperatureForce;
		int m_InternalMin, m_InternalMax;

        std::string m_strOPTid, m_strSSvalue, m_strSSgain;

		uint16_t m_instrumentCounters[eCounterMax];
		vector<std::string> m_counterString;
};

#endif // INSTRUMENTINFO_H
