/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    InstrumentInfo.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CanBuffer class.
 @details

 ****************************************************************************
*/

#include <string.h>
#include <iostream>
#include <fstream>
#include "InstrumentInfo.h"
#include "CommonInclude.h"
#include "MainExecutor.h"

#define INSTRUMENT_SERIAL_ENTRY	"Instrument"
#define MASTER_SERIAL_ENTRY		"MasterBoard"
#define NSH_SERIAL_ENTRY		"NSH"
#define SECTIONA_SERIAL_ENTRY	"SectionA"
#define SECTIONB_SERIAL_ENTRY	"SectionB"
#define CAMERA_SERIAL_ENTRY		"Camera"

#define SYSTEM_VERSIONS_FILE        "/etc/sw-versions"
#define KERNEL_RELEASE_ENTRY        "kernel"
#define FILESYSTEM_RELEASE_ENTRY	"rootfs"
#define BOOT_RELEASE_ENTRY          "bootloader"

#define APPLICATION_RELEASE_ENTRY	"Application"

#define OPT_ID_ENTRY            "OPTID"

InstrumentInfo::InstrumentInfo()
{
	m_strMasterBoot.clear();
	m_strMasterKernel.clear();
    m_strMasterFileSystem.clear();
	m_strCameraApplication.clear();
	m_strNshBoot.clear();
	m_strNshApplication.clear();
	m_strSectionABoot.clear();
	m_strSectionAFpga.clear();
	m_strSectionAApplication.clear();
	m_strSectionBBoot.clear();
	m_strSectionBFpga.clear();
	m_strSectionBApplication.clear();
	m_strInstrumentSerial.clear();
	m_strMasterSerial.clear();
	m_strCameraSerial.clear();
	m_strNshSerial.clear();
	m_strSectionASerial.clear();
	m_strSectionBSerial.clear();
	m_bMaintenanceMode = false;
	m_bSetTime = false;
	m_clocksStart = 0;
	m_clockRatio = 0;
    m_strOPTid.clear();
    m_strSSvalue.clear();
    m_strSSgain.clear();

	for(uint8_t section = SCT_A_ID; section < SCT_NUM_TOT_SECTIONS; section++)
	{
		m_SprMin[section] = m_SprMax[section] = -1;
		m_SprTarget[section] = m_SprTolerance[section] = -1;
		m_TrayMin[section] = m_TrayMax[section] = -1;
		m_TrayTarget[section] = m_TrayTolerance[section] = -1;
	}
	m_InternalMax = m_InternalMin = -1;
	m_bTemperatureForce = false;

	for(uint8_t i = 0; i < eCounterMax; i++)
	{
		m_instrumentCounters[i] = 0;
	}

	m_counterString.push_back(COUNTER_PROTOCOLS_A_STRING);
	m_counterString.push_back(COUNTER_PUMP_A_STRING);
	m_counterString.push_back(COUNTER_TOWER_A_STRING);
	m_counterString.push_back(COUNTER_TRAY_A_STRING);
	m_counterString.push_back(COUNTER_SPR_A_STRING);
	m_counterString.push_back(COUNTER_PROTOCOLS_B_STRING);
	m_counterString.push_back(COUNTER_PUMP_B_STRING);
	m_counterString.push_back(COUNTER_TOWER_B_STRING);
	m_counterString.push_back(COUNTER_TRAY_B_STRING);
	m_counterString.push_back(COUNTER_SPR_B_STRING);
	m_counterString.push_back(COUNTER_DATA_MATRIX_STRING);
	m_counterString.push_back(COUNTER_BARCODE_STRING);
	m_counterString.push_back(COUNTER_WELL_0_STRING);
	m_counterString.push_back(COUNTER_WELL_3_STRING);
	m_counterString.push_back(COUNTER_READINGS_STRING);
	m_counterString.push_back(COUNTER_CALIBRATION_STRING);
	m_counterString.push_back(COUNTER_UP_TIME_STRING);
}

bool InstrumentInfo::init()
{
    // read from system file the Linux settings
    readReleaseFile(SYSTEM_VERSIONS_FILE);

    // then the application
    readReleaseFile(RELEASE_SETUP_FILE);

	// read from file the Instrument and Master serial number
	readSerialFile();

	// read from file the Instrument counters
	readCountersFile();

    // read the OPT identifier
    readOPTFile();

	return true;
}

bool InstrumentInfo::stop()
{
	// at least at the end save the counters
	writeCountersFile();
	return true;
}

std::__cxx11::string InstrumentInfo::getMasterApplicationRelease()
{
	return m_strMasterApplicationRelease;
}

std::__cxx11::string InstrumentInfo::getMasterBootRelease()
{
	return m_strMasterBoot;
}

std::__cxx11::string InstrumentInfo::getMasterKernelRelease()
{
	return m_strMasterKernel;
}

std::__cxx11::string InstrumentInfo::getMasterMiniKernelRelease()
{
    return m_strMasterFileSystem;
}

std::__cxx11::string InstrumentInfo::getCameraApplicationRelease()
{
	return m_strCameraApplication;
}

void InstrumentInfo::setCameraApplicationRelease(std::__cxx11::string release)
{
	m_strCameraApplication.assign(release);
}

std::__cxx11::string InstrumentInfo::getNshApplicationRelease()
{
	return m_strNshApplication;
}

void InstrumentInfo::setNshApplicationRelease(std::__cxx11::string release)
{
	m_strNshApplication.assign(release);
}

std::__cxx11::string InstrumentInfo::getNshBootRelease()
{
	return m_strNshBoot;
}

void InstrumentInfo::setNshBootRelease(std::__cxx11::string release)
{
	m_strNshBoot.assign(release);
}

void InstrumentInfo::setSectionApplicationRelease(int section, std::__cxx11::string release)
{
	if(section == SCT_A_ID)
		m_strSectionAApplication.assign(release);
	else
		m_strSectionBApplication.assign(release);
}

void InstrumentInfo::setSectionBootRelease(int section, std::__cxx11::string release)
{
	if(section == SCT_A_ID)
		m_strSectionABoot.assign(release);
	else
		m_strSectionBBoot.assign(release);
}

void InstrumentInfo::setSectionFPGARelease(int section, std::__cxx11::string release)
{
	if(section == SCT_A_ID)
		m_strSectionAFpga.assign(release);
	else
		m_strSectionBFpga.assign(release);
}

std::__cxx11::string InstrumentInfo::getSectionAApplicationRelease()
{
	return m_strSectionAApplication;
}

std::__cxx11::string InstrumentInfo::getSectionABootRelease()
{
	return m_strSectionABoot;
}

std::__cxx11::string InstrumentInfo::getSectionAFpgaRelease()
{
	return m_strSectionAFpga;
}

std::__cxx11::string InstrumentInfo::getSectionBApplicationRelease()
{
	return m_strSectionBApplication;
}

std::__cxx11::string InstrumentInfo::getSectionBBootRelease()
{
	return m_strSectionBBoot;
}

std::__cxx11::string InstrumentInfo::getSectionBFpgaRelease()
{
	return m_strSectionBFpga;
}

std::__cxx11::string InstrumentInfo::getInstrumentSerialNumber()
{
	return m_strInstrumentSerial;
}

void InstrumentInfo::setInstrumentSerialNumber(std::__cxx11::string serial)
{
	if(strcmp(m_strInstrumentSerial.c_str(), serial.c_str()) != 0)
	{
		writeSerialFile();
	}
	m_strInstrumentSerial.assign(serial);
}

std::__cxx11::string InstrumentInfo::getMasterSerialNumber()
{
	return m_strMasterSerial;
}

void InstrumentInfo::setMasterSerialNumber(std::__cxx11::string serial)
{
	if(strcmp(m_strMasterSerial.c_str(), serial.c_str()) != 0)
	{
		writeSerialFile();
	}
	m_strMasterSerial.assign(serial);
}

std::__cxx11::string InstrumentInfo::getCameraSerialNumber()
{
	return m_strCameraSerial;
}

void InstrumentInfo::setCameraSerialNumber(std::__cxx11::string serial)
{
	m_strCameraSerial.assign(serial);
}

std::__cxx11::string InstrumentInfo::getNshSerialNumber()
{
	return m_strNshSerial;
}

void InstrumentInfo::setNshSerialNumber(std::__cxx11::string serial)
{
	if(strcmp(m_strNshSerial.c_str(), serial.c_str()) != 0)
	{
		writeSerialFile();
	}
	m_strNshSerial.assign(serial);
}

std::__cxx11::string InstrumentInfo::getSectionASerialNumber()
{
	return m_strSectionASerial;
}

void InstrumentInfo::setSectionASerialNumber(std::__cxx11::string serial)
{
	if(strcmp(m_strSectionASerial.c_str(), serial.c_str()) != 0)
	{
		writeSerialFile();
	}
	m_strSectionASerial.assign(serial);
}

std::__cxx11::string InstrumentInfo::getSectionBSerialNumber()
{
	return m_strSectionBSerial;
}

void InstrumentInfo::setSectionBSerialNumber(std::__cxx11::string serial)
{
	if(strcmp(m_strSectionBSerial.c_str(), serial.c_str()) != 0)
	{
		writeSerialFile();
	}
	m_strSectionBSerial.assign(serial);
}

bool InstrumentInfo::getMaintenanceMode()
{
	return m_bMaintenanceMode;
}

void InstrumentInfo::setMaintenanceMode(bool bStatus)
{
	m_bMaintenanceMode = bStatus;
}

bool InstrumentInfo::getSetTime()
{
	return m_bSetTime;
}

void InstrumentInfo::setSetTime(bool bStatus)
{
	m_bSetTime = bStatus;
}

void InstrumentInfo::setStartTime(tm* pTimeStart, unsigned long long clocksStart)
{
	m_clocksStart = clocksStart;
	m_timeStart = *pTimeStart;
}

void InstrumentInfo::setClockRatio(float ratio)
{
	m_clockRatio = ratio;
}

float InstrumentInfo::getClockRatio()
{
	return m_clockRatio;
}

uint64_t InstrumentInfo::getClockStart()
{
	return m_clocksStart;
}

void InstrumentInfo::readSerialFile()
{
	std::string strRead, strId, strSerial;
	std::ifstream inFile;

	inFile.open(SERIAL_SETUP_FILE);
	if(inFile.fail())
	{
		//File does not exist code here
		printf("InstrumentInfo::readSerialFile-->%s",strerror(errno));
		return;
	}

	while(!inFile.eof())
	{
		getline(inFile, strRead);
		if(!strRead.empty())
		{
			// lines beginning with # are comments
			if(strRead.front() != '#')
			{
				size_t posStr = 0;
				if((posStr = strRead.find("=")) != std::string::npos)
				{
					// extract the value for the login / password
					posStr = strRead.find("=");
					strSerial = strRead.substr(posStr + 1);
					strId = strRead.substr(0, posStr);
					if(strcmp(strId.c_str(), INSTRUMENT_SERIAL_ENTRY) == 0)
					{
						m_strInstrumentSerial.assign(strSerial);
					}
					else if(strcmp(strId.c_str(), MASTER_SERIAL_ENTRY) == 0)
					{
						m_strMasterSerial.assign(strSerial);
					}
					else if(strcmp(strId.c_str(), NSH_SERIAL_ENTRY) == 0)
					{
						m_strNshSerial.assign(strSerial);
					}
					else if(strcmp(strId.c_str(), SECTIONA_SERIAL_ENTRY) == 0)
					{
						m_strSectionASerial.assign(strSerial);
					}
					else if(strcmp(strId.c_str(), SECTIONB_SERIAL_ENTRY) == 0)
					{
						m_strSectionBSerial.assign(strSerial);
					}
					else if(strcmp(strId.c_str(), CAMERA_SERIAL_ENTRY) == 0)
					{
						m_strCameraSerial.assign(strSerial);
					}
				}
			}
		}
	}
    inFile.close();
}

void InstrumentInfo::readReleaseFile(std::string strFileName)
{
    std::string strRead, strId, strSerial;
    std::ifstream inFile;

    inFile.open(strFileName);
    if(inFile.fail())
    {
        //File does not exist code here
        printf("InstrumentInfo::readSerialFile-->%s", strerror(errno));
        return;
    }

    while(!inFile.eof())
    {
        getline(inFile, strRead);
        if(!strRead.empty())
        {
            // lines beginning with # are comments
            if(strRead.front() != '#')
            {
                size_t posStr = std::string::npos;
                size_t posTab = std::string::npos;
                posStr = strRead.find(" ");
                posTab = strRead.find("\t");
                if(posStr != std::string::npos || posTab != std::string::npos)
                {
                    // extract the value for the system OR application versions
                    if(posStr == std::string::npos)
                        posStr = posTab;

                    strSerial = strRead.substr(posStr + 1);
                    strSerial = trimString(strSerial);

                    strId = strRead.substr(0, posStr);
                    strId = trimString(strId);

                    if(strcmp(strId.c_str(), KERNEL_RELEASE_ENTRY) == 0)
                    {
                        if(m_strMasterKernel.empty())
                        {
                            m_strMasterKernel.assign(strSerial);
                        }
                    }
                    else if(strcmp(strId.c_str(), FILESYSTEM_RELEASE_ENTRY) == 0)
                    {
                        if(m_strMasterFileSystem.empty())
                        {
                            m_strMasterFileSystem.assign(strSerial);
                        }
                    }
                    else if(strcmp(strId.c_str(), BOOT_RELEASE_ENTRY) == 0)
                    {
                        if(m_strMasterBoot.empty())
                        {
                            m_strMasterBoot.assign(strSerial);
                        }
                    }
                    else if(strcmp(strId.c_str(), APPLICATION_RELEASE_ENTRY) == 0)
                    {
                        if(m_strMasterApplicationRelease.empty())
                        {
                            m_strMasterApplicationRelease.assign(strSerial);
                        }
                    }
                }
            }
        }
    }
    inFile.close();
}

void InstrumentInfo::writeSerialFile()
{
	std::string strId;

	std::ofstream outFile(SERIAL_SETUP_FILE);

	strId.assign(INSTRUMENT_SERIAL_ENTRY);
	strId.append("=");
	strId.append(m_strInstrumentSerial);
	outFile << strId << std::endl;

	strId.assign(MASTER_SERIAL_ENTRY);
	strId.append("=");
	strId.append(m_strMasterSerial);
	outFile << strId << std::endl;

	strId.assign(NSH_SERIAL_ENTRY);
	strId.append("=");
	strId.append(m_strNshSerial);
	outFile << strId << std::endl;

	strId.assign(SECTIONA_SERIAL_ENTRY);
	strId.append("=");
	strId.append(m_strSectionASerial);
	outFile << strId << std::endl;

	strId.assign(SECTIONB_SERIAL_ENTRY);
	strId.append("=");
	strId.append(m_strSectionBSerial);
	outFile << strId << std::endl;

	strId.assign(CAMERA_SERIAL_ENTRY);
	strId.append("=");
	strId.append(m_strCameraSerial);
	outFile << strId << std::endl;
}

void InstrumentInfo::readOPTFile()
{
    std::string strRead, strId, strSerial;
    std::ifstream inFile;

    inFile.open(NSH_OPTID_FILE);
    if(inFile.fail())
    {
        //File does not exist code here
        printf("InstrumentInfo::readOPTFile-->%s", strerror(errno));
        return;
    }

    while(!inFile.eof())
    {
        getline(inFile, strRead);
        if(!strRead.empty())
        {
            // lines beginning with # are comments
            if(strRead.front() != '#')
            {
                size_t posStr = 0;
                if((posStr = strRead.find("=")) != std::string::npos)
                {
                    // extract the value for the system OR application versions
                    posStr = strRead.find("=");

                    strSerial = strRead.substr(posStr + 1);
                    strSerial = trimString(strSerial);

                    strId = strRead.substr(0, posStr);
                    strId = trimString(strId);

                    if(strcmp(strId.c_str(), OPT_ID_ENTRY) == 0)
                    {
                        m_strOPTid.assign(strSerial);
                    }
                }
            }
        }
    }
    inFile.close();
}

void InstrumentInfo::writeOPTFile()
{
    std::string strId;

    std::ofstream outFile(NSH_OPTID_FILE);

    strId.assign(OPT_ID_ENTRY);
    strId.append("=");
    strId.append(m_strOPTid);
    outFile << strId << std::endl;
}


void InstrumentInfo::setSprMinTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprMin[section] = value;
	}
}

void InstrumentInfo::setSprMaxTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprMax[section] = value;
	}
}

void InstrumentInfo::setSprTargetTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprTarget[section] = value;
	}
}

void InstrumentInfo::setSprToleranceTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_SprTolerance[section] = value;
	}
}

void InstrumentInfo::setTrayMinTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayMin[section] = value;
	}
}

void InstrumentInfo::setTrayMaxTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayMax[section] = value;
	}
}

void InstrumentInfo::setTrayTargetTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayTarget[section] = value;
	}
}

void InstrumentInfo::setTrayToleranceTemperature(uint8_t section, int value)
{
	if(section < SCT_NUM_TOT_SECTIONS)
	{
		m_TrayTolerance[section] = value;
	}
}

void InstrumentInfo::setInternalMinTemperature(int value)
{
	m_InternalMin = value;
}

void InstrumentInfo::setInternalMaxTemperature(int value)
{
	m_InternalMax = value;
}

void InstrumentInfo::setForceTemperature(bool status)
{
	m_bTemperatureForce = status;
}

int InstrumentInfo::getSprMinTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprMin[section];
}

int InstrumentInfo::getSprMaxTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprMax[section];
}

int InstrumentInfo::getSprTargetTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprTarget[section];
}

int InstrumentInfo::getSprToleranceTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_SprTolerance[section];
}

int InstrumentInfo::getTrayMinTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayMin[section];
}

int InstrumentInfo::getTrayMaxTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayMax[section];
}

int InstrumentInfo::getTrayTargetTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayTarget[section];
}

int InstrumentInfo::getTrayToleranceTemperature(uint8_t section)
{
	if(section >= SCT_NUM_TOT_SECTIONS) return 0;

	return m_TrayTolerance[section];
}

int InstrumentInfo::getInternalMinTemperature()
{
	return m_InternalMin;
}

int InstrumentInfo::getInternalMaxTemperature()
{
	return m_InternalMax;
}

bool InstrumentInfo::getForceTemperature()
{
	return m_bTemperatureForce;
}

void InstrumentInfo::readCountersFile()
{
	std::string strRead;
	std::ifstream inFile;

	inFile.open(COUNTERS_SETUP_FILE);
	if(inFile.fail())
	{
		//File does not exist code here
		printf("InstrumentInfo::readCountersFile-->%s", strerror(errno));
		return;
	}

	uint8_t counter = 0;

	while(!inFile.eof() && counter < eCounterMax)
	{
		getline(inFile, strRead);
		if(!strRead.empty())
		{
			// file has a fixed structure -> see the enum in h file
			m_instrumentCounters[counter] = atoi(strRead.c_str());
			counter++;
		}
	}
	inFile.close();
	// the startup time is anyway cleared in order to start from now
	m_instrumentCounters[eCounterSecondsStart] = 0;
}

void InstrumentInfo::writeCountersFile()
{
	std::ofstream outFile(COUNTERS_SETUP_FILE);

	for(uint8_t counter = 0; counter < eCounterMax; counter++)
	{
		outFile << std::to_string(m_instrumentCounters[counter]) << std::endl;
	}
}

bool InstrumentInfo::setInstrumentCounter(eCounterId counter, uint16_t value)
{
	if(counter >= eCounterMax) return false;

	m_instrumentCounters[counter] = value;
	return true;
}

bool InstrumentInfo::addInstrumentCounter(eCounterId counter, uint16_t value)
{
	if(counter >= eCounterMax) return false;

	m_instrumentCounters[counter] += value;
	return true;
}

uint16_t InstrumentInfo::getInstrumentCounter(eCounterId counter)
{
	if(counter >= eCounterMax) return 0;

	if(counter == eCounterSecondsStart)
	{
		// calculate on the fly the startup time
		if(m_clockRatio > 0 && m_clocksStart > 0)
		{
			if(getClockFromStart() > m_clocksStart)
			{
				uint64_t clocksElapsed = (getClockFromStart() - m_clocksStart);
				clocksElapsed /= m_clockRatio;
				m_instrumentCounters[eCounterSecondsStart] = (uint16_t)(clocksElapsed);
			}
		}
	}

	return	m_instrumentCounters[counter];
}

void InstrumentInfo::saveCountersToFile()
{
	writeCountersFile();
}

int8_t InstrumentInfo::getInstrumentCounterIndexFromString(string strCounter)
{
	uint8_t i;

	for(i = 0; i < eCounterMax; i++)
	{
		if(strCounter.compare(m_counterString.at(i)) == 0)
			break;
	}

	if(i == eCounterMax) return -1;

    return i;
}

string InstrumentInfo::getOPTid()
{
    return m_strOPTid;
}

void InstrumentInfo::setOPTid(const string strID)
{
    m_strOPTid.assign(strID);
    // store to file
    writeOPTFile();
}

string InstrumentInfo::getSSvalue()
{
    return m_strSSvalue;
}

string InstrumentInfo::getSSgain()
{
    return m_strSSgain;
}

void InstrumentInfo::setSSvalue(const string strID)
{
    m_strSSvalue.assign(strID);
}

void InstrumentInfo::setSSgain(const string strID)
{
    m_strSSgain.assign(strID);
}
