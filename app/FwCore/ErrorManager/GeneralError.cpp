/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GeneralError.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the GeneralError class.
 @details

 ****************************************************************************
*/

#include "GeneralError.h"

#include "MainExecutor.h"

GeneralError::GeneralError()
{
	m_nLevel = GENERAL_ERROR_LEVEL_NONE;

	m_strDevice.clear();
	m_strFlag.clear();
	m_strCode.clear();
	m_strDescription.clear();
	m_strCategory.clear();

	m_bActive = false;
	m_bEnabled = false;
	m_bRestoreEnabled = false;
}

GeneralError::~GeneralError()
{

}


void GeneralError::setParameters(string strLevel, string strChar, string strDevice,
								 string strDescription, string code, string category,
								 string strEnabled, string strRestoreEnabled)
{

	if(strcmp(strLevel.c_str(), GENERAL_ERROR_LEVEL_STRING_WARNING) == 0)
		m_nLevel = GENERAL_ERROR_LEVEL_WARNING;
	else if(strcmp(strLevel.c_str(), GENERAL_ERROR_LEVEL_STRING_ERROR) == 0)
		m_nLevel = GENERAL_ERROR_LEVEL_ERROR;
	else if(strcmp(strLevel.c_str(), GENERAL_ERROR_LEVEL_STRING_ALARM) == 0)
		m_nLevel = GENERAL_ERROR_LEVEL_ALARM;

	m_strDevice.assign(strDevice);
	m_strFlag.assign(strChar);
	m_strDescription.assign(strDescription);
	m_strCode = code;
	m_strCategory = category;

	if(strcmp(strEnabled.c_str(), "true") == 0)
		m_bEnabled = true;
	else
		m_bEnabled = false;

	m_bActive = false;

	if (strcmp(strRestoreEnabled.c_str(), "true") == 0)
		m_bRestoreEnabled = true;
	else
		m_bRestoreEnabled = false;
}

string GeneralError::getDevice()
{
	return m_strDevice;
}

string GeneralError::getDescription()
{
	return m_strDescription;
}

bool GeneralError::getActive()
{
	return m_bActive;
}

bool GeneralError::getEnabled()
{
	return m_bEnabled;
}

uint8_t GeneralError::getLevel()
{
	return m_nLevel;
}

string GeneralError::getCode()
{
	return m_strCode;
}

bool GeneralError::getRestoreEnabled()
{
	return m_bRestoreEnabled;
}

string GeneralError::getCategory()
{
	return m_strCategory;
}

string GeneralError::getFlag(void)
{
	return m_strFlag;
}

string GeneralError::getStrTime()
{
	return m_strTime;
}

void GeneralError::setStrTime(string strTime)
{
	m_strTime.assign(strTime);
}

void GeneralError::clear()
{
	m_nLevel = GENERAL_ERROR_LEVEL_NONE;
	m_strDevice.clear();
	m_strFlag.clear();
	m_strCode.clear();
	m_strDescription.clear();
	m_strCategory.clear();
	m_bActive = false;
	m_bEnabled = false;
	m_bRestoreEnabled = false;
}

uint64_t GeneralError::getIntTime()
{
	return m_intTime;
}

void GeneralError::setActive(bool status, bool setTime)
{
	m_bActive = status;

	if(m_bActive == false)
	{
		// when the error is cleared also the time info are cleared
		m_strTime.clear();
		m_intTime = 0;
	}
	else
	{
		// when the error is set the msecs from startup are set
		m_intTime = getClockFromStart();

		// if the time has been set by the external command also the time string is set
		if(setTime)
		{
			m_strTime = getFormattedTime(false);
		}
	}
}



