/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    MasterBoardError.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the MasterBoardError class.
 @details

 ****************************************************************************
*/

#ifndef MASTERBOARDERROR_H
#define MASTERBOARDERROR_H

#include "GeneralError.h"

class MasterBoardError : public GeneralError
{
	public:

		/*!
		 * @brief constructor
		 */
		MasterBoardError();

};


#endif // MASTERBOARDERROR_H
