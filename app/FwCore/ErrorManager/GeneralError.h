/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    GeneralError.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the GeneralError class.
 @details

 ****************************************************************************
*/

#ifndef GENERALERROR_H
#define GENERALERROR_H

#include <iostream>
#include <stdint.h>
#include <string.h>


#define GENERAL_ERROR_LEVEL_STRING_NONE		""
#define GENERAL_ERROR_LEVEL_STRING_WARNING	"W"
#define GENERAL_ERROR_LEVEL_STRING_ERROR	"E"
#define GENERAL_ERROR_LEVEL_STRING_ALARM	"A"


using namespace std;

class GeneralError
{
	public:

		/*!
		 * @brief constructor
		 */
		GeneralError();

		/*!
		 * @brief destructor
		 */
		virtual ~GeneralError();

		/*!
		 * @brief setParameters set the error parameters as read from the file
		 * @param strLevel the error level (see enum in SectionInclude.h)
         * @param strDevice string identifying the device
		 * @param strChar first and second chars of the section asynchronous message
		 * @param strDescription the error description as string
		 * @param code the error code to be associated while sending the error to PC
		 * @param category the error category to be associated while sending the error to PC
		 * @param strEnabled true if error is to be send to PC, false if not
		 * @param strRestoreEnabled true if error is a restoring Error typology, false if not
		 */
		void setParameters(string strLevel, string strChar, string strDevice,
						   string strDescription, string code, string category,
						   string strEnabled, string strRestoreEnabled);


		/*!
		 * @brief getter functions
		 *
		 */
		string getDevice(void);
		string getDescription();
		string getFlag(void);
		bool getActive();
		bool getEnabled();
        uint8_t getLevel();
		string getCode();
		bool getRestoreEnabled();
		string getCategory();
		string getStrTime();
		uint64_t getIntTime();

		/*!
		 * @brief setter functions
		 *
		 */

		void setActive(bool status, bool setTime = false);
		void setStrTime(std::string strTime);

		void clear(void);

	protected :

        uint8_t 	m_nLevel;
		string		m_strDescription;
		string		m_strFlag;
		string		m_strDevice;
		string		m_strCode;
		string		m_strCategory;
		string		m_strTime;

		bool		m_bEnabled;
		bool		m_bRestoreEnabled;
		bool		m_bActive;

		uint64_t	m_intTime;
};

#endif // GENERALERROR_H
