/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionError.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the SectionError class.
 @details

 ****************************************************************************
*/

#ifndef SECTIONERROR_H
#define SECTIONERROR_H

#include "GeneralError.h"

class SectionError : public GeneralError
{
	public:

		/*!
		 * @brief constructor
		 */
		SectionError();

		/*!
		 * @brief compareChars compares the 2 chars with m_chars
		 * @param charF1 first char of the section asynchronous message
		 * @param charF2 second char of the section asynchronous message
		 * @return true if the chars are corresponding, false otherwise
		 */
		bool compareChars(uint8_t charF1, uint8_t charF2);


	protected :

		uint8_t		m_charF1, m_charF2;
};

#endif // SECTIONERROR_H
