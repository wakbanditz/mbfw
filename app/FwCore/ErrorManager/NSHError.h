/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    NSHError.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the NSHError class.
 @details

 ****************************************************************************
*/

#ifndef NSHERROR_H
#define NSHERROR_H

#include "GeneralError.h"

class NSHError : public GeneralError
{
	public:

		/*! *************************************************************************************************
		 * @brief NSHError constructor
		 * **************************************************************************************************
		 */
		NSHError();

		/*! *************************************************************************************************
		 * @brief NSHError constructor
		 * **************************************************************************************************
		 */
		virtual ~NSHError();
};

#endif // NSHERROR_H
