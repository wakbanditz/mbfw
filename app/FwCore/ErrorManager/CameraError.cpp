/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CameraError.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the CaneraError class.
 @details

 ****************************************************************************
*/

#include "CameraError.h"

CameraError::CameraError()
{

}

CameraError::~CameraError()
{

}
