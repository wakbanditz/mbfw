/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PressureError.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the PressureError class.
 @details

 ****************************************************************************
*/

#include "PressureError.h"

PressureError::PressureError() : SectionError()
{
	m_channel = SCT_NUM_TOT_SLOTS + ZERO_CHAR;
	m_well = SCT_SLOT_NUM_WELLS + ZERO_CHAR;
}

void PressureError::setErrorInfo(SectionError* pSectionError)
{
	m_nLevel = pSectionError->getLevel();
	m_charF1 = 0;
	m_charF2 = 0;
	m_strDescription.assign(pSectionError->getDescription());
	m_strFlag.assign(pSectionError->getFlag());
	m_strCode = pSectionError->getCode();
	m_strCategory = pSectionError->getCategory();
	m_bActive = pSectionError->getActive();
	m_bEnabled = pSectionError->getEnabled();
	m_strTime.assign(pSectionError->getStrTime());
	m_bRestoreEnabled = false;
}

void PressureError::setPositionInfo(uint8_t channel, uint8_t well)
{
	// Note: these are real chars so 1 is '1'
	m_channel = channel;
	m_well = well;
}

uint8_t PressureError::getChannel()
{
	return m_channel;
}

uint8_t PressureError::getWell()
{
	return m_well;
}
