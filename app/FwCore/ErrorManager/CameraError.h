/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    CameraError.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the CameraError class.
 @details

 ****************************************************************************
*/

#ifndef CAMERAERROR_H
#define CAMERAERROR_H

#include "GeneralError.h"

class CameraError : public GeneralError
{

	public:

		/*! *************************************************************************************************
		 * @brief CameraError constructor
		 * **************************************************************************************************
		 */
		CameraError();

		/*! *************************************************************************************************
		 * @brief CameraError destructor
		 * **************************************************************************************************
		 */
		virtual ~CameraError();

};

#endif // CAMERAERROR_H
