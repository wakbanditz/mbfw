/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ErrorUtilities.h
 @author  BmxIta FW dept
 @brief   Contains the functions for the Error utilities.
 @details

 ****************************************************************************
*/

#ifndef ERRORUTILITIES_H
#define ERRORUTILITIES_H

#include <vector>
#include <string>

#include "GeneralError.h"
#include "SectionError.h"
#include "PressureError.h"
#include "MasterBoardError.h"
#include "CameraError.h"
#include "NSHError.h"

enum
{
	eErrorGeneral = 0,
    eErrorSection,
    eErrorPressure,
    eErrorMaster,
    eErrorCamera,
    eErrorNsh,
    eErrorNone
};


/*!
 * @brief splitString	split the input string according to the requested delimiter filling a string vector
 * @param phrase		the input string to be splitted
 * @param delimiter		the separator string
 * @return				the string vector
 */
std::vector<std::string> splitString(std::string phrase, std::string delimiter);

/*!
 * @brief trimString	remove starting and ending spaces from the input string
 * @param phrase		the input string to be trimmed
 * @return				the trimmed string
 */
std::string trimString(std::string phrase);

/*!
 * @brief formatTimeStamp	reformat the string yyyymmddhhmmss as yyyy-mm-ddThh:mm:ss
 * @param strTime		the input string to be formatted
 * @return				the formatted string
 */
std::string formatTimestamp(std::string strTime);

/*!
 * @brief parseErrorFile	The functions parse the content of the ERR file defining the list of errors
 *					and the associated parameters for the requested device
 * @param strDevice			the device string
 * @param pVectorErrors		pointer to the error vector corresponding to the file
 * @param errorType the type of error (-> the type of object to be added to the vector) according to the enum above
 * @return					the size of the vector
 */
int parseErrorFile(std::string strDevice,
				   std::vector< GeneralError * > * pVectorErrors, uint8_t errorType);

/*!
 * @brief writeOPTCheckFile	The function writes to the OPTCHECK.cfg file the status
 * @param status			the OPTCHECK status (true if error)
 * @return					true if succesfull, false otherwise
 */
bool writeOPTCheckFile(bool status);

/*!
 * @brief readOPTCheckFile	The function read from the OPTCHECK.cfg file the status
 * @return			true if the OPTCHECK status is error
 */
bool readOPTCheckFile();

#endif // ERRORUTILITIES_H
