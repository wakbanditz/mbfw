INCLUDEPATH += $$PWD/../ErrorManager/

HEADERS += \
    $$PWD/SectionError.h \
    $$PWD/PressureError.h \
    $$PWD/ErrorUtilities.h \
    $$PWD/GeneralError.h \
    $$PWD/MasterBoardError.h \
    $$PWD/NSHError.h \
    $$PWD/CameraError.h

SOURCES += \
    $$PWD/SectionError.cpp \
    $$PWD/PressureError.cpp \
    $$PWD/ErrorUtilities.cpp \
    $$PWD/GeneralError.cpp \
    $$PWD/MasterBoardError.cpp \
    $$PWD/NSHError.cpp \
    $$PWD/CameraError.cpp
