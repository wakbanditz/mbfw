/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PressureError.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the PressureError class.
 @details

 ****************************************************************************
*/

#ifndef PRESSUREERROR_H
#define PRESSUREERROR_H

#include "SectionError.h"
#include "CommonInclude.h"

class PressureError : public SectionError
{
	public:

		PressureError();

		/*!
		 * @brief setErrorInfo set the error parameters copied form the SectionError object
		 * @param pSectionError pointer to the equivalent SectionError object
		 */
		void setErrorInfo(SectionError * pSectionError);

		/*!
		 * @brief setPositionInfo set the channel/well info where the pressure error occurred
		 * @param channel the channel (strip) index
		 * @param well the well index
		 */
		void setPositionInfo(uint8_t channel, uint8_t well);

		/*!
		 * @brief getChannel get the channel index
		 * @return the channel index ('0' - '5')
		 */
		uint8_t getChannel();

		/*!
		 * @brief getWell get the well index
		 * @return the well index ('0' - 'A')
		 */
		uint8_t getWell();

	private :

		uint8_t m_channel;
		uint8_t m_well;
};

#endif // PRESSUREERROR_H
