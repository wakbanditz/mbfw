/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ErrorUtilities.cpp
 @author  BmxIta FW dept
 @brief   Contains the support functions for the ErrorManager classes.
 @details

 ****************************************************************************
*/

#include <fstream>

#include "CommonInclude.h"
#include "ErrorUtilities.h"


// ---------------------------------------------------------------------------------- //

int parseErrorFile(std::string strDevice,
				   std::vector< GeneralError * > * pVectorErrors, uint8_t errorType)
{
	std::string fileName;
	std::string strRead;
	std::ifstream inFile;

	GeneralError * pGeneralError;
	SectionError * pSectionError;
	PressureError * pPressureError;
	MasterBoardError * pMasterError;
	CameraError* pCameraError;
	NSHError* pNSHError;

	std::string fileNameBase;

	switch(errorType)
	{
		case eErrorGeneral :
            fileNameBase.assign(GENERAL_ERROR_FILE_NAME);
		break;
		case eErrorSection :
            fileNameBase.assign(SECTION_ERROR_FILE_NAME);
		break;
		case eErrorPressure :
            fileNameBase.assign(PRESSURE_ERROR_FILE_NAME);
		break;
		case eErrorMaster :
            fileNameBase.assign(MASTER_ERROR_FILE_NAME);
		break;
		case eErrorCamera:
            fileNameBase.assign(CAMERA_ERROR_FILE_NAME);
		break;
		case eErrorNsh:
            fileNameBase.assign(NSH_ERROR_FILE_NAME);
		break;

		case eErrorNone :
		default:
			return 0;
		break;
	}

    fileName.assign(fileNameBase);

	inFile.open(fileName);
	if(inFile.fail())
	{
		return 0;
	}

	while(!inFile.eof())
	{
		getline(inFile, strRead);
		if(!strRead.empty())
		{
			// lines beginning with # are comments
			if(strRead.front() != '#')
			{
                // split the line separating the fields
				std::vector<std::string> listStr;

				listStr = splitString(strRead, HASHTAG_SEPARATOR);
				if(listStr.size() == 8)
				{
					// split is valid: compare the device
					if(strcmp(listStr.at(0).c_str(), strDevice.c_str()) == 0)
					{
						switch(errorType)
						{
							case eErrorGeneral :
								pGeneralError = new GeneralError();
								pGeneralError->setParameters(listStr.at(2),
																 listStr.at(1),
																 listStr.at(0),
																 listStr.at(5),
																 listStr.at(3),
																 listStr.at(4),
																 listStr.at(6),
																 listStr.at(7));
								pVectorErrors->push_back(pGeneralError);
							break;

							case eErrorSection :
								pSectionError = new SectionError();
								pSectionError->setParameters(listStr.at(2),
																 listStr.at(1),
																 listStr.at(0),
																 listStr.at(5),
																 listStr.at(3),
																 listStr.at(4),
																 listStr.at(6),
																 listStr.at(7));
								pVectorErrors->push_back(pSectionError);
							break;

							case eErrorPressure :
								pPressureError = new PressureError();
								pPressureError->setParameters(listStr.at(2),
																 listStr.at(1),
																 listStr.at(0),
																 listStr.at(5),
																 listStr.at(3),
																 listStr.at(4),
																 listStr.at(6),
																 listStr.at(7));
								pVectorErrors->push_back(pPressureError);
							break;

							case eErrorMaster :
								pMasterError = new MasterBoardError();
								pMasterError->setParameters(listStr.at(2),
																 listStr.at(1),
																 listStr.at(0),
																 listStr.at(5),
																 listStr.at(3),
																 listStr.at(4),
																 listStr.at(6),
																 listStr.at(7));
								pVectorErrors->push_back(pMasterError);
							break;

							case eErrorCamera :
								pCameraError = new CameraError();
								pCameraError->setParameters(listStr.at(2),
															listStr.at(1),
															listStr.at(0),
															listStr.at(5),
															listStr.at(3),
															listStr.at(4),
															listStr.at(6),
															listStr.at(7));
								pVectorErrors->push_back(pCameraError);
							break;

							case eErrorNsh :
								pNSHError = new NSHError();
								pNSHError->setParameters(listStr.at(2),
														 listStr.at(1),
														 listStr.at(0),
														 listStr.at(5),
														 listStr.at(3),
														 listStr.at(4),
														 listStr.at(6),
														 listStr.at(7));
								pVectorErrors->push_back(pNSHError);
							break;

							case eErrorNone :
							default:
								continue;
							break;
						}

					}
				}

			}
		}
	}
	inFile.close();

	return pVectorErrors->size();
}

bool writeOPTCheckFile(bool status)
{
    FILE* fp;

    fp = fopen(NSH_OPTCHECK_FILE, "w");
    if ( fp == NULL )
    {
        return false;
    }

    if(status)
    {
        fprintf(fp, "OPTCheck = KO\n");
    }
    else
    {
        fprintf(fp, "OPTCheck = OK\n");
    }

    fflush(fp);
    fclose(fp);
    return true;
}

bool readOPTCheckFile()
{
    FILE* fp;
    char buffer[128];
    char * pch = 0;

    buffer[0] = 0;

    fp = fopen(NSH_OPTCHECK_FILE, "r");
    if ( fp != NULL )
    {
        fread(buffer, sizeof(char), sizeof(buffer) - 1, fp);
        fclose(fp);
    }

    // if OPTCHECK is in error ...
    if(buffer[0])
    {
        pch = strstr(buffer, "KO");
    }

    // return true if in error
    return (pch != 0);
}

std::vector<std::string> splitString(std::string phrase, std::string delimiter)
{
	std::vector<std::string> list;

	std::string s;
	s.assign(phrase);

	size_t pos = 0;
	std::string token;

	while ((pos = s.find(delimiter)) != string::npos)
	{
		token = s.substr(0, pos);
		list.push_back(trimString(token));
		s.erase(0, pos + delimiter.length());
	}
	list.push_back(trimString(s));
	return list;
}

std::string trimString(std::string phrase)
{
	std::string strRet;

	strRet.assign(phrase);

	// trim at beginning
    while(strRet.front() == ' ' || strRet.front() == '\t') strRet.erase(0, 1);

	// trim at end
    while(strRet.back() == ' ' || strRet.back() == '\t') strRet.erase(strRet.length() - 1, 1);

	return strRet;
}

std::string formatTimestamp(std::string strTime)
{
	std::string timestamp;
	timestamp.clear();

	if(strTime.size() != 14)
	{
		return timestamp;
	}

	// build timestamp as yyyy-mm-ddThh:mm:ss
	timestamp.assign(strTime);
	timestamp.insert(4, "-");
	timestamp.insert(7, "-");
	timestamp.insert(10, "T");
	timestamp.insert(13, ":");
	timestamp.insert(16, ":");
	return timestamp;
}
