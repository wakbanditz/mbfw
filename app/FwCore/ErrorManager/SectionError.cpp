/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionError.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the SectionError class.
 @details

 ****************************************************************************
*/

#include "SectionError.h"

SectionError::SectionError()  : GeneralError()
{
	m_charF1 = 0;
	m_charF2 = 0;
}


bool SectionError::compareChars(uint8_t charF1, uint8_t charF2)
{
	m_charF1 = m_strFlag.at(0);
	m_charF2 = m_strFlag.at(1);

	if(charF1 == m_charF1 && charF2 == m_charF2)
		return true;

	return false;
}
