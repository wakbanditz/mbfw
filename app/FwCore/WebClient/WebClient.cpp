/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WebClient.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementation for the WebClient class.
 @details

 ****************************************************************************
*/

#include "WebClient.h"

WebClient::WebClient()
{

}

WebClient::~WebClient()
{

}

bool WebClient::init()
{
	CURLcode res = curl_global_init(CURL_GLOBAL_ALL);
	if ( res != CURLE_OK )
		return false;

	// Inizializzare gli header che devono essere mandati in tutti i messaggi

	return true;
}

bool WebClient::sendMessageToAllLoggedUsers(std::string& strMessage)
{
	(void)(strMessage);

	return true;
}
