/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    WebClient.h
 @author  BmxIta FW dept
 @brief   Contains the declaration for the WebClient class.
 @details

 ****************************************************************************
*/

#ifndef WEBCLIENT_H
#define WEBCLIENT_H
#include <string>

#include "StaticSingleton.h"
#include "Loggable.h"
#include "HttpConnection.h"

class WebClient :	public StaticSingleton<WebClient>,
					public Loggable

{
	public:

        /*! ***********************************************************************************************************
         * @brief constructor
         * ************************************************************************************************************
         */
        WebClient();

        /*! ***********************************************************************************************************
         * @brief destructor
         * ************************************************************************************************************
         */
        virtual ~WebClient();

        /*! ***********************************************************************************************************
         * @brief initialization
         * @return true if success
         * ************************************************************************************************************
         */
        bool init();

        /*! ***********************************************************************************************************
         * @brief to send the message to all connected users
         * @param strMessage the message string
         * @return true if success
         * ************************************************************************************************************
         */
        bool sendMessageToAllLoggedUsers(std::string& strMessage);

    private:

        HeaderFields m_headerFields;

};

#endif // WEBCLIENT_H
