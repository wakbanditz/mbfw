INCLUDEPATH += $$PWD/../WebClient
INCLUDEPATH += $$PWD/../WebServer
INCLUDEPATH += $$PWD/../WebServer/HttpPages
INCLUDEPATH += $$PWD/../WebServer/Parser
INCLUDEPATH += $$PWD/../WebServer/Utilities
INCLUDEPATH += $$PWD/../WebServer/Commands/Incoming
INCLUDEPATH += $$PWD/../WebServer/Commands/Outgoing

INCLUDEPATH += $$PWD/../Workflow

HEADERS += \ 
    $$PWD/WebClient.h
SOURCES += \ 
    $$PWD/WebClient.cpp
