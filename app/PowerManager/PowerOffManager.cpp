/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PowerOffManager.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the PowerOffManager class.
 @details

 ****************************************************************************
*/

#include <linux/reboot.h>
#include <sys/reboot.h>
#include "PowerOffManager.h"
#include "Log.h"

PowerOffManager::PowerOffManager()
{
    m_pGpio = NULL;
//    m_nMasterOffIdx = 0;
    m_nItrPwrManIdx = 0;
    m_eState = ePOFFWaitIrq;
    m_bshutdownState = false;
}

PowerOffManager::~PowerOffManager()
{

}

int PowerOffManager::initPMManager(const char* sConfigFileName, GpioInterface *gpioInterface, PowerManagerDevice &device, StatusLedDevice &ledDev)
{
    if (gpioInterface == NULL)
    {
        log(LOG_ERR, "PowerOffManager::initPowerOffManager--> Enable to Init PowerOffManager");
        return -1;
    }

    m_pGpio = gpioInterface;
    m_ppowermanagerDevice = device;
    m_pledDevice = ledDev;

    // Reads configuration file
    config_key_t config_keys[] =
    {
        // Single SPI configuration keys
        { PM_CFG_POFF_GPIO_ITR, (char*)"poff.itr.gpioIdx", (char*)"GENERAL", T_int, (char*)PM_POFF_DEF_GPIO_IDX_ITR, DEFAULT_ACCEPTED},
        { PM_CFG_POFF_GPIO_MOFF, (char*)"poff.out.gpioIdx", (char*)"GENERAL", T_int, (char*)PM_POFF_DEF_GPIO_IDX_OUT, DEFAULT_ACCEPTED},
    };

    int num_keys = sizeof(config_keys) / sizeof(config_key_t);

    if ( !m_Config.Init(config_keys, num_keys, sConfigFileName) )
    {
        char error[1024];
        Config::GetLastError(error, 1024);
        log(LOG_ERR, "Impossible to read parameter from <%s> (error: <%s>)", sConfigFileName, error);
        m_Config.Free();
        return false;
    }

    m_nItrPwrManIdx = m_Config.GetInt(PM_CFG_POFF_GPIO_ITR);

    return 0;
}

int PowerOffManager::checkItrPwrManager(uint32_t uliTimeOutMsec)
{

    if ( !m_pGpio->waiForIrq(m_nItrPwrManIdx, uliTimeOutMsec)  )
    {
        log(LOG_DEBUG_PARANOIC, "PowerOffManager::checkItrPwrManager--> TIMEOUT ITR");
        return -1;
    }

    uint32_t ulTemp;
    m_ppowermanagerDevice.readStatusRegister(ulTemp);
    log(LOG_DEBUG_PARANOIC,"Read Status Register: 0x%X", ulTemp);

    //Test with GPI3-->shutdown pin
    if (!(ulTemp & MASK_MFR_STATUS_GPI3))
    {
        log(LOG_DEBUG, "PowerOffManager::checkItrPwrManager--> got ITR not on GPI3 pin  \n");
        int res = m_ppowermanagerDevice.writeClearFaults();
        if (res == 0)
        {
            log(LOG_DEBUG, "PowerOffManager::checkItrPwrManager--> clear faults");
        }
        else
        {
           log(LOG_ERR, "PowerOffManager::checkItrPwrManager--> clear faults err");
        }
        return -1;
    }

    log(LOG_INFO,"PowerOffManager::checkItrPwrManager-->GPI3 got it");
    return 0;
}

bool PowerOffManager::getShutdownRequest(void)
{
    return m_bshutdownState;
}


void PowerOffManager::shutdownBoard()
{
    sync();
    system("poweroff");
}

int PowerOffManager::workerThread(void)
{
    while (isRunning())
    {
        switch (m_eState)
        {
            case ePOFFWaitIrq:
            {
                int res = checkItrPwrManager();
                if (res == 0)
                {
                    // m_pledDevice.setLed(eModuleLedBlink);
                    m_eState = ePOFFResetFlag;
                    log(LOG_INFO, "PowerOffManager::workerThread--> go to state %s", "ePOFFResetFlag");
                }
                //msleep(1000);
            }
            break;

            case ePOFFResetFlag:
            {
                //Send command to PWR MANAGER to reset all flags
                int res;
                res = m_ppowermanagerDevice.writeClearFaults();
                if (res == 0)
                {
                    m_eState = ePOFFShutdown;
                    log(LOG_INFO,"PowerOffManager::workerThread--> go to state %s", "ePOFFShutdown");
                }
                else
                {
                    m_eState = ePOFFWaitIrq;
                    log(LOG_ERR,"PowerOffManager::workerThread--> go to state %s", "ePOFFWaitIrq");
                }

                //msleep(1000);
            }
            break;

            case ePOFFSendMsgToGW:
                //TODO
            break;

            case ePOFFWaitMsgFromGW:
                //TODO
            break;

            case ePOFFCtrlGWOff:
                //TODO
            break;

            case ePOFFShutdown:
                log(LOG_ERR,"PowerOffManager::workerThread--> send SHUTDOWN request!!");
                //msleep(1000);
                m_bshutdownState = true;
            break;

            default:
                //TODO
            break;
        }

        usleep(100);
    }
    return 0;
}

void PowerOffManager::beforeWorkerThread(void)
{
    log(LOG_DEBUG, "PowerOffManager::started");
}

void PowerOffManager::afterWorkerThread(void)
{
    log(LOG_DEBUG, "PowerOffManager::stopped");
}

