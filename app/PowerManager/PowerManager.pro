TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#internal libraries inclusion
unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/GpioInterface/ -lGpioInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/GpioInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/GpioInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/GpioInterface/libGpioInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/I2CInterface/ -lI2CInterface
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/I2CInterface
DEPENDPATH += $$PWD/../../bmx_libs/Libs/I2CInterface
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/I2CInterface/libI2CInterface.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Thread
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Thread/libThread.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Log
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Config/ -lConfig
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Config
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Config/libConfig.a



INCLUDEPATH += $$PWD/../../

LIBS += -pthread -lrt -lpthread

SOURCES += \
        PowerManagerDevice.cpp \
        PowerOffManager.cpp \
        StatusLedDevice.cpp \
        main.cpp

HEADERS += \
    CommonIncludePowerManager.h \
    PowerManagerDevice.h \
    PowerOffManager.h \
    StatusLedDevice.h

TARGET = PowerManager

# deployment directives
target.path = /home/root
INSTALLS += target


