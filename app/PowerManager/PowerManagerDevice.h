/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PowerManagerDevice.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the PowerManagerDevice class.
 @details

 ****************************************************************************
*/

#ifndef POWERMANAGERDEVICE_H
#define POWERMANAGERDEVICE_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include "Thread.h"
#include "CommonInclude.h"

using namespace std;

class I2CInterface;
class Log;

class PowerManagerDevice
{
    Log				* m_pLogger;
    I2CInterface	* m_pI2CIfr;
    int32_t			m_slDeviceAddress;

    public:
        PowerManagerDevice();
        ~PowerManagerDevice();

        /*****************************************************************************************************
         *
         * INIT FUNCTIONS
         *
         * ***************************************************************************************************/
        /**
         * @brief init	Initialization of the logger object and I2CInterface for data write and read.
         * @param pLogger
         * @param pI2CIfr
         * @return 0 if success, -1 otherwise
         */
        int		init( Log * pLogger, I2CInterface * pI2CIfr);

        /**
         * @brief setDeviceAddress	The function sets the I2C device address.
         * @param slDeviceAddress
         * @return 0 if success, -1 otherwise
         */
        int		setDeviceAddress( int32_t slDeviceAddress );

        /**
         * @brief getDeviceAddress The function retrieves the specific device address.
         * @param slDeviceAddress
         * @return 0 if success, -1 otherwise
         */
        int		getDeviceAddress( int32_t &slDeviceAddress );

        /*********************************************************************************************************
         *
         * WRITE operations
         *
         *********************************************************************************************************/

        /**
         * @brief resetSoft	This Write-Only Send Byte command restarts the controller firmware. Any active voltage outputs are
         *					turned off before the firmware restarts.
         * @return 0 if success | -1 otherwise
         */
        int writeResetSoft( void );

        /**
         * @brief writeClearFaults	This function is used for clear the flag in the faults register
         * @return if success | -1 otherwise
         */
        int writeClearFaults( void );

        /*********************************************************************************************************
         *
         * READ operations
         *
         *********************************************************************************************************/

        /**
         * @brief readDeviceID Retrieve the device ID, the string contains the model and the FW version of the device.
         * @param pubDeviceID
         * @return 0 if success | -1 otherwise
         */
        int readDeviceID( string &pubDeviceID );

        /*!
         * \brief readStatusRegister    Read status register
         * \param ubVal         Status register
         * \return 0 if success | -1 otherwise
         */
        int readStatusRegister(uint32_t &ubVal);

    private:
        /*****************************************************************************************************
         * GENERAL FUNCTIONS
         * ***************************************************************************************************/
        /**
         * @brief write
         * @param ubCmd
         * @param pubTxBuff
         * @param usLen
         * @return 0 if success, -1 otherwise
         */
        int write(uint8_t ubCmd, uint8_t *pubTxBuff, uint16_t usLen);

        /**
         * @brief writeCustomAdd	The function allows to send
         * @param ubAdd
         * @param ubCmd
         * @param pubTxBuff
         * @param usLen
         * @return 0 if success, -1 otherwise
         */
        int writeCustomAdd(uint8_t ubAdd, uint8_t ubCmd, uint8_t *pubTxBuff, uint16_t usLen);

        /**
         * @brief writeCustomAdd
         * @param ubAdd
         * @param ubCmd
         * @return 0 if success, -1 otherwise
         */
        int writeCustomAdd(uint8_t ubAdd, uint8_t ubCmd);

        /**
         * @brief read
         * @param ubCmd
         * @param pubRxBuff
         * @param usLen
         * @return 0 if success, -1 otherwise
         */
        int read(uint8_t ubCmd, uint8_t *pubRxBuff, uint16_t usLen);
};

#endif // POWERMANAGERDEVICE_H
