/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    main.cpp
 @author  BmxIta FW dept
 @brief   Contains the main function.
 @details

 ****************************************************************************
*/

#include <iostream>
#include <signal.h>

#include "Log.h"
#include "Loggable.h"
#include "GpioInterface.h"
#include "I2CInterface.h"
#include "PowerManagerDevice.h"
#include "PowerOffManager.h"
#include "StatusLedDevice.h"
#include "CommonIncludePowerManager.h"

#define PM_CONFIG_FILE	"/home/root/mbpm.config"
#define PROGRAM_NAME    "PowerManager"
#define LOG_FILE        "/home/root/APPLOGS/mbpm.log"
#define LOG_LEVEL       7
#define LOG_NUM_FILES   1

#define PM_I2C_INTERFACE    "/dev/i2c-2"
#define SL_I2C_INTERFACE    "/dev/i2c-1"
#define STATUSLED_DEVICE_ADDRESS         0x65

using namespace std;

static bool bKeepRunning = true;

void error(char* sMsg)
{
    perror(sMsg);
    exit(1);
}

void signalHandler(int nSignal)
{
    static int sig_counter = 1;
    printf("Received signal %d\n", nSignal);
    switch ( nSignal )
    {
        case SIGTERM:
        case SIGINT:
        case SIGKILL:
            bKeepRunning = false;
            if ( ++sig_counter > 5 )
            {
                // Fifth time that the soft mode has failed brute exits
                printf("Brute force exit ...");
                exit(1);
            }
        break;
        case SIGTSTP:
            bKeepRunning = false;
        break;
    }
}

int main()
{
    signal(SIGINT, signalHandler);
    signal(SIGKILL, signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGTSTP, signalHandler);

    /* ********************************************************************************************
     * LOGGER INITIALIZATION
     * ********************************************************************************************
     */
    Log logger;
//    int liLogEnable = configurator.GetInt(CFG_CAN_MANAGER_LOG_ENABLE);
//    bool bEnable = (liLogEnable > 0 ? true : false);
    bool bEnable = true;
    logger.enable(bEnable);
    logger.setAppName(PROGRAM_NAME);
    logger.setFile(LOG_FILE);
    logger.setLevel(LOG_LEVEL);
    logger.setMaxNumFiles(LOG_NUM_FILES);
    logger.log(LOG_INFO, "Starting << %s >>", PROGRAM_NAME);

    /* ********************************************************************************************
     * INTERFACE INITIALIZATION
     * ********************************************************************************************
     */
    // Gpio Interface
    GpioInterface gpioInterface;
    gpioInterface.setLogger(&logger);
    if( ! gpioInterface.init(PM_CONFIG_FILE) )
    {
        logger.log(LOG_ERR, "Main: error, unable to initialize power manager gpioInterface");
        return 0;
    }
    logger.log(LOG_INFO, "Main::initGpioInterface: done");

    I2CInterface	itfI2C;
    itfI2C.setLogger(&logger);
    bool bRes = itfI2C.init(PM_I2C_INTERFACE, false, false);
    if ( bRes == false )
    {
        logger.log(LOG_ERR, "Main:: unable to init %s",PM_I2C_INTERFACE);
        return -1;
    }

    I2CInterface	itfI2CStatusLed;
    itfI2CStatusLed.setLogger(&logger);
    bRes = itfI2CStatusLed.init(SL_I2C_INTERFACE, false, false);
    if ( bRes == false )
    {
        logger.log(LOG_ERR, "Main:: unable to init %s",SL_I2C_INTERFACE);
        return -1;
    }
    /* ********************************************************************************************
     * STATUS LED INITIALIZATION
     * ********************************************************************************************
     */
    StatusLedDevice LedDriver;
    LedDriver.setLogger(&logger);
    LedDriver.init(&itfI2CStatusLed);
    LedDriver.setDeviceAddress(STATUSLED_DEVICE_ADDRESS);
    LedDriver.setLed(eModuleLedSteady);

    /* ********************************************************************************************
     * POWER MANAGER INITIALIZATION
     * ********************************************************************************************
     */
    PowerManagerDevice  powerManager;
    int ret = powerManager.init(&logger, &itfI2C);
    if ( ret != 0 )
    {
        logger.log(LOG_ERR, "Main: unable to initialize PowerManager");
        return -1;
    }

    ret = powerManager.setDeviceAddress(I2C_TEST_UCD9090_DEVICE_ADDRESS);
    if ( ret != 0 )
    {
        logger.log(LOG_ERR, "Main: unable to set PowerManager I2C address");
        return -1;
    }

    string strID;
    ret = powerManager.readDeviceID(strID);
    if (ret == 0)
    {
        logger.log(LOG_INFO, "Main: device ID PowerManager:%s", strID.c_str());
    }
    else
    {
        return -1;
    }

    ret = powerManager.writeClearFaults();
    if (ret != 0)
    {
        logger.log(LOG_ERR, "Main: clear faults ERROR");
        return -1;
    }
    logger.log(LOG_INFO, "Main--> Power Manager device correctly initialized ");

    PowerOffManager managerPM;
    managerPM.setLogger(&logger);
    ret = managerPM.initPMManager(PM_CONFIG_FILE, &gpioInterface, powerManager, LedDriver);
    if ( ret != 0 )
    {
        logger.log(LOG_ERR, "Main: unable to init PowerOffManager");
        return -1;
    }

    if (!managerPM.startThread())
    {
        logger.log(LOG_ERR, "Main: unable to start thread PowerOffManager");
        return -1;
    }
    logger.log(LOG_INFO, "Main--> PowerOffManager correctly initialized ");

    // wait for all tasks and threads to be started ...
    usleep(100);

    /* ********************************************************************************************
     * MAIN Application
     * ********************************************************************************************
     */
    while (bKeepRunning)
    {
        //getshutdown
        if (managerPM.getShutdownRequest())
        {            
            LedDriver.setLed(eModuleLedBlink);
            bKeepRunning = false;
        }
        usleep(200);
    }

    /* ********************************************************************************************
     * END application
     * ********************************************************************************************
     */
    managerPM.stopThread();
    logger.log(LOG_INFO, "Closing Power Manager application");
    if (managerPM.getShutdownRequest())
    {
        managerPM.shutdownBoard();
    }
    return 0;
}
