#ifndef STATUSLEDDEVICE_H
#define STATUSLEDDEVICE_H

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#include "stdint.h"
#include "Log.h"
#include "Loggable.h"
#include "CommonIncludePowerManager.h"

#define STATLED_MODE2_BLNK_CTL			0x01
#define STATLED_MODE2_DIM_CTL			0x02

#define PCA9952_MODE1					0x00
#define PCA9952_MODE2					0x01
#define PCA9952_LEDOUT0					0x02
#define PCA9952_LEDOUT1					0x03
#define PCA9952_LEDOUT2					0x04
#define PCA9952_LEDOUT3					0x05

#define PCA9952_GRPPWM					0x06
#define PCA9952_GRPFREQ					0x07

typedef enum
{
    eModuleLedOff = 0,
    eModuleLedSteady,
    eModuleLedBlink
}eModuleLedMode;

class I2CInterface;
class Log;

class StatusLedDevice : public Loggable
{
    private:
        I2CInterface	* m_pI2CIfr;
        int32_t			m_slDeviceAddress;

    public:
        StatusLedDevice();
         virtual ~StatusLedDevice();

        int		init(I2CInterface * pI2CIfr);
        int setDeviceAddress( int32_t slDeviceAddress );
        int		getDeviceAddress( int32_t &slDeviceAddress );

        int write(uint8_t ubCmd, uint8_t *pubTxBuff, uint16_t usLen);

        bool setLed(eModuleLedMode mode);

        int config(uint8_t ubDmBlinkState);
        int writeMode1(uint8_t ubMode);
        int writeMode2(uint8_t ubMode);
        int writeLedOut(uint8_t ubNumLed, uint8_t ubState);
};

#endif // STATUSLEDDEVICE_H
