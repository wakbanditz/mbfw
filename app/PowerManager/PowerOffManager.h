/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PowerOffManager.h
 @author  BmxIta FW dept
 @brief   Contains the declarations for the PowerOffManager class.
 @details

 ****************************************************************************
*/

#ifndef POWEROFFMANAGER_H
#define POWEROFFMANAGER_H

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#include "stdint.h"
#include "Thread.h"
#include "GpioInterface.h"
#include "Log.h"
#include "Loggable.h"
#include "PowerManagerDevice.h"
#include "StatusLedDevice.h"
#include "CommonIncludePowerManager.h"

typedef enum
{
    ePOFFWaitIrq			= 0,
    ePOFFResetFlag			= 1,
    ePOFFSendMsgToGW		= 2,
    ePOFFWaitMsgFromGW		= 3,
    ePOFFCtrlGWOff			= 4,
    ePOFFShutdown			= 5
}ePowerOffManagerStatus;

#define POFF_TIMEOUT_WAIT_ITR	10000

/**
 * @brief The PowerOffManager class is dedicated for the management of the power off procedure.
 */
class PowerOffManager : public Thread,
                        public Loggable
{
    public:

        /** ***********************************************************************************************************
         * @brief PowerOffManager constructor
         * ************************************************************************************************************
         */
        PowerOffManager();

        /** ***********************************************************************************************************
         * @brief PowerOffManager destructor
         * ************************************************************************************************************
         */
        virtual ~PowerOffManager();

        /**
         * @brief initPowerOffManager It initializes gpioInterface, already present in MainExecutor and set the gpios
         *		index values.
         * @param gpioInterface
         * @param device
         * @param sConfigFileName
         * @param ledDev
         * @return 0 in case of success, -1 in case of error.
         */
        int initPMManager(const char* sConfigFileName, GpioInterface *gpioInterface, PowerManagerDevice &device, StatusLedDevice &ledDev);

        /**
         * @brief shutdownBoard It calls the library function to invoke the shutdown of the system
         */
        void shutdownBoard(void);

        /**
         * @brief getShutdownRequest    It provides if the board should should be turned off
         * @return true if request is ok | false
         */
        bool getShutdownRequest(void);

        /**
         * @brief checkItrPwrManager Checks if the relates interrupt is triggered on the requested pin.
         * @param uliTimeOutMsec
         * @return
         */
        int checkItrPwrManager(uint32_t uliTimeOutMsec = POFF_TIMEOUT_WAIT_ITR);


    protected:

        /** ***********************************************************************************************************
         * @brief workerThread neverending thread normally waiting on the semaphore : when the semaphore it's released
         *		the thread executes the procedure
         * @return 0 when thread ends
         * ***********************************************************************************************************
         */
        int workerThread(void);

        /** ***********************************************************************************************************
         * @brief beforeWorkerThread to perform actions before the start of the thread
         * ***********************************************************************************************************
         */
        void beforeWorkerThread(void);

        /** ***********************************************************************************************************
         * @brief beforeWorkerThread to perform actions after the end of the thread
         * ***********************************************************************************************************
         */
        void afterWorkerThread(void);

    private:

        GpioInterface           *m_pGpio;
        PowerManagerDevice      m_ppowermanagerDevice;
        StatusLedDevice         m_pledDevice;
        int                  m_nItrPwrManIdx;
        ePowerOffManagerStatus	m_eState;
        Config                  m_Config;
        bool                    m_bshutdownState;

};


#endif // POWEROFFMANAGER_H
