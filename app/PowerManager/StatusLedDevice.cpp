#include "StatusLedDevice.h"
#include "I2CInterface.h"
#include "CommonIncludePowerManager.h"

StatusLedDevice::StatusLedDevice()
{
    m_pI2CIfr = NULL;
}

StatusLedDevice::~StatusLedDevice()
{

}

int StatusLedDevice::init( I2CInterface * pI2CIfr )
{
    if ( pI2CIfr == NULL )
    {
        return -1;
        m_pLogger->log( LOG_ERR, "StatusLedDevice::init--> ERROR");
    }
    else
    {
        m_pI2CIfr = pI2CIfr;

        m_pLogger->log( LOG_DEBUG_PARANOIC, "StatusLedDevice::init--> DONE");
    }

    return 0;
}

int StatusLedDevice::setDeviceAddress( int32_t slDeviceAddress )
{
    if ( slDeviceAddress > 0 )
    {
        m_slDeviceAddress = slDeviceAddress;
        m_pLogger->log( LOG_INFO, "PowerManagerDevice::setDeviceAddress--> set Address 0x%X", m_slDeviceAddress);
    }
    else
    {
        m_pLogger->log(LOG_ERR, "PowerManagerDevice::setDeviceAddress-->ERR");
        return -1;
    }

    return 0;
}

int StatusLedDevice::getDeviceAddress( int32_t &slDeviceAddress )
{
    if ( m_slDeviceAddress >= 0 )
    {
        slDeviceAddress = m_slDeviceAddress;
    }
    else
    {
        m_pLogger->log(LOG_ERR, "PowerManagerDevice::getDeviceAddress-->ERR");
        return -1;
    }

    return 0;
}

int StatusLedDevice::write(uint8_t ubCmd, uint8_t *pubTxBuff, uint16_t usLen)
{
    if ( ( pubTxBuff == NULL )	||
         ( usLen == 0) )
    {
        int32_t slDeviceAddress = -1;
        int ret = getDeviceAddress(slDeviceAddress);
        if ( ret == 0 )
        {
            bool bRet = m_pI2CIfr->singleWriteGeneral(slDeviceAddress, &ubCmd, 1);
            if ( bRet == false )
            {
                m_pLogger->log( LOG_ERR, "PowerManagerDevice::write-->Error on write operation");
                return -1;
            }
        }
    }
    else
    {
        int32_t slDeviceAddress = -1;
        int ret = getDeviceAddress(slDeviceAddress);
        if ( ret == 0 )
        {
            bool bRet = m_pI2CIfr->multiWrite( slDeviceAddress, pubTxBuff, usLen, ubCmd);
            if ( bRet == false )
            {
                m_pLogger->log( LOG_ERR, "PowerManagerDevice::write-->Error on write operation");
                return -1;
            }
        }
    }

    return 0;
}

bool StatusLedDevice::setLed(eModuleLedMode mode)
{
    if (mode == eModuleLedBlink)
    {
        config(STATLED_MODE2_BLNK_CTL);
        writeLedOut(0, 3);
    }
    else if (mode == eModuleLedSteady)
    {
        config(STATLED_MODE2_DIM_CTL);
        writeLedOut(0, 3);
    }
    else
    {
        writeLedOut(0, 0);
    }    
    return true;
}

int StatusLedDevice::config(uint8_t ubDmBlinkState)
{
    int res = writeMode1(0);
    if ( res != 0 )
    {
        log( LOG_ERR, "StatusLedDevice::config-->Unable to set MODE2 register");
        return -1;
    }

    if (ubDmBlinkState == STATLED_MODE2_BLNK_CTL)
    {
        res = writeMode2(0x25);
        log( LOG_DEBUG, "StatusLedDevice::config-->Blink mode on");
    }
    else
    {
        res = writeMode2(0x05);
        log( LOG_DEBUG, "StatusLedDevice::config-->Dim mode on");
    }

    if ( res != 0 )
    {
        log( LOG_ERR, "PCA9952::config-->Unable to set MODE2 register");
        return -1;
    }

    return 0;
}

int StatusLedDevice::writeMode1(uint8_t ubMode)
{
    uint8_t pubTxBuff[32];
    memset(pubTxBuff, 0, sizeof(pubTxBuff));

    pubTxBuff[0] = ubMode;

    int res = write(PCA9952_MODE1, pubTxBuff, 1);
    if ( res != 0 )
    {
        m_pLogger->log( LOG_ERR, "PCA9952::writeMode1-->Unable to write on Mode1 register");
        return -1;
    }
    else
    {
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9952::writeMode1-->Write on Mode1 register");
    }
    return 0;
}

int StatusLedDevice::writeMode2(uint8_t ubMode)
{
    uint8_t pubTxBuff[32];
    memset(pubTxBuff, 0, sizeof(pubTxBuff));

    pubTxBuff[0] = ubMode;

    int res = write(PCA9952_MODE2, pubTxBuff, 1);
    if ( res != 0 )
    {
        m_pLogger->log( LOG_ERR, "PCA9952::writeMode2-->Unable to write on Mode2 register");
        return -1;
    }
    else
    {
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9952::writeMode2-->Write on Mode2 register");
    }
    return 0;
}

int StatusLedDevice::writeLedOut(uint8_t ubNumLed, uint8_t ubState)
{
    uint8_t pubTxBuff[32];
    memset(pubTxBuff, 0, sizeof(pubTxBuff));

    if (ubNumLed >= 32)
    {
        m_pLogger->log( LOG_ERR, "PCA9952::writeLedOut-->Num LED out of range");
        return -1;
    }

//	uint8_t pubRxBuff;
//	int res = read(m_tableLedGrp[ubNumLed].ubAddLedGrp, &pubRxBuff, 1);
//	if (res != 0)
//	{
//		m_pLogger->log( LOG_ERR, "PCA9952::writeLedOut-->Unable to read register");
//		return -1;
//	}

//	uint8_t ubTmp = 0;
//	ubTmp = pubRxBuff & (~m_tableLedGrp[ubNumLed].ubMaskLedGrp);
//	pubTxBuff[0] = ubTmp | (ubState << m_tableLedGrp[ubNumLed].ubPosLedGrp);
    pubTxBuff[0] = ubState << ubNumLed;

    int res = write(0, pubTxBuff, 1);
    if ( res != 0 )
    {
        m_pLogger->log( LOG_ERR, "PCA9952::writeLedOut-->Unable to write ");
        return -1;
    }
    else
    {
        m_pLogger->log( LOG_DEBUG_PARANOIC, "PCA9952::writeLedOut-->Write ");
    }
    return 0;
}
