/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    PowerManagerDevice.cpp
 @author  BmxIta FW dept
 @brief   Contains the implementations for the PowerManagerDevice class.
 @details

 ****************************************************************************
*/

#include "PowerManagerDevice.h"
#include "I2CInterface.h"
#include "Log.h"
#include "CommonIncludePowerManager.h"

PowerManagerDevice::PowerManagerDevice()
{
    m_pLogger = NULL;
    m_pI2CIfr = NULL;
}

PowerManagerDevice::~PowerManagerDevice()
{

}

int PowerManagerDevice::init( Log * pLogger, I2CInterface * pI2CIfr )
{
    if ( ( pLogger == NULL ) ||
         ( pI2CIfr == NULL ) )
    {
        return -1;
        m_pLogger->log( LOG_ERR, "PowerManagerDevice::init--> ERROR");
    }
    else
    {
        m_pLogger = pLogger;
        m_pI2CIfr = pI2CIfr;

        m_pLogger->log( LOG_DEBUG_PARANOIC, "PowerManagerDevice::init--> DONE");
    }

    return 0;
}

int PowerManagerDevice::setDeviceAddress( int32_t slDeviceAddress )
{
    if ( slDeviceAddress > 0 )
    {
        m_slDeviceAddress = slDeviceAddress;
        m_pLogger->log( LOG_INFO, "PowerManagerDevice::setDeviceAddress--> set Address 0x%X", m_slDeviceAddress);
    }
    else
    {
        m_pLogger->log(LOG_ERR, "PowerManagerDevice::setDeviceAddress-->ERR");
        return -1;
    }

    return 0;
}

int PowerManagerDevice::getDeviceAddress( int32_t &slDeviceAddress )
{
    if ( m_slDeviceAddress >= 0 )
    {
        slDeviceAddress = m_slDeviceAddress;
    }
    else
    {
        m_pLogger->log(LOG_ERR, "PowerManagerDevice::getDeviceAddress-->ERR");
        return -1;
    }

    return 0;
}

int PowerManagerDevice::write(uint8_t ubCmd, uint8_t *pubTxBuff, uint16_t usLen)
{
    if ( ( pubTxBuff == NULL )	||
         ( usLen == 0) )
    {
        int32_t slDeviceAddress = -1;
        int ret = getDeviceAddress(slDeviceAddress);
        if ( ret == 0 )
        {
            bool bRet = m_pI2CIfr->singleWriteGeneral(slDeviceAddress, &ubCmd, 1);
            if ( bRet == false )
            {
                m_pLogger->log( LOG_ERR, "PowerManagerDevice::write-->Error on write operation");
                return -1;
            }
        }
    }
    else
    {
        int32_t slDeviceAddress = -1;
        int ret = getDeviceAddress(slDeviceAddress);
        if ( ret == 0 )
        {
            bool bRet = m_pI2CIfr->multiWrite( slDeviceAddress, pubTxBuff, usLen, ubCmd);
            if ( bRet == false )
            {
                m_pLogger->log( LOG_ERR, "PowerManagerDevice::write-->Error on write operation");
                return -1;
            }
        }
    }

    return 0;
}

int PowerManagerDevice::writeCustomAdd(uint8_t ubAdd, uint8_t ubCmd)
{
    bool bRet = m_pI2CIfr->singleWriteGeneral(ubAdd, &ubCmd, 1);
    if ( bRet == false )
    {
        m_pLogger->log( LOG_ERR, "I2CBoardBase::writeCustomAdd-->Error on write operation");
        return -1;
    }
    return 0;
}

int PowerManagerDevice::writeCustomAdd(uint8_t ubAdd, uint8_t ubCmd, uint8_t *pubTxBuff, uint16_t usLen)
{

    bool bRet = m_pI2CIfr->multiWrite( ubAdd, pubTxBuff, usLen, ubCmd);
    if ( bRet == false )
    {
        m_pLogger->log( LOG_ERR, "PowerManagerDevice::writeCustomAdd-->Error on write operation");
        return -1;
    }

    return 0;
}

int PowerManagerDevice::read(uint8_t ubCmd, uint8_t *pubRxBuff, uint16_t usLen)
{
    if ( ( pubRxBuff == NULL )	||
         ( usLen == 0) )
    {
        m_pLogger->log( LOG_ERR, "PowerManagerDevice::read-->ERR Parameters");
        return -1;
    }
    else
    {
        int32_t slDeviceAddress = -1;
        int ret = getDeviceAddress(slDeviceAddress);
        if ( ret == 0 )
        {
            bool bRet = m_pI2CIfr->multiRead( slDeviceAddress, pubRxBuff, usLen, ubCmd);
            if ( bRet == false )
            {
                m_pLogger->log( LOG_ERR, "PowerManagerDevice::read-->Error on read operation");
                return -1;
            }
        }
    }

    return 0;
}

/*********************************************************************************************************
 *
 * WRITE operations
 *
 *********************************************************************************************************/
int PowerManagerDevice::writeResetSoft( void )
{
    uint8_t pubTxBuff[PM_MAX_MSG_SIZE_BUFF];
    memset(pubTxBuff, 0, sizeof(pubTxBuff));

    int res = write(PMBUS_MFR_SOFT_RESET, pubTxBuff, 0);
    if (res != 0)
    {
        m_pLogger->log(LOG_ERR, "PowerManagerDevice::resetSoft-->Failed");
        return -1;
    }

    return 0;
}

int PowerManagerDevice::writeClearFaults( void )
{
    uint8_t pubTxBuff[PM_MAX_MSG_SIZE_BUFF];
    memset(pubTxBuff, 0, sizeof(pubTxBuff));

    int res = write(PMBUS_CLEAR_FAULTS, pubTxBuff, 0);
    if (res != 0)
    {
        m_pLogger->log(LOG_ERR, "PowerManagerDevice::writeClearFaults-->Failed");
        return -1;
    }

    return 0;
}

/*********************************************************************************************************
 *
 * READ operations
 *
 *********************************************************************************************************/
int PowerManagerDevice::readDeviceID(string &pubDeviceID)
{
    uint8_t pubRxBuff[PM_MAX_MSG_SIZE_BUFF];
    memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read(PMBUS_MFR_DEVICE_ID, pubRxBuff, 32);
    if (res != 0)
    {
        m_pLogger->log(LOG_ERR, "PowerManagerDevice::readDeviceID--> Failed");

        return -1;
    }
    else
    {
        pubDeviceID = (char*)&pubRxBuff[0];
    }

    return 0;
}

int PowerManagerDevice::readStatusRegister(uint32_t &ubVal)
{
    uint8_t pubRxBuff[PM_MAX_MSG_SIZE_BUFF];
    memset(pubRxBuff, 0, sizeof(pubRxBuff));

    int res = read( PMBUS_MFR_STATUS, pubRxBuff, 5 );
    if (res != 0)
    {
        m_pLogger->log(LOG_ERR, "PowerManagerDevice::readStatusRegister-->ERR");
        return -1;
    }
    else
    {
        ubVal = 0;
        ubVal = pubRxBuff[1] << 24 | pubRxBuff[2] << 16 | pubRxBuff[3] << 8 | pubRxBuff[4];
    }

    return 0;
}
