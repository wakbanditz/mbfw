/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    main.cpp
 @author  BmxIta FW dept
 @brief   Contains the main function.
 @details

 ****************************************************************************
*/

#include <time.h>
#include <dirent.h>
#include <iostream>
#include <fstream>
#include <termios.h>
#include <fcntl.h>
#include <linux/input.h>


#include "CommonInclude.h"
#include "Log.h"
#include "Config.h"
#include "ProcessManager.h"


#define MAX_CMD_SIZE	8192

static bool bKeepRunning = true;

void signalHandler(int nSignal)
{
	static int sig_counter = 1;
	printf("Received signal %d\n", nSignal);
	switch(nSignal)
	{
		case SIGTERM:
		case SIGINT:
		case SIGKILL:
			bKeepRunning = false;
			if ( ++sig_counter > 5 )
			{
				// Fifth time that the soft mode has failed brute exits
				printf("Brute force exit ...");
				exit(1);
			}
		break;
	}
}


int main(void)
{
	signal(SIGINT, signalHandler);
	signal(SIGKILL, signalHandler);
	signal(SIGTERM, signalHandler);
	signal(SIGTSTP, signalHandler);

	char sConfigFileName[1024] = CONF_FILE;
	char sError[1024];

	/* ********************************************************************************************
	 * READS CONFIGURATION FILE
	 * ********************************************************************************************
	 */
	printf("Read configuration file <%s> ", sConfigFileName);
	Config config;
	config_key_t config_keys[] =
	{
		{ CFG_PROCESS_MONITOR_LOG_ENABLE,	(char*)"log.ProcessMonitor.enable",
					(char*)"GENERAL", T_int, (char*)"1", DEFAULT_ACCEPTED},
		{ CFG_PROCESS_MONITOR_LOG_FILE,		(char*)"log.ProcessMonitor.file",
					(char*)"GENERAL", T_string, (char*)"", DEFAULT_ACCEPTED},
		{ CFG_PROCESS_MONITOR_LOG_LEVEL,	(char*)"log.ProcessMonitor.level",
					(char*)"GENERAL", T_int, (char*)"6", DEFAULT_ACCEPTED},
		{ CFG_PROCESS_MONITOR_LOG_NUM_FILES, (char*)"log.ProcessMonitor.numFiles",
					(char*)"GENERAL", T_int, (char*)"5", DEFAULT_ACCEPTED},
	};

	int num_keys = sizeof(config_keys) / sizeof(config_key_t);
	if ( !config.Init(config_keys, num_keys, sConfigFileName) )
	{
		Config::GetLastError(sError, 1024);
		printf("Impossible to read configuration file <%s> (error: %s)", sConfigFileName, sError);
		return 1;
	}

	/* ********************************************************************************************
	 * LOGGER INITIALIZATION
	 * ********************************************************************************************
	 */
	Log logger;
	int liLogEnable = config.GetInt(CFG_PROCESS_MONITOR_LOG_ENABLE);
	bool bEnable = (liLogEnable > 0 ? true : false);
	logger.enable(bEnable);
	logger.setAppName(PROGRAM_NAME);
	logger.setFile(config.GetString(CFG_PROCESS_MONITOR_LOG_FILE));
	logger.setLevel(config.GetInt(CFG_PROCESS_MONITOR_LOG_LEVEL));
	logger.setMaxNumFiles(config.GetInt(CFG_PROCESS_MONITOR_LOG_NUM_FILES));
	if ( config.GetInt(CFG_PROCESS_MONITOR_LOG_LEVEL) >= LOG_DEBUG )
	{
		config.PrintParams();
	}
	logger.log(LOG_INFO, "Starting << %s >>", PROGRAM_NAME);

	/* ********************************************************************************************
	 * PROCESSOR TEST INITIALIZATION
	 * ********************************************************************************************
	 */
	ProcessManager pm;
	pm.setLogger(&logger);
	pm.initFromConfigFile(sConfigFileName);
	logger.log(LOG_INFO, "Starting processes");
	pm.startThread();

	/* ********************************************************************************************
	 * MAIN LOOP, does nothing...
	 * ********************************************************************************************
	 */
#ifdef KILL_TEST_PROC
	int fd, bytes;
	struct input_event data;

	const char *pDevice = "/dev/input/event0";

	// Open Keyboard
	fd = open(pDevice, O_RDONLY | O_NONBLOCK);
	if(fd == -1)
	{
		printf("ERROR Opening %s\n", pDevice);
		return -1;
	}

	while (bKeepRunning)
	{
		// Read Keyboard Data
		bytes = read(fd, &data, sizeof(data));
		if(bytes > 0)
		{
			printf("Keypress value=%x, type=%x, code=%x\n", data.value, data.type, data.code);
		}
		else
		{
			// Nothing read
			sleep(1);
		}
	}
#else
	while ( bKeepRunning )
	{
		sleep(1);
		if(pm.isStopped())
		{
			bKeepRunning = false;
		}
	}
#endif


	logger.log(LOG_INFO, "Stopping processes");

	/* ********************************************************************************************
	 * CLOSE AND FREE RESOURCES
	 * ********************************************************************************************
	 */
	if ( pm.isStopped() == false &&
		 pm.stop(pm.getRespawnCycleDurationMSec()))
	{
		logger.log(LOG_INFO, "All Process Monitor processes correctly closed");
	}
	if ( pm.isStopped() == false)
	{
		pm.stopThread();
	}
	if ( pm.isStopped() )
	{
		logger.log(LOG_INFO, "Process Monitor itself correctly closed");
	}

	return 0;
}
