TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

# path inclusions
INCLUDEPATH += $$PWD/../../

# system libraries inclusion
LIBS += -pthread -lrt

# internal libraries
unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/ProcessManager/ -lProcessManager
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/ProcessManager
DEPENDPATH += $$PWD/../../bmx_libs/Libs/ProcessManager
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/ProcessManager/libProcessManager.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Config/ -lConfig
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Config
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Config
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Config/libConfig.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Log/ -lLog
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Log
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Log
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Log/libLog.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/TimeStamp/ -lTimeStamp
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/TimeStamp
DEPENDPATH += $$PWD/../../bmx_libs/Libs/TimeStamp
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/TimeStamp/libTimeStamp.a

unix:!macx: LIBS += -L$$OUT_PWD/../../bmx_libs/Libs/Thread/ -lThread
INCLUDEPATH += $$PWD/../../bmx_libs/Libs/Thread
DEPENDPATH += $$PWD/../../bmx_libs/Libs/Thread
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../bmx_libs/Libs/Thread/libThread.a


#headers and sources inside project subfolders
include(../../mbfwInclude.pri)

TARGET = ProcessMonitor

# deployment directives
target.path = /home/root
INSTALLS += target

# project defines
##DEFINES += CONF_FILE=\\\"$${CONFIG_FILE}\\\"
DEFINES += PROGRAM_NAME=\\\"$${PROCESS_NAME_PROCESS_MONITOR}\\\"
