/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    ModuleInclude.h
 @author  BmxIta FW dept
 @brief   Contains the Module defines.
 @details

 ****************************************************************************
*/

#ifndef MODULEINCLUDE_H
#define MODULEINCLUDE_H

#define NUM_OF_FANS 3

enum
{
	eMasterActionInit = 0,
	eMasterActionWaitInit,
	eMasterActionLed,
	eMasterActionCmd,
	eMasterActionExecute,
	eMasterActionFwUpdate,
	eMasterActionNone
};

typedef enum
{
	eModuleLedRed = 0,
	eModuleLedGreen,
	eModuleLedOrange,
	eModuleLedMax
}eModuleLedId;

typedef enum
{
	eModuleLedOff = 0,
	eModuleLedSteady,
	eModuleLedBlink
}eModuleLedMode;

typedef enum
{
    eFanSupply = 0,
    eFanSectionA,
    eFanSectionB,
	eFanNone
}eModuleFanId;

#define FAN_NAME_SUPPLY     "Supply"
#define FAN_NAME_SECTIONA   "SectionA"
#define FAN_NAME_SECTIONB   "SectionB"

typedef enum
{
    eNtcPowerManager = 0,
    eNtcInstrument,
	eNtcSupply,
    eNtcSpare,
	eNtcNone
}eModuleNtcId;

#endif // MODULEINCLUDE_H
