PROCESS_NAME_PROCESS_MONITOR = PROCESS_MONITOR
PROCESS_NAME_FW_CORE = FW_CORE
PROCESS_NAME_CAN_MANAGER = CAN_MANAGER
PROCESS_NAME_TEST_APP = TEST_APP
CONFIG_FILE = mbfw.config

BASE_FOLDER = /home/root

DISTFILES += \
            $$PWD/CAMERA/BATCH_CALIBR/1D.batch \
            $$PWD/CAMERA/BATCH_CALIBR/2D.batch \
            $$PWD/CAMERA/BATCH_CALIBR/ConeAbsence.batch \
            $$PWD/CAMERA/BATCH_CALIBR/X0.batch \
            $$PWD/CAMERA/BATCH_CALIBR/X3.batch \
            $$PWD/CAMERA/BATCH_USAGE/1D.batch \
            $$PWD/CAMERA/BATCH_USAGE/2D.batch \
            $$PWD/CAMERA/BATCH_USAGE/ConeAbsence.batch \
            $$PWD/CAMERA/BATCH_USAGE/X0.batch \
            $$PWD/CAMERA/BATCH_USAGE/X3.batch \
            $$PWD/APPLOGS/note.txt \
            $$PWD/CAMERA/reader.test \
            $$PWD/CAMERA/results.test \
            $$PWD/COMMANDS/OPT_CHECK.cfg \
            $$PWD/COMMANDS/PUMP.cfg \
            $$PWD/CRC/CanManager.crc \
            $$PWD/CRC/FwCore.crc \
            $$PWD/FANS/fanSectionA.cfg \
            $$PWD/FANS/fanSectionB.cfg \
            $$PWD/FANS/fanSupply.cfg \
            $$PWD/FANS/fanTemperatureSectionA.cfg \
            $$PWD/FANS/fanTemperatureSectionB.cfg \
            $$PWD/FANS/fanTemperatureSupply.cfg \
            $$PWD/NSH/NSH.cfg \
            $$PWD/POSITIONS/Camera_cal_default.cfg \
            $$PWD/POSITIONS/NSHcalibration_default.cal \
            $$PWD/POSITIONS/PumpVolume.cfg \
            $$PWD/UPDATE/updateTime.cfg \
            $$PWD/mbfw.config \
            $$PWD/loginSetup.cfg \
            $$PWD/ERRORS/SECTION.err \
            $$PWD/COMMANDS/HELLO.cfg \
            $$PWD/COMMANDS/MOVE.cfg \
            $$PWD/COMMANDS/LOGIN.cfg \
            $$PWD/COMMANDS/GETSENSOR.cfg \
            $$PWD/COMMANDS/GETSERIAL.cfg \
            $$PWD/COMMANDS/GETVERSIONS.cfg \
            $$PWD/COMMANDS/LOGOUT.cfg \
            $$PWD/COMMANDS/READSEC.cfg \
            $$PWD/COMMANDS/SETLED.cfg \
            $$PWD/COMMANDS/VIDASEP.cfg \
            $$PWD/COMMANDS/MONITOR_PRESSURE.cfg \
            $$PWD/COMMANDS/GETCALIBRATION.cfg \
            $$PWD/COMMANDS/GETPARAMETER.cfg \
            $$PWD/COMMANDS/SAVECALIBRATION.cfg \
            $$PWD/COMMANDS/SETMOTORACTIVATION.cfg \
            $$PWD/COMMANDS/INIT.cfg \
            $$PWD/COMMANDS/CAPTURE_IMAGE.cfg \
            $$PWD/COMMANDS/GETPOSITION.cfg \
            $$PWD/COMMANDS/CANCELSECTION.cfg \
            $$PWD/COMMANDS/GENERAL_COMMAND.cfg \
            $$PWD/COMMANDS/GETMOTORACTIVATION.cfg \
            $$PWD/POSITIONS/Pump_cal.cfg \
            $$PWD/POSITIONS/Pump_coded.cfg \
            $$PWD/POSITIONS/Tower_cal.cfg \
            $$PWD/POSITIONS/Tower_coded.cfg \
            $$PWD/POSITIONS/Tray_cal.cfg \
            $$PWD/POSITIONS/Tray_coded.cfg \
            $$PWD/POSITIONS/SPR_cal.cfg \
            $$PWD/POSITIONS/SPR_coded.cfg \
            $$PWD/COMMANDS/GET_PRESSURE_OFFSETS.cfg \
            $$PWD/COMMANDS/GET_PRESSURE_SETTINGS.cfg \
            $$PWD/COMMANDS/SETMAINTENANCEMODE.cfg \
            $$PWD/COMMANDS/SS_CALIBRATE.cfg \
            $$PWD/COMMANDS/TRAY_READ.cfg \
            $$PWD/COMMANDS/SH_CALIBRATE.cfg \
            $$PWD/COMMANDS/GET_SS_REFERENCE.cfg \
            $$PWD/COMMANDS/SETSERIAL.cfg \
            $$PWD/COMMANDS/SETTIME.cfg \
            $$PWD/COMMANDS/ROUTE.cfg \
            $$PWD/COMMANDS/OPT_CALIBRATE.cfg \
            $$PWD/COMMANDS/AIR_CALIBRATE.cfg \
            $$PWD/COMMANDS/RUN.cfg \
            $$PWD/COMMANDS/SHUTDOWN.cfg \
            $$PWD/COMMANDS/DISABLE.cfg \
            $$PWD/COMMANDS/SLEEP.cfg \
            $$PWD/ERRORS/PRESSURE.err \
            $$PWD/COMMANDS/AUTO_CALIBRATE.cfg \
            $$PWD/ERRORS/NSH.err \
            $$PWD/ERRORS/MASTERBOARD.err \
            $$PWD/ERRORS/CAMERA.err \
            $$PWD/COMMANDS/SIGNAL_READ.cfg \
            $$PWD/COMMANDS/SH_FORCE_LED.cfg \
            $$PWD/COMMANDS/GETCAMERAROI.cfg \
            $$PWD/COMMANDS/GETCOUNTER.cfg \
            $$PWD/COMMANDS/SETCOUNTER.cfg \
            $$PWD/RUN/RUN.cfg \
            $$PWD/COMMANDS/SET_GLOBAL_SETTINGS.cfg \
            $$PWD/COMMANDS/GET_GLOBAL_SETTINGS.cfg \
            $$PWD/COMMANDS/SETPARAMETER.cfg \
            $$PWD/COMMANDS/SET_FAN_SETTINGS.cfg \
            $$PWD/COMMANDS/GET_FAN_SETTINGS.cfg \
            $$PWD/COMMANDS/SAVECAMERAROI.cfg \
            $$PWD/CAMERA/Camera_def.conf \
            $$PWD/UPDATE/UpdateLog.log \
            $$PWD/COMMANDS/FWUPDATE.cfg \
            $$PWD/updateAppl.sh \
            $$PWD/FANS/fanSupply.cfg \
            $$PWD/FANS/fanSectionA.cfg \
            $$PWD/FANS/fanSectionB.cfg \
            $$PWD/releaseLabel.cfg

conf.path = $$BASE_FOLDER
conf.files += $$PWD/mbfw.config \
              $$PWD/loginSetup.cfg \
              $$PWD/updateAppl.sh \
              $$PWD/releaseLabel.cfg


INSTALLS += conf

comm.path = $$BASE_FOLDER/COMMANDS

comm.files += \
            $$PWD/COMMANDS/HELLO.cfg \
            $$PWD/COMMANDS/MOVE.cfg \
            $$PWD/COMMANDS/LOGIN.cfg \
            $$PWD/COMMANDS/GETSENSOR.cfg \
            $$PWD/COMMANDS/GETSERIAL.cfg \
            $$PWD/COMMANDS/GETVERSIONS.cfg \
            $$PWD/COMMANDS/LOGOUT.cfg \
            $$PWD/COMMANDS/READSEC.cfg \
            $$PWD/COMMANDS/ROUTE.cfg \
            $$PWD/COMMANDS/SETLED.cfg \
            $$PWD/COMMANDS/MONITOR_PRESSURE.cfg \
            $$PWD/COMMANDS/VIDASEP.cfg \
            $$PWD/COMMANDS/GETCALIBRATION.cfg \
            $$PWD/COMMANDS/GETPARAMETER.cfg \
            $$PWD/COMMANDS/SAVECALIBRATION.cfg \
            $$PWD/COMMANDS/SETMOTORACTIVATION.cfg \
            $$PWD/COMMANDS/INIT.cfg \
            $$PWD/COMMANDS/GETPOSITION.cfg \
            $$PWD/COMMANDS/CAPTURE_IMAGE.cfg \
            $$PWD/COMMANDS/RUN.cfg \
            $$PWD/COMMANDS/CANCELSECTION.cfg \
            $$PWD/COMMANDS/GETMOTORACTIVATION.cfg \
            $$PWD/COMMANDS/GET_PRESSURE_OFFSETS.cfg \
            $$PWD/COMMANDS/GET_PRESSURE_SETTINGS.cfg \
            $$PWD/COMMANDS/SETMAINTENANCEMODE.cfg \
            $$PWD/COMMANDS/SS_CALIBRATE.cfg \
            $$PWD/COMMANDS/TRAY_READ.cfg \
            $$PWD/COMMANDS/SH_CALIBRATE.cfg \
            $$PWD/COMMANDS/GET_SS_REFERENCE.cfg \
            $$PWD/COMMANDS/SETSERIAL.cfg \
            $$PWD/COMMANDS/SETTIME.cfg \
            $$PWD/COMMANDS/OPT_CALIBRATE.cfg \
            $$PWD/COMMANDS/AIR_CALIBRATE.cfg \
            $$PWD/COMMANDS/SHUTDOWN.cfg \
            $$PWD/COMMANDS/DISABLE.cfg \
            $$PWD/COMMANDS/SLEEP.cfg \
            $$PWD/COMMANDS/AUTO_CALIBRATE.cfg \
            $$PWD/COMMANDS/SIGNAL_READ.cfg \
            $$PWD/COMMANDS/SH_FORCE_LED.cfg \
            $$PWD/COMMANDS/GETCAMERAROI.cfg \
            $$PWD/COMMANDS/GETCOUNTER.cfg \
            $$PWD/COMMANDS/SETCOUNTER.cfg \
            $$PWD/COMMANDS/SET_GLOBAL_SETTINGS.cfg \
            $$PWD/COMMANDS/GET_GLOBAL_SETTINGS.cfg \
            $$PWD/COMMANDS/SET_FAN_SETTINGS.cfg \
            $$PWD/COMMANDS/GET_FAN_SETTINGS.cfg \
            $$PWD/COMMANDS/SETPARAMETER.cfg \
            $$PWD/COMMANDS/SAVECAMERAROI.cfg \
            $$PWD/COMMANDS/FWUPDATE.cfg \
            $$PWD/COMMANDS/PUMP.cfg \
            $$PWD/COMMANDS/OPT_CHECK.cfg \


INSTALLS += comm

positions.path = $$BASE_FOLDER/POSITIONS

positions.files += \
                    $$PWD/POSITIONS/Pump_cal.cfg \
                    $$PWD/POSITIONS/Pump_coded.cfg \
                    $$PWD/POSITIONS/SPR_cal.cfg \
                    $$PWD/POSITIONS/SPR_coded.cfg \
                    $$PWD/POSITIONS/Tower_cal.cfg \
                    $$PWD/POSITIONS/Tower_coded.cfg \
                    $$PWD/POSITIONS/Tray_cal.cfg \
                    $$PWD/POSITIONS/Tray_coded.cfg \
                    $$PWD/POSITIONS/NSHcalibration_default.cal \
                    $$PWD/POSITIONS/Camera_cal_default.cfg \
                    $$PWD/POSITIONS/PumpVolume.cfg

INSTALLS += positions


errors.path = $$BASE_FOLDER/ERRORS

errors.files += \
                $$PWD/ERRORS/SECTION.err \
                $$PWD/ERRORS/PRESSURE.err \
                $$PWD/ERRORS/NSH.err \
                $$PWD/ERRORS/MASTERBOARD.err \
                $$PWD/ERRORS/CAMERA.err

INSTALLS += errors

fans.path = $$BASE_FOLDER/FANS

fans.files += \
            $$PWD/FANS/fanTemperatureSectionA.cfg \
            $$PWD/FANS/fanTemperatureSectionB.cfg \
            $$PWD/FANS/fanTemperatureSupply.cfg \
            $$PWD/FANS/fanSupply.cfg \
            $$PWD/FANS/fanSectionA.cfg \
            $$PWD/FANS/fanSectionB.cfg

INSTALLS += fans


runs.path = $$BASE_FOLDER/RUN

runs.files +=	$$PWD/RUN/RUN.cfg

INSTALLS += runs

#LOGS folder to store mbfw.log, canmanager.log, mbpm.log
applogs.path = $$BASE_FOLDER/APPLOGS
applogs.files += \
                $$PWD/APPLOGS/note.txt

INSTALLS += applogs

batch_cal.path = $$BASE_FOLDER/CAMERA/BATCH_CALIBR
batch_cal.files += \
            $$PWD/CAMERA/BATCH_CALIBR/ConeAbsence.batch \
            $$PWD/CAMERA/BATCH_CALIBR/X0.batch \
            $$PWD/CAMERA/BATCH_CALIBR/1D.batch \
            $$PWD/CAMERA/BATCH_CALIBR/2D.batch \
            $$PWD/CAMERA/BATCH_CALIBR/X3.batch \
            $$PWD/CAMERA/BATCH_CALIBR/ConeAbsence.batch
INSTALLS += batch_cal

batch_usg.path = $$BASE_FOLDER/CAMERA/BATCH_USAGE
batch_usg.files += \
            $$PWD/CAMERA/BATCH_USAGE/ConeAbsence.batch \
            $$PWD/CAMERA/BATCH_USAGE/X0.batch \
            $$PWD/CAMERA/BATCH_USAGE/1D.batch \
            $$PWD/CAMERA/BATCH_USAGE/2D.batch \
            $$PWD/CAMERA/BATCH_USAGE/X3.batch \
            $$PWD/CAMERA/BATCH_USAGE/ConeAbsence.batch
INSTALLS += batch_usg


update.path = $$BASE_FOLDER/UPDATE
update.files += \
            $$PWD/UPDATE/UpdateLog.log \
            $$PWD/UPDATE/updateTime.cfg

INSTALLS += update


camera.path = $$BASE_FOLDER/CAMERA/
camera.files += \
            $$PWD/CAMERA/Camera_def.conf \
            $$PWD/CAMERA/reader.test \
            $$PWD/CAMERA/results.test

INSTALLS += camera

NSH.path = $$BASE_FOLDER/NSH/
NSH.files += \
            $$PWD/NSH/NSH.cfg

INSTALLS += NSH

CRC.path = $$BASE_FOLDER/CRC/
CRC.files += \
            $$PWD/CRC/CanManager.crc \
            $$PWD/CRC/FwCore.crc \

INSTALLS += CRC



DISTFILES += $$PWD/httpd.conf

apache2config.path = /etc/apache2/
apache2config.files += $$PWD/httpd.conf

INSTALLS += apache2config

HEADERS += \
        ErrorCodes.h \
        $$PWD/CommonInclude.h \
        $$PWD/StatusInclude.h \
        $$PWD/SectionInclude.h \
        $$PWD/Common/crc16.h \
        $$PWD/Common/crc32.h \
        $$PWD/Common/swap.h \
        $$PWD/ModuleInclude.h

SOURCES += \
        $$PWD/Common/crc16.c \
        $$PWD/Common/crc32.c \
        $$PWD/Common/swap.c



