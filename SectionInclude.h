/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    SectionInclude.h
 @author  BmxIta FW dept
 @brief   Contains the Section defines.
 @details

 ****************************************************************************
*/

#ifndef SECTIONINCLUDE_H
#define SECTIONINCLUDE_H

#include <string>

#define SECTION_CHAR_A			'A'

#define SECTION_PUMP_DEVICE		0
#define SECTION_TOWER_DEVICE	1
#define SECTION_TRAY_DEVICE		2
#define SECTION_SPR_DEVICE		3
#define SECTION_PIB_DEVICE		4
#define SECTION_GENERAL_DEVICE	5

#define SECTION_PUMP_CHAR		(ZERO_CHAR + SECTION_PUMP_DEVICE)
#define SECTION_TOWER_CHAR		(ZERO_CHAR + SECTION_TOWER_DEVICE)
#define SECTION_TRAY_CHAR		(ZERO_CHAR + SECTION_TRAY_DEVICE)
#define SECTION_SPR_CHAR		(ZERO_CHAR + SECTION_SPR_DEVICE)
#define SECTION_PIB_CHAR		(ZERO_CHAR + SECTION_PIB_DEVICE)
#define SECTION_GENERAL_CHAR	(ZERO_CHAR + SECTION_GENERAL_DEVICE)

#define SECTION_PUMP_STRING		"Pump"
#define SECTION_TOWER_STRING	"Tower"
#define SECTION_TRAY_STRING		"Tray"
#define SECTION_SPR_STRING		"SPR"
#define SECTION_PIB_STRING		"PIB"
#define SECTION_GENERAL_STRING	"General"

#define DEVICE_NSH_STRING		"NSH"

#define DEFAULT_REQUEST_DELAY		(5 * 1000)	// default timeout on commands = 5 secs
#define DEFAULT_SETTING_DELAY		(1 * 1000)	// default timeout on commands = 5 secs
#define ABORT_REQUEST_DELAY         (5 * 60 * 1000)  // timeout for Abort = 5 mins

#define STRING_CURRENT_ENABLED	"ENABLED"
#define STRING_CURRENT_DISABLED	"DISABLED"

#define SECTION_DOOR_OPEN_STRING	"OPEN"
#define SECTION_DOOR_CLOSE_STRING	"CLOSED"

#define SLEEP_DISABLE_ENABLE		'1'
#define SLEEP_DISABLE_DISABLE		'0'

#define SET_DISABLE		'1'
#define SET_SLEEP		'0'

enum
{
	SECTION_EVENT_TEMPERATURE = 0,
	SECTION_EVENT_NONE
};

enum
{
	SECTION_ACTION_INIT = 0,
	SECTION_ACTION_REINIT,
	SECTION_ACTION_CMD,
	SECTION_ACTION_EXECUTE,
	SECTION_ACTION_MOVE,
	SECTION_ACTION_FW_UPDATE,
	SECTION_ACTION_END,
    SECTION_ACTION_STATUS,
	SECTION_ACTION_NONE
};

enum
{
	MONITOR_PRESSURE_NONE = -1,
	MONITOR_PRESSURE_STOP = 0,
    MONITOR_PRESSURE_START = 1,
    MONITOR_PRESSURE_PROTOCOL_START = 2
};

typedef enum
{
	eDoorUnknown		= -1,
	eDoorOpen	 		= 0,
	eDoorClosed			= 1
} eDoorStatus;

typedef enum
{
	eTemperatureUnknown	= -1,
	eTemperatureHigh 	= 0,
	eTemperatureLow		= 1,
	eTemperatureOk		= 2
} eTemperatureStatus;

typedef enum
{
	eStopAcq				= '0',
	eStartAcq				= '1'
} enumSectionPIBStartStateAcq;

typedef struct
{
	std::string strType;			//set if it is a calibration request or a position request
	std::string strComponent;
	std::string strMotor;
	std::string strLabel;
	std::string strValue;

} structSaveCalibration;


enum class eSectionMotor { ePump, eSpr, eTower, eTray};

// ------------------------ PRESSURE MANAGEMENT ---------------------------------- //

#define CLINIC_ALGO_STRING			"CLINIC"
#define INDUSTRY_ALGO_STRING		"INDUSTRY"
#define VETERINARY_ALGO_STRING		"VETERINARY"

#define CLINIC_ALGO_INT			0
#define INDUSTRY_ALGO_INT		1
#define VETERINARY_ALGO_INT		2

// Pressure Section structures

// structure used in Section Application
// unsigned char -> uint8_t
// unsigned long -> uint32_t
// unsigned int -> uint32_t
// unsigned short -> uint16_t
// signed short -> int16_t
// signed long -> int32_t
// signed long long -> int64_t

#pragma pack(push,1)

typedef struct
{
	uint8_t motion_num;			// current cycle
	uint8_t max_cycle;			// maximum number of cycles/mix (= 0 for single aspiration)
	uint8_t pos;					// position of the well in the strip
	uint8_t motion_type;			// command in execution (letter)
	uint8_t speed_char;			// pump speed (letter)
	uint8_t volume_char;			// pump volume (letter)
}well_data_struct;		// size = 6 bytes

typedef struct
{
	int64_t area;					// value of the calculated area
	int64_t integral;				// value of the calculated integral		[A/D units] -> (from 32 to 64 bits)
//	int64_t area_dx;		// area of the curve at the right side of the minimum   (considering as bottom line the minimum value)
	int64_t area_sx;		// area of the curve at the left side of the minimum	(considering as bottom line the minimum value)
	uint32_t start_time;				// start time linked to protocol
	uint32_t first_sample_val;			// value of the first sample			[A/D units] -> 24 bits
	uint32_t last_sample_val;			// value of the last sample				[A/D units] -> 24 bits
	uint32_t max_sample_val;		// maximum value among all the acquired samples		[A/D units] -> 24 bits
	uint32_t min_sample_val;			// minimum value among all the acquired samples		[A/D units] -> 24 bits
	uint32_t max_min_difference;	// difference between superMax and superMin in Industry algo	[A/D units] -> 24 bits
//	uint32_t max_cycles_val;			// superMax in Industry algo [A/D units] -> 24 bits
	uint32_t min_cycles_val;			// superMin in Industry algo [A/D units] -> 24 bits
	uint32_t low_threshold;				// low threshold to be used as compare for current motion  	[A/D units]
	uint32_t high_threshold;			// high threshold to be used as compare for current motion  	[A/D units]

    uint16_t max_sample_pos;		// position of the sample having the maximum value		-> 24 bits
    uint16_t min_sample_pos;			// position of the sample having the minimum value -> 24 bits
    uint16_t tot_sample;				// number of samples acquired

    uint8_t air_area_counter;		// counter of the air aspiration in a loop/macro in Industry algo

    int8_t curve_slope;		// curve slop in the final part
	uint8_t result_flag;				// result of the detection algorithm
    uint8_t algorithm;				// result of the detection algorithm
}well_sampling_struct;		// size = 70 bytes

#pragma pack(pop)

// copied as used in Section Application
const uint8_t elab_header_chars[] = {0x00, 0x81, 0x90, 0x00, 0x00, 0x00, 0x00, 0x00 };
const uint8_t elab_footer_chars[] = {0xff, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

// same defines used in Section Application
#define AREA_RATIO_CLINIC_PARAMETER				'1'
#define MIN_SAMPLE_POS_CLINIC_PARAMETER			'2'
#define SLOPE_PERCENTAGE_CLINIC_PARAMETER		'3'
#define SLOPE_THRESHOLD_CLINIC_PARAMETER		'4'
#define LOW_THRESHOLD_INDUSTRY_PARAMETER		'5'
#define HIGH_THRESHOLD_INDUSTRY_PARAMETER		'6'
#define CYCLES_PERCENTAGE_INDUSTRY_PARAMETER	'7'
#define CYCLES_MINIMUM_INDUSTRY_PARAMETER		'8'
#define CYCLES_THRESHOLD_INDUSTRY_PARAMETER		'9'
#define ALGORITHM_SELECTION_PARAMETER			'A'


#endif // SECTIONINCLUDE_H
