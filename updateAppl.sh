#!/bin/bash

#for ((i=20; i> 0; --i))
#do
#    echo "Wait $i s"
#    sleep 1
#done

#systemctl stop modular

#for ((i=10; i> 0; --i))
#do
#    echo "Wait $i s"
#    sleep 1
#done

folderBackup=/home/root/OLDAPP/
folderCRC=/home/root/CRC/

if [ -d $folderBackup ];then
    echo "folderBackup is present"
else
    echo "folderBackup is not present, so create it"
   mkdir OLDAPP
fi

echo "remove files in $folderBackup folder"
rm $folderBackup*

echo "copy application and crc files"
cp CanManager        $folderBackup
cp FwCore            $folderBackup
cp $folderCRC*.crc   $folderBackup

tar -xvf MasterBoard.tar

rm -rf MasterApplication.tar
rm -rf MasterBoard.tar
rm *.swu
rm -rf InstrumentPack.tar

reboot


